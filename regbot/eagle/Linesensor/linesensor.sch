<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.6.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting keepoldvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="11" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="11" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="11" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="Invisible" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="61" name="stand" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="no" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="bLogo" color="7" fill="1" visible="no" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="transistor-small-signal">
<description>&lt;b&gt;Small Signal Transistors&lt;/b&gt;&lt;p&gt;
Packages from :&lt;br&gt;
www.infineon.com; &lt;br&gt;
www.semiconductors.com;&lt;br&gt;
www.irf.com&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="SOT23">
<description>&lt;b&gt;SOT-23&lt;/b&gt;</description>
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.4224" y1="-0.6604" x2="-1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.6604" x2="-1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="51"/>
<smd name="3" x="0" y="1.1" dx="1" dy="1.4" layer="1"/>
<smd name="2" x="0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<smd name="1" x="-0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
<wire x1="-1.27" y1="-0.2159" x2="-1.27" y2="0.635" width="0.127" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-0.6858" y2="0.635" width="0.127" layer="21"/>
<wire x1="0.6858" y1="0.635" x2="1.27" y2="0.635" width="0.127" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.2159" width="0.127" layer="21"/>
<wire x1="0.254" y1="-0.635" x2="-0.254" y2="-0.635" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="N-MOS">
<wire x1="-1.27" y1="0" x2="-0.254" y2="0.381" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="0.381" x2="-0.254" y2="-0.381" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-0.381" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="0" x2="-0.889" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.032" x2="0" y2="-2.794" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="3.048" x2="1.27" y2="3.048" width="0.1524" layer="94"/>
<wire x1="1.27" y1="3.048" x2="1.27" y2="0.762" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0.762" x2="1.27" y2="0.508" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0.508" x2="1.27" y2="-2.794" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-2.794" x2="0" y2="-2.794" width="0.1524" layer="94"/>
<wire x1="1.778" y1="0" x2="0.762" y2="0" width="0.1524" layer="94"/>
<wire x1="0.762" y1="0" x2="1.27" y2="0.762" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0.762" x2="1.778" y2="0" width="0.1524" layer="94"/>
<wire x1="1.778" y1="0.762" x2="0.762" y2="0.762" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="2.032" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="2.032" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-2.032" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="0" x2="-0.381" y2="-0.254" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-0.254" x2="-0.381" y2="0.254" width="0.254" layer="94"/>
<wire x1="-0.381" y1="0.254" x2="-0.889" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0.508" x2="1.016" y2="0.127" width="0.254" layer="94"/>
<wire x1="1.016" y1="0.127" x2="1.524" y2="0.127" width="0.254" layer="94"/>
<wire x1="1.524" y1="0.127" x2="1.27" y2="0.508" width="0.254" layer="94"/>
<circle x="0" y="-2.794" radius="0.3592" width="0" layer="94"/>
<circle x="0" y="-2.032" radius="0.3592" width="0" layer="94"/>
<circle x="0" y="3.048" radius="0.3592" width="0" layer="94"/>
<text x="2.54" y="0" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="1.27" x2="-1.524" y2="2.54" layer="94"/>
<rectangle x1="-2.032" y1="-2.54" x2="-1.524" y2="-1.27" layer="94"/>
<rectangle x1="-2.032" y1="-0.762" x2="-1.524" y2="0.762" layer="94"/>
<pin name="G" x="-5.08" y="-2.54" visible="off" length="short" direction="pas"/>
<pin name="D" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="S" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
</symbol>
<symbol name="P-MOS">
<wire x1="0" y1="0" x2="-1.016" y2="0.381" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="0.381" x2="-1.016" y2="-0.381" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="-0.381" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="2.032" x2="0" y2="2.794" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="0" x2="-0.508" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.508" y1="0" x2="-0.381" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-3.048" x2="1.27" y2="-3.048" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-3.048" x2="1.27" y2="-0.254" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-0.254" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="2.794" width="0.1524" layer="94"/>
<wire x1="1.27" y1="2.794" x2="0" y2="2.794" width="0.1524" layer="94"/>
<wire x1="0.762" y1="-0.762" x2="1.778" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-0.762" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="0.762" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="0.762" y1="0" x2="1.778" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-2.032" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.032" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="2.032" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="0" x2="-0.889" y2="-0.127" width="0.254" layer="94"/>
<wire x1="-0.889" y1="-0.127" x2="-0.889" y2="0.127" width="0.254" layer="94"/>
<wire x1="-0.889" y1="0.127" x2="-0.508" y2="0" width="0.254" layer="94"/>
<wire x1="1.016" y1="-0.635" x2="1.524" y2="-0.635" width="0.254" layer="94"/>
<wire x1="1.524" y1="-0.635" x2="1.27" y2="-0.254" width="0.254" layer="94"/>
<wire x1="1.27" y1="-0.254" x2="1.016" y2="-0.635" width="0.254" layer="94"/>
<circle x="0" y="2.794" radius="0.3592" width="0" layer="94"/>
<circle x="0" y="2.032" radius="0.3592" width="0" layer="94"/>
<circle x="0" y="-3.048" radius="0.3592" width="0" layer="94"/>
<text x="2.54" y="0" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-2.54" x2="-1.524" y2="-1.27" layer="94"/>
<rectangle x1="-2.032" y1="1.27" x2="-1.524" y2="2.54" layer="94"/>
<rectangle x1="-2.032" y1="-0.762" x2="-1.524" y2="0.762" layer="94"/>
<pin name="G" x="-5.08" y="2.54" visible="off" length="short" direction="pas"/>
<pin name="D" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="S" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="BSS123" prefix="Q">
<description>&lt;b&gt;N-CHANNEL MOS FET&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="N-MOS" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BSS84" prefix="Q">
<description>&lt;b&gt;P-CHANNEL MOS FET&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="P-MOS" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-samtec">
<description>&lt;b&gt;Samtec Connectors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="SSW-102-02-S-S">
<description>&lt;b&gt;THROUGH-HOLE .025" SQ POST SOCKET&lt;/b&gt;&lt;p&gt;
Source: Samtec SSW.pdf</description>
<wire x1="-2.669" y1="1.155" x2="2.669" y2="1.155" width="0.2032" layer="21"/>
<wire x1="2.669" y1="1.155" x2="2.669" y2="-1.155" width="0.2032" layer="21"/>
<wire x1="2.669" y1="-1.155" x2="-2.669" y2="-1.155" width="0.2032" layer="21"/>
<wire x1="-2.669" y1="-1.155" x2="-2.669" y2="1.155" width="0.2032" layer="21"/>
<wire x1="-2.015" y1="0.755" x2="-0.515" y2="0.755" width="0.2032" layer="51"/>
<wire x1="-0.515" y1="0.755" x2="-0.515" y2="-0.745" width="0.2032" layer="51"/>
<wire x1="-0.515" y1="-0.745" x2="-2.015" y2="-0.745" width="0.2032" layer="51"/>
<wire x1="-2.015" y1="-0.745" x2="-2.015" y2="0.755" width="0.2032" layer="51"/>
<wire x1="0.525" y1="0.755" x2="2.025" y2="0.755" width="0.2032" layer="51"/>
<wire x1="2.025" y1="0.755" x2="2.025" y2="-0.745" width="0.2032" layer="51"/>
<wire x1="2.025" y1="-0.745" x2="0.525" y2="-0.745" width="0.2032" layer="51"/>
<wire x1="0.525" y1="-0.745" x2="0.525" y2="0.755" width="0.2032" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="1" diameter="1.5" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="1" diameter="1.5" shape="octagon"/>
<text x="-3.175" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="4.445" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
</package>
<package name="SSW-102-02-S-S-RA">
<description>&lt;b&gt;THROUGH-HOLE .025" SQ POST SOCKET&lt;/b&gt;&lt;p&gt;
Source: Samtec SSW.pdf</description>
<wire x1="-2.669" y1="-8.396" x2="2.669" y2="-8.396" width="0.2032" layer="21"/>
<wire x1="2.669" y1="-8.396" x2="2.669" y2="-0.106" width="0.2032" layer="21"/>
<wire x1="2.669" y1="-0.106" x2="-2.669" y2="-0.106" width="0.2032" layer="21"/>
<wire x1="-2.669" y1="-0.106" x2="-2.669" y2="-8.396" width="0.2032" layer="21"/>
<pad name="1" x="-1.27" y="1.524" drill="1" diameter="1.5" shape="octagon"/>
<pad name="2" x="1.27" y="1.524" drill="1" diameter="1.5" shape="octagon"/>
<text x="-1.865" y="-7.65" size="1.6764" layer="21" font="vector">1</text>
<text x="-3.175" y="-7.62" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="4.445" y="-7.62" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.524" y1="0" x2="-1.016" y2="1.778" layer="51"/>
<rectangle x1="1.016" y1="0" x2="1.524" y2="1.778" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="FPINV">
<wire x1="-1.778" y1="0.508" x2="0" y2="0.508" width="0.254" layer="94"/>
<wire x1="0" y1="0.508" x2="0" y2="-0.508" width="0.254" layer="94"/>
<wire x1="0" y1="-0.508" x2="-1.778" y2="-0.508" width="0.254" layer="94"/>
<text x="-2.54" y="2.54" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.048" y="0.762" size="1.524" layer="95" rot="R180">&gt;NAME</text>
<pin name="1" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="FPIN">
<wire x1="-1.778" y1="0.508" x2="0" y2="0.508" width="0.254" layer="94"/>
<wire x1="0" y1="0.508" x2="0" y2="-0.508" width="0.254" layer="94"/>
<wire x1="0" y1="-0.508" x2="-1.778" y2="-0.508" width="0.254" layer="94"/>
<text x="-3.048" y="0.762" size="1.524" layer="95" rot="R180">&gt;NAME</text>
<pin name="1" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SSW-102-02-S-S" prefix="X">
<description>&lt;b&gt;THROUGH-HOLE .025" SQ POST SOCKET&lt;/b&gt;&lt;p&gt;
Source: Samtec SSW.pdf</description>
<gates>
<gate name="-1" symbol="FPINV" x="0" y="0" addlevel="always"/>
<gate name="-2" symbol="FPIN" x="0" y="-2.54" addlevel="always"/>
</gates>
<devices>
<device name="" package="SSW-102-02-S-S">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="SSW-102-02-S-S" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="11P9351" constant="no"/>
</technology>
</technologies>
</device>
<device name="-RA" package="SSW-102-02-S-S-RA">
<connects>
<connect gate="-1" pin="1" pad="1"/>
<connect gate="-2" pin="1" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="SSW-102-02-S-S-RA" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="11P9352" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply2">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
Please keep in mind, that these devices are necessary for the
automatic wiring of the supply signals.&lt;p&gt;
The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-1.905" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="VDD">
<circle x="0" y="1.27" radius="1.27" width="0.254" layer="94"/>
<text x="-1.905" y="3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="VDD" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VDD" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="VDD" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="DINA4_P">
<frame x1="0" y1="0" x2="180.34" y2="264.16" columns="4" rows="4" layer="94" border-left="no" border-top="no" border-right="no" border-bottom="no"/>
</symbol>
<symbol name="DOCFIELD">
<wire x1="0" y1="0" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="101.6" y1="15.24" x2="87.63" y2="15.24" width="0.1016" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="71.12" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="15.24" width="0.1016" layer="94"/>
<wire x1="101.6" y1="15.24" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="0" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="87.63" y1="15.24" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="87.63" y1="15.24" x2="0" y2="15.24" width="0.1016" layer="94"/>
<wire x1="87.63" y1="5.08" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="101.6" y1="5.08" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="0" y1="15.24" x2="0" y2="22.86" width="0.1016" layer="94"/>
<wire x1="101.6" y1="35.56" x2="0" y2="35.56" width="0.1016" layer="94"/>
<wire x1="101.6" y1="35.56" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="0" y2="35.56" width="0.1016" layer="94"/>
<wire x1="101.6" y1="22.86" x2="101.6" y2="15.24" width="0.1016" layer="94"/>
<text x="1.27" y="1.27" size="2.54" layer="94">Date:</text>
<text x="12.7" y="1.27" size="2.54" layer="94">&gt;LAST_DATE_TIME</text>
<text x="72.39" y="1.27" size="2.54" layer="94">Sheet:</text>
<text x="86.36" y="1.27" size="2.54" layer="94">&gt;SHEET</text>
<text x="88.9" y="11.43" size="2.54" layer="94">REV:</text>
<text x="1.27" y="19.05" size="2.54" layer="94">TITLE:</text>
<text x="1.27" y="11.43" size="2.54" layer="94">Document Number:</text>
<text x="17.78" y="19.05" size="2.54" layer="94">&gt;DRAWING_NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="DINA4_P" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, portrait with extra doc field</description>
<gates>
<gate name="G$1" symbol="DINA4_P" x="0" y="0"/>
<gate name="G$2" symbol="DOCFIELD" x="78.74" y="0" addlevel="must"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="led">
<description>&lt;b&gt;LEDs&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;&lt;br&gt;
Extended by Federico Battaglin &lt;author&gt;&amp;lt;federico.rd@fdpinternational.com&amp;gt;&lt;/author&gt; with DUOLED</description>
<packages>
<package name="1206">
<description>&lt;b&gt;CHICAGO MINIATURE LAMP, INC.&lt;/b&gt;&lt;p&gt;
7022X Series SMT LEDs 1206 Package Size</description>
<wire x1="1.55" y1="-0.75" x2="-1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="-0.75" x2="-1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="0.75" x2="1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.55" y1="0.75" x2="1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-0.55" y1="-0.5" x2="0.55" y2="-0.5" width="0.1016" layer="21" curve="95.452622"/>
<wire x1="-0.55" y1="-0.5" x2="-0.55" y2="0.5" width="0.1016" layer="51" curve="-84.547378"/>
<wire x1="-0.55" y1="0.5" x2="0.55" y2="0.5" width="0.1016" layer="21" curve="-95.452622"/>
<wire x1="0.55" y1="0.5" x2="0.55" y2="-0.5" width="0.1016" layer="51" curve="-84.547378"/>
<smd name="A" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="C" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1" y1="-0.1" x2="0.1" y2="0.1" layer="21"/>
<rectangle x1="0.45" y1="-0.7" x2="0.8" y2="-0.45" layer="51"/>
<rectangle x1="0.8" y1="-0.7" x2="0.9" y2="0.5" layer="51"/>
<rectangle x1="0.8" y1="0.55" x2="0.9" y2="0.7" layer="51"/>
<rectangle x1="-0.9" y1="-0.7" x2="-0.8" y2="0.5" layer="51"/>
<rectangle x1="-0.9" y1="0.55" x2="-0.8" y2="0.7" layer="51"/>
<rectangle x1="0.45" y1="-0.7" x2="0.6" y2="-0.45" layer="21"/>
</package>
<package name="LD260">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
5 mm, square, Siemens</description>
<wire x1="-1.27" y1="-1.27" x2="0" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.27" x2="1.27" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="0" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="1.27" x2="-1.27" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.889" x2="1.27" y2="0" width="0.1524" layer="51"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.889" x2="-1.27" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="0.889" width="0.1524" layer="51"/>
<wire x1="0" y1="1.27" x2="0.9917" y2="0.7934" width="0.1524" layer="21" curve="-51.33923"/>
<wire x1="-0.9917" y1="0.7934" x2="0" y2="1.27" width="0.1524" layer="21" curve="-51.33923"/>
<wire x1="0" y1="-1.27" x2="0.9917" y2="-0.7934" width="0.1524" layer="21" curve="51.33923"/>
<wire x1="-0.9917" y1="-0.7934" x2="0" y2="-1.27" width="0.1524" layer="21" curve="51.33923"/>
<wire x1="0.9558" y1="-0.8363" x2="1.27" y2="0" width="0.1524" layer="51" curve="41.185419"/>
<wire x1="0.9756" y1="0.813" x2="1.2699" y2="0" width="0.1524" layer="51" curve="-39.806332"/>
<wire x1="-1.27" y1="0" x2="-0.9643" y2="-0.8265" width="0.1524" layer="51" curve="40.600331"/>
<wire x1="-1.27" y1="0" x2="-0.9643" y2="0.8265" width="0.1524" layer="51" curve="-40.600331"/>
<wire x1="-0.889" y1="0" x2="0" y2="0.889" width="0.1524" layer="51" curve="-90"/>
<wire x1="-0.508" y1="0" x2="0" y2="0.508" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.508" x2="0.508" y2="0" width="0.1524" layer="21" curve="90"/>
<wire x1="0" y1="-0.889" x2="0.889" y2="0" width="0.1524" layer="51" curve="90"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.2954" y="1.4732" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-2.4892" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="1.27" y1="-0.635" x2="2.032" y2="0.635" layer="51"/>
<rectangle x1="1.905" y1="-0.635" x2="2.032" y2="0.635" layer="21"/>
</package>
<package name="LED2X5">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
2 x 5 mm, rectangle</description>
<wire x1="-2.54" y1="-1.27" x2="2.54" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.27" x2="-2.54" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="0" x2="0.508" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="0.381" x2="-0.508" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="0.381" x2="0.508" y2="0" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0" x2="1.778" y2="0" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0" x2="-0.508" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0.381" x2="0.508" y2="0" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0" x2="0.508" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="0.889" y1="-0.254" x2="1.143" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.762" x2="1.143" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.762" x2="0.9398" y2="-0.6096" width="0.1524" layer="51"/>
<wire x1="0.9398" y1="-0.6096" x2="1.143" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="1.397" y1="-0.254" x2="1.651" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-0.762" x2="1.651" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-0.762" x2="1.4478" y2="-0.6096" width="0.1524" layer="51"/>
<wire x1="1.4478" y1="-0.6096" x2="1.651" y2="-0.508" width="0.1524" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.397" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-2.413" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.159" y1="-1.27" x2="2.413" y2="1.27" layer="21"/>
</package>
<package name="LED3MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
3 mm, round</description>
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="-1.524" y1="0" x2="-1.1708" y2="0.9756" width="0.1524" layer="51" curve="-39.80361"/>
<wire x1="-1.524" y1="0" x2="-1.1391" y2="-1.0125" width="0.1524" layer="51" curve="41.633208"/>
<wire x1="1.1571" y1="0.9918" x2="1.524" y2="0" width="0.1524" layer="51" curve="-40.601165"/>
<wire x1="1.1708" y1="-0.9756" x2="1.524" y2="0" width="0.1524" layer="51" curve="39.80361"/>
<wire x1="0" y1="1.524" x2="1.2401" y2="0.8858" width="0.1524" layer="51" curve="-54.461337"/>
<wire x1="-1.2192" y1="0.9144" x2="0" y2="1.524" width="0.1524" layer="51" curve="-53.130102"/>
<wire x1="0" y1="-1.524" x2="1.203" y2="-0.9356" width="0.1524" layer="51" curve="52.126876"/>
<wire x1="-1.203" y1="-0.9356" x2="0" y2="-1.524" width="0.1524" layer="51" curve="52.126876"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0" y1="-1.016" x2="1.016" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="51" curve="-50.193108"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="51" curve="-61.926949"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="51" curve="49.763022"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="51" curve="60.255215"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="2.64848125" y="0.424765625" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="2.64848125" y="-1.585353125" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<wire x1="2.475209375" y1="1.357528125" x2="2.475209375" y2="-1.357528125" width="0.254" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="-1.27" width="0.254" layer="21" curve="53.130102"/>
</package>
<package name="LED5MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
5 mm, round</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LSU260">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
1 mm, round, Siemens</description>
<wire x1="0" y1="-0.508" x2="-1.143" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="-0.508" x2="-1.143" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="0.508" x2="0" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="-0.254" x2="-1.397" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="-0.254" x2="-1.143" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-1.397" y1="-0.254" x2="-1.397" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-1.397" y1="0.254" x2="-1.143" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="0.254" x2="-1.143" y2="0.508" width="0.1524" layer="51"/>
<wire x1="0.508" y1="-0.254" x2="1.397" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="1.397" y1="-0.254" x2="1.397" y2="0.254" width="0.1524" layer="51"/>
<wire x1="1.397" y1="0.254" x2="0.508" y2="0.254" width="0.1524" layer="51"/>
<wire x1="0.381" y1="-0.381" x2="0.254" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-0.508" x2="-0.254" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="-0.381" x2="-0.254" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.381" y1="0.381" x2="0.254" y2="0.508" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.508" x2="-0.254" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0.381" x2="-0.254" y2="0.508" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.254" x2="0.254" y2="0" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.254" y1="0" x2="0" y2="0.254" width="0.1524" layer="21" curve="-90"/>
<wire x1="0.381" y1="-0.381" x2="0.381" y2="0.381" width="0.1524" layer="21" curve="90"/>
<circle x="0" y="0" radius="0.508" width="0.1524" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.2954" y="0.8382" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-1.8542" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.397" y1="-0.254" x2="-1.143" y2="0.254" layer="51"/>
<rectangle x1="0.508" y1="-0.254" x2="1.397" y2="0.254" layer="51"/>
</package>
<package name="LZR181">
<description>&lt;B&gt;LED BLOCK&lt;/B&gt;&lt;p&gt;
1 LED, Siemens</description>
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.889" x2="1.27" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.889" x2="-1.27" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="0" x2="0" y2="0.889" width="0.1524" layer="51" curve="-90"/>
<wire x1="-0.508" y1="0" x2="0" y2="0.508" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.508" x2="0.508" y2="0" width="0.1524" layer="21" curve="90"/>
<wire x1="0" y1="-0.889" x2="0.889" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-0.8678" y1="0.7439" x2="0" y2="1.143" width="0.1524" layer="21" curve="-49.396139"/>
<wire x1="0" y1="1.143" x2="0.8678" y2="0.7439" width="0.1524" layer="21" curve="-49.396139"/>
<wire x1="-0.8678" y1="-0.7439" x2="0" y2="-1.143" width="0.1524" layer="21" curve="49.396139"/>
<wire x1="0" y1="-1.143" x2="0.8678" y2="-0.7439" width="0.1524" layer="21" curve="49.396139"/>
<wire x1="0.8678" y1="0.7439" x2="1.143" y2="0" width="0.1524" layer="51" curve="-40.604135"/>
<wire x1="0.8678" y1="-0.7439" x2="1.143" y2="0" width="0.1524" layer="51" curve="40.604135"/>
<wire x1="-1.143" y1="0" x2="-0.8678" y2="0.7439" width="0.1524" layer="51" curve="-40.604135"/>
<wire x1="-1.143" y1="0" x2="-0.8678" y2="-0.7439" width="0.1524" layer="51" curve="40.604135"/>
<wire x1="-1.27" y1="1.27" x2="1.27" y2="1.27" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.2954" y="1.4732" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-2.4892" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="1.27" y1="-0.889" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.27" y2="0.254" layer="51"/>
</package>
<package name="Q62902-B152">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="-2.9718" y1="-1.8542" x2="-2.9718" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-2.9718" y1="-0.254" x2="-2.9718" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-2.9718" y1="0.254" x2="-2.9718" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="2.9718" y1="-1.8542" x2="-2.1082" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-2.1082" y1="-1.8542" x2="-2.54" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.8542" x2="-2.1082" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.1082" y1="1.8542" x2="2.9718" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.8542" x2="-2.54" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.8542" x2="-2.9718" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.8542" x2="-2.9718" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.9718" y1="0.254" x2="-2.9718" y2="-0.254" width="0.1524" layer="21" curve="180"/>
<wire x1="-1.1486" y1="0.8814" x2="0" y2="1.4478" width="0.1524" layer="21" curve="-52.498642"/>
<wire x1="0" y1="1.4478" x2="1.1476" y2="0.8827" width="0.1524" layer="21" curve="-52.433716"/>
<wire x1="-1.1351" y1="-0.8987" x2="0" y2="-1.4478" width="0.1524" layer="21" curve="51.629985"/>
<wire x1="0" y1="-1.4478" x2="1.1305" y2="-0.9044" width="0.1524" layer="21" curve="51.339172"/>
<wire x1="1.1281" y1="-0.9074" x2="1.4478" y2="0" width="0.1524" layer="51" curve="38.811177"/>
<wire x1="1.1401" y1="0.8923" x2="1.4478" y2="0" width="0.1524" layer="51" curve="-38.048073"/>
<wire x1="-1.4478" y1="0" x2="-1.1305" y2="-0.9044" width="0.1524" layer="51" curve="38.659064"/>
<wire x1="-1.4478" y1="0" x2="-1.1456" y2="0.8853" width="0.1524" layer="51" curve="-37.696376"/>
<wire x1="0" y1="1.7018" x2="1.4674" y2="0.8618" width="0.1524" layer="21" curve="-59.573488"/>
<wire x1="-1.4618" y1="0.8714" x2="0" y2="1.7018" width="0.1524" layer="21" curve="-59.200638"/>
<wire x1="0" y1="-1.7018" x2="1.4571" y2="-0.8793" width="0.1524" layer="21" curve="58.891781"/>
<wire x1="-1.4571" y1="-0.8793" x2="0" y2="-1.7018" width="0.1524" layer="21" curve="58.891781"/>
<wire x1="-1.7018" y1="0" x2="-1.4447" y2="0.8995" width="0.1524" layer="51" curve="-31.907626"/>
<wire x1="-1.7018" y1="0" x2="-1.4502" y2="-0.8905" width="0.1524" layer="51" curve="31.551992"/>
<wire x1="1.4521" y1="0.8874" x2="1.7018" y2="0" width="0.1524" layer="51" curve="-31.429586"/>
<wire x1="1.4459" y1="-0.8975" x2="1.7018" y2="0" width="0.1524" layer="51" curve="31.828757"/>
<wire x1="-2.1082" y1="1.8542" x2="-2.1082" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0.0539" y1="-1.0699" x2="1.0699" y2="-0.0539" width="0.1524" layer="51" curve="90"/>
<wire x1="2.9718" y1="1.8542" x2="2.9718" y2="-1.8542" width="0.1524" layer="21"/>
<pad name="K" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="A" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-1.905" y="2.286" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.905" y="-3.556" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="Q62902-B153">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="-5.5118" y1="-3.5052" x2="-5.5118" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-5.5118" y1="-0.254" x2="-5.5118" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-5.5118" y1="0.254" x2="-5.5118" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="5.5118" y1="-3.5052" x2="-4.6482" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-4.6482" y1="-3.5052" x2="-5.08" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="3.5052" x2="-4.6482" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-4.6482" y1="3.5052" x2="5.5118" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-3.5052" x2="-5.08" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-3.5052" x2="-5.5118" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="3.5052" x2="-5.5118" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-5.5118" y1="0.254" x2="-5.5118" y2="-0.254" width="0.1524" layer="21" curve="180"/>
<wire x1="-4.6482" y1="3.5052" x2="-4.6482" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="5.5118" y1="3.5052" x2="5.5118" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-2.54" y2="-1.905" width="0.254" layer="21"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-2.2129" y1="0.0539" x2="-0.0539" y2="2.2129" width="0.1524" layer="51" curve="-90.010616"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="3.175" width="0.254" layer="21"/>
<pad name="A" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="K" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-4.191" y="3.937" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-5.08" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="Q62902-B155">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="-1.27" y1="-3.048" x2="-1.27" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="10.033" y1="3.048" x2="2.921" y2="3.048" width="0.1524" layer="21"/>
<wire x1="10.033" y1="3.048" x2="10.033" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-3.048" x2="2.921" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-3.048" x2="2.921" y2="3.048" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-3.048" x2="10.033" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="2.921" y1="3.048" x2="-1.27" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="-5.207" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="-2.54" x2="-1.27" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.54" x2="-5.207" y2="-2.54" width="0.1524" layer="21" curve="180"/>
<wire x1="-6.985" y1="0.635" x2="-6.985" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="1.397" x2="-6.096" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="1.905" x2="-5.207" y2="-1.905" width="0.1524" layer="21"/>
<pad name="K" x="7.62" y="1.27" drill="0.8128" shape="long"/>
<pad name="A" x="7.62" y="-1.27" drill="0.8128" shape="long"/>
<text x="3.302" y="-2.794" size="1.016" layer="21" ratio="14">A+</text>
<text x="3.302" y="1.778" size="1.016" layer="21" ratio="14">K-</text>
<text x="11.684" y="-2.794" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="0.635" y="-4.445" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.921" y1="1.016" x2="6.731" y2="1.524" layer="21"/>
<rectangle x1="2.921" y1="-1.524" x2="6.731" y2="-1.016" layer="21"/>
<hole x="0" y="0" drill="0.8128"/>
</package>
<package name="Q62902-B156">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90"/>
<wire x1="0.0539" y1="-2.2129" x2="2.2129" y2="-0.0539" width="0.1524" layer="51" curve="90.005308"/>
<wire x1="2.54" y1="3.81" x2="3.81" y2="2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="3.81" x2="-3.81" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-3.81" x2="-3.81" y2="3.81" width="0.1524" layer="21"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-3.81" x2="-2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-3.302" x2="-2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-3.81" x2="2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-3.302" x2="2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-3.302" x2="-2.54" y2="-3.302" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="3.175" width="0.254" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="K" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-3.81" y="4.0894" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.7846" y="-5.3594" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-3.556" y="-3.302" size="1.016" layer="21" ratio="14">+</text>
<text x="2.794" y="-3.302" size="1.016" layer="21" ratio="14">-</text>
</package>
<package name="SFH480">
<description>&lt;B&gt;IR LED&lt;/B&gt;&lt;p&gt;
infrared emitting diode, Infineon
TO-18, lead spacing 2.54 mm, cathode marking&lt;p&gt;
Inifineon</description>
<wire x1="-2.159" y1="1.524" x2="-2.794" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="0" y1="1.778" x2="1.5358" y2="0.8959" width="0.1524" layer="21" curve="-59.743278"/>
<wire x1="-1.5358" y1="0.8959" x2="0" y2="1.778" width="0.1524" layer="21" curve="-59.743278"/>
<wire x1="-1.5358" y1="-0.8959" x2="0" y2="-1.778" width="0.1524" layer="21" curve="59.743278"/>
<wire x1="0" y1="-1.778" x2="1.5358" y2="-0.8959" width="0.1524" layer="21" curve="59.743278"/>
<wire x1="1.5142" y1="0.9318" x2="1.778" y2="0" width="0.1524" layer="51" curve="-31.606487"/>
<wire x1="1.5" y1="-0.9546" x2="1.778" y2="0" width="0.1524" layer="51" curve="32.472615"/>
<wire x1="-1.778" y1="0" x2="-1.5142" y2="-0.9318" width="0.1524" layer="51" curve="31.606487"/>
<wire x1="-1.778" y1="0" x2="-1.5" y2="0.9546" width="0.1524" layer="51" curve="-32.472615"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0.0539" y1="-1.0699" x2="1.0699" y2="-0.0539" width="0.1524" layer="51" curve="90"/>
<circle x="0" y="0" radius="2.667" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="2.413" width="0.254" layer="21"/>
<pad name="K" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="A" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-1.27" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-4.318" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="SFH482">
<description>&lt;B&gt;IR LED&lt;/B&gt;&lt;p&gt;
infrared emitting diode, Infineon
TO-18, lead spacing 2.54 mm, cathode marking&lt;p&gt;
Inifineon</description>
<wire x1="-2.159" y1="1.524" x2="-2.794" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="0" y1="1.778" x2="1.5358" y2="0.8959" width="0.1524" layer="21" curve="-59.743278"/>
<wire x1="-1.5358" y1="0.8959" x2="0" y2="1.778" width="0.1524" layer="21" curve="-59.743278"/>
<wire x1="-1.5358" y1="-0.8959" x2="0" y2="-1.778" width="0.1524" layer="21" curve="59.743278"/>
<wire x1="0" y1="-1.778" x2="1.5358" y2="-0.8959" width="0.1524" layer="21" curve="59.743278"/>
<wire x1="1.5142" y1="0.9318" x2="1.778" y2="0" width="0.1524" layer="51" curve="-31.606487"/>
<wire x1="1.5" y1="-0.9546" x2="1.778" y2="0" width="0.1524" layer="51" curve="32.472615"/>
<wire x1="-1.778" y1="0" x2="-1.5142" y2="-0.9318" width="0.1524" layer="51" curve="31.606487"/>
<wire x1="-1.778" y1="0" x2="-1.5" y2="0.9546" width="0.1524" layer="51" curve="-32.472615"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0.0539" y1="-1.0699" x2="1.0699" y2="-0.0539" width="0.1524" layer="51" curve="90"/>
<circle x="0" y="0" radius="2.667" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="2.413" width="0.254" layer="21"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-1.27" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-4.318" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="U57X32">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
rectangle, 5.7 x 3.2 mm</description>
<wire x1="-3.175" y1="1.905" x2="3.175" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.905" x2="3.175" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.905" x2="-3.175" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.905" x2="-3.175" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="1.397" x2="2.667" y2="1.397" width="0.1524" layer="21"/>
<wire x1="2.667" y1="-1.397" x2="2.667" y2="1.397" width="0.1524" layer="21"/>
<wire x1="2.667" y1="-1.397" x2="-2.667" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="1.397" x2="-2.667" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.016" x2="2.54" y2="1.016" width="0.1524" layer="51"/>
<wire x1="2.286" y1="1.27" x2="2.286" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="0.508" x2="2.54" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="-0.508" x2="2.54" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="-1.016" x2="2.54" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.778" y1="1.27" x2="-1.778" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="1.27" x2="-0.762" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="1.27" x2="-0.254" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="0.254" y1="1.27" x2="0.254" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="0.762" y1="1.27" x2="0.762" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.778" y1="1.27" x2="1.778" y2="-1.27" width="0.1524" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="3.683" y="0.254" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.683" y="-1.524" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="IRL80A">
<description>&lt;B&gt;IR LED&lt;/B&gt;&lt;p&gt;
IR transmitter Siemens</description>
<wire x1="0.889" y1="2.286" x2="0.889" y2="1.778" width="0.1524" layer="21"/>
<wire x1="0.889" y1="1.778" x2="0.889" y2="0.762" width="0.1524" layer="51"/>
<wire x1="0.889" y1="0.762" x2="0.889" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.889" y1="-0.635" x2="0.889" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="0.889" y1="-1.778" x2="0.889" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="0.889" y1="-2.286" x2="-0.889" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="2.286" x2="-0.889" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="1.778" x2="-0.889" y2="0.762" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="0.762" x2="-0.889" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-0.762" x2="-0.889" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="-1.778" x2="-0.889" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="2.286" x2="0.889" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-0.762" x2="-0.889" y2="0.762" width="0.1524" layer="21" curve="-180"/>
<wire x1="-1.397" y1="0.254" x2="-1.397" y2="-0.254" width="0.0508" layer="21"/>
<wire x1="-1.143" y1="0.508" x2="-1.143" y2="-0.508" width="0.0508" layer="21"/>
<pad name="K" x="0" y="1.27" drill="0.8128" shape="octagon"/>
<pad name="A" x="0" y="-1.27" drill="0.8128" shape="octagon"/>
<text x="1.27" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.27" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="P-LCC-2">
<description>&lt;b&gt;TOPLED® High-optical Power LED (HOP)&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... ls_t675.pdf</description>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.6" x2="-1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-0.85" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="-1.6" x2="1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-1.6" x2="1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="1.6" x2="1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="1.6" x2="-1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="1.8" x2="1.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="1.1" y1="1.8" x2="1.1" y2="1.6" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-1.1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="-1.8" x2="1.1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="1.1" y1="-1.8" x2="1.1" y2="-1.6" width="0.1016" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.1" width="0.2032" layer="51"/>
<smd name="C" x="0" y="-2.75" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="2.75" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="-2.54" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-0.635" y="2.54" size="1.27" layer="21">A</text>
<text x="-0.635" y="-3.81" size="1.27" layer="21">C</text>
<rectangle x1="-1.3" y1="-2.25" x2="1.3" y2="-0.75" layer="31"/>
<rectangle x1="-1.3" y1="0.75" x2="1.3" y2="2.25" layer="31"/>
<rectangle x1="-0.25" y1="-0.25" x2="0.25" y2="0.25" layer="21"/>
<rectangle x1="-1.4" y1="0.65" x2="1.4" y2="2.35" layer="29"/>
<rectangle x1="-1.4" y1="-2.35" x2="1.4" y2="-0.65" layer="29"/>
</package>
<package name="OSRAM-MINI-TOP-LED">
<description>&lt;b&gt;BLUE LINETM Hyper Mini TOPLED® Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LB M676.pdf</description>
<wire x1="-0.6" y1="0.9" x2="-0.6" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="-0.9" x2="-0.4" y2="-0.9" width="0.1016" layer="51"/>
<wire x1="-0.4" y1="-0.9" x2="0.6" y2="-0.9" width="0.1016" layer="51"/>
<wire x1="0.6" y1="-0.9" x2="0.6" y2="0.9" width="0.1016" layer="51"/>
<wire x1="0.6" y1="0.9" x2="-0.6" y2="0.9" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="0.95" x2="-0.45" y2="1.1" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="1.1" x2="0.45" y2="1.1" width="0.1016" layer="51"/>
<wire x1="0.45" y1="1.1" x2="0.45" y2="0.95" width="0.1016" layer="51"/>
<wire x1="-0.6" y1="-0.7" x2="-0.4" y2="-0.9" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="-0.9" x2="-0.45" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="-1.1" x2="0.45" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="0.45" y1="-1.1" x2="0.45" y2="-0.95" width="0.1016" layer="51"/>
<smd name="A" x="0" y="2.6" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="C" x="0" y="-2.6" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="-0.635" y="1.905" size="1.27" layer="21">A</text>
<text x="-0.635" y="-3.175" size="1.27" layer="21">C</text>
<text x="-2.54" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.5" y1="0.6" x2="0.5" y2="1.4" layer="29"/>
<rectangle x1="-0.5" y1="-1.4" x2="0.5" y2="-0.6" layer="29"/>
<rectangle x1="-0.15" y1="-0.6" x2="0.15" y2="-0.3" layer="51"/>
<rectangle x1="-0.45" y1="0.65" x2="0.45" y2="1.35" layer="31"/>
<rectangle x1="-0.45" y1="-1.35" x2="0.45" y2="-0.65" layer="31"/>
</package>
<package name="OSRAM-SIDELED">
<description>&lt;b&gt;Super SIDELED® High-Current LED&lt;/b&gt;&lt;p&gt;
LG A672, LP A672 &lt;br&gt;
Source: http://www.osram.convergy.de/ ... LG_LP_A672.pdf (2004.05.13)</description>
<wire x1="-1.85" y1="-2.05" x2="-1.85" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.85" y1="-0.75" x2="-1.7" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.7" y1="-0.75" x2="-1.7" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.7" y1="0.75" x2="-1.85" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.85" y1="0.75" x2="-1.85" y2="2.05" width="0.1016" layer="51"/>
<wire x1="-1.85" y1="2.05" x2="0.9" y2="2.05" width="0.1016" layer="51"/>
<wire x1="0.9" y1="2.05" x2="0.9" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="0.9" y1="-2.05" x2="-1.85" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="0.9" y1="-2.05" x2="1.05" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="1.05" y1="-2.05" x2="1.85" y2="-1.85" width="0.1016" layer="51"/>
<wire x1="1.85" y1="-1.85" x2="1.85" y2="1.85" width="0.1016" layer="51"/>
<wire x1="1.85" y1="1.85" x2="1.05" y2="2.05" width="0.1016" layer="51"/>
<wire x1="1.05" y1="2.05" x2="0.9" y2="2.05" width="0.1016" layer="51"/>
<wire x1="1.05" y1="2.05" x2="1.05" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="-0.55" y1="-0.9" x2="-0.55" y2="0.9" width="0.1016" layer="51" curve="-167.319617"/>
<wire x1="-0.55" y1="-0.9" x2="0.85" y2="-1.2" width="0.1016" layer="51" style="shortdash"/>
<wire x1="-0.55" y1="0.9" x2="0.85" y2="1.2" width="0.1016" layer="51" style="shortdash"/>
<smd name="C" x="0" y="-2.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="2.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="0.635" y="-3.175" size="1.27" layer="21" rot="R90">C</text>
<text x="0.635" y="2.54" size="1.27" layer="21" rot="R90">A</text>
<text x="-2.54" y="-2.54" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-2.54" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.1" y1="-2.2" x2="2.1" y2="-0.4" layer="29"/>
<rectangle x1="-2.1" y1="0.4" x2="2.1" y2="2.2" layer="29"/>
<rectangle x1="-1.9" y1="-2.1" x2="1.9" y2="-0.6" layer="31"/>
<rectangle x1="-1.9" y1="0.6" x2="1.9" y2="2.1" layer="31"/>
<rectangle x1="-1.85" y1="-2.05" x2="-0.7" y2="-1" layer="51"/>
</package>
<package name="SMART-LED">
<description>&lt;b&gt;SmartLEDTM Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY L896.pdf</description>
<wire x1="-0.35" y1="0.6" x2="0.35" y2="0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="0.35" y1="0.6" x2="0.35" y2="-0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.6" x2="0.15" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="0.15" y1="-0.6" x2="-0.35" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="-0.35" y1="-0.6" x2="-0.35" y2="0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.4" x2="0.15" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<smd name="A" x="0" y="0.725" dx="0.35" dy="0.35" layer="1"/>
<smd name="B" x="0" y="-0.725" dx="0.35" dy="0.35" layer="1"/>
<text x="-0.635" y="-0.635" size="1.016" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-0.635" size="1.016" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.15" y1="-0.35" x2="0.15" y2="-0.05" layer="21"/>
<rectangle x1="-0.15" y1="0.6" x2="0.15" y2="0.85" layer="51"/>
<rectangle x1="-0.15" y1="-0.85" x2="0.15" y2="-0.6" layer="51"/>
</package>
<package name="P-LCC-2-TOPLED-RG">
<description>&lt;b&gt;Hyper TOPLED® RG Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY T776.pdf</description>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.6" x2="-1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-0.85" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="-1.6" x2="1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-1.6" x2="1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="1.6" x2="1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="1.6" x2="-1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.1" y2="2.45" width="0.1016" layer="51"/>
<wire x1="1.1" y1="2.45" x2="1.1" y2="1.6" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-1.1" y2="-2.45" width="0.1016" layer="51"/>
<wire x1="1.1" y1="-2.45" x2="1.1" y2="-1.6" width="0.1016" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.1" width="0.2032" layer="21"/>
<smd name="C" x="0" y="-3.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="3.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="-2.54" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-0.635" y="3.29" size="1.27" layer="21">A</text>
<text x="-0.635" y="-4.56" size="1.27" layer="21">C</text>
<rectangle x1="-1.3" y1="-3" x2="1.3" y2="-1.5" layer="31"/>
<rectangle x1="-1.3" y1="1.5" x2="1.3" y2="3" layer="31"/>
<rectangle x1="-0.25" y1="-0.25" x2="0.25" y2="0.25" layer="21"/>
<rectangle x1="-1.15" y1="2.4" x2="1.15" y2="2.7" layer="51"/>
<rectangle x1="-1.15" y1="-2.7" x2="1.15" y2="-2.4" layer="51"/>
<rectangle x1="-1.5" y1="1.5" x2="1.5" y2="3.2" layer="29"/>
<rectangle x1="-1.5" y1="-3.2" x2="1.5" y2="-1.5" layer="29"/>
<hole x="0" y="0" drill="2.8"/>
</package>
<package name="MICRO-SIDELED">
<description>&lt;b&gt;Hyper Micro SIDELED®&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY Y876.pdf</description>
<wire x1="0.65" y1="1.1" x2="-0.1" y2="1.1" width="0.1016" layer="51"/>
<wire x1="-0.1" y1="1.1" x2="-0.35" y2="1" width="0.1016" layer="51"/>
<wire x1="-0.35" y1="1" x2="-0.35" y2="-0.9" width="0.1016" layer="21"/>
<wire x1="-0.35" y1="-0.9" x2="-0.1" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="-0.1" y1="-1.1" x2="0.65" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="0.65" y1="-1.1" x2="0.65" y2="1.1" width="0.1016" layer="21"/>
<wire x1="0.6" y1="0.9" x2="0.25" y2="0.7" width="0.0508" layer="21"/>
<wire x1="0.25" y1="0.7" x2="0.25" y2="-0.7" width="0.0508" layer="21"/>
<wire x1="0.25" y1="-0.7" x2="0.6" y2="-0.9" width="0.0508" layer="21"/>
<smd name="A" x="0" y="1.95" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<smd name="C" x="0" y="-1.95" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<text x="-1.27" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.4" y1="1.1" x2="0.4" y2="1.8" layer="29"/>
<rectangle x1="-0.4" y1="-1.8" x2="0.4" y2="-1.1" layer="29"/>
<rectangle x1="-0.35" y1="-1.75" x2="0.35" y2="-1.15" layer="31"/>
<rectangle x1="-0.35" y1="1.15" x2="0.35" y2="1.75" layer="31"/>
<rectangle x1="-0.125" y1="1.125" x2="0.125" y2="1.75" layer="51"/>
<rectangle x1="-0.125" y1="-1.75" x2="0.125" y2="-1.125" layer="51"/>
</package>
<package name="P-LCC-4">
<description>&lt;b&gt;Power TOPLED®&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LA_LY E67B.pdf</description>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.6" x2="-1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1" y1="-1.6" x2="-0.85" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1" y1="-1.6" x2="1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-1.6" x2="1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="1.6" x2="1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="1.6" x2="-1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1" y1="1.6" x2="-1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1" y1="1.6" x2="-1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="-1" y1="1.8" x2="-0.5" y2="1.8" width="0.1016" layer="51"/>
<wire x1="-0.5" y1="1.8" x2="-0.5" y2="1.65" width="0.1016" layer="51"/>
<wire x1="0.5" y1="1.65" x2="0.5" y2="1.8" width="0.1016" layer="51"/>
<wire x1="0.5" y1="1.8" x2="1.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="1.1" y1="1.8" x2="1.1" y2="1.6" width="0.1016" layer="51"/>
<wire x1="-1" y1="-1.6" x2="-1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="-1" y1="-1.8" x2="-0.5" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="-0.5" y1="-1.8" x2="-0.5" y2="-1.65" width="0.1016" layer="51"/>
<wire x1="0.5" y1="-1.65" x2="0.5" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="0.5" y1="-1.8" x2="1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="1" y1="-1.8" x2="1" y2="-1.6" width="0.1016" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.1" width="0.2032" layer="51"/>
<smd name="A" x="-2" y="3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<smd name="C@3" x="2" y="3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<smd name="C@4" x="2" y="-3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<smd name="C@1" x="-2" y="-3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<text x="-3.81" y="-2.54" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="5.08" y="-2.54" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-1.905" y="-3.81" size="1.27" layer="21">C</text>
<text x="-1.905" y="2.54" size="1.27" layer="21">A</text>
<text x="1.27" y="2.54" size="1.27" layer="21">C</text>
<text x="1.27" y="-3.81" size="1.27" layer="21">C</text>
<rectangle x1="-1.15" y1="0.75" x2="-0.35" y2="1.85" layer="29"/>
<rectangle x1="0.35" y1="0.75" x2="1.15" y2="1.85" layer="29"/>
<rectangle x1="0.35" y1="-1.85" x2="1.15" y2="-0.75" layer="29"/>
<rectangle x1="-1.15" y1="-1.85" x2="-0.35" y2="-0.75" layer="29"/>
<rectangle x1="-1.1" y1="-1.8" x2="-0.4" y2="-0.8" layer="31"/>
<rectangle x1="0.4" y1="-1.8" x2="1.1" y2="-0.8" layer="31"/>
<rectangle x1="0.4" y1="0.8" x2="1.1" y2="1.8" layer="31"/>
<rectangle x1="-1.1" y1="0.8" x2="-0.4" y2="1.8" layer="31"/>
<rectangle x1="-0.2" y1="-0.2" x2="0.2" y2="0.2" layer="21"/>
</package>
<package name="CHIP-LED0603">
<description>&lt;b&gt;Hyper CHIPLED Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
LB Q993&lt;br&gt;
Source: http://www.osram.convergy.de/ ... Lb_q993.pdf</description>
<wire x1="-0.4" y1="0.45" x2="-0.4" y2="-0.45" width="0.1016" layer="51"/>
<wire x1="0.4" y1="0.45" x2="0.4" y2="-0.45" width="0.1016" layer="51"/>
<smd name="C" x="0" y="0.75" dx="0.8" dy="0.8" layer="1"/>
<smd name="A" x="0" y="-0.75" dx="0.8" dy="0.8" layer="1"/>
<text x="-0.635" y="-0.635" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-0.635" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.45" y1="0.45" x2="0.45" y2="0.85" layer="51"/>
<rectangle x1="-0.45" y1="-0.85" x2="0.45" y2="-0.45" layer="51"/>
<rectangle x1="-0.45" y1="0" x2="-0.3" y2="0.3" layer="21"/>
<rectangle x1="0.3" y1="0" x2="0.45" y2="0.3" layer="21"/>
<rectangle x1="-0.15" y1="0" x2="0.15" y2="0.3" layer="21"/>
</package>
<package name="CHIP-LED0805">
<description>&lt;b&gt;Hyper CHIPLED Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
LB R99A&lt;br&gt;
Source: http://www.osram.convergy.de/ ... lb_r99a.pdf</description>
<wire x1="-0.625" y1="0.45" x2="-0.625" y2="-0.45" width="0.1016" layer="51"/>
<wire x1="0.625" y1="0.45" x2="0.625" y2="-0.475" width="0.1016" layer="51"/>
<smd name="C" x="0" y="1.05" dx="1.2" dy="1.2" layer="1"/>
<smd name="A" x="0" y="-1.05" dx="1.2" dy="1.2" layer="1"/>
<text x="-1.27" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.675" y1="0" x2="-0.525" y2="0.3" layer="21"/>
<rectangle x1="0.525" y1="0" x2="0.675" y2="0.3" layer="21"/>
<rectangle x1="-0.15" y1="0" x2="0.15" y2="0.3" layer="21"/>
<rectangle x1="-0.675" y1="0.45" x2="0.675" y2="1.05" layer="51"/>
<rectangle x1="-0.675" y1="-1.05" x2="0.675" y2="-0.45" layer="51"/>
</package>
<package name="MINI-TOPLED-SANTANA">
<description>&lt;b&gt;Mini TOPLED Santana®&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG M470.pdf</description>
<wire x1="0.7" y1="-1" x2="0.35" y2="-1" width="0.1016" layer="21"/>
<wire x1="0.35" y1="-1" x2="-0.7" y2="-1" width="0.1016" layer="21"/>
<wire x1="-0.7" y1="-1" x2="-0.7" y2="1" width="0.1016" layer="21"/>
<wire x1="-0.7" y1="1" x2="0.7" y2="1" width="0.1016" layer="21"/>
<wire x1="0.7" y1="1" x2="0.7" y2="-0.65" width="0.1016" layer="21"/>
<wire x1="0.7" y1="-0.65" x2="0.7" y2="-1" width="0.1016" layer="21"/>
<wire x1="0.45" y1="-0.7" x2="-0.45" y2="-0.7" width="0.1016" layer="21"/>
<wire x1="-0.45" y1="-0.7" x2="-0.45" y2="0.7" width="0.1016" layer="21"/>
<wire x1="-0.45" y1="0.7" x2="0.45" y2="0.7" width="0.1016" layer="21"/>
<wire x1="0.45" y1="0.7" x2="0.45" y2="-0.7" width="0.1016" layer="21"/>
<wire x1="0.7" y1="-0.65" x2="0.35" y2="-1" width="0.1016" layer="21"/>
<smd name="C" x="0" y="-2.2" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="2.2" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<text x="-1.27" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.55" y1="1.5" x2="0.55" y2="2.1" layer="29"/>
<rectangle x1="-0.55" y1="-2.1" x2="0.55" y2="-1.5" layer="29"/>
<rectangle x1="-0.5" y1="-2.05" x2="0.5" y2="-1.55" layer="31"/>
<rectangle x1="-0.5" y1="1.55" x2="0.5" y2="2.05" layer="31"/>
<rectangle x1="-0.2" y1="-0.4" x2="0.15" y2="-0.05" layer="21"/>
<rectangle x1="-0.5" y1="-2.1" x2="0.5" y2="-1.4" layer="51"/>
<rectangle x1="-0.5" y1="1.4" x2="0.5" y2="2.05" layer="51"/>
<rectangle x1="-0.5" y1="1" x2="0.5" y2="1.4" layer="21"/>
<rectangle x1="-0.5" y1="-1.4" x2="0.5" y2="-1.05" layer="21"/>
<hole x="0" y="0" drill="2.7"/>
</package>
<package name="CHIPLED_0805">
<description>&lt;b&gt;CHIPLED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG_R971.pdf</description>
<wire x1="-0.35" y1="0.925" x2="0.35" y2="0.925" width="0.1016" layer="51" curve="162.394521"/>
<wire x1="-0.35" y1="-0.925" x2="0.35" y2="-0.925" width="0.1016" layer="51" curve="-162.394521"/>
<wire x1="0.575" y1="0.525" x2="0.575" y2="-0.525" width="0.1016" layer="51"/>
<wire x1="-0.575" y1="-0.5" x2="-0.575" y2="0.925" width="0.1016" layer="51"/>
<circle x="-0.45" y="0.85" radius="0.103" width="0.1016" layer="51"/>
<smd name="C" x="0" y="1.05" dx="1.2" dy="1.2" layer="1"/>
<smd name="A" x="0" y="-1.05" dx="1.2" dy="1.2" layer="1"/>
<text x="-1.27" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="0.3" y1="0.5" x2="0.625" y2="1" layer="51"/>
<rectangle x1="-0.325" y1="0.5" x2="-0.175" y2="0.75" layer="51"/>
<rectangle x1="0.175" y1="0.5" x2="0.325" y2="0.75" layer="51"/>
<rectangle x1="-0.2" y1="0.5" x2="0.2" y2="0.675" layer="51"/>
<rectangle x1="0.3" y1="-1" x2="0.625" y2="-0.5" layer="51"/>
<rectangle x1="-0.625" y1="-1" x2="-0.3" y2="-0.5" layer="51"/>
<rectangle x1="0.175" y1="-0.75" x2="0.325" y2="-0.5" layer="51"/>
<rectangle x1="-0.325" y1="-0.75" x2="-0.175" y2="-0.5" layer="51"/>
<rectangle x1="-0.2" y1="-0.675" x2="0.2" y2="-0.5" layer="51"/>
<rectangle x1="-0.1" y1="0" x2="0.1" y2="0.2" layer="21"/>
<rectangle x1="-0.6" y1="0.5" x2="-0.3" y2="0.8" layer="51"/>
<rectangle x1="-0.625" y1="0.925" x2="-0.3" y2="1" layer="51"/>
</package>
<package name="CHIPLED_1206">
<description>&lt;b&gt;CHIPLED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG_LY N971.pdf</description>
<wire x1="-0.4" y1="1.6" x2="0.4" y2="1.6" width="0.1016" layer="51" curve="172.619069"/>
<wire x1="-0.8" y1="-0.95" x2="-0.8" y2="0.95" width="0.1016" layer="51"/>
<wire x1="0.8" y1="0.95" x2="0.8" y2="-0.95" width="0.1016" layer="51"/>
<circle x="-0.55" y="1.425" radius="0.1" width="0.1016" layer="51"/>
<smd name="C" x="0" y="1.75" dx="1.5" dy="1.5" layer="1"/>
<smd name="A" x="0" y="-1.75" dx="1.5" dy="1.5" layer="1"/>
<text x="-1.27" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.85" y1="1.525" x2="-0.35" y2="1.65" layer="51"/>
<rectangle x1="-0.85" y1="1.225" x2="-0.625" y2="1.55" layer="51"/>
<rectangle x1="-0.45" y1="1.225" x2="-0.325" y2="1.45" layer="51"/>
<rectangle x1="-0.65" y1="1.225" x2="-0.225" y2="1.35" layer="51"/>
<rectangle x1="0.35" y1="1.3" x2="0.85" y2="1.65" layer="51"/>
<rectangle x1="0.25" y1="1.225" x2="0.85" y2="1.35" layer="51"/>
<rectangle x1="-0.85" y1="0.95" x2="0.85" y2="1.25" layer="51"/>
<rectangle x1="-0.85" y1="-1.65" x2="0.85" y2="-0.95" layer="51"/>
<rectangle x1="-0.85" y1="0.35" x2="-0.525" y2="0.775" layer="21"/>
<rectangle x1="0.525" y1="0.35" x2="0.85" y2="0.775" layer="21"/>
<rectangle x1="-0.175" y1="0" x2="0.175" y2="0.35" layer="21"/>
</package>
<package name="CHIPLED_0603">
<description>&lt;b&gt;CHIPLED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG_LY Q971.pdf</description>
<wire x1="-0.3" y1="0.8" x2="0.3" y2="0.8" width="0.1016" layer="51" curve="170.055574"/>
<wire x1="-0.275" y1="-0.825" x2="0.275" y2="-0.825" width="0.0508" layer="51" curve="-180"/>
<wire x1="-0.4" y1="0.375" x2="-0.4" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="0.4" y1="0.35" x2="0.4" y2="-0.35" width="0.1016" layer="51"/>
<circle x="-0.35" y="0.625" radius="0.075" width="0.0508" layer="51"/>
<smd name="C" x="0" y="0.75" dx="0.8" dy="0.8" layer="1"/>
<smd name="A" x="0" y="-0.75" dx="0.8" dy="0.8" layer="1"/>
<text x="-0.635" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.45" y1="0.7" x2="-0.25" y2="0.85" layer="51"/>
<rectangle x1="-0.275" y1="0.55" x2="-0.225" y2="0.6" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="-0.4" y2="0.725" layer="51"/>
<rectangle x1="0.25" y1="0.55" x2="0.45" y2="0.85" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="0.45" y2="0.575" layer="51"/>
<rectangle x1="-0.45" y1="-0.85" x2="-0.25" y2="-0.35" layer="51"/>
<rectangle x1="0.25" y1="-0.85" x2="0.45" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.575" x2="0.275" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.65" x2="-0.175" y2="-0.55" layer="51"/>
<rectangle x1="0.175" y1="-0.65" x2="0.275" y2="-0.55" layer="51"/>
<rectangle x1="-0.125" y1="0" x2="0.125" y2="0.25" layer="21"/>
</package>
<package name="CHIPLED-0603-TTW">
<description>&lt;b&gt;CHIPLED-0603&lt;/b&gt;&lt;p&gt;
Recommended Solder Pad useable for SmartLEDTM and Chipled - Package 0603&lt;br&gt;
Package able to withstand TTW-soldering heat&lt;br&gt;
Package suitable for TTW-soldering&lt;br&gt;
Source: http://www.osram.convergy.de/ ... LO_LS_LY L89K.pdf</description>
<wire x1="-0.3" y1="0.8" x2="0.3" y2="0.8" width="0.1016" layer="51" curve="170.055574"/>
<wire x1="-0.275" y1="-0.825" x2="0.275" y2="-0.825" width="0.0508" layer="51" curve="-180"/>
<wire x1="-0.4" y1="0.375" x2="-0.4" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="0.4" y1="0.35" x2="0.4" y2="-0.35" width="0.1016" layer="51"/>
<circle x="-0.35" y="0.625" radius="0.075" width="0.0508" layer="51"/>
<smd name="C" x="0" y="0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="-0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A@1" x="0" y="-0.5" dx="0.35" dy="0.35" layer="1"/>
<smd name="C@1" x="0" y="0.5" dx="0.35" dy="0.35" layer="1"/>
<text x="-0.635" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.45" y1="0.7" x2="-0.25" y2="0.85" layer="51"/>
<rectangle x1="-0.275" y1="0.55" x2="-0.225" y2="0.6" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="-0.4" y2="0.725" layer="51"/>
<rectangle x1="0.25" y1="0.55" x2="0.45" y2="0.85" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="0.45" y2="0.575" layer="51"/>
<rectangle x1="-0.45" y1="-0.85" x2="-0.25" y2="-0.35" layer="51"/>
<rectangle x1="0.25" y1="-0.85" x2="0.45" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.575" x2="0.275" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.65" x2="-0.175" y2="-0.55" layer="51"/>
<rectangle x1="0.175" y1="-0.65" x2="0.275" y2="-0.55" layer="51"/>
<rectangle x1="-0.125" y1="0" x2="0.125" y2="0.25" layer="21"/>
<rectangle x1="-0.175" y1="0.325" x2="0.175" y2="0.7" layer="29"/>
<rectangle x1="-0.4" y1="0.625" x2="0.4" y2="1.125" layer="29"/>
<rectangle x1="-0.4" y1="-1.125" x2="0.4" y2="-0.625" layer="29"/>
<rectangle x1="-0.175" y1="-0.675" x2="0.175" y2="-0.325" layer="29"/>
</package>
<package name="SMARTLED-TTW">
<description>&lt;b&gt;SmartLED TTW&lt;/b&gt;&lt;p&gt;
Recommended Solder Pad useable for SmartLEDTM and Chipled - Package 0603&lt;br&gt;
Package able to withstand TTW-soldering heat&lt;br&gt;
Package suitable for TTW-soldering&lt;br&gt;
Source: http://www.osram.convergy.de/ ... LO_LS_LY L89K.pdf</description>
<wire x1="-0.35" y1="0.6" x2="0.35" y2="0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="0.35" y1="0.6" x2="0.35" y2="-0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.6" x2="0.15" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="0.15" y1="-0.6" x2="-0.35" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="-0.35" y1="-0.6" x2="-0.35" y2="0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.4" x2="0.15" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<smd name="C" x="0" y="0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="-0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A@1" x="0" y="-0.5" dx="0.35" dy="0.35" layer="1" stop="no" cream="no"/>
<smd name="C@1" x="0" y="0.5" dx="0.35" dy="0.35" layer="1" stop="no" cream="no"/>
<text x="-0.635" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.175" y1="0.325" x2="0.175" y2="0.7" layer="29"/>
<rectangle x1="-0.15" y1="-0.35" x2="0.15" y2="-0.05" layer="21"/>
<rectangle x1="-0.15" y1="0.6" x2="0.15" y2="0.85" layer="51"/>
<rectangle x1="-0.15" y1="-0.85" x2="0.15" y2="-0.6" layer="51"/>
<rectangle x1="-0.225" y1="0.3" x2="0.225" y2="0.975" layer="31"/>
<rectangle x1="-0.175" y1="-0.7" x2="0.175" y2="-0.325" layer="29" rot="R180"/>
<rectangle x1="-0.225" y1="-0.975" x2="0.225" y2="-0.3" layer="31" rot="R180"/>
</package>
<package name="LUMILED+">
<description>&lt;b&gt;Lumileds Lighting. LUXEON®&lt;/b&gt; with cool pad&lt;p&gt;
Source: K2.pdf</description>
<wire x1="-3.575" y1="2.3375" x2="-2.3375" y2="3.575" width="0.2032" layer="21"/>
<wire x1="-2.3375" y1="3.575" x2="2.3375" y2="3.575" width="0.2032" layer="21"/>
<wire x1="3.575" y1="2.3375" x2="3.575" y2="-3.575" width="0.2032" layer="21"/>
<wire x1="3.575" y1="-3.575" x2="-2.3375" y2="-3.575" width="0.2032" layer="21"/>
<wire x1="-2.3375" y1="-3.575" x2="-2.5" y2="-3.4125" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-3.4125" x2="-3.4125" y2="-2.5" width="0.2032" layer="21" curve="167.429893"/>
<wire x1="-3.4125" y1="-2.5" x2="-3.575" y2="-2.3375" width="0.2032" layer="21"/>
<wire x1="-3.575" y1="-2.3375" x2="-3.575" y2="2.3375" width="0.2032" layer="21"/>
<wire x1="2.3375" y1="3.575" x2="2.5" y2="3.4125" width="0.2032" layer="21"/>
<wire x1="2.5" y1="3.4125" x2="3.4125" y2="2.5" width="0.2032" layer="21" curve="167.429893"/>
<wire x1="3.4125" y1="2.5" x2="3.575" y2="2.3375" width="0.2032" layer="21"/>
<wire x1="-1.725" y1="2.225" x2="-1.0625" y2="2.5625" width="0.2032" layer="21" curve="-255.44999"/>
<wire x1="1.725" y1="-2.225" x2="1.0625" y2="-2.5625" width="0.2032" layer="21" curve="-255.44999"/>
<circle x="0" y="0" radius="2.725" width="0.2032" layer="51"/>
<smd name="1NC" x="-5.2" y="1.15" dx="2.9" dy="1.7" layer="1"/>
<smd name="2+" x="-5.2" y="-1.15" dx="2.9" dy="1.7" layer="1"/>
<smd name="3NC" x="5.2" y="-1.15" dx="2.9" dy="1.7" layer="1" rot="R180"/>
<smd name="4-" x="5.2" y="1.15" dx="2.9" dy="1.7" layer="1" rot="R180"/>
<text x="-3.175" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-5.975" y1="0.575" x2="-3.625" y2="1.6" layer="51"/>
<rectangle x1="-5.975" y1="-1.6" x2="-3.625" y2="-0.575" layer="51"/>
<rectangle x1="3.625" y1="-1.6" x2="5.975" y2="-0.575" layer="51" rot="R180"/>
<rectangle x1="3.625" y1="0.575" x2="5.975" y2="1.6" layer="51" rot="R180"/>
<polygon width="0.4064" layer="1">
<vertex x="2.3383" y="1.35"/>
<vertex x="0" y="2.7"/>
<vertex x="-2.3383" y="1.35"/>
<vertex x="-2.3383" y="-1.35"/>
<vertex x="0" y="-2.7"/>
<vertex x="2.3383" y="-1.35"/>
</polygon>
<polygon width="0.4064" layer="29">
<vertex x="2.3383" y="1.35"/>
<vertex x="0" y="2.7"/>
<vertex x="-2.3383" y="1.35"/>
<vertex x="-2.3383" y="-1.35"/>
<vertex x="0" y="-2.7"/>
<vertex x="2.3383" y="-1.35"/>
</polygon>
<polygon width="0.4064" layer="31">
<vertex x="2.3383" y="1.35"/>
<vertex x="0" y="2.7"/>
<vertex x="-2.3383" y="1.35"/>
<vertex x="-2.3383" y="-1.35"/>
<vertex x="0" y="-2.7"/>
<vertex x="2.3383" y="-1.35"/>
</polygon>
</package>
<package name="LUMILED">
<description>&lt;b&gt;Lumileds Lighting. LUXEON®&lt;/b&gt; without cool pad&lt;p&gt;
Source: K2.pdf</description>
<wire x1="-3.575" y1="2.3375" x2="-2.3375" y2="3.575" width="0.2032" layer="21"/>
<wire x1="-2.3375" y1="3.575" x2="2.3375" y2="3.575" width="0.2032" layer="21"/>
<wire x1="3.575" y1="2.3375" x2="3.575" y2="-3.575" width="0.2032" layer="21"/>
<wire x1="3.575" y1="-3.575" x2="-2.3375" y2="-3.575" width="0.2032" layer="21"/>
<wire x1="-2.3375" y1="-3.575" x2="-2.5" y2="-3.4125" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-3.4125" x2="-3.4125" y2="-2.5" width="0.2032" layer="21" curve="167.429893"/>
<wire x1="-3.4125" y1="-2.5" x2="-3.575" y2="-2.3375" width="0.2032" layer="21"/>
<wire x1="-3.575" y1="-2.3375" x2="-3.575" y2="2.3375" width="0.2032" layer="21"/>
<wire x1="2.3375" y1="3.575" x2="2.5" y2="3.4125" width="0.2032" layer="21"/>
<wire x1="2.5" y1="3.4125" x2="3.4125" y2="2.5" width="0.2032" layer="21" curve="167.429893"/>
<wire x1="3.4125" y1="2.5" x2="3.575" y2="2.3375" width="0.2032" layer="21"/>
<wire x1="-1.725" y1="2.225" x2="-1.0625" y2="2.5625" width="0.2032" layer="21" curve="-255.44999"/>
<wire x1="1.725" y1="-2.225" x2="1.0625" y2="-2.5625" width="0.2032" layer="21" curve="-255.44999"/>
<circle x="0" y="0" radius="2.725" width="0.2032" layer="51"/>
<smd name="1NC" x="-5.2" y="1.15" dx="2.9" dy="1.7" layer="1"/>
<smd name="2+" x="-5.2" y="-1.15" dx="2.9" dy="1.7" layer="1"/>
<smd name="3NC" x="5.2" y="-1.15" dx="2.9" dy="1.7" layer="1" rot="R180"/>
<smd name="4-" x="5.2" y="1.15" dx="2.9" dy="1.7" layer="1" rot="R180"/>
<text x="-3.175" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-5.975" y1="0.575" x2="-3.625" y2="1.6" layer="51"/>
<rectangle x1="-5.975" y1="-1.6" x2="-3.625" y2="-0.575" layer="51"/>
<rectangle x1="3.625" y1="-1.6" x2="5.975" y2="-0.575" layer="51" rot="R180"/>
<rectangle x1="3.625" y1="0.575" x2="5.975" y2="1.6" layer="51" rot="R180"/>
<polygon width="0.4064" layer="29">
<vertex x="2.3383" y="1.35"/>
<vertex x="0" y="2.7"/>
<vertex x="-2.3383" y="1.35"/>
<vertex x="-2.3383" y="-1.35"/>
<vertex x="0" y="-2.7"/>
<vertex x="2.3383" y="-1.35"/>
</polygon>
<polygon width="0.4064" layer="31">
<vertex x="2.3383" y="1.35"/>
<vertex x="0" y="2.7"/>
<vertex x="-2.3383" y="1.35"/>
<vertex x="-2.3383" y="-1.35"/>
<vertex x="0" y="-2.7"/>
<vertex x="2.3383" y="-1.35"/>
</polygon>
</package>
<package name="LED10MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
10 mm, round</description>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="2.54" width="0.254" layer="21" curve="-306.869898"/>
<wire x1="4.445" y1="0" x2="0" y2="-4.445" width="0.127" layer="21" curve="-90"/>
<wire x1="3.81" y1="0" x2="0" y2="-3.81" width="0.127" layer="21" curve="-90"/>
<wire x1="3.175" y1="0" x2="0" y2="-3.175" width="0.127" layer="21" curve="-90"/>
<wire x1="2.54" y1="0" x2="0" y2="-2.54" width="0.127" layer="21" curve="-90"/>
<wire x1="-4.445" y1="0" x2="0" y2="4.445" width="0.127" layer="21" curve="-90"/>
<wire x1="-3.81" y1="0" x2="0" y2="3.81" width="0.127" layer="21" curve="-90"/>
<wire x1="-3.175" y1="0" x2="0" y2="3.175" width="0.127" layer="21" curve="-90"/>
<wire x1="-2.54" y1="0" x2="0" y2="2.54" width="0.127" layer="21" curve="-90"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-2.54" width="0.254" layer="21"/>
<circle x="0" y="0" radius="5.08" width="0.127" layer="21"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.6764" shape="square"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<text x="6.35" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="6.35" y="-1.27" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="KA-3528ASYC">
<description>&lt;b&gt;SURFACE MOUNT LED LAMP&lt;/b&gt; 3.5x2.8mm&lt;p&gt;
Source: http://www.kingbright.com/manager/upload/pdf/KA-3528ASYC(Ver1189474662.1)</description>
<wire x1="-1.55" y1="1.35" x2="1.55" y2="1.35" width="0.1016" layer="21"/>
<wire x1="1.55" y1="1.35" x2="1.55" y2="-1.35" width="0.1016" layer="51"/>
<wire x1="1.55" y1="-1.35" x2="-1.55" y2="-1.35" width="0.1016" layer="21"/>
<wire x1="-1.55" y1="-1.35" x2="-1.55" y2="1.35" width="0.1016" layer="51"/>
<wire x1="-0.65" y1="0.95" x2="0.65" y2="0.95" width="0.1016" layer="21" curve="-68.40813"/>
<wire x1="0.65" y1="-0.95" x2="-0.65" y2="-0.95" width="0.1016" layer="21" curve="-68.40813"/>
<circle x="0" y="0" radius="1.15" width="0.1016" layer="51"/>
<smd name="A" x="-1.55" y="0" dx="1.5" dy="2.2" layer="1"/>
<smd name="C" x="1.55" y="0" dx="1.5" dy="2.2" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.75" y1="0.6" x2="-1.6" y2="1.1" layer="51"/>
<rectangle x1="-1.75" y1="-1.1" x2="-1.6" y2="-0.6" layer="51"/>
<rectangle x1="1.6" y1="-1.1" x2="1.75" y2="-0.6" layer="51" rot="R180"/>
<rectangle x1="1.6" y1="0.6" x2="1.75" y2="1.1" layer="51" rot="R180"/>
<polygon width="0.1016" layer="51">
<vertex x="1.55" y="-1.35"/>
<vertex x="1.55" y="-0.625"/>
<vertex x="0.825" y="-1.35"/>
</polygon>
<polygon width="0.1016" layer="21">
<vertex x="1.55" y="-1.35"/>
<vertex x="1.55" y="-1.175"/>
<vertex x="1" y="-1.175"/>
<vertex x="0.825" y="-1.35"/>
</polygon>
</package>
<package name="SML0805">
<description>&lt;b&gt;SML0805-2CW-TR (0805 PROFILE)&lt;/b&gt; COOL WHITE&lt;p&gt;
Source: http://www.ledtronics.com/ds/smd-0603/Dstr0093.pdf</description>
<wire x1="-0.95" y1="-0.55" x2="0.95" y2="-0.55" width="0.1016" layer="51"/>
<wire x1="0.95" y1="-0.55" x2="0.95" y2="0.55" width="0.1016" layer="51"/>
<wire x1="0.95" y1="0.55" x2="-0.95" y2="0.55" width="0.1016" layer="51"/>
<wire x1="-0.95" y1="0.55" x2="-0.95" y2="-0.55" width="0.1016" layer="51"/>
<wire x1="-0.175" y1="-0.025" x2="0" y2="0.15" width="0.0634" layer="21"/>
<wire x1="0" y1="0.15" x2="0.15" y2="0" width="0.0634" layer="21"/>
<wire x1="0.15" y1="0" x2="-0.025" y2="-0.175" width="0.0634" layer="21"/>
<wire x1="-0.025" y1="-0.175" x2="-0.175" y2="-0.025" width="0.0634" layer="21"/>
<circle x="-0.275" y="0.4" radius="0.125" width="0" layer="21"/>
<smd name="C" x="-1.05" y="0" dx="1.2" dy="1.2" layer="1"/>
<smd name="A" x="1.05" y="0" dx="1.2" dy="1.2" layer="1"/>
<text x="-1.5" y="1" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.5" y="-2" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="SML1206">
<description>&lt;b&gt;SML10XXKH-TR (HIGH INTENSITY) LED&lt;/b&gt;&lt;p&gt;
&lt;table&gt;
&lt;tr&gt;&lt;td&gt;SML10R3KH-TR&lt;/td&gt;&lt;td&gt;ULTRA RED&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10E3KH-TR&lt;/td&gt;&lt;td&gt;SUPER REDSUPER BLUE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10O3KH-TR&lt;/td&gt;&lt;td&gt;SUPER ORANGE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10PY3KH-TR&lt;/td&gt;&lt;td&gt;PURE YELLOW&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10OY3KH-TR&lt;/td&gt;&lt;td&gt;ULTRA YELLOW&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10AG3KH-TR&lt;/td&gt;&lt;td&gt;AQUA GREEN&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10BG3KH-TR&lt;/td&gt;&lt;td&gt;BLUE GREEN&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10PB1KH-TR&lt;/td&gt;&lt;td&gt;SUPER BLUE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10CW1KH-TR&lt;/td&gt;&lt;td&gt;WHITE&lt;/td&gt;&lt;/tr&gt;
&lt;/table&gt;

Source: http://www.ledtronics.com/ds/smd-1206/dstr0094.PDF</description>
<wire x1="-1.5" y1="0.5" x2="-1.5" y2="-0.5" width="0.2032" layer="51" curve="-180"/>
<wire x1="1.5" y1="-0.5" x2="1.5" y2="0.5" width="0.2032" layer="51" curve="-180"/>
<wire x1="-1.55" y1="0.75" x2="1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.55" y1="-0.75" x2="-1.55" y2="-0.75" width="0.1016" layer="51"/>
<circle x="-0.725" y="0.525" radius="0.125" width="0" layer="21"/>
<smd name="C" x="-1.75" y="0" dx="1.5" dy="1.5" layer="1"/>
<smd name="A" x="1.75" y="0" dx="1.5" dy="1.5" layer="1"/>
<text x="-1.5" y="1" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.5" y="-2.5" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="0.4" x2="-1.15" y2="0.8" layer="51"/>
<rectangle x1="-1.6" y1="-0.8" x2="-1.15" y2="-0.4" layer="51"/>
<rectangle x1="-1.175" y1="-0.6" x2="-1" y2="-0.275" layer="51"/>
<rectangle x1="1.15" y1="-0.8" x2="1.6" y2="-0.4" layer="51" rot="R180"/>
<rectangle x1="1.15" y1="0.4" x2="1.6" y2="0.8" layer="51" rot="R180"/>
<rectangle x1="1" y1="0.275" x2="1.175" y2="0.6" layer="51" rot="R180"/>
<rectangle x1="-0.1" y1="-0.1" x2="0.1" y2="0.1" layer="21"/>
</package>
<package name="SML0603">
<description>&lt;b&gt;SML0603-XXX (HIGH INTENSITY) LED&lt;/b&gt;&lt;p&gt;
&lt;table&gt;
&lt;tr&gt;&lt;td&gt;AG3K&lt;/td&gt;&lt;td&gt;AQUA GREEN&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;B1K&lt;/td&gt;&lt;td&gt;SUPER BLUE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;R1K&lt;/td&gt;&lt;td&gt;SUPER RED&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;R3K&lt;/td&gt;&lt;td&gt;ULTRA RED&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;O3K&lt;/td&gt;&lt;td&gt;SUPER ORANGE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;O3KH&lt;/td&gt;&lt;td&gt;SOFT ORANGE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;Y3KH&lt;/td&gt;&lt;td&gt;SUPER YELLOW&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;Y3K&lt;/td&gt;&lt;td&gt;SUPER YELLOW&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;2CW&lt;/td&gt;&lt;td&gt;WHITE&lt;/td&gt;&lt;/tr&gt;
&lt;/table&gt;
Source: http://www.ledtronics.com/ds/smd-0603/Dstr0092.pdf</description>
<wire x1="-0.75" y1="0.35" x2="0.75" y2="0.35" width="0.1016" layer="51"/>
<wire x1="0.75" y1="0.35" x2="0.75" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="0.75" y1="-0.35" x2="-0.75" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="-0.75" y1="-0.35" x2="-0.75" y2="0.35" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="0.3" x2="-0.45" y2="-0.3" width="0.1016" layer="51"/>
<wire x1="0.45" y1="0.3" x2="0.45" y2="-0.3" width="0.1016" layer="51"/>
<wire x1="-0.2" y1="0.35" x2="0.2" y2="0.35" width="0.1016" layer="21"/>
<wire x1="-0.2" y1="-0.35" x2="0.2" y2="-0.35" width="0.1016" layer="21"/>
<smd name="C" x="-0.75" y="0" dx="0.8" dy="0.8" layer="1"/>
<smd name="A" x="0.75" y="0" dx="0.8" dy="0.8" layer="1"/>
<text x="-1" y="1" size="1.27" layer="25">&gt;NAME</text>
<text x="-1" y="-2" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.4" y1="0.175" x2="0" y2="0.4" layer="51"/>
<rectangle x1="-0.25" y1="0.175" x2="0" y2="0.4" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="LED">
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<text x="3.556" y="-4.572" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-4.572" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED" prefix="LED" uservalue="yes">
<description>&lt;b&gt;LED&lt;/b&gt;&lt;p&gt;
&lt;u&gt;OSRAM&lt;/u&gt;:&lt;br&gt;

- &lt;u&gt;CHIPLED&lt;/u&gt;&lt;br&gt;
LG R971, LG N971, LY N971, LG Q971, LY Q971, LO R971, LY R971
LH N974, LH R974&lt;br&gt;
LS Q976, LO Q976, LY Q976&lt;br&gt;
LO Q996&lt;br&gt;

- &lt;u&gt;Hyper CHIPLED&lt;/u&gt;&lt;br&gt;
LW Q18S&lt;br&gt;
LB Q993, LB Q99A, LB R99A&lt;br&gt;

- &lt;u&gt;SideLED&lt;/u&gt;&lt;br&gt;
LS A670, LO A670, LY A670, LG A670, LP A670&lt;br&gt;
LB A673, LV A673, LT A673, LW A673&lt;br&gt;
LH A674&lt;br&gt;
LY A675&lt;br&gt;
LS A676, LA A676, LO A676, LY A676, LW A676&lt;br&gt;
LS A679, LY A679, LG A679&lt;br&gt;

-  &lt;u&gt;Hyper Micro SIDELED®&lt;/u&gt;&lt;br&gt;
LS Y876, LA Y876, LO Y876, LY Y876&lt;br&gt;
LT Y87S&lt;br&gt;

- &lt;u&gt;SmartLED&lt;/u&gt;&lt;br&gt;
LW L88C, LW L88S&lt;br&gt;
LB L89C, LB L89S, LG L890&lt;br&gt;
LS L89K, LO L89K, LY L89K&lt;br&gt;
LS L896, LA L896, LO L896, LY L896&lt;br&gt;

- &lt;u&gt;TOPLED&lt;/u&gt;&lt;br&gt;
LS T670, LO T670, LY T670, LG T670, LP T670&lt;br&gt;
LSG T670, LSP T670, LSY T670, LOP T670, LYG T670&lt;br&gt;
LG T671, LOG T671, LSG T671&lt;br&gt;
LB T673, LV T673, LT T673, LW T673&lt;br&gt;
LH T674&lt;br&gt;
LS T676, LA T676, LO T676, LY T676, LB T676, LH T676, LSB T676, LW T676&lt;br&gt;
LB T67C, LV T67C, LT T67C, LS T67K, LO T67K, LY T67K, LW E67C&lt;br&gt;
LS E67B, LA E67B, LO E67B, LY E67B, LB E67C, LV E67C, LT E67C&lt;br&gt;
LW T67C&lt;br&gt;
LS T679, LY T679, LG T679&lt;br&gt;
LS T770, LO T770, LY T770, LG T770, LP T770&lt;br&gt;
LB T773, LV T773, LT T773, LW T773&lt;br&gt;
LH T774&lt;br&gt;
LS E675, LA E675, LY E675, LS T675&lt;br&gt;
LS T776, LA T776, LO T776, LY T776, LB T776&lt;br&gt;
LHGB T686&lt;br&gt;
LT T68C, LB T68C&lt;br&gt;

- &lt;u&gt;Hyper Mini TOPLED®&lt;/u&gt;&lt;br&gt;
LB M676&lt;br&gt;

- &lt;u&gt;Mini TOPLED Santana®&lt;/u&gt;&lt;br&gt;
LG M470&lt;br&gt;
LS M47K, LO M47K, LY M47K
&lt;p&gt;
Source: http://www.osram.convergy.de&lt;p&gt;

&lt;u&gt;LUXEON:&lt;/u&gt;&lt;br&gt;
- &lt;u&gt;LUMILED®&lt;/u&gt;&lt;br&gt;
LXK2-PW12-R00, LXK2-PW12-S00, LXK2-PW14-U00, LXK2-PW14-V00&lt;br&gt;
LXK2-PM12-R00, LXK2-PM12-S00, LXK2-PM14-U00&lt;br&gt;
LXK2-PE12-Q00, LXK2-PE12-R00, LXK2-PE12-S00, LXK2-PE14-T00, LXK2-PE14-U00&lt;br&gt;
LXK2-PB12-K00, LXK2-PB12-L00, LXK2-PB12-M00, LXK2-PB14-N00, LXK2-PB14-P00, LXK2-PB14-Q00&lt;br&gt;
LXK2-PR12-L00, LXK2-PR12-M00, LXK2-PR14-Q00, LXK2-PR14-R00&lt;br&gt;
LXK2-PD12-Q00, LXK2-PD12-R00, LXK2-PD12-S00&lt;br&gt;
LXK2-PH12-R00, LXK2-PH12-S00&lt;br&gt;
LXK2-PL12-P00, LXK2-PL12-Q00, LXK2-PL12-R00
&lt;p&gt;
Source: www.luxeon.com&lt;p&gt;

&lt;u&gt;KINGBRIGHT:&lt;/U&gt;&lt;p&gt;
KA-3528ASYC&lt;br&gt;
Source: www.kingbright.com</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="SMT1206" package="1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LD260" package="LD260">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SQR2X5" package="LED2X5">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3MM" package="LED3MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM" package="LED5MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LSU260" package="LSU260">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LZR181" package="LZR181">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B152" package="Q62902-B152">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B153" package="Q62902-B153">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B155" package="Q62902-B155">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B156" package="Q62902-B156">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SFH480" package="SFH480">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SFH482" package="SFH482">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SQR5.7X3.2" package="U57X32">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="IRL80A" package="IRL80A">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="P-LCC-2" package="P-LCC-2">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MINI-TOP" package="OSRAM-MINI-TOP-LED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SIDELED" package="OSRAM-SIDELED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMART-LED" package="SMART-LED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="B"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="P-LCC-2-BACK" package="P-LCC-2-TOPLED-RG">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MICRO-SIDELED" package="MICRO-SIDELED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="P-LCC-4" package="P-LCC-4">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C@4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIP-LED0603" package="CHIP-LED0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIP-LED0805" package="CHIP-LED0805">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TOPLED-SANTANA" package="MINI-TOPLED-SANTANA">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIPLED_0805" package="CHIPLED_0805">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIPLED_1206" package="CHIPLED_1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIPLED_0603" package="CHIPLED_0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIPLED-0603-TTW" package="CHIPLED-0603-TTW">
<connects>
<connect gate="G$1" pin="A" pad="A@1"/>
<connect gate="G$1" pin="C" pad="C@1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="SMARTLED-TTW">
<connects>
<connect gate="G$1" pin="A" pad="A@1"/>
<connect gate="G$1" pin="C" pad="C@1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-LUMILED+" package="LUMILED+">
<connects>
<connect gate="G$1" pin="A" pad="2+"/>
<connect gate="G$1" pin="C" pad="4-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-LUMILED" package="LUMILED">
<connects>
<connect gate="G$1" pin="A" pad="2+"/>
<connect gate="G$1" pin="C" pad="4-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="10MM" package="LED10MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="KA-3528ASYC" package="KA-3528ASYC">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SML0805" package="SML0805">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SML1206" package="SML1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SML0603" package="SML0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Resistors">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find resistors, capacitors, inductors, test points, jumper pads, etc.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="0805">
<wire x1="-0.3" y1="0.6" x2="0.3" y2="0.6" width="0.1524" layer="21"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="-0.6" width="0.1524" layer="21"/>
<smd name="1" x="-0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="0.4064" layer="27">&gt;VALUE</text>
<wire x1="-1.4986" y1="0.8128" x2="1.4986" y2="0.8128" width="0.0508" layer="39"/>
<wire x1="1.4986" y1="0.8128" x2="1.4986" y2="-0.8128" width="0.0508" layer="39"/>
<wire x1="1.4986" y1="-0.8128" x2="-1.4986" y2="-0.8128" width="0.0508" layer="39"/>
<wire x1="-1.4986" y1="-0.8128" x2="-1.4986" y2="0.8128" width="0.0508" layer="39"/>
</package>
<package name="1206">
<wire x1="-2.4003" y1="1.1049" x2="2.4003" y2="1.1049" width="0.0508" layer="39"/>
<wire x1="2.4003" y1="-1.1049" x2="-2.4003" y2="-1.1049" width="0.0508" layer="39"/>
<wire x1="-2.4003" y1="-1.1049" x2="-2.4003" y2="1.1049" width="0.0508" layer="39"/>
<wire x1="2.4003" y1="1.1049" x2="2.4003" y2="-1.1049" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.397" y="-1.524" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="AXIAL-0.3">
<wire x1="-2.54" y1="0.762" x2="2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="-2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="3.81" y="0" drill="0.9" diameter="1.8796"/>
<text x="-2.54" y="1.27" size="0.4064" layer="25" font="vector">&gt;Name</text>
<text x="-2.032" y="-0.381" size="1.016" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="R2010">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.302" y1="1.5621" x2="3.302" y2="1.5621" width="0.0508" layer="39"/>
<wire x1="3.302" y1="1.5621" x2="3.302" y2="-1.5621" width="0.0508" layer="39"/>
<wire x1="3.302" y1="-1.5621" x2="-3.302" y2="-1.5621" width="0.0508" layer="39"/>
<wire x1="-3.302" y1="-1.5621" x2="-3.302" y2="1.5621" width="0.0508" layer="39"/>
<wire x1="-1.027" y1="1.245" x2="1.027" y2="1.245" width="0.1524" layer="21"/>
<wire x1="-1.002" y1="-1.245" x2="1.016" y2="-1.245" width="0.1524" layer="21"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-2.54" y="1.5875" size="0.4064" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.032" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="0603-RES">
<wire x1="-1.6002" y1="0.6858" x2="1.6002" y2="0.6858" width="0.0508" layer="39"/>
<wire x1="1.6002" y1="0.6858" x2="1.6002" y2="-0.6858" width="0.0508" layer="39"/>
<wire x1="1.6002" y1="-0.6858" x2="-1.6002" y2="-0.6858" width="0.0508" layer="39"/>
<wire x1="-1.6002" y1="-0.6858" x2="-1.6002" y2="0.6858" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="0.762" size="0.4064" layer="25" font="vector">&gt;NAME</text>
<text x="-1.016" y="-1.143" size="0.4064" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.1905" y1="-0.381" x2="0.1905" y2="0.381" layer="21"/>
</package>
<package name="1/6W-RES">
<description>1/6W Thru-hole Resistor - *UNPROVEN*</description>
<wire x1="-1.55" y1="0.85" x2="-1.55" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="-1.55" y1="-0.85" x2="1.55" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="1.55" y1="-0.85" x2="1.55" y2="0.85" width="0.2032" layer="21"/>
<wire x1="1.55" y1="0.85" x2="-1.55" y2="0.85" width="0.2032" layer="21"/>
<pad name="1" x="-2.5" y="0" drill="0.762" diameter="1.6764"/>
<pad name="2" x="2.5" y="0" drill="0.762" diameter="1.6764"/>
<text x="-1.2662" y="0.9552" size="0.6096" layer="25">&gt;NAME</text>
<text x="-1.423" y="-0.4286" size="0.8128" layer="21" ratio="15">&gt;VALUE</text>
</package>
<package name="R2512">
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<wire x1="-3.9116" y1="-1.8034" x2="3.9116" y2="-1.8034" width="0.0508" layer="39"/>
<wire x1="3.9116" y1="-1.8034" x2="3.9116" y2="1.8034" width="0.0508" layer="39"/>
<wire x1="3.9116" y1="1.8034" x2="-3.9116" y2="1.8034" width="0.0508" layer="39"/>
<wire x1="-3.9116" y1="1.8034" x2="-3.9116" y2="-1.8034" width="0.0508" layer="39"/>
</package>
<package name="AXIAL-0.4">
<description>1/4W Resistor, 0.4" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-3.15" y1="-1.2" x2="-3.15" y2="1.2" width="0.2032" layer="21"/>
<wire x1="-3.15" y1="1.2" x2="3.15" y2="1.2" width="0.2032" layer="21"/>
<wire x1="3.15" y1="1.2" x2="3.15" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="3.15" y1="-1.2" x2="-3.15" y2="-1.2" width="0.2032" layer="21"/>
<pad name="P$1" x="-5.08" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="5.08" y="0" drill="0.9" diameter="1.8796"/>
<text x="-3.175" y="1.905" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-2.286" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.5">
<description>1/2W Resistor, 0.5" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-4.5" y1="-1.65" x2="-4.5" y2="1.65" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="1.65" x2="4.5" y2="1.65" width="0.2032" layer="21"/>
<wire x1="4.5" y1="1.65" x2="4.5" y2="-1.65" width="0.2032" layer="21"/>
<wire x1="4.5" y1="-1.65" x2="-4.5" y2="-1.65" width="0.2032" layer="21"/>
<pad name="P$1" x="-6.35" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="6.35" y="0" drill="0.9" diameter="1.8796"/>
<text x="-4.445" y="2.54" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-3.429" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.6">
<description>1W Resistor, 0.6" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-5.75" y1="-2.25" x2="-5.75" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-5.75" y1="2.25" x2="5.75" y2="2.25" width="0.2032" layer="21"/>
<wire x1="5.75" y1="2.25" x2="5.75" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="5.75" y1="-2.25" x2="-5.75" y2="-2.25" width="0.2032" layer="21"/>
<pad name="P$1" x="-7.62" y="0" drill="1.2" diameter="1.8796"/>
<pad name="P$2" x="7.62" y="0" drill="1.2" diameter="1.8796"/>
<text x="-5.715" y="3.175" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-4.064" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.8">
<description>2W Resistor, 0.8" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-7.75" y1="-2.5" x2="-7.75" y2="2.5" width="0.2032" layer="21"/>
<wire x1="-7.75" y1="2.5" x2="7.75" y2="2.5" width="0.2032" layer="21"/>
<wire x1="7.75" y1="2.5" x2="7.75" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="7.75" y1="-2.5" x2="-7.75" y2="-2.5" width="0.2032" layer="21"/>
<pad name="P$1" x="-10.16" y="0" drill="1.2" diameter="1.8796"/>
<pad name="P$2" x="10.16" y="0" drill="1.2" diameter="1.8796"/>
<text x="-7.62" y="3.175" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-5.969" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.3-KIT">
<description>&lt;h3&gt;AXIAL-0.3-KIT&lt;/h3&gt;

Commonly used for 1/4W through-hole resistors. 0.3" pitch between holes.&lt;br&gt;
&lt;br&gt;

&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of the AXIAL-0.3 package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.</description>
<wire x1="-2.54" y1="1.27" x2="2.54" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="0" width="0.254" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="-2.54" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="0" width="0.254" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.254" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.254" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="1.016" diameter="2.032" stop="no"/>
<pad name="P$2" x="3.81" y="0" drill="1.016" diameter="2.032" stop="no"/>
<text x="-2.54" y="1.27" size="0.4064" layer="25" font="vector">&gt;Name</text>
<text x="-2.159" y="-0.762" size="1.27" layer="21" font="vector" ratio="15">&gt;Value</text>
<polygon width="0.127" layer="30">
<vertex x="3.8201" y="-0.9449" curve="-90"/>
<vertex x="2.8652" y="-0.0152" curve="-90.011749"/>
<vertex x="3.8176" y="0.9602" curve="-90"/>
<vertex x="4.7676" y="-0.0178" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="3.8176" y="-0.4369" curve="-90.012891"/>
<vertex x="3.3731" y="-0.0127" curve="-90"/>
<vertex x="3.8176" y="0.4546" curve="-90"/>
<vertex x="4.2595" y="-0.0025" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="-3.8075" y="-0.9525" curve="-90"/>
<vertex x="-4.7624" y="-0.0228" curve="-90.011749"/>
<vertex x="-3.81" y="0.9526" curve="-90"/>
<vertex x="-2.86" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-3.81" y="-0.4445" curve="-90.012891"/>
<vertex x="-4.2545" y="-0.0203" curve="-90"/>
<vertex x="-3.81" y="0.447" curve="-90"/>
<vertex x="-3.3681" y="-0.0101" curve="-90.012967"/>
</polygon>
</package>
<package name="AXIAL-0.3EZ">
<description>This is the "EZ" version of the standard .3" spaced resistor package.&lt;br&gt;
It has a reduced top mask to make it harder to install upside-down.</description>
<wire x1="-2.54" y1="0.762" x2="2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="-2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="0.9" diameter="1.8796" stop="no"/>
<pad name="P$2" x="3.81" y="0" drill="0.9" diameter="1.8796" stop="no"/>
<text x="-2.54" y="1.27" size="0.4064" layer="25" font="vector">&gt;Name</text>
<text x="-2.032" y="-0.381" size="1.016" layer="21" font="vector" ratio="15">&gt;Value</text>
<circle x="-3.81" y="0" radius="0.508" width="0" layer="29"/>
<circle x="3.81" y="0" radius="0.523634375" width="0" layer="29"/>
<circle x="-3.81" y="0" radius="1.02390625" width="0" layer="30"/>
<circle x="3.81" y="0" radius="1.04726875" width="0" layer="30"/>
</package>
<package name="AXIAL-0.1EZ">
<wire x1="1.27" y1="-0.762" x2="1.27" y2="0" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0" x2="1.27" y2="0.762" width="0.2032" layer="21"/>
<wire x1="1.524" y1="0" x2="1.27" y2="0" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0" x2="1.016" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="0" y="0" drill="0.9" diameter="1.8796" stop="no"/>
<pad name="P$2" x="2.54" y="0" drill="0.9" diameter="1.8796" stop="no"/>
<text x="0" y="1.27" size="1.016" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="0" y="-2.159" size="1.016" layer="21" font="vector" ratio="15">&gt;Value</text>
<circle x="0" y="0" radius="0.4572" width="0" layer="29"/>
<circle x="0" y="0" radius="1.016" width="0" layer="30"/>
<circle x="2.54" y="0" radius="1.016" width="0" layer="30"/>
<circle x="0" y="0" radius="0.4572" width="0" layer="29"/>
<circle x="2.54" y="0" radius="0.4572" width="0" layer="29"/>
</package>
<package name="AXIAL-0.1">
<wire x1="1.27" y1="-0.762" x2="1.27" y2="0" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0" x2="1.27" y2="0.762" width="0.2032" layer="21"/>
<wire x1="1.524" y1="0" x2="1.27" y2="0" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0" x2="1.016" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="0" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="2.54" y="0" drill="0.9" diameter="1.8796"/>
<text x="0" y="1.27" size="1.016" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="0" y="-2.159" size="1.016" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="0402-RES">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.2065" y1="0.6477" x2="1.2065" y2="0.6477" width="0.0508" layer="39"/>
<wire x1="1.2065" y1="0.6477" x2="1.2065" y2="-0.6477" width="0.0508" layer="39"/>
<wire x1="1.2065" y1="-0.6477" x2="-1.2065" y2="-0.6477" width="0.0508" layer="39"/>
<wire x1="-1.2065" y1="-0.6477" x2="-1.2065" y2="0.6477" width="0.0508" layer="39"/>
<smd name="1" x="-0.58" y="0" dx="0.85" dy="0.9" layer="1"/>
<smd name="2" x="0.58" y="0" dx="0.85" dy="0.9" layer="1"/>
<text x="-0.889" y="0.6985" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-1.143" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2032" y1="-0.3556" x2="0.2032" y2="0.3556" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="RESISTOR" prefix="R" uservalue="yes">
<description>&lt;b&gt;Resistor&lt;/b&gt;
Basic schematic elements and footprints for 0603, 1206, and PTH resistors.</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="AXIAL-0.3" package="AXIAL-0.3">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805-RES" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603-RES" package="0603-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="0603-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/6W" package="1/6W-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/4W" package="AXIAL-0.4">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/2W" package="AXIAL-0.5">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1W" package="AXIAL-0.6">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-2W" package="AXIAL-0.8">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="KIT" package="AXIAL-0.3-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EZ" package="AXIAL-0.3EZ">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/4W-VERT-KIT" package="AXIAL-0.1EZ">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/4W-VERT" package="AXIAL-0.1">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="0402-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Capacitors">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find resistors, capacitors, inductors, test points, jumper pads, etc.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="1210">
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.3" x2="1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="-1.3" x2="-1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-1.3" x2="-1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.2032" layer="21"/>
<wire x1="-1.6" y1="-1.3" x2="1.6" y2="-1.3" width="0.2032" layer="21"/>
<smd name="1" x="-1.6" y="0" dx="1.2" dy="2" layer="1"/>
<smd name="2" x="1.6" y="0" dx="1.2" dy="2" layer="1"/>
<text x="-0.8" y="0.5" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.9" y="-0.7" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="CAP-PTH-SMALL">
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="0.7" diameter="1.651"/>
<pad name="2" x="2.54" y="0" drill="0.7" diameter="1.651"/>
<text x="0.508" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="0.254" y="-1.524" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="CAP-PTH-SMALL2">
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="3.81" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="1.27" x2="3.81" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-1.27" x2="-1.27" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="1.27" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="0.7" diameter="1.651"/>
<pad name="2" x="2.54" y="0" drill="0.7" diameter="1.651"/>
<text x="-1.27" y="1.905" size="0.6096" layer="25">&gt;Name</text>
<text x="-1.27" y="-2.54" size="0.6096" layer="27">&gt;Value</text>
</package>
<package name="0805">
<wire x1="-0.3" y1="0.6" x2="0.3" y2="0.6" width="0.1524" layer="21"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="-0.6" width="0.1524" layer="21"/>
<smd name="1" x="-0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="CAP-PTH-LARGE">
<wire x1="0" y1="0.635" x2="0" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="2.54" y2="0" width="0.2032" layer="21"/>
<pad name="1" x="-4.826" y="0" drill="0.9" diameter="1.905"/>
<pad name="2" x="4.572" y="0" drill="0.9" diameter="1.905"/>
<text x="-0.762" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.016" y="-1.524" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="GRM43D">
<wire x1="2.25" y1="1.6" x2="1.1" y2="1.6" width="0.127" layer="51"/>
<wire x1="1.1" y1="1.6" x2="-1.1" y2="1.6" width="0.127" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-2.25" y2="1.6" width="0.127" layer="51"/>
<wire x1="-2.25" y1="1.6" x2="-2.25" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-2.25" y1="-1.6" x2="-1.1" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="1.1" y2="-1.6" width="0.127" layer="51"/>
<wire x1="1.1" y1="-1.6" x2="2.25" y2="-1.6" width="0.127" layer="51"/>
<wire x1="2.25" y1="-1.6" x2="2.25" y2="1.6" width="0.127" layer="51"/>
<wire x1="1.1" y1="1.6" x2="1.1" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.1" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-2.3" y1="1.8" x2="2.3" y2="1.8" width="0.127" layer="21"/>
<wire x1="-2.3" y1="-1.8" x2="2.3" y2="-1.8" width="0.127" layer="21"/>
<smd name="A" x="1.927" y="0" dx="3.2" dy="1.65" layer="1" rot="R90"/>
<smd name="C" x="-1.927" y="0" dx="3.2" dy="1.65" layer="1" rot="R90"/>
<text x="-2" y="2" size="0.4064" layer="25">&gt;NAME</text>
<text x="0" y="-2" size="0.4064" layer="27" rot="R180">&gt;VALUE</text>
<rectangle x1="-2.2" y1="-1.6" x2="-1.1" y2="1.6" layer="51"/>
<rectangle x1="1.1" y1="-1.6" x2="2.2" y2="1.6" layer="51"/>
</package>
<package name="0603-CAP">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<wire x1="0" y1="0.027940625" x2="0" y2="-0.027940625" width="0.381" layer="21"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="0.762" size="0.4064" layer="25" font="vector">&gt;NAME</text>
<text x="-1.016" y="-1.143" size="0.4064" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="0402-CAP">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="0" y1="0.0305" x2="0" y2="-0.0305" width="0.4064" layer="21"/>
<smd name="1" x="-0.58" y="0" dx="0.85" dy="0.9" layer="1"/>
<smd name="2" x="0.58" y="0" dx="0.85" dy="0.9" layer="1"/>
<text x="-0.889" y="0.6985" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-1.143" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="CAP-PTH-5MM">
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="-2.5" y="0" drill="0.7" diameter="1.651"/>
<pad name="2" x="2.5" y="0" drill="0.7" diameter="1.651"/>
<text x="-0.762" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.016" y="-1.524" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="AXIAL-5MM">
<wire x1="-1.14" y1="0.762" x2="1.14" y2="0.762" width="0.2032" layer="21"/>
<wire x1="1.14" y1="0.762" x2="1.14" y2="0" width="0.2032" layer="21"/>
<wire x1="1.14" y1="0" x2="1.14" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="1.14" y1="-0.762" x2="-1.14" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-1.14" y1="-0.762" x2="-1.14" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.14" y1="0" x2="-1.14" y2="0.762" width="0.2032" layer="21"/>
<wire x1="1.14" y1="0" x2="1.394" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.14" y1="0" x2="-1.394" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-2.5" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="2.5" y="0" drill="0.9" diameter="1.8796"/>
<text x="-2.54" y="1.17" size="0.4" layer="25">&gt;Name</text>
<text x="-1.032" y="-0.208" size="0.4" layer="21" ratio="15">&gt;Value</text>
</package>
<package name="1206">
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.397" y="-1.524" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="CTZ3">
<description>CTZ3 Series land pattern for variable capacitor - CTZ3E-50C-W1-PF</description>
<wire x1="-1.6" y1="1.4" x2="-1.6" y2="-2.25" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-2.25" x2="1.6" y2="-2.25" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.4" x2="1.6" y2="-2.25" width="0.127" layer="51"/>
<wire x1="-0.5" y1="0" x2="0.5" y2="0" width="0.127" layer="51"/>
<wire x1="-1.6" y1="1.4" x2="-1" y2="2.2" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.4" x2="1" y2="2.2" width="0.127" layer="51"/>
<wire x1="-1" y1="2.2" x2="1" y2="2.2" width="0.127" layer="51"/>
<wire x1="0" y1="0.8" x2="0" y2="-0.8" width="0.127" layer="51"/>
<wire x1="-0.8" y1="0" x2="0.8" y2="0" width="0.127" layer="51"/>
<wire x1="-1.05" y1="2.25" x2="-1.7" y2="1.45" width="0.127" layer="21"/>
<wire x1="-1.7" y1="1.45" x2="-1.7" y2="-2.35" width="0.127" layer="21"/>
<wire x1="-1.7" y1="-2.35" x2="-1.05" y2="-2.35" width="0.127" layer="21"/>
<wire x1="1.05" y1="2.25" x2="1.7" y2="1.4" width="0.127" layer="21"/>
<wire x1="1.7" y1="1.4" x2="1.7" y2="-2.35" width="0.127" layer="21"/>
<wire x1="1.7" y1="-2.35" x2="1.05" y2="-2.35" width="0.127" layer="21"/>
<smd name="+" x="0" y="2.05" dx="1.5" dy="1.2" layer="1"/>
<smd name="-" x="0" y="-2.05" dx="1.5" dy="1.2" layer="1"/>
<text x="-2" y="3" size="0.4064" layer="25">&gt;NAME</text>
<text x="-2" y="-3.4" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="CAP-PTH-SMALL-KIT">
<description>&lt;h3&gt;CAP-PTH-SMALL-KIT&lt;/h3&gt;
Commonly used for small ceramic capacitors. Like our 0.1uF (http://www.sparkfun.com/products/8375) or 22pF caps (http://www.sparkfun.com/products/8571).&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.</description>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.254" layer="21"/>
<wire x1="-2.667" y1="1.27" x2="2.667" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.667" y1="1.27" x2="2.667" y2="-1.27" width="0.254" layer="21"/>
<wire x1="2.667" y1="-1.27" x2="-2.667" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-2.667" y1="-1.27" x2="-2.667" y2="1.27" width="0.254" layer="21"/>
<pad name="1" x="-1.397" y="0" drill="1.016" diameter="2.032" stop="no"/>
<pad name="2" x="1.397" y="0" drill="1.016" diameter="2.032" stop="no"/>
<polygon width="0.127" layer="30">
<vertex x="-1.4021" y="-0.9475" curve="-90"/>
<vertex x="-2.357" y="-0.0178" curve="-90.011749"/>
<vertex x="-1.4046" y="0.9576" curve="-90"/>
<vertex x="-0.4546" y="-0.0204" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-1.4046" y="-0.4395" curve="-90.012891"/>
<vertex x="-1.8491" y="-0.0153" curve="-90"/>
<vertex x="-1.4046" y="0.452" curve="-90"/>
<vertex x="-0.9627" y="-0.0051" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="1.397" y="-0.9475" curve="-90"/>
<vertex x="0.4421" y="-0.0178" curve="-90.011749"/>
<vertex x="1.3945" y="0.9576" curve="-90"/>
<vertex x="2.3445" y="-0.0204" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="1.3945" y="-0.4395" curve="-90.012891"/>
<vertex x="0.95" y="-0.0153" curve="-90"/>
<vertex x="1.3945" y="0.452" curve="-90"/>
<vertex x="1.8364" y="-0.0051" curve="-90.012967"/>
</polygon>
</package>
<package name="CAP-PTH-SMALLEZ">
<description>This is the "EZ" version of the .1" spaced ceramic thru-hole cap.&lt;br&gt;
It has reduced top mask to make it harder to put the component on the wrong side of the board.</description>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="3.81" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="1.27" x2="3.81" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-1.27" x2="-1.27" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="1.27" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="0.7" diameter="1.651" stop="no"/>
<pad name="2" x="2.54" y="0" drill="0.7" diameter="1.651" stop="no"/>
<text x="-1.27" y="1.905" size="0.6096" layer="25">&gt;Name</text>
<text x="-1.27" y="-2.54" size="0.6096" layer="27">&gt;Value</text>
<circle x="0" y="0" radius="0.898025" width="0" layer="30"/>
<circle x="2.54" y="0" radius="0.915809375" width="0" layer="30"/>
<circle x="0" y="0" radius="0.40160625" width="0" layer="29"/>
<circle x="2.54" y="0" radius="0.40160625" width="0" layer="29"/>
</package>
</packages>
<symbols>
<symbol name="CAP">
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="1.524" y="2.921" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CAP" prefix="C" uservalue="yes">
<description>&lt;b&gt;Capacitor&lt;/b&gt;
Standard 0603 ceramic capacitor, and 0.1" leaded capacitor.</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="PTH" package="CAP-PTH-SMALL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH2" package="CAP-PTH-SMALL2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH3" package="CAP-PTH-LARGE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD" package="GRM43D">
<connects>
<connect gate="G$1" pin="1" pad="A"/>
<connect gate="G$1" pin="2" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603-CAP" package="0603-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402-CAP" package="0402-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH1" package="CAP-PTH-5MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_" package="AXIAL-5MM">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="ASMD" package="CTZ3">
<connects>
<connect gate="G$1" pin="1" pad="+"/>
<connect gate="G$1" pin="2" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="KIT" package="CAP-PTH-SMALL-KIT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EZ" package="CAP-PTH-SMALLEZ">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Logos">
<packages>
<package name="DTU-LARGE">
<rectangle x1="-3.7719" y1="-10.4267" x2="-3.7465" y2="-10.4013" layer="21"/>
<rectangle x1="3.4925" y1="-10.4267" x2="3.5433" y2="-10.4013" layer="21"/>
<rectangle x1="-3.8227" y1="-10.4013" x2="-3.6703" y2="-10.3759" layer="21"/>
<rectangle x1="3.4417" y1="-10.4013" x2="3.5941" y2="-10.3759" layer="21"/>
<rectangle x1="-3.8735" y1="-10.3759" x2="-3.5941" y2="-10.3505" layer="21"/>
<rectangle x1="3.3655" y1="-10.3759" x2="3.6449" y2="-10.3505" layer="21"/>
<rectangle x1="-3.9243" y1="-10.3505" x2="-3.5433" y2="-10.3251" layer="21"/>
<rectangle x1="3.3147" y1="-10.3505" x2="3.6957" y2="-10.3251" layer="21"/>
<rectangle x1="-3.9751" y1="-10.3251" x2="-3.4671" y2="-10.2997" layer="21"/>
<rectangle x1="3.2385" y1="-10.3251" x2="3.7465" y2="-10.2997" layer="21"/>
<rectangle x1="-4.0259" y1="-10.2997" x2="-3.4163" y2="-10.2743" layer="21"/>
<rectangle x1="3.1877" y1="-10.2997" x2="3.7973" y2="-10.2743" layer="21"/>
<rectangle x1="-4.0767" y1="-10.2743" x2="-3.3401" y2="-10.2489" layer="21"/>
<rectangle x1="3.1115" y1="-10.2743" x2="3.8481" y2="-10.2489" layer="21"/>
<rectangle x1="-4.1275" y1="-10.2489" x2="-3.2639" y2="-10.2235" layer="21"/>
<rectangle x1="3.0353" y1="-10.2489" x2="3.8989" y2="-10.2235" layer="21"/>
<rectangle x1="-4.1783" y1="-10.2235" x2="-3.2131" y2="-10.1981" layer="21"/>
<rectangle x1="2.9845" y1="-10.2235" x2="3.9497" y2="-10.1981" layer="21"/>
<rectangle x1="-4.2291" y1="-10.1981" x2="-3.1369" y2="-10.1727" layer="21"/>
<rectangle x1="2.9083" y1="-10.1981" x2="4.0005" y2="-10.1727" layer="21"/>
<rectangle x1="-4.2545" y1="-10.1727" x2="-3.0607" y2="-10.1473" layer="21"/>
<rectangle x1="2.8575" y1="-10.1727" x2="4.0259" y2="-10.1473" layer="21"/>
<rectangle x1="-4.3053" y1="-10.1473" x2="-3.0099" y2="-10.1219" layer="21"/>
<rectangle x1="2.7813" y1="-10.1473" x2="4.0767" y2="-10.1219" layer="21"/>
<rectangle x1="-4.3561" y1="-10.1219" x2="-2.9337" y2="-10.0965" layer="21"/>
<rectangle x1="2.7051" y1="-10.1219" x2="4.1275" y2="-10.0965" layer="21"/>
<rectangle x1="-4.4069" y1="-10.0965" x2="-2.8829" y2="-10.0711" layer="21"/>
<rectangle x1="2.6543" y1="-10.0965" x2="4.1783" y2="-10.0711" layer="21"/>
<rectangle x1="-4.4577" y1="-10.0711" x2="-2.8067" y2="-10.0457" layer="21"/>
<rectangle x1="2.5781" y1="-10.0711" x2="4.2291" y2="-10.0457" layer="21"/>
<rectangle x1="-4.5085" y1="-10.0457" x2="-2.7305" y2="-10.0203" layer="21"/>
<rectangle x1="2.5019" y1="-10.0457" x2="4.2799" y2="-10.0203" layer="21"/>
<rectangle x1="-4.5593" y1="-10.0203" x2="-2.6543" y2="-9.9949" layer="21"/>
<rectangle x1="2.4257" y1="-10.0203" x2="4.3307" y2="-9.9949" layer="21"/>
<rectangle x1="-4.6101" y1="-9.9949" x2="-2.6035" y2="-9.9695" layer="21"/>
<rectangle x1="2.3749" y1="-9.9949" x2="4.3815" y2="-9.9695" layer="21"/>
<rectangle x1="-4.6609" y1="-9.9695" x2="-2.5273" y2="-9.9441" layer="21"/>
<rectangle x1="2.2987" y1="-9.9695" x2="4.4323" y2="-9.9441" layer="21"/>
<rectangle x1="-4.7117" y1="-9.9441" x2="-2.4511" y2="-9.9187" layer="21"/>
<rectangle x1="2.2225" y1="-9.9441" x2="4.4831" y2="-9.9187" layer="21"/>
<rectangle x1="-4.7625" y1="-9.9187" x2="-2.3749" y2="-9.8933" layer="21"/>
<rectangle x1="2.1463" y1="-9.9187" x2="4.5339" y2="-9.8933" layer="21"/>
<rectangle x1="-4.7879" y1="-9.8933" x2="-2.3241" y2="-9.8679" layer="21"/>
<rectangle x1="2.0955" y1="-9.8933" x2="4.5593" y2="-9.8679" layer="21"/>
<rectangle x1="-4.8387" y1="-9.8679" x2="-2.2479" y2="-9.8425" layer="21"/>
<rectangle x1="2.0193" y1="-9.8679" x2="4.6101" y2="-9.8425" layer="21"/>
<rectangle x1="-4.8895" y1="-9.8425" x2="-2.1717" y2="-9.8171" layer="21"/>
<rectangle x1="1.9431" y1="-9.8425" x2="4.6609" y2="-9.8171" layer="21"/>
<rectangle x1="-4.9403" y1="-9.8171" x2="-2.0955" y2="-9.7917" layer="21"/>
<rectangle x1="1.8669" y1="-9.8171" x2="4.7117" y2="-9.7917" layer="21"/>
<rectangle x1="-4.9911" y1="-9.7917" x2="-2.0193" y2="-9.7663" layer="21"/>
<rectangle x1="1.7907" y1="-9.7917" x2="4.7625" y2="-9.7663" layer="21"/>
<rectangle x1="-5.0419" y1="-9.7663" x2="-1.9431" y2="-9.7409" layer="21"/>
<rectangle x1="1.7145" y1="-9.7663" x2="4.8133" y2="-9.7409" layer="21"/>
<rectangle x1="-5.0927" y1="-9.7409" x2="-1.8669" y2="-9.7155" layer="21"/>
<rectangle x1="1.6383" y1="-9.7409" x2="4.8641" y2="-9.7155" layer="21"/>
<rectangle x1="-5.1435" y1="-9.7155" x2="-1.7653" y2="-9.6901" layer="21"/>
<rectangle x1="1.5367" y1="-9.7155" x2="4.9149" y2="-9.6901" layer="21"/>
<rectangle x1="-5.1943" y1="-9.6901" x2="-1.6891" y2="-9.6647" layer="21"/>
<rectangle x1="1.4605" y1="-9.6901" x2="4.9657" y2="-9.6647" layer="21"/>
<rectangle x1="-5.2451" y1="-9.6647" x2="-1.6129" y2="-9.6393" layer="21"/>
<rectangle x1="1.3843" y1="-9.6647" x2="5.0165" y2="-9.6393" layer="21"/>
<rectangle x1="-5.2959" y1="-9.6393" x2="-1.5113" y2="-9.6139" layer="21"/>
<rectangle x1="1.2827" y1="-9.6393" x2="5.0673" y2="-9.6139" layer="21"/>
<rectangle x1="-5.3213" y1="-9.6139" x2="-1.4351" y2="-9.5885" layer="21"/>
<rectangle x1="1.2065" y1="-9.6139" x2="5.0927" y2="-9.5885" layer="21"/>
<rectangle x1="-5.3721" y1="-9.5885" x2="-1.3335" y2="-9.5631" layer="21"/>
<rectangle x1="1.1049" y1="-9.5885" x2="5.1435" y2="-9.5631" layer="21"/>
<rectangle x1="-5.4229" y1="-9.5631" x2="-1.2319" y2="-9.5377" layer="21"/>
<rectangle x1="1.0033" y1="-9.5631" x2="5.1943" y2="-9.5377" layer="21"/>
<rectangle x1="-5.4737" y1="-9.5377" x2="-1.1303" y2="-9.5123" layer="21"/>
<rectangle x1="0.9017" y1="-9.5377" x2="5.2451" y2="-9.5123" layer="21"/>
<rectangle x1="-5.5245" y1="-9.5123" x2="-1.0033" y2="-9.4869" layer="21"/>
<rectangle x1="0.7747" y1="-9.5123" x2="5.2959" y2="-9.4869" layer="21"/>
<rectangle x1="-5.5753" y1="-9.4869" x2="-0.8763" y2="-9.4615" layer="21"/>
<rectangle x1="0.6477" y1="-9.4869" x2="5.3467" y2="-9.4615" layer="21"/>
<rectangle x1="-5.6261" y1="-9.4615" x2="-0.6985" y2="-9.4361" layer="21"/>
<rectangle x1="0.4699" y1="-9.4615" x2="5.3975" y2="-9.4361" layer="21"/>
<rectangle x1="-5.6769" y1="-9.4361" x2="-0.4953" y2="-9.4107" layer="21"/>
<rectangle x1="0.2667" y1="-9.4361" x2="5.4483" y2="-9.4107" layer="21"/>
<rectangle x1="-5.7277" y1="-9.4107" x2="5.4991" y2="-9.3853" layer="21"/>
<rectangle x1="-5.7785" y1="-9.3853" x2="5.5499" y2="-9.3599" layer="21"/>
<rectangle x1="-5.8039" y1="-9.3599" x2="5.5753" y2="-9.3345" layer="21"/>
<rectangle x1="-5.8547" y1="-9.3345" x2="5.6261" y2="-9.3091" layer="21"/>
<rectangle x1="-5.9055" y1="-9.3091" x2="5.6769" y2="-9.2837" layer="21"/>
<rectangle x1="-5.9563" y1="-9.2837" x2="5.7277" y2="-9.2583" layer="21"/>
<rectangle x1="-6.0071" y1="-9.2583" x2="5.7785" y2="-9.2329" layer="21"/>
<rectangle x1="-6.0579" y1="-9.2329" x2="5.8293" y2="-9.2075" layer="21"/>
<rectangle x1="-6.1087" y1="-9.2075" x2="5.8801" y2="-9.1821" layer="21"/>
<rectangle x1="-6.1595" y1="-9.1821" x2="5.9309" y2="-9.1567" layer="21"/>
<rectangle x1="-6.2103" y1="-9.1567" x2="5.9817" y2="-9.1313" layer="21"/>
<rectangle x1="-6.2611" y1="-9.1313" x2="6.0325" y2="-9.1059" layer="21"/>
<rectangle x1="-6.3119" y1="-9.1059" x2="6.0833" y2="-9.0805" layer="21"/>
<rectangle x1="-6.3373" y1="-9.0805" x2="6.1087" y2="-9.0551" layer="21"/>
<rectangle x1="-6.3881" y1="-9.0551" x2="6.1595" y2="-9.0297" layer="21"/>
<rectangle x1="-6.4389" y1="-9.0297" x2="6.2103" y2="-9.0043" layer="21"/>
<rectangle x1="-6.4389" y1="-9.0043" x2="6.2103" y2="-8.9789" layer="21"/>
<rectangle x1="-6.3881" y1="-8.9789" x2="6.1595" y2="-8.9535" layer="21"/>
<rectangle x1="-6.3373" y1="-8.9535" x2="6.1087" y2="-8.9281" layer="21"/>
<rectangle x1="-6.2865" y1="-8.9281" x2="6.0579" y2="-8.9027" layer="21"/>
<rectangle x1="-6.2357" y1="-8.9027" x2="6.0071" y2="-8.8773" layer="21"/>
<rectangle x1="-6.1849" y1="-8.8773" x2="5.9563" y2="-8.8519" layer="21"/>
<rectangle x1="-6.1341" y1="-8.8519" x2="5.9055" y2="-8.8265" layer="21"/>
<rectangle x1="-6.0833" y1="-8.8265" x2="5.8547" y2="-8.8011" layer="21"/>
<rectangle x1="-6.0325" y1="-8.8011" x2="5.8039" y2="-8.7757" layer="21"/>
<rectangle x1="-5.9817" y1="-8.7757" x2="5.7531" y2="-8.7503" layer="21"/>
<rectangle x1="-5.9563" y1="-8.7503" x2="5.7277" y2="-8.7249" layer="21"/>
<rectangle x1="-5.9055" y1="-8.7249" x2="5.6769" y2="-8.6995" layer="21"/>
<rectangle x1="-5.8547" y1="-8.6995" x2="5.6261" y2="-8.6741" layer="21"/>
<rectangle x1="-5.8039" y1="-8.6741" x2="5.5753" y2="-8.6487" layer="21"/>
<rectangle x1="-5.7531" y1="-8.6487" x2="5.5245" y2="-8.6233" layer="21"/>
<rectangle x1="-5.7023" y1="-8.6233" x2="-0.2413" y2="-8.5979" layer="21"/>
<rectangle x1="0.0127" y1="-8.6233" x2="5.4737" y2="-8.5979" layer="21"/>
<rectangle x1="-5.6515" y1="-8.5979" x2="-0.5969" y2="-8.5725" layer="21"/>
<rectangle x1="0.3683" y1="-8.5979" x2="5.4229" y2="-8.5725" layer="21"/>
<rectangle x1="-5.6007" y1="-8.5725" x2="-0.7747" y2="-8.5471" layer="21"/>
<rectangle x1="0.5461" y1="-8.5725" x2="5.3721" y2="-8.5471" layer="21"/>
<rectangle x1="-5.5499" y1="-8.5471" x2="-0.9271" y2="-8.5217" layer="21"/>
<rectangle x1="0.6985" y1="-8.5471" x2="5.3213" y2="-8.5217" layer="21"/>
<rectangle x1="-5.4991" y1="-8.5217" x2="-1.0541" y2="-8.4963" layer="21"/>
<rectangle x1="0.8255" y1="-8.5217" x2="5.2705" y2="-8.4963" layer="21"/>
<rectangle x1="-5.4483" y1="-8.4963" x2="-1.1557" y2="-8.4709" layer="21"/>
<rectangle x1="0.9271" y1="-8.4963" x2="5.2197" y2="-8.4709" layer="21"/>
<rectangle x1="-5.4229" y1="-8.4709" x2="-1.2827" y2="-8.4455" layer="21"/>
<rectangle x1="1.0541" y1="-8.4709" x2="5.1943" y2="-8.4455" layer="21"/>
<rectangle x1="-5.3721" y1="-8.4455" x2="-1.3589" y2="-8.4201" layer="21"/>
<rectangle x1="1.1303" y1="-8.4455" x2="5.1435" y2="-8.4201" layer="21"/>
<rectangle x1="-5.3213" y1="-8.4201" x2="-1.4605" y2="-8.3947" layer="21"/>
<rectangle x1="1.2319" y1="-8.4201" x2="5.0927" y2="-8.3947" layer="21"/>
<rectangle x1="-5.2705" y1="-8.3947" x2="-1.5621" y2="-8.3693" layer="21"/>
<rectangle x1="1.3335" y1="-8.3947" x2="5.0419" y2="-8.3693" layer="21"/>
<rectangle x1="-5.2197" y1="-8.3693" x2="-1.6383" y2="-8.3439" layer="21"/>
<rectangle x1="1.4097" y1="-8.3693" x2="4.9911" y2="-8.3439" layer="21"/>
<rectangle x1="-5.1689" y1="-8.3439" x2="-1.7145" y2="-8.3185" layer="21"/>
<rectangle x1="1.4859" y1="-8.3439" x2="4.9403" y2="-8.3185" layer="21"/>
<rectangle x1="-5.1181" y1="-8.3185" x2="-1.8161" y2="-8.2931" layer="21"/>
<rectangle x1="1.5875" y1="-8.3185" x2="4.8895" y2="-8.2931" layer="21"/>
<rectangle x1="-5.0673" y1="-8.2931" x2="-1.8923" y2="-8.2677" layer="21"/>
<rectangle x1="1.6637" y1="-8.2931" x2="4.8387" y2="-8.2677" layer="21"/>
<rectangle x1="-5.0165" y1="-8.2677" x2="-1.9685" y2="-8.2423" layer="21"/>
<rectangle x1="1.7399" y1="-8.2677" x2="4.7879" y2="-8.2423" layer="21"/>
<rectangle x1="-4.9657" y1="-8.2423" x2="-2.0447" y2="-8.2169" layer="21"/>
<rectangle x1="1.8161" y1="-8.2423" x2="4.7371" y2="-8.2169" layer="21"/>
<rectangle x1="-4.9149" y1="-8.2169" x2="-2.1209" y2="-8.1915" layer="21"/>
<rectangle x1="1.8923" y1="-8.2169" x2="4.6863" y2="-8.1915" layer="21"/>
<rectangle x1="-4.8895" y1="-8.1915" x2="-2.1971" y2="-8.1661" layer="21"/>
<rectangle x1="1.9685" y1="-8.1915" x2="4.6609" y2="-8.1661" layer="21"/>
<rectangle x1="-4.8387" y1="-8.1661" x2="-2.2733" y2="-8.1407" layer="21"/>
<rectangle x1="2.0447" y1="-8.1661" x2="4.6101" y2="-8.1407" layer="21"/>
<rectangle x1="-4.7879" y1="-8.1407" x2="-2.3495" y2="-8.1153" layer="21"/>
<rectangle x1="2.1209" y1="-8.1407" x2="4.5593" y2="-8.1153" layer="21"/>
<rectangle x1="-4.7371" y1="-8.1153" x2="-2.4003" y2="-8.0899" layer="21"/>
<rectangle x1="2.1717" y1="-8.1153" x2="4.5085" y2="-8.0899" layer="21"/>
<rectangle x1="-4.6863" y1="-8.0899" x2="-2.4765" y2="-8.0645" layer="21"/>
<rectangle x1="2.2479" y1="-8.0899" x2="4.4577" y2="-8.0645" layer="21"/>
<rectangle x1="-4.6355" y1="-8.0645" x2="-2.5527" y2="-8.0391" layer="21"/>
<rectangle x1="2.3241" y1="-8.0645" x2="4.4069" y2="-8.0391" layer="21"/>
<rectangle x1="-4.5847" y1="-8.0391" x2="-2.6289" y2="-8.0137" layer="21"/>
<rectangle x1="2.4003" y1="-8.0391" x2="4.3561" y2="-8.0137" layer="21"/>
<rectangle x1="-4.5339" y1="-8.0137" x2="-2.6797" y2="-7.9883" layer="21"/>
<rectangle x1="2.4511" y1="-8.0137" x2="4.3053" y2="-7.9883" layer="21"/>
<rectangle x1="-4.4831" y1="-7.9883" x2="-2.7559" y2="-7.9629" layer="21"/>
<rectangle x1="2.5273" y1="-7.9883" x2="4.2545" y2="-7.9629" layer="21"/>
<rectangle x1="-4.4323" y1="-7.9629" x2="-2.8321" y2="-7.9375" layer="21"/>
<rectangle x1="2.6035" y1="-7.9629" x2="4.2037" y2="-7.9375" layer="21"/>
<rectangle x1="-4.4069" y1="-7.9375" x2="-2.9083" y2="-7.9121" layer="21"/>
<rectangle x1="2.6797" y1="-7.9375" x2="4.1783" y2="-7.9121" layer="21"/>
<rectangle x1="-4.3561" y1="-7.9121" x2="-2.9591" y2="-7.8867" layer="21"/>
<rectangle x1="2.7305" y1="-7.9121" x2="4.1275" y2="-7.8867" layer="21"/>
<rectangle x1="-4.3053" y1="-7.8867" x2="-3.0353" y2="-7.8613" layer="21"/>
<rectangle x1="2.8067" y1="-7.8867" x2="4.0767" y2="-7.8613" layer="21"/>
<rectangle x1="-4.2545" y1="-7.8613" x2="-3.0861" y2="-7.8359" layer="21"/>
<rectangle x1="2.8575" y1="-7.8613" x2="4.0259" y2="-7.8359" layer="21"/>
<rectangle x1="-4.2037" y1="-7.8359" x2="-3.1623" y2="-7.8105" layer="21"/>
<rectangle x1="2.9337" y1="-7.8359" x2="3.9751" y2="-7.8105" layer="21"/>
<rectangle x1="-4.1529" y1="-7.8105" x2="-3.2385" y2="-7.7851" layer="21"/>
<rectangle x1="3.0099" y1="-7.8105" x2="3.9243" y2="-7.7851" layer="21"/>
<rectangle x1="-4.1021" y1="-7.7851" x2="-3.2893" y2="-7.7597" layer="21"/>
<rectangle x1="3.0607" y1="-7.7851" x2="3.8735" y2="-7.7597" layer="21"/>
<rectangle x1="-4.0513" y1="-7.7597" x2="-3.3655" y2="-7.7343" layer="21"/>
<rectangle x1="3.1369" y1="-7.7597" x2="3.8227" y2="-7.7343" layer="21"/>
<rectangle x1="-4.0005" y1="-7.7343" x2="-3.4163" y2="-7.7089" layer="21"/>
<rectangle x1="3.1877" y1="-7.7343" x2="3.7719" y2="-7.7089" layer="21"/>
<rectangle x1="-3.9497" y1="-7.7089" x2="-3.4925" y2="-7.6835" layer="21"/>
<rectangle x1="3.2639" y1="-7.7089" x2="3.7211" y2="-7.6835" layer="21"/>
<rectangle x1="-3.8989" y1="-7.6835" x2="-3.5687" y2="-7.6581" layer="21"/>
<rectangle x1="3.3401" y1="-7.6835" x2="3.6703" y2="-7.6581" layer="21"/>
<rectangle x1="-3.8735" y1="-7.6581" x2="-3.6195" y2="-7.6327" layer="21"/>
<rectangle x1="3.3909" y1="-7.6581" x2="3.6449" y2="-7.6327" layer="21"/>
<rectangle x1="-3.8227" y1="-7.6327" x2="-3.6957" y2="-7.6073" layer="21"/>
<rectangle x1="3.4671" y1="-7.6327" x2="3.5941" y2="-7.6073" layer="21"/>
<rectangle x1="-3.7719" y1="-6.9215" x2="-3.7211" y2="-6.8961" layer="21"/>
<rectangle x1="3.4925" y1="-6.9215" x2="3.5433" y2="-6.8961" layer="21"/>
<rectangle x1="-3.8227" y1="-6.8961" x2="-3.6703" y2="-6.8707" layer="21"/>
<rectangle x1="3.4417" y1="-6.8961" x2="3.5941" y2="-6.8707" layer="21"/>
<rectangle x1="-3.8735" y1="-6.8707" x2="-3.5941" y2="-6.8453" layer="21"/>
<rectangle x1="3.3655" y1="-6.8707" x2="3.6449" y2="-6.8453" layer="21"/>
<rectangle x1="-3.9243" y1="-6.8453" x2="-3.5433" y2="-6.8199" layer="21"/>
<rectangle x1="3.3147" y1="-6.8453" x2="3.6957" y2="-6.8199" layer="21"/>
<rectangle x1="-3.9751" y1="-6.8199" x2="-3.4671" y2="-6.7945" layer="21"/>
<rectangle x1="3.2385" y1="-6.8199" x2="3.7465" y2="-6.7945" layer="21"/>
<rectangle x1="-4.0259" y1="-6.7945" x2="-3.3909" y2="-6.7691" layer="21"/>
<rectangle x1="3.1623" y1="-6.7945" x2="3.7973" y2="-6.7691" layer="21"/>
<rectangle x1="-4.0767" y1="-6.7691" x2="-3.3401" y2="-6.7437" layer="21"/>
<rectangle x1="3.1115" y1="-6.7691" x2="3.8481" y2="-6.7437" layer="21"/>
<rectangle x1="-4.1275" y1="-6.7437" x2="-3.2639" y2="-6.7183" layer="21"/>
<rectangle x1="3.0353" y1="-6.7437" x2="3.8989" y2="-6.7183" layer="21"/>
<rectangle x1="-4.1783" y1="-6.7183" x2="-3.2131" y2="-6.6929" layer="21"/>
<rectangle x1="2.9845" y1="-6.7183" x2="3.9497" y2="-6.6929" layer="21"/>
<rectangle x1="-4.2291" y1="-6.6929" x2="-3.1369" y2="-6.6675" layer="21"/>
<rectangle x1="2.9083" y1="-6.6929" x2="4.0005" y2="-6.6675" layer="21"/>
<rectangle x1="-4.2799" y1="-6.6675" x2="-3.0607" y2="-6.6421" layer="21"/>
<rectangle x1="2.8321" y1="-6.6675" x2="4.0513" y2="-6.6421" layer="21"/>
<rectangle x1="-4.3053" y1="-6.6421" x2="-3.0099" y2="-6.6167" layer="21"/>
<rectangle x1="2.7813" y1="-6.6421" x2="4.0767" y2="-6.6167" layer="21"/>
<rectangle x1="-4.3561" y1="-6.6167" x2="-2.9337" y2="-6.5913" layer="21"/>
<rectangle x1="2.7051" y1="-6.6167" x2="4.1275" y2="-6.5913" layer="21"/>
<rectangle x1="-4.4069" y1="-6.5913" x2="-2.8829" y2="-6.5659" layer="21"/>
<rectangle x1="2.6289" y1="-6.5913" x2="4.1783" y2="-6.5659" layer="21"/>
<rectangle x1="-4.4577" y1="-6.5659" x2="-2.8067" y2="-6.5405" layer="21"/>
<rectangle x1="2.5781" y1="-6.5659" x2="4.2291" y2="-6.5405" layer="21"/>
<rectangle x1="-4.5085" y1="-6.5405" x2="-2.7305" y2="-6.5151" layer="21"/>
<rectangle x1="2.5019" y1="-6.5405" x2="4.2799" y2="-6.5151" layer="21"/>
<rectangle x1="-4.5593" y1="-6.5151" x2="-2.6543" y2="-6.4897" layer="21"/>
<rectangle x1="2.4257" y1="-6.5151" x2="4.3307" y2="-6.4897" layer="21"/>
<rectangle x1="-4.6101" y1="-6.4897" x2="-2.6035" y2="-6.4643" layer="21"/>
<rectangle x1="2.3749" y1="-6.4897" x2="4.3815" y2="-6.4643" layer="21"/>
<rectangle x1="-4.6609" y1="-6.4643" x2="-2.5273" y2="-6.4389" layer="21"/>
<rectangle x1="2.2987" y1="-6.4643" x2="4.4323" y2="-6.4389" layer="21"/>
<rectangle x1="-4.7117" y1="-6.4389" x2="-2.4511" y2="-6.4135" layer="21"/>
<rectangle x1="2.2225" y1="-6.4389" x2="4.4831" y2="-6.4135" layer="21"/>
<rectangle x1="-4.7625" y1="-6.4135" x2="-2.3749" y2="-6.3881" layer="21"/>
<rectangle x1="2.1463" y1="-6.4135" x2="4.5339" y2="-6.3881" layer="21"/>
<rectangle x1="-4.8133" y1="-6.3881" x2="-2.3241" y2="-6.3627" layer="21"/>
<rectangle x1="2.0955" y1="-6.3881" x2="4.5593" y2="-6.3627" layer="21"/>
<rectangle x1="-4.8387" y1="-6.3627" x2="-2.2479" y2="-6.3373" layer="21"/>
<rectangle x1="2.0193" y1="-6.3627" x2="4.6101" y2="-6.3373" layer="21"/>
<rectangle x1="-4.8895" y1="-6.3373" x2="-2.1717" y2="-6.3119" layer="21"/>
<rectangle x1="1.9431" y1="-6.3373" x2="4.6609" y2="-6.3119" layer="21"/>
<rectangle x1="-4.9403" y1="-6.3119" x2="-2.0955" y2="-6.2865" layer="21"/>
<rectangle x1="1.8669" y1="-6.3119" x2="4.7117" y2="-6.2865" layer="21"/>
<rectangle x1="-4.9911" y1="-6.2865" x2="-2.0193" y2="-6.2611" layer="21"/>
<rectangle x1="1.7907" y1="-6.2865" x2="4.7625" y2="-6.2611" layer="21"/>
<rectangle x1="-5.0419" y1="-6.2611" x2="-1.9431" y2="-6.2357" layer="21"/>
<rectangle x1="1.7145" y1="-6.2611" x2="4.8133" y2="-6.2357" layer="21"/>
<rectangle x1="-5.0927" y1="-6.2357" x2="-1.8669" y2="-6.2103" layer="21"/>
<rectangle x1="1.6383" y1="-6.2357" x2="4.8641" y2="-6.2103" layer="21"/>
<rectangle x1="-5.1435" y1="-6.2103" x2="-1.7653" y2="-6.1849" layer="21"/>
<rectangle x1="1.5367" y1="-6.2103" x2="4.9149" y2="-6.1849" layer="21"/>
<rectangle x1="-5.1943" y1="-6.1849" x2="-1.6891" y2="-6.1595" layer="21"/>
<rectangle x1="1.4605" y1="-6.1849" x2="4.9657" y2="-6.1595" layer="21"/>
<rectangle x1="-5.2451" y1="-6.1595" x2="-1.6129" y2="-6.1341" layer="21"/>
<rectangle x1="1.3843" y1="-6.1595" x2="5.0165" y2="-6.1341" layer="21"/>
<rectangle x1="-5.2959" y1="-6.1341" x2="-1.5113" y2="-6.1087" layer="21"/>
<rectangle x1="1.2827" y1="-6.1341" x2="5.0673" y2="-6.1087" layer="21"/>
<rectangle x1="-5.3213" y1="-6.1087" x2="-1.4351" y2="-6.0833" layer="21"/>
<rectangle x1="1.2065" y1="-6.1087" x2="5.0927" y2="-6.0833" layer="21"/>
<rectangle x1="-5.3721" y1="-6.0833" x2="-1.3335" y2="-6.0579" layer="21"/>
<rectangle x1="1.1049" y1="-6.0833" x2="5.1435" y2="-6.0579" layer="21"/>
<rectangle x1="-5.4229" y1="-6.0579" x2="-1.2319" y2="-6.0325" layer="21"/>
<rectangle x1="1.0033" y1="-6.0579" x2="5.1943" y2="-6.0325" layer="21"/>
<rectangle x1="-5.4737" y1="-6.0325" x2="-1.1303" y2="-6.0071" layer="21"/>
<rectangle x1="0.9017" y1="-6.0325" x2="5.2451" y2="-6.0071" layer="21"/>
<rectangle x1="-5.5245" y1="-6.0071" x2="-1.0033" y2="-5.9817" layer="21"/>
<rectangle x1="0.7747" y1="-6.0071" x2="5.2959" y2="-5.9817" layer="21"/>
<rectangle x1="-5.5753" y1="-5.9817" x2="-0.8763" y2="-5.9563" layer="21"/>
<rectangle x1="0.6477" y1="-5.9817" x2="5.3467" y2="-5.9563" layer="21"/>
<rectangle x1="-5.6261" y1="-5.9563" x2="-0.6985" y2="-5.9309" layer="21"/>
<rectangle x1="0.4699" y1="-5.9563" x2="5.3975" y2="-5.9309" layer="21"/>
<rectangle x1="-5.6769" y1="-5.9309" x2="-0.4953" y2="-5.9055" layer="21"/>
<rectangle x1="0.2667" y1="-5.9309" x2="5.4483" y2="-5.9055" layer="21"/>
<rectangle x1="-5.7277" y1="-5.9055" x2="5.4991" y2="-5.8801" layer="21"/>
<rectangle x1="-5.7785" y1="-5.8801" x2="5.5499" y2="-5.8547" layer="21"/>
<rectangle x1="-5.8293" y1="-5.8547" x2="5.6007" y2="-5.8293" layer="21"/>
<rectangle x1="-5.8547" y1="-5.8293" x2="5.6261" y2="-5.8039" layer="21"/>
<rectangle x1="-5.9055" y1="-5.8039" x2="5.6769" y2="-5.7785" layer="21"/>
<rectangle x1="-5.9563" y1="-5.7785" x2="5.7277" y2="-5.7531" layer="21"/>
<rectangle x1="-6.0071" y1="-5.7531" x2="5.7785" y2="-5.7277" layer="21"/>
<rectangle x1="-6.0579" y1="-5.7277" x2="5.8293" y2="-5.7023" layer="21"/>
<rectangle x1="-6.1087" y1="-5.7023" x2="5.8801" y2="-5.6769" layer="21"/>
<rectangle x1="-6.1595" y1="-5.6769" x2="5.9309" y2="-5.6515" layer="21"/>
<rectangle x1="-6.2103" y1="-5.6515" x2="5.9817" y2="-5.6261" layer="21"/>
<rectangle x1="-6.2611" y1="-5.6261" x2="6.0325" y2="-5.6007" layer="21"/>
<rectangle x1="-6.3119" y1="-5.6007" x2="6.0833" y2="-5.5753" layer="21"/>
<rectangle x1="-6.3373" y1="-5.5753" x2="6.1087" y2="-5.5499" layer="21"/>
<rectangle x1="-6.3881" y1="-5.5499" x2="6.1595" y2="-5.5245" layer="21"/>
<rectangle x1="-6.4389" y1="-5.5245" x2="6.2103" y2="-5.4991" layer="21"/>
<rectangle x1="-6.4389" y1="-5.4991" x2="6.2103" y2="-5.4737" layer="21"/>
<rectangle x1="-6.3881" y1="-5.4737" x2="6.1595" y2="-5.4483" layer="21"/>
<rectangle x1="-6.3373" y1="-5.4483" x2="6.1087" y2="-5.4229" layer="21"/>
<rectangle x1="-6.2865" y1="-5.4229" x2="6.0579" y2="-5.3975" layer="21"/>
<rectangle x1="-6.2357" y1="-5.3975" x2="6.0071" y2="-5.3721" layer="21"/>
<rectangle x1="-6.1849" y1="-5.3721" x2="5.9563" y2="-5.3467" layer="21"/>
<rectangle x1="-6.1341" y1="-5.3467" x2="5.9055" y2="-5.3213" layer="21"/>
<rectangle x1="-6.0833" y1="-5.3213" x2="5.8547" y2="-5.2959" layer="21"/>
<rectangle x1="-6.0325" y1="-5.2959" x2="5.8039" y2="-5.2705" layer="21"/>
<rectangle x1="-5.9817" y1="-5.2705" x2="5.7531" y2="-5.2451" layer="21"/>
<rectangle x1="-5.9309" y1="-5.2451" x2="5.7023" y2="-5.2197" layer="21"/>
<rectangle x1="-5.9055" y1="-5.2197" x2="5.6769" y2="-5.1943" layer="21"/>
<rectangle x1="-5.8547" y1="-5.1943" x2="5.6261" y2="-5.1689" layer="21"/>
<rectangle x1="-5.8039" y1="-5.1689" x2="5.5753" y2="-5.1435" layer="21"/>
<rectangle x1="-5.7531" y1="-5.1435" x2="5.5245" y2="-5.1181" layer="21"/>
<rectangle x1="-5.7023" y1="-5.1181" x2="-0.2413" y2="-5.0927" layer="21"/>
<rectangle x1="0.0127" y1="-5.1181" x2="5.4737" y2="-5.0927" layer="21"/>
<rectangle x1="-5.6515" y1="-5.0927" x2="-0.5969" y2="-5.0673" layer="21"/>
<rectangle x1="0.3683" y1="-5.0927" x2="5.4229" y2="-5.0673" layer="21"/>
<rectangle x1="-5.6007" y1="-5.0673" x2="-0.7747" y2="-5.0419" layer="21"/>
<rectangle x1="0.5461" y1="-5.0673" x2="5.3721" y2="-5.0419" layer="21"/>
<rectangle x1="-5.5499" y1="-5.0419" x2="-0.9271" y2="-5.0165" layer="21"/>
<rectangle x1="0.6985" y1="-5.0419" x2="5.3213" y2="-5.0165" layer="21"/>
<rectangle x1="-5.4991" y1="-5.0165" x2="-1.0541" y2="-4.9911" layer="21"/>
<rectangle x1="0.8255" y1="-5.0165" x2="5.2705" y2="-4.9911" layer="21"/>
<rectangle x1="-5.4483" y1="-4.9911" x2="-1.1557" y2="-4.9657" layer="21"/>
<rectangle x1="0.9271" y1="-4.9911" x2="5.2197" y2="-4.9657" layer="21"/>
<rectangle x1="-5.4229" y1="-4.9657" x2="-1.2827" y2="-4.9403" layer="21"/>
<rectangle x1="1.0541" y1="-4.9657" x2="5.1943" y2="-4.9403" layer="21"/>
<rectangle x1="-5.3721" y1="-4.9403" x2="-1.3589" y2="-4.9149" layer="21"/>
<rectangle x1="1.1303" y1="-4.9403" x2="5.1435" y2="-4.9149" layer="21"/>
<rectangle x1="-5.3213" y1="-4.9149" x2="-1.4605" y2="-4.8895" layer="21"/>
<rectangle x1="1.2319" y1="-4.9149" x2="5.0927" y2="-4.8895" layer="21"/>
<rectangle x1="-5.2705" y1="-4.8895" x2="-1.5621" y2="-4.8641" layer="21"/>
<rectangle x1="1.3335" y1="-4.8895" x2="5.0419" y2="-4.8641" layer="21"/>
<rectangle x1="-5.2197" y1="-4.8641" x2="-1.6383" y2="-4.8387" layer="21"/>
<rectangle x1="1.4097" y1="-4.8641" x2="4.9911" y2="-4.8387" layer="21"/>
<rectangle x1="-5.1689" y1="-4.8387" x2="-1.7145" y2="-4.8133" layer="21"/>
<rectangle x1="1.4859" y1="-4.8387" x2="4.9403" y2="-4.8133" layer="21"/>
<rectangle x1="-5.1181" y1="-4.8133" x2="-1.8161" y2="-4.7879" layer="21"/>
<rectangle x1="1.5875" y1="-4.8133" x2="4.8895" y2="-4.7879" layer="21"/>
<rectangle x1="-5.0673" y1="-4.7879" x2="-1.8923" y2="-4.7625" layer="21"/>
<rectangle x1="1.6637" y1="-4.7879" x2="4.8387" y2="-4.7625" layer="21"/>
<rectangle x1="-5.0165" y1="-4.7625" x2="-1.9685" y2="-4.7371" layer="21"/>
<rectangle x1="1.7399" y1="-4.7625" x2="4.7879" y2="-4.7371" layer="21"/>
<rectangle x1="-4.9657" y1="-4.7371" x2="-2.0447" y2="-4.7117" layer="21"/>
<rectangle x1="1.8161" y1="-4.7371" x2="4.7371" y2="-4.7117" layer="21"/>
<rectangle x1="-4.9149" y1="-4.7117" x2="-2.1209" y2="-4.6863" layer="21"/>
<rectangle x1="1.8923" y1="-4.7117" x2="4.6863" y2="-4.6863" layer="21"/>
<rectangle x1="-4.8895" y1="-4.6863" x2="-2.1971" y2="-4.6609" layer="21"/>
<rectangle x1="1.9685" y1="-4.6863" x2="4.6609" y2="-4.6609" layer="21"/>
<rectangle x1="-4.8387" y1="-4.6609" x2="-2.2733" y2="-4.6355" layer="21"/>
<rectangle x1="2.0447" y1="-4.6609" x2="4.6101" y2="-4.6355" layer="21"/>
<rectangle x1="-4.7879" y1="-4.6355" x2="-2.3495" y2="-4.6101" layer="21"/>
<rectangle x1="2.1209" y1="-4.6355" x2="4.5593" y2="-4.6101" layer="21"/>
<rectangle x1="-4.7371" y1="-4.6101" x2="-2.4003" y2="-4.5847" layer="21"/>
<rectangle x1="2.1717" y1="-4.6101" x2="4.5085" y2="-4.5847" layer="21"/>
<rectangle x1="-4.6863" y1="-4.5847" x2="-2.4765" y2="-4.5593" layer="21"/>
<rectangle x1="2.2479" y1="-4.5847" x2="4.4577" y2="-4.5593" layer="21"/>
<rectangle x1="-4.6355" y1="-4.5593" x2="-2.5527" y2="-4.5339" layer="21"/>
<rectangle x1="2.3241" y1="-4.5593" x2="4.4069" y2="-4.5339" layer="21"/>
<rectangle x1="-4.5847" y1="-4.5339" x2="-2.6289" y2="-4.5085" layer="21"/>
<rectangle x1="2.4003" y1="-4.5339" x2="4.3561" y2="-4.5085" layer="21"/>
<rectangle x1="-4.5339" y1="-4.5085" x2="-2.6797" y2="-4.4831" layer="21"/>
<rectangle x1="2.4511" y1="-4.5085" x2="4.3053" y2="-4.4831" layer="21"/>
<rectangle x1="-4.4831" y1="-4.4831" x2="-2.7559" y2="-4.4577" layer="21"/>
<rectangle x1="2.5273" y1="-4.4831" x2="4.2545" y2="-4.4577" layer="21"/>
<rectangle x1="-4.4323" y1="-4.4577" x2="-2.8321" y2="-4.4323" layer="21"/>
<rectangle x1="2.6035" y1="-4.4577" x2="4.2037" y2="-4.4323" layer="21"/>
<rectangle x1="-4.4069" y1="-4.4323" x2="-2.9083" y2="-4.4069" layer="21"/>
<rectangle x1="2.6797" y1="-4.4323" x2="4.1529" y2="-4.4069" layer="21"/>
<rectangle x1="-4.3561" y1="-4.4069" x2="-2.9591" y2="-4.3815" layer="21"/>
<rectangle x1="2.7305" y1="-4.4069" x2="4.1275" y2="-4.3815" layer="21"/>
<rectangle x1="-4.3053" y1="-4.3815" x2="-3.0353" y2="-4.3561" layer="21"/>
<rectangle x1="2.8067" y1="-4.3815" x2="4.0767" y2="-4.3561" layer="21"/>
<rectangle x1="-4.2545" y1="-4.3561" x2="-3.0861" y2="-4.3307" layer="21"/>
<rectangle x1="2.8575" y1="-4.3561" x2="4.0259" y2="-4.3307" layer="21"/>
<rectangle x1="-4.2037" y1="-4.3307" x2="-3.1623" y2="-4.3053" layer="21"/>
<rectangle x1="2.9337" y1="-4.3307" x2="3.9751" y2="-4.3053" layer="21"/>
<rectangle x1="-4.1529" y1="-4.3053" x2="-3.2385" y2="-4.2799" layer="21"/>
<rectangle x1="3.0099" y1="-4.3053" x2="3.9243" y2="-4.2799" layer="21"/>
<rectangle x1="-4.1021" y1="-4.2799" x2="-3.2893" y2="-4.2545" layer="21"/>
<rectangle x1="3.0607" y1="-4.2799" x2="3.8735" y2="-4.2545" layer="21"/>
<rectangle x1="-4.0513" y1="-4.2545" x2="-3.3655" y2="-4.2291" layer="21"/>
<rectangle x1="3.1369" y1="-4.2545" x2="3.8227" y2="-4.2291" layer="21"/>
<rectangle x1="-4.0005" y1="-4.2291" x2="-3.4163" y2="-4.2037" layer="21"/>
<rectangle x1="3.1877" y1="-4.2291" x2="3.7719" y2="-4.2037" layer="21"/>
<rectangle x1="-3.9497" y1="-4.2037" x2="-3.4925" y2="-4.1783" layer="21"/>
<rectangle x1="3.2639" y1="-4.2037" x2="3.7211" y2="-4.1783" layer="21"/>
<rectangle x1="-3.8989" y1="-4.1783" x2="-3.5687" y2="-4.1529" layer="21"/>
<rectangle x1="3.3401" y1="-4.1783" x2="3.6703" y2="-4.1529" layer="21"/>
<rectangle x1="-3.8735" y1="-4.1529" x2="-3.6195" y2="-4.1275" layer="21"/>
<rectangle x1="3.3909" y1="-4.1529" x2="3.6449" y2="-4.1275" layer="21"/>
<rectangle x1="-3.8227" y1="-4.1275" x2="-3.6957" y2="-4.1021" layer="21"/>
<rectangle x1="3.4671" y1="-4.1275" x2="3.5941" y2="-4.1021" layer="21"/>
<rectangle x1="-3.7719" y1="-3.4163" x2="-3.7211" y2="-3.3909" layer="21"/>
<rectangle x1="3.4925" y1="-3.4163" x2="3.5433" y2="-3.3909" layer="21"/>
<rectangle x1="-3.8227" y1="-3.3909" x2="-3.6703" y2="-3.3655" layer="21"/>
<rectangle x1="3.4417" y1="-3.3909" x2="3.5941" y2="-3.3655" layer="21"/>
<rectangle x1="-3.8735" y1="-3.3655" x2="-3.5941" y2="-3.3401" layer="21"/>
<rectangle x1="3.3655" y1="-3.3655" x2="3.6449" y2="-3.3401" layer="21"/>
<rectangle x1="-3.9243" y1="-3.3401" x2="-3.5433" y2="-3.3147" layer="21"/>
<rectangle x1="3.3147" y1="-3.3401" x2="3.6957" y2="-3.3147" layer="21"/>
<rectangle x1="-3.9751" y1="-3.3147" x2="-3.4671" y2="-3.2893" layer="21"/>
<rectangle x1="3.2385" y1="-3.3147" x2="3.7465" y2="-3.2893" layer="21"/>
<rectangle x1="-4.0259" y1="-3.2893" x2="-3.3909" y2="-3.2639" layer="21"/>
<rectangle x1="3.1623" y1="-3.2893" x2="3.7973" y2="-3.2639" layer="21"/>
<rectangle x1="-4.0767" y1="-3.2639" x2="-3.3401" y2="-3.2385" layer="21"/>
<rectangle x1="3.1115" y1="-3.2639" x2="3.8481" y2="-3.2385" layer="21"/>
<rectangle x1="-4.1275" y1="-3.2385" x2="-3.2639" y2="-3.2131" layer="21"/>
<rectangle x1="3.0353" y1="-3.2385" x2="3.8989" y2="-3.2131" layer="21"/>
<rectangle x1="-4.1783" y1="-3.2131" x2="-3.2131" y2="-3.1877" layer="21"/>
<rectangle x1="2.9845" y1="-3.2131" x2="3.9497" y2="-3.1877" layer="21"/>
<rectangle x1="-4.2291" y1="-3.1877" x2="-3.1369" y2="-3.1623" layer="21"/>
<rectangle x1="2.9083" y1="-3.1877" x2="4.0005" y2="-3.1623" layer="21"/>
<rectangle x1="-4.2799" y1="-3.1623" x2="-3.0607" y2="-3.1369" layer="21"/>
<rectangle x1="2.8321" y1="-3.1623" x2="4.0513" y2="-3.1369" layer="21"/>
<rectangle x1="-4.3053" y1="-3.1369" x2="-3.0099" y2="-3.1115" layer="21"/>
<rectangle x1="2.7813" y1="-3.1369" x2="4.0767" y2="-3.1115" layer="21"/>
<rectangle x1="-4.3561" y1="-3.1115" x2="-2.9337" y2="-3.0861" layer="21"/>
<rectangle x1="2.7051" y1="-3.1115" x2="4.1275" y2="-3.0861" layer="21"/>
<rectangle x1="-4.4069" y1="-3.0861" x2="-2.8829" y2="-3.0607" layer="21"/>
<rectangle x1="2.6289" y1="-3.0861" x2="4.1783" y2="-3.0607" layer="21"/>
<rectangle x1="-4.4577" y1="-3.0607" x2="-2.8067" y2="-3.0353" layer="21"/>
<rectangle x1="2.5781" y1="-3.0607" x2="4.2291" y2="-3.0353" layer="21"/>
<rectangle x1="-4.5085" y1="-3.0353" x2="-2.7305" y2="-3.0099" layer="21"/>
<rectangle x1="2.5019" y1="-3.0353" x2="4.2799" y2="-3.0099" layer="21"/>
<rectangle x1="-4.5593" y1="-3.0099" x2="-2.6543" y2="-2.9845" layer="21"/>
<rectangle x1="2.4257" y1="-3.0099" x2="4.3307" y2="-2.9845" layer="21"/>
<rectangle x1="-4.6101" y1="-2.9845" x2="-2.6035" y2="-2.9591" layer="21"/>
<rectangle x1="2.3749" y1="-2.9845" x2="4.3815" y2="-2.9591" layer="21"/>
<rectangle x1="-4.6609" y1="-2.9591" x2="-2.5273" y2="-2.9337" layer="21"/>
<rectangle x1="2.2987" y1="-2.9591" x2="4.4323" y2="-2.9337" layer="21"/>
<rectangle x1="-4.7117" y1="-2.9337" x2="-2.4511" y2="-2.9083" layer="21"/>
<rectangle x1="2.2225" y1="-2.9337" x2="4.4831" y2="-2.9083" layer="21"/>
<rectangle x1="-4.7625" y1="-2.9083" x2="-2.3749" y2="-2.8829" layer="21"/>
<rectangle x1="2.1463" y1="-2.9083" x2="4.5339" y2="-2.8829" layer="21"/>
<rectangle x1="-4.8133" y1="-2.8829" x2="-2.3241" y2="-2.8575" layer="21"/>
<rectangle x1="2.0955" y1="-2.8829" x2="4.5847" y2="-2.8575" layer="21"/>
<rectangle x1="-4.8387" y1="-2.8575" x2="-2.2479" y2="-2.8321" layer="21"/>
<rectangle x1="2.0193" y1="-2.8575" x2="4.6101" y2="-2.8321" layer="21"/>
<rectangle x1="-4.8895" y1="-2.8321" x2="-2.1717" y2="-2.8067" layer="21"/>
<rectangle x1="1.9431" y1="-2.8321" x2="4.6609" y2="-2.8067" layer="21"/>
<rectangle x1="-4.9403" y1="-2.8067" x2="-2.0955" y2="-2.7813" layer="21"/>
<rectangle x1="1.8669" y1="-2.8067" x2="4.7117" y2="-2.7813" layer="21"/>
<rectangle x1="-4.9911" y1="-2.7813" x2="-2.0193" y2="-2.7559" layer="21"/>
<rectangle x1="1.7907" y1="-2.7813" x2="4.7625" y2="-2.7559" layer="21"/>
<rectangle x1="-5.0419" y1="-2.7559" x2="-1.9431" y2="-2.7305" layer="21"/>
<rectangle x1="1.7145" y1="-2.7559" x2="4.8133" y2="-2.7305" layer="21"/>
<rectangle x1="-5.0927" y1="-2.7305" x2="-1.8669" y2="-2.7051" layer="21"/>
<rectangle x1="1.6383" y1="-2.7305" x2="4.8641" y2="-2.7051" layer="21"/>
<rectangle x1="-5.1435" y1="-2.7051" x2="-1.7653" y2="-2.6797" layer="21"/>
<rectangle x1="1.5367" y1="-2.7051" x2="4.9149" y2="-2.6797" layer="21"/>
<rectangle x1="-5.1943" y1="-2.6797" x2="-1.6891" y2="-2.6543" layer="21"/>
<rectangle x1="1.4605" y1="-2.6797" x2="4.9657" y2="-2.6543" layer="21"/>
<rectangle x1="-5.2451" y1="-2.6543" x2="-1.6129" y2="-2.6289" layer="21"/>
<rectangle x1="1.3843" y1="-2.6543" x2="5.0165" y2="-2.6289" layer="21"/>
<rectangle x1="-5.2959" y1="-2.6289" x2="-1.5113" y2="-2.6035" layer="21"/>
<rectangle x1="1.2827" y1="-2.6289" x2="5.0673" y2="-2.6035" layer="21"/>
<rectangle x1="-5.3213" y1="-2.6035" x2="-1.4351" y2="-2.5781" layer="21"/>
<rectangle x1="1.2065" y1="-2.6035" x2="5.0927" y2="-2.5781" layer="21"/>
<rectangle x1="-5.3721" y1="-2.5781" x2="-1.3335" y2="-2.5527" layer="21"/>
<rectangle x1="1.1049" y1="-2.5781" x2="5.1435" y2="-2.5527" layer="21"/>
<rectangle x1="-5.4229" y1="-2.5527" x2="-1.2319" y2="-2.5273" layer="21"/>
<rectangle x1="1.0033" y1="-2.5527" x2="5.1943" y2="-2.5273" layer="21"/>
<rectangle x1="-5.4737" y1="-2.5273" x2="-1.1303" y2="-2.5019" layer="21"/>
<rectangle x1="0.9017" y1="-2.5273" x2="5.2451" y2="-2.5019" layer="21"/>
<rectangle x1="-5.5245" y1="-2.5019" x2="-1.0033" y2="-2.4765" layer="21"/>
<rectangle x1="0.7747" y1="-2.5019" x2="5.2959" y2="-2.4765" layer="21"/>
<rectangle x1="-5.5753" y1="-2.4765" x2="-0.8763" y2="-2.4511" layer="21"/>
<rectangle x1="0.6477" y1="-2.4765" x2="5.3467" y2="-2.4511" layer="21"/>
<rectangle x1="-5.6261" y1="-2.4511" x2="-0.6985" y2="-2.4257" layer="21"/>
<rectangle x1="0.4699" y1="-2.4511" x2="5.3975" y2="-2.4257" layer="21"/>
<rectangle x1="-5.6769" y1="-2.4257" x2="-0.4953" y2="-2.4003" layer="21"/>
<rectangle x1="0.2667" y1="-2.4257" x2="5.4483" y2="-2.4003" layer="21"/>
<rectangle x1="-5.7277" y1="-2.4003" x2="5.4991" y2="-2.3749" layer="21"/>
<rectangle x1="-5.7785" y1="-2.3749" x2="5.5499" y2="-2.3495" layer="21"/>
<rectangle x1="-5.8293" y1="-2.3495" x2="5.6007" y2="-2.3241" layer="21"/>
<rectangle x1="-5.8547" y1="-2.3241" x2="5.6261" y2="-2.2987" layer="21"/>
<rectangle x1="-5.9055" y1="-2.2987" x2="5.6769" y2="-2.2733" layer="21"/>
<rectangle x1="-5.9563" y1="-2.2733" x2="5.7277" y2="-2.2479" layer="21"/>
<rectangle x1="-6.0071" y1="-2.2479" x2="5.7785" y2="-2.2225" layer="21"/>
<rectangle x1="-6.0579" y1="-2.2225" x2="5.8293" y2="-2.1971" layer="21"/>
<rectangle x1="-6.1087" y1="-2.1971" x2="5.8801" y2="-2.1717" layer="21"/>
<rectangle x1="-6.1595" y1="-2.1717" x2="5.9309" y2="-2.1463" layer="21"/>
<rectangle x1="-6.2103" y1="-2.1463" x2="5.9817" y2="-2.1209" layer="21"/>
<rectangle x1="-6.2611" y1="-2.1209" x2="6.0325" y2="-2.0955" layer="21"/>
<rectangle x1="-6.3119" y1="-2.0955" x2="6.0833" y2="-2.0701" layer="21"/>
<rectangle x1="-6.3627" y1="-2.0701" x2="6.1341" y2="-2.0447" layer="21"/>
<rectangle x1="-6.3881" y1="-2.0447" x2="6.1595" y2="-2.0193" layer="21"/>
<rectangle x1="-6.4389" y1="-2.0193" x2="6.2103" y2="-1.9939" layer="21"/>
<rectangle x1="-6.4389" y1="-1.9939" x2="6.2103" y2="-1.9685" layer="21"/>
<rectangle x1="-6.3881" y1="-1.9685" x2="6.1595" y2="-1.9431" layer="21"/>
<rectangle x1="-6.3373" y1="-1.9431" x2="6.1087" y2="-1.9177" layer="21"/>
<rectangle x1="-6.2865" y1="-1.9177" x2="6.0579" y2="-1.8923" layer="21"/>
<rectangle x1="-6.2357" y1="-1.8923" x2="6.0071" y2="-1.8669" layer="21"/>
<rectangle x1="-6.1849" y1="-1.8669" x2="5.9563" y2="-1.8415" layer="21"/>
<rectangle x1="-6.1341" y1="-1.8415" x2="5.9055" y2="-1.8161" layer="21"/>
<rectangle x1="-6.0833" y1="-1.8161" x2="5.8547" y2="-1.7907" layer="21"/>
<rectangle x1="-6.0325" y1="-1.7907" x2="5.8039" y2="-1.7653" layer="21"/>
<rectangle x1="-5.9817" y1="-1.7653" x2="5.7531" y2="-1.7399" layer="21"/>
<rectangle x1="-5.9309" y1="-1.7399" x2="5.7023" y2="-1.7145" layer="21"/>
<rectangle x1="-5.9055" y1="-1.7145" x2="5.6769" y2="-1.6891" layer="21"/>
<rectangle x1="-5.8547" y1="-1.6891" x2="5.6261" y2="-1.6637" layer="21"/>
<rectangle x1="-5.8039" y1="-1.6637" x2="5.5753" y2="-1.6383" layer="21"/>
<rectangle x1="-5.7531" y1="-1.6383" x2="5.5245" y2="-1.6129" layer="21"/>
<rectangle x1="-5.7023" y1="-1.6129" x2="-0.2413" y2="-1.5875" layer="21"/>
<rectangle x1="0.0127" y1="-1.6129" x2="5.4737" y2="-1.5875" layer="21"/>
<rectangle x1="-5.6515" y1="-1.5875" x2="-0.5969" y2="-1.5621" layer="21"/>
<rectangle x1="0.3683" y1="-1.5875" x2="5.4229" y2="-1.5621" layer="21"/>
<rectangle x1="-5.6007" y1="-1.5621" x2="-0.7747" y2="-1.5367" layer="21"/>
<rectangle x1="0.5461" y1="-1.5621" x2="5.3721" y2="-1.5367" layer="21"/>
<rectangle x1="-5.5499" y1="-1.5367" x2="-0.9271" y2="-1.5113" layer="21"/>
<rectangle x1="0.6985" y1="-1.5367" x2="5.3213" y2="-1.5113" layer="21"/>
<rectangle x1="-5.4991" y1="-1.5113" x2="-1.0541" y2="-1.4859" layer="21"/>
<rectangle x1="0.8255" y1="-1.5113" x2="5.2705" y2="-1.4859" layer="21"/>
<rectangle x1="-5.4483" y1="-1.4859" x2="-1.1557" y2="-1.4605" layer="21"/>
<rectangle x1="0.9271" y1="-1.4859" x2="5.2197" y2="-1.4605" layer="21"/>
<rectangle x1="-5.4229" y1="-1.4605" x2="-1.2827" y2="-1.4351" layer="21"/>
<rectangle x1="1.0541" y1="-1.4605" x2="5.1943" y2="-1.4351" layer="21"/>
<rectangle x1="-5.3721" y1="-1.4351" x2="-1.3589" y2="-1.4097" layer="21"/>
<rectangle x1="1.1303" y1="-1.4351" x2="5.1435" y2="-1.4097" layer="21"/>
<rectangle x1="-5.3213" y1="-1.4097" x2="-1.4605" y2="-1.3843" layer="21"/>
<rectangle x1="1.2319" y1="-1.4097" x2="5.0927" y2="-1.3843" layer="21"/>
<rectangle x1="-5.2705" y1="-1.3843" x2="-1.5621" y2="-1.3589" layer="21"/>
<rectangle x1="1.3335" y1="-1.3843" x2="5.0419" y2="-1.3589" layer="21"/>
<rectangle x1="-5.2197" y1="-1.3589" x2="-1.6383" y2="-1.3335" layer="21"/>
<rectangle x1="1.4097" y1="-1.3589" x2="4.9911" y2="-1.3335" layer="21"/>
<rectangle x1="-5.1689" y1="-1.3335" x2="-1.7145" y2="-1.3081" layer="21"/>
<rectangle x1="1.4859" y1="-1.3335" x2="4.9403" y2="-1.3081" layer="21"/>
<rectangle x1="-5.1181" y1="-1.3081" x2="-1.8161" y2="-1.2827" layer="21"/>
<rectangle x1="1.5875" y1="-1.3081" x2="4.8895" y2="-1.2827" layer="21"/>
<rectangle x1="-5.0673" y1="-1.2827" x2="-1.8923" y2="-1.2573" layer="21"/>
<rectangle x1="1.6637" y1="-1.2827" x2="4.8387" y2="-1.2573" layer="21"/>
<rectangle x1="-5.0165" y1="-1.2573" x2="-1.9685" y2="-1.2319" layer="21"/>
<rectangle x1="1.7399" y1="-1.2573" x2="4.7879" y2="-1.2319" layer="21"/>
<rectangle x1="-4.9657" y1="-1.2319" x2="-2.0447" y2="-1.2065" layer="21"/>
<rectangle x1="1.8161" y1="-1.2319" x2="4.7371" y2="-1.2065" layer="21"/>
<rectangle x1="-4.9149" y1="-1.2065" x2="-2.1209" y2="-1.1811" layer="21"/>
<rectangle x1="1.8923" y1="-1.2065" x2="4.6863" y2="-1.1811" layer="21"/>
<rectangle x1="-4.8895" y1="-1.1811" x2="-2.1971" y2="-1.1557" layer="21"/>
<rectangle x1="1.9685" y1="-1.1811" x2="4.6609" y2="-1.1557" layer="21"/>
<rectangle x1="-4.8387" y1="-1.1557" x2="-2.2733" y2="-1.1303" layer="21"/>
<rectangle x1="2.0447" y1="-1.1557" x2="4.6101" y2="-1.1303" layer="21"/>
<rectangle x1="-4.7879" y1="-1.1303" x2="-2.3495" y2="-1.1049" layer="21"/>
<rectangle x1="2.1209" y1="-1.1303" x2="4.5593" y2="-1.1049" layer="21"/>
<rectangle x1="-4.7371" y1="-1.1049" x2="-2.4003" y2="-1.0795" layer="21"/>
<rectangle x1="2.1717" y1="-1.1049" x2="4.5085" y2="-1.0795" layer="21"/>
<rectangle x1="-4.6863" y1="-1.0795" x2="-2.4765" y2="-1.0541" layer="21"/>
<rectangle x1="2.2479" y1="-1.0795" x2="4.4577" y2="-1.0541" layer="21"/>
<rectangle x1="-4.6355" y1="-1.0541" x2="-2.5527" y2="-1.0287" layer="21"/>
<rectangle x1="2.3241" y1="-1.0541" x2="4.4069" y2="-1.0287" layer="21"/>
<rectangle x1="-4.5847" y1="-1.0287" x2="-2.6289" y2="-1.0033" layer="21"/>
<rectangle x1="2.4003" y1="-1.0287" x2="4.3561" y2="-1.0033" layer="21"/>
<rectangle x1="-4.5339" y1="-1.0033" x2="-2.6797" y2="-0.9779" layer="21"/>
<rectangle x1="2.4511" y1="-1.0033" x2="4.3053" y2="-0.9779" layer="21"/>
<rectangle x1="-4.4831" y1="-0.9779" x2="-2.7559" y2="-0.9525" layer="21"/>
<rectangle x1="2.5273" y1="-0.9779" x2="4.2545" y2="-0.9525" layer="21"/>
<rectangle x1="-4.4323" y1="-0.9525" x2="-2.8321" y2="-0.9271" layer="21"/>
<rectangle x1="2.6035" y1="-0.9525" x2="4.2037" y2="-0.9271" layer="21"/>
<rectangle x1="-4.4069" y1="-0.9271" x2="-2.9083" y2="-0.9017" layer="21"/>
<rectangle x1="2.6797" y1="-0.9271" x2="4.1529" y2="-0.9017" layer="21"/>
<rectangle x1="-4.3561" y1="-0.9017" x2="-2.9591" y2="-0.8763" layer="21"/>
<rectangle x1="2.7305" y1="-0.9017" x2="4.1275" y2="-0.8763" layer="21"/>
<rectangle x1="-4.3053" y1="-0.8763" x2="-3.0353" y2="-0.8509" layer="21"/>
<rectangle x1="2.8067" y1="-0.8763" x2="4.0767" y2="-0.8509" layer="21"/>
<rectangle x1="-4.2545" y1="-0.8509" x2="-3.0861" y2="-0.8255" layer="21"/>
<rectangle x1="2.8575" y1="-0.8509" x2="4.0259" y2="-0.8255" layer="21"/>
<rectangle x1="-4.2037" y1="-0.8255" x2="-3.1623" y2="-0.8001" layer="21"/>
<rectangle x1="2.9337" y1="-0.8255" x2="3.9751" y2="-0.8001" layer="21"/>
<rectangle x1="-4.1529" y1="-0.8001" x2="-3.2385" y2="-0.7747" layer="21"/>
<rectangle x1="3.0099" y1="-0.8001" x2="3.9243" y2="-0.7747" layer="21"/>
<rectangle x1="-4.1021" y1="-0.7747" x2="-3.2893" y2="-0.7493" layer="21"/>
<rectangle x1="3.0607" y1="-0.7747" x2="3.8735" y2="-0.7493" layer="21"/>
<rectangle x1="-4.0513" y1="-0.7493" x2="-3.3655" y2="-0.7239" layer="21"/>
<rectangle x1="3.1369" y1="-0.7493" x2="3.8227" y2="-0.7239" layer="21"/>
<rectangle x1="-4.0005" y1="-0.7239" x2="-3.4163" y2="-0.6985" layer="21"/>
<rectangle x1="3.1877" y1="-0.7239" x2="3.7719" y2="-0.6985" layer="21"/>
<rectangle x1="-3.9497" y1="-0.6985" x2="-3.4925" y2="-0.6731" layer="21"/>
<rectangle x1="3.2639" y1="-0.6985" x2="3.7211" y2="-0.6731" layer="21"/>
<rectangle x1="-3.8989" y1="-0.6731" x2="-3.5687" y2="-0.6477" layer="21"/>
<rectangle x1="3.3401" y1="-0.6731" x2="3.6703" y2="-0.6477" layer="21"/>
<rectangle x1="-3.8735" y1="-0.6477" x2="-3.6195" y2="-0.6223" layer="21"/>
<rectangle x1="3.3909" y1="-0.6477" x2="3.6449" y2="-0.6223" layer="21"/>
<rectangle x1="-3.8227" y1="-0.6223" x2="-3.6957" y2="-0.5969" layer="21"/>
<rectangle x1="3.4671" y1="-0.6223" x2="3.5941" y2="-0.5969" layer="21"/>
<rectangle x1="3.4925" y1="1.2827" x2="4.0767" y2="1.3081" layer="21"/>
<rectangle x1="3.2893" y1="1.3081" x2="4.2799" y2="1.3335" layer="21"/>
<rectangle x1="3.1623" y1="1.3335" x2="4.4069" y2="1.3589" layer="21"/>
<rectangle x1="-5.6769" y1="1.3589" x2="-3.7973" y2="1.3843" layer="21"/>
<rectangle x1="-0.5969" y1="1.3589" x2="0.3429" y2="1.3843" layer="21"/>
<rectangle x1="3.0607" y1="1.3589" x2="4.4831" y2="1.3843" layer="21"/>
<rectangle x1="-5.7531" y1="1.3843" x2="-3.5179" y2="1.4097" layer="21"/>
<rectangle x1="-0.6731" y1="1.3843" x2="0.3937" y2="1.4097" layer="21"/>
<rectangle x1="2.9845" y1="1.3843" x2="4.5847" y2="1.4097" layer="21"/>
<rectangle x1="-5.7531" y1="1.4097" x2="-3.3655" y2="1.4351" layer="21"/>
<rectangle x1="-0.6731" y1="1.4097" x2="0.4191" y2="1.4351" layer="21"/>
<rectangle x1="2.9083" y1="1.4097" x2="4.6355" y2="1.4351" layer="21"/>
<rectangle x1="-5.7785" y1="1.4351" x2="-3.2639" y2="1.4605" layer="21"/>
<rectangle x1="-0.6985" y1="1.4351" x2="0.4191" y2="1.4605" layer="21"/>
<rectangle x1="2.8575" y1="1.4351" x2="4.7117" y2="1.4605" layer="21"/>
<rectangle x1="-5.7785" y1="1.4605" x2="-3.1877" y2="1.4859" layer="21"/>
<rectangle x1="-0.6985" y1="1.4605" x2="0.4191" y2="1.4859" layer="21"/>
<rectangle x1="2.8067" y1="1.4605" x2="4.7625" y2="1.4859" layer="21"/>
<rectangle x1="-5.7785" y1="1.4859" x2="-3.1115" y2="1.5113" layer="21"/>
<rectangle x1="-0.6985" y1="1.4859" x2="0.4445" y2="1.5113" layer="21"/>
<rectangle x1="2.7559" y1="1.4859" x2="4.8133" y2="1.5113" layer="21"/>
<rectangle x1="-5.7785" y1="1.5113" x2="-3.0607" y2="1.5367" layer="21"/>
<rectangle x1="-0.6985" y1="1.5113" x2="0.4445" y2="1.5367" layer="21"/>
<rectangle x1="2.7051" y1="1.5113" x2="4.8387" y2="1.5367" layer="21"/>
<rectangle x1="-5.7785" y1="1.5367" x2="-3.0099" y2="1.5621" layer="21"/>
<rectangle x1="-0.6985" y1="1.5367" x2="0.4445" y2="1.5621" layer="21"/>
<rectangle x1="2.6543" y1="1.5367" x2="4.8895" y2="1.5621" layer="21"/>
<rectangle x1="-5.7785" y1="1.5621" x2="-2.9591" y2="1.5875" layer="21"/>
<rectangle x1="-0.6985" y1="1.5621" x2="0.4445" y2="1.5875" layer="21"/>
<rectangle x1="2.6289" y1="1.5621" x2="4.9403" y2="1.5875" layer="21"/>
<rectangle x1="-5.7785" y1="1.5875" x2="-2.9083" y2="1.6129" layer="21"/>
<rectangle x1="-0.6985" y1="1.5875" x2="0.4445" y2="1.6129" layer="21"/>
<rectangle x1="2.6035" y1="1.5875" x2="4.9657" y2="1.6129" layer="21"/>
<rectangle x1="-5.7785" y1="1.6129" x2="-2.8829" y2="1.6383" layer="21"/>
<rectangle x1="-0.6985" y1="1.6129" x2="0.4445" y2="1.6383" layer="21"/>
<rectangle x1="2.5527" y1="1.6129" x2="4.9911" y2="1.6383" layer="21"/>
<rectangle x1="-5.7785" y1="1.6383" x2="-2.8321" y2="1.6637" layer="21"/>
<rectangle x1="-0.6985" y1="1.6383" x2="0.4445" y2="1.6637" layer="21"/>
<rectangle x1="2.5273" y1="1.6383" x2="5.0165" y2="1.6637" layer="21"/>
<rectangle x1="-5.7785" y1="1.6637" x2="-2.8067" y2="1.6891" layer="21"/>
<rectangle x1="-0.6985" y1="1.6637" x2="0.4445" y2="1.6891" layer="21"/>
<rectangle x1="2.5019" y1="1.6637" x2="5.0419" y2="1.6891" layer="21"/>
<rectangle x1="-5.7785" y1="1.6891" x2="-2.7813" y2="1.7145" layer="21"/>
<rectangle x1="-0.6985" y1="1.6891" x2="0.4445" y2="1.7145" layer="21"/>
<rectangle x1="2.4765" y1="1.6891" x2="5.0673" y2="1.7145" layer="21"/>
<rectangle x1="-5.7785" y1="1.7145" x2="-2.7559" y2="1.7399" layer="21"/>
<rectangle x1="-0.6985" y1="1.7145" x2="0.4445" y2="1.7399" layer="21"/>
<rectangle x1="2.4511" y1="1.7145" x2="5.0927" y2="1.7399" layer="21"/>
<rectangle x1="-5.7785" y1="1.7399" x2="-2.7305" y2="1.7653" layer="21"/>
<rectangle x1="-0.6985" y1="1.7399" x2="0.4445" y2="1.7653" layer="21"/>
<rectangle x1="2.4257" y1="1.7399" x2="5.1181" y2="1.7653" layer="21"/>
<rectangle x1="-5.7785" y1="1.7653" x2="-2.7051" y2="1.7907" layer="21"/>
<rectangle x1="-0.6985" y1="1.7653" x2="0.4445" y2="1.7907" layer="21"/>
<rectangle x1="2.4003" y1="1.7653" x2="5.1435" y2="1.7907" layer="21"/>
<rectangle x1="-5.7785" y1="1.7907" x2="-2.6797" y2="1.8161" layer="21"/>
<rectangle x1="-0.6985" y1="1.7907" x2="0.4445" y2="1.8161" layer="21"/>
<rectangle x1="2.3749" y1="1.7907" x2="5.1689" y2="1.8161" layer="21"/>
<rectangle x1="-5.7785" y1="1.8161" x2="-2.6543" y2="1.8415" layer="21"/>
<rectangle x1="-0.6985" y1="1.8161" x2="0.4445" y2="1.8415" layer="21"/>
<rectangle x1="2.3495" y1="1.8161" x2="5.1943" y2="1.8415" layer="21"/>
<rectangle x1="-5.7785" y1="1.8415" x2="-2.6543" y2="1.8669" layer="21"/>
<rectangle x1="-0.6985" y1="1.8415" x2="0.4445" y2="1.8669" layer="21"/>
<rectangle x1="2.3495" y1="1.8415" x2="5.2197" y2="1.8669" layer="21"/>
<rectangle x1="-5.7785" y1="1.8669" x2="-2.6289" y2="1.8923" layer="21"/>
<rectangle x1="-0.6985" y1="1.8669" x2="0.4445" y2="1.8923" layer="21"/>
<rectangle x1="2.3241" y1="1.8669" x2="5.2197" y2="1.8923" layer="21"/>
<rectangle x1="-5.7785" y1="1.8923" x2="-2.6035" y2="1.9177" layer="21"/>
<rectangle x1="-0.6985" y1="1.8923" x2="0.4445" y2="1.9177" layer="21"/>
<rectangle x1="2.2987" y1="1.8923" x2="5.2451" y2="1.9177" layer="21"/>
<rectangle x1="-5.7785" y1="1.9177" x2="-2.5781" y2="1.9431" layer="21"/>
<rectangle x1="-0.6985" y1="1.9177" x2="0.4445" y2="1.9431" layer="21"/>
<rectangle x1="2.2987" y1="1.9177" x2="5.2705" y2="1.9431" layer="21"/>
<rectangle x1="-5.7785" y1="1.9431" x2="-2.5781" y2="1.9685" layer="21"/>
<rectangle x1="-0.6985" y1="1.9431" x2="0.4445" y2="1.9685" layer="21"/>
<rectangle x1="2.2733" y1="1.9431" x2="5.2705" y2="1.9685" layer="21"/>
<rectangle x1="-5.7785" y1="1.9685" x2="-2.5527" y2="1.9939" layer="21"/>
<rectangle x1="-0.6985" y1="1.9685" x2="0.4445" y2="1.9939" layer="21"/>
<rectangle x1="2.2479" y1="1.9685" x2="5.2959" y2="1.9939" layer="21"/>
<rectangle x1="-5.7785" y1="1.9939" x2="-2.5527" y2="2.0193" layer="21"/>
<rectangle x1="-0.6985" y1="1.9939" x2="0.4445" y2="2.0193" layer="21"/>
<rectangle x1="2.2479" y1="1.9939" x2="5.2959" y2="2.0193" layer="21"/>
<rectangle x1="-5.7785" y1="2.0193" x2="-2.5273" y2="2.0447" layer="21"/>
<rectangle x1="-0.6985" y1="2.0193" x2="0.4445" y2="2.0447" layer="21"/>
<rectangle x1="2.2225" y1="2.0193" x2="5.3213" y2="2.0447" layer="21"/>
<rectangle x1="-5.7785" y1="2.0447" x2="-2.5273" y2="2.0701" layer="21"/>
<rectangle x1="-0.6985" y1="2.0447" x2="0.4445" y2="2.0701" layer="21"/>
<rectangle x1="2.2225" y1="2.0447" x2="3.6195" y2="2.0701" layer="21"/>
<rectangle x1="3.9243" y1="2.0447" x2="5.3213" y2="2.0701" layer="21"/>
<rectangle x1="-5.7785" y1="2.0701" x2="-2.5019" y2="2.0955" layer="21"/>
<rectangle x1="-0.6985" y1="2.0701" x2="0.4445" y2="2.0955" layer="21"/>
<rectangle x1="2.1971" y1="2.0701" x2="3.5179" y2="2.0955" layer="21"/>
<rectangle x1="4.0259" y1="2.0701" x2="5.3467" y2="2.0955" layer="21"/>
<rectangle x1="-5.7785" y1="2.0955" x2="-2.5019" y2="2.1209" layer="21"/>
<rectangle x1="-0.6985" y1="2.0955" x2="0.4445" y2="2.1209" layer="21"/>
<rectangle x1="2.1971" y1="2.0955" x2="3.4671" y2="2.1209" layer="21"/>
<rectangle x1="4.1021" y1="2.0955" x2="5.3467" y2="2.1209" layer="21"/>
<rectangle x1="-5.7785" y1="2.1209" x2="-2.4765" y2="2.1463" layer="21"/>
<rectangle x1="-0.6985" y1="2.1209" x2="0.4445" y2="2.1463" layer="21"/>
<rectangle x1="2.1971" y1="2.1209" x2="3.4163" y2="2.1463" layer="21"/>
<rectangle x1="4.1529" y1="2.1209" x2="5.3721" y2="2.1463" layer="21"/>
<rectangle x1="-5.7785" y1="2.1463" x2="-4.7117" y2="2.1717" layer="21"/>
<rectangle x1="-3.9497" y1="2.1463" x2="-2.4765" y2="2.1717" layer="21"/>
<rectangle x1="-0.6985" y1="2.1463" x2="0.4445" y2="2.1717" layer="21"/>
<rectangle x1="2.1717" y1="2.1463" x2="3.3909" y2="2.1717" layer="21"/>
<rectangle x1="4.1783" y1="2.1463" x2="5.3721" y2="2.1717" layer="21"/>
<rectangle x1="-5.7785" y1="2.1717" x2="-4.7117" y2="2.1971" layer="21"/>
<rectangle x1="-3.8227" y1="2.1717" x2="-2.4765" y2="2.1971" layer="21"/>
<rectangle x1="-0.6985" y1="2.1717" x2="0.4445" y2="2.1971" layer="21"/>
<rectangle x1="2.1717" y1="2.1717" x2="3.3401" y2="2.1971" layer="21"/>
<rectangle x1="4.2037" y1="2.1717" x2="5.3975" y2="2.1971" layer="21"/>
<rectangle x1="-5.7785" y1="2.1971" x2="-4.7117" y2="2.2225" layer="21"/>
<rectangle x1="-3.7465" y1="2.1971" x2="-2.4511" y2="2.2225" layer="21"/>
<rectangle x1="-0.6985" y1="2.1971" x2="0.4445" y2="2.2225" layer="21"/>
<rectangle x1="2.1717" y1="2.1971" x2="3.3147" y2="2.2225" layer="21"/>
<rectangle x1="4.2291" y1="2.1971" x2="5.3975" y2="2.2225" layer="21"/>
<rectangle x1="-5.7785" y1="2.2225" x2="-4.7117" y2="2.2479" layer="21"/>
<rectangle x1="-3.6957" y1="2.2225" x2="-2.4511" y2="2.2479" layer="21"/>
<rectangle x1="-0.6985" y1="2.2225" x2="0.4445" y2="2.2479" layer="21"/>
<rectangle x1="2.1463" y1="2.2225" x2="3.3147" y2="2.2479" layer="21"/>
<rectangle x1="4.2545" y1="2.2225" x2="5.3975" y2="2.2479" layer="21"/>
<rectangle x1="-5.7785" y1="2.2479" x2="-4.7117" y2="2.2733" layer="21"/>
<rectangle x1="-3.6703" y1="2.2479" x2="-2.4257" y2="2.2733" layer="21"/>
<rectangle x1="-0.6985" y1="2.2479" x2="0.4445" y2="2.2733" layer="21"/>
<rectangle x1="2.1463" y1="2.2479" x2="3.2893" y2="2.2733" layer="21"/>
<rectangle x1="4.2799" y1="2.2479" x2="5.4229" y2="2.2733" layer="21"/>
<rectangle x1="-5.7785" y1="2.2733" x2="-4.7117" y2="2.2987" layer="21"/>
<rectangle x1="-3.6449" y1="2.2733" x2="-2.4257" y2="2.2987" layer="21"/>
<rectangle x1="-0.6985" y1="2.2733" x2="0.4445" y2="2.2987" layer="21"/>
<rectangle x1="2.1463" y1="2.2733" x2="3.2639" y2="2.2987" layer="21"/>
<rectangle x1="4.3053" y1="2.2733" x2="5.4229" y2="2.2987" layer="21"/>
<rectangle x1="-5.7785" y1="2.2987" x2="-4.7117" y2="2.3241" layer="21"/>
<rectangle x1="-3.6195" y1="2.2987" x2="-2.4257" y2="2.3241" layer="21"/>
<rectangle x1="-0.6985" y1="2.2987" x2="0.4445" y2="2.3241" layer="21"/>
<rectangle x1="2.1209" y1="2.2987" x2="3.2385" y2="2.3241" layer="21"/>
<rectangle x1="4.3307" y1="2.2987" x2="5.4229" y2="2.3241" layer="21"/>
<rectangle x1="-5.7785" y1="2.3241" x2="-4.7117" y2="2.3495" layer="21"/>
<rectangle x1="-3.5941" y1="2.3241" x2="-2.4257" y2="2.3495" layer="21"/>
<rectangle x1="-0.6985" y1="2.3241" x2="0.4445" y2="2.3495" layer="21"/>
<rectangle x1="2.1209" y1="2.3241" x2="3.2385" y2="2.3495" layer="21"/>
<rectangle x1="4.3307" y1="2.3241" x2="5.4229" y2="2.3495" layer="21"/>
<rectangle x1="-5.7785" y1="2.3495" x2="-4.7117" y2="2.3749" layer="21"/>
<rectangle x1="-3.5687" y1="2.3495" x2="-2.4003" y2="2.3749" layer="21"/>
<rectangle x1="-0.6985" y1="2.3495" x2="0.4445" y2="2.3749" layer="21"/>
<rectangle x1="2.1209" y1="2.3495" x2="3.2131" y2="2.3749" layer="21"/>
<rectangle x1="4.3561" y1="2.3495" x2="5.4483" y2="2.3749" layer="21"/>
<rectangle x1="-5.7785" y1="2.3749" x2="-4.7117" y2="2.4003" layer="21"/>
<rectangle x1="-3.5433" y1="2.3749" x2="-2.4003" y2="2.4003" layer="21"/>
<rectangle x1="-0.6985" y1="2.3749" x2="0.4445" y2="2.4003" layer="21"/>
<rectangle x1="2.1209" y1="2.3749" x2="3.2131" y2="2.4003" layer="21"/>
<rectangle x1="4.3561" y1="2.3749" x2="5.4483" y2="2.4003" layer="21"/>
<rectangle x1="-5.7785" y1="2.4003" x2="-4.7117" y2="2.4257" layer="21"/>
<rectangle x1="-3.5433" y1="2.4003" x2="-2.4003" y2="2.4257" layer="21"/>
<rectangle x1="-0.6985" y1="2.4003" x2="0.4445" y2="2.4257" layer="21"/>
<rectangle x1="2.0955" y1="2.4003" x2="3.1877" y2="2.4257" layer="21"/>
<rectangle x1="4.3561" y1="2.4003" x2="5.4483" y2="2.4257" layer="21"/>
<rectangle x1="-5.7785" y1="2.4257" x2="-4.7117" y2="2.4511" layer="21"/>
<rectangle x1="-3.5179" y1="2.4257" x2="-2.4003" y2="2.4511" layer="21"/>
<rectangle x1="-0.6985" y1="2.4257" x2="0.4445" y2="2.4511" layer="21"/>
<rectangle x1="2.0955" y1="2.4257" x2="3.1877" y2="2.4511" layer="21"/>
<rectangle x1="4.3815" y1="2.4257" x2="5.4483" y2="2.4511" layer="21"/>
<rectangle x1="-5.7785" y1="2.4511" x2="-4.7117" y2="2.4765" layer="21"/>
<rectangle x1="-3.5179" y1="2.4511" x2="-2.3749" y2="2.4765" layer="21"/>
<rectangle x1="-0.6985" y1="2.4511" x2="0.4445" y2="2.4765" layer="21"/>
<rectangle x1="2.0955" y1="2.4511" x2="3.1877" y2="2.4765" layer="21"/>
<rectangle x1="4.3815" y1="2.4511" x2="5.4737" y2="2.4765" layer="21"/>
<rectangle x1="-5.7785" y1="2.4765" x2="-4.7117" y2="2.5019" layer="21"/>
<rectangle x1="-3.4925" y1="2.4765" x2="-2.3749" y2="2.5019" layer="21"/>
<rectangle x1="-0.6985" y1="2.4765" x2="0.4445" y2="2.5019" layer="21"/>
<rectangle x1="2.0955" y1="2.4765" x2="3.1877" y2="2.5019" layer="21"/>
<rectangle x1="4.3815" y1="2.4765" x2="5.4737" y2="2.5019" layer="21"/>
<rectangle x1="-5.7785" y1="2.5019" x2="-4.7117" y2="2.5273" layer="21"/>
<rectangle x1="-3.4925" y1="2.5019" x2="-2.3749" y2="2.5273" layer="21"/>
<rectangle x1="-0.6985" y1="2.5019" x2="0.4445" y2="2.5273" layer="21"/>
<rectangle x1="2.0955" y1="2.5019" x2="3.1623" y2="2.5273" layer="21"/>
<rectangle x1="4.4069" y1="2.5019" x2="5.4737" y2="2.5273" layer="21"/>
<rectangle x1="-5.7785" y1="2.5273" x2="-4.7117" y2="2.5527" layer="21"/>
<rectangle x1="-3.4671" y1="2.5273" x2="-2.3749" y2="2.5527" layer="21"/>
<rectangle x1="-0.6985" y1="2.5273" x2="0.4445" y2="2.5527" layer="21"/>
<rectangle x1="2.0955" y1="2.5273" x2="3.1623" y2="2.5527" layer="21"/>
<rectangle x1="4.4069" y1="2.5273" x2="5.4737" y2="2.5527" layer="21"/>
<rectangle x1="-5.7785" y1="2.5527" x2="-4.7117" y2="2.5781" layer="21"/>
<rectangle x1="-3.4671" y1="2.5527" x2="-2.3749" y2="2.5781" layer="21"/>
<rectangle x1="-0.6985" y1="2.5527" x2="0.4445" y2="2.5781" layer="21"/>
<rectangle x1="2.0701" y1="2.5527" x2="3.1623" y2="2.5781" layer="21"/>
<rectangle x1="4.4069" y1="2.5527" x2="5.4737" y2="2.5781" layer="21"/>
<rectangle x1="-5.7785" y1="2.5781" x2="-4.7117" y2="2.6035" layer="21"/>
<rectangle x1="-3.4671" y1="2.5781" x2="-2.3495" y2="2.6035" layer="21"/>
<rectangle x1="-0.6985" y1="2.5781" x2="0.4445" y2="2.6035" layer="21"/>
<rectangle x1="2.0701" y1="2.5781" x2="3.1623" y2="2.6035" layer="21"/>
<rectangle x1="4.4069" y1="2.5781" x2="5.4737" y2="2.6035" layer="21"/>
<rectangle x1="-5.7785" y1="2.6035" x2="-4.7117" y2="2.6289" layer="21"/>
<rectangle x1="-3.4417" y1="2.6035" x2="-2.3495" y2="2.6289" layer="21"/>
<rectangle x1="-0.6985" y1="2.6035" x2="0.4445" y2="2.6289" layer="21"/>
<rectangle x1="2.0701" y1="2.6035" x2="3.1369" y2="2.6289" layer="21"/>
<rectangle x1="4.4069" y1="2.6035" x2="5.4991" y2="2.6289" layer="21"/>
<rectangle x1="-5.7785" y1="2.6289" x2="-4.7117" y2="2.6543" layer="21"/>
<rectangle x1="-3.4417" y1="2.6289" x2="-2.3495" y2="2.6543" layer="21"/>
<rectangle x1="-0.6985" y1="2.6289" x2="0.4445" y2="2.6543" layer="21"/>
<rectangle x1="2.0701" y1="2.6289" x2="3.1369" y2="2.6543" layer="21"/>
<rectangle x1="4.4323" y1="2.6289" x2="5.4991" y2="2.6543" layer="21"/>
<rectangle x1="-5.7785" y1="2.6543" x2="-4.7117" y2="2.6797" layer="21"/>
<rectangle x1="-3.4417" y1="2.6543" x2="-2.3495" y2="2.6797" layer="21"/>
<rectangle x1="-0.6985" y1="2.6543" x2="0.4445" y2="2.6797" layer="21"/>
<rectangle x1="2.0701" y1="2.6543" x2="3.1369" y2="2.6797" layer="21"/>
<rectangle x1="4.4323" y1="2.6543" x2="5.4991" y2="2.6797" layer="21"/>
<rectangle x1="-5.7785" y1="2.6797" x2="-4.7117" y2="2.7051" layer="21"/>
<rectangle x1="-3.4417" y1="2.6797" x2="-2.3495" y2="2.7051" layer="21"/>
<rectangle x1="-0.6985" y1="2.6797" x2="0.4445" y2="2.7051" layer="21"/>
<rectangle x1="2.0701" y1="2.6797" x2="3.1369" y2="2.7051" layer="21"/>
<rectangle x1="4.4323" y1="2.6797" x2="5.4991" y2="2.7051" layer="21"/>
<rectangle x1="-5.7785" y1="2.7051" x2="-4.7117" y2="2.7305" layer="21"/>
<rectangle x1="-3.4417" y1="2.7051" x2="-2.3495" y2="2.7305" layer="21"/>
<rectangle x1="-0.6985" y1="2.7051" x2="0.4445" y2="2.7305" layer="21"/>
<rectangle x1="2.0701" y1="2.7051" x2="3.1369" y2="2.7305" layer="21"/>
<rectangle x1="4.4323" y1="2.7051" x2="5.4991" y2="2.7305" layer="21"/>
<rectangle x1="-5.7785" y1="2.7305" x2="-4.7117" y2="2.7559" layer="21"/>
<rectangle x1="-3.4163" y1="2.7305" x2="-2.3495" y2="2.7559" layer="21"/>
<rectangle x1="-0.6985" y1="2.7305" x2="0.4445" y2="2.7559" layer="21"/>
<rectangle x1="2.0701" y1="2.7305" x2="3.1369" y2="2.7559" layer="21"/>
<rectangle x1="4.4323" y1="2.7305" x2="5.4991" y2="2.7559" layer="21"/>
<rectangle x1="-5.7785" y1="2.7559" x2="-4.7117" y2="2.7813" layer="21"/>
<rectangle x1="-3.4163" y1="2.7559" x2="-2.3495" y2="2.7813" layer="21"/>
<rectangle x1="-0.6985" y1="2.7559" x2="0.4445" y2="2.7813" layer="21"/>
<rectangle x1="2.0701" y1="2.7559" x2="3.1369" y2="2.7813" layer="21"/>
<rectangle x1="4.4323" y1="2.7559" x2="5.4991" y2="2.7813" layer="21"/>
<rectangle x1="-5.7785" y1="2.7813" x2="-4.7117" y2="2.8067" layer="21"/>
<rectangle x1="-3.4163" y1="2.7813" x2="-2.3241" y2="2.8067" layer="21"/>
<rectangle x1="-0.6985" y1="2.7813" x2="0.4445" y2="2.8067" layer="21"/>
<rectangle x1="2.0701" y1="2.7813" x2="3.1369" y2="2.8067" layer="21"/>
<rectangle x1="4.4323" y1="2.7813" x2="5.4991" y2="2.8067" layer="21"/>
<rectangle x1="-5.7785" y1="2.8067" x2="-4.7117" y2="2.8321" layer="21"/>
<rectangle x1="-3.4163" y1="2.8067" x2="-2.3241" y2="2.8321" layer="21"/>
<rectangle x1="-0.6985" y1="2.8067" x2="0.4445" y2="2.8321" layer="21"/>
<rectangle x1="2.0447" y1="2.8067" x2="3.1115" y2="2.8321" layer="21"/>
<rectangle x1="4.4323" y1="2.8067" x2="5.4991" y2="2.8321" layer="21"/>
<rectangle x1="-5.7785" y1="2.8321" x2="-4.7117" y2="2.8575" layer="21"/>
<rectangle x1="-3.4163" y1="2.8321" x2="-2.3241" y2="2.8575" layer="21"/>
<rectangle x1="-0.6985" y1="2.8321" x2="0.4445" y2="2.8575" layer="21"/>
<rectangle x1="2.0447" y1="2.8321" x2="3.1115" y2="2.8575" layer="21"/>
<rectangle x1="4.4323" y1="2.8321" x2="5.4991" y2="2.8575" layer="21"/>
<rectangle x1="-5.7785" y1="2.8575" x2="-4.7117" y2="2.8829" layer="21"/>
<rectangle x1="-3.4163" y1="2.8575" x2="-2.3241" y2="2.8829" layer="21"/>
<rectangle x1="-0.6985" y1="2.8575" x2="0.4445" y2="2.8829" layer="21"/>
<rectangle x1="2.0447" y1="2.8575" x2="3.1115" y2="2.8829" layer="21"/>
<rectangle x1="4.4577" y1="2.8575" x2="5.5245" y2="2.8829" layer="21"/>
<rectangle x1="-5.7785" y1="2.8829" x2="-4.7117" y2="2.9083" layer="21"/>
<rectangle x1="-3.4163" y1="2.8829" x2="-2.3241" y2="2.9083" layer="21"/>
<rectangle x1="-0.6985" y1="2.8829" x2="0.4445" y2="2.9083" layer="21"/>
<rectangle x1="2.0447" y1="2.8829" x2="3.1115" y2="2.9083" layer="21"/>
<rectangle x1="4.4577" y1="2.8829" x2="5.5245" y2="2.9083" layer="21"/>
<rectangle x1="-5.7785" y1="2.9083" x2="-4.7117" y2="2.9337" layer="21"/>
<rectangle x1="-3.4163" y1="2.9083" x2="-2.3241" y2="2.9337" layer="21"/>
<rectangle x1="-0.6985" y1="2.9083" x2="0.4445" y2="2.9337" layer="21"/>
<rectangle x1="2.0447" y1="2.9083" x2="3.1115" y2="2.9337" layer="21"/>
<rectangle x1="4.4577" y1="2.9083" x2="5.5245" y2="2.9337" layer="21"/>
<rectangle x1="-5.7785" y1="2.9337" x2="-4.7117" y2="2.9591" layer="21"/>
<rectangle x1="-3.3909" y1="2.9337" x2="-2.3241" y2="2.9591" layer="21"/>
<rectangle x1="-0.6985" y1="2.9337" x2="0.4445" y2="2.9591" layer="21"/>
<rectangle x1="2.0447" y1="2.9337" x2="3.1115" y2="2.9591" layer="21"/>
<rectangle x1="4.4577" y1="2.9337" x2="5.5245" y2="2.9591" layer="21"/>
<rectangle x1="-5.7785" y1="2.9591" x2="-4.7117" y2="2.9845" layer="21"/>
<rectangle x1="-3.3909" y1="2.9591" x2="-2.3241" y2="2.9845" layer="21"/>
<rectangle x1="-0.6985" y1="2.9591" x2="0.4445" y2="2.9845" layer="21"/>
<rectangle x1="2.0447" y1="2.9591" x2="3.1115" y2="2.9845" layer="21"/>
<rectangle x1="4.4577" y1="2.9591" x2="5.5245" y2="2.9845" layer="21"/>
<rectangle x1="-5.7785" y1="2.9845" x2="-4.7117" y2="3.0099" layer="21"/>
<rectangle x1="-3.3909" y1="2.9845" x2="-2.3241" y2="3.0099" layer="21"/>
<rectangle x1="-0.6985" y1="2.9845" x2="0.4445" y2="3.0099" layer="21"/>
<rectangle x1="2.0447" y1="2.9845" x2="3.1115" y2="3.0099" layer="21"/>
<rectangle x1="4.4577" y1="2.9845" x2="5.5245" y2="3.0099" layer="21"/>
<rectangle x1="-5.7785" y1="3.0099" x2="-4.7117" y2="3.0353" layer="21"/>
<rectangle x1="-3.3909" y1="3.0099" x2="-2.3241" y2="3.0353" layer="21"/>
<rectangle x1="-0.6985" y1="3.0099" x2="0.4445" y2="3.0353" layer="21"/>
<rectangle x1="2.0447" y1="3.0099" x2="3.1115" y2="3.0353" layer="21"/>
<rectangle x1="4.4577" y1="3.0099" x2="5.5245" y2="3.0353" layer="21"/>
<rectangle x1="-5.7785" y1="3.0353" x2="-4.7117" y2="3.0607" layer="21"/>
<rectangle x1="-3.3909" y1="3.0353" x2="-2.3241" y2="3.0607" layer="21"/>
<rectangle x1="-0.6985" y1="3.0353" x2="0.4445" y2="3.0607" layer="21"/>
<rectangle x1="2.0447" y1="3.0353" x2="3.1115" y2="3.0607" layer="21"/>
<rectangle x1="4.4577" y1="3.0353" x2="5.5245" y2="3.0607" layer="21"/>
<rectangle x1="-5.7785" y1="3.0607" x2="-4.7117" y2="3.0861" layer="21"/>
<rectangle x1="-3.3909" y1="3.0607" x2="-2.3241" y2="3.0861" layer="21"/>
<rectangle x1="-0.6985" y1="3.0607" x2="0.4445" y2="3.0861" layer="21"/>
<rectangle x1="2.0447" y1="3.0607" x2="3.1115" y2="3.0861" layer="21"/>
<rectangle x1="4.4577" y1="3.0607" x2="5.5245" y2="3.0861" layer="21"/>
<rectangle x1="-5.7785" y1="3.0861" x2="-4.7117" y2="3.1115" layer="21"/>
<rectangle x1="-3.3909" y1="3.0861" x2="-2.3241" y2="3.1115" layer="21"/>
<rectangle x1="-0.6985" y1="3.0861" x2="0.4445" y2="3.1115" layer="21"/>
<rectangle x1="2.0447" y1="3.0861" x2="3.1115" y2="3.1115" layer="21"/>
<rectangle x1="4.4577" y1="3.0861" x2="5.5245" y2="3.1115" layer="21"/>
<rectangle x1="-5.7785" y1="3.1115" x2="-4.7117" y2="3.1369" layer="21"/>
<rectangle x1="-3.3909" y1="3.1115" x2="-2.2987" y2="3.1369" layer="21"/>
<rectangle x1="-0.6985" y1="3.1115" x2="0.4445" y2="3.1369" layer="21"/>
<rectangle x1="2.0447" y1="3.1115" x2="3.1115" y2="3.1369" layer="21"/>
<rectangle x1="4.4577" y1="3.1115" x2="5.5245" y2="3.1369" layer="21"/>
<rectangle x1="-5.7785" y1="3.1369" x2="-4.7117" y2="3.1623" layer="21"/>
<rectangle x1="-3.3909" y1="3.1369" x2="-2.2987" y2="3.1623" layer="21"/>
<rectangle x1="-0.6985" y1="3.1369" x2="0.4445" y2="3.1623" layer="21"/>
<rectangle x1="2.0447" y1="3.1369" x2="3.1115" y2="3.1623" layer="21"/>
<rectangle x1="4.4577" y1="3.1369" x2="5.5245" y2="3.1623" layer="21"/>
<rectangle x1="-5.7785" y1="3.1623" x2="-4.7117" y2="3.1877" layer="21"/>
<rectangle x1="-3.3909" y1="3.1623" x2="-2.2987" y2="3.1877" layer="21"/>
<rectangle x1="-0.6985" y1="3.1623" x2="0.4445" y2="3.1877" layer="21"/>
<rectangle x1="2.0447" y1="3.1623" x2="3.1115" y2="3.1877" layer="21"/>
<rectangle x1="4.4577" y1="3.1623" x2="5.5245" y2="3.1877" layer="21"/>
<rectangle x1="-5.7785" y1="3.1877" x2="-4.7117" y2="3.2131" layer="21"/>
<rectangle x1="-3.3909" y1="3.1877" x2="-2.2987" y2="3.2131" layer="21"/>
<rectangle x1="-0.6985" y1="3.1877" x2="0.4445" y2="3.2131" layer="21"/>
<rectangle x1="2.0447" y1="3.1877" x2="3.1115" y2="3.2131" layer="21"/>
<rectangle x1="4.4577" y1="3.1877" x2="5.5245" y2="3.2131" layer="21"/>
<rectangle x1="-5.7785" y1="3.2131" x2="-4.7117" y2="3.2385" layer="21"/>
<rectangle x1="-3.3909" y1="3.2131" x2="-2.2987" y2="3.2385" layer="21"/>
<rectangle x1="-0.6985" y1="3.2131" x2="0.4445" y2="3.2385" layer="21"/>
<rectangle x1="2.0447" y1="3.2131" x2="3.1115" y2="3.2385" layer="21"/>
<rectangle x1="4.4577" y1="3.2131" x2="5.5245" y2="3.2385" layer="21"/>
<rectangle x1="-5.7785" y1="3.2385" x2="-4.7117" y2="3.2639" layer="21"/>
<rectangle x1="-3.3909" y1="3.2385" x2="-2.2987" y2="3.2639" layer="21"/>
<rectangle x1="-0.6985" y1="3.2385" x2="0.4445" y2="3.2639" layer="21"/>
<rectangle x1="2.0447" y1="3.2385" x2="3.1115" y2="3.2639" layer="21"/>
<rectangle x1="4.4577" y1="3.2385" x2="5.5245" y2="3.2639" layer="21"/>
<rectangle x1="-5.7785" y1="3.2639" x2="-4.7117" y2="3.2893" layer="21"/>
<rectangle x1="-3.3909" y1="3.2639" x2="-2.2987" y2="3.2893" layer="21"/>
<rectangle x1="-0.6985" y1="3.2639" x2="0.4445" y2="3.2893" layer="21"/>
<rectangle x1="2.0447" y1="3.2639" x2="3.1115" y2="3.2893" layer="21"/>
<rectangle x1="4.4577" y1="3.2639" x2="5.5245" y2="3.2893" layer="21"/>
<rectangle x1="-5.7785" y1="3.2893" x2="-4.7117" y2="3.3147" layer="21"/>
<rectangle x1="-3.3909" y1="3.2893" x2="-2.2987" y2="3.3147" layer="21"/>
<rectangle x1="-0.6985" y1="3.2893" x2="0.4445" y2="3.3147" layer="21"/>
<rectangle x1="2.0447" y1="3.2893" x2="3.1115" y2="3.3147" layer="21"/>
<rectangle x1="4.4577" y1="3.2893" x2="5.5245" y2="3.3147" layer="21"/>
<rectangle x1="-5.7785" y1="3.3147" x2="-4.7117" y2="3.3401" layer="21"/>
<rectangle x1="-3.3909" y1="3.3147" x2="-2.2987" y2="3.3401" layer="21"/>
<rectangle x1="-0.6985" y1="3.3147" x2="0.4445" y2="3.3401" layer="21"/>
<rectangle x1="2.0447" y1="3.3147" x2="3.1115" y2="3.3401" layer="21"/>
<rectangle x1="4.4577" y1="3.3147" x2="5.5245" y2="3.3401" layer="21"/>
<rectangle x1="-5.7785" y1="3.3401" x2="-4.7117" y2="3.3655" layer="21"/>
<rectangle x1="-3.3909" y1="3.3401" x2="-2.2987" y2="3.3655" layer="21"/>
<rectangle x1="-0.6985" y1="3.3401" x2="0.4445" y2="3.3655" layer="21"/>
<rectangle x1="2.0447" y1="3.3401" x2="3.1115" y2="3.3655" layer="21"/>
<rectangle x1="4.4577" y1="3.3401" x2="5.5245" y2="3.3655" layer="21"/>
<rectangle x1="-5.7785" y1="3.3655" x2="-4.7117" y2="3.3909" layer="21"/>
<rectangle x1="-3.3909" y1="3.3655" x2="-2.2987" y2="3.3909" layer="21"/>
<rectangle x1="-0.6985" y1="3.3655" x2="0.4445" y2="3.3909" layer="21"/>
<rectangle x1="2.0447" y1="3.3655" x2="3.1115" y2="3.3909" layer="21"/>
<rectangle x1="4.4577" y1="3.3655" x2="5.5245" y2="3.3909" layer="21"/>
<rectangle x1="-5.7785" y1="3.3909" x2="-4.7117" y2="3.4163" layer="21"/>
<rectangle x1="-3.3909" y1="3.3909" x2="-2.2987" y2="3.4163" layer="21"/>
<rectangle x1="-0.6985" y1="3.3909" x2="0.4445" y2="3.4163" layer="21"/>
<rectangle x1="2.0447" y1="3.3909" x2="3.1115" y2="3.4163" layer="21"/>
<rectangle x1="4.4577" y1="3.3909" x2="5.5245" y2="3.4163" layer="21"/>
<rectangle x1="-5.7785" y1="3.4163" x2="-4.7117" y2="3.4417" layer="21"/>
<rectangle x1="-3.3909" y1="3.4163" x2="-2.2987" y2="3.4417" layer="21"/>
<rectangle x1="-0.6985" y1="3.4163" x2="0.4445" y2="3.4417" layer="21"/>
<rectangle x1="2.0447" y1="3.4163" x2="3.1115" y2="3.4417" layer="21"/>
<rectangle x1="4.4577" y1="3.4163" x2="5.5245" y2="3.4417" layer="21"/>
<rectangle x1="-5.7785" y1="3.4417" x2="-4.7117" y2="3.4671" layer="21"/>
<rectangle x1="-3.3909" y1="3.4417" x2="-2.2987" y2="3.4671" layer="21"/>
<rectangle x1="-0.6985" y1="3.4417" x2="0.4445" y2="3.4671" layer="21"/>
<rectangle x1="2.0447" y1="3.4417" x2="3.1115" y2="3.4671" layer="21"/>
<rectangle x1="4.4577" y1="3.4417" x2="5.5245" y2="3.4671" layer="21"/>
<rectangle x1="-5.7785" y1="3.4671" x2="-4.7117" y2="3.4925" layer="21"/>
<rectangle x1="-3.3909" y1="3.4671" x2="-2.2987" y2="3.4925" layer="21"/>
<rectangle x1="-0.6985" y1="3.4671" x2="0.4445" y2="3.4925" layer="21"/>
<rectangle x1="2.0447" y1="3.4671" x2="3.1115" y2="3.4925" layer="21"/>
<rectangle x1="4.4577" y1="3.4671" x2="5.5245" y2="3.4925" layer="21"/>
<rectangle x1="-5.7785" y1="3.4925" x2="-4.7117" y2="3.5179" layer="21"/>
<rectangle x1="-3.3909" y1="3.4925" x2="-2.2987" y2="3.5179" layer="21"/>
<rectangle x1="-0.6985" y1="3.4925" x2="0.4445" y2="3.5179" layer="21"/>
<rectangle x1="2.0447" y1="3.4925" x2="3.1115" y2="3.5179" layer="21"/>
<rectangle x1="4.4577" y1="3.4925" x2="5.5245" y2="3.5179" layer="21"/>
<rectangle x1="-5.7785" y1="3.5179" x2="-4.7117" y2="3.5433" layer="21"/>
<rectangle x1="-3.3909" y1="3.5179" x2="-2.2987" y2="3.5433" layer="21"/>
<rectangle x1="-0.6985" y1="3.5179" x2="0.4445" y2="3.5433" layer="21"/>
<rectangle x1="2.0447" y1="3.5179" x2="3.1115" y2="3.5433" layer="21"/>
<rectangle x1="4.4577" y1="3.5179" x2="5.5245" y2="3.5433" layer="21"/>
<rectangle x1="-5.7785" y1="3.5433" x2="-4.7117" y2="3.5687" layer="21"/>
<rectangle x1="-3.3909" y1="3.5433" x2="-2.2987" y2="3.5687" layer="21"/>
<rectangle x1="-0.6985" y1="3.5433" x2="0.4445" y2="3.5687" layer="21"/>
<rectangle x1="2.0447" y1="3.5433" x2="3.1115" y2="3.5687" layer="21"/>
<rectangle x1="4.4577" y1="3.5433" x2="5.5245" y2="3.5687" layer="21"/>
<rectangle x1="-5.7785" y1="3.5687" x2="-4.7117" y2="3.5941" layer="21"/>
<rectangle x1="-3.3909" y1="3.5687" x2="-2.2987" y2="3.5941" layer="21"/>
<rectangle x1="-0.6985" y1="3.5687" x2="0.4445" y2="3.5941" layer="21"/>
<rectangle x1="2.0447" y1="3.5687" x2="3.1115" y2="3.5941" layer="21"/>
<rectangle x1="4.4577" y1="3.5687" x2="5.5245" y2="3.5941" layer="21"/>
<rectangle x1="-5.7785" y1="3.5941" x2="-4.7117" y2="3.6195" layer="21"/>
<rectangle x1="-3.3909" y1="3.5941" x2="-2.2987" y2="3.6195" layer="21"/>
<rectangle x1="-0.6985" y1="3.5941" x2="0.4445" y2="3.6195" layer="21"/>
<rectangle x1="2.0447" y1="3.5941" x2="3.1115" y2="3.6195" layer="21"/>
<rectangle x1="4.4577" y1="3.5941" x2="5.5245" y2="3.6195" layer="21"/>
<rectangle x1="-5.7785" y1="3.6195" x2="-4.7117" y2="3.6449" layer="21"/>
<rectangle x1="-3.3909" y1="3.6195" x2="-2.2987" y2="3.6449" layer="21"/>
<rectangle x1="-0.6985" y1="3.6195" x2="0.4445" y2="3.6449" layer="21"/>
<rectangle x1="2.0447" y1="3.6195" x2="3.1115" y2="3.6449" layer="21"/>
<rectangle x1="4.4577" y1="3.6195" x2="5.5245" y2="3.6449" layer="21"/>
<rectangle x1="-5.7785" y1="3.6449" x2="-4.7117" y2="3.6703" layer="21"/>
<rectangle x1="-3.3909" y1="3.6449" x2="-2.2987" y2="3.6703" layer="21"/>
<rectangle x1="-0.6985" y1="3.6449" x2="0.4445" y2="3.6703" layer="21"/>
<rectangle x1="2.0447" y1="3.6449" x2="3.1115" y2="3.6703" layer="21"/>
<rectangle x1="4.4577" y1="3.6449" x2="5.5245" y2="3.6703" layer="21"/>
<rectangle x1="-5.7785" y1="3.6703" x2="-4.7117" y2="3.6957" layer="21"/>
<rectangle x1="-3.3909" y1="3.6703" x2="-2.2987" y2="3.6957" layer="21"/>
<rectangle x1="-0.6985" y1="3.6703" x2="0.4445" y2="3.6957" layer="21"/>
<rectangle x1="2.0447" y1="3.6703" x2="3.1115" y2="3.6957" layer="21"/>
<rectangle x1="4.4577" y1="3.6703" x2="5.5245" y2="3.6957" layer="21"/>
<rectangle x1="-5.7785" y1="3.6957" x2="-4.7117" y2="3.7211" layer="21"/>
<rectangle x1="-3.3909" y1="3.6957" x2="-2.2987" y2="3.7211" layer="21"/>
<rectangle x1="-0.6985" y1="3.6957" x2="0.4445" y2="3.7211" layer="21"/>
<rectangle x1="2.0447" y1="3.6957" x2="3.1115" y2="3.7211" layer="21"/>
<rectangle x1="4.4577" y1="3.6957" x2="5.5245" y2="3.7211" layer="21"/>
<rectangle x1="-5.7785" y1="3.7211" x2="-4.7117" y2="3.7465" layer="21"/>
<rectangle x1="-3.3909" y1="3.7211" x2="-2.2987" y2="3.7465" layer="21"/>
<rectangle x1="-0.6985" y1="3.7211" x2="0.4445" y2="3.7465" layer="21"/>
<rectangle x1="2.0447" y1="3.7211" x2="3.1115" y2="3.7465" layer="21"/>
<rectangle x1="4.4577" y1="3.7211" x2="5.5245" y2="3.7465" layer="21"/>
<rectangle x1="-5.7785" y1="3.7465" x2="-4.7117" y2="3.7719" layer="21"/>
<rectangle x1="-3.3909" y1="3.7465" x2="-2.2987" y2="3.7719" layer="21"/>
<rectangle x1="-0.6985" y1="3.7465" x2="0.4445" y2="3.7719" layer="21"/>
<rectangle x1="2.0447" y1="3.7465" x2="3.1115" y2="3.7719" layer="21"/>
<rectangle x1="4.4577" y1="3.7465" x2="5.5245" y2="3.7719" layer="21"/>
<rectangle x1="-5.7785" y1="3.7719" x2="-4.7117" y2="3.7973" layer="21"/>
<rectangle x1="-3.3909" y1="3.7719" x2="-2.2987" y2="3.7973" layer="21"/>
<rectangle x1="-0.6985" y1="3.7719" x2="0.4445" y2="3.7973" layer="21"/>
<rectangle x1="2.0447" y1="3.7719" x2="3.1115" y2="3.7973" layer="21"/>
<rectangle x1="4.4577" y1="3.7719" x2="5.5245" y2="3.7973" layer="21"/>
<rectangle x1="-5.7785" y1="3.7973" x2="-4.7117" y2="3.8227" layer="21"/>
<rectangle x1="-3.3909" y1="3.7973" x2="-2.2987" y2="3.8227" layer="21"/>
<rectangle x1="-0.6985" y1="3.7973" x2="0.4445" y2="3.8227" layer="21"/>
<rectangle x1="2.0447" y1="3.7973" x2="3.1115" y2="3.8227" layer="21"/>
<rectangle x1="4.4577" y1="3.7973" x2="5.5245" y2="3.8227" layer="21"/>
<rectangle x1="-5.7785" y1="3.8227" x2="-4.7117" y2="3.8481" layer="21"/>
<rectangle x1="-3.3909" y1="3.8227" x2="-2.2987" y2="3.8481" layer="21"/>
<rectangle x1="-0.6985" y1="3.8227" x2="0.4445" y2="3.8481" layer="21"/>
<rectangle x1="2.0447" y1="3.8227" x2="3.1115" y2="3.8481" layer="21"/>
<rectangle x1="4.4577" y1="3.8227" x2="5.5245" y2="3.8481" layer="21"/>
<rectangle x1="-5.7785" y1="3.8481" x2="-4.7117" y2="3.8735" layer="21"/>
<rectangle x1="-3.3909" y1="3.8481" x2="-2.2987" y2="3.8735" layer="21"/>
<rectangle x1="-0.6985" y1="3.8481" x2="0.4445" y2="3.8735" layer="21"/>
<rectangle x1="2.0447" y1="3.8481" x2="3.1115" y2="3.8735" layer="21"/>
<rectangle x1="4.4577" y1="3.8481" x2="5.5245" y2="3.8735" layer="21"/>
<rectangle x1="-5.7785" y1="3.8735" x2="-4.7117" y2="3.8989" layer="21"/>
<rectangle x1="-3.3909" y1="3.8735" x2="-2.2987" y2="3.8989" layer="21"/>
<rectangle x1="-0.6985" y1="3.8735" x2="0.4445" y2="3.8989" layer="21"/>
<rectangle x1="2.0447" y1="3.8735" x2="3.1115" y2="3.8989" layer="21"/>
<rectangle x1="4.4577" y1="3.8735" x2="5.5245" y2="3.8989" layer="21"/>
<rectangle x1="-5.7785" y1="3.8989" x2="-4.7117" y2="3.9243" layer="21"/>
<rectangle x1="-3.3909" y1="3.8989" x2="-2.2987" y2="3.9243" layer="21"/>
<rectangle x1="-0.6985" y1="3.8989" x2="0.4445" y2="3.9243" layer="21"/>
<rectangle x1="2.0447" y1="3.8989" x2="3.1115" y2="3.9243" layer="21"/>
<rectangle x1="4.4577" y1="3.8989" x2="5.5245" y2="3.9243" layer="21"/>
<rectangle x1="-5.7785" y1="3.9243" x2="-4.7117" y2="3.9497" layer="21"/>
<rectangle x1="-3.3655" y1="3.9243" x2="-2.2987" y2="3.9497" layer="21"/>
<rectangle x1="-0.6985" y1="3.9243" x2="0.4445" y2="3.9497" layer="21"/>
<rectangle x1="2.0447" y1="3.9243" x2="3.1115" y2="3.9497" layer="21"/>
<rectangle x1="4.4577" y1="3.9243" x2="5.5245" y2="3.9497" layer="21"/>
<rectangle x1="-5.7785" y1="3.9497" x2="-4.7117" y2="3.9751" layer="21"/>
<rectangle x1="-3.3655" y1="3.9497" x2="-2.2987" y2="3.9751" layer="21"/>
<rectangle x1="-0.6985" y1="3.9497" x2="0.4445" y2="3.9751" layer="21"/>
<rectangle x1="2.0447" y1="3.9497" x2="3.1115" y2="3.9751" layer="21"/>
<rectangle x1="4.4577" y1="3.9497" x2="5.5245" y2="3.9751" layer="21"/>
<rectangle x1="-5.7785" y1="3.9751" x2="-4.7117" y2="4.0005" layer="21"/>
<rectangle x1="-3.3655" y1="3.9751" x2="-2.2987" y2="4.0005" layer="21"/>
<rectangle x1="-0.6985" y1="3.9751" x2="0.4445" y2="4.0005" layer="21"/>
<rectangle x1="2.0447" y1="3.9751" x2="3.1115" y2="4.0005" layer="21"/>
<rectangle x1="4.4577" y1="3.9751" x2="5.5245" y2="4.0005" layer="21"/>
<rectangle x1="-5.7785" y1="4.0005" x2="-4.7117" y2="4.0259" layer="21"/>
<rectangle x1="-3.3655" y1="4.0005" x2="-2.2987" y2="4.0259" layer="21"/>
<rectangle x1="-0.6985" y1="4.0005" x2="0.4445" y2="4.0259" layer="21"/>
<rectangle x1="2.0447" y1="4.0005" x2="3.1115" y2="4.0259" layer="21"/>
<rectangle x1="4.4577" y1="4.0005" x2="5.5245" y2="4.0259" layer="21"/>
<rectangle x1="-5.7785" y1="4.0259" x2="-4.7117" y2="4.0513" layer="21"/>
<rectangle x1="-3.3655" y1="4.0259" x2="-2.2987" y2="4.0513" layer="21"/>
<rectangle x1="-0.6985" y1="4.0259" x2="0.4445" y2="4.0513" layer="21"/>
<rectangle x1="2.0447" y1="4.0259" x2="3.1115" y2="4.0513" layer="21"/>
<rectangle x1="4.4577" y1="4.0259" x2="5.5245" y2="4.0513" layer="21"/>
<rectangle x1="-5.7785" y1="4.0513" x2="-4.7117" y2="4.0767" layer="21"/>
<rectangle x1="-3.3655" y1="4.0513" x2="-2.2987" y2="4.0767" layer="21"/>
<rectangle x1="-0.6985" y1="4.0513" x2="0.4445" y2="4.0767" layer="21"/>
<rectangle x1="2.0447" y1="4.0513" x2="3.1115" y2="4.0767" layer="21"/>
<rectangle x1="4.4577" y1="4.0513" x2="5.5245" y2="4.0767" layer="21"/>
<rectangle x1="-5.7785" y1="4.0767" x2="-4.7117" y2="4.1021" layer="21"/>
<rectangle x1="-3.3655" y1="4.0767" x2="-2.2987" y2="4.1021" layer="21"/>
<rectangle x1="-0.6985" y1="4.0767" x2="0.4445" y2="4.1021" layer="21"/>
<rectangle x1="2.0447" y1="4.0767" x2="3.1115" y2="4.1021" layer="21"/>
<rectangle x1="4.4577" y1="4.0767" x2="5.5245" y2="4.1021" layer="21"/>
<rectangle x1="-5.7785" y1="4.1021" x2="-4.7117" y2="4.1275" layer="21"/>
<rectangle x1="-3.3655" y1="4.1021" x2="-2.2987" y2="4.1275" layer="21"/>
<rectangle x1="-0.6985" y1="4.1021" x2="0.4445" y2="4.1275" layer="21"/>
<rectangle x1="2.0447" y1="4.1021" x2="3.1115" y2="4.1275" layer="21"/>
<rectangle x1="4.4577" y1="4.1021" x2="5.5245" y2="4.1275" layer="21"/>
<rectangle x1="-5.7785" y1="4.1275" x2="-4.7117" y2="4.1529" layer="21"/>
<rectangle x1="-3.3655" y1="4.1275" x2="-2.2987" y2="4.1529" layer="21"/>
<rectangle x1="-0.6985" y1="4.1275" x2="0.4445" y2="4.1529" layer="21"/>
<rectangle x1="2.0447" y1="4.1275" x2="3.1115" y2="4.1529" layer="21"/>
<rectangle x1="4.4577" y1="4.1275" x2="5.5245" y2="4.1529" layer="21"/>
<rectangle x1="-5.7785" y1="4.1529" x2="-4.7117" y2="4.1783" layer="21"/>
<rectangle x1="-3.3655" y1="4.1529" x2="-2.2987" y2="4.1783" layer="21"/>
<rectangle x1="-0.6985" y1="4.1529" x2="0.4445" y2="4.1783" layer="21"/>
<rectangle x1="2.0447" y1="4.1529" x2="3.1115" y2="4.1783" layer="21"/>
<rectangle x1="4.4577" y1="4.1529" x2="5.5245" y2="4.1783" layer="21"/>
<rectangle x1="-5.7785" y1="4.1783" x2="-4.7117" y2="4.2037" layer="21"/>
<rectangle x1="-3.3655" y1="4.1783" x2="-2.2987" y2="4.2037" layer="21"/>
<rectangle x1="-0.6985" y1="4.1783" x2="0.4445" y2="4.2037" layer="21"/>
<rectangle x1="2.0447" y1="4.1783" x2="3.1115" y2="4.2037" layer="21"/>
<rectangle x1="4.4577" y1="4.1783" x2="5.5245" y2="4.2037" layer="21"/>
<rectangle x1="-5.7785" y1="4.2037" x2="-4.7117" y2="4.2291" layer="21"/>
<rectangle x1="-3.3655" y1="4.2037" x2="-2.2987" y2="4.2291" layer="21"/>
<rectangle x1="-0.6985" y1="4.2037" x2="0.4445" y2="4.2291" layer="21"/>
<rectangle x1="2.0447" y1="4.2037" x2="3.1115" y2="4.2291" layer="21"/>
<rectangle x1="4.4577" y1="4.2037" x2="5.5245" y2="4.2291" layer="21"/>
<rectangle x1="-5.7785" y1="4.2291" x2="-4.7117" y2="4.2545" layer="21"/>
<rectangle x1="-3.3655" y1="4.2291" x2="-2.2987" y2="4.2545" layer="21"/>
<rectangle x1="-0.6985" y1="4.2291" x2="0.4445" y2="4.2545" layer="21"/>
<rectangle x1="2.0447" y1="4.2291" x2="3.1115" y2="4.2545" layer="21"/>
<rectangle x1="4.4577" y1="4.2291" x2="5.5245" y2="4.2545" layer="21"/>
<rectangle x1="-5.7785" y1="4.2545" x2="-4.7117" y2="4.2799" layer="21"/>
<rectangle x1="-3.3655" y1="4.2545" x2="-2.2987" y2="4.2799" layer="21"/>
<rectangle x1="-0.6985" y1="4.2545" x2="0.4445" y2="4.2799" layer="21"/>
<rectangle x1="2.0447" y1="4.2545" x2="3.1115" y2="4.2799" layer="21"/>
<rectangle x1="4.4577" y1="4.2545" x2="5.5245" y2="4.2799" layer="21"/>
<rectangle x1="-5.7785" y1="4.2799" x2="-4.7117" y2="4.3053" layer="21"/>
<rectangle x1="-3.3655" y1="4.2799" x2="-2.2987" y2="4.3053" layer="21"/>
<rectangle x1="-0.6985" y1="4.2799" x2="0.4445" y2="4.3053" layer="21"/>
<rectangle x1="2.0447" y1="4.2799" x2="3.1115" y2="4.3053" layer="21"/>
<rectangle x1="4.4577" y1="4.2799" x2="5.5245" y2="4.3053" layer="21"/>
<rectangle x1="-5.7785" y1="4.3053" x2="-4.7117" y2="4.3307" layer="21"/>
<rectangle x1="-3.3655" y1="4.3053" x2="-2.2987" y2="4.3307" layer="21"/>
<rectangle x1="-0.6985" y1="4.3053" x2="0.4445" y2="4.3307" layer="21"/>
<rectangle x1="2.0447" y1="4.3053" x2="3.1115" y2="4.3307" layer="21"/>
<rectangle x1="4.4577" y1="4.3053" x2="5.5245" y2="4.3307" layer="21"/>
<rectangle x1="-5.7785" y1="4.3307" x2="-4.7117" y2="4.3561" layer="21"/>
<rectangle x1="-3.3655" y1="4.3307" x2="-2.2987" y2="4.3561" layer="21"/>
<rectangle x1="-0.6985" y1="4.3307" x2="0.4445" y2="4.3561" layer="21"/>
<rectangle x1="2.0447" y1="4.3307" x2="3.1115" y2="4.3561" layer="21"/>
<rectangle x1="4.4577" y1="4.3307" x2="5.5245" y2="4.3561" layer="21"/>
<rectangle x1="-5.7785" y1="4.3561" x2="-4.7117" y2="4.3815" layer="21"/>
<rectangle x1="-3.3655" y1="4.3561" x2="-2.2987" y2="4.3815" layer="21"/>
<rectangle x1="-0.6985" y1="4.3561" x2="0.4445" y2="4.3815" layer="21"/>
<rectangle x1="2.0447" y1="4.3561" x2="3.1115" y2="4.3815" layer="21"/>
<rectangle x1="4.4577" y1="4.3561" x2="5.5245" y2="4.3815" layer="21"/>
<rectangle x1="-5.7785" y1="4.3815" x2="-4.7117" y2="4.4069" layer="21"/>
<rectangle x1="-3.3655" y1="4.3815" x2="-2.2987" y2="4.4069" layer="21"/>
<rectangle x1="-0.6985" y1="4.3815" x2="0.4445" y2="4.4069" layer="21"/>
<rectangle x1="2.0447" y1="4.3815" x2="3.1115" y2="4.4069" layer="21"/>
<rectangle x1="4.4577" y1="4.3815" x2="5.5245" y2="4.4069" layer="21"/>
<rectangle x1="-5.7785" y1="4.4069" x2="-4.7117" y2="4.4323" layer="21"/>
<rectangle x1="-3.3655" y1="4.4069" x2="-2.2987" y2="4.4323" layer="21"/>
<rectangle x1="-0.6985" y1="4.4069" x2="0.4445" y2="4.4323" layer="21"/>
<rectangle x1="2.0447" y1="4.4069" x2="3.1115" y2="4.4323" layer="21"/>
<rectangle x1="4.4577" y1="4.4069" x2="5.5245" y2="4.4323" layer="21"/>
<rectangle x1="-5.7785" y1="4.4323" x2="-4.7117" y2="4.4577" layer="21"/>
<rectangle x1="-3.3655" y1="4.4323" x2="-2.2987" y2="4.4577" layer="21"/>
<rectangle x1="-0.6985" y1="4.4323" x2="0.4445" y2="4.4577" layer="21"/>
<rectangle x1="2.0447" y1="4.4323" x2="3.1115" y2="4.4577" layer="21"/>
<rectangle x1="4.4577" y1="4.4323" x2="5.5245" y2="4.4577" layer="21"/>
<rectangle x1="-5.7785" y1="4.4577" x2="-4.7117" y2="4.4831" layer="21"/>
<rectangle x1="-3.3655" y1="4.4577" x2="-2.2987" y2="4.4831" layer="21"/>
<rectangle x1="-0.6985" y1="4.4577" x2="0.4445" y2="4.4831" layer="21"/>
<rectangle x1="2.0447" y1="4.4577" x2="3.1115" y2="4.4831" layer="21"/>
<rectangle x1="4.4577" y1="4.4577" x2="5.5245" y2="4.4831" layer="21"/>
<rectangle x1="-5.7785" y1="4.4831" x2="-4.7117" y2="4.5085" layer="21"/>
<rectangle x1="-3.3655" y1="4.4831" x2="-2.2987" y2="4.5085" layer="21"/>
<rectangle x1="-0.6985" y1="4.4831" x2="0.4445" y2="4.5085" layer="21"/>
<rectangle x1="2.0447" y1="4.4831" x2="3.1115" y2="4.5085" layer="21"/>
<rectangle x1="4.4577" y1="4.4831" x2="5.5245" y2="4.5085" layer="21"/>
<rectangle x1="-5.7785" y1="4.5085" x2="-4.7117" y2="4.5339" layer="21"/>
<rectangle x1="-3.3655" y1="4.5085" x2="-2.2987" y2="4.5339" layer="21"/>
<rectangle x1="-0.6985" y1="4.5085" x2="0.4445" y2="4.5339" layer="21"/>
<rectangle x1="2.0447" y1="4.5085" x2="3.1115" y2="4.5339" layer="21"/>
<rectangle x1="4.4577" y1="4.5085" x2="5.5245" y2="4.5339" layer="21"/>
<rectangle x1="-5.7785" y1="4.5339" x2="-4.7117" y2="4.5593" layer="21"/>
<rectangle x1="-3.3655" y1="4.5339" x2="-2.2987" y2="4.5593" layer="21"/>
<rectangle x1="-0.6985" y1="4.5339" x2="0.4445" y2="4.5593" layer="21"/>
<rectangle x1="2.0447" y1="4.5339" x2="3.1115" y2="4.5593" layer="21"/>
<rectangle x1="4.4577" y1="4.5339" x2="5.5245" y2="4.5593" layer="21"/>
<rectangle x1="-5.7785" y1="4.5593" x2="-4.7117" y2="4.5847" layer="21"/>
<rectangle x1="-3.3655" y1="4.5593" x2="-2.2987" y2="4.5847" layer="21"/>
<rectangle x1="-0.6985" y1="4.5593" x2="0.4445" y2="4.5847" layer="21"/>
<rectangle x1="2.0447" y1="4.5593" x2="3.1115" y2="4.5847" layer="21"/>
<rectangle x1="4.4577" y1="4.5593" x2="5.5245" y2="4.5847" layer="21"/>
<rectangle x1="-5.7785" y1="4.5847" x2="-4.7117" y2="4.6101" layer="21"/>
<rectangle x1="-3.3655" y1="4.5847" x2="-2.2987" y2="4.6101" layer="21"/>
<rectangle x1="-0.6985" y1="4.5847" x2="0.4445" y2="4.6101" layer="21"/>
<rectangle x1="2.0447" y1="4.5847" x2="3.1115" y2="4.6101" layer="21"/>
<rectangle x1="4.4577" y1="4.5847" x2="5.5245" y2="4.6101" layer="21"/>
<rectangle x1="-5.7785" y1="4.6101" x2="-4.7117" y2="4.6355" layer="21"/>
<rectangle x1="-3.3655" y1="4.6101" x2="-2.2987" y2="4.6355" layer="21"/>
<rectangle x1="-0.6985" y1="4.6101" x2="0.4445" y2="4.6355" layer="21"/>
<rectangle x1="2.0447" y1="4.6101" x2="3.1115" y2="4.6355" layer="21"/>
<rectangle x1="4.4577" y1="4.6101" x2="5.5245" y2="4.6355" layer="21"/>
<rectangle x1="-5.7785" y1="4.6355" x2="-4.7117" y2="4.6609" layer="21"/>
<rectangle x1="-3.3655" y1="4.6355" x2="-2.2987" y2="4.6609" layer="21"/>
<rectangle x1="-0.6985" y1="4.6355" x2="0.4445" y2="4.6609" layer="21"/>
<rectangle x1="2.0447" y1="4.6355" x2="3.1115" y2="4.6609" layer="21"/>
<rectangle x1="4.4577" y1="4.6355" x2="5.5245" y2="4.6609" layer="21"/>
<rectangle x1="-5.7785" y1="4.6609" x2="-4.7117" y2="4.6863" layer="21"/>
<rectangle x1="-3.3655" y1="4.6609" x2="-2.2987" y2="4.6863" layer="21"/>
<rectangle x1="-0.6985" y1="4.6609" x2="0.4445" y2="4.6863" layer="21"/>
<rectangle x1="2.0447" y1="4.6609" x2="3.1115" y2="4.6863" layer="21"/>
<rectangle x1="4.4577" y1="4.6609" x2="5.5245" y2="4.6863" layer="21"/>
<rectangle x1="-5.7785" y1="4.6863" x2="-4.7117" y2="4.7117" layer="21"/>
<rectangle x1="-3.3655" y1="4.6863" x2="-2.2987" y2="4.7117" layer="21"/>
<rectangle x1="-0.6985" y1="4.6863" x2="0.4445" y2="4.7117" layer="21"/>
<rectangle x1="2.0447" y1="4.6863" x2="3.1115" y2="4.7117" layer="21"/>
<rectangle x1="4.4577" y1="4.6863" x2="5.5245" y2="4.7117" layer="21"/>
<rectangle x1="-5.7785" y1="4.7117" x2="-4.7117" y2="4.7371" layer="21"/>
<rectangle x1="-3.3655" y1="4.7117" x2="-2.2987" y2="4.7371" layer="21"/>
<rectangle x1="-0.6985" y1="4.7117" x2="0.4445" y2="4.7371" layer="21"/>
<rectangle x1="2.0447" y1="4.7117" x2="3.1115" y2="4.7371" layer="21"/>
<rectangle x1="4.4577" y1="4.7117" x2="5.5245" y2="4.7371" layer="21"/>
<rectangle x1="-5.7785" y1="4.7371" x2="-4.7117" y2="4.7625" layer="21"/>
<rectangle x1="-3.3655" y1="4.7371" x2="-2.2987" y2="4.7625" layer="21"/>
<rectangle x1="-0.6985" y1="4.7371" x2="0.4445" y2="4.7625" layer="21"/>
<rectangle x1="2.0447" y1="4.7371" x2="3.1115" y2="4.7625" layer="21"/>
<rectangle x1="4.4577" y1="4.7371" x2="5.5245" y2="4.7625" layer="21"/>
<rectangle x1="-5.7785" y1="4.7625" x2="-4.7117" y2="4.7879" layer="21"/>
<rectangle x1="-3.3655" y1="4.7625" x2="-2.2987" y2="4.7879" layer="21"/>
<rectangle x1="-0.6985" y1="4.7625" x2="0.4445" y2="4.7879" layer="21"/>
<rectangle x1="2.0447" y1="4.7625" x2="3.1115" y2="4.7879" layer="21"/>
<rectangle x1="4.4577" y1="4.7625" x2="5.5245" y2="4.7879" layer="21"/>
<rectangle x1="-5.7785" y1="4.7879" x2="-4.7117" y2="4.8133" layer="21"/>
<rectangle x1="-3.3655" y1="4.7879" x2="-2.2987" y2="4.8133" layer="21"/>
<rectangle x1="-0.6985" y1="4.7879" x2="0.4445" y2="4.8133" layer="21"/>
<rectangle x1="2.0447" y1="4.7879" x2="3.1115" y2="4.8133" layer="21"/>
<rectangle x1="4.4577" y1="4.7879" x2="5.5245" y2="4.8133" layer="21"/>
<rectangle x1="-5.7785" y1="4.8133" x2="-4.7117" y2="4.8387" layer="21"/>
<rectangle x1="-3.3655" y1="4.8133" x2="-2.2987" y2="4.8387" layer="21"/>
<rectangle x1="-0.6985" y1="4.8133" x2="0.4445" y2="4.8387" layer="21"/>
<rectangle x1="2.0447" y1="4.8133" x2="3.1115" y2="4.8387" layer="21"/>
<rectangle x1="4.4577" y1="4.8133" x2="5.5245" y2="4.8387" layer="21"/>
<rectangle x1="-5.7785" y1="4.8387" x2="-4.7117" y2="4.8641" layer="21"/>
<rectangle x1="-3.3655" y1="4.8387" x2="-2.2987" y2="4.8641" layer="21"/>
<rectangle x1="-0.6985" y1="4.8387" x2="0.4445" y2="4.8641" layer="21"/>
<rectangle x1="2.0447" y1="4.8387" x2="3.1115" y2="4.8641" layer="21"/>
<rectangle x1="4.4577" y1="4.8387" x2="5.5245" y2="4.8641" layer="21"/>
<rectangle x1="-5.7785" y1="4.8641" x2="-4.7117" y2="4.8895" layer="21"/>
<rectangle x1="-3.3655" y1="4.8641" x2="-2.2987" y2="4.8895" layer="21"/>
<rectangle x1="-0.6985" y1="4.8641" x2="0.4445" y2="4.8895" layer="21"/>
<rectangle x1="2.0447" y1="4.8641" x2="3.1115" y2="4.8895" layer="21"/>
<rectangle x1="4.4577" y1="4.8641" x2="5.5245" y2="4.8895" layer="21"/>
<rectangle x1="-5.7785" y1="4.8895" x2="-4.7117" y2="4.9149" layer="21"/>
<rectangle x1="-3.3655" y1="4.8895" x2="-2.2987" y2="4.9149" layer="21"/>
<rectangle x1="-0.6985" y1="4.8895" x2="0.4445" y2="4.9149" layer="21"/>
<rectangle x1="2.0447" y1="4.8895" x2="3.1115" y2="4.9149" layer="21"/>
<rectangle x1="4.4577" y1="4.8895" x2="5.5245" y2="4.9149" layer="21"/>
<rectangle x1="-5.7785" y1="4.9149" x2="-4.7117" y2="4.9403" layer="21"/>
<rectangle x1="-3.3655" y1="4.9149" x2="-2.2987" y2="4.9403" layer="21"/>
<rectangle x1="-0.6985" y1="4.9149" x2="0.4445" y2="4.9403" layer="21"/>
<rectangle x1="2.0447" y1="4.9149" x2="3.1115" y2="4.9403" layer="21"/>
<rectangle x1="4.4577" y1="4.9149" x2="5.5245" y2="4.9403" layer="21"/>
<rectangle x1="-5.7785" y1="4.9403" x2="-4.7117" y2="4.9657" layer="21"/>
<rectangle x1="-3.3655" y1="4.9403" x2="-2.2987" y2="4.9657" layer="21"/>
<rectangle x1="-0.6985" y1="4.9403" x2="0.4445" y2="4.9657" layer="21"/>
<rectangle x1="2.0447" y1="4.9403" x2="3.1115" y2="4.9657" layer="21"/>
<rectangle x1="4.4577" y1="4.9403" x2="5.5245" y2="4.9657" layer="21"/>
<rectangle x1="-5.7785" y1="4.9657" x2="-4.7117" y2="4.9911" layer="21"/>
<rectangle x1="-3.3655" y1="4.9657" x2="-2.2987" y2="4.9911" layer="21"/>
<rectangle x1="-0.6985" y1="4.9657" x2="0.4445" y2="4.9911" layer="21"/>
<rectangle x1="2.0447" y1="4.9657" x2="3.1115" y2="4.9911" layer="21"/>
<rectangle x1="4.4577" y1="4.9657" x2="5.5245" y2="4.9911" layer="21"/>
<rectangle x1="-5.7785" y1="4.9911" x2="-4.7117" y2="5.0165" layer="21"/>
<rectangle x1="-3.3655" y1="4.9911" x2="-2.2987" y2="5.0165" layer="21"/>
<rectangle x1="-0.6985" y1="4.9911" x2="0.4445" y2="5.0165" layer="21"/>
<rectangle x1="2.0447" y1="4.9911" x2="3.1115" y2="5.0165" layer="21"/>
<rectangle x1="4.4577" y1="4.9911" x2="5.5245" y2="5.0165" layer="21"/>
<rectangle x1="-5.7785" y1="5.0165" x2="-4.7117" y2="5.0419" layer="21"/>
<rectangle x1="-3.3655" y1="5.0165" x2="-2.2987" y2="5.0419" layer="21"/>
<rectangle x1="-0.6985" y1="5.0165" x2="0.4445" y2="5.0419" layer="21"/>
<rectangle x1="2.0447" y1="5.0165" x2="3.1115" y2="5.0419" layer="21"/>
<rectangle x1="4.4577" y1="5.0165" x2="5.5245" y2="5.0419" layer="21"/>
<rectangle x1="-5.7785" y1="5.0419" x2="-4.7117" y2="5.0673" layer="21"/>
<rectangle x1="-3.3655" y1="5.0419" x2="-2.2987" y2="5.0673" layer="21"/>
<rectangle x1="-0.6985" y1="5.0419" x2="0.4445" y2="5.0673" layer="21"/>
<rectangle x1="2.0447" y1="5.0419" x2="3.1115" y2="5.0673" layer="21"/>
<rectangle x1="4.4577" y1="5.0419" x2="5.5245" y2="5.0673" layer="21"/>
<rectangle x1="-5.7785" y1="5.0673" x2="-4.7117" y2="5.0927" layer="21"/>
<rectangle x1="-3.3655" y1="5.0673" x2="-2.2987" y2="5.0927" layer="21"/>
<rectangle x1="-0.6985" y1="5.0673" x2="0.4445" y2="5.0927" layer="21"/>
<rectangle x1="2.0447" y1="5.0673" x2="3.1115" y2="5.0927" layer="21"/>
<rectangle x1="4.4577" y1="5.0673" x2="5.5245" y2="5.0927" layer="21"/>
<rectangle x1="-5.7785" y1="5.0927" x2="-4.7117" y2="5.1181" layer="21"/>
<rectangle x1="-3.3655" y1="5.0927" x2="-2.2987" y2="5.1181" layer="21"/>
<rectangle x1="-0.6985" y1="5.0927" x2="0.4445" y2="5.1181" layer="21"/>
<rectangle x1="2.0447" y1="5.0927" x2="3.1115" y2="5.1181" layer="21"/>
<rectangle x1="4.4577" y1="5.0927" x2="5.5245" y2="5.1181" layer="21"/>
<rectangle x1="-5.7785" y1="5.1181" x2="-4.7117" y2="5.1435" layer="21"/>
<rectangle x1="-3.3655" y1="5.1181" x2="-2.2987" y2="5.1435" layer="21"/>
<rectangle x1="-0.6985" y1="5.1181" x2="0.4445" y2="5.1435" layer="21"/>
<rectangle x1="2.0447" y1="5.1181" x2="3.1115" y2="5.1435" layer="21"/>
<rectangle x1="4.4577" y1="5.1181" x2="5.5245" y2="5.1435" layer="21"/>
<rectangle x1="-5.7785" y1="5.1435" x2="-4.7117" y2="5.1689" layer="21"/>
<rectangle x1="-3.3655" y1="5.1435" x2="-2.2987" y2="5.1689" layer="21"/>
<rectangle x1="-0.6985" y1="5.1435" x2="0.4445" y2="5.1689" layer="21"/>
<rectangle x1="2.0447" y1="5.1435" x2="3.1115" y2="5.1689" layer="21"/>
<rectangle x1="4.4577" y1="5.1435" x2="5.5245" y2="5.1689" layer="21"/>
<rectangle x1="-5.7785" y1="5.1689" x2="-4.7117" y2="5.1943" layer="21"/>
<rectangle x1="-3.3655" y1="5.1689" x2="-2.2987" y2="5.1943" layer="21"/>
<rectangle x1="-0.6985" y1="5.1689" x2="0.4445" y2="5.1943" layer="21"/>
<rectangle x1="2.0447" y1="5.1689" x2="3.1115" y2="5.1943" layer="21"/>
<rectangle x1="4.4577" y1="5.1689" x2="5.5245" y2="5.1943" layer="21"/>
<rectangle x1="-5.7785" y1="5.1943" x2="-4.7117" y2="5.2197" layer="21"/>
<rectangle x1="-3.3655" y1="5.1943" x2="-2.2987" y2="5.2197" layer="21"/>
<rectangle x1="-0.6985" y1="5.1943" x2="0.4445" y2="5.2197" layer="21"/>
<rectangle x1="2.0447" y1="5.1943" x2="3.1115" y2="5.2197" layer="21"/>
<rectangle x1="4.4577" y1="5.1943" x2="5.5245" y2="5.2197" layer="21"/>
<rectangle x1="-5.7785" y1="5.2197" x2="-4.7117" y2="5.2451" layer="21"/>
<rectangle x1="-3.3655" y1="5.2197" x2="-2.2987" y2="5.2451" layer="21"/>
<rectangle x1="-0.6985" y1="5.2197" x2="0.4445" y2="5.2451" layer="21"/>
<rectangle x1="2.0447" y1="5.2197" x2="3.1115" y2="5.2451" layer="21"/>
<rectangle x1="4.4577" y1="5.2197" x2="5.5245" y2="5.2451" layer="21"/>
<rectangle x1="-5.7785" y1="5.2451" x2="-4.7117" y2="5.2705" layer="21"/>
<rectangle x1="-3.3655" y1="5.2451" x2="-2.2987" y2="5.2705" layer="21"/>
<rectangle x1="-0.6985" y1="5.2451" x2="0.4445" y2="5.2705" layer="21"/>
<rectangle x1="2.0447" y1="5.2451" x2="3.1115" y2="5.2705" layer="21"/>
<rectangle x1="4.4577" y1="5.2451" x2="5.5245" y2="5.2705" layer="21"/>
<rectangle x1="-5.7785" y1="5.2705" x2="-4.7117" y2="5.2959" layer="21"/>
<rectangle x1="-3.3655" y1="5.2705" x2="-2.2987" y2="5.2959" layer="21"/>
<rectangle x1="-0.6985" y1="5.2705" x2="0.4445" y2="5.2959" layer="21"/>
<rectangle x1="2.0447" y1="5.2705" x2="3.1115" y2="5.2959" layer="21"/>
<rectangle x1="4.4577" y1="5.2705" x2="5.5245" y2="5.2959" layer="21"/>
<rectangle x1="-5.7785" y1="5.2959" x2="-4.7117" y2="5.3213" layer="21"/>
<rectangle x1="-3.3655" y1="5.2959" x2="-2.2987" y2="5.3213" layer="21"/>
<rectangle x1="-0.6985" y1="5.2959" x2="0.4445" y2="5.3213" layer="21"/>
<rectangle x1="2.0447" y1="5.2959" x2="3.1115" y2="5.3213" layer="21"/>
<rectangle x1="4.4577" y1="5.2959" x2="5.5245" y2="5.3213" layer="21"/>
<rectangle x1="-5.7785" y1="5.3213" x2="-4.7117" y2="5.3467" layer="21"/>
<rectangle x1="-3.3655" y1="5.3213" x2="-2.2987" y2="5.3467" layer="21"/>
<rectangle x1="-0.6985" y1="5.3213" x2="0.4445" y2="5.3467" layer="21"/>
<rectangle x1="2.0447" y1="5.3213" x2="3.1115" y2="5.3467" layer="21"/>
<rectangle x1="4.4577" y1="5.3213" x2="5.5245" y2="5.3467" layer="21"/>
<rectangle x1="-5.7785" y1="5.3467" x2="-4.7117" y2="5.3721" layer="21"/>
<rectangle x1="-3.3655" y1="5.3467" x2="-2.2987" y2="5.3721" layer="21"/>
<rectangle x1="-0.6985" y1="5.3467" x2="0.4445" y2="5.3721" layer="21"/>
<rectangle x1="2.0447" y1="5.3467" x2="3.1115" y2="5.3721" layer="21"/>
<rectangle x1="4.4577" y1="5.3467" x2="5.5245" y2="5.3721" layer="21"/>
<rectangle x1="-5.7785" y1="5.3721" x2="-4.7117" y2="5.3975" layer="21"/>
<rectangle x1="-3.3655" y1="5.3721" x2="-2.2987" y2="5.3975" layer="21"/>
<rectangle x1="-0.6985" y1="5.3721" x2="0.4445" y2="5.3975" layer="21"/>
<rectangle x1="2.0447" y1="5.3721" x2="3.1115" y2="5.3975" layer="21"/>
<rectangle x1="4.4577" y1="5.3721" x2="5.5245" y2="5.3975" layer="21"/>
<rectangle x1="-5.7785" y1="5.3975" x2="-4.7117" y2="5.4229" layer="21"/>
<rectangle x1="-3.3655" y1="5.3975" x2="-2.2987" y2="5.4229" layer="21"/>
<rectangle x1="-0.6985" y1="5.3975" x2="0.4445" y2="5.4229" layer="21"/>
<rectangle x1="2.0447" y1="5.3975" x2="3.1115" y2="5.4229" layer="21"/>
<rectangle x1="4.4577" y1="5.3975" x2="5.5245" y2="5.4229" layer="21"/>
<rectangle x1="-5.7785" y1="5.4229" x2="-4.7117" y2="5.4483" layer="21"/>
<rectangle x1="-3.3655" y1="5.4229" x2="-2.2987" y2="5.4483" layer="21"/>
<rectangle x1="-0.6985" y1="5.4229" x2="0.4445" y2="5.4483" layer="21"/>
<rectangle x1="2.0447" y1="5.4229" x2="3.1115" y2="5.4483" layer="21"/>
<rectangle x1="4.4577" y1="5.4229" x2="5.5245" y2="5.4483" layer="21"/>
<rectangle x1="-5.7785" y1="5.4483" x2="-4.7117" y2="5.4737" layer="21"/>
<rectangle x1="-3.3655" y1="5.4483" x2="-2.2987" y2="5.4737" layer="21"/>
<rectangle x1="-0.6985" y1="5.4483" x2="0.4445" y2="5.4737" layer="21"/>
<rectangle x1="2.0447" y1="5.4483" x2="3.1115" y2="5.4737" layer="21"/>
<rectangle x1="4.4577" y1="5.4483" x2="5.5245" y2="5.4737" layer="21"/>
<rectangle x1="-5.7785" y1="5.4737" x2="-4.7117" y2="5.4991" layer="21"/>
<rectangle x1="-3.3655" y1="5.4737" x2="-2.2987" y2="5.4991" layer="21"/>
<rectangle x1="-0.6985" y1="5.4737" x2="0.4445" y2="5.4991" layer="21"/>
<rectangle x1="2.0447" y1="5.4737" x2="3.1115" y2="5.4991" layer="21"/>
<rectangle x1="4.4577" y1="5.4737" x2="5.5245" y2="5.4991" layer="21"/>
<rectangle x1="-5.7785" y1="5.4991" x2="-4.7117" y2="5.5245" layer="21"/>
<rectangle x1="-3.3655" y1="5.4991" x2="-2.2987" y2="5.5245" layer="21"/>
<rectangle x1="-0.6985" y1="5.4991" x2="0.4445" y2="5.5245" layer="21"/>
<rectangle x1="2.0447" y1="5.4991" x2="3.1115" y2="5.5245" layer="21"/>
<rectangle x1="4.4577" y1="5.4991" x2="5.5245" y2="5.5245" layer="21"/>
<rectangle x1="-5.7785" y1="5.5245" x2="-4.7117" y2="5.5499" layer="21"/>
<rectangle x1="-3.3655" y1="5.5245" x2="-2.2987" y2="5.5499" layer="21"/>
<rectangle x1="-0.6985" y1="5.5245" x2="0.4445" y2="5.5499" layer="21"/>
<rectangle x1="2.0447" y1="5.5245" x2="3.1115" y2="5.5499" layer="21"/>
<rectangle x1="4.4577" y1="5.5245" x2="5.5245" y2="5.5499" layer="21"/>
<rectangle x1="-5.7785" y1="5.5499" x2="-4.7117" y2="5.5753" layer="21"/>
<rectangle x1="-3.3909" y1="5.5499" x2="-2.2987" y2="5.5753" layer="21"/>
<rectangle x1="-0.6985" y1="5.5499" x2="0.4445" y2="5.5753" layer="21"/>
<rectangle x1="2.0447" y1="5.5499" x2="3.1115" y2="5.5753" layer="21"/>
<rectangle x1="4.4577" y1="5.5499" x2="5.5245" y2="5.5753" layer="21"/>
<rectangle x1="-5.7785" y1="5.5753" x2="-4.7117" y2="5.6007" layer="21"/>
<rectangle x1="-3.3909" y1="5.5753" x2="-2.2987" y2="5.6007" layer="21"/>
<rectangle x1="-0.6985" y1="5.5753" x2="0.4445" y2="5.6007" layer="21"/>
<rectangle x1="2.0447" y1="5.5753" x2="3.1115" y2="5.6007" layer="21"/>
<rectangle x1="4.4577" y1="5.5753" x2="5.5245" y2="5.6007" layer="21"/>
<rectangle x1="-5.7785" y1="5.6007" x2="-4.7117" y2="5.6261" layer="21"/>
<rectangle x1="-3.3909" y1="5.6007" x2="-2.2987" y2="5.6261" layer="21"/>
<rectangle x1="-0.6985" y1="5.6007" x2="0.4445" y2="5.6261" layer="21"/>
<rectangle x1="2.0447" y1="5.6007" x2="3.1115" y2="5.6261" layer="21"/>
<rectangle x1="4.4577" y1="5.6007" x2="5.5245" y2="5.6261" layer="21"/>
<rectangle x1="-5.7785" y1="5.6261" x2="-4.7117" y2="5.6515" layer="21"/>
<rectangle x1="-3.3909" y1="5.6261" x2="-2.2987" y2="5.6515" layer="21"/>
<rectangle x1="-0.6985" y1="5.6261" x2="0.4445" y2="5.6515" layer="21"/>
<rectangle x1="2.0447" y1="5.6261" x2="3.1115" y2="5.6515" layer="21"/>
<rectangle x1="4.4577" y1="5.6261" x2="5.5245" y2="5.6515" layer="21"/>
<rectangle x1="-5.7785" y1="5.6515" x2="-4.7117" y2="5.6769" layer="21"/>
<rectangle x1="-3.3909" y1="5.6515" x2="-2.2987" y2="5.6769" layer="21"/>
<rectangle x1="-0.6985" y1="5.6515" x2="0.4445" y2="5.6769" layer="21"/>
<rectangle x1="2.0447" y1="5.6515" x2="3.1115" y2="5.6769" layer="21"/>
<rectangle x1="4.4577" y1="5.6515" x2="5.5245" y2="5.6769" layer="21"/>
<rectangle x1="-5.7785" y1="5.6769" x2="-4.7117" y2="5.7023" layer="21"/>
<rectangle x1="-3.3909" y1="5.6769" x2="-2.2987" y2="5.7023" layer="21"/>
<rectangle x1="-0.6985" y1="5.6769" x2="0.4445" y2="5.7023" layer="21"/>
<rectangle x1="2.0447" y1="5.6769" x2="3.1115" y2="5.7023" layer="21"/>
<rectangle x1="4.4577" y1="5.6769" x2="5.5245" y2="5.7023" layer="21"/>
<rectangle x1="-5.7785" y1="5.7023" x2="-4.7117" y2="5.7277" layer="21"/>
<rectangle x1="-3.3909" y1="5.7023" x2="-2.2987" y2="5.7277" layer="21"/>
<rectangle x1="-0.6985" y1="5.7023" x2="0.4445" y2="5.7277" layer="21"/>
<rectangle x1="2.0447" y1="5.7023" x2="3.1115" y2="5.7277" layer="21"/>
<rectangle x1="4.4577" y1="5.7023" x2="5.5245" y2="5.7277" layer="21"/>
<rectangle x1="-5.7785" y1="5.7277" x2="-4.7117" y2="5.7531" layer="21"/>
<rectangle x1="-3.3909" y1="5.7277" x2="-2.2987" y2="5.7531" layer="21"/>
<rectangle x1="-0.6985" y1="5.7277" x2="0.4445" y2="5.7531" layer="21"/>
<rectangle x1="2.0447" y1="5.7277" x2="3.1115" y2="5.7531" layer="21"/>
<rectangle x1="4.4577" y1="5.7277" x2="5.5245" y2="5.7531" layer="21"/>
<rectangle x1="-5.7785" y1="5.7531" x2="-4.7117" y2="5.7785" layer="21"/>
<rectangle x1="-3.3909" y1="5.7531" x2="-2.2987" y2="5.7785" layer="21"/>
<rectangle x1="-0.6985" y1="5.7531" x2="0.4445" y2="5.7785" layer="21"/>
<rectangle x1="2.0447" y1="5.7531" x2="3.1115" y2="5.7785" layer="21"/>
<rectangle x1="4.4577" y1="5.7531" x2="5.5245" y2="5.7785" layer="21"/>
<rectangle x1="-5.7785" y1="5.7785" x2="-4.7117" y2="5.8039" layer="21"/>
<rectangle x1="-3.3909" y1="5.7785" x2="-2.2987" y2="5.8039" layer="21"/>
<rectangle x1="-0.6985" y1="5.7785" x2="0.4445" y2="5.8039" layer="21"/>
<rectangle x1="2.0447" y1="5.7785" x2="3.1115" y2="5.8039" layer="21"/>
<rectangle x1="4.4577" y1="5.7785" x2="5.5245" y2="5.8039" layer="21"/>
<rectangle x1="-5.7785" y1="5.8039" x2="-4.7117" y2="5.8293" layer="21"/>
<rectangle x1="-3.3909" y1="5.8039" x2="-2.2987" y2="5.8293" layer="21"/>
<rectangle x1="-0.6985" y1="5.8039" x2="0.4445" y2="5.8293" layer="21"/>
<rectangle x1="2.0447" y1="5.8039" x2="3.1115" y2="5.8293" layer="21"/>
<rectangle x1="4.4577" y1="5.8039" x2="5.5245" y2="5.8293" layer="21"/>
<rectangle x1="-5.7785" y1="5.8293" x2="-4.7117" y2="5.8547" layer="21"/>
<rectangle x1="-3.3909" y1="5.8293" x2="-2.2987" y2="5.8547" layer="21"/>
<rectangle x1="-0.6985" y1="5.8293" x2="0.4445" y2="5.8547" layer="21"/>
<rectangle x1="2.0447" y1="5.8293" x2="3.1115" y2="5.8547" layer="21"/>
<rectangle x1="4.4577" y1="5.8293" x2="5.5245" y2="5.8547" layer="21"/>
<rectangle x1="-5.7785" y1="5.8547" x2="-4.7117" y2="5.8801" layer="21"/>
<rectangle x1="-3.3909" y1="5.8547" x2="-2.2987" y2="5.8801" layer="21"/>
<rectangle x1="-0.6985" y1="5.8547" x2="0.4445" y2="5.8801" layer="21"/>
<rectangle x1="2.0447" y1="5.8547" x2="3.1115" y2="5.8801" layer="21"/>
<rectangle x1="4.4577" y1="5.8547" x2="5.5245" y2="5.8801" layer="21"/>
<rectangle x1="-5.7785" y1="5.8801" x2="-4.7117" y2="5.9055" layer="21"/>
<rectangle x1="-3.3909" y1="5.8801" x2="-2.2987" y2="5.9055" layer="21"/>
<rectangle x1="-0.6985" y1="5.8801" x2="0.4445" y2="5.9055" layer="21"/>
<rectangle x1="2.0447" y1="5.8801" x2="3.1115" y2="5.9055" layer="21"/>
<rectangle x1="4.4577" y1="5.8801" x2="5.5245" y2="5.9055" layer="21"/>
<rectangle x1="-5.7785" y1="5.9055" x2="-4.7117" y2="5.9309" layer="21"/>
<rectangle x1="-3.3909" y1="5.9055" x2="-2.2987" y2="5.9309" layer="21"/>
<rectangle x1="-0.6985" y1="5.9055" x2="0.4445" y2="5.9309" layer="21"/>
<rectangle x1="2.0447" y1="5.9055" x2="3.1115" y2="5.9309" layer="21"/>
<rectangle x1="4.4577" y1="5.9055" x2="5.5245" y2="5.9309" layer="21"/>
<rectangle x1="-5.7785" y1="5.9309" x2="-4.7117" y2="5.9563" layer="21"/>
<rectangle x1="-3.3909" y1="5.9309" x2="-2.2987" y2="5.9563" layer="21"/>
<rectangle x1="-0.6985" y1="5.9309" x2="0.4445" y2="5.9563" layer="21"/>
<rectangle x1="2.0447" y1="5.9309" x2="3.1115" y2="5.9563" layer="21"/>
<rectangle x1="4.4577" y1="5.9309" x2="5.5245" y2="5.9563" layer="21"/>
<rectangle x1="-5.7785" y1="5.9563" x2="-4.7117" y2="5.9817" layer="21"/>
<rectangle x1="-3.3909" y1="5.9563" x2="-2.2987" y2="5.9817" layer="21"/>
<rectangle x1="-0.6985" y1="5.9563" x2="0.4445" y2="5.9817" layer="21"/>
<rectangle x1="2.0447" y1="5.9563" x2="3.1115" y2="5.9817" layer="21"/>
<rectangle x1="4.4577" y1="5.9563" x2="5.5245" y2="5.9817" layer="21"/>
<rectangle x1="-5.7785" y1="5.9817" x2="-4.7117" y2="6.0071" layer="21"/>
<rectangle x1="-3.3909" y1="5.9817" x2="-2.2987" y2="6.0071" layer="21"/>
<rectangle x1="-0.6985" y1="5.9817" x2="0.4445" y2="6.0071" layer="21"/>
<rectangle x1="2.0447" y1="5.9817" x2="3.1115" y2="6.0071" layer="21"/>
<rectangle x1="4.4577" y1="5.9817" x2="5.5245" y2="6.0071" layer="21"/>
<rectangle x1="-5.7785" y1="6.0071" x2="-4.7117" y2="6.0325" layer="21"/>
<rectangle x1="-3.3909" y1="6.0071" x2="-2.2987" y2="6.0325" layer="21"/>
<rectangle x1="-0.6985" y1="6.0071" x2="0.4445" y2="6.0325" layer="21"/>
<rectangle x1="2.0447" y1="6.0071" x2="3.1115" y2="6.0325" layer="21"/>
<rectangle x1="4.4577" y1="6.0071" x2="5.5245" y2="6.0325" layer="21"/>
<rectangle x1="-5.7785" y1="6.0325" x2="-4.7117" y2="6.0579" layer="21"/>
<rectangle x1="-3.3909" y1="6.0325" x2="-2.2987" y2="6.0579" layer="21"/>
<rectangle x1="-0.6985" y1="6.0325" x2="0.4445" y2="6.0579" layer="21"/>
<rectangle x1="2.0447" y1="6.0325" x2="3.1115" y2="6.0579" layer="21"/>
<rectangle x1="4.4577" y1="6.0325" x2="5.5245" y2="6.0579" layer="21"/>
<rectangle x1="-5.7785" y1="6.0579" x2="-4.7117" y2="6.0833" layer="21"/>
<rectangle x1="-3.3909" y1="6.0579" x2="-2.2987" y2="6.0833" layer="21"/>
<rectangle x1="-0.6985" y1="6.0579" x2="0.4445" y2="6.0833" layer="21"/>
<rectangle x1="2.0447" y1="6.0579" x2="3.1115" y2="6.0833" layer="21"/>
<rectangle x1="4.4577" y1="6.0579" x2="5.5245" y2="6.0833" layer="21"/>
<rectangle x1="-5.7785" y1="6.0833" x2="-4.7117" y2="6.1087" layer="21"/>
<rectangle x1="-3.3909" y1="6.0833" x2="-2.2987" y2="6.1087" layer="21"/>
<rectangle x1="-0.6985" y1="6.0833" x2="0.4445" y2="6.1087" layer="21"/>
<rectangle x1="2.0447" y1="6.0833" x2="3.1115" y2="6.1087" layer="21"/>
<rectangle x1="4.4577" y1="6.0833" x2="5.5245" y2="6.1087" layer="21"/>
<rectangle x1="-5.7785" y1="6.1087" x2="-4.7117" y2="6.1341" layer="21"/>
<rectangle x1="-3.3909" y1="6.1087" x2="-2.2987" y2="6.1341" layer="21"/>
<rectangle x1="-0.6985" y1="6.1087" x2="0.4445" y2="6.1341" layer="21"/>
<rectangle x1="2.0447" y1="6.1087" x2="3.1115" y2="6.1341" layer="21"/>
<rectangle x1="4.4577" y1="6.1087" x2="5.5245" y2="6.1341" layer="21"/>
<rectangle x1="-5.7785" y1="6.1341" x2="-4.7117" y2="6.1595" layer="21"/>
<rectangle x1="-3.3909" y1="6.1341" x2="-2.2987" y2="6.1595" layer="21"/>
<rectangle x1="-0.6985" y1="6.1341" x2="0.4445" y2="6.1595" layer="21"/>
<rectangle x1="2.0447" y1="6.1341" x2="3.1115" y2="6.1595" layer="21"/>
<rectangle x1="4.4577" y1="6.1341" x2="5.5245" y2="6.1595" layer="21"/>
<rectangle x1="-5.7785" y1="6.1595" x2="-4.7117" y2="6.1849" layer="21"/>
<rectangle x1="-3.3909" y1="6.1595" x2="-2.2987" y2="6.1849" layer="21"/>
<rectangle x1="-0.6985" y1="6.1595" x2="0.4445" y2="6.1849" layer="21"/>
<rectangle x1="2.0447" y1="6.1595" x2="3.1115" y2="6.1849" layer="21"/>
<rectangle x1="4.4577" y1="6.1595" x2="5.5245" y2="6.1849" layer="21"/>
<rectangle x1="-5.7785" y1="6.1849" x2="-4.7117" y2="6.2103" layer="21"/>
<rectangle x1="-3.3909" y1="6.1849" x2="-2.2987" y2="6.2103" layer="21"/>
<rectangle x1="-0.6985" y1="6.1849" x2="0.4445" y2="6.2103" layer="21"/>
<rectangle x1="2.0447" y1="6.1849" x2="3.1115" y2="6.2103" layer="21"/>
<rectangle x1="4.4577" y1="6.1849" x2="5.5245" y2="6.2103" layer="21"/>
<rectangle x1="-5.7785" y1="6.2103" x2="-4.7117" y2="6.2357" layer="21"/>
<rectangle x1="-3.3909" y1="6.2103" x2="-2.2987" y2="6.2357" layer="21"/>
<rectangle x1="-0.6985" y1="6.2103" x2="0.4445" y2="6.2357" layer="21"/>
<rectangle x1="2.0447" y1="6.2103" x2="3.1115" y2="6.2357" layer="21"/>
<rectangle x1="4.4577" y1="6.2103" x2="5.5245" y2="6.2357" layer="21"/>
<rectangle x1="-5.7785" y1="6.2357" x2="-4.7117" y2="6.2611" layer="21"/>
<rectangle x1="-3.3909" y1="6.2357" x2="-2.2987" y2="6.2611" layer="21"/>
<rectangle x1="-0.6985" y1="6.2357" x2="0.4445" y2="6.2611" layer="21"/>
<rectangle x1="2.0447" y1="6.2357" x2="3.1115" y2="6.2611" layer="21"/>
<rectangle x1="4.4577" y1="6.2357" x2="5.5245" y2="6.2611" layer="21"/>
<rectangle x1="-5.7785" y1="6.2611" x2="-4.7117" y2="6.2865" layer="21"/>
<rectangle x1="-3.3909" y1="6.2611" x2="-2.2987" y2="6.2865" layer="21"/>
<rectangle x1="-0.6985" y1="6.2611" x2="0.4445" y2="6.2865" layer="21"/>
<rectangle x1="2.0447" y1="6.2611" x2="3.1115" y2="6.2865" layer="21"/>
<rectangle x1="4.4577" y1="6.2611" x2="5.5245" y2="6.2865" layer="21"/>
<rectangle x1="-5.7785" y1="6.2865" x2="-4.7117" y2="6.3119" layer="21"/>
<rectangle x1="-3.3909" y1="6.2865" x2="-2.2987" y2="6.3119" layer="21"/>
<rectangle x1="-0.6985" y1="6.2865" x2="0.4445" y2="6.3119" layer="21"/>
<rectangle x1="2.0447" y1="6.2865" x2="3.1115" y2="6.3119" layer="21"/>
<rectangle x1="4.4577" y1="6.2865" x2="5.5245" y2="6.3119" layer="21"/>
<rectangle x1="-5.7785" y1="6.3119" x2="-4.7117" y2="6.3373" layer="21"/>
<rectangle x1="-3.3909" y1="6.3119" x2="-2.2987" y2="6.3373" layer="21"/>
<rectangle x1="-0.6985" y1="6.3119" x2="0.4445" y2="6.3373" layer="21"/>
<rectangle x1="2.0447" y1="6.3119" x2="3.1115" y2="6.3373" layer="21"/>
<rectangle x1="4.4577" y1="6.3119" x2="5.5245" y2="6.3373" layer="21"/>
<rectangle x1="-5.7785" y1="6.3373" x2="-4.7117" y2="6.3627" layer="21"/>
<rectangle x1="-3.3909" y1="6.3373" x2="-2.3241" y2="6.3627" layer="21"/>
<rectangle x1="-0.6985" y1="6.3373" x2="0.4445" y2="6.3627" layer="21"/>
<rectangle x1="2.0447" y1="6.3373" x2="3.1115" y2="6.3627" layer="21"/>
<rectangle x1="4.4577" y1="6.3373" x2="5.5245" y2="6.3627" layer="21"/>
<rectangle x1="-5.7785" y1="6.3627" x2="-4.7117" y2="6.3881" layer="21"/>
<rectangle x1="-3.3909" y1="6.3627" x2="-2.3241" y2="6.3881" layer="21"/>
<rectangle x1="-0.6985" y1="6.3627" x2="0.4445" y2="6.3881" layer="21"/>
<rectangle x1="2.0447" y1="6.3627" x2="3.1115" y2="6.3881" layer="21"/>
<rectangle x1="4.4577" y1="6.3627" x2="5.5245" y2="6.3881" layer="21"/>
<rectangle x1="-5.7785" y1="6.3881" x2="-4.7117" y2="6.4135" layer="21"/>
<rectangle x1="-3.3909" y1="6.3881" x2="-2.3241" y2="6.4135" layer="21"/>
<rectangle x1="-0.6985" y1="6.3881" x2="0.4445" y2="6.4135" layer="21"/>
<rectangle x1="2.0447" y1="6.3881" x2="3.1115" y2="6.4135" layer="21"/>
<rectangle x1="4.4577" y1="6.3881" x2="5.5245" y2="6.4135" layer="21"/>
<rectangle x1="-5.7785" y1="6.4135" x2="-4.7117" y2="6.4389" layer="21"/>
<rectangle x1="-3.3909" y1="6.4135" x2="-2.3241" y2="6.4389" layer="21"/>
<rectangle x1="-0.6985" y1="6.4135" x2="0.4445" y2="6.4389" layer="21"/>
<rectangle x1="2.0447" y1="6.4135" x2="3.1115" y2="6.4389" layer="21"/>
<rectangle x1="4.4577" y1="6.4135" x2="5.5245" y2="6.4389" layer="21"/>
<rectangle x1="-5.7785" y1="6.4389" x2="-4.7117" y2="6.4643" layer="21"/>
<rectangle x1="-3.3909" y1="6.4389" x2="-2.3241" y2="6.4643" layer="21"/>
<rectangle x1="-0.6985" y1="6.4389" x2="0.4445" y2="6.4643" layer="21"/>
<rectangle x1="2.0447" y1="6.4389" x2="3.1115" y2="6.4643" layer="21"/>
<rectangle x1="4.4577" y1="6.4389" x2="5.5245" y2="6.4643" layer="21"/>
<rectangle x1="-5.7785" y1="6.4643" x2="-4.7117" y2="6.4897" layer="21"/>
<rectangle x1="-3.3909" y1="6.4643" x2="-2.3241" y2="6.4897" layer="21"/>
<rectangle x1="-0.6985" y1="6.4643" x2="0.4445" y2="6.4897" layer="21"/>
<rectangle x1="2.0447" y1="6.4643" x2="3.1115" y2="6.4897" layer="21"/>
<rectangle x1="4.4577" y1="6.4643" x2="5.5245" y2="6.4897" layer="21"/>
<rectangle x1="-5.7785" y1="6.4897" x2="-4.7117" y2="6.5151" layer="21"/>
<rectangle x1="-3.3909" y1="6.4897" x2="-2.3241" y2="6.5151" layer="21"/>
<rectangle x1="-0.6985" y1="6.4897" x2="0.4445" y2="6.5151" layer="21"/>
<rectangle x1="2.0447" y1="6.4897" x2="3.1115" y2="6.5151" layer="21"/>
<rectangle x1="4.4577" y1="6.4897" x2="5.5245" y2="6.5151" layer="21"/>
<rectangle x1="-5.7785" y1="6.5151" x2="-4.7117" y2="6.5405" layer="21"/>
<rectangle x1="-3.4163" y1="6.5151" x2="-2.3241" y2="6.5405" layer="21"/>
<rectangle x1="-0.6985" y1="6.5151" x2="0.4445" y2="6.5405" layer="21"/>
<rectangle x1="2.0447" y1="6.5151" x2="3.1115" y2="6.5405" layer="21"/>
<rectangle x1="4.4577" y1="6.5151" x2="5.5245" y2="6.5405" layer="21"/>
<rectangle x1="-5.7785" y1="6.5405" x2="-4.7117" y2="6.5659" layer="21"/>
<rectangle x1="-3.4163" y1="6.5405" x2="-2.3241" y2="6.5659" layer="21"/>
<rectangle x1="-0.6985" y1="6.5405" x2="0.4445" y2="6.5659" layer="21"/>
<rectangle x1="2.0447" y1="6.5405" x2="3.1115" y2="6.5659" layer="21"/>
<rectangle x1="4.4577" y1="6.5405" x2="5.5245" y2="6.5659" layer="21"/>
<rectangle x1="-5.7785" y1="6.5659" x2="-4.7117" y2="6.5913" layer="21"/>
<rectangle x1="-3.4163" y1="6.5659" x2="-2.3241" y2="6.5913" layer="21"/>
<rectangle x1="-0.6985" y1="6.5659" x2="0.4445" y2="6.5913" layer="21"/>
<rectangle x1="2.0447" y1="6.5659" x2="3.1115" y2="6.5913" layer="21"/>
<rectangle x1="4.4577" y1="6.5659" x2="5.5245" y2="6.5913" layer="21"/>
<rectangle x1="-5.7785" y1="6.5913" x2="-4.7117" y2="6.6167" layer="21"/>
<rectangle x1="-3.4163" y1="6.5913" x2="-2.3241" y2="6.6167" layer="21"/>
<rectangle x1="-0.6985" y1="6.5913" x2="0.4445" y2="6.6167" layer="21"/>
<rectangle x1="2.0447" y1="6.5913" x2="3.1115" y2="6.6167" layer="21"/>
<rectangle x1="4.4577" y1="6.5913" x2="5.5245" y2="6.6167" layer="21"/>
<rectangle x1="-5.7785" y1="6.6167" x2="-4.7117" y2="6.6421" layer="21"/>
<rectangle x1="-3.4163" y1="6.6167" x2="-2.3241" y2="6.6421" layer="21"/>
<rectangle x1="-0.6985" y1="6.6167" x2="0.4445" y2="6.6421" layer="21"/>
<rectangle x1="2.0447" y1="6.6167" x2="3.1115" y2="6.6421" layer="21"/>
<rectangle x1="4.4577" y1="6.6167" x2="5.5245" y2="6.6421" layer="21"/>
<rectangle x1="-5.7785" y1="6.6421" x2="-4.7117" y2="6.6675" layer="21"/>
<rectangle x1="-3.4163" y1="6.6421" x2="-2.3241" y2="6.6675" layer="21"/>
<rectangle x1="-0.6985" y1="6.6421" x2="0.4445" y2="6.6675" layer="21"/>
<rectangle x1="2.0447" y1="6.6421" x2="3.1115" y2="6.6675" layer="21"/>
<rectangle x1="4.4577" y1="6.6421" x2="5.5245" y2="6.6675" layer="21"/>
<rectangle x1="-5.7785" y1="6.6675" x2="-4.7117" y2="6.6929" layer="21"/>
<rectangle x1="-3.4163" y1="6.6675" x2="-2.3495" y2="6.6929" layer="21"/>
<rectangle x1="-0.6985" y1="6.6675" x2="0.4445" y2="6.6929" layer="21"/>
<rectangle x1="2.0447" y1="6.6675" x2="3.1115" y2="6.6929" layer="21"/>
<rectangle x1="4.4577" y1="6.6675" x2="5.5245" y2="6.6929" layer="21"/>
<rectangle x1="-5.7785" y1="6.6929" x2="-4.7117" y2="6.7183" layer="21"/>
<rectangle x1="-3.4163" y1="6.6929" x2="-2.3495" y2="6.7183" layer="21"/>
<rectangle x1="-0.6985" y1="6.6929" x2="0.4445" y2="6.7183" layer="21"/>
<rectangle x1="2.0447" y1="6.6929" x2="3.1115" y2="6.7183" layer="21"/>
<rectangle x1="4.4577" y1="6.6929" x2="5.5245" y2="6.7183" layer="21"/>
<rectangle x1="-5.7785" y1="6.7183" x2="-4.7117" y2="6.7437" layer="21"/>
<rectangle x1="-3.4417" y1="6.7183" x2="-2.3495" y2="6.7437" layer="21"/>
<rectangle x1="-0.6985" y1="6.7183" x2="0.4445" y2="6.7437" layer="21"/>
<rectangle x1="2.0447" y1="6.7183" x2="3.1115" y2="6.7437" layer="21"/>
<rectangle x1="4.4577" y1="6.7183" x2="5.5245" y2="6.7437" layer="21"/>
<rectangle x1="-5.7785" y1="6.7437" x2="-4.7117" y2="6.7691" layer="21"/>
<rectangle x1="-3.4417" y1="6.7437" x2="-2.3495" y2="6.7691" layer="21"/>
<rectangle x1="-0.6985" y1="6.7437" x2="0.4445" y2="6.7691" layer="21"/>
<rectangle x1="2.0447" y1="6.7437" x2="3.1115" y2="6.7691" layer="21"/>
<rectangle x1="4.4577" y1="6.7437" x2="5.5245" y2="6.7691" layer="21"/>
<rectangle x1="-5.7785" y1="6.7691" x2="-4.7117" y2="6.7945" layer="21"/>
<rectangle x1="-3.4417" y1="6.7691" x2="-2.3495" y2="6.7945" layer="21"/>
<rectangle x1="-0.6985" y1="6.7691" x2="0.4445" y2="6.7945" layer="21"/>
<rectangle x1="2.0447" y1="6.7691" x2="3.1115" y2="6.7945" layer="21"/>
<rectangle x1="4.4577" y1="6.7691" x2="5.5245" y2="6.7945" layer="21"/>
<rectangle x1="-5.7785" y1="6.7945" x2="-4.7117" y2="6.8199" layer="21"/>
<rectangle x1="-3.4417" y1="6.7945" x2="-2.3495" y2="6.8199" layer="21"/>
<rectangle x1="-0.6985" y1="6.7945" x2="0.4445" y2="6.8199" layer="21"/>
<rectangle x1="2.0447" y1="6.7945" x2="3.1115" y2="6.8199" layer="21"/>
<rectangle x1="4.4577" y1="6.7945" x2="5.5245" y2="6.8199" layer="21"/>
<rectangle x1="-5.7785" y1="6.8199" x2="-4.7117" y2="6.8453" layer="21"/>
<rectangle x1="-3.4417" y1="6.8199" x2="-2.3495" y2="6.8453" layer="21"/>
<rectangle x1="-0.6985" y1="6.8199" x2="0.4445" y2="6.8453" layer="21"/>
<rectangle x1="2.0447" y1="6.8199" x2="3.1115" y2="6.8453" layer="21"/>
<rectangle x1="4.4577" y1="6.8199" x2="5.5245" y2="6.8453" layer="21"/>
<rectangle x1="-5.7785" y1="6.8453" x2="-4.7117" y2="6.8707" layer="21"/>
<rectangle x1="-3.4671" y1="6.8453" x2="-2.3495" y2="6.8707" layer="21"/>
<rectangle x1="-0.6985" y1="6.8453" x2="0.4445" y2="6.8707" layer="21"/>
<rectangle x1="2.0447" y1="6.8453" x2="3.1115" y2="6.8707" layer="21"/>
<rectangle x1="4.4577" y1="6.8453" x2="5.5245" y2="6.8707" layer="21"/>
<rectangle x1="-5.7785" y1="6.8707" x2="-4.7117" y2="6.8961" layer="21"/>
<rectangle x1="-3.4671" y1="6.8707" x2="-2.3749" y2="6.8961" layer="21"/>
<rectangle x1="-0.6985" y1="6.8707" x2="0.4445" y2="6.8961" layer="21"/>
<rectangle x1="2.0447" y1="6.8707" x2="3.1115" y2="6.8961" layer="21"/>
<rectangle x1="4.4577" y1="6.8707" x2="5.5245" y2="6.8961" layer="21"/>
<rectangle x1="-5.7785" y1="6.8961" x2="-4.7117" y2="6.9215" layer="21"/>
<rectangle x1="-3.4671" y1="6.8961" x2="-2.3749" y2="6.9215" layer="21"/>
<rectangle x1="-0.6985" y1="6.8961" x2="0.4445" y2="6.9215" layer="21"/>
<rectangle x1="2.0447" y1="6.8961" x2="3.1115" y2="6.9215" layer="21"/>
<rectangle x1="4.4577" y1="6.8961" x2="5.5245" y2="6.9215" layer="21"/>
<rectangle x1="-5.7785" y1="6.9215" x2="-4.7117" y2="6.9469" layer="21"/>
<rectangle x1="-3.4925" y1="6.9215" x2="-2.3749" y2="6.9469" layer="21"/>
<rectangle x1="-0.6985" y1="6.9215" x2="0.4445" y2="6.9469" layer="21"/>
<rectangle x1="2.0447" y1="6.9215" x2="3.1115" y2="6.9469" layer="21"/>
<rectangle x1="4.4577" y1="6.9215" x2="5.5245" y2="6.9469" layer="21"/>
<rectangle x1="-5.7785" y1="6.9469" x2="-4.7117" y2="6.9723" layer="21"/>
<rectangle x1="-3.4925" y1="6.9469" x2="-2.3749" y2="6.9723" layer="21"/>
<rectangle x1="-0.6985" y1="6.9469" x2="0.4445" y2="6.9723" layer="21"/>
<rectangle x1="2.0447" y1="6.9469" x2="3.1115" y2="6.9723" layer="21"/>
<rectangle x1="4.4577" y1="6.9469" x2="5.5245" y2="6.9723" layer="21"/>
<rectangle x1="-5.7785" y1="6.9723" x2="-4.7117" y2="6.9977" layer="21"/>
<rectangle x1="-3.4925" y1="6.9723" x2="-2.3749" y2="6.9977" layer="21"/>
<rectangle x1="-0.6985" y1="6.9723" x2="0.4445" y2="6.9977" layer="21"/>
<rectangle x1="2.0447" y1="6.9723" x2="3.1115" y2="6.9977" layer="21"/>
<rectangle x1="4.4577" y1="6.9723" x2="5.5245" y2="6.9977" layer="21"/>
<rectangle x1="-5.7785" y1="6.9977" x2="-4.7117" y2="7.0231" layer="21"/>
<rectangle x1="-3.5179" y1="6.9977" x2="-2.4003" y2="7.0231" layer="21"/>
<rectangle x1="-0.6985" y1="6.9977" x2="0.4445" y2="7.0231" layer="21"/>
<rectangle x1="2.0447" y1="6.9977" x2="3.1115" y2="7.0231" layer="21"/>
<rectangle x1="4.4577" y1="6.9977" x2="5.5245" y2="7.0231" layer="21"/>
<rectangle x1="-5.7785" y1="7.0231" x2="-4.7117" y2="7.0485" layer="21"/>
<rectangle x1="-3.5179" y1="7.0231" x2="-2.4003" y2="7.0485" layer="21"/>
<rectangle x1="-0.6985" y1="7.0231" x2="0.4445" y2="7.0485" layer="21"/>
<rectangle x1="2.0447" y1="7.0231" x2="3.1115" y2="7.0485" layer="21"/>
<rectangle x1="4.4577" y1="7.0231" x2="5.5245" y2="7.0485" layer="21"/>
<rectangle x1="-5.7785" y1="7.0485" x2="-4.7117" y2="7.0739" layer="21"/>
<rectangle x1="-3.5433" y1="7.0485" x2="-2.4003" y2="7.0739" layer="21"/>
<rectangle x1="-0.6985" y1="7.0485" x2="0.4445" y2="7.0739" layer="21"/>
<rectangle x1="2.0447" y1="7.0485" x2="3.1115" y2="7.0739" layer="21"/>
<rectangle x1="4.4577" y1="7.0485" x2="5.5245" y2="7.0739" layer="21"/>
<rectangle x1="-5.7785" y1="7.0739" x2="-4.7117" y2="7.0993" layer="21"/>
<rectangle x1="-3.5687" y1="7.0739" x2="-2.4003" y2="7.0993" layer="21"/>
<rectangle x1="-0.6985" y1="7.0739" x2="0.4445" y2="7.0993" layer="21"/>
<rectangle x1="2.0447" y1="7.0739" x2="3.1115" y2="7.0993" layer="21"/>
<rectangle x1="4.4577" y1="7.0739" x2="5.5245" y2="7.0993" layer="21"/>
<rectangle x1="-5.7785" y1="7.0993" x2="-4.7117" y2="7.1247" layer="21"/>
<rectangle x1="-3.5687" y1="7.0993" x2="-2.4257" y2="7.1247" layer="21"/>
<rectangle x1="-0.6985" y1="7.0993" x2="0.4445" y2="7.1247" layer="21"/>
<rectangle x1="2.0447" y1="7.0993" x2="3.1115" y2="7.1247" layer="21"/>
<rectangle x1="4.4577" y1="7.0993" x2="5.5245" y2="7.1247" layer="21"/>
<rectangle x1="-5.7785" y1="7.1247" x2="-4.7117" y2="7.1501" layer="21"/>
<rectangle x1="-3.5941" y1="7.1247" x2="-2.4257" y2="7.1501" layer="21"/>
<rectangle x1="-0.6985" y1="7.1247" x2="0.4445" y2="7.1501" layer="21"/>
<rectangle x1="2.0447" y1="7.1247" x2="3.1115" y2="7.1501" layer="21"/>
<rectangle x1="4.4577" y1="7.1247" x2="5.5245" y2="7.1501" layer="21"/>
<rectangle x1="-5.7785" y1="7.1501" x2="-4.7117" y2="7.1755" layer="21"/>
<rectangle x1="-3.6195" y1="7.1501" x2="-2.4257" y2="7.1755" layer="21"/>
<rectangle x1="-0.6985" y1="7.1501" x2="0.4445" y2="7.1755" layer="21"/>
<rectangle x1="2.0447" y1="7.1501" x2="3.1115" y2="7.1755" layer="21"/>
<rectangle x1="4.4577" y1="7.1501" x2="5.5245" y2="7.1755" layer="21"/>
<rectangle x1="-5.7785" y1="7.1755" x2="-4.7117" y2="7.2009" layer="21"/>
<rectangle x1="-3.6449" y1="7.1755" x2="-2.4257" y2="7.2009" layer="21"/>
<rectangle x1="-0.6985" y1="7.1755" x2="0.4445" y2="7.2009" layer="21"/>
<rectangle x1="2.0447" y1="7.1755" x2="3.1115" y2="7.2009" layer="21"/>
<rectangle x1="4.4577" y1="7.1755" x2="5.5245" y2="7.2009" layer="21"/>
<rectangle x1="-5.7785" y1="7.2009" x2="-4.7117" y2="7.2263" layer="21"/>
<rectangle x1="-3.6957" y1="7.2009" x2="-2.4511" y2="7.2263" layer="21"/>
<rectangle x1="-0.6985" y1="7.2009" x2="0.4445" y2="7.2263" layer="21"/>
<rectangle x1="2.0447" y1="7.2009" x2="3.1115" y2="7.2263" layer="21"/>
<rectangle x1="4.4577" y1="7.2009" x2="5.5245" y2="7.2263" layer="21"/>
<rectangle x1="-5.7785" y1="7.2263" x2="-4.7117" y2="7.2517" layer="21"/>
<rectangle x1="-3.7465" y1="7.2263" x2="-2.4511" y2="7.2517" layer="21"/>
<rectangle x1="-0.7239" y1="7.2263" x2="0.4445" y2="7.2517" layer="21"/>
<rectangle x1="2.0447" y1="7.2263" x2="3.1115" y2="7.2517" layer="21"/>
<rectangle x1="4.4577" y1="7.2263" x2="5.5245" y2="7.2517" layer="21"/>
<rectangle x1="-5.7785" y1="7.2517" x2="-4.7117" y2="7.2771" layer="21"/>
<rectangle x1="-3.7973" y1="7.2517" x2="-2.4511" y2="7.2771" layer="21"/>
<rectangle x1="-1.8161" y1="7.2517" x2="1.5367" y2="7.2771" layer="21"/>
<rectangle x1="2.0447" y1="7.2517" x2="3.1115" y2="7.2771" layer="21"/>
<rectangle x1="4.4577" y1="7.2517" x2="5.5245" y2="7.2771" layer="21"/>
<rectangle x1="-5.7785" y1="7.2771" x2="-4.7117" y2="7.3025" layer="21"/>
<rectangle x1="-3.8989" y1="7.2771" x2="-2.4765" y2="7.3025" layer="21"/>
<rectangle x1="-1.8415" y1="7.2771" x2="1.5621" y2="7.3025" layer="21"/>
<rectangle x1="2.0447" y1="7.2771" x2="3.1115" y2="7.3025" layer="21"/>
<rectangle x1="4.4577" y1="7.2771" x2="5.5245" y2="7.3025" layer="21"/>
<rectangle x1="-5.7785" y1="7.3025" x2="-2.4765" y2="7.3279" layer="21"/>
<rectangle x1="-1.8415" y1="7.3025" x2="1.5621" y2="7.3279" layer="21"/>
<rectangle x1="2.0447" y1="7.3025" x2="3.1115" y2="7.3279" layer="21"/>
<rectangle x1="4.4577" y1="7.3025" x2="5.5245" y2="7.3279" layer="21"/>
<rectangle x1="-5.7785" y1="7.3279" x2="-2.5019" y2="7.3533" layer="21"/>
<rectangle x1="-1.8669" y1="7.3279" x2="1.5875" y2="7.3533" layer="21"/>
<rectangle x1="2.0447" y1="7.3279" x2="3.1115" y2="7.3533" layer="21"/>
<rectangle x1="4.4577" y1="7.3279" x2="5.5245" y2="7.3533" layer="21"/>
<rectangle x1="-5.7785" y1="7.3533" x2="-2.5019" y2="7.3787" layer="21"/>
<rectangle x1="-1.8669" y1="7.3533" x2="1.5875" y2="7.3787" layer="21"/>
<rectangle x1="2.0447" y1="7.3533" x2="3.1115" y2="7.3787" layer="21"/>
<rectangle x1="4.4577" y1="7.3533" x2="5.5245" y2="7.3787" layer="21"/>
<rectangle x1="-5.7785" y1="7.3787" x2="-2.5273" y2="7.4041" layer="21"/>
<rectangle x1="-1.8669" y1="7.3787" x2="1.5875" y2="7.4041" layer="21"/>
<rectangle x1="2.0447" y1="7.3787" x2="3.1115" y2="7.4041" layer="21"/>
<rectangle x1="4.4577" y1="7.3787" x2="5.5245" y2="7.4041" layer="21"/>
<rectangle x1="-5.7785" y1="7.4041" x2="-2.5273" y2="7.4295" layer="21"/>
<rectangle x1="-1.8669" y1="7.4041" x2="1.5875" y2="7.4295" layer="21"/>
<rectangle x1="2.0447" y1="7.4041" x2="3.1115" y2="7.4295" layer="21"/>
<rectangle x1="4.4577" y1="7.4041" x2="5.5245" y2="7.4295" layer="21"/>
<rectangle x1="-5.7785" y1="7.4295" x2="-2.5527" y2="7.4549" layer="21"/>
<rectangle x1="-1.8669" y1="7.4295" x2="1.5875" y2="7.4549" layer="21"/>
<rectangle x1="2.0447" y1="7.4295" x2="3.1115" y2="7.4549" layer="21"/>
<rectangle x1="4.4577" y1="7.4295" x2="5.5245" y2="7.4549" layer="21"/>
<rectangle x1="-5.7785" y1="7.4549" x2="-2.5527" y2="7.4803" layer="21"/>
<rectangle x1="-1.8669" y1="7.4549" x2="1.5875" y2="7.4803" layer="21"/>
<rectangle x1="2.0447" y1="7.4549" x2="3.1115" y2="7.4803" layer="21"/>
<rectangle x1="4.4577" y1="7.4549" x2="5.5245" y2="7.4803" layer="21"/>
<rectangle x1="-5.7785" y1="7.4803" x2="-2.5781" y2="7.5057" layer="21"/>
<rectangle x1="-1.8669" y1="7.4803" x2="1.5875" y2="7.5057" layer="21"/>
<rectangle x1="2.0447" y1="7.4803" x2="3.1115" y2="7.5057" layer="21"/>
<rectangle x1="4.4577" y1="7.4803" x2="5.5245" y2="7.5057" layer="21"/>
<rectangle x1="-5.7785" y1="7.5057" x2="-2.5781" y2="7.5311" layer="21"/>
<rectangle x1="-1.8669" y1="7.5057" x2="1.5875" y2="7.5311" layer="21"/>
<rectangle x1="2.0447" y1="7.5057" x2="3.1115" y2="7.5311" layer="21"/>
<rectangle x1="4.4577" y1="7.5057" x2="5.5245" y2="7.5311" layer="21"/>
<rectangle x1="-5.7785" y1="7.5311" x2="-2.6035" y2="7.5565" layer="21"/>
<rectangle x1="-1.8669" y1="7.5311" x2="1.5875" y2="7.5565" layer="21"/>
<rectangle x1="2.0447" y1="7.5311" x2="3.1115" y2="7.5565" layer="21"/>
<rectangle x1="4.4577" y1="7.5311" x2="5.5245" y2="7.5565" layer="21"/>
<rectangle x1="-5.7785" y1="7.5565" x2="-2.6289" y2="7.5819" layer="21"/>
<rectangle x1="-1.8669" y1="7.5565" x2="1.5875" y2="7.5819" layer="21"/>
<rectangle x1="2.0447" y1="7.5565" x2="3.1115" y2="7.5819" layer="21"/>
<rectangle x1="4.4577" y1="7.5565" x2="5.5245" y2="7.5819" layer="21"/>
<rectangle x1="-5.7785" y1="7.5819" x2="-2.6289" y2="7.6073" layer="21"/>
<rectangle x1="-1.8669" y1="7.5819" x2="1.5875" y2="7.6073" layer="21"/>
<rectangle x1="2.0447" y1="7.5819" x2="3.1115" y2="7.6073" layer="21"/>
<rectangle x1="4.4577" y1="7.5819" x2="5.5245" y2="7.6073" layer="21"/>
<rectangle x1="-5.7785" y1="7.6073" x2="-2.6543" y2="7.6327" layer="21"/>
<rectangle x1="-1.8669" y1="7.6073" x2="1.5875" y2="7.6327" layer="21"/>
<rectangle x1="2.0447" y1="7.6073" x2="3.1115" y2="7.6327" layer="21"/>
<rectangle x1="4.4577" y1="7.6073" x2="5.5245" y2="7.6327" layer="21"/>
<rectangle x1="-5.7785" y1="7.6327" x2="-2.6797" y2="7.6581" layer="21"/>
<rectangle x1="-1.8669" y1="7.6327" x2="1.5875" y2="7.6581" layer="21"/>
<rectangle x1="2.0447" y1="7.6327" x2="3.1115" y2="7.6581" layer="21"/>
<rectangle x1="4.4577" y1="7.6327" x2="5.5245" y2="7.6581" layer="21"/>
<rectangle x1="-5.7785" y1="7.6581" x2="-2.7051" y2="7.6835" layer="21"/>
<rectangle x1="-1.8669" y1="7.6581" x2="1.5875" y2="7.6835" layer="21"/>
<rectangle x1="2.0447" y1="7.6581" x2="3.1115" y2="7.6835" layer="21"/>
<rectangle x1="4.4577" y1="7.6581" x2="5.5245" y2="7.6835" layer="21"/>
<rectangle x1="-5.7785" y1="7.6835" x2="-2.7305" y2="7.7089" layer="21"/>
<rectangle x1="-1.8669" y1="7.6835" x2="1.5875" y2="7.7089" layer="21"/>
<rectangle x1="2.0447" y1="7.6835" x2="3.1115" y2="7.7089" layer="21"/>
<rectangle x1="4.4577" y1="7.6835" x2="5.5245" y2="7.7089" layer="21"/>
<rectangle x1="-5.7785" y1="7.7089" x2="-2.7559" y2="7.7343" layer="21"/>
<rectangle x1="-1.8669" y1="7.7089" x2="1.5875" y2="7.7343" layer="21"/>
<rectangle x1="2.0447" y1="7.7089" x2="3.1115" y2="7.7343" layer="21"/>
<rectangle x1="4.4577" y1="7.7089" x2="5.5245" y2="7.7343" layer="21"/>
<rectangle x1="-5.7785" y1="7.7343" x2="-2.7813" y2="7.7597" layer="21"/>
<rectangle x1="-1.8669" y1="7.7343" x2="1.5875" y2="7.7597" layer="21"/>
<rectangle x1="2.0447" y1="7.7343" x2="3.1115" y2="7.7597" layer="21"/>
<rectangle x1="4.4577" y1="7.7343" x2="5.5245" y2="7.7597" layer="21"/>
<rectangle x1="-5.7785" y1="7.7597" x2="-2.8067" y2="7.7851" layer="21"/>
<rectangle x1="-1.8669" y1="7.7597" x2="1.5875" y2="7.7851" layer="21"/>
<rectangle x1="2.0447" y1="7.7597" x2="3.1115" y2="7.7851" layer="21"/>
<rectangle x1="4.4577" y1="7.7597" x2="5.5245" y2="7.7851" layer="21"/>
<rectangle x1="-5.7785" y1="7.7851" x2="-2.8321" y2="7.8105" layer="21"/>
<rectangle x1="-1.8669" y1="7.7851" x2="1.5875" y2="7.8105" layer="21"/>
<rectangle x1="2.0447" y1="7.7851" x2="3.1115" y2="7.8105" layer="21"/>
<rectangle x1="4.4577" y1="7.7851" x2="5.5245" y2="7.8105" layer="21"/>
<rectangle x1="-5.7785" y1="7.8105" x2="-2.8575" y2="7.8359" layer="21"/>
<rectangle x1="-1.8669" y1="7.8105" x2="1.5875" y2="7.8359" layer="21"/>
<rectangle x1="2.0447" y1="7.8105" x2="3.1115" y2="7.8359" layer="21"/>
<rectangle x1="4.4577" y1="7.8105" x2="5.5245" y2="7.8359" layer="21"/>
<rectangle x1="-5.7785" y1="7.8359" x2="-2.9083" y2="7.8613" layer="21"/>
<rectangle x1="-1.8669" y1="7.8359" x2="1.5875" y2="7.8613" layer="21"/>
<rectangle x1="2.0447" y1="7.8359" x2="3.1115" y2="7.8613" layer="21"/>
<rectangle x1="4.4577" y1="7.8359" x2="5.5245" y2="7.8613" layer="21"/>
<rectangle x1="-5.7785" y1="7.8613" x2="-2.9337" y2="7.8867" layer="21"/>
<rectangle x1="-1.8669" y1="7.8613" x2="1.5875" y2="7.8867" layer="21"/>
<rectangle x1="2.0447" y1="7.8613" x2="3.1115" y2="7.8867" layer="21"/>
<rectangle x1="4.4577" y1="7.8613" x2="5.5245" y2="7.8867" layer="21"/>
<rectangle x1="-5.7785" y1="7.8867" x2="-2.9845" y2="7.9121" layer="21"/>
<rectangle x1="-1.8669" y1="7.8867" x2="1.5875" y2="7.9121" layer="21"/>
<rectangle x1="2.0447" y1="7.8867" x2="3.1115" y2="7.9121" layer="21"/>
<rectangle x1="4.4577" y1="7.8867" x2="5.5245" y2="7.9121" layer="21"/>
<rectangle x1="-5.7785" y1="7.9121" x2="-3.0353" y2="7.9375" layer="21"/>
<rectangle x1="-1.8669" y1="7.9121" x2="1.5875" y2="7.9375" layer="21"/>
<rectangle x1="2.0447" y1="7.9121" x2="3.1115" y2="7.9375" layer="21"/>
<rectangle x1="4.4577" y1="7.9121" x2="5.5245" y2="7.9375" layer="21"/>
<rectangle x1="-5.7785" y1="7.9375" x2="-3.0861" y2="7.9629" layer="21"/>
<rectangle x1="-1.8669" y1="7.9375" x2="1.5875" y2="7.9629" layer="21"/>
<rectangle x1="2.0447" y1="7.9375" x2="3.1115" y2="7.9629" layer="21"/>
<rectangle x1="4.4577" y1="7.9375" x2="5.5245" y2="7.9629" layer="21"/>
<rectangle x1="-5.7785" y1="7.9629" x2="-3.1623" y2="7.9883" layer="21"/>
<rectangle x1="-1.8669" y1="7.9629" x2="1.5875" y2="7.9883" layer="21"/>
<rectangle x1="2.0447" y1="7.9629" x2="3.1115" y2="7.9883" layer="21"/>
<rectangle x1="4.4577" y1="7.9629" x2="5.5245" y2="7.9883" layer="21"/>
<rectangle x1="-5.7785" y1="7.9883" x2="-3.2385" y2="8.0137" layer="21"/>
<rectangle x1="-1.8415" y1="7.9883" x2="1.5875" y2="8.0137" layer="21"/>
<rectangle x1="2.0447" y1="7.9883" x2="3.1115" y2="8.0137" layer="21"/>
<rectangle x1="4.4577" y1="7.9883" x2="5.5245" y2="8.0137" layer="21"/>
<rectangle x1="-5.7785" y1="8.0137" x2="-3.3401" y2="8.0391" layer="21"/>
<rectangle x1="-1.8415" y1="8.0137" x2="1.5621" y2="8.0391" layer="21"/>
<rectangle x1="2.0701" y1="8.0137" x2="3.0861" y2="8.0391" layer="21"/>
<rectangle x1="4.4831" y1="8.0137" x2="5.4991" y2="8.0391" layer="21"/>
<rectangle x1="-5.7531" y1="8.0391" x2="-3.4671" y2="8.0645" layer="21"/>
<rectangle x1="-1.8161" y1="8.0391" x2="1.5621" y2="8.0645" layer="21"/>
<rectangle x1="2.0701" y1="8.0391" x2="3.0607" y2="8.0645" layer="21"/>
<rectangle x1="4.4831" y1="8.0391" x2="5.4737" y2="8.0645" layer="21"/>
<rectangle x1="-5.7023" y1="8.0645" x2="-3.6703" y2="8.0899" layer="21"/>
<rectangle x1="-1.7907" y1="8.0645" x2="1.5113" y2="8.0899" layer="21"/>
<rectangle x1="2.1209" y1="8.0645" x2="3.0353" y2="8.0899" layer="21"/>
<rectangle x1="4.5339" y1="8.0645" x2="5.4483" y2="8.0899" layer="21"/>
</package>
<package name="DTU-SMALL-5MM">
<rectangle x1="-1.6615" y1="-4.7135" x2="-1.6465" y2="-4.6985" layer="21"/>
<rectangle x1="1.5185" y1="-4.7135" x2="1.5335" y2="-4.6985" layer="21"/>
<rectangle x1="-1.6915" y1="-4.6985" x2="-1.6015" y2="-4.6835" layer="21"/>
<rectangle x1="1.4735" y1="-4.6985" x2="1.5635" y2="-4.6835" layer="21"/>
<rectangle x1="-1.7215" y1="-4.6835" x2="-1.5715" y2="-4.6685" layer="21"/>
<rectangle x1="1.4435" y1="-4.6835" x2="1.5935" y2="-4.6685" layer="21"/>
<rectangle x1="-1.7515" y1="-4.6685" x2="-1.5265" y2="-4.6535" layer="21"/>
<rectangle x1="1.3985" y1="-4.6685" x2="1.6235" y2="-4.6535" layer="21"/>
<rectangle x1="-1.7815" y1="-4.6535" x2="-1.4965" y2="-4.6385" layer="21"/>
<rectangle x1="1.3685" y1="-4.6535" x2="1.6535" y2="-4.6385" layer="21"/>
<rectangle x1="-1.8115" y1="-4.6385" x2="-1.4515" y2="-4.6235" layer="21"/>
<rectangle x1="1.3235" y1="-4.6385" x2="1.6835" y2="-4.6235" layer="21"/>
<rectangle x1="-1.8265" y1="-4.6235" x2="-1.4215" y2="-4.6085" layer="21"/>
<rectangle x1="1.2935" y1="-4.6235" x2="1.6985" y2="-4.6085" layer="21"/>
<rectangle x1="-1.8565" y1="-4.6085" x2="-1.3765" y2="-4.5935" layer="21"/>
<rectangle x1="1.2485" y1="-4.6085" x2="1.7285" y2="-4.5935" layer="21"/>
<rectangle x1="-1.8865" y1="-4.5935" x2="-1.3315" y2="-4.5785" layer="21"/>
<rectangle x1="1.2035" y1="-4.5935" x2="1.7585" y2="-4.5785" layer="21"/>
<rectangle x1="-1.9165" y1="-4.5785" x2="-1.3015" y2="-4.5635" layer="21"/>
<rectangle x1="1.1735" y1="-4.5785" x2="1.7885" y2="-4.5635" layer="21"/>
<rectangle x1="-1.9465" y1="-4.5635" x2="-1.2565" y2="-4.5485" layer="21"/>
<rectangle x1="1.1285" y1="-4.5635" x2="1.8185" y2="-4.5485" layer="21"/>
<rectangle x1="-1.9765" y1="-4.5485" x2="-1.2115" y2="-4.5335" layer="21"/>
<rectangle x1="1.0835" y1="-4.5485" x2="1.8485" y2="-4.5335" layer="21"/>
<rectangle x1="-2.0065" y1="-4.5335" x2="-1.1665" y2="-4.5185" layer="21"/>
<rectangle x1="1.0385" y1="-4.5335" x2="1.8785" y2="-4.5185" layer="21"/>
<rectangle x1="-2.0365" y1="-4.5185" x2="-1.1365" y2="-4.5035" layer="21"/>
<rectangle x1="1.0085" y1="-4.5185" x2="1.9085" y2="-4.5035" layer="21"/>
<rectangle x1="-2.0665" y1="-4.5035" x2="-1.0915" y2="-4.4885" layer="21"/>
<rectangle x1="0.9635" y1="-4.5035" x2="1.9385" y2="-4.4885" layer="21"/>
<rectangle x1="-2.0965" y1="-4.4885" x2="-1.0465" y2="-4.4735" layer="21"/>
<rectangle x1="0.9185" y1="-4.4885" x2="1.9685" y2="-4.4735" layer="21"/>
<rectangle x1="-2.1265" y1="-4.4735" x2="-1.0015" y2="-4.4585" layer="21"/>
<rectangle x1="0.8735" y1="-4.4735" x2="1.9985" y2="-4.4585" layer="21"/>
<rectangle x1="-2.1415" y1="-4.4585" x2="-0.9565" y2="-4.4435" layer="21"/>
<rectangle x1="0.8285" y1="-4.4585" x2="2.0135" y2="-4.4435" layer="21"/>
<rectangle x1="-2.1715" y1="-4.4435" x2="-0.9265" y2="-4.4285" layer="21"/>
<rectangle x1="0.7985" y1="-4.4435" x2="2.0435" y2="-4.4285" layer="21"/>
<rectangle x1="-2.2015" y1="-4.4285" x2="-0.8815" y2="-4.4135" layer="21"/>
<rectangle x1="0.7535" y1="-4.4285" x2="2.0735" y2="-4.4135" layer="21"/>
<rectangle x1="-2.2315" y1="-4.4135" x2="-0.8365" y2="-4.3985" layer="21"/>
<rectangle x1="0.7085" y1="-4.4135" x2="2.1035" y2="-4.3985" layer="21"/>
<rectangle x1="-2.2615" y1="-4.3985" x2="-0.7765" y2="-4.3835" layer="21"/>
<rectangle x1="0.6485" y1="-4.3985" x2="2.1335" y2="-4.3835" layer="21"/>
<rectangle x1="-2.2915" y1="-4.3835" x2="-0.7315" y2="-4.3685" layer="21"/>
<rectangle x1="0.6035" y1="-4.3835" x2="2.1635" y2="-4.3685" layer="21"/>
<rectangle x1="-2.3215" y1="-4.3685" x2="-0.6865" y2="-4.3535" layer="21"/>
<rectangle x1="0.5585" y1="-4.3685" x2="2.1935" y2="-4.3535" layer="21"/>
<rectangle x1="-2.3515" y1="-4.3535" x2="-0.6265" y2="-4.3385" layer="21"/>
<rectangle x1="0.4985" y1="-4.3535" x2="2.2235" y2="-4.3385" layer="21"/>
<rectangle x1="-2.3815" y1="-4.3385" x2="-0.5665" y2="-4.3235" layer="21"/>
<rectangle x1="0.4385" y1="-4.3385" x2="2.2535" y2="-4.3235" layer="21"/>
<rectangle x1="-2.4115" y1="-4.3235" x2="-0.4915" y2="-4.3085" layer="21"/>
<rectangle x1="0.3635" y1="-4.3235" x2="2.2835" y2="-4.3085" layer="21"/>
<rectangle x1="-2.4415" y1="-4.3085" x2="-0.4315" y2="-4.2935" layer="21"/>
<rectangle x1="0.3035" y1="-4.3085" x2="2.3135" y2="-4.2935" layer="21"/>
<rectangle x1="-2.4565" y1="-4.2935" x2="-0.3415" y2="-4.2785" layer="21"/>
<rectangle x1="0.2135" y1="-4.2935" x2="2.3435" y2="-4.2785" layer="21"/>
<rectangle x1="-2.4865" y1="-4.2785" x2="-0.2065" y2="-4.2635" layer="21"/>
<rectangle x1="0.0785" y1="-4.2785" x2="2.3585" y2="-4.2635" layer="21"/>
<rectangle x1="-2.5165" y1="-4.2635" x2="2.3885" y2="-4.2485" layer="21"/>
<rectangle x1="-2.5465" y1="-4.2485" x2="2.4185" y2="-4.2335" layer="21"/>
<rectangle x1="-2.5765" y1="-4.2335" x2="2.4485" y2="-4.2185" layer="21"/>
<rectangle x1="-2.6065" y1="-4.2185" x2="2.4785" y2="-4.2035" layer="21"/>
<rectangle x1="-2.6365" y1="-4.2035" x2="2.5085" y2="-4.1885" layer="21"/>
<rectangle x1="-2.6665" y1="-4.1885" x2="2.5385" y2="-4.1735" layer="21"/>
<rectangle x1="-2.6965" y1="-4.1735" x2="2.5685" y2="-4.1585" layer="21"/>
<rectangle x1="-2.7115" y1="-4.1585" x2="2.5985" y2="-4.1435" layer="21"/>
<rectangle x1="-2.7415" y1="-4.1435" x2="2.6135" y2="-4.1285" layer="21"/>
<rectangle x1="-2.7715" y1="-4.1285" x2="2.6435" y2="-4.1135" layer="21"/>
<rectangle x1="-2.8015" y1="-4.1135" x2="2.6735" y2="-4.0985" layer="21"/>
<rectangle x1="-2.8165" y1="-4.0985" x2="2.6885" y2="-4.0835" layer="21"/>
<rectangle x1="-2.8015" y1="-4.0835" x2="2.6735" y2="-4.0685" layer="21"/>
<rectangle x1="-2.7715" y1="-4.0685" x2="2.6435" y2="-4.0535" layer="21"/>
<rectangle x1="-2.7415" y1="-4.0535" x2="2.6135" y2="-4.0385" layer="21"/>
<rectangle x1="-2.7115" y1="-4.0385" x2="2.5835" y2="-4.0235" layer="21"/>
<rectangle x1="-2.6815" y1="-4.0235" x2="2.5535" y2="-4.0085" layer="21"/>
<rectangle x1="-2.6515" y1="-4.0085" x2="2.5235" y2="-3.9935" layer="21"/>
<rectangle x1="-2.6215" y1="-3.9935" x2="2.4935" y2="-3.9785" layer="21"/>
<rectangle x1="-2.5915" y1="-3.9785" x2="2.4635" y2="-3.9635" layer="21"/>
<rectangle x1="-2.5615" y1="-3.9635" x2="2.4335" y2="-3.9485" layer="21"/>
<rectangle x1="-2.5315" y1="-3.9485" x2="2.4035" y2="-3.9335" layer="21"/>
<rectangle x1="-2.5015" y1="-3.9335" x2="2.3735" y2="-3.9185" layer="21"/>
<rectangle x1="-2.4715" y1="-3.9185" x2="-0.2815" y2="-3.9035" layer="21"/>
<rectangle x1="0.1535" y1="-3.9185" x2="2.3585" y2="-3.9035" layer="21"/>
<rectangle x1="-2.4565" y1="-3.9035" x2="-0.3865" y2="-3.8885" layer="21"/>
<rectangle x1="0.2585" y1="-3.9035" x2="2.3285" y2="-3.8885" layer="21"/>
<rectangle x1="-2.4265" y1="-3.8885" x2="-0.4615" y2="-3.8735" layer="21"/>
<rectangle x1="0.3335" y1="-3.8885" x2="2.2985" y2="-3.8735" layer="21"/>
<rectangle x1="-2.3965" y1="-3.8735" x2="-0.5215" y2="-3.8585" layer="21"/>
<rectangle x1="0.4085" y1="-3.8735" x2="2.2685" y2="-3.8585" layer="21"/>
<rectangle x1="-2.3665" y1="-3.8585" x2="-0.5815" y2="-3.8435" layer="21"/>
<rectangle x1="0.4685" y1="-3.8585" x2="2.2385" y2="-3.8435" layer="21"/>
<rectangle x1="-2.3365" y1="-3.8435" x2="-0.6415" y2="-3.8285" layer="21"/>
<rectangle x1="0.5135" y1="-3.8435" x2="2.2085" y2="-3.8285" layer="21"/>
<rectangle x1="-2.3065" y1="-3.8285" x2="-0.7015" y2="-3.8135" layer="21"/>
<rectangle x1="0.5735" y1="-3.8285" x2="2.1785" y2="-3.8135" layer="21"/>
<rectangle x1="-2.2765" y1="-3.8135" x2="-0.7465" y2="-3.7985" layer="21"/>
<rectangle x1="0.6185" y1="-3.8135" x2="2.1485" y2="-3.7985" layer="21"/>
<rectangle x1="-2.2465" y1="-3.7985" x2="-0.8065" y2="-3.7835" layer="21"/>
<rectangle x1="0.6785" y1="-3.7985" x2="2.1185" y2="-3.7835" layer="21"/>
<rectangle x1="-2.2165" y1="-3.7835" x2="-0.8515" y2="-3.7685" layer="21"/>
<rectangle x1="0.7235" y1="-3.7835" x2="2.0885" y2="-3.7685" layer="21"/>
<rectangle x1="-2.1865" y1="-3.7685" x2="-0.8965" y2="-3.7535" layer="21"/>
<rectangle x1="0.7685" y1="-3.7685" x2="2.0585" y2="-3.7535" layer="21"/>
<rectangle x1="-2.1565" y1="-3.7535" x2="-0.9415" y2="-3.7385" layer="21"/>
<rectangle x1="0.8135" y1="-3.7535" x2="2.0285" y2="-3.7385" layer="21"/>
<rectangle x1="-2.1415" y1="-3.7385" x2="-0.9865" y2="-3.7235" layer="21"/>
<rectangle x1="0.8585" y1="-3.7385" x2="2.0135" y2="-3.7235" layer="21"/>
<rectangle x1="-2.1115" y1="-3.7235" x2="-1.0315" y2="-3.7085" layer="21"/>
<rectangle x1="0.9035" y1="-3.7235" x2="1.9835" y2="-3.7085" layer="21"/>
<rectangle x1="-2.0815" y1="-3.7085" x2="-1.0615" y2="-3.6935" layer="21"/>
<rectangle x1="0.9485" y1="-3.7085" x2="1.9535" y2="-3.6935" layer="21"/>
<rectangle x1="-2.0515" y1="-3.6935" x2="-1.1065" y2="-3.6785" layer="21"/>
<rectangle x1="0.9785" y1="-3.6935" x2="1.9235" y2="-3.6785" layer="21"/>
<rectangle x1="-2.0215" y1="-3.6785" x2="-1.1515" y2="-3.6635" layer="21"/>
<rectangle x1="1.0235" y1="-3.6785" x2="1.8935" y2="-3.6635" layer="21"/>
<rectangle x1="-1.9915" y1="-3.6635" x2="-1.1965" y2="-3.6485" layer="21"/>
<rectangle x1="1.0685" y1="-3.6635" x2="1.8635" y2="-3.6485" layer="21"/>
<rectangle x1="-1.9615" y1="-3.6485" x2="-1.2265" y2="-3.6335" layer="21"/>
<rectangle x1="1.0985" y1="-3.6485" x2="1.8335" y2="-3.6335" layer="21"/>
<rectangle x1="-1.9315" y1="-3.6335" x2="-1.2715" y2="-3.6185" layer="21"/>
<rectangle x1="1.1435" y1="-3.6335" x2="1.8035" y2="-3.6185" layer="21"/>
<rectangle x1="-1.9015" y1="-3.6185" x2="-1.3165" y2="-3.6035" layer="21"/>
<rectangle x1="1.1885" y1="-3.6185" x2="1.7885" y2="-3.6035" layer="21"/>
<rectangle x1="-1.8865" y1="-3.6035" x2="-1.3465" y2="-3.5885" layer="21"/>
<rectangle x1="1.2185" y1="-3.6035" x2="1.7585" y2="-3.5885" layer="21"/>
<rectangle x1="-1.8565" y1="-3.5885" x2="-1.3915" y2="-3.5735" layer="21"/>
<rectangle x1="1.2635" y1="-3.5885" x2="1.7285" y2="-3.5735" layer="21"/>
<rectangle x1="-1.8265" y1="-3.5735" x2="-1.4365" y2="-3.5585" layer="21"/>
<rectangle x1="1.3085" y1="-3.5735" x2="1.6985" y2="-3.5585" layer="21"/>
<rectangle x1="-1.7965" y1="-3.5585" x2="-1.4665" y2="-3.5435" layer="21"/>
<rectangle x1="1.3385" y1="-3.5585" x2="1.6685" y2="-3.5435" layer="21"/>
<rectangle x1="-1.7665" y1="-3.5435" x2="-1.5115" y2="-3.5285" layer="21"/>
<rectangle x1="1.3835" y1="-3.5435" x2="1.6385" y2="-3.5285" layer="21"/>
<rectangle x1="-1.7365" y1="-3.5285" x2="-1.5565" y2="-3.5135" layer="21"/>
<rectangle x1="1.4285" y1="-3.5285" x2="1.6085" y2="-3.5135" layer="21"/>
<rectangle x1="-1.7065" y1="-3.5135" x2="-1.5865" y2="-3.4985" layer="21"/>
<rectangle x1="1.4585" y1="-3.5135" x2="1.5785" y2="-3.4985" layer="21"/>
<rectangle x1="-1.6765" y1="-3.4985" x2="-1.6315" y2="-3.4835" layer="21"/>
<rectangle x1="1.5035" y1="-3.4985" x2="1.5485" y2="-3.4835" layer="21"/>
<rectangle x1="-1.6615" y1="-3.1835" x2="-1.6465" y2="-3.1685" layer="21"/>
<rectangle x1="1.5185" y1="-3.1835" x2="1.5335" y2="-3.1685" layer="21"/>
<rectangle x1="-1.6915" y1="-3.1685" x2="-1.6165" y2="-3.1535" layer="21"/>
<rectangle x1="1.4885" y1="-3.1685" x2="1.5635" y2="-3.1535" layer="21"/>
<rectangle x1="-1.7215" y1="-3.1535" x2="-1.5715" y2="-3.1385" layer="21"/>
<rectangle x1="1.4435" y1="-3.1535" x2="1.5935" y2="-3.1385" layer="21"/>
<rectangle x1="-1.7515" y1="-3.1385" x2="-1.5265" y2="-3.1235" layer="21"/>
<rectangle x1="1.4135" y1="-3.1385" x2="1.6235" y2="-3.1235" layer="21"/>
<rectangle x1="-1.7815" y1="-3.1235" x2="-1.4965" y2="-3.1085" layer="21"/>
<rectangle x1="1.3685" y1="-3.1235" x2="1.6535" y2="-3.1085" layer="21"/>
<rectangle x1="-1.7965" y1="-3.1085" x2="-1.4515" y2="-3.0935" layer="21"/>
<rectangle x1="1.3235" y1="-3.1085" x2="1.6685" y2="-3.0935" layer="21"/>
<rectangle x1="-1.8265" y1="-3.0935" x2="-1.4215" y2="-3.0785" layer="21"/>
<rectangle x1="1.2935" y1="-3.0935" x2="1.6985" y2="-3.0785" layer="21"/>
<rectangle x1="-1.8565" y1="-3.0785" x2="-1.3765" y2="-3.0635" layer="21"/>
<rectangle x1="1.2485" y1="-3.0785" x2="1.7285" y2="-3.0635" layer="21"/>
<rectangle x1="-1.8865" y1="-3.0635" x2="-1.3315" y2="-3.0485" layer="21"/>
<rectangle x1="1.2035" y1="-3.0635" x2="1.7585" y2="-3.0485" layer="21"/>
<rectangle x1="-1.9165" y1="-3.0485" x2="-1.3015" y2="-3.0335" layer="21"/>
<rectangle x1="1.1735" y1="-3.0485" x2="1.7885" y2="-3.0335" layer="21"/>
<rectangle x1="-1.9465" y1="-3.0335" x2="-1.2565" y2="-3.0185" layer="21"/>
<rectangle x1="1.1285" y1="-3.0335" x2="1.8185" y2="-3.0185" layer="21"/>
<rectangle x1="-1.9765" y1="-3.0185" x2="-1.2115" y2="-3.0035" layer="21"/>
<rectangle x1="1.0835" y1="-3.0185" x2="1.8485" y2="-3.0035" layer="21"/>
<rectangle x1="-2.0065" y1="-3.0035" x2="-1.1665" y2="-2.9885" layer="21"/>
<rectangle x1="1.0385" y1="-3.0035" x2="1.8785" y2="-2.9885" layer="21"/>
<rectangle x1="-2.0365" y1="-2.9885" x2="-1.1365" y2="-2.9735" layer="21"/>
<rectangle x1="1.0085" y1="-2.9885" x2="1.9085" y2="-2.9735" layer="21"/>
<rectangle x1="-2.0665" y1="-2.9735" x2="-1.0915" y2="-2.9585" layer="21"/>
<rectangle x1="0.9635" y1="-2.9735" x2="1.9385" y2="-2.9585" layer="21"/>
<rectangle x1="-2.0965" y1="-2.9585" x2="-1.0465" y2="-2.9435" layer="21"/>
<rectangle x1="0.9185" y1="-2.9585" x2="1.9685" y2="-2.9435" layer="21"/>
<rectangle x1="-2.1115" y1="-2.9435" x2="-1.0165" y2="-2.9285" layer="21"/>
<rectangle x1="0.8885" y1="-2.9435" x2="1.9835" y2="-2.9285" layer="21"/>
<rectangle x1="-2.1415" y1="-2.9285" x2="-0.9715" y2="-2.9135" layer="21"/>
<rectangle x1="0.8435" y1="-2.9285" x2="2.0135" y2="-2.9135" layer="21"/>
<rectangle x1="-2.1715" y1="-2.9135" x2="-0.9265" y2="-2.8985" layer="21"/>
<rectangle x1="0.7985" y1="-2.9135" x2="2.0435" y2="-2.8985" layer="21"/>
<rectangle x1="-2.2015" y1="-2.8985" x2="-0.8815" y2="-2.8835" layer="21"/>
<rectangle x1="0.7535" y1="-2.8985" x2="2.0735" y2="-2.8835" layer="21"/>
<rectangle x1="-2.2315" y1="-2.8835" x2="-0.8365" y2="-2.8685" layer="21"/>
<rectangle x1="0.7085" y1="-2.8835" x2="2.1035" y2="-2.8685" layer="21"/>
<rectangle x1="-2.2615" y1="-2.8685" x2="-0.7765" y2="-2.8535" layer="21"/>
<rectangle x1="0.6635" y1="-2.8685" x2="2.1335" y2="-2.8535" layer="21"/>
<rectangle x1="-2.2915" y1="-2.8535" x2="-0.7315" y2="-2.8385" layer="21"/>
<rectangle x1="0.6035" y1="-2.8535" x2="2.1635" y2="-2.8385" layer="21"/>
<rectangle x1="-2.3215" y1="-2.8385" x2="-0.6865" y2="-2.8235" layer="21"/>
<rectangle x1="0.5585" y1="-2.8385" x2="2.1935" y2="-2.8235" layer="21"/>
<rectangle x1="-2.3515" y1="-2.8235" x2="-0.6265" y2="-2.8085" layer="21"/>
<rectangle x1="0.4985" y1="-2.8235" x2="2.2235" y2="-2.8085" layer="21"/>
<rectangle x1="-2.3815" y1="-2.8085" x2="-0.5665" y2="-2.7935" layer="21"/>
<rectangle x1="0.4385" y1="-2.8085" x2="2.2535" y2="-2.7935" layer="21"/>
<rectangle x1="-2.4115" y1="-2.7935" x2="-0.5065" y2="-2.7785" layer="21"/>
<rectangle x1="0.3785" y1="-2.7935" x2="2.2835" y2="-2.7785" layer="21"/>
<rectangle x1="-2.4265" y1="-2.7785" x2="-0.4315" y2="-2.7635" layer="21"/>
<rectangle x1="0.3035" y1="-2.7785" x2="2.3135" y2="-2.7635" layer="21"/>
<rectangle x1="-2.4565" y1="-2.7635" x2="-0.3415" y2="-2.7485" layer="21"/>
<rectangle x1="0.2135" y1="-2.7635" x2="2.3285" y2="-2.7485" layer="21"/>
<rectangle x1="-2.4865" y1="-2.7485" x2="-0.2215" y2="-2.7335" layer="21"/>
<rectangle x1="0.0935" y1="-2.7485" x2="2.3585" y2="-2.7335" layer="21"/>
<rectangle x1="-2.5165" y1="-2.7335" x2="2.3885" y2="-2.7185" layer="21"/>
<rectangle x1="-2.5465" y1="-2.7185" x2="2.4185" y2="-2.7035" layer="21"/>
<rectangle x1="-2.5765" y1="-2.7035" x2="2.4485" y2="-2.6885" layer="21"/>
<rectangle x1="-2.6065" y1="-2.6885" x2="2.4785" y2="-2.6735" layer="21"/>
<rectangle x1="-2.6365" y1="-2.6735" x2="2.5085" y2="-2.6585" layer="21"/>
<rectangle x1="-2.6665" y1="-2.6585" x2="2.5385" y2="-2.6435" layer="21"/>
<rectangle x1="-2.6965" y1="-2.6435" x2="2.5685" y2="-2.6285" layer="21"/>
<rectangle x1="-2.7265" y1="-2.6285" x2="2.5985" y2="-2.6135" layer="21"/>
<rectangle x1="-2.7415" y1="-2.6135" x2="2.6135" y2="-2.5985" layer="21"/>
<rectangle x1="-2.7715" y1="-2.5985" x2="2.6435" y2="-2.5835" layer="21"/>
<rectangle x1="-2.8015" y1="-2.5835" x2="2.6735" y2="-2.5685" layer="21"/>
<rectangle x1="-2.8165" y1="-2.5685" x2="2.6885" y2="-2.5535" layer="21"/>
<rectangle x1="-2.8015" y1="-2.5535" x2="2.6735" y2="-2.5385" layer="21"/>
<rectangle x1="-2.7715" y1="-2.5385" x2="2.6435" y2="-2.5235" layer="21"/>
<rectangle x1="-2.7415" y1="-2.5235" x2="2.6135" y2="-2.5085" layer="21"/>
<rectangle x1="-2.7115" y1="-2.5085" x2="2.5835" y2="-2.4935" layer="21"/>
<rectangle x1="-2.6815" y1="-2.4935" x2="2.5535" y2="-2.4785" layer="21"/>
<rectangle x1="-2.6515" y1="-2.4785" x2="2.5235" y2="-2.4635" layer="21"/>
<rectangle x1="-2.6215" y1="-2.4635" x2="2.4935" y2="-2.4485" layer="21"/>
<rectangle x1="-2.5915" y1="-2.4485" x2="2.4635" y2="-2.4335" layer="21"/>
<rectangle x1="-2.5615" y1="-2.4335" x2="2.4335" y2="-2.4185" layer="21"/>
<rectangle x1="-2.5315" y1="-2.4185" x2="2.4035" y2="-2.4035" layer="21"/>
<rectangle x1="-2.5015" y1="-2.4035" x2="2.3885" y2="-2.3885" layer="21"/>
<rectangle x1="-2.4865" y1="-2.3885" x2="-0.2815" y2="-2.3735" layer="21"/>
<rectangle x1="0.1535" y1="-2.3885" x2="2.3585" y2="-2.3735" layer="21"/>
<rectangle x1="-2.4565" y1="-2.3735" x2="-0.3715" y2="-2.3585" layer="21"/>
<rectangle x1="0.2435" y1="-2.3735" x2="2.3285" y2="-2.3585" layer="21"/>
<rectangle x1="-2.4265" y1="-2.3585" x2="-0.4615" y2="-2.3435" layer="21"/>
<rectangle x1="0.3335" y1="-2.3585" x2="2.2985" y2="-2.3435" layer="21"/>
<rectangle x1="-2.3965" y1="-2.3435" x2="-0.5215" y2="-2.3285" layer="21"/>
<rectangle x1="0.3935" y1="-2.3435" x2="2.2685" y2="-2.3285" layer="21"/>
<rectangle x1="-2.3665" y1="-2.3285" x2="-0.5815" y2="-2.3135" layer="21"/>
<rectangle x1="0.4535" y1="-2.3285" x2="2.2385" y2="-2.3135" layer="21"/>
<rectangle x1="-2.3365" y1="-2.3135" x2="-0.6415" y2="-2.2985" layer="21"/>
<rectangle x1="0.5135" y1="-2.3135" x2="2.2085" y2="-2.2985" layer="21"/>
<rectangle x1="-2.3065" y1="-2.2985" x2="-0.7015" y2="-2.2835" layer="21"/>
<rectangle x1="0.5735" y1="-2.2985" x2="2.1785" y2="-2.2835" layer="21"/>
<rectangle x1="-2.2765" y1="-2.2835" x2="-0.7465" y2="-2.2685" layer="21"/>
<rectangle x1="0.6185" y1="-2.2835" x2="2.1485" y2="-2.2685" layer="21"/>
<rectangle x1="-2.2465" y1="-2.2685" x2="-0.8065" y2="-2.2535" layer="21"/>
<rectangle x1="0.6785" y1="-2.2685" x2="2.1185" y2="-2.2535" layer="21"/>
<rectangle x1="-2.2165" y1="-2.2535" x2="-0.8515" y2="-2.2385" layer="21"/>
<rectangle x1="0.7235" y1="-2.2535" x2="2.0885" y2="-2.2385" layer="21"/>
<rectangle x1="-2.1865" y1="-2.2385" x2="-0.8965" y2="-2.2235" layer="21"/>
<rectangle x1="0.7685" y1="-2.2385" x2="2.0585" y2="-2.2235" layer="21"/>
<rectangle x1="-2.1715" y1="-2.2235" x2="-0.9415" y2="-2.2085" layer="21"/>
<rectangle x1="0.8135" y1="-2.2235" x2="2.0435" y2="-2.2085" layer="21"/>
<rectangle x1="-2.1415" y1="-2.2085" x2="-0.9865" y2="-2.1935" layer="21"/>
<rectangle x1="0.8585" y1="-2.2085" x2="2.0135" y2="-2.1935" layer="21"/>
<rectangle x1="-2.1115" y1="-2.1935" x2="-1.0165" y2="-2.1785" layer="21"/>
<rectangle x1="0.9035" y1="-2.1935" x2="1.9835" y2="-2.1785" layer="21"/>
<rectangle x1="-2.0815" y1="-2.1785" x2="-1.0615" y2="-2.1635" layer="21"/>
<rectangle x1="0.9335" y1="-2.1785" x2="1.9535" y2="-2.1635" layer="21"/>
<rectangle x1="-2.0515" y1="-2.1635" x2="-1.1065" y2="-2.1485" layer="21"/>
<rectangle x1="0.9785" y1="-2.1635" x2="1.9235" y2="-2.1485" layer="21"/>
<rectangle x1="-2.0215" y1="-2.1485" x2="-1.1515" y2="-2.1335" layer="21"/>
<rectangle x1="1.0235" y1="-2.1485" x2="1.8935" y2="-2.1335" layer="21"/>
<rectangle x1="-1.9915" y1="-2.1335" x2="-1.1815" y2="-2.1185" layer="21"/>
<rectangle x1="1.0535" y1="-2.1335" x2="1.8635" y2="-2.1185" layer="21"/>
<rectangle x1="-1.9615" y1="-2.1185" x2="-1.2265" y2="-2.1035" layer="21"/>
<rectangle x1="1.0985" y1="-2.1185" x2="1.8335" y2="-2.1035" layer="21"/>
<rectangle x1="-1.9315" y1="-2.1035" x2="-1.2715" y2="-2.0885" layer="21"/>
<rectangle x1="1.1435" y1="-2.1035" x2="1.8035" y2="-2.0885" layer="21"/>
<rectangle x1="-1.9165" y1="-2.0885" x2="-1.3015" y2="-2.0735" layer="21"/>
<rectangle x1="1.1885" y1="-2.0885" x2="1.7885" y2="-2.0735" layer="21"/>
<rectangle x1="-1.8865" y1="-2.0735" x2="-1.3465" y2="-2.0585" layer="21"/>
<rectangle x1="1.2185" y1="-2.0735" x2="1.7585" y2="-2.0585" layer="21"/>
<rectangle x1="-1.8565" y1="-2.0585" x2="-1.3915" y2="-2.0435" layer="21"/>
<rectangle x1="1.2635" y1="-2.0585" x2="1.7285" y2="-2.0435" layer="21"/>
<rectangle x1="-1.8265" y1="-2.0435" x2="-1.4365" y2="-2.0285" layer="21"/>
<rectangle x1="1.3085" y1="-2.0435" x2="1.6985" y2="-2.0285" layer="21"/>
<rectangle x1="-1.7965" y1="-2.0285" x2="-1.4665" y2="-2.0135" layer="21"/>
<rectangle x1="1.3385" y1="-2.0285" x2="1.6685" y2="-2.0135" layer="21"/>
<rectangle x1="-1.7665" y1="-2.0135" x2="-1.5115" y2="-1.9985" layer="21"/>
<rectangle x1="1.3835" y1="-2.0135" x2="1.6385" y2="-1.9985" layer="21"/>
<rectangle x1="-1.7365" y1="-1.9985" x2="-1.5415" y2="-1.9835" layer="21"/>
<rectangle x1="1.4135" y1="-1.9985" x2="1.6085" y2="-1.9835" layer="21"/>
<rectangle x1="-1.7065" y1="-1.9835" x2="-1.5865" y2="-1.9685" layer="21"/>
<rectangle x1="1.4585" y1="-1.9835" x2="1.5785" y2="-1.9685" layer="21"/>
<rectangle x1="-1.6765" y1="-1.9685" x2="-1.6165" y2="-1.9535" layer="21"/>
<rectangle x1="1.4885" y1="-1.9685" x2="1.5485" y2="-1.9535" layer="21"/>
<rectangle x1="1.5185" y1="-1.6535" x2="1.5335" y2="-1.6385" layer="21"/>
<rectangle x1="-1.6915" y1="-1.6385" x2="-1.6165" y2="-1.6235" layer="21"/>
<rectangle x1="1.4885" y1="-1.6385" x2="1.5635" y2="-1.6235" layer="21"/>
<rectangle x1="-1.7215" y1="-1.6235" x2="-1.5715" y2="-1.6085" layer="21"/>
<rectangle x1="1.4435" y1="-1.6235" x2="1.5935" y2="-1.6085" layer="21"/>
<rectangle x1="-1.7515" y1="-1.6085" x2="-1.5415" y2="-1.5935" layer="21"/>
<rectangle x1="1.4135" y1="-1.6085" x2="1.6235" y2="-1.5935" layer="21"/>
<rectangle x1="-1.7665" y1="-1.5935" x2="-1.4965" y2="-1.5785" layer="21"/>
<rectangle x1="1.3685" y1="-1.5935" x2="1.6385" y2="-1.5785" layer="21"/>
<rectangle x1="-1.7965" y1="-1.5785" x2="-1.4515" y2="-1.5635" layer="21"/>
<rectangle x1="1.3235" y1="-1.5785" x2="1.6685" y2="-1.5635" layer="21"/>
<rectangle x1="-1.8265" y1="-1.5635" x2="-1.4215" y2="-1.5485" layer="21"/>
<rectangle x1="1.2935" y1="-1.5635" x2="1.6985" y2="-1.5485" layer="21"/>
<rectangle x1="-1.8565" y1="-1.5485" x2="-1.3765" y2="-1.5335" layer="21"/>
<rectangle x1="1.2485" y1="-1.5485" x2="1.7285" y2="-1.5335" layer="21"/>
<rectangle x1="-1.8865" y1="-1.5335" x2="-1.3315" y2="-1.5185" layer="21"/>
<rectangle x1="1.2035" y1="-1.5335" x2="1.7585" y2="-1.5185" layer="21"/>
<rectangle x1="-1.9165" y1="-1.5185" x2="-1.3015" y2="-1.5035" layer="21"/>
<rectangle x1="1.1735" y1="-1.5185" x2="1.7885" y2="-1.5035" layer="21"/>
<rectangle x1="-1.9465" y1="-1.5035" x2="-1.2565" y2="-1.4885" layer="21"/>
<rectangle x1="1.1285" y1="-1.5035" x2="1.8185" y2="-1.4885" layer="21"/>
<rectangle x1="-1.9765" y1="-1.4885" x2="-1.2115" y2="-1.4735" layer="21"/>
<rectangle x1="1.0835" y1="-1.4885" x2="1.8485" y2="-1.4735" layer="21"/>
<rectangle x1="-2.0065" y1="-1.4735" x2="-1.1815" y2="-1.4585" layer="21"/>
<rectangle x1="1.0535" y1="-1.4735" x2="1.8785" y2="-1.4585" layer="21"/>
<rectangle x1="-2.0365" y1="-1.4585" x2="-1.1365" y2="-1.4435" layer="21"/>
<rectangle x1="1.0085" y1="-1.4585" x2="1.9085" y2="-1.4435" layer="21"/>
<rectangle x1="-2.0665" y1="-1.4435" x2="-1.0915" y2="-1.4285" layer="21"/>
<rectangle x1="0.9635" y1="-1.4435" x2="1.9385" y2="-1.4285" layer="21"/>
<rectangle x1="-2.0815" y1="-1.4285" x2="-1.0615" y2="-1.4135" layer="21"/>
<rectangle x1="0.9335" y1="-1.4285" x2="1.9535" y2="-1.4135" layer="21"/>
<rectangle x1="-2.1115" y1="-1.4135" x2="-1.0165" y2="-1.3985" layer="21"/>
<rectangle x1="0.8885" y1="-1.4135" x2="1.9835" y2="-1.3985" layer="21"/>
<rectangle x1="-2.1415" y1="-1.3985" x2="-0.9715" y2="-1.3835" layer="21"/>
<rectangle x1="0.8435" y1="-1.3985" x2="2.0135" y2="-1.3835" layer="21"/>
<rectangle x1="-2.1715" y1="-1.3835" x2="-0.9265" y2="-1.3685" layer="21"/>
<rectangle x1="0.7985" y1="-1.3835" x2="2.0435" y2="-1.3685" layer="21"/>
<rectangle x1="-2.2015" y1="-1.3685" x2="-0.8815" y2="-1.3535" layer="21"/>
<rectangle x1="0.7535" y1="-1.3685" x2="2.0735" y2="-1.3535" layer="21"/>
<rectangle x1="-2.2315" y1="-1.3535" x2="-0.8365" y2="-1.3385" layer="21"/>
<rectangle x1="0.7085" y1="-1.3535" x2="2.1035" y2="-1.3385" layer="21"/>
<rectangle x1="-2.2615" y1="-1.3385" x2="-0.7765" y2="-1.3235" layer="21"/>
<rectangle x1="0.6635" y1="-1.3385" x2="2.1335" y2="-1.3235" layer="21"/>
<rectangle x1="-2.2915" y1="-1.3235" x2="-0.7315" y2="-1.3085" layer="21"/>
<rectangle x1="0.6035" y1="-1.3235" x2="2.1635" y2="-1.3085" layer="21"/>
<rectangle x1="-2.3215" y1="-1.3085" x2="-0.6865" y2="-1.2935" layer="21"/>
<rectangle x1="0.5585" y1="-1.3085" x2="2.1935" y2="-1.2935" layer="21"/>
<rectangle x1="-2.3515" y1="-1.2935" x2="-0.6265" y2="-1.2785" layer="21"/>
<rectangle x1="0.4985" y1="-1.2935" x2="2.2235" y2="-1.2785" layer="21"/>
<rectangle x1="-2.3815" y1="-1.2785" x2="-0.5665" y2="-1.2635" layer="21"/>
<rectangle x1="0.4385" y1="-1.2785" x2="2.2535" y2="-1.2635" layer="21"/>
<rectangle x1="-2.3965" y1="-1.2635" x2="-0.5065" y2="-1.2485" layer="21"/>
<rectangle x1="0.3785" y1="-1.2635" x2="2.2835" y2="-1.2485" layer="21"/>
<rectangle x1="-2.4265" y1="-1.2485" x2="-0.4315" y2="-1.2335" layer="21"/>
<rectangle x1="0.3035" y1="-1.2485" x2="2.2985" y2="-1.2335" layer="21"/>
<rectangle x1="-2.4565" y1="-1.2335" x2="-0.3565" y2="-1.2185" layer="21"/>
<rectangle x1="0.2285" y1="-1.2335" x2="2.3285" y2="-1.2185" layer="21"/>
<rectangle x1="-2.4865" y1="-1.2185" x2="-0.2365" y2="-1.2035" layer="21"/>
<rectangle x1="0.1085" y1="-1.2185" x2="2.3585" y2="-1.2035" layer="21"/>
<rectangle x1="-2.5165" y1="-1.2035" x2="2.3885" y2="-1.1885" layer="21"/>
<rectangle x1="-2.5465" y1="-1.1885" x2="2.4185" y2="-1.1735" layer="21"/>
<rectangle x1="-2.5765" y1="-1.1735" x2="2.4485" y2="-1.1585" layer="21"/>
<rectangle x1="-2.6065" y1="-1.1585" x2="2.4785" y2="-1.1435" layer="21"/>
<rectangle x1="-2.6365" y1="-1.1435" x2="2.5085" y2="-1.1285" layer="21"/>
<rectangle x1="-2.6665" y1="-1.1285" x2="2.5385" y2="-1.1135" layer="21"/>
<rectangle x1="-2.6965" y1="-1.1135" x2="2.5685" y2="-1.0985" layer="21"/>
<rectangle x1="-2.7115" y1="-1.0985" x2="2.5985" y2="-1.0835" layer="21"/>
<rectangle x1="-2.7415" y1="-1.0835" x2="2.6135" y2="-1.0685" layer="21"/>
<rectangle x1="-2.7715" y1="-1.0685" x2="2.6435" y2="-1.0535" layer="21"/>
<rectangle x1="-2.8015" y1="-1.0535" x2="2.6735" y2="-1.0385" layer="21"/>
<rectangle x1="-2.8165" y1="-1.0385" x2="2.6885" y2="-1.0235" layer="21"/>
<rectangle x1="-2.8015" y1="-1.0235" x2="2.6735" y2="-1.0085" layer="21"/>
<rectangle x1="-2.7715" y1="-1.0085" x2="2.6435" y2="-0.9935" layer="21"/>
<rectangle x1="-2.7415" y1="-0.9935" x2="2.6135" y2="-0.9785" layer="21"/>
<rectangle x1="-2.7115" y1="-0.9785" x2="2.5835" y2="-0.9635" layer="21"/>
<rectangle x1="-2.6815" y1="-0.9635" x2="2.5535" y2="-0.9485" layer="21"/>
<rectangle x1="-2.6515" y1="-0.9485" x2="2.5235" y2="-0.9335" layer="21"/>
<rectangle x1="-2.6215" y1="-0.9335" x2="2.4935" y2="-0.9185" layer="21"/>
<rectangle x1="-2.5915" y1="-0.9185" x2="2.4635" y2="-0.9035" layer="21"/>
<rectangle x1="-2.5615" y1="-0.9035" x2="2.4335" y2="-0.8885" layer="21"/>
<rectangle x1="-2.5315" y1="-0.8885" x2="2.4185" y2="-0.8735" layer="21"/>
<rectangle x1="-2.5165" y1="-0.8735" x2="2.3885" y2="-0.8585" layer="21"/>
<rectangle x1="-2.4865" y1="-0.8585" x2="-0.2665" y2="-0.8435" layer="21"/>
<rectangle x1="0.1385" y1="-0.8585" x2="2.3585" y2="-0.8435" layer="21"/>
<rectangle x1="-2.4565" y1="-0.8435" x2="-0.3715" y2="-0.8285" layer="21"/>
<rectangle x1="0.2435" y1="-0.8435" x2="2.3285" y2="-0.8285" layer="21"/>
<rectangle x1="-2.4265" y1="-0.8285" x2="-0.4465" y2="-0.8135" layer="21"/>
<rectangle x1="0.3185" y1="-0.8285" x2="2.2985" y2="-0.8135" layer="21"/>
<rectangle x1="-2.3965" y1="-0.8135" x2="-0.5215" y2="-0.7985" layer="21"/>
<rectangle x1="0.3935" y1="-0.8135" x2="2.2685" y2="-0.7985" layer="21"/>
<rectangle x1="-2.3665" y1="-0.7985" x2="-0.5815" y2="-0.7835" layer="21"/>
<rectangle x1="0.4535" y1="-0.7985" x2="2.2385" y2="-0.7835" layer="21"/>
<rectangle x1="-2.3365" y1="-0.7835" x2="-0.6415" y2="-0.7685" layer="21"/>
<rectangle x1="0.5135" y1="-0.7835" x2="2.2085" y2="-0.7685" layer="21"/>
<rectangle x1="-2.3065" y1="-0.7685" x2="-0.7015" y2="-0.7535" layer="21"/>
<rectangle x1="0.5735" y1="-0.7685" x2="2.1785" y2="-0.7535" layer="21"/>
<rectangle x1="-2.2765" y1="-0.7535" x2="-0.7465" y2="-0.7385" layer="21"/>
<rectangle x1="0.6185" y1="-0.7535" x2="2.1485" y2="-0.7385" layer="21"/>
<rectangle x1="-2.2465" y1="-0.7385" x2="-0.7915" y2="-0.7235" layer="21"/>
<rectangle x1="0.6785" y1="-0.7385" x2="2.1185" y2="-0.7235" layer="21"/>
<rectangle x1="-2.2165" y1="-0.7235" x2="-0.8515" y2="-0.7085" layer="21"/>
<rectangle x1="0.7235" y1="-0.7235" x2="2.0885" y2="-0.7085" layer="21"/>
<rectangle x1="-2.2015" y1="-0.7085" x2="-0.8965" y2="-0.6935" layer="21"/>
<rectangle x1="0.7685" y1="-0.7085" x2="2.0735" y2="-0.6935" layer="21"/>
<rectangle x1="-2.1715" y1="-0.6935" x2="-0.9415" y2="-0.6785" layer="21"/>
<rectangle x1="0.8135" y1="-0.6935" x2="2.0435" y2="-0.6785" layer="21"/>
<rectangle x1="-2.1415" y1="-0.6785" x2="-0.9715" y2="-0.6635" layer="21"/>
<rectangle x1="0.8435" y1="-0.6785" x2="2.0135" y2="-0.6635" layer="21"/>
<rectangle x1="-2.1115" y1="-0.6635" x2="-1.0165" y2="-0.6485" layer="21"/>
<rectangle x1="0.8885" y1="-0.6635" x2="1.9835" y2="-0.6485" layer="21"/>
<rectangle x1="-2.0815" y1="-0.6485" x2="-1.0615" y2="-0.6335" layer="21"/>
<rectangle x1="0.9335" y1="-0.6485" x2="1.9535" y2="-0.6335" layer="21"/>
<rectangle x1="-2.0515" y1="-0.6335" x2="-1.1065" y2="-0.6185" layer="21"/>
<rectangle x1="0.9785" y1="-0.6335" x2="1.9235" y2="-0.6185" layer="21"/>
<rectangle x1="-2.0215" y1="-0.6185" x2="-1.1515" y2="-0.6035" layer="21"/>
<rectangle x1="1.0235" y1="-0.6185" x2="1.8935" y2="-0.6035" layer="21"/>
<rectangle x1="-1.9915" y1="-0.6035" x2="-1.1815" y2="-0.5885" layer="21"/>
<rectangle x1="1.0535" y1="-0.6035" x2="1.8635" y2="-0.5885" layer="21"/>
<rectangle x1="-1.9615" y1="-0.5885" x2="-1.2265" y2="-0.5735" layer="21"/>
<rectangle x1="1.0985" y1="-0.5885" x2="1.8335" y2="-0.5735" layer="21"/>
<rectangle x1="-1.9315" y1="-0.5735" x2="-1.2715" y2="-0.5585" layer="21"/>
<rectangle x1="1.1435" y1="-0.5735" x2="1.8185" y2="-0.5585" layer="21"/>
<rectangle x1="-1.9165" y1="-0.5585" x2="-1.3015" y2="-0.5435" layer="21"/>
<rectangle x1="1.1885" y1="-0.5585" x2="1.7885" y2="-0.5435" layer="21"/>
<rectangle x1="-1.8865" y1="-0.5435" x2="-1.3465" y2="-0.5285" layer="21"/>
<rectangle x1="1.2185" y1="-0.5435" x2="1.7585" y2="-0.5285" layer="21"/>
<rectangle x1="-1.8565" y1="-0.5285" x2="-1.3915" y2="-0.5135" layer="21"/>
<rectangle x1="1.2635" y1="-0.5285" x2="1.7285" y2="-0.5135" layer="21"/>
<rectangle x1="-1.8265" y1="-0.5135" x2="-1.4215" y2="-0.4985" layer="21"/>
<rectangle x1="1.2935" y1="-0.5135" x2="1.6985" y2="-0.4985" layer="21"/>
<rectangle x1="-1.7965" y1="-0.4985" x2="-1.4665" y2="-0.4835" layer="21"/>
<rectangle x1="1.3385" y1="-0.4985" x2="1.6685" y2="-0.4835" layer="21"/>
<rectangle x1="-1.7665" y1="-0.4835" x2="-1.5115" y2="-0.4685" layer="21"/>
<rectangle x1="1.3835" y1="-0.4835" x2="1.6385" y2="-0.4685" layer="21"/>
<rectangle x1="-1.7365" y1="-0.4685" x2="-1.5415" y2="-0.4535" layer="21"/>
<rectangle x1="1.4135" y1="-0.4685" x2="1.6085" y2="-0.4535" layer="21"/>
<rectangle x1="-1.7065" y1="-0.4535" x2="-1.5865" y2="-0.4385" layer="21"/>
<rectangle x1="1.4585" y1="-0.4535" x2="1.5785" y2="-0.4385" layer="21"/>
<rectangle x1="-1.6765" y1="-0.4385" x2="-1.6165" y2="-0.4235" layer="21"/>
<rectangle x1="1.4885" y1="-0.4385" x2="1.5485" y2="-0.4235" layer="21"/>
<rectangle x1="1.5035" y1="0.4015" x2="1.7735" y2="0.4165" layer="21"/>
<rectangle x1="1.3985" y1="0.4165" x2="1.8785" y2="0.4315" layer="21"/>
<rectangle x1="1.3385" y1="0.4315" x2="1.9385" y2="0.4465" layer="21"/>
<rectangle x1="-2.5165" y1="0.4465" x2="-1.5415" y2="0.4615" layer="21"/>
<rectangle x1="-0.2965" y1="0.4465" x2="0.1535" y2="0.4615" layer="21"/>
<rectangle x1="1.2785" y1="0.4465" x2="1.9835" y2="0.4615" layer="21"/>
<rectangle x1="-2.5315" y1="0.4615" x2="-1.4665" y2="0.4765" layer="21"/>
<rectangle x1="-0.3115" y1="0.4615" x2="0.1685" y2="0.4765" layer="21"/>
<rectangle x1="1.2485" y1="0.4615" x2="2.0285" y2="0.4765" layer="21"/>
<rectangle x1="-2.5315" y1="0.4765" x2="-1.4065" y2="0.4915" layer="21"/>
<rectangle x1="-0.3265" y1="0.4765" x2="0.1685" y2="0.4915" layer="21"/>
<rectangle x1="1.2185" y1="0.4765" x2="2.0585" y2="0.4915" layer="21"/>
<rectangle x1="-2.5315" y1="0.4915" x2="-1.3765" y2="0.5065" layer="21"/>
<rectangle x1="-0.3265" y1="0.4915" x2="0.1685" y2="0.5065" layer="21"/>
<rectangle x1="1.1885" y1="0.4915" x2="2.0885" y2="0.5065" layer="21"/>
<rectangle x1="-2.5315" y1="0.5065" x2="-1.3315" y2="0.5215" layer="21"/>
<rectangle x1="-0.3265" y1="0.5065" x2="0.1685" y2="0.5215" layer="21"/>
<rectangle x1="1.1585" y1="0.5065" x2="2.1185" y2="0.5215" layer="21"/>
<rectangle x1="-2.5315" y1="0.5215" x2="-1.3015" y2="0.5365" layer="21"/>
<rectangle x1="-0.3265" y1="0.5215" x2="0.1685" y2="0.5365" layer="21"/>
<rectangle x1="1.1435" y1="0.5215" x2="2.1335" y2="0.5365" layer="21"/>
<rectangle x1="-2.5315" y1="0.5365" x2="-1.2865" y2="0.5515" layer="21"/>
<rectangle x1="-0.3265" y1="0.5365" x2="0.1685" y2="0.5515" layer="21"/>
<rectangle x1="1.1135" y1="0.5365" x2="2.1485" y2="0.5515" layer="21"/>
<rectangle x1="-2.5315" y1="0.5515" x2="-1.2565" y2="0.5665" layer="21"/>
<rectangle x1="-0.3265" y1="0.5515" x2="0.1685" y2="0.5665" layer="21"/>
<rectangle x1="1.0985" y1="0.5515" x2="2.1785" y2="0.5665" layer="21"/>
<rectangle x1="-2.5315" y1="0.5665" x2="-1.2415" y2="0.5815" layer="21"/>
<rectangle x1="-0.3265" y1="0.5665" x2="0.1685" y2="0.5815" layer="21"/>
<rectangle x1="1.0835" y1="0.5665" x2="2.1935" y2="0.5815" layer="21"/>
<rectangle x1="-2.5315" y1="0.5815" x2="-1.2265" y2="0.5965" layer="21"/>
<rectangle x1="-0.3265" y1="0.5815" x2="0.1685" y2="0.5965" layer="21"/>
<rectangle x1="1.0685" y1="0.5815" x2="2.2085" y2="0.5965" layer="21"/>
<rectangle x1="-2.5315" y1="0.5965" x2="-1.2115" y2="0.6115" layer="21"/>
<rectangle x1="-0.3265" y1="0.5965" x2="0.1685" y2="0.6115" layer="21"/>
<rectangle x1="1.0535" y1="0.5965" x2="2.2235" y2="0.6115" layer="21"/>
<rectangle x1="-2.5315" y1="0.6115" x2="-1.1965" y2="0.6265" layer="21"/>
<rectangle x1="-0.3265" y1="0.6115" x2="0.1685" y2="0.6265" layer="21"/>
<rectangle x1="1.0385" y1="0.6115" x2="2.2385" y2="0.6265" layer="21"/>
<rectangle x1="-2.5315" y1="0.6265" x2="-1.1815" y2="0.6415" layer="21"/>
<rectangle x1="-0.3265" y1="0.6265" x2="0.1685" y2="0.6415" layer="21"/>
<rectangle x1="1.0235" y1="0.6265" x2="2.2385" y2="0.6415" layer="21"/>
<rectangle x1="-2.5315" y1="0.6415" x2="-1.1665" y2="0.6565" layer="21"/>
<rectangle x1="-0.3265" y1="0.6415" x2="0.1685" y2="0.6565" layer="21"/>
<rectangle x1="1.0085" y1="0.6415" x2="2.2535" y2="0.6565" layer="21"/>
<rectangle x1="-2.5315" y1="0.6565" x2="-1.1665" y2="0.6715" layer="21"/>
<rectangle x1="-0.3265" y1="0.6565" x2="0.1685" y2="0.6715" layer="21"/>
<rectangle x1="0.9935" y1="0.6565" x2="2.2685" y2="0.6715" layer="21"/>
<rectangle x1="-2.5315" y1="0.6715" x2="-1.1515" y2="0.6865" layer="21"/>
<rectangle x1="-0.3265" y1="0.6715" x2="0.1685" y2="0.6865" layer="21"/>
<rectangle x1="0.9935" y1="0.6715" x2="2.2835" y2="0.6865" layer="21"/>
<rectangle x1="-2.5315" y1="0.6865" x2="-1.1365" y2="0.7015" layer="21"/>
<rectangle x1="-0.3265" y1="0.6865" x2="0.1685" y2="0.7015" layer="21"/>
<rectangle x1="0.9785" y1="0.6865" x2="2.2835" y2="0.7015" layer="21"/>
<rectangle x1="-2.5315" y1="0.7015" x2="-1.1365" y2="0.7165" layer="21"/>
<rectangle x1="-0.3265" y1="0.7015" x2="0.1685" y2="0.7165" layer="21"/>
<rectangle x1="0.9785" y1="0.7015" x2="2.2985" y2="0.7165" layer="21"/>
<rectangle x1="-2.5315" y1="0.7165" x2="-1.1215" y2="0.7315" layer="21"/>
<rectangle x1="-0.3265" y1="0.7165" x2="0.1685" y2="0.7315" layer="21"/>
<rectangle x1="0.9635" y1="0.7165" x2="2.2985" y2="0.7315" layer="21"/>
<rectangle x1="-2.5315" y1="0.7315" x2="-1.1215" y2="0.7465" layer="21"/>
<rectangle x1="-0.3265" y1="0.7315" x2="0.1685" y2="0.7465" layer="21"/>
<rectangle x1="0.9635" y1="0.7315" x2="1.5485" y2="0.7465" layer="21"/>
<rectangle x1="1.7285" y1="0.7315" x2="2.3135" y2="0.7465" layer="21"/>
<rectangle x1="-2.5315" y1="0.7465" x2="-1.1065" y2="0.7615" layer="21"/>
<rectangle x1="-0.3265" y1="0.7465" x2="0.1685" y2="0.7615" layer="21"/>
<rectangle x1="0.9485" y1="0.7465" x2="1.5035" y2="0.7615" layer="21"/>
<rectangle x1="1.7735" y1="0.7465" x2="2.3135" y2="0.7615" layer="21"/>
<rectangle x1="-2.5315" y1="0.7615" x2="-1.1065" y2="0.7765" layer="21"/>
<rectangle x1="-0.3265" y1="0.7615" x2="0.1685" y2="0.7765" layer="21"/>
<rectangle x1="0.9485" y1="0.7615" x2="1.4735" y2="0.7765" layer="21"/>
<rectangle x1="1.8035" y1="0.7615" x2="2.3285" y2="0.7765" layer="21"/>
<rectangle x1="-2.5315" y1="0.7765" x2="-2.0815" y2="0.7915" layer="21"/>
<rectangle x1="-1.7065" y1="0.7765" x2="-1.0915" y2="0.7915" layer="21"/>
<rectangle x1="-0.3265" y1="0.7765" x2="0.1685" y2="0.7915" layer="21"/>
<rectangle x1="0.9335" y1="0.7765" x2="1.4585" y2="0.7915" layer="21"/>
<rectangle x1="1.8185" y1="0.7765" x2="2.3285" y2="0.7915" layer="21"/>
<rectangle x1="-2.5315" y1="0.7915" x2="-2.0815" y2="0.8065" layer="21"/>
<rectangle x1="-1.6615" y1="0.7915" x2="-1.0915" y2="0.8065" layer="21"/>
<rectangle x1="-0.3265" y1="0.7915" x2="0.1685" y2="0.8065" layer="21"/>
<rectangle x1="0.9335" y1="0.7915" x2="1.4435" y2="0.8065" layer="21"/>
<rectangle x1="1.8335" y1="0.7915" x2="2.3285" y2="0.8065" layer="21"/>
<rectangle x1="-2.5315" y1="0.8065" x2="-2.0815" y2="0.8215" layer="21"/>
<rectangle x1="-1.6315" y1="0.8065" x2="-1.0915" y2="0.8215" layer="21"/>
<rectangle x1="-0.3265" y1="0.8065" x2="0.1685" y2="0.8215" layer="21"/>
<rectangle x1="0.9335" y1="0.8065" x2="1.4285" y2="0.8215" layer="21"/>
<rectangle x1="1.8485" y1="0.8065" x2="2.3435" y2="0.8215" layer="21"/>
<rectangle x1="-2.5315" y1="0.8215" x2="-2.0815" y2="0.8365" layer="21"/>
<rectangle x1="-1.6015" y1="0.8215" x2="-1.0765" y2="0.8365" layer="21"/>
<rectangle x1="-0.3265" y1="0.8215" x2="0.1685" y2="0.8365" layer="21"/>
<rectangle x1="0.9185" y1="0.8215" x2="1.4135" y2="0.8365" layer="21"/>
<rectangle x1="1.8635" y1="0.8215" x2="2.3435" y2="0.8365" layer="21"/>
<rectangle x1="-2.5315" y1="0.8365" x2="-2.0815" y2="0.8515" layer="21"/>
<rectangle x1="-1.5865" y1="0.8365" x2="-1.0765" y2="0.8515" layer="21"/>
<rectangle x1="-0.3265" y1="0.8365" x2="0.1685" y2="0.8515" layer="21"/>
<rectangle x1="0.9185" y1="0.8365" x2="1.3985" y2="0.8515" layer="21"/>
<rectangle x1="1.8635" y1="0.8365" x2="2.3435" y2="0.8515" layer="21"/>
<rectangle x1="-2.5315" y1="0.8515" x2="-2.0815" y2="0.8665" layer="21"/>
<rectangle x1="-1.5715" y1="0.8515" x2="-1.0765" y2="0.8665" layer="21"/>
<rectangle x1="-0.3265" y1="0.8515" x2="0.1685" y2="0.8665" layer="21"/>
<rectangle x1="0.9185" y1="0.8515" x2="1.3985" y2="0.8665" layer="21"/>
<rectangle x1="1.8785" y1="0.8515" x2="2.3585" y2="0.8665" layer="21"/>
<rectangle x1="-2.5315" y1="0.8665" x2="-2.0815" y2="0.8815" layer="21"/>
<rectangle x1="-1.5715" y1="0.8665" x2="-1.0615" y2="0.8815" layer="21"/>
<rectangle x1="-0.3265" y1="0.8665" x2="0.1685" y2="0.8815" layer="21"/>
<rectangle x1="0.9185" y1="0.8665" x2="1.3835" y2="0.8815" layer="21"/>
<rectangle x1="1.8935" y1="0.8665" x2="2.3585" y2="0.8815" layer="21"/>
<rectangle x1="-2.5315" y1="0.8815" x2="-2.0815" y2="0.8965" layer="21"/>
<rectangle x1="-1.5565" y1="0.8815" x2="-1.0615" y2="0.8965" layer="21"/>
<rectangle x1="-0.3265" y1="0.8815" x2="0.1685" y2="0.8965" layer="21"/>
<rectangle x1="0.9035" y1="0.8815" x2="1.3835" y2="0.8965" layer="21"/>
<rectangle x1="1.8935" y1="0.8815" x2="2.3585" y2="0.8965" layer="21"/>
<rectangle x1="-2.5315" y1="0.8965" x2="-2.0815" y2="0.9115" layer="21"/>
<rectangle x1="-1.5415" y1="0.8965" x2="-1.0615" y2="0.9115" layer="21"/>
<rectangle x1="-0.3265" y1="0.8965" x2="0.1685" y2="0.9115" layer="21"/>
<rectangle x1="0.9035" y1="0.8965" x2="1.3835" y2="0.9115" layer="21"/>
<rectangle x1="1.8935" y1="0.8965" x2="2.3585" y2="0.9115" layer="21"/>
<rectangle x1="-2.5315" y1="0.9115" x2="-2.0815" y2="0.9265" layer="21"/>
<rectangle x1="-1.5415" y1="0.9115" x2="-1.0615" y2="0.9265" layer="21"/>
<rectangle x1="-0.3265" y1="0.9115" x2="0.1685" y2="0.9265" layer="21"/>
<rectangle x1="0.9035" y1="0.9115" x2="1.3685" y2="0.9265" layer="21"/>
<rectangle x1="1.9085" y1="0.9115" x2="2.3735" y2="0.9265" layer="21"/>
<rectangle x1="-2.5315" y1="0.9265" x2="-2.0815" y2="0.9415" layer="21"/>
<rectangle x1="-1.5265" y1="0.9265" x2="-1.0615" y2="0.9415" layer="21"/>
<rectangle x1="-0.3265" y1="0.9265" x2="0.1685" y2="0.9415" layer="21"/>
<rectangle x1="0.9035" y1="0.9265" x2="1.3685" y2="0.9415" layer="21"/>
<rectangle x1="1.9085" y1="0.9265" x2="2.3735" y2="0.9415" layer="21"/>
<rectangle x1="-2.5315" y1="0.9415" x2="-2.0815" y2="0.9565" layer="21"/>
<rectangle x1="-1.5265" y1="0.9415" x2="-1.0465" y2="0.9565" layer="21"/>
<rectangle x1="-0.3265" y1="0.9415" x2="0.1685" y2="0.9565" layer="21"/>
<rectangle x1="0.9035" y1="0.9415" x2="1.3685" y2="0.9565" layer="21"/>
<rectangle x1="1.9085" y1="0.9415" x2="2.3735" y2="0.9565" layer="21"/>
<rectangle x1="-2.5315" y1="0.9565" x2="-2.0815" y2="0.9715" layer="21"/>
<rectangle x1="-1.5265" y1="0.9565" x2="-1.0465" y2="0.9715" layer="21"/>
<rectangle x1="-0.3265" y1="0.9565" x2="0.1685" y2="0.9715" layer="21"/>
<rectangle x1="0.9035" y1="0.9565" x2="1.3535" y2="0.9715" layer="21"/>
<rectangle x1="1.9085" y1="0.9565" x2="2.3735" y2="0.9715" layer="21"/>
<rectangle x1="-2.5315" y1="0.9715" x2="-2.0815" y2="0.9865" layer="21"/>
<rectangle x1="-1.5115" y1="0.9715" x2="-1.0465" y2="0.9865" layer="21"/>
<rectangle x1="-0.3265" y1="0.9715" x2="0.1685" y2="0.9865" layer="21"/>
<rectangle x1="0.9035" y1="0.9715" x2="1.3535" y2="0.9865" layer="21"/>
<rectangle x1="1.9235" y1="0.9715" x2="2.3735" y2="0.9865" layer="21"/>
<rectangle x1="-2.5315" y1="0.9865" x2="-2.0815" y2="1.0015" layer="21"/>
<rectangle x1="-1.5115" y1="0.9865" x2="-1.0465" y2="1.0015" layer="21"/>
<rectangle x1="-0.3265" y1="0.9865" x2="0.1685" y2="1.0015" layer="21"/>
<rectangle x1="0.8885" y1="0.9865" x2="1.3535" y2="1.0015" layer="21"/>
<rectangle x1="1.9235" y1="0.9865" x2="2.3735" y2="1.0015" layer="21"/>
<rectangle x1="-2.5315" y1="1.0015" x2="-2.0815" y2="1.0165" layer="21"/>
<rectangle x1="-1.5115" y1="1.0015" x2="-1.0465" y2="1.0165" layer="21"/>
<rectangle x1="-0.3265" y1="1.0015" x2="0.1685" y2="1.0165" layer="21"/>
<rectangle x1="0.8885" y1="1.0015" x2="1.3535" y2="1.0165" layer="21"/>
<rectangle x1="1.9235" y1="1.0015" x2="2.3885" y2="1.0165" layer="21"/>
<rectangle x1="-2.5315" y1="1.0165" x2="-2.0815" y2="1.0315" layer="21"/>
<rectangle x1="-1.5115" y1="1.0165" x2="-1.0465" y2="1.0315" layer="21"/>
<rectangle x1="-0.3265" y1="1.0165" x2="0.1685" y2="1.0315" layer="21"/>
<rectangle x1="0.8885" y1="1.0165" x2="1.3535" y2="1.0315" layer="21"/>
<rectangle x1="1.9235" y1="1.0165" x2="2.3885" y2="1.0315" layer="21"/>
<rectangle x1="-2.5315" y1="1.0315" x2="-2.0815" y2="1.0465" layer="21"/>
<rectangle x1="-1.5115" y1="1.0315" x2="-1.0315" y2="1.0465" layer="21"/>
<rectangle x1="-0.3265" y1="1.0315" x2="0.1685" y2="1.0465" layer="21"/>
<rectangle x1="0.8885" y1="1.0315" x2="1.3535" y2="1.0465" layer="21"/>
<rectangle x1="1.9235" y1="1.0315" x2="2.3885" y2="1.0465" layer="21"/>
<rectangle x1="-2.5315" y1="1.0465" x2="-2.0815" y2="1.0615" layer="21"/>
<rectangle x1="-1.4965" y1="1.0465" x2="-1.0315" y2="1.0615" layer="21"/>
<rectangle x1="-0.3265" y1="1.0465" x2="0.1685" y2="1.0615" layer="21"/>
<rectangle x1="0.8885" y1="1.0465" x2="1.3535" y2="1.0615" layer="21"/>
<rectangle x1="1.9235" y1="1.0465" x2="2.3885" y2="1.0615" layer="21"/>
<rectangle x1="-2.5315" y1="1.0615" x2="-2.0815" y2="1.0765" layer="21"/>
<rectangle x1="-1.4965" y1="1.0615" x2="-1.0315" y2="1.0765" layer="21"/>
<rectangle x1="-0.3265" y1="1.0615" x2="0.1685" y2="1.0765" layer="21"/>
<rectangle x1="0.8885" y1="1.0615" x2="1.3385" y2="1.0765" layer="21"/>
<rectangle x1="1.9235" y1="1.0615" x2="2.3885" y2="1.0765" layer="21"/>
<rectangle x1="-2.5315" y1="1.0765" x2="-2.0815" y2="1.0915" layer="21"/>
<rectangle x1="-1.4965" y1="1.0765" x2="-1.0315" y2="1.0915" layer="21"/>
<rectangle x1="-0.3265" y1="1.0765" x2="0.1685" y2="1.0915" layer="21"/>
<rectangle x1="0.8885" y1="1.0765" x2="1.3385" y2="1.0915" layer="21"/>
<rectangle x1="1.9235" y1="1.0765" x2="2.3885" y2="1.0915" layer="21"/>
<rectangle x1="-2.5315" y1="1.0915" x2="-2.0815" y2="1.1065" layer="21"/>
<rectangle x1="-1.4965" y1="1.0915" x2="-1.0315" y2="1.1065" layer="21"/>
<rectangle x1="-0.3265" y1="1.0915" x2="0.1685" y2="1.1065" layer="21"/>
<rectangle x1="0.8885" y1="1.0915" x2="1.3385" y2="1.1065" layer="21"/>
<rectangle x1="1.9385" y1="1.0915" x2="2.3885" y2="1.1065" layer="21"/>
<rectangle x1="-2.5315" y1="1.1065" x2="-2.0815" y2="1.1215" layer="21"/>
<rectangle x1="-1.4965" y1="1.1065" x2="-1.0315" y2="1.1215" layer="21"/>
<rectangle x1="-0.3265" y1="1.1065" x2="0.1685" y2="1.1215" layer="21"/>
<rectangle x1="0.8885" y1="1.1065" x2="1.3385" y2="1.1215" layer="21"/>
<rectangle x1="1.9385" y1="1.1065" x2="2.3885" y2="1.1215" layer="21"/>
<rectangle x1="-2.5315" y1="1.1215" x2="-2.0815" y2="1.1365" layer="21"/>
<rectangle x1="-1.4965" y1="1.1215" x2="-1.0315" y2="1.1365" layer="21"/>
<rectangle x1="-0.3265" y1="1.1215" x2="0.1685" y2="1.1365" layer="21"/>
<rectangle x1="0.8885" y1="1.1215" x2="1.3385" y2="1.1365" layer="21"/>
<rectangle x1="1.9385" y1="1.1215" x2="2.3885" y2="1.1365" layer="21"/>
<rectangle x1="-2.5315" y1="1.1365" x2="-2.0815" y2="1.1515" layer="21"/>
<rectangle x1="-1.4965" y1="1.1365" x2="-1.0315" y2="1.1515" layer="21"/>
<rectangle x1="-0.3265" y1="1.1365" x2="0.1685" y2="1.1515" layer="21"/>
<rectangle x1="0.8885" y1="1.1365" x2="1.3385" y2="1.1515" layer="21"/>
<rectangle x1="1.9385" y1="1.1365" x2="2.3885" y2="1.1515" layer="21"/>
<rectangle x1="-2.5315" y1="1.1515" x2="-2.0815" y2="1.1665" layer="21"/>
<rectangle x1="-1.4965" y1="1.1515" x2="-1.0315" y2="1.1665" layer="21"/>
<rectangle x1="-0.3265" y1="1.1515" x2="0.1685" y2="1.1665" layer="21"/>
<rectangle x1="0.8885" y1="1.1515" x2="1.3385" y2="1.1665" layer="21"/>
<rectangle x1="1.9385" y1="1.1515" x2="2.3885" y2="1.1665" layer="21"/>
<rectangle x1="-2.5315" y1="1.1665" x2="-2.0815" y2="1.1815" layer="21"/>
<rectangle x1="-1.4965" y1="1.1665" x2="-1.0315" y2="1.1815" layer="21"/>
<rectangle x1="-0.3265" y1="1.1665" x2="0.1685" y2="1.1815" layer="21"/>
<rectangle x1="0.8885" y1="1.1665" x2="1.3385" y2="1.1815" layer="21"/>
<rectangle x1="1.9385" y1="1.1665" x2="2.3885" y2="1.1815" layer="21"/>
<rectangle x1="-2.5315" y1="1.1815" x2="-2.0815" y2="1.1965" layer="21"/>
<rectangle x1="-1.4965" y1="1.1815" x2="-1.0315" y2="1.1965" layer="21"/>
<rectangle x1="-0.3265" y1="1.1815" x2="0.1685" y2="1.1965" layer="21"/>
<rectangle x1="0.8885" y1="1.1815" x2="1.3385" y2="1.1965" layer="21"/>
<rectangle x1="1.9385" y1="1.1815" x2="2.3885" y2="1.1965" layer="21"/>
<rectangle x1="-2.5315" y1="1.1965" x2="-2.0815" y2="1.2115" layer="21"/>
<rectangle x1="-1.4965" y1="1.1965" x2="-1.0315" y2="1.2115" layer="21"/>
<rectangle x1="-0.3265" y1="1.1965" x2="0.1685" y2="1.2115" layer="21"/>
<rectangle x1="0.8885" y1="1.1965" x2="1.3385" y2="1.2115" layer="21"/>
<rectangle x1="1.9385" y1="1.1965" x2="2.3885" y2="1.2115" layer="21"/>
<rectangle x1="-2.5315" y1="1.2115" x2="-2.0815" y2="1.2265" layer="21"/>
<rectangle x1="-1.4815" y1="1.2115" x2="-1.0315" y2="1.2265" layer="21"/>
<rectangle x1="-0.3265" y1="1.2115" x2="0.1685" y2="1.2265" layer="21"/>
<rectangle x1="0.8885" y1="1.2115" x2="1.3385" y2="1.2265" layer="21"/>
<rectangle x1="1.9385" y1="1.2115" x2="2.3885" y2="1.2265" layer="21"/>
<rectangle x1="-2.5315" y1="1.2265" x2="-2.0815" y2="1.2415" layer="21"/>
<rectangle x1="-1.4815" y1="1.2265" x2="-1.0315" y2="1.2415" layer="21"/>
<rectangle x1="-0.3265" y1="1.2265" x2="0.1685" y2="1.2415" layer="21"/>
<rectangle x1="0.8885" y1="1.2265" x2="1.3385" y2="1.2415" layer="21"/>
<rectangle x1="1.9385" y1="1.2265" x2="2.3885" y2="1.2415" layer="21"/>
<rectangle x1="-2.5315" y1="1.2415" x2="-2.0815" y2="1.2565" layer="21"/>
<rectangle x1="-1.4815" y1="1.2415" x2="-1.0165" y2="1.2565" layer="21"/>
<rectangle x1="-0.3265" y1="1.2415" x2="0.1685" y2="1.2565" layer="21"/>
<rectangle x1="0.8885" y1="1.2415" x2="1.3385" y2="1.2565" layer="21"/>
<rectangle x1="1.9385" y1="1.2415" x2="2.3885" y2="1.2565" layer="21"/>
<rectangle x1="-2.5315" y1="1.2565" x2="-2.0815" y2="1.2715" layer="21"/>
<rectangle x1="-1.4815" y1="1.2565" x2="-1.0165" y2="1.2715" layer="21"/>
<rectangle x1="-0.3265" y1="1.2565" x2="0.1685" y2="1.2715" layer="21"/>
<rectangle x1="0.8885" y1="1.2565" x2="1.3385" y2="1.2715" layer="21"/>
<rectangle x1="1.9385" y1="1.2565" x2="2.3885" y2="1.2715" layer="21"/>
<rectangle x1="-2.5315" y1="1.2715" x2="-2.0815" y2="1.2865" layer="21"/>
<rectangle x1="-1.4815" y1="1.2715" x2="-1.0165" y2="1.2865" layer="21"/>
<rectangle x1="-0.3265" y1="1.2715" x2="0.1685" y2="1.2865" layer="21"/>
<rectangle x1="0.8885" y1="1.2715" x2="1.3385" y2="1.2865" layer="21"/>
<rectangle x1="1.9385" y1="1.2715" x2="2.3885" y2="1.2865" layer="21"/>
<rectangle x1="-2.5315" y1="1.2865" x2="-2.0815" y2="1.3015" layer="21"/>
<rectangle x1="-1.4815" y1="1.2865" x2="-1.0165" y2="1.3015" layer="21"/>
<rectangle x1="-0.3265" y1="1.2865" x2="0.1685" y2="1.3015" layer="21"/>
<rectangle x1="0.8885" y1="1.2865" x2="1.3385" y2="1.3015" layer="21"/>
<rectangle x1="1.9385" y1="1.2865" x2="2.3885" y2="1.3015" layer="21"/>
<rectangle x1="-2.5315" y1="1.3015" x2="-2.0815" y2="1.3165" layer="21"/>
<rectangle x1="-1.4815" y1="1.3015" x2="-1.0165" y2="1.3165" layer="21"/>
<rectangle x1="-0.3265" y1="1.3015" x2="0.1685" y2="1.3165" layer="21"/>
<rectangle x1="0.8885" y1="1.3015" x2="1.3385" y2="1.3165" layer="21"/>
<rectangle x1="1.9385" y1="1.3015" x2="2.3885" y2="1.3165" layer="21"/>
<rectangle x1="-2.5315" y1="1.3165" x2="-2.0815" y2="1.3315" layer="21"/>
<rectangle x1="-1.4815" y1="1.3165" x2="-1.0165" y2="1.3315" layer="21"/>
<rectangle x1="-0.3265" y1="1.3165" x2="0.1685" y2="1.3315" layer="21"/>
<rectangle x1="0.8885" y1="1.3165" x2="1.3385" y2="1.3315" layer="21"/>
<rectangle x1="1.9385" y1="1.3165" x2="2.3885" y2="1.3315" layer="21"/>
<rectangle x1="-2.5315" y1="1.3315" x2="-2.0815" y2="1.3465" layer="21"/>
<rectangle x1="-1.4815" y1="1.3315" x2="-1.0165" y2="1.3465" layer="21"/>
<rectangle x1="-0.3265" y1="1.3315" x2="0.1685" y2="1.3465" layer="21"/>
<rectangle x1="0.8885" y1="1.3315" x2="1.3385" y2="1.3465" layer="21"/>
<rectangle x1="1.9385" y1="1.3315" x2="2.3885" y2="1.3465" layer="21"/>
<rectangle x1="-2.5315" y1="1.3465" x2="-2.0815" y2="1.3615" layer="21"/>
<rectangle x1="-1.4815" y1="1.3465" x2="-1.0165" y2="1.3615" layer="21"/>
<rectangle x1="-0.3265" y1="1.3465" x2="0.1685" y2="1.3615" layer="21"/>
<rectangle x1="0.8885" y1="1.3465" x2="1.3385" y2="1.3615" layer="21"/>
<rectangle x1="1.9385" y1="1.3465" x2="2.3885" y2="1.3615" layer="21"/>
<rectangle x1="-2.5315" y1="1.3615" x2="-2.0815" y2="1.3765" layer="21"/>
<rectangle x1="-1.4815" y1="1.3615" x2="-1.0165" y2="1.3765" layer="21"/>
<rectangle x1="-0.3265" y1="1.3615" x2="0.1685" y2="1.3765" layer="21"/>
<rectangle x1="0.8885" y1="1.3615" x2="1.3385" y2="1.3765" layer="21"/>
<rectangle x1="1.9385" y1="1.3615" x2="2.3885" y2="1.3765" layer="21"/>
<rectangle x1="-2.5315" y1="1.3765" x2="-2.0815" y2="1.3915" layer="21"/>
<rectangle x1="-1.4815" y1="1.3765" x2="-1.0165" y2="1.3915" layer="21"/>
<rectangle x1="-0.3265" y1="1.3765" x2="0.1685" y2="1.3915" layer="21"/>
<rectangle x1="0.8885" y1="1.3765" x2="1.3385" y2="1.3915" layer="21"/>
<rectangle x1="1.9385" y1="1.3765" x2="2.3885" y2="1.3915" layer="21"/>
<rectangle x1="-2.5315" y1="1.3915" x2="-2.0815" y2="1.4065" layer="21"/>
<rectangle x1="-1.4815" y1="1.3915" x2="-1.0165" y2="1.4065" layer="21"/>
<rectangle x1="-0.3265" y1="1.3915" x2="0.1685" y2="1.4065" layer="21"/>
<rectangle x1="0.8885" y1="1.3915" x2="1.3385" y2="1.4065" layer="21"/>
<rectangle x1="1.9385" y1="1.3915" x2="2.3885" y2="1.4065" layer="21"/>
<rectangle x1="-2.5315" y1="1.4065" x2="-2.0815" y2="1.4215" layer="21"/>
<rectangle x1="-1.4815" y1="1.4065" x2="-1.0165" y2="1.4215" layer="21"/>
<rectangle x1="-0.3265" y1="1.4065" x2="0.1685" y2="1.4215" layer="21"/>
<rectangle x1="0.8885" y1="1.4065" x2="1.3385" y2="1.4215" layer="21"/>
<rectangle x1="1.9385" y1="1.4065" x2="2.3885" y2="1.4215" layer="21"/>
<rectangle x1="-2.5315" y1="1.4215" x2="-2.0815" y2="1.4365" layer="21"/>
<rectangle x1="-1.4815" y1="1.4215" x2="-1.0165" y2="1.4365" layer="21"/>
<rectangle x1="-0.3265" y1="1.4215" x2="0.1685" y2="1.4365" layer="21"/>
<rectangle x1="0.8885" y1="1.4215" x2="1.3385" y2="1.4365" layer="21"/>
<rectangle x1="1.9385" y1="1.4215" x2="2.3885" y2="1.4365" layer="21"/>
<rectangle x1="-2.5315" y1="1.4365" x2="-2.0815" y2="1.4515" layer="21"/>
<rectangle x1="-1.4815" y1="1.4365" x2="-1.0165" y2="1.4515" layer="21"/>
<rectangle x1="-0.3265" y1="1.4365" x2="0.1685" y2="1.4515" layer="21"/>
<rectangle x1="0.8885" y1="1.4365" x2="1.3385" y2="1.4515" layer="21"/>
<rectangle x1="1.9385" y1="1.4365" x2="2.3885" y2="1.4515" layer="21"/>
<rectangle x1="-2.5315" y1="1.4515" x2="-2.0815" y2="1.4665" layer="21"/>
<rectangle x1="-1.4815" y1="1.4515" x2="-1.0165" y2="1.4665" layer="21"/>
<rectangle x1="-0.3265" y1="1.4515" x2="0.1685" y2="1.4665" layer="21"/>
<rectangle x1="0.8885" y1="1.4515" x2="1.3385" y2="1.4665" layer="21"/>
<rectangle x1="1.9385" y1="1.4515" x2="2.3885" y2="1.4665" layer="21"/>
<rectangle x1="-2.5315" y1="1.4665" x2="-2.0815" y2="1.4815" layer="21"/>
<rectangle x1="-1.4815" y1="1.4665" x2="-1.0165" y2="1.4815" layer="21"/>
<rectangle x1="-0.3265" y1="1.4665" x2="0.1685" y2="1.4815" layer="21"/>
<rectangle x1="0.8885" y1="1.4665" x2="1.3385" y2="1.4815" layer="21"/>
<rectangle x1="1.9385" y1="1.4665" x2="2.3885" y2="1.4815" layer="21"/>
<rectangle x1="-2.5315" y1="1.4815" x2="-2.0815" y2="1.4965" layer="21"/>
<rectangle x1="-1.4815" y1="1.4815" x2="-1.0165" y2="1.4965" layer="21"/>
<rectangle x1="-0.3265" y1="1.4815" x2="0.1685" y2="1.4965" layer="21"/>
<rectangle x1="0.8885" y1="1.4815" x2="1.3385" y2="1.4965" layer="21"/>
<rectangle x1="1.9385" y1="1.4815" x2="2.3885" y2="1.4965" layer="21"/>
<rectangle x1="-2.5315" y1="1.4965" x2="-2.0815" y2="1.5115" layer="21"/>
<rectangle x1="-1.4815" y1="1.4965" x2="-1.0165" y2="1.5115" layer="21"/>
<rectangle x1="-0.3265" y1="1.4965" x2="0.1685" y2="1.5115" layer="21"/>
<rectangle x1="0.8885" y1="1.4965" x2="1.3385" y2="1.5115" layer="21"/>
<rectangle x1="1.9385" y1="1.4965" x2="2.3885" y2="1.5115" layer="21"/>
<rectangle x1="-2.5315" y1="1.5115" x2="-2.0815" y2="1.5265" layer="21"/>
<rectangle x1="-1.4815" y1="1.5115" x2="-1.0165" y2="1.5265" layer="21"/>
<rectangle x1="-0.3265" y1="1.5115" x2="0.1685" y2="1.5265" layer="21"/>
<rectangle x1="0.8885" y1="1.5115" x2="1.3385" y2="1.5265" layer="21"/>
<rectangle x1="1.9385" y1="1.5115" x2="2.3885" y2="1.5265" layer="21"/>
<rectangle x1="-2.5315" y1="1.5265" x2="-2.0815" y2="1.5415" layer="21"/>
<rectangle x1="-1.4815" y1="1.5265" x2="-1.0165" y2="1.5415" layer="21"/>
<rectangle x1="-0.3265" y1="1.5265" x2="0.1685" y2="1.5415" layer="21"/>
<rectangle x1="0.8885" y1="1.5265" x2="1.3385" y2="1.5415" layer="21"/>
<rectangle x1="1.9385" y1="1.5265" x2="2.3885" y2="1.5415" layer="21"/>
<rectangle x1="-2.5315" y1="1.5415" x2="-2.0815" y2="1.5565" layer="21"/>
<rectangle x1="-1.4815" y1="1.5415" x2="-1.0165" y2="1.5565" layer="21"/>
<rectangle x1="-0.3265" y1="1.5415" x2="0.1685" y2="1.5565" layer="21"/>
<rectangle x1="0.8885" y1="1.5415" x2="1.3385" y2="1.5565" layer="21"/>
<rectangle x1="1.9385" y1="1.5415" x2="2.3885" y2="1.5565" layer="21"/>
<rectangle x1="-2.5315" y1="1.5565" x2="-2.0815" y2="1.5715" layer="21"/>
<rectangle x1="-1.4815" y1="1.5565" x2="-1.0165" y2="1.5715" layer="21"/>
<rectangle x1="-0.3265" y1="1.5565" x2="0.1685" y2="1.5715" layer="21"/>
<rectangle x1="0.8885" y1="1.5565" x2="1.3385" y2="1.5715" layer="21"/>
<rectangle x1="1.9385" y1="1.5565" x2="2.3885" y2="1.5715" layer="21"/>
<rectangle x1="-2.5315" y1="1.5715" x2="-2.0815" y2="1.5865" layer="21"/>
<rectangle x1="-1.4815" y1="1.5715" x2="-1.0165" y2="1.5865" layer="21"/>
<rectangle x1="-0.3265" y1="1.5715" x2="0.1685" y2="1.5865" layer="21"/>
<rectangle x1="0.8885" y1="1.5715" x2="1.3385" y2="1.5865" layer="21"/>
<rectangle x1="1.9385" y1="1.5715" x2="2.3885" y2="1.5865" layer="21"/>
<rectangle x1="-2.5315" y1="1.5865" x2="-2.0815" y2="1.6015" layer="21"/>
<rectangle x1="-1.4815" y1="1.5865" x2="-1.0165" y2="1.6015" layer="21"/>
<rectangle x1="-0.3265" y1="1.5865" x2="0.1685" y2="1.6015" layer="21"/>
<rectangle x1="0.8885" y1="1.5865" x2="1.3385" y2="1.6015" layer="21"/>
<rectangle x1="1.9385" y1="1.5865" x2="2.3885" y2="1.6015" layer="21"/>
<rectangle x1="-2.5315" y1="1.6015" x2="-2.0815" y2="1.6165" layer="21"/>
<rectangle x1="-1.4815" y1="1.6015" x2="-1.0165" y2="1.6165" layer="21"/>
<rectangle x1="-0.3265" y1="1.6015" x2="0.1685" y2="1.6165" layer="21"/>
<rectangle x1="0.8885" y1="1.6015" x2="1.3385" y2="1.6165" layer="21"/>
<rectangle x1="1.9385" y1="1.6015" x2="2.3885" y2="1.6165" layer="21"/>
<rectangle x1="-2.5315" y1="1.6165" x2="-2.0815" y2="1.6315" layer="21"/>
<rectangle x1="-1.4815" y1="1.6165" x2="-1.0165" y2="1.6315" layer="21"/>
<rectangle x1="-0.3265" y1="1.6165" x2="0.1685" y2="1.6315" layer="21"/>
<rectangle x1="0.8885" y1="1.6165" x2="1.3385" y2="1.6315" layer="21"/>
<rectangle x1="1.9385" y1="1.6165" x2="2.3885" y2="1.6315" layer="21"/>
<rectangle x1="-2.5315" y1="1.6315" x2="-2.0815" y2="1.6465" layer="21"/>
<rectangle x1="-1.4815" y1="1.6315" x2="-1.0165" y2="1.6465" layer="21"/>
<rectangle x1="-0.3265" y1="1.6315" x2="0.1685" y2="1.6465" layer="21"/>
<rectangle x1="0.8885" y1="1.6315" x2="1.3385" y2="1.6465" layer="21"/>
<rectangle x1="1.9385" y1="1.6315" x2="2.3885" y2="1.6465" layer="21"/>
<rectangle x1="-2.5315" y1="1.6465" x2="-2.0815" y2="1.6615" layer="21"/>
<rectangle x1="-1.4815" y1="1.6465" x2="-1.0165" y2="1.6615" layer="21"/>
<rectangle x1="-0.3265" y1="1.6465" x2="0.1685" y2="1.6615" layer="21"/>
<rectangle x1="0.8885" y1="1.6465" x2="1.3385" y2="1.6615" layer="21"/>
<rectangle x1="1.9385" y1="1.6465" x2="2.3885" y2="1.6615" layer="21"/>
<rectangle x1="-2.5315" y1="1.6615" x2="-2.0815" y2="1.6765" layer="21"/>
<rectangle x1="-1.4815" y1="1.6615" x2="-1.0165" y2="1.6765" layer="21"/>
<rectangle x1="-0.3265" y1="1.6615" x2="0.1685" y2="1.6765" layer="21"/>
<rectangle x1="0.8885" y1="1.6615" x2="1.3385" y2="1.6765" layer="21"/>
<rectangle x1="1.9385" y1="1.6615" x2="2.3885" y2="1.6765" layer="21"/>
<rectangle x1="-2.5315" y1="1.6765" x2="-2.0815" y2="1.6915" layer="21"/>
<rectangle x1="-1.4815" y1="1.6765" x2="-1.0165" y2="1.6915" layer="21"/>
<rectangle x1="-0.3265" y1="1.6765" x2="0.1685" y2="1.6915" layer="21"/>
<rectangle x1="0.8885" y1="1.6765" x2="1.3385" y2="1.6915" layer="21"/>
<rectangle x1="1.9385" y1="1.6765" x2="2.3885" y2="1.6915" layer="21"/>
<rectangle x1="-2.5315" y1="1.6915" x2="-2.0815" y2="1.7065" layer="21"/>
<rectangle x1="-1.4815" y1="1.6915" x2="-1.0165" y2="1.7065" layer="21"/>
<rectangle x1="-0.3265" y1="1.6915" x2="0.1685" y2="1.7065" layer="21"/>
<rectangle x1="0.8885" y1="1.6915" x2="1.3385" y2="1.7065" layer="21"/>
<rectangle x1="1.9385" y1="1.6915" x2="2.3885" y2="1.7065" layer="21"/>
<rectangle x1="-2.5315" y1="1.7065" x2="-2.0815" y2="1.7215" layer="21"/>
<rectangle x1="-1.4815" y1="1.7065" x2="-1.0165" y2="1.7215" layer="21"/>
<rectangle x1="-0.3265" y1="1.7065" x2="0.1685" y2="1.7215" layer="21"/>
<rectangle x1="0.8885" y1="1.7065" x2="1.3385" y2="1.7215" layer="21"/>
<rectangle x1="1.9385" y1="1.7065" x2="2.3885" y2="1.7215" layer="21"/>
<rectangle x1="-2.5315" y1="1.7215" x2="-2.0815" y2="1.7365" layer="21"/>
<rectangle x1="-1.4815" y1="1.7215" x2="-1.0165" y2="1.7365" layer="21"/>
<rectangle x1="-0.3265" y1="1.7215" x2="0.1685" y2="1.7365" layer="21"/>
<rectangle x1="0.8885" y1="1.7215" x2="1.3385" y2="1.7365" layer="21"/>
<rectangle x1="1.9385" y1="1.7215" x2="2.3885" y2="1.7365" layer="21"/>
<rectangle x1="-2.5315" y1="1.7365" x2="-2.0815" y2="1.7515" layer="21"/>
<rectangle x1="-1.4815" y1="1.7365" x2="-1.0165" y2="1.7515" layer="21"/>
<rectangle x1="-0.3265" y1="1.7365" x2="0.1685" y2="1.7515" layer="21"/>
<rectangle x1="0.8885" y1="1.7365" x2="1.3385" y2="1.7515" layer="21"/>
<rectangle x1="1.9385" y1="1.7365" x2="2.3885" y2="1.7515" layer="21"/>
<rectangle x1="-2.5315" y1="1.7515" x2="-2.0815" y2="1.7665" layer="21"/>
<rectangle x1="-1.4815" y1="1.7515" x2="-1.0165" y2="1.7665" layer="21"/>
<rectangle x1="-0.3265" y1="1.7515" x2="0.1685" y2="1.7665" layer="21"/>
<rectangle x1="0.8885" y1="1.7515" x2="1.3385" y2="1.7665" layer="21"/>
<rectangle x1="1.9385" y1="1.7515" x2="2.3885" y2="1.7665" layer="21"/>
<rectangle x1="-2.5315" y1="1.7665" x2="-2.0815" y2="1.7815" layer="21"/>
<rectangle x1="-1.4815" y1="1.7665" x2="-1.0165" y2="1.7815" layer="21"/>
<rectangle x1="-0.3265" y1="1.7665" x2="0.1685" y2="1.7815" layer="21"/>
<rectangle x1="0.8885" y1="1.7665" x2="1.3385" y2="1.7815" layer="21"/>
<rectangle x1="1.9385" y1="1.7665" x2="2.3885" y2="1.7815" layer="21"/>
<rectangle x1="-2.5315" y1="1.7815" x2="-2.0815" y2="1.7965" layer="21"/>
<rectangle x1="-1.4815" y1="1.7815" x2="-1.0165" y2="1.7965" layer="21"/>
<rectangle x1="-0.3265" y1="1.7815" x2="0.1685" y2="1.7965" layer="21"/>
<rectangle x1="0.8885" y1="1.7815" x2="1.3385" y2="1.7965" layer="21"/>
<rectangle x1="1.9385" y1="1.7815" x2="2.3885" y2="1.7965" layer="21"/>
<rectangle x1="-2.5315" y1="1.7965" x2="-2.0815" y2="1.8115" layer="21"/>
<rectangle x1="-1.4815" y1="1.7965" x2="-1.0165" y2="1.8115" layer="21"/>
<rectangle x1="-0.3265" y1="1.7965" x2="0.1685" y2="1.8115" layer="21"/>
<rectangle x1="0.8885" y1="1.7965" x2="1.3385" y2="1.8115" layer="21"/>
<rectangle x1="1.9385" y1="1.7965" x2="2.3885" y2="1.8115" layer="21"/>
<rectangle x1="-2.5315" y1="1.8115" x2="-2.0815" y2="1.8265" layer="21"/>
<rectangle x1="-1.4815" y1="1.8115" x2="-1.0165" y2="1.8265" layer="21"/>
<rectangle x1="-0.3265" y1="1.8115" x2="0.1685" y2="1.8265" layer="21"/>
<rectangle x1="0.8885" y1="1.8115" x2="1.3385" y2="1.8265" layer="21"/>
<rectangle x1="1.9385" y1="1.8115" x2="2.3885" y2="1.8265" layer="21"/>
<rectangle x1="-2.5315" y1="1.8265" x2="-2.0815" y2="1.8415" layer="21"/>
<rectangle x1="-1.4815" y1="1.8265" x2="-1.0165" y2="1.8415" layer="21"/>
<rectangle x1="-0.3265" y1="1.8265" x2="0.1685" y2="1.8415" layer="21"/>
<rectangle x1="0.8885" y1="1.8265" x2="1.3385" y2="1.8415" layer="21"/>
<rectangle x1="1.9385" y1="1.8265" x2="2.3885" y2="1.8415" layer="21"/>
<rectangle x1="-2.5315" y1="1.8415" x2="-2.0815" y2="1.8565" layer="21"/>
<rectangle x1="-1.4815" y1="1.8415" x2="-1.0165" y2="1.8565" layer="21"/>
<rectangle x1="-0.3265" y1="1.8415" x2="0.1685" y2="1.8565" layer="21"/>
<rectangle x1="0.8885" y1="1.8415" x2="1.3385" y2="1.8565" layer="21"/>
<rectangle x1="1.9385" y1="1.8415" x2="2.3885" y2="1.8565" layer="21"/>
<rectangle x1="-2.5315" y1="1.8565" x2="-2.0815" y2="1.8715" layer="21"/>
<rectangle x1="-1.4815" y1="1.8565" x2="-1.0165" y2="1.8715" layer="21"/>
<rectangle x1="-0.3265" y1="1.8565" x2="0.1685" y2="1.8715" layer="21"/>
<rectangle x1="0.8885" y1="1.8565" x2="1.3385" y2="1.8715" layer="21"/>
<rectangle x1="1.9385" y1="1.8565" x2="2.3885" y2="1.8715" layer="21"/>
<rectangle x1="-2.5315" y1="1.8715" x2="-2.0815" y2="1.8865" layer="21"/>
<rectangle x1="-1.4815" y1="1.8715" x2="-1.0165" y2="1.8865" layer="21"/>
<rectangle x1="-0.3265" y1="1.8715" x2="0.1685" y2="1.8865" layer="21"/>
<rectangle x1="0.8885" y1="1.8715" x2="1.3385" y2="1.8865" layer="21"/>
<rectangle x1="1.9385" y1="1.8715" x2="2.3885" y2="1.8865" layer="21"/>
<rectangle x1="-2.5315" y1="1.8865" x2="-2.0815" y2="1.9015" layer="21"/>
<rectangle x1="-1.4815" y1="1.8865" x2="-1.0165" y2="1.9015" layer="21"/>
<rectangle x1="-0.3265" y1="1.8865" x2="0.1685" y2="1.9015" layer="21"/>
<rectangle x1="0.8885" y1="1.8865" x2="1.3385" y2="1.9015" layer="21"/>
<rectangle x1="1.9385" y1="1.8865" x2="2.3885" y2="1.9015" layer="21"/>
<rectangle x1="-2.5315" y1="1.9015" x2="-2.0815" y2="1.9165" layer="21"/>
<rectangle x1="-1.4815" y1="1.9015" x2="-1.0165" y2="1.9165" layer="21"/>
<rectangle x1="-0.3265" y1="1.9015" x2="0.1685" y2="1.9165" layer="21"/>
<rectangle x1="0.8885" y1="1.9015" x2="1.3385" y2="1.9165" layer="21"/>
<rectangle x1="1.9385" y1="1.9015" x2="2.3885" y2="1.9165" layer="21"/>
<rectangle x1="-2.5315" y1="1.9165" x2="-2.0815" y2="1.9315" layer="21"/>
<rectangle x1="-1.4815" y1="1.9165" x2="-1.0165" y2="1.9315" layer="21"/>
<rectangle x1="-0.3265" y1="1.9165" x2="0.1685" y2="1.9315" layer="21"/>
<rectangle x1="0.8885" y1="1.9165" x2="1.3385" y2="1.9315" layer="21"/>
<rectangle x1="1.9385" y1="1.9165" x2="2.3885" y2="1.9315" layer="21"/>
<rectangle x1="-2.5315" y1="1.9315" x2="-2.0815" y2="1.9465" layer="21"/>
<rectangle x1="-1.4815" y1="1.9315" x2="-1.0165" y2="1.9465" layer="21"/>
<rectangle x1="-0.3265" y1="1.9315" x2="0.1685" y2="1.9465" layer="21"/>
<rectangle x1="0.8885" y1="1.9315" x2="1.3385" y2="1.9465" layer="21"/>
<rectangle x1="1.9385" y1="1.9315" x2="2.3885" y2="1.9465" layer="21"/>
<rectangle x1="-2.5315" y1="1.9465" x2="-2.0815" y2="1.9615" layer="21"/>
<rectangle x1="-1.4815" y1="1.9465" x2="-1.0165" y2="1.9615" layer="21"/>
<rectangle x1="-0.3265" y1="1.9465" x2="0.1685" y2="1.9615" layer="21"/>
<rectangle x1="0.8885" y1="1.9465" x2="1.3385" y2="1.9615" layer="21"/>
<rectangle x1="1.9385" y1="1.9465" x2="2.3885" y2="1.9615" layer="21"/>
<rectangle x1="-2.5315" y1="1.9615" x2="-2.0815" y2="1.9765" layer="21"/>
<rectangle x1="-1.4815" y1="1.9615" x2="-1.0165" y2="1.9765" layer="21"/>
<rectangle x1="-0.3265" y1="1.9615" x2="0.1685" y2="1.9765" layer="21"/>
<rectangle x1="0.8885" y1="1.9615" x2="1.3385" y2="1.9765" layer="21"/>
<rectangle x1="1.9385" y1="1.9615" x2="2.3885" y2="1.9765" layer="21"/>
<rectangle x1="-2.5315" y1="1.9765" x2="-2.0815" y2="1.9915" layer="21"/>
<rectangle x1="-1.4815" y1="1.9765" x2="-1.0165" y2="1.9915" layer="21"/>
<rectangle x1="-0.3265" y1="1.9765" x2="0.1685" y2="1.9915" layer="21"/>
<rectangle x1="0.8885" y1="1.9765" x2="1.3385" y2="1.9915" layer="21"/>
<rectangle x1="1.9385" y1="1.9765" x2="2.3885" y2="1.9915" layer="21"/>
<rectangle x1="-2.5315" y1="1.9915" x2="-2.0815" y2="2.0065" layer="21"/>
<rectangle x1="-1.4815" y1="1.9915" x2="-1.0165" y2="2.0065" layer="21"/>
<rectangle x1="-0.3265" y1="1.9915" x2="0.1685" y2="2.0065" layer="21"/>
<rectangle x1="0.8885" y1="1.9915" x2="1.3385" y2="2.0065" layer="21"/>
<rectangle x1="1.9385" y1="1.9915" x2="2.3885" y2="2.0065" layer="21"/>
<rectangle x1="-2.5315" y1="2.0065" x2="-2.0815" y2="2.0215" layer="21"/>
<rectangle x1="-1.4815" y1="2.0065" x2="-1.0165" y2="2.0215" layer="21"/>
<rectangle x1="-0.3265" y1="2.0065" x2="0.1685" y2="2.0215" layer="21"/>
<rectangle x1="0.8885" y1="2.0065" x2="1.3385" y2="2.0215" layer="21"/>
<rectangle x1="1.9385" y1="2.0065" x2="2.3885" y2="2.0215" layer="21"/>
<rectangle x1="-2.5315" y1="2.0215" x2="-2.0815" y2="2.0365" layer="21"/>
<rectangle x1="-1.4815" y1="2.0215" x2="-1.0165" y2="2.0365" layer="21"/>
<rectangle x1="-0.3265" y1="2.0215" x2="0.1685" y2="2.0365" layer="21"/>
<rectangle x1="0.8885" y1="2.0215" x2="1.3385" y2="2.0365" layer="21"/>
<rectangle x1="1.9385" y1="2.0215" x2="2.3885" y2="2.0365" layer="21"/>
<rectangle x1="-2.5315" y1="2.0365" x2="-2.0815" y2="2.0515" layer="21"/>
<rectangle x1="-1.4815" y1="2.0365" x2="-1.0165" y2="2.0515" layer="21"/>
<rectangle x1="-0.3265" y1="2.0365" x2="0.1685" y2="2.0515" layer="21"/>
<rectangle x1="0.8885" y1="2.0365" x2="1.3385" y2="2.0515" layer="21"/>
<rectangle x1="1.9385" y1="2.0365" x2="2.3885" y2="2.0515" layer="21"/>
<rectangle x1="-2.5315" y1="2.0515" x2="-2.0815" y2="2.0665" layer="21"/>
<rectangle x1="-1.4815" y1="2.0515" x2="-1.0165" y2="2.0665" layer="21"/>
<rectangle x1="-0.3265" y1="2.0515" x2="0.1685" y2="2.0665" layer="21"/>
<rectangle x1="0.8885" y1="2.0515" x2="1.3385" y2="2.0665" layer="21"/>
<rectangle x1="1.9385" y1="2.0515" x2="2.3885" y2="2.0665" layer="21"/>
<rectangle x1="-2.5315" y1="2.0665" x2="-2.0815" y2="2.0815" layer="21"/>
<rectangle x1="-1.4815" y1="2.0665" x2="-1.0165" y2="2.0815" layer="21"/>
<rectangle x1="-0.3265" y1="2.0665" x2="0.1685" y2="2.0815" layer="21"/>
<rectangle x1="0.8885" y1="2.0665" x2="1.3385" y2="2.0815" layer="21"/>
<rectangle x1="1.9385" y1="2.0665" x2="2.3885" y2="2.0815" layer="21"/>
<rectangle x1="-2.5315" y1="2.0815" x2="-2.0815" y2="2.0965" layer="21"/>
<rectangle x1="-1.4815" y1="2.0815" x2="-1.0165" y2="2.0965" layer="21"/>
<rectangle x1="-0.3265" y1="2.0815" x2="0.1685" y2="2.0965" layer="21"/>
<rectangle x1="0.8885" y1="2.0815" x2="1.3385" y2="2.0965" layer="21"/>
<rectangle x1="1.9385" y1="2.0815" x2="2.3885" y2="2.0965" layer="21"/>
<rectangle x1="-2.5315" y1="2.0965" x2="-2.0815" y2="2.1115" layer="21"/>
<rectangle x1="-1.4815" y1="2.0965" x2="-1.0165" y2="2.1115" layer="21"/>
<rectangle x1="-0.3265" y1="2.0965" x2="0.1685" y2="2.1115" layer="21"/>
<rectangle x1="0.8885" y1="2.0965" x2="1.3385" y2="2.1115" layer="21"/>
<rectangle x1="1.9385" y1="2.0965" x2="2.3885" y2="2.1115" layer="21"/>
<rectangle x1="-2.5315" y1="2.1115" x2="-2.0815" y2="2.1265" layer="21"/>
<rectangle x1="-1.4815" y1="2.1115" x2="-1.0165" y2="2.1265" layer="21"/>
<rectangle x1="-0.3265" y1="2.1115" x2="0.1685" y2="2.1265" layer="21"/>
<rectangle x1="0.8885" y1="2.1115" x2="1.3385" y2="2.1265" layer="21"/>
<rectangle x1="1.9385" y1="2.1115" x2="2.3885" y2="2.1265" layer="21"/>
<rectangle x1="-2.5315" y1="2.1265" x2="-2.0815" y2="2.1415" layer="21"/>
<rectangle x1="-1.4815" y1="2.1265" x2="-1.0165" y2="2.1415" layer="21"/>
<rectangle x1="-0.3265" y1="2.1265" x2="0.1685" y2="2.1415" layer="21"/>
<rectangle x1="0.8885" y1="2.1265" x2="1.3385" y2="2.1415" layer="21"/>
<rectangle x1="1.9385" y1="2.1265" x2="2.3885" y2="2.1415" layer="21"/>
<rectangle x1="-2.5315" y1="2.1415" x2="-2.0815" y2="2.1565" layer="21"/>
<rectangle x1="-1.4815" y1="2.1415" x2="-1.0165" y2="2.1565" layer="21"/>
<rectangle x1="-0.3265" y1="2.1415" x2="0.1685" y2="2.1565" layer="21"/>
<rectangle x1="0.8885" y1="2.1415" x2="1.3385" y2="2.1565" layer="21"/>
<rectangle x1="1.9385" y1="2.1415" x2="2.3885" y2="2.1565" layer="21"/>
<rectangle x1="-2.5315" y1="2.1565" x2="-2.0815" y2="2.1715" layer="21"/>
<rectangle x1="-1.4815" y1="2.1565" x2="-1.0165" y2="2.1715" layer="21"/>
<rectangle x1="-0.3265" y1="2.1565" x2="0.1685" y2="2.1715" layer="21"/>
<rectangle x1="0.8885" y1="2.1565" x2="1.3385" y2="2.1715" layer="21"/>
<rectangle x1="1.9385" y1="2.1565" x2="2.3885" y2="2.1715" layer="21"/>
<rectangle x1="-2.5315" y1="2.1715" x2="-2.0815" y2="2.1865" layer="21"/>
<rectangle x1="-1.4815" y1="2.1715" x2="-1.0165" y2="2.1865" layer="21"/>
<rectangle x1="-0.3265" y1="2.1715" x2="0.1685" y2="2.1865" layer="21"/>
<rectangle x1="0.8885" y1="2.1715" x2="1.3385" y2="2.1865" layer="21"/>
<rectangle x1="1.9385" y1="2.1715" x2="2.3885" y2="2.1865" layer="21"/>
<rectangle x1="-2.5315" y1="2.1865" x2="-2.0815" y2="2.2015" layer="21"/>
<rectangle x1="-1.4815" y1="2.1865" x2="-1.0165" y2="2.2015" layer="21"/>
<rectangle x1="-0.3265" y1="2.1865" x2="0.1685" y2="2.2015" layer="21"/>
<rectangle x1="0.8885" y1="2.1865" x2="1.3385" y2="2.2015" layer="21"/>
<rectangle x1="1.9385" y1="2.1865" x2="2.3885" y2="2.2015" layer="21"/>
<rectangle x1="-2.5315" y1="2.2015" x2="-2.0815" y2="2.2165" layer="21"/>
<rectangle x1="-1.4815" y1="2.2015" x2="-1.0165" y2="2.2165" layer="21"/>
<rectangle x1="-0.3265" y1="2.2015" x2="0.1685" y2="2.2165" layer="21"/>
<rectangle x1="0.8885" y1="2.2015" x2="1.3385" y2="2.2165" layer="21"/>
<rectangle x1="1.9385" y1="2.2015" x2="2.3885" y2="2.2165" layer="21"/>
<rectangle x1="-2.5315" y1="2.2165" x2="-2.0815" y2="2.2315" layer="21"/>
<rectangle x1="-1.4815" y1="2.2165" x2="-1.0165" y2="2.2315" layer="21"/>
<rectangle x1="-0.3265" y1="2.2165" x2="0.1685" y2="2.2315" layer="21"/>
<rectangle x1="0.8885" y1="2.2165" x2="1.3385" y2="2.2315" layer="21"/>
<rectangle x1="1.9385" y1="2.2165" x2="2.3885" y2="2.2315" layer="21"/>
<rectangle x1="-2.5315" y1="2.2315" x2="-2.0815" y2="2.2465" layer="21"/>
<rectangle x1="-1.4815" y1="2.2315" x2="-1.0165" y2="2.2465" layer="21"/>
<rectangle x1="-0.3265" y1="2.2315" x2="0.1685" y2="2.2465" layer="21"/>
<rectangle x1="0.8885" y1="2.2315" x2="1.3385" y2="2.2465" layer="21"/>
<rectangle x1="1.9385" y1="2.2315" x2="2.3885" y2="2.2465" layer="21"/>
<rectangle x1="-2.5315" y1="2.2465" x2="-2.0815" y2="2.2615" layer="21"/>
<rectangle x1="-1.4815" y1="2.2465" x2="-1.0165" y2="2.2615" layer="21"/>
<rectangle x1="-0.3265" y1="2.2465" x2="0.1685" y2="2.2615" layer="21"/>
<rectangle x1="0.8885" y1="2.2465" x2="1.3385" y2="2.2615" layer="21"/>
<rectangle x1="1.9385" y1="2.2465" x2="2.3885" y2="2.2615" layer="21"/>
<rectangle x1="-2.5315" y1="2.2615" x2="-2.0815" y2="2.2765" layer="21"/>
<rectangle x1="-1.4815" y1="2.2615" x2="-1.0165" y2="2.2765" layer="21"/>
<rectangle x1="-0.3265" y1="2.2615" x2="0.1685" y2="2.2765" layer="21"/>
<rectangle x1="0.8885" y1="2.2615" x2="1.3385" y2="2.2765" layer="21"/>
<rectangle x1="1.9385" y1="2.2615" x2="2.3885" y2="2.2765" layer="21"/>
<rectangle x1="-2.5315" y1="2.2765" x2="-2.0815" y2="2.2915" layer="21"/>
<rectangle x1="-1.4815" y1="2.2765" x2="-1.0165" y2="2.2915" layer="21"/>
<rectangle x1="-0.3265" y1="2.2765" x2="0.1685" y2="2.2915" layer="21"/>
<rectangle x1="0.8885" y1="2.2765" x2="1.3385" y2="2.2915" layer="21"/>
<rectangle x1="1.9385" y1="2.2765" x2="2.3885" y2="2.2915" layer="21"/>
<rectangle x1="-2.5315" y1="2.2915" x2="-2.0815" y2="2.3065" layer="21"/>
<rectangle x1="-1.4815" y1="2.2915" x2="-1.0165" y2="2.3065" layer="21"/>
<rectangle x1="-0.3265" y1="2.2915" x2="0.1685" y2="2.3065" layer="21"/>
<rectangle x1="0.8885" y1="2.2915" x2="1.3385" y2="2.3065" layer="21"/>
<rectangle x1="1.9385" y1="2.2915" x2="2.3885" y2="2.3065" layer="21"/>
<rectangle x1="-2.5315" y1="2.3065" x2="-2.0815" y2="2.3215" layer="21"/>
<rectangle x1="-1.4815" y1="2.3065" x2="-1.0165" y2="2.3215" layer="21"/>
<rectangle x1="-0.3265" y1="2.3065" x2="0.1685" y2="2.3215" layer="21"/>
<rectangle x1="0.8885" y1="2.3065" x2="1.3385" y2="2.3215" layer="21"/>
<rectangle x1="1.9385" y1="2.3065" x2="2.3885" y2="2.3215" layer="21"/>
<rectangle x1="-2.5315" y1="2.3215" x2="-2.0815" y2="2.3365" layer="21"/>
<rectangle x1="-1.4815" y1="2.3215" x2="-1.0165" y2="2.3365" layer="21"/>
<rectangle x1="-0.3265" y1="2.3215" x2="0.1685" y2="2.3365" layer="21"/>
<rectangle x1="0.8885" y1="2.3215" x2="1.3385" y2="2.3365" layer="21"/>
<rectangle x1="1.9385" y1="2.3215" x2="2.3885" y2="2.3365" layer="21"/>
<rectangle x1="-2.5315" y1="2.3365" x2="-2.0815" y2="2.3515" layer="21"/>
<rectangle x1="-1.4815" y1="2.3365" x2="-1.0165" y2="2.3515" layer="21"/>
<rectangle x1="-0.3265" y1="2.3365" x2="0.1685" y2="2.3515" layer="21"/>
<rectangle x1="0.8885" y1="2.3365" x2="1.3385" y2="2.3515" layer="21"/>
<rectangle x1="1.9385" y1="2.3365" x2="2.3885" y2="2.3515" layer="21"/>
<rectangle x1="-2.5315" y1="2.3515" x2="-2.0815" y2="2.3665" layer="21"/>
<rectangle x1="-1.4815" y1="2.3515" x2="-1.0165" y2="2.3665" layer="21"/>
<rectangle x1="-0.3265" y1="2.3515" x2="0.1685" y2="2.3665" layer="21"/>
<rectangle x1="0.8885" y1="2.3515" x2="1.3385" y2="2.3665" layer="21"/>
<rectangle x1="1.9385" y1="2.3515" x2="2.3885" y2="2.3665" layer="21"/>
<rectangle x1="-2.5315" y1="2.3665" x2="-2.0815" y2="2.3815" layer="21"/>
<rectangle x1="-1.4815" y1="2.3665" x2="-1.0165" y2="2.3815" layer="21"/>
<rectangle x1="-0.3265" y1="2.3665" x2="0.1685" y2="2.3815" layer="21"/>
<rectangle x1="0.8885" y1="2.3665" x2="1.3385" y2="2.3815" layer="21"/>
<rectangle x1="1.9385" y1="2.3665" x2="2.3885" y2="2.3815" layer="21"/>
<rectangle x1="-2.5315" y1="2.3815" x2="-2.0815" y2="2.3965" layer="21"/>
<rectangle x1="-1.4815" y1="2.3815" x2="-1.0165" y2="2.3965" layer="21"/>
<rectangle x1="-0.3265" y1="2.3815" x2="0.1685" y2="2.3965" layer="21"/>
<rectangle x1="0.8885" y1="2.3815" x2="1.3385" y2="2.3965" layer="21"/>
<rectangle x1="1.9385" y1="2.3815" x2="2.3885" y2="2.3965" layer="21"/>
<rectangle x1="-2.5315" y1="2.3965" x2="-2.0815" y2="2.4115" layer="21"/>
<rectangle x1="-1.4815" y1="2.3965" x2="-1.0165" y2="2.4115" layer="21"/>
<rectangle x1="-0.3265" y1="2.3965" x2="0.1685" y2="2.4115" layer="21"/>
<rectangle x1="0.8885" y1="2.3965" x2="1.3385" y2="2.4115" layer="21"/>
<rectangle x1="1.9385" y1="2.3965" x2="2.3885" y2="2.4115" layer="21"/>
<rectangle x1="-2.5315" y1="2.4115" x2="-2.0815" y2="2.4265" layer="21"/>
<rectangle x1="-1.4815" y1="2.4115" x2="-1.0165" y2="2.4265" layer="21"/>
<rectangle x1="-0.3265" y1="2.4115" x2="0.1685" y2="2.4265" layer="21"/>
<rectangle x1="0.8885" y1="2.4115" x2="1.3385" y2="2.4265" layer="21"/>
<rectangle x1="1.9385" y1="2.4115" x2="2.3885" y2="2.4265" layer="21"/>
<rectangle x1="-2.5315" y1="2.4265" x2="-2.0815" y2="2.4415" layer="21"/>
<rectangle x1="-1.4815" y1="2.4265" x2="-1.0165" y2="2.4415" layer="21"/>
<rectangle x1="-0.3265" y1="2.4265" x2="0.1685" y2="2.4415" layer="21"/>
<rectangle x1="0.8885" y1="2.4265" x2="1.3385" y2="2.4415" layer="21"/>
<rectangle x1="1.9385" y1="2.4265" x2="2.3885" y2="2.4415" layer="21"/>
<rectangle x1="-2.5315" y1="2.4415" x2="-2.0815" y2="2.4565" layer="21"/>
<rectangle x1="-1.4815" y1="2.4415" x2="-1.0165" y2="2.4565" layer="21"/>
<rectangle x1="-0.3265" y1="2.4415" x2="0.1685" y2="2.4565" layer="21"/>
<rectangle x1="0.8885" y1="2.4415" x2="1.3385" y2="2.4565" layer="21"/>
<rectangle x1="1.9385" y1="2.4415" x2="2.3885" y2="2.4565" layer="21"/>
<rectangle x1="-2.5315" y1="2.4565" x2="-2.0815" y2="2.4715" layer="21"/>
<rectangle x1="-1.4815" y1="2.4565" x2="-1.0165" y2="2.4715" layer="21"/>
<rectangle x1="-0.3265" y1="2.4565" x2="0.1685" y2="2.4715" layer="21"/>
<rectangle x1="0.8885" y1="2.4565" x2="1.3385" y2="2.4715" layer="21"/>
<rectangle x1="1.9385" y1="2.4565" x2="2.3885" y2="2.4715" layer="21"/>
<rectangle x1="-2.5315" y1="2.4715" x2="-2.0815" y2="2.4865" layer="21"/>
<rectangle x1="-1.4815" y1="2.4715" x2="-1.0165" y2="2.4865" layer="21"/>
<rectangle x1="-0.3265" y1="2.4715" x2="0.1685" y2="2.4865" layer="21"/>
<rectangle x1="0.8885" y1="2.4715" x2="1.3385" y2="2.4865" layer="21"/>
<rectangle x1="1.9385" y1="2.4715" x2="2.3885" y2="2.4865" layer="21"/>
<rectangle x1="-2.5315" y1="2.4865" x2="-2.0815" y2="2.5015" layer="21"/>
<rectangle x1="-1.4815" y1="2.4865" x2="-1.0165" y2="2.5015" layer="21"/>
<rectangle x1="-0.3265" y1="2.4865" x2="0.1685" y2="2.5015" layer="21"/>
<rectangle x1="0.8885" y1="2.4865" x2="1.3385" y2="2.5015" layer="21"/>
<rectangle x1="1.9385" y1="2.4865" x2="2.3885" y2="2.5015" layer="21"/>
<rectangle x1="-2.5315" y1="2.5015" x2="-2.0815" y2="2.5165" layer="21"/>
<rectangle x1="-1.4815" y1="2.5015" x2="-1.0165" y2="2.5165" layer="21"/>
<rectangle x1="-0.3265" y1="2.5015" x2="0.1685" y2="2.5165" layer="21"/>
<rectangle x1="0.8885" y1="2.5015" x2="1.3385" y2="2.5165" layer="21"/>
<rectangle x1="1.9385" y1="2.5015" x2="2.3885" y2="2.5165" layer="21"/>
<rectangle x1="-2.5315" y1="2.5165" x2="-2.0815" y2="2.5315" layer="21"/>
<rectangle x1="-1.4815" y1="2.5165" x2="-1.0165" y2="2.5315" layer="21"/>
<rectangle x1="-0.3265" y1="2.5165" x2="0.1685" y2="2.5315" layer="21"/>
<rectangle x1="0.8885" y1="2.5165" x2="1.3385" y2="2.5315" layer="21"/>
<rectangle x1="1.9385" y1="2.5165" x2="2.3885" y2="2.5315" layer="21"/>
<rectangle x1="-2.5315" y1="2.5315" x2="-2.0815" y2="2.5465" layer="21"/>
<rectangle x1="-1.4815" y1="2.5315" x2="-1.0165" y2="2.5465" layer="21"/>
<rectangle x1="-0.3265" y1="2.5315" x2="0.1685" y2="2.5465" layer="21"/>
<rectangle x1="0.8885" y1="2.5315" x2="1.3385" y2="2.5465" layer="21"/>
<rectangle x1="1.9385" y1="2.5315" x2="2.3885" y2="2.5465" layer="21"/>
<rectangle x1="-2.5315" y1="2.5465" x2="-2.0815" y2="2.5615" layer="21"/>
<rectangle x1="-1.4815" y1="2.5465" x2="-1.0165" y2="2.5615" layer="21"/>
<rectangle x1="-0.3265" y1="2.5465" x2="0.1685" y2="2.5615" layer="21"/>
<rectangle x1="0.8885" y1="2.5465" x2="1.3385" y2="2.5615" layer="21"/>
<rectangle x1="1.9385" y1="2.5465" x2="2.3885" y2="2.5615" layer="21"/>
<rectangle x1="-2.5315" y1="2.5615" x2="-2.0815" y2="2.5765" layer="21"/>
<rectangle x1="-1.4815" y1="2.5615" x2="-1.0315" y2="2.5765" layer="21"/>
<rectangle x1="-0.3265" y1="2.5615" x2="0.1685" y2="2.5765" layer="21"/>
<rectangle x1="0.8885" y1="2.5615" x2="1.3385" y2="2.5765" layer="21"/>
<rectangle x1="1.9385" y1="2.5615" x2="2.3885" y2="2.5765" layer="21"/>
<rectangle x1="-2.5315" y1="2.5765" x2="-2.0815" y2="2.5915" layer="21"/>
<rectangle x1="-1.4815" y1="2.5765" x2="-1.0315" y2="2.5915" layer="21"/>
<rectangle x1="-0.3265" y1="2.5765" x2="0.1685" y2="2.5915" layer="21"/>
<rectangle x1="0.8885" y1="2.5765" x2="1.3385" y2="2.5915" layer="21"/>
<rectangle x1="1.9385" y1="2.5765" x2="2.3885" y2="2.5915" layer="21"/>
<rectangle x1="-2.5315" y1="2.5915" x2="-2.0815" y2="2.6065" layer="21"/>
<rectangle x1="-1.4965" y1="2.5915" x2="-1.0315" y2="2.6065" layer="21"/>
<rectangle x1="-0.3265" y1="2.5915" x2="0.1685" y2="2.6065" layer="21"/>
<rectangle x1="0.8885" y1="2.5915" x2="1.3385" y2="2.6065" layer="21"/>
<rectangle x1="1.9385" y1="2.5915" x2="2.3885" y2="2.6065" layer="21"/>
<rectangle x1="-2.5315" y1="2.6065" x2="-2.0815" y2="2.6215" layer="21"/>
<rectangle x1="-1.4965" y1="2.6065" x2="-1.0315" y2="2.6215" layer="21"/>
<rectangle x1="-0.3265" y1="2.6065" x2="0.1685" y2="2.6215" layer="21"/>
<rectangle x1="0.8885" y1="2.6065" x2="1.3385" y2="2.6215" layer="21"/>
<rectangle x1="1.9385" y1="2.6065" x2="2.3885" y2="2.6215" layer="21"/>
<rectangle x1="-2.5315" y1="2.6215" x2="-2.0815" y2="2.6365" layer="21"/>
<rectangle x1="-1.4965" y1="2.6215" x2="-1.0315" y2="2.6365" layer="21"/>
<rectangle x1="-0.3265" y1="2.6215" x2="0.1685" y2="2.6365" layer="21"/>
<rectangle x1="0.8885" y1="2.6215" x2="1.3385" y2="2.6365" layer="21"/>
<rectangle x1="1.9385" y1="2.6215" x2="2.3885" y2="2.6365" layer="21"/>
<rectangle x1="-2.5315" y1="2.6365" x2="-2.0815" y2="2.6515" layer="21"/>
<rectangle x1="-1.4965" y1="2.6365" x2="-1.0315" y2="2.6515" layer="21"/>
<rectangle x1="-0.3265" y1="2.6365" x2="0.1685" y2="2.6515" layer="21"/>
<rectangle x1="0.8885" y1="2.6365" x2="1.3385" y2="2.6515" layer="21"/>
<rectangle x1="1.9385" y1="2.6365" x2="2.3885" y2="2.6515" layer="21"/>
<rectangle x1="-2.5315" y1="2.6515" x2="-2.0815" y2="2.6665" layer="21"/>
<rectangle x1="-1.4965" y1="2.6515" x2="-1.0315" y2="2.6665" layer="21"/>
<rectangle x1="-0.3265" y1="2.6515" x2="0.1685" y2="2.6665" layer="21"/>
<rectangle x1="0.8885" y1="2.6515" x2="1.3385" y2="2.6665" layer="21"/>
<rectangle x1="1.9385" y1="2.6515" x2="2.3885" y2="2.6665" layer="21"/>
<rectangle x1="-2.5315" y1="2.6665" x2="-2.0815" y2="2.6815" layer="21"/>
<rectangle x1="-1.4965" y1="2.6665" x2="-1.0315" y2="2.6815" layer="21"/>
<rectangle x1="-0.3265" y1="2.6665" x2="0.1685" y2="2.6815" layer="21"/>
<rectangle x1="0.8885" y1="2.6665" x2="1.3385" y2="2.6815" layer="21"/>
<rectangle x1="1.9385" y1="2.6665" x2="2.3885" y2="2.6815" layer="21"/>
<rectangle x1="-2.5315" y1="2.6815" x2="-2.0815" y2="2.6965" layer="21"/>
<rectangle x1="-1.4965" y1="2.6815" x2="-1.0315" y2="2.6965" layer="21"/>
<rectangle x1="-0.3265" y1="2.6815" x2="0.1685" y2="2.6965" layer="21"/>
<rectangle x1="0.8885" y1="2.6815" x2="1.3385" y2="2.6965" layer="21"/>
<rectangle x1="1.9385" y1="2.6815" x2="2.3885" y2="2.6965" layer="21"/>
<rectangle x1="-2.5315" y1="2.6965" x2="-2.0815" y2="2.7115" layer="21"/>
<rectangle x1="-1.4965" y1="2.6965" x2="-1.0315" y2="2.7115" layer="21"/>
<rectangle x1="-0.3265" y1="2.6965" x2="0.1685" y2="2.7115" layer="21"/>
<rectangle x1="0.8885" y1="2.6965" x2="1.3385" y2="2.7115" layer="21"/>
<rectangle x1="1.9385" y1="2.6965" x2="2.3885" y2="2.7115" layer="21"/>
<rectangle x1="-2.5315" y1="2.7115" x2="-2.0815" y2="2.7265" layer="21"/>
<rectangle x1="-1.4965" y1="2.7115" x2="-1.0315" y2="2.7265" layer="21"/>
<rectangle x1="-0.3265" y1="2.7115" x2="0.1685" y2="2.7265" layer="21"/>
<rectangle x1="0.8885" y1="2.7115" x2="1.3385" y2="2.7265" layer="21"/>
<rectangle x1="1.9385" y1="2.7115" x2="2.3885" y2="2.7265" layer="21"/>
<rectangle x1="-2.5315" y1="2.7265" x2="-2.0815" y2="2.7415" layer="21"/>
<rectangle x1="-1.4965" y1="2.7265" x2="-1.0315" y2="2.7415" layer="21"/>
<rectangle x1="-0.3265" y1="2.7265" x2="0.1685" y2="2.7415" layer="21"/>
<rectangle x1="0.8885" y1="2.7265" x2="1.3385" y2="2.7415" layer="21"/>
<rectangle x1="1.9385" y1="2.7265" x2="2.3885" y2="2.7415" layer="21"/>
<rectangle x1="-2.5315" y1="2.7415" x2="-2.0815" y2="2.7565" layer="21"/>
<rectangle x1="-1.4965" y1="2.7415" x2="-1.0315" y2="2.7565" layer="21"/>
<rectangle x1="-0.3265" y1="2.7415" x2="0.1685" y2="2.7565" layer="21"/>
<rectangle x1="0.8885" y1="2.7415" x2="1.3385" y2="2.7565" layer="21"/>
<rectangle x1="1.9385" y1="2.7415" x2="2.3885" y2="2.7565" layer="21"/>
<rectangle x1="-2.5315" y1="2.7565" x2="-2.0815" y2="2.7715" layer="21"/>
<rectangle x1="-1.5115" y1="2.7565" x2="-1.0315" y2="2.7715" layer="21"/>
<rectangle x1="-0.3265" y1="2.7565" x2="0.1685" y2="2.7715" layer="21"/>
<rectangle x1="0.8885" y1="2.7565" x2="1.3385" y2="2.7715" layer="21"/>
<rectangle x1="1.9385" y1="2.7565" x2="2.3885" y2="2.7715" layer="21"/>
<rectangle x1="-2.5315" y1="2.7715" x2="-2.0815" y2="2.7865" layer="21"/>
<rectangle x1="-1.5115" y1="2.7715" x2="-1.0465" y2="2.7865" layer="21"/>
<rectangle x1="-0.3265" y1="2.7715" x2="0.1685" y2="2.7865" layer="21"/>
<rectangle x1="0.8885" y1="2.7715" x2="1.3385" y2="2.7865" layer="21"/>
<rectangle x1="1.9385" y1="2.7715" x2="2.3885" y2="2.7865" layer="21"/>
<rectangle x1="-2.5315" y1="2.7865" x2="-2.0815" y2="2.8015" layer="21"/>
<rectangle x1="-1.5115" y1="2.7865" x2="-1.0465" y2="2.8015" layer="21"/>
<rectangle x1="-0.3265" y1="2.7865" x2="0.1685" y2="2.8015" layer="21"/>
<rectangle x1="0.8885" y1="2.7865" x2="1.3385" y2="2.8015" layer="21"/>
<rectangle x1="1.9385" y1="2.7865" x2="2.3885" y2="2.8015" layer="21"/>
<rectangle x1="-2.5315" y1="2.8015" x2="-2.0815" y2="2.8165" layer="21"/>
<rectangle x1="-1.5115" y1="2.8015" x2="-1.0465" y2="2.8165" layer="21"/>
<rectangle x1="-0.3265" y1="2.8015" x2="0.1685" y2="2.8165" layer="21"/>
<rectangle x1="0.8885" y1="2.8015" x2="1.3385" y2="2.8165" layer="21"/>
<rectangle x1="1.9385" y1="2.8015" x2="2.3885" y2="2.8165" layer="21"/>
<rectangle x1="-2.5315" y1="2.8165" x2="-2.0815" y2="2.8315" layer="21"/>
<rectangle x1="-1.5115" y1="2.8165" x2="-1.0465" y2="2.8315" layer="21"/>
<rectangle x1="-0.3265" y1="2.8165" x2="0.1685" y2="2.8315" layer="21"/>
<rectangle x1="0.8885" y1="2.8165" x2="1.3385" y2="2.8315" layer="21"/>
<rectangle x1="1.9385" y1="2.8165" x2="2.3885" y2="2.8315" layer="21"/>
<rectangle x1="-2.5315" y1="2.8315" x2="-2.0815" y2="2.8465" layer="21"/>
<rectangle x1="-1.5265" y1="2.8315" x2="-1.0465" y2="2.8465" layer="21"/>
<rectangle x1="-0.3265" y1="2.8315" x2="0.1685" y2="2.8465" layer="21"/>
<rectangle x1="0.8885" y1="2.8315" x2="1.3385" y2="2.8465" layer="21"/>
<rectangle x1="1.9385" y1="2.8315" x2="2.3885" y2="2.8465" layer="21"/>
<rectangle x1="-2.5315" y1="2.8465" x2="-2.0815" y2="2.8615" layer="21"/>
<rectangle x1="-1.5265" y1="2.8465" x2="-1.0465" y2="2.8615" layer="21"/>
<rectangle x1="-0.3265" y1="2.8465" x2="0.1685" y2="2.8615" layer="21"/>
<rectangle x1="0.8885" y1="2.8465" x2="1.3385" y2="2.8615" layer="21"/>
<rectangle x1="1.9385" y1="2.8465" x2="2.3885" y2="2.8615" layer="21"/>
<rectangle x1="-2.5315" y1="2.8615" x2="-2.0815" y2="2.8765" layer="21"/>
<rectangle x1="-1.5265" y1="2.8615" x2="-1.0465" y2="2.8765" layer="21"/>
<rectangle x1="-0.3265" y1="2.8615" x2="0.1685" y2="2.8765" layer="21"/>
<rectangle x1="0.8885" y1="2.8615" x2="1.3385" y2="2.8765" layer="21"/>
<rectangle x1="1.9385" y1="2.8615" x2="2.3885" y2="2.8765" layer="21"/>
<rectangle x1="-2.5315" y1="2.8765" x2="-2.0815" y2="2.8915" layer="21"/>
<rectangle x1="-1.5415" y1="2.8765" x2="-1.0615" y2="2.8915" layer="21"/>
<rectangle x1="-0.3265" y1="2.8765" x2="0.1685" y2="2.8915" layer="21"/>
<rectangle x1="0.8885" y1="2.8765" x2="1.3385" y2="2.8915" layer="21"/>
<rectangle x1="1.9385" y1="2.8765" x2="2.3885" y2="2.8915" layer="21"/>
<rectangle x1="-2.5315" y1="2.8915" x2="-2.0815" y2="2.9065" layer="21"/>
<rectangle x1="-1.5415" y1="2.8915" x2="-1.0615" y2="2.9065" layer="21"/>
<rectangle x1="-0.3265" y1="2.8915" x2="0.1685" y2="2.9065" layer="21"/>
<rectangle x1="0.8885" y1="2.8915" x2="1.3385" y2="2.9065" layer="21"/>
<rectangle x1="1.9385" y1="2.8915" x2="2.3885" y2="2.9065" layer="21"/>
<rectangle x1="-2.5315" y1="2.9065" x2="-2.0815" y2="2.9215" layer="21"/>
<rectangle x1="-1.5565" y1="2.9065" x2="-1.0615" y2="2.9215" layer="21"/>
<rectangle x1="-0.3265" y1="2.9065" x2="0.1685" y2="2.9215" layer="21"/>
<rectangle x1="0.8885" y1="2.9065" x2="1.3385" y2="2.9215" layer="21"/>
<rectangle x1="1.9385" y1="2.9065" x2="2.3885" y2="2.9215" layer="21"/>
<rectangle x1="-2.5315" y1="2.9215" x2="-2.0815" y2="2.9365" layer="21"/>
<rectangle x1="-1.5565" y1="2.9215" x2="-1.0615" y2="2.9365" layer="21"/>
<rectangle x1="-0.3265" y1="2.9215" x2="0.1685" y2="2.9365" layer="21"/>
<rectangle x1="0.8885" y1="2.9215" x2="1.3385" y2="2.9365" layer="21"/>
<rectangle x1="1.9385" y1="2.9215" x2="2.3885" y2="2.9365" layer="21"/>
<rectangle x1="-2.5315" y1="2.9365" x2="-2.0815" y2="2.9515" layer="21"/>
<rectangle x1="-1.5715" y1="2.9365" x2="-1.0765" y2="2.9515" layer="21"/>
<rectangle x1="-0.3265" y1="2.9365" x2="0.1685" y2="2.9515" layer="21"/>
<rectangle x1="0.8885" y1="2.9365" x2="1.3385" y2="2.9515" layer="21"/>
<rectangle x1="1.9385" y1="2.9365" x2="2.3885" y2="2.9515" layer="21"/>
<rectangle x1="-2.5315" y1="2.9515" x2="-2.0815" y2="2.9665" layer="21"/>
<rectangle x1="-1.5865" y1="2.9515" x2="-1.0765" y2="2.9665" layer="21"/>
<rectangle x1="-0.3265" y1="2.9515" x2="0.1685" y2="2.9665" layer="21"/>
<rectangle x1="0.8885" y1="2.9515" x2="1.3385" y2="2.9665" layer="21"/>
<rectangle x1="1.9385" y1="2.9515" x2="2.3885" y2="2.9665" layer="21"/>
<rectangle x1="-2.5315" y1="2.9665" x2="-2.0815" y2="2.9815" layer="21"/>
<rectangle x1="-1.6015" y1="2.9665" x2="-1.0765" y2="2.9815" layer="21"/>
<rectangle x1="-0.3265" y1="2.9665" x2="0.1685" y2="2.9815" layer="21"/>
<rectangle x1="0.8885" y1="2.9665" x2="1.3385" y2="2.9815" layer="21"/>
<rectangle x1="1.9385" y1="2.9665" x2="2.3885" y2="2.9815" layer="21"/>
<rectangle x1="-2.5315" y1="2.9815" x2="-2.0815" y2="2.9965" layer="21"/>
<rectangle x1="-1.6165" y1="2.9815" x2="-1.0765" y2="2.9965" layer="21"/>
<rectangle x1="-0.3265" y1="2.9815" x2="0.1685" y2="2.9965" layer="21"/>
<rectangle x1="0.8885" y1="2.9815" x2="1.3385" y2="2.9965" layer="21"/>
<rectangle x1="1.9385" y1="2.9815" x2="2.3885" y2="2.9965" layer="21"/>
<rectangle x1="-2.5315" y1="2.9965" x2="-2.0815" y2="3.0115" layer="21"/>
<rectangle x1="-1.6465" y1="2.9965" x2="-1.0915" y2="3.0115" layer="21"/>
<rectangle x1="-0.3265" y1="2.9965" x2="0.1835" y2="3.0115" layer="21"/>
<rectangle x1="0.8885" y1="2.9965" x2="1.3385" y2="3.0115" layer="21"/>
<rectangle x1="1.9385" y1="2.9965" x2="2.3885" y2="3.0115" layer="21"/>
<rectangle x1="-2.5315" y1="3.0115" x2="-2.0815" y2="3.0265" layer="21"/>
<rectangle x1="-1.6765" y1="3.0115" x2="-1.0915" y2="3.0265" layer="21"/>
<rectangle x1="-0.8065" y1="3.0115" x2="0.6635" y2="3.0265" layer="21"/>
<rectangle x1="0.8885" y1="3.0115" x2="1.3385" y2="3.0265" layer="21"/>
<rectangle x1="1.9385" y1="3.0115" x2="2.3885" y2="3.0265" layer="21"/>
<rectangle x1="-2.5315" y1="3.0265" x2="-1.1065" y2="3.0415" layer="21"/>
<rectangle x1="-0.8215" y1="3.0265" x2="0.6635" y2="3.0415" layer="21"/>
<rectangle x1="0.8885" y1="3.0265" x2="1.3385" y2="3.0415" layer="21"/>
<rectangle x1="1.9385" y1="3.0265" x2="2.3885" y2="3.0415" layer="21"/>
<rectangle x1="-2.5315" y1="3.0415" x2="-1.1065" y2="3.0565" layer="21"/>
<rectangle x1="-0.8215" y1="3.0415" x2="0.6785" y2="3.0565" layer="21"/>
<rectangle x1="0.8885" y1="3.0415" x2="1.3385" y2="3.0565" layer="21"/>
<rectangle x1="1.9385" y1="3.0415" x2="2.3885" y2="3.0565" layer="21"/>
<rectangle x1="-2.5315" y1="3.0565" x2="-1.1065" y2="3.0715" layer="21"/>
<rectangle x1="-0.8215" y1="3.0565" x2="0.6785" y2="3.0715" layer="21"/>
<rectangle x1="0.8885" y1="3.0565" x2="1.3385" y2="3.0715" layer="21"/>
<rectangle x1="1.9385" y1="3.0565" x2="2.3885" y2="3.0715" layer="21"/>
<rectangle x1="-2.5315" y1="3.0715" x2="-1.1215" y2="3.0865" layer="21"/>
<rectangle x1="-0.8215" y1="3.0715" x2="0.6785" y2="3.0865" layer="21"/>
<rectangle x1="0.8885" y1="3.0715" x2="1.3385" y2="3.0865" layer="21"/>
<rectangle x1="1.9385" y1="3.0715" x2="2.3885" y2="3.0865" layer="21"/>
<rectangle x1="-2.5315" y1="3.0865" x2="-1.1215" y2="3.1015" layer="21"/>
<rectangle x1="-0.8215" y1="3.0865" x2="0.6785" y2="3.1015" layer="21"/>
<rectangle x1="0.8885" y1="3.0865" x2="1.3385" y2="3.1015" layer="21"/>
<rectangle x1="1.9385" y1="3.0865" x2="2.3885" y2="3.1015" layer="21"/>
<rectangle x1="-2.5315" y1="3.1015" x2="-1.1365" y2="3.1165" layer="21"/>
<rectangle x1="-0.8215" y1="3.1015" x2="0.6785" y2="3.1165" layer="21"/>
<rectangle x1="0.8885" y1="3.1015" x2="1.3385" y2="3.1165" layer="21"/>
<rectangle x1="1.9385" y1="3.1015" x2="2.3885" y2="3.1165" layer="21"/>
<rectangle x1="-2.5315" y1="3.1165" x2="-1.1515" y2="3.1315" layer="21"/>
<rectangle x1="-0.8215" y1="3.1165" x2="0.6785" y2="3.1315" layer="21"/>
<rectangle x1="0.8885" y1="3.1165" x2="1.3385" y2="3.1315" layer="21"/>
<rectangle x1="1.9385" y1="3.1165" x2="2.3885" y2="3.1315" layer="21"/>
<rectangle x1="-2.5315" y1="3.1315" x2="-1.1515" y2="3.1465" layer="21"/>
<rectangle x1="-0.8215" y1="3.1315" x2="0.6785" y2="3.1465" layer="21"/>
<rectangle x1="0.8885" y1="3.1315" x2="1.3385" y2="3.1465" layer="21"/>
<rectangle x1="1.9385" y1="3.1315" x2="2.3885" y2="3.1465" layer="21"/>
<rectangle x1="-2.5315" y1="3.1465" x2="-1.1665" y2="3.1615" layer="21"/>
<rectangle x1="-0.8215" y1="3.1465" x2="0.6785" y2="3.1615" layer="21"/>
<rectangle x1="0.8885" y1="3.1465" x2="1.3385" y2="3.1615" layer="21"/>
<rectangle x1="1.9385" y1="3.1465" x2="2.3885" y2="3.1615" layer="21"/>
<rectangle x1="-2.5315" y1="3.1615" x2="-1.1815" y2="3.1765" layer="21"/>
<rectangle x1="-0.8215" y1="3.1615" x2="0.6785" y2="3.1765" layer="21"/>
<rectangle x1="0.8885" y1="3.1615" x2="1.3385" y2="3.1765" layer="21"/>
<rectangle x1="1.9385" y1="3.1615" x2="2.3885" y2="3.1765" layer="21"/>
<rectangle x1="-2.5315" y1="3.1765" x2="-1.1965" y2="3.1915" layer="21"/>
<rectangle x1="-0.8215" y1="3.1765" x2="0.6785" y2="3.1915" layer="21"/>
<rectangle x1="0.8885" y1="3.1765" x2="1.3385" y2="3.1915" layer="21"/>
<rectangle x1="1.9385" y1="3.1765" x2="2.3885" y2="3.1915" layer="21"/>
<rectangle x1="-2.5315" y1="3.1915" x2="-1.1965" y2="3.2065" layer="21"/>
<rectangle x1="-0.8215" y1="3.1915" x2="0.6785" y2="3.2065" layer="21"/>
<rectangle x1="0.8885" y1="3.1915" x2="1.3385" y2="3.2065" layer="21"/>
<rectangle x1="1.9385" y1="3.1915" x2="2.3885" y2="3.2065" layer="21"/>
<rectangle x1="-2.5315" y1="3.2065" x2="-1.2115" y2="3.2215" layer="21"/>
<rectangle x1="-0.8215" y1="3.2065" x2="0.6785" y2="3.2215" layer="21"/>
<rectangle x1="0.8885" y1="3.2065" x2="1.3385" y2="3.2215" layer="21"/>
<rectangle x1="1.9385" y1="3.2065" x2="2.3885" y2="3.2215" layer="21"/>
<rectangle x1="-2.5315" y1="3.2215" x2="-1.2265" y2="3.2365" layer="21"/>
<rectangle x1="-0.8215" y1="3.2215" x2="0.6785" y2="3.2365" layer="21"/>
<rectangle x1="0.8885" y1="3.2215" x2="1.3385" y2="3.2365" layer="21"/>
<rectangle x1="1.9385" y1="3.2215" x2="2.3885" y2="3.2365" layer="21"/>
<rectangle x1="-2.5315" y1="3.2365" x2="-1.2565" y2="3.2515" layer="21"/>
<rectangle x1="-0.8215" y1="3.2365" x2="0.6785" y2="3.2515" layer="21"/>
<rectangle x1="0.8885" y1="3.2365" x2="1.3385" y2="3.2515" layer="21"/>
<rectangle x1="1.9385" y1="3.2365" x2="2.3885" y2="3.2515" layer="21"/>
<rectangle x1="-2.5315" y1="3.2515" x2="-1.2715" y2="3.2665" layer="21"/>
<rectangle x1="-0.8215" y1="3.2515" x2="0.6785" y2="3.2665" layer="21"/>
<rectangle x1="0.8885" y1="3.2515" x2="1.3385" y2="3.2665" layer="21"/>
<rectangle x1="1.9385" y1="3.2515" x2="2.3885" y2="3.2665" layer="21"/>
<rectangle x1="-2.5315" y1="3.2665" x2="-1.2865" y2="3.2815" layer="21"/>
<rectangle x1="-0.8215" y1="3.2665" x2="0.6785" y2="3.2815" layer="21"/>
<rectangle x1="0.8885" y1="3.2665" x2="1.3385" y2="3.2815" layer="21"/>
<rectangle x1="1.9385" y1="3.2665" x2="2.3885" y2="3.2815" layer="21"/>
<rectangle x1="-2.5315" y1="3.2815" x2="-1.3165" y2="3.2965" layer="21"/>
<rectangle x1="-0.8215" y1="3.2815" x2="0.6785" y2="3.2965" layer="21"/>
<rectangle x1="0.8885" y1="3.2815" x2="1.3385" y2="3.2965" layer="21"/>
<rectangle x1="1.9385" y1="3.2815" x2="2.3885" y2="3.2965" layer="21"/>
<rectangle x1="-2.5315" y1="3.2965" x2="-1.3465" y2="3.3115" layer="21"/>
<rectangle x1="-0.8215" y1="3.2965" x2="0.6785" y2="3.3115" layer="21"/>
<rectangle x1="0.8885" y1="3.2965" x2="1.3385" y2="3.3115" layer="21"/>
<rectangle x1="1.9385" y1="3.2965" x2="2.3885" y2="3.3115" layer="21"/>
<rectangle x1="-2.5315" y1="3.3115" x2="-1.3915" y2="3.3265" layer="21"/>
<rectangle x1="-0.8215" y1="3.3115" x2="0.6785" y2="3.3265" layer="21"/>
<rectangle x1="0.8885" y1="3.3115" x2="1.3385" y2="3.3265" layer="21"/>
<rectangle x1="1.9385" y1="3.3115" x2="2.3885" y2="3.3265" layer="21"/>
<rectangle x1="-2.5315" y1="3.3265" x2="-1.4365" y2="3.3415" layer="21"/>
<rectangle x1="-0.8215" y1="3.3265" x2="0.6785" y2="3.3415" layer="21"/>
<rectangle x1="0.8885" y1="3.3265" x2="1.3385" y2="3.3415" layer="21"/>
<rectangle x1="1.9385" y1="3.3265" x2="2.3885" y2="3.3415" layer="21"/>
<rectangle x1="-2.5315" y1="3.3415" x2="-1.4965" y2="3.3565" layer="21"/>
<rectangle x1="-0.8065" y1="3.3415" x2="0.6635" y2="3.3565" layer="21"/>
<rectangle x1="0.8885" y1="3.3415" x2="1.3235" y2="3.3565" layer="21"/>
<rectangle x1="1.9385" y1="3.3415" x2="2.3885" y2="3.3565" layer="21"/>
<rectangle x1="-2.5015" y1="3.3565" x2="-1.6165" y2="3.3715" layer="21"/>
<rectangle x1="-0.7915" y1="3.3565" x2="0.6485" y2="3.3715" layer="21"/>
<rectangle x1="0.9185" y1="3.3565" x2="1.3085" y2="3.3715" layer="21"/>
<rectangle x1="1.9685" y1="3.3565" x2="2.3585" y2="3.3715" layer="21"/>
</package>
<package name="DTU-XTRASMALL-2.5MM">
<rectangle x1="-0.619125" y1="-1.616075" x2="-0.568325" y2="-1.609725" layer="21"/>
<rectangle x1="0.720725" y1="-1.616075" x2="0.771525" y2="-1.609725" layer="21"/>
<rectangle x1="-0.631825" y1="-1.609725" x2="-0.555625" y2="-1.603375" layer="21"/>
<rectangle x1="0.708025" y1="-1.609725" x2="0.784225" y2="-1.603375" layer="21"/>
<rectangle x1="-0.644525" y1="-1.603375" x2="-0.536575" y2="-1.597025" layer="21"/>
<rectangle x1="0.688975" y1="-1.603375" x2="0.796925" y2="-1.597025" layer="21"/>
<rectangle x1="-0.650875" y1="-1.597025" x2="-0.523875" y2="-1.590675" layer="21"/>
<rectangle x1="0.676275" y1="-1.597025" x2="0.803275" y2="-1.590675" layer="21"/>
<rectangle x1="-0.663575" y1="-1.590675" x2="-0.504825" y2="-1.584325" layer="21"/>
<rectangle x1="0.657225" y1="-1.590675" x2="0.815975" y2="-1.584325" layer="21"/>
<rectangle x1="-0.676275" y1="-1.584325" x2="-0.492125" y2="-1.577975" layer="21"/>
<rectangle x1="0.644525" y1="-1.584325" x2="0.828675" y2="-1.577975" layer="21"/>
<rectangle x1="-0.688975" y1="-1.577975" x2="-0.473075" y2="-1.571625" layer="21"/>
<rectangle x1="0.625475" y1="-1.577975" x2="0.841375" y2="-1.571625" layer="21"/>
<rectangle x1="-0.701675" y1="-1.571625" x2="-0.460375" y2="-1.565275" layer="21"/>
<rectangle x1="0.612775" y1="-1.571625" x2="0.854075" y2="-1.565275" layer="21"/>
<rectangle x1="-0.708025" y1="-1.565275" x2="-0.441325" y2="-1.558925" layer="21"/>
<rectangle x1="0.593725" y1="-1.565275" x2="0.860425" y2="-1.558925" layer="21"/>
<rectangle x1="-0.720725" y1="-1.558925" x2="-0.428625" y2="-1.552575" layer="21"/>
<rectangle x1="0.581025" y1="-1.558925" x2="0.873125" y2="-1.552575" layer="21"/>
<rectangle x1="-0.733425" y1="-1.552575" x2="-0.409575" y2="-1.546225" layer="21"/>
<rectangle x1="0.561975" y1="-1.552575" x2="0.885825" y2="-1.546225" layer="21"/>
<rectangle x1="-0.746125" y1="-1.546225" x2="-0.390525" y2="-1.539875" layer="21"/>
<rectangle x1="0.549275" y1="-1.546225" x2="0.898525" y2="-1.539875" layer="21"/>
<rectangle x1="-0.758825" y1="-1.539875" x2="-0.377825" y2="-1.533525" layer="21"/>
<rectangle x1="0.530225" y1="-1.539875" x2="0.911225" y2="-1.533525" layer="21"/>
<rectangle x1="-0.771525" y1="-1.533525" x2="-0.352425" y2="-1.527175" layer="21"/>
<rectangle x1="0.504825" y1="-1.533525" x2="0.923925" y2="-1.527175" layer="21"/>
<rectangle x1="-0.790575" y1="-1.527175" x2="-0.333375" y2="-1.520825" layer="21"/>
<rectangle x1="0.485775" y1="-1.527175" x2="0.942975" y2="-1.520825" layer="21"/>
<rectangle x1="-0.796925" y1="-1.520825" x2="-0.314325" y2="-1.514475" layer="21"/>
<rectangle x1="0.466725" y1="-1.520825" x2="0.955675" y2="-1.514475" layer="21"/>
<rectangle x1="-0.809625" y1="-1.514475" x2="-0.295275" y2="-1.508125" layer="21"/>
<rectangle x1="0.447675" y1="-1.514475" x2="0.962025" y2="-1.508125" layer="21"/>
<rectangle x1="-0.822325" y1="-1.508125" x2="-0.276225" y2="-1.501775" layer="21"/>
<rectangle x1="0.428625" y1="-1.508125" x2="0.974725" y2="-1.501775" layer="21"/>
<rectangle x1="-0.835025" y1="-1.501775" x2="-0.263525" y2="-1.495425" layer="21"/>
<rectangle x1="0.415925" y1="-1.501775" x2="0.987425" y2="-1.495425" layer="21"/>
<rectangle x1="-0.847725" y1="-1.495425" x2="-0.244475" y2="-1.489075" layer="21"/>
<rectangle x1="0.396875" y1="-1.495425" x2="1.000125" y2="-1.489075" layer="21"/>
<rectangle x1="-0.854075" y1="-1.489075" x2="-0.225425" y2="-1.482725" layer="21"/>
<rectangle x1="0.377825" y1="-1.489075" x2="1.006475" y2="-1.482725" layer="21"/>
<rectangle x1="-0.866775" y1="-1.482725" x2="-0.200025" y2="-1.476375" layer="21"/>
<rectangle x1="0.352425" y1="-1.482725" x2="1.019175" y2="-1.476375" layer="21"/>
<rectangle x1="-0.879475" y1="-1.476375" x2="-0.180975" y2="-1.470025" layer="21"/>
<rectangle x1="0.333375" y1="-1.476375" x2="1.031875" y2="-1.470025" layer="21"/>
<rectangle x1="-0.892175" y1="-1.470025" x2="-0.161925" y2="-1.463675" layer="21"/>
<rectangle x1="0.314325" y1="-1.470025" x2="1.044575" y2="-1.463675" layer="21"/>
<rectangle x1="-0.904875" y1="-1.463675" x2="-0.136525" y2="-1.457325" layer="21"/>
<rectangle x1="0.288925" y1="-1.463675" x2="1.057275" y2="-1.457325" layer="21"/>
<rectangle x1="-0.917575" y1="-1.457325" x2="-0.111125" y2="-1.450975" layer="21"/>
<rectangle x1="0.263525" y1="-1.457325" x2="1.069975" y2="-1.450975" layer="21"/>
<rectangle x1="-0.930275" y1="-1.450975" x2="-0.079375" y2="-1.444625" layer="21"/>
<rectangle x1="0.231775" y1="-1.450975" x2="1.082675" y2="-1.444625" layer="21"/>
<rectangle x1="-0.942975" y1="-1.444625" x2="-0.041275" y2="-1.438275" layer="21"/>
<rectangle x1="0.193675" y1="-1.444625" x2="1.095375" y2="-1.438275" layer="21"/>
<rectangle x1="-0.955675" y1="-1.438275" x2="1.108075" y2="-1.431925" layer="21"/>
<rectangle x1="-0.968375" y1="-1.431925" x2="1.120775" y2="-1.425575" layer="21"/>
<rectangle x1="-0.981075" y1="-1.425575" x2="1.133475" y2="-1.419225" layer="21"/>
<rectangle x1="-0.993775" y1="-1.419225" x2="1.146175" y2="-1.412875" layer="21"/>
<rectangle x1="-1.006475" y1="-1.412875" x2="1.158875" y2="-1.406525" layer="21"/>
<rectangle x1="-1.012825" y1="-1.406525" x2="1.165225" y2="-1.400175" layer="21"/>
<rectangle x1="-1.025525" y1="-1.400175" x2="1.177925" y2="-1.393825" layer="21"/>
<rectangle x1="-1.038225" y1="-1.393825" x2="1.190625" y2="-1.387475" layer="21"/>
<rectangle x1="-1.050925" y1="-1.387475" x2="1.203325" y2="-1.381125" layer="21"/>
<rectangle x1="-1.063625" y1="-1.381125" x2="1.216025" y2="-1.374775" layer="21"/>
<rectangle x1="-1.069975" y1="-1.374775" x2="1.228725" y2="-1.368425" layer="21"/>
<rectangle x1="-1.082675" y1="-1.368425" x2="1.235075" y2="-1.362075" layer="21"/>
<rectangle x1="-1.095375" y1="-1.362075" x2="1.247775" y2="-1.355725" layer="21"/>
<rectangle x1="-1.095375" y1="-1.355725" x2="1.247775" y2="-1.349375" layer="21"/>
<rectangle x1="-1.095375" y1="-1.349375" x2="1.247775" y2="-1.343025" layer="21"/>
<rectangle x1="-1.082675" y1="-1.343025" x2="1.235075" y2="-1.336675" layer="21"/>
<rectangle x1="-1.069975" y1="-1.336675" x2="1.222375" y2="-1.330325" layer="21"/>
<rectangle x1="-1.057275" y1="-1.330325" x2="1.209675" y2="-1.323975" layer="21"/>
<rectangle x1="-1.044575" y1="-1.323975" x2="1.196975" y2="-1.317625" layer="21"/>
<rectangle x1="-1.031875" y1="-1.317625" x2="1.184275" y2="-1.311275" layer="21"/>
<rectangle x1="-1.019175" y1="-1.311275" x2="1.171575" y2="-1.304925" layer="21"/>
<rectangle x1="-1.006475" y1="-1.304925" x2="1.158875" y2="-1.298575" layer="21"/>
<rectangle x1="-0.993775" y1="-1.298575" x2="1.152525" y2="-1.292225" layer="21"/>
<rectangle x1="-0.987425" y1="-1.292225" x2="1.139825" y2="-1.285875" layer="21"/>
<rectangle x1="-0.974725" y1="-1.285875" x2="1.127125" y2="-1.279525" layer="21"/>
<rectangle x1="-0.962025" y1="-1.279525" x2="1.114425" y2="-1.273175" layer="21"/>
<rectangle x1="-0.949325" y1="-1.273175" x2="0.003175" y2="-1.266825" layer="21"/>
<rectangle x1="0.149225" y1="-1.273175" x2="1.101725" y2="-1.266825" layer="21"/>
<rectangle x1="-0.942975" y1="-1.266825" x2="-0.041275" y2="-1.260475" layer="21"/>
<rectangle x1="0.193675" y1="-1.266825" x2="1.095375" y2="-1.260475" layer="21"/>
<rectangle x1="-0.930275" y1="-1.260475" x2="-0.079375" y2="-1.254125" layer="21"/>
<rectangle x1="0.231775" y1="-1.260475" x2="1.082675" y2="-1.254125" layer="21"/>
<rectangle x1="-0.917575" y1="-1.254125" x2="-0.104775" y2="-1.247775" layer="21"/>
<rectangle x1="0.263525" y1="-1.254125" x2="1.069975" y2="-1.247775" layer="21"/>
<rectangle x1="-0.898525" y1="-1.247775" x2="-0.149225" y2="-1.241425" layer="21"/>
<rectangle x1="0.301625" y1="-1.247775" x2="1.050925" y2="-1.241425" layer="21"/>
<rectangle x1="-0.885825" y1="-1.241425" x2="-0.168275" y2="-1.235075" layer="21"/>
<rectangle x1="0.327025" y1="-1.241425" x2="1.038225" y2="-1.235075" layer="21"/>
<rectangle x1="-0.873125" y1="-1.235075" x2="-0.193675" y2="-1.228725" layer="21"/>
<rectangle x1="0.346075" y1="-1.235075" x2="1.025525" y2="-1.228725" layer="21"/>
<rectangle x1="-0.860425" y1="-1.228725" x2="-0.212725" y2="-1.222375" layer="21"/>
<rectangle x1="0.365125" y1="-1.228725" x2="1.012825" y2="-1.222375" layer="21"/>
<rectangle x1="-0.847725" y1="-1.222375" x2="-0.238125" y2="-1.216025" layer="21"/>
<rectangle x1="0.390525" y1="-1.222375" x2="1.000125" y2="-1.216025" layer="21"/>
<rectangle x1="-0.841375" y1="-1.216025" x2="-0.257175" y2="-1.209675" layer="21"/>
<rectangle x1="0.409575" y1="-1.216025" x2="0.993775" y2="-1.209675" layer="21"/>
<rectangle x1="-0.828675" y1="-1.209675" x2="-0.269875" y2="-1.203325" layer="21"/>
<rectangle x1="0.422275" y1="-1.209675" x2="0.981075" y2="-1.203325" layer="21"/>
<rectangle x1="-0.815975" y1="-1.203325" x2="-0.288925" y2="-1.196975" layer="21"/>
<rectangle x1="0.441325" y1="-1.203325" x2="0.968375" y2="-1.196975" layer="21"/>
<rectangle x1="-0.803275" y1="-1.196975" x2="-0.307975" y2="-1.190625" layer="21"/>
<rectangle x1="0.460375" y1="-1.196975" x2="0.955675" y2="-1.190625" layer="21"/>
<rectangle x1="-0.790575" y1="-1.190625" x2="-0.327025" y2="-1.184275" layer="21"/>
<rectangle x1="0.479425" y1="-1.190625" x2="0.942975" y2="-1.184275" layer="21"/>
<rectangle x1="-0.784225" y1="-1.184275" x2="-0.339725" y2="-1.177925" layer="21"/>
<rectangle x1="0.498475" y1="-1.184275" x2="0.936625" y2="-1.177925" layer="21"/>
<rectangle x1="-0.771525" y1="-1.177925" x2="-0.358775" y2="-1.171575" layer="21"/>
<rectangle x1="0.511175" y1="-1.177925" x2="0.923925" y2="-1.171575" layer="21"/>
<rectangle x1="-0.758825" y1="-1.171575" x2="-0.377825" y2="-1.165225" layer="21"/>
<rectangle x1="0.530225" y1="-1.171575" x2="0.911225" y2="-1.165225" layer="21"/>
<rectangle x1="-0.746125" y1="-1.165225" x2="-0.390525" y2="-1.158875" layer="21"/>
<rectangle x1="0.542925" y1="-1.165225" x2="0.898525" y2="-1.158875" layer="21"/>
<rectangle x1="-0.733425" y1="-1.158875" x2="-0.415925" y2="-1.152525" layer="21"/>
<rectangle x1="0.568325" y1="-1.158875" x2="0.885825" y2="-1.152525" layer="21"/>
<rectangle x1="-0.714375" y1="-1.152525" x2="-0.434975" y2="-1.146175" layer="21"/>
<rectangle x1="0.587375" y1="-1.152525" x2="0.866775" y2="-1.146175" layer="21"/>
<rectangle x1="-0.701675" y1="-1.146175" x2="-0.454025" y2="-1.139825" layer="21"/>
<rectangle x1="0.606425" y1="-1.146175" x2="0.854075" y2="-1.139825" layer="21"/>
<rectangle x1="-0.695325" y1="-1.139825" x2="-0.466725" y2="-1.133475" layer="21"/>
<rectangle x1="0.619125" y1="-1.139825" x2="0.847725" y2="-1.133475" layer="21"/>
<rectangle x1="-0.682625" y1="-1.133475" x2="-0.485775" y2="-1.127125" layer="21"/>
<rectangle x1="0.638175" y1="-1.133475" x2="0.835025" y2="-1.127125" layer="21"/>
<rectangle x1="-0.669925" y1="-1.127125" x2="-0.498475" y2="-1.120775" layer="21"/>
<rectangle x1="0.650875" y1="-1.127125" x2="0.822325" y2="-1.120775" layer="21"/>
<rectangle x1="-0.657225" y1="-1.120775" x2="-0.517525" y2="-1.114425" layer="21"/>
<rectangle x1="0.669925" y1="-1.120775" x2="0.809625" y2="-1.114425" layer="21"/>
<rectangle x1="-0.644525" y1="-1.114425" x2="-0.530225" y2="-1.108075" layer="21"/>
<rectangle x1="0.682625" y1="-1.114425" x2="0.796925" y2="-1.108075" layer="21"/>
<rectangle x1="-0.631825" y1="-1.108075" x2="-0.549275" y2="-1.101725" layer="21"/>
<rectangle x1="0.701675" y1="-1.108075" x2="0.784225" y2="-1.101725" layer="21"/>
<rectangle x1="-0.625475" y1="-1.101725" x2="-0.561975" y2="-1.095375" layer="21"/>
<rectangle x1="0.714375" y1="-1.101725" x2="0.777875" y2="-1.095375" layer="21"/>
<rectangle x1="-0.612775" y1="-1.095375" x2="-0.581025" y2="-1.089025" layer="21"/>
<rectangle x1="0.733425" y1="-1.095375" x2="0.765175" y2="-1.089025" layer="21"/>
<rectangle x1="-0.600075" y1="-1.089025" x2="-0.593725" y2="-1.082675" layer="21"/>
<rectangle x1="0.746125" y1="-1.089025" x2="0.752475" y2="-1.082675" layer="21"/>
<rectangle x1="-0.606425" y1="-0.974725" x2="-0.587375" y2="-0.968375" layer="21"/>
<rectangle x1="0.739775" y1="-0.974725" x2="0.758825" y2="-0.968375" layer="21"/>
<rectangle x1="-0.619125" y1="-0.968375" x2="-0.568325" y2="-0.962025" layer="21"/>
<rectangle x1="0.720725" y1="-0.968375" x2="0.771525" y2="-0.962025" layer="21"/>
<rectangle x1="-0.631825" y1="-0.962025" x2="-0.555625" y2="-0.955675" layer="21"/>
<rectangle x1="0.708025" y1="-0.962025" x2="0.784225" y2="-0.955675" layer="21"/>
<rectangle x1="-0.638175" y1="-0.955675" x2="-0.542925" y2="-0.949325" layer="21"/>
<rectangle x1="0.695325" y1="-0.955675" x2="0.790575" y2="-0.949325" layer="21"/>
<rectangle x1="-0.650875" y1="-0.949325" x2="-0.523875" y2="-0.942975" layer="21"/>
<rectangle x1="0.676275" y1="-0.949325" x2="0.803275" y2="-0.942975" layer="21"/>
<rectangle x1="-0.663575" y1="-0.942975" x2="-0.511175" y2="-0.936625" layer="21"/>
<rectangle x1="0.663575" y1="-0.942975" x2="0.815975" y2="-0.936625" layer="21"/>
<rectangle x1="-0.676275" y1="-0.936625" x2="-0.492125" y2="-0.930275" layer="21"/>
<rectangle x1="0.644525" y1="-0.936625" x2="0.828675" y2="-0.930275" layer="21"/>
<rectangle x1="-0.688975" y1="-0.930275" x2="-0.479425" y2="-0.923925" layer="21"/>
<rectangle x1="0.631825" y1="-0.930275" x2="0.841375" y2="-0.923925" layer="21"/>
<rectangle x1="-0.695325" y1="-0.923925" x2="-0.460375" y2="-0.917575" layer="21"/>
<rectangle x1="0.612775" y1="-0.923925" x2="0.847725" y2="-0.917575" layer="21"/>
<rectangle x1="-0.708025" y1="-0.917575" x2="-0.447675" y2="-0.911225" layer="21"/>
<rectangle x1="0.600075" y1="-0.917575" x2="0.860425" y2="-0.911225" layer="21"/>
<rectangle x1="-0.720725" y1="-0.911225" x2="-0.428625" y2="-0.904875" layer="21"/>
<rectangle x1="0.581025" y1="-0.911225" x2="0.873125" y2="-0.904875" layer="21"/>
<rectangle x1="-0.733425" y1="-0.904875" x2="-0.409575" y2="-0.898525" layer="21"/>
<rectangle x1="0.561975" y1="-0.904875" x2="0.885825" y2="-0.898525" layer="21"/>
<rectangle x1="-0.746125" y1="-0.898525" x2="-0.396875" y2="-0.892175" layer="21"/>
<rectangle x1="0.542925" y1="-0.898525" x2="0.898525" y2="-0.892175" layer="21"/>
<rectangle x1="-0.765175" y1="-0.892175" x2="-0.365125" y2="-0.885825" layer="21"/>
<rectangle x1="0.523875" y1="-0.892175" x2="0.917575" y2="-0.885825" layer="21"/>
<rectangle x1="-0.777875" y1="-0.885825" x2="-0.352425" y2="-0.879475" layer="21"/>
<rectangle x1="0.504825" y1="-0.885825" x2="0.930275" y2="-0.879475" layer="21"/>
<rectangle x1="-0.784225" y1="-0.879475" x2="-0.333375" y2="-0.873125" layer="21"/>
<rectangle x1="0.485775" y1="-0.879475" x2="0.936625" y2="-0.873125" layer="21"/>
<rectangle x1="-0.796925" y1="-0.873125" x2="-0.314325" y2="-0.866775" layer="21"/>
<rectangle x1="0.466725" y1="-0.873125" x2="0.949325" y2="-0.866775" layer="21"/>
<rectangle x1="-0.809625" y1="-0.866775" x2="-0.301625" y2="-0.860425" layer="21"/>
<rectangle x1="0.454025" y1="-0.866775" x2="0.962025" y2="-0.860425" layer="21"/>
<rectangle x1="-0.822325" y1="-0.860425" x2="-0.282575" y2="-0.854075" layer="21"/>
<rectangle x1="0.434975" y1="-0.860425" x2="0.974725" y2="-0.854075" layer="21"/>
<rectangle x1="-0.835025" y1="-0.854075" x2="-0.263525" y2="-0.847725" layer="21"/>
<rectangle x1="0.415925" y1="-0.854075" x2="0.987425" y2="-0.847725" layer="21"/>
<rectangle x1="-0.841375" y1="-0.847725" x2="-0.244475" y2="-0.841375" layer="21"/>
<rectangle x1="0.396875" y1="-0.847725" x2="0.993775" y2="-0.841375" layer="21"/>
<rectangle x1="-0.854075" y1="-0.841375" x2="-0.225425" y2="-0.835025" layer="21"/>
<rectangle x1="0.377825" y1="-0.841375" x2="1.006475" y2="-0.835025" layer="21"/>
<rectangle x1="-0.866775" y1="-0.835025" x2="-0.206375" y2="-0.828675" layer="21"/>
<rectangle x1="0.358775" y1="-0.835025" x2="1.019175" y2="-0.828675" layer="21"/>
<rectangle x1="-0.879475" y1="-0.828675" x2="-0.187325" y2="-0.822325" layer="21"/>
<rectangle x1="0.339725" y1="-0.828675" x2="1.031875" y2="-0.822325" layer="21"/>
<rectangle x1="-0.892175" y1="-0.822325" x2="-0.161925" y2="-0.815975" layer="21"/>
<rectangle x1="0.314325" y1="-0.822325" x2="1.044575" y2="-0.815975" layer="21"/>
<rectangle x1="-0.904875" y1="-0.815975" x2="-0.136525" y2="-0.809625" layer="21"/>
<rectangle x1="0.288925" y1="-0.815975" x2="1.057275" y2="-0.809625" layer="21"/>
<rectangle x1="-0.917575" y1="-0.809625" x2="-0.111125" y2="-0.803275" layer="21"/>
<rectangle x1="0.263525" y1="-0.809625" x2="1.069975" y2="-0.803275" layer="21"/>
<rectangle x1="-0.930275" y1="-0.803275" x2="-0.066675" y2="-0.796925" layer="21"/>
<rectangle x1="0.219075" y1="-0.803275" x2="1.082675" y2="-0.796925" layer="21"/>
<rectangle x1="-0.942975" y1="-0.796925" x2="-0.022225" y2="-0.790575" layer="21"/>
<rectangle x1="0.174625" y1="-0.796925" x2="1.095375" y2="-0.790575" layer="21"/>
<rectangle x1="-0.955675" y1="-0.790575" x2="0.047625" y2="-0.784225" layer="21"/>
<rectangle x1="0.104775" y1="-0.790575" x2="1.108075" y2="-0.784225" layer="21"/>
<rectangle x1="-0.968375" y1="-0.784225" x2="1.120775" y2="-0.777875" layer="21"/>
<rectangle x1="-0.981075" y1="-0.777875" x2="1.133475" y2="-0.771525" layer="21"/>
<rectangle x1="-0.987425" y1="-0.771525" x2="1.146175" y2="-0.765175" layer="21"/>
<rectangle x1="-1.000125" y1="-0.765175" x2="1.152525" y2="-0.758825" layer="21"/>
<rectangle x1="-1.012825" y1="-0.758825" x2="1.165225" y2="-0.752475" layer="21"/>
<rectangle x1="-1.025525" y1="-0.752475" x2="1.177925" y2="-0.746125" layer="21"/>
<rectangle x1="-1.038225" y1="-0.746125" x2="1.190625" y2="-0.739775" layer="21"/>
<rectangle x1="-1.050925" y1="-0.739775" x2="1.203325" y2="-0.733425" layer="21"/>
<rectangle x1="-1.057275" y1="-0.733425" x2="1.216025" y2="-0.727075" layer="21"/>
<rectangle x1="-1.069975" y1="-0.727075" x2="1.222375" y2="-0.720725" layer="21"/>
<rectangle x1="-1.082675" y1="-0.720725" x2="1.235075" y2="-0.714375" layer="21"/>
<rectangle x1="-1.095375" y1="-0.714375" x2="1.247775" y2="-0.708025" layer="21"/>
<rectangle x1="-1.095375" y1="-0.708025" x2="1.247775" y2="-0.701675" layer="21"/>
<rectangle x1="-1.095375" y1="-0.701675" x2="1.247775" y2="-0.695325" layer="21"/>
<rectangle x1="-1.082675" y1="-0.695325" x2="1.235075" y2="-0.688975" layer="21"/>
<rectangle x1="-1.069975" y1="-0.688975" x2="1.222375" y2="-0.682625" layer="21"/>
<rectangle x1="-1.057275" y1="-0.682625" x2="1.209675" y2="-0.676275" layer="21"/>
<rectangle x1="-1.044575" y1="-0.676275" x2="1.196975" y2="-0.669925" layer="21"/>
<rectangle x1="-1.031875" y1="-0.669925" x2="1.184275" y2="-0.663575" layer="21"/>
<rectangle x1="-1.019175" y1="-0.663575" x2="1.171575" y2="-0.657225" layer="21"/>
<rectangle x1="-1.012825" y1="-0.657225" x2="1.165225" y2="-0.650875" layer="21"/>
<rectangle x1="-1.000125" y1="-0.650875" x2="1.152525" y2="-0.644525" layer="21"/>
<rectangle x1="-0.987425" y1="-0.644525" x2="1.139825" y2="-0.638175" layer="21"/>
<rectangle x1="-0.974725" y1="-0.638175" x2="1.127125" y2="-0.631825" layer="21"/>
<rectangle x1="-0.962025" y1="-0.631825" x2="1.114425" y2="-0.625475" layer="21"/>
<rectangle x1="-0.955675" y1="-0.625475" x2="0.015875" y2="-0.619125" layer="21"/>
<rectangle x1="0.136525" y1="-0.625475" x2="1.108075" y2="-0.619125" layer="21"/>
<rectangle x1="-0.942975" y1="-0.619125" x2="-0.034925" y2="-0.612775" layer="21"/>
<rectangle x1="0.187325" y1="-0.619125" x2="1.095375" y2="-0.612775" layer="21"/>
<rectangle x1="-0.923925" y1="-0.612775" x2="-0.092075" y2="-0.606425" layer="21"/>
<rectangle x1="0.244475" y1="-0.612775" x2="1.076325" y2="-0.606425" layer="21"/>
<rectangle x1="-0.911225" y1="-0.606425" x2="-0.117475" y2="-0.600075" layer="21"/>
<rectangle x1="0.276225" y1="-0.606425" x2="1.063625" y2="-0.600075" layer="21"/>
<rectangle x1="-0.898525" y1="-0.600075" x2="-0.149225" y2="-0.593725" layer="21"/>
<rectangle x1="0.301625" y1="-0.600075" x2="1.050925" y2="-0.593725" layer="21"/>
<rectangle x1="-0.885825" y1="-0.593725" x2="-0.168275" y2="-0.587375" layer="21"/>
<rectangle x1="0.320675" y1="-0.593725" x2="1.038225" y2="-0.587375" layer="21"/>
<rectangle x1="-0.873125" y1="-0.587375" x2="-0.193675" y2="-0.581025" layer="21"/>
<rectangle x1="0.346075" y1="-0.587375" x2="1.025525" y2="-0.581025" layer="21"/>
<rectangle x1="-0.860425" y1="-0.581025" x2="-0.212725" y2="-0.574675" layer="21"/>
<rectangle x1="0.365125" y1="-0.581025" x2="1.012825" y2="-0.574675" layer="21"/>
<rectangle x1="-0.854075" y1="-0.574675" x2="-0.231775" y2="-0.568325" layer="21"/>
<rectangle x1="0.384175" y1="-0.574675" x2="1.006475" y2="-0.568325" layer="21"/>
<rectangle x1="-0.841375" y1="-0.568325" x2="-0.250825" y2="-0.561975" layer="21"/>
<rectangle x1="0.403225" y1="-0.568325" x2="0.993775" y2="-0.561975" layer="21"/>
<rectangle x1="-0.828675" y1="-0.561975" x2="-0.269875" y2="-0.555625" layer="21"/>
<rectangle x1="0.422275" y1="-0.561975" x2="0.981075" y2="-0.555625" layer="21"/>
<rectangle x1="-0.815975" y1="-0.555625" x2="-0.288925" y2="-0.549275" layer="21"/>
<rectangle x1="0.441325" y1="-0.555625" x2="0.968375" y2="-0.549275" layer="21"/>
<rectangle x1="-0.803275" y1="-0.549275" x2="-0.307975" y2="-0.542925" layer="21"/>
<rectangle x1="0.460375" y1="-0.549275" x2="0.955675" y2="-0.542925" layer="21"/>
<rectangle x1="-0.796925" y1="-0.542925" x2="-0.320675" y2="-0.536575" layer="21"/>
<rectangle x1="0.473075" y1="-0.542925" x2="0.949325" y2="-0.536575" layer="21"/>
<rectangle x1="-0.784225" y1="-0.536575" x2="-0.339725" y2="-0.530225" layer="21"/>
<rectangle x1="0.492125" y1="-0.536575" x2="0.936625" y2="-0.530225" layer="21"/>
<rectangle x1="-0.771525" y1="-0.530225" x2="-0.358775" y2="-0.523875" layer="21"/>
<rectangle x1="0.511175" y1="-0.530225" x2="0.923925" y2="-0.523875" layer="21"/>
<rectangle x1="-0.758825" y1="-0.523875" x2="-0.377825" y2="-0.517525" layer="21"/>
<rectangle x1="0.530225" y1="-0.523875" x2="0.911225" y2="-0.517525" layer="21"/>
<rectangle x1="-0.739775" y1="-0.517525" x2="-0.396875" y2="-0.511175" layer="21"/>
<rectangle x1="0.549275" y1="-0.517525" x2="0.892175" y2="-0.511175" layer="21"/>
<rectangle x1="-0.727075" y1="-0.511175" x2="-0.415925" y2="-0.504825" layer="21"/>
<rectangle x1="0.568325" y1="-0.511175" x2="0.879475" y2="-0.504825" layer="21"/>
<rectangle x1="-0.714375" y1="-0.504825" x2="-0.434975" y2="-0.498475" layer="21"/>
<rectangle x1="0.587375" y1="-0.504825" x2="0.873125" y2="-0.498475" layer="21"/>
<rectangle x1="-0.708025" y1="-0.498475" x2="-0.454025" y2="-0.492125" layer="21"/>
<rectangle x1="0.606425" y1="-0.498475" x2="0.860425" y2="-0.492125" layer="21"/>
<rectangle x1="-0.695325" y1="-0.492125" x2="-0.466725" y2="-0.485775" layer="21"/>
<rectangle x1="0.619125" y1="-0.492125" x2="0.847725" y2="-0.485775" layer="21"/>
<rectangle x1="-0.682625" y1="-0.485775" x2="-0.485775" y2="-0.479425" layer="21"/>
<rectangle x1="0.638175" y1="-0.485775" x2="0.835025" y2="-0.479425" layer="21"/>
<rectangle x1="-0.669925" y1="-0.479425" x2="-0.498475" y2="-0.473075" layer="21"/>
<rectangle x1="0.650875" y1="-0.479425" x2="0.822325" y2="-0.473075" layer="21"/>
<rectangle x1="-0.657225" y1="-0.473075" x2="-0.517525" y2="-0.466725" layer="21"/>
<rectangle x1="0.669925" y1="-0.473075" x2="0.809625" y2="-0.466725" layer="21"/>
<rectangle x1="-0.650875" y1="-0.466725" x2="-0.530225" y2="-0.460375" layer="21"/>
<rectangle x1="0.682625" y1="-0.466725" x2="0.803275" y2="-0.460375" layer="21"/>
<rectangle x1="-0.638175" y1="-0.460375" x2="-0.542925" y2="-0.454025" layer="21"/>
<rectangle x1="0.695325" y1="-0.460375" x2="0.790575" y2="-0.454025" layer="21"/>
<rectangle x1="-0.625475" y1="-0.454025" x2="-0.561975" y2="-0.447675" layer="21"/>
<rectangle x1="0.714375" y1="-0.454025" x2="0.777875" y2="-0.447675" layer="21"/>
<rectangle x1="-0.612775" y1="-0.447675" x2="-0.574675" y2="-0.441325" layer="21"/>
<rectangle x1="0.727075" y1="-0.447675" x2="0.765175" y2="-0.441325" layer="21"/>
<rectangle x1="-0.600075" y1="-0.441325" x2="-0.593725" y2="-0.434975" layer="21"/>
<rectangle x1="0.746125" y1="-0.441325" x2="0.752475" y2="-0.434975" layer="21"/>
<rectangle x1="-0.606425" y1="-0.327025" x2="-0.587375" y2="-0.320675" layer="21"/>
<rectangle x1="0.739775" y1="-0.327025" x2="0.758825" y2="-0.320675" layer="21"/>
<rectangle x1="-0.612775" y1="-0.320675" x2="-0.574675" y2="-0.314325" layer="21"/>
<rectangle x1="0.727075" y1="-0.320675" x2="0.765175" y2="-0.314325" layer="21"/>
<rectangle x1="-0.625475" y1="-0.314325" x2="-0.555625" y2="-0.307975" layer="21"/>
<rectangle x1="0.708025" y1="-0.314325" x2="0.777875" y2="-0.307975" layer="21"/>
<rectangle x1="-0.638175" y1="-0.307975" x2="-0.542925" y2="-0.301625" layer="21"/>
<rectangle x1="0.695325" y1="-0.307975" x2="0.790575" y2="-0.301625" layer="21"/>
<rectangle x1="-0.650875" y1="-0.301625" x2="-0.530225" y2="-0.295275" layer="21"/>
<rectangle x1="0.682625" y1="-0.301625" x2="0.803275" y2="-0.295275" layer="21"/>
<rectangle x1="-0.663575" y1="-0.295275" x2="-0.511175" y2="-0.288925" layer="21"/>
<rectangle x1="0.663575" y1="-0.295275" x2="0.815975" y2="-0.288925" layer="21"/>
<rectangle x1="-0.676275" y1="-0.288925" x2="-0.498475" y2="-0.282575" layer="21"/>
<rectangle x1="0.650875" y1="-0.288925" x2="0.822325" y2="-0.282575" layer="21"/>
<rectangle x1="-0.682625" y1="-0.282575" x2="-0.479425" y2="-0.276225" layer="21"/>
<rectangle x1="0.631825" y1="-0.282575" x2="0.835025" y2="-0.276225" layer="21"/>
<rectangle x1="-0.695325" y1="-0.276225" x2="-0.460375" y2="-0.269875" layer="21"/>
<rectangle x1="0.612775" y1="-0.276225" x2="0.847725" y2="-0.269875" layer="21"/>
<rectangle x1="-0.708025" y1="-0.269875" x2="-0.447675" y2="-0.263525" layer="21"/>
<rectangle x1="0.600075" y1="-0.269875" x2="0.860425" y2="-0.263525" layer="21"/>
<rectangle x1="-0.720725" y1="-0.263525" x2="-0.428625" y2="-0.257175" layer="21"/>
<rectangle x1="0.581025" y1="-0.263525" x2="0.873125" y2="-0.257175" layer="21"/>
<rectangle x1="-0.739775" y1="-0.257175" x2="-0.403225" y2="-0.250825" layer="21"/>
<rectangle x1="0.555625" y1="-0.257175" x2="0.892175" y2="-0.250825" layer="21"/>
<rectangle x1="-0.752475" y1="-0.250825" x2="-0.384175" y2="-0.244475" layer="21"/>
<rectangle x1="0.536575" y1="-0.250825" x2="0.904875" y2="-0.244475" layer="21"/>
<rectangle x1="-0.765175" y1="-0.244475" x2="-0.371475" y2="-0.238125" layer="21"/>
<rectangle x1="0.523875" y1="-0.244475" x2="0.917575" y2="-0.238125" layer="21"/>
<rectangle x1="-0.771525" y1="-0.238125" x2="-0.352425" y2="-0.231775" layer="21"/>
<rectangle x1="0.504825" y1="-0.238125" x2="0.923925" y2="-0.231775" layer="21"/>
<rectangle x1="-0.784225" y1="-0.231775" x2="-0.339725" y2="-0.225425" layer="21"/>
<rectangle x1="0.492125" y1="-0.231775" x2="0.936625" y2="-0.225425" layer="21"/>
<rectangle x1="-0.796925" y1="-0.225425" x2="-0.320675" y2="-0.219075" layer="21"/>
<rectangle x1="0.473075" y1="-0.225425" x2="0.949325" y2="-0.219075" layer="21"/>
<rectangle x1="-0.809625" y1="-0.219075" x2="-0.301625" y2="-0.212725" layer="21"/>
<rectangle x1="0.454025" y1="-0.219075" x2="0.962025" y2="-0.212725" layer="21"/>
<rectangle x1="-0.822325" y1="-0.212725" x2="-0.282575" y2="-0.206375" layer="21"/>
<rectangle x1="0.434975" y1="-0.212725" x2="0.974725" y2="-0.206375" layer="21"/>
<rectangle x1="-0.828675" y1="-0.206375" x2="-0.263525" y2="-0.200025" layer="21"/>
<rectangle x1="0.422275" y1="-0.206375" x2="0.981075" y2="-0.200025" layer="21"/>
<rectangle x1="-0.841375" y1="-0.200025" x2="-0.250825" y2="-0.193675" layer="21"/>
<rectangle x1="0.403225" y1="-0.200025" x2="0.993775" y2="-0.193675" layer="21"/>
<rectangle x1="-0.854075" y1="-0.193675" x2="-0.225425" y2="-0.187325" layer="21"/>
<rectangle x1="0.384175" y1="-0.193675" x2="1.006475" y2="-0.187325" layer="21"/>
<rectangle x1="-0.866775" y1="-0.187325" x2="-0.206375" y2="-0.180975" layer="21"/>
<rectangle x1="0.358775" y1="-0.187325" x2="1.019175" y2="-0.180975" layer="21"/>
<rectangle x1="-0.879475" y1="-0.180975" x2="-0.187325" y2="-0.174625" layer="21"/>
<rectangle x1="0.339725" y1="-0.180975" x2="1.031875" y2="-0.174625" layer="21"/>
<rectangle x1="-0.892175" y1="-0.174625" x2="-0.161925" y2="-0.168275" layer="21"/>
<rectangle x1="0.314325" y1="-0.174625" x2="1.044575" y2="-0.168275" layer="21"/>
<rectangle x1="-0.904875" y1="-0.168275" x2="-0.130175" y2="-0.161925" layer="21"/>
<rectangle x1="0.288925" y1="-0.168275" x2="1.057275" y2="-0.161925" layer="21"/>
<rectangle x1="-0.917575" y1="-0.161925" x2="-0.098425" y2="-0.155575" layer="21"/>
<rectangle x1="0.250825" y1="-0.161925" x2="1.069975" y2="-0.155575" layer="21"/>
<rectangle x1="-0.930275" y1="-0.155575" x2="-0.066675" y2="-0.149225" layer="21"/>
<rectangle x1="0.219075" y1="-0.155575" x2="1.082675" y2="-0.149225" layer="21"/>
<rectangle x1="-0.942975" y1="-0.149225" x2="-0.028575" y2="-0.142875" layer="21"/>
<rectangle x1="0.180975" y1="-0.149225" x2="1.095375" y2="-0.142875" layer="21"/>
<rectangle x1="-0.955675" y1="-0.142875" x2="0.028575" y2="-0.136525" layer="21"/>
<rectangle x1="0.123825" y1="-0.142875" x2="1.108075" y2="-0.136525" layer="21"/>
<rectangle x1="-0.968375" y1="-0.136525" x2="1.120775" y2="-0.130175" layer="21"/>
<rectangle x1="-0.974725" y1="-0.130175" x2="1.127125" y2="-0.123825" layer="21"/>
<rectangle x1="-0.987425" y1="-0.123825" x2="1.139825" y2="-0.117475" layer="21"/>
<rectangle x1="-1.000125" y1="-0.117475" x2="1.152525" y2="-0.111125" layer="21"/>
<rectangle x1="-1.012825" y1="-0.111125" x2="1.165225" y2="-0.104775" layer="21"/>
<rectangle x1="-1.025525" y1="-0.104775" x2="1.177925" y2="-0.098425" layer="21"/>
<rectangle x1="-1.038225" y1="-0.098425" x2="1.190625" y2="-0.092075" layer="21"/>
<rectangle x1="-1.044575" y1="-0.092075" x2="1.203325" y2="-0.085725" layer="21"/>
<rectangle x1="-1.057275" y1="-0.085725" x2="1.216025" y2="-0.079375" layer="21"/>
<rectangle x1="-1.069975" y1="-0.079375" x2="1.228725" y2="-0.073025" layer="21"/>
<rectangle x1="-1.089025" y1="-0.073025" x2="1.241425" y2="-0.066675" layer="21"/>
<rectangle x1="-1.095375" y1="-0.066675" x2="1.247775" y2="-0.060325" layer="21"/>
<rectangle x1="-1.095375" y1="-0.060325" x2="1.247775" y2="-0.053975" layer="21"/>
<rectangle x1="-1.095375" y1="-0.053975" x2="1.247775" y2="-0.047625" layer="21"/>
<rectangle x1="-1.082675" y1="-0.047625" x2="1.235075" y2="-0.041275" layer="21"/>
<rectangle x1="-1.069975" y1="-0.041275" x2="1.222375" y2="-0.034925" layer="21"/>
<rectangle x1="-1.057275" y1="-0.034925" x2="1.209675" y2="-0.028575" layer="21"/>
<rectangle x1="-1.044575" y1="-0.028575" x2="1.196975" y2="-0.022225" layer="21"/>
<rectangle x1="-1.031875" y1="-0.022225" x2="1.184275" y2="-0.015875" layer="21"/>
<rectangle x1="-1.025525" y1="-0.015875" x2="1.177925" y2="-0.009525" layer="21"/>
<rectangle x1="-1.012825" y1="-0.009525" x2="1.165225" y2="-0.003175" layer="21"/>
<rectangle x1="-1.000125" y1="-0.003175" x2="1.152525" y2="0.003175" layer="21"/>
<rectangle x1="-0.987425" y1="0.003175" x2="1.139825" y2="0.009525" layer="21"/>
<rectangle x1="-0.974725" y1="0.009525" x2="1.127125" y2="0.015875" layer="21"/>
<rectangle x1="-0.968375" y1="0.015875" x2="1.120775" y2="0.022225" layer="21"/>
<rectangle x1="-0.949325" y1="0.022225" x2="0.015875" y2="0.028575" layer="21"/>
<rectangle x1="0.136525" y1="0.022225" x2="1.108075" y2="0.028575" layer="21"/>
<rectangle x1="-0.936625" y1="0.028575" x2="-0.053975" y2="0.034925" layer="21"/>
<rectangle x1="0.206375" y1="0.028575" x2="1.089025" y2="0.034925" layer="21"/>
<rectangle x1="-0.923925" y1="0.034925" x2="-0.092075" y2="0.041275" layer="21"/>
<rectangle x1="0.244475" y1="0.034925" x2="1.076325" y2="0.041275" layer="21"/>
<rectangle x1="-0.911225" y1="0.041275" x2="-0.117475" y2="0.047625" layer="21"/>
<rectangle x1="0.269875" y1="0.041275" x2="1.063625" y2="0.047625" layer="21"/>
<rectangle x1="-0.898525" y1="0.047625" x2="-0.142875" y2="0.053975" layer="21"/>
<rectangle x1="0.295275" y1="0.047625" x2="1.050925" y2="0.053975" layer="21"/>
<rectangle x1="-0.885825" y1="0.053975" x2="-0.168275" y2="0.060325" layer="21"/>
<rectangle x1="0.320675" y1="0.053975" x2="1.038225" y2="0.060325" layer="21"/>
<rectangle x1="-0.873125" y1="0.060325" x2="-0.187325" y2="0.066675" layer="21"/>
<rectangle x1="0.339725" y1="0.060325" x2="1.031875" y2="0.066675" layer="21"/>
<rectangle x1="-0.866775" y1="0.066675" x2="-0.206375" y2="0.073025" layer="21"/>
<rectangle x1="0.365125" y1="0.066675" x2="1.019175" y2="0.073025" layer="21"/>
<rectangle x1="-0.854075" y1="0.073025" x2="-0.231775" y2="0.079375" layer="21"/>
<rectangle x1="0.384175" y1="0.073025" x2="1.006475" y2="0.079375" layer="21"/>
<rectangle x1="-0.841375" y1="0.079375" x2="-0.250825" y2="0.085725" layer="21"/>
<rectangle x1="0.403225" y1="0.079375" x2="0.993775" y2="0.085725" layer="21"/>
<rectangle x1="-0.828675" y1="0.085725" x2="-0.269875" y2="0.092075" layer="21"/>
<rectangle x1="0.422275" y1="0.085725" x2="0.981075" y2="0.092075" layer="21"/>
<rectangle x1="-0.822325" y1="0.092075" x2="-0.282575" y2="0.098425" layer="21"/>
<rectangle x1="0.434975" y1="0.092075" x2="0.974725" y2="0.098425" layer="21"/>
<rectangle x1="-0.809625" y1="0.098425" x2="-0.301625" y2="0.104775" layer="21"/>
<rectangle x1="0.454025" y1="0.098425" x2="0.962025" y2="0.104775" layer="21"/>
<rectangle x1="-0.796925" y1="0.104775" x2="-0.320675" y2="0.111125" layer="21"/>
<rectangle x1="0.473075" y1="0.104775" x2="0.949325" y2="0.111125" layer="21"/>
<rectangle x1="-0.784225" y1="0.111125" x2="-0.339725" y2="0.117475" layer="21"/>
<rectangle x1="0.492125" y1="0.111125" x2="0.936625" y2="0.117475" layer="21"/>
<rectangle x1="-0.765175" y1="0.117475" x2="-0.365125" y2="0.123825" layer="21"/>
<rectangle x1="0.517525" y1="0.117475" x2="0.917575" y2="0.123825" layer="21"/>
<rectangle x1="-0.752475" y1="0.123825" x2="-0.384175" y2="0.130175" layer="21"/>
<rectangle x1="0.536575" y1="0.123825" x2="0.904875" y2="0.130175" layer="21"/>
<rectangle x1="-0.739775" y1="0.130175" x2="-0.396875" y2="0.136525" layer="21"/>
<rectangle x1="0.549275" y1="0.130175" x2="0.892175" y2="0.136525" layer="21"/>
<rectangle x1="-0.733425" y1="0.136525" x2="-0.415925" y2="0.142875" layer="21"/>
<rectangle x1="0.568325" y1="0.136525" x2="0.885825" y2="0.142875" layer="21"/>
<rectangle x1="-0.720725" y1="0.142875" x2="-0.434975" y2="0.149225" layer="21"/>
<rectangle x1="0.587375" y1="0.142875" x2="0.873125" y2="0.149225" layer="21"/>
<rectangle x1="-0.708025" y1="0.149225" x2="-0.447675" y2="0.155575" layer="21"/>
<rectangle x1="0.600075" y1="0.149225" x2="0.860425" y2="0.155575" layer="21"/>
<rectangle x1="-0.695325" y1="0.155575" x2="-0.466725" y2="0.161925" layer="21"/>
<rectangle x1="0.619125" y1="0.155575" x2="0.847725" y2="0.161925" layer="21"/>
<rectangle x1="-0.682625" y1="0.161925" x2="-0.479425" y2="0.168275" layer="21"/>
<rectangle x1="0.631825" y1="0.161925" x2="0.835025" y2="0.168275" layer="21"/>
<rectangle x1="-0.669925" y1="0.168275" x2="-0.498475" y2="0.174625" layer="21"/>
<rectangle x1="0.650875" y1="0.168275" x2="0.822325" y2="0.174625" layer="21"/>
<rectangle x1="-0.663575" y1="0.174625" x2="-0.511175" y2="0.180975" layer="21"/>
<rectangle x1="0.663575" y1="0.174625" x2="0.815975" y2="0.180975" layer="21"/>
<rectangle x1="-0.650875" y1="0.180975" x2="-0.530225" y2="0.187325" layer="21"/>
<rectangle x1="0.682625" y1="0.180975" x2="0.803275" y2="0.187325" layer="21"/>
<rectangle x1="-0.638175" y1="0.187325" x2="-0.542925" y2="0.193675" layer="21"/>
<rectangle x1="0.695325" y1="0.187325" x2="0.790575" y2="0.193675" layer="21"/>
<rectangle x1="-0.625475" y1="0.193675" x2="-0.555625" y2="0.200025" layer="21"/>
<rectangle x1="0.708025" y1="0.193675" x2="0.777875" y2="0.200025" layer="21"/>
<rectangle x1="-0.612775" y1="0.200025" x2="-0.574675" y2="0.206375" layer="21"/>
<rectangle x1="0.727075" y1="0.200025" x2="0.765175" y2="0.206375" layer="21"/>
<rectangle x1="0.739775" y1="0.542925" x2="0.854075" y2="0.549275" layer="21"/>
<rectangle x1="0.695325" y1="0.549275" x2="0.892175" y2="0.555625" layer="21"/>
<rectangle x1="0.669925" y1="0.555625" x2="0.923925" y2="0.561975" layer="21"/>
<rectangle x1="-0.968375" y1="0.561975" x2="-0.530225" y2="0.568325" layer="21"/>
<rectangle x1="-0.028575" y1="0.561975" x2="0.174625" y2="0.568325" layer="21"/>
<rectangle x1="0.638175" y1="0.561975" x2="0.949325" y2="0.568325" layer="21"/>
<rectangle x1="-0.974725" y1="0.568325" x2="-0.504825" y2="0.574675" layer="21"/>
<rectangle x1="-0.034925" y1="0.568325" x2="0.180975" y2="0.574675" layer="21"/>
<rectangle x1="0.625475" y1="0.568325" x2="0.968375" y2="0.574675" layer="21"/>
<rectangle x1="-0.974725" y1="0.574675" x2="-0.485775" y2="0.581025" layer="21"/>
<rectangle x1="-0.034925" y1="0.574675" x2="0.180975" y2="0.581025" layer="21"/>
<rectangle x1="0.612775" y1="0.574675" x2="0.981075" y2="0.581025" layer="21"/>
<rectangle x1="-0.974725" y1="0.581025" x2="-0.473075" y2="0.587375" layer="21"/>
<rectangle x1="-0.034925" y1="0.581025" x2="0.180975" y2="0.587375" layer="21"/>
<rectangle x1="0.600075" y1="0.581025" x2="0.993775" y2="0.587375" layer="21"/>
<rectangle x1="-0.974725" y1="0.587375" x2="-0.460375" y2="0.593725" layer="21"/>
<rectangle x1="-0.034925" y1="0.587375" x2="0.180975" y2="0.593725" layer="21"/>
<rectangle x1="0.587375" y1="0.587375" x2="1.000125" y2="0.593725" layer="21"/>
<rectangle x1="-0.974725" y1="0.593725" x2="-0.447675" y2="0.600075" layer="21"/>
<rectangle x1="-0.034925" y1="0.593725" x2="0.180975" y2="0.600075" layer="21"/>
<rectangle x1="0.581025" y1="0.593725" x2="1.012825" y2="0.600075" layer="21"/>
<rectangle x1="-0.974725" y1="0.600075" x2="-0.434975" y2="0.606425" layer="21"/>
<rectangle x1="-0.034925" y1="0.600075" x2="0.180975" y2="0.606425" layer="21"/>
<rectangle x1="0.574675" y1="0.600075" x2="1.019175" y2="0.606425" layer="21"/>
<rectangle x1="-0.974725" y1="0.606425" x2="-0.428625" y2="0.612775" layer="21"/>
<rectangle x1="-0.034925" y1="0.606425" x2="0.180975" y2="0.612775" layer="21"/>
<rectangle x1="0.561975" y1="0.606425" x2="1.025525" y2="0.612775" layer="21"/>
<rectangle x1="-0.974725" y1="0.612775" x2="-0.422275" y2="0.619125" layer="21"/>
<rectangle x1="-0.034925" y1="0.612775" x2="0.180975" y2="0.619125" layer="21"/>
<rectangle x1="0.555625" y1="0.612775" x2="1.031875" y2="0.619125" layer="21"/>
<rectangle x1="-0.974725" y1="0.619125" x2="-0.415925" y2="0.625475" layer="21"/>
<rectangle x1="-0.034925" y1="0.619125" x2="0.180975" y2="0.625475" layer="21"/>
<rectangle x1="0.549275" y1="0.619125" x2="1.038225" y2="0.625475" layer="21"/>
<rectangle x1="-0.974725" y1="0.625475" x2="-0.409575" y2="0.631825" layer="21"/>
<rectangle x1="-0.034925" y1="0.625475" x2="0.180975" y2="0.631825" layer="21"/>
<rectangle x1="0.542925" y1="0.625475" x2="1.044575" y2="0.631825" layer="21"/>
<rectangle x1="-0.974725" y1="0.631825" x2="-0.403225" y2="0.638175" layer="21"/>
<rectangle x1="-0.034925" y1="0.631825" x2="0.180975" y2="0.638175" layer="21"/>
<rectangle x1="0.536575" y1="0.631825" x2="1.050925" y2="0.638175" layer="21"/>
<rectangle x1="-0.974725" y1="0.638175" x2="-0.396875" y2="0.644525" layer="21"/>
<rectangle x1="-0.034925" y1="0.638175" x2="0.180975" y2="0.644525" layer="21"/>
<rectangle x1="0.530225" y1="0.638175" x2="1.057275" y2="0.644525" layer="21"/>
<rectangle x1="-0.974725" y1="0.644525" x2="-0.390525" y2="0.650875" layer="21"/>
<rectangle x1="-0.034925" y1="0.644525" x2="0.180975" y2="0.650875" layer="21"/>
<rectangle x1="0.523875" y1="0.644525" x2="1.063625" y2="0.650875" layer="21"/>
<rectangle x1="-0.974725" y1="0.650875" x2="-0.384175" y2="0.657225" layer="21"/>
<rectangle x1="-0.034925" y1="0.650875" x2="0.180975" y2="0.657225" layer="21"/>
<rectangle x1="0.523875" y1="0.650875" x2="1.069975" y2="0.657225" layer="21"/>
<rectangle x1="-0.974725" y1="0.657225" x2="-0.377825" y2="0.663575" layer="21"/>
<rectangle x1="-0.034925" y1="0.657225" x2="0.180975" y2="0.663575" layer="21"/>
<rectangle x1="0.517525" y1="0.657225" x2="1.069975" y2="0.663575" layer="21"/>
<rectangle x1="-0.974725" y1="0.663575" x2="-0.377825" y2="0.669925" layer="21"/>
<rectangle x1="-0.034925" y1="0.663575" x2="0.180975" y2="0.669925" layer="21"/>
<rectangle x1="0.517525" y1="0.663575" x2="1.076325" y2="0.669925" layer="21"/>
<rectangle x1="-0.974725" y1="0.669925" x2="-0.371475" y2="0.676275" layer="21"/>
<rectangle x1="-0.034925" y1="0.669925" x2="0.180975" y2="0.676275" layer="21"/>
<rectangle x1="0.511175" y1="0.669925" x2="1.076325" y2="0.676275" layer="21"/>
<rectangle x1="-0.974725" y1="0.676275" x2="-0.365125" y2="0.682625" layer="21"/>
<rectangle x1="-0.034925" y1="0.676275" x2="0.180975" y2="0.682625" layer="21"/>
<rectangle x1="0.504825" y1="0.676275" x2="1.082675" y2="0.682625" layer="21"/>
<rectangle x1="-0.974725" y1="0.682625" x2="-0.365125" y2="0.688975" layer="21"/>
<rectangle x1="-0.034925" y1="0.682625" x2="0.180975" y2="0.688975" layer="21"/>
<rectangle x1="0.504825" y1="0.682625" x2="1.082675" y2="0.688975" layer="21"/>
<rectangle x1="-0.974725" y1="0.688975" x2="-0.358775" y2="0.695325" layer="21"/>
<rectangle x1="-0.034925" y1="0.688975" x2="0.180975" y2="0.695325" layer="21"/>
<rectangle x1="0.504825" y1="0.688975" x2="1.089025" y2="0.695325" layer="21"/>
<rectangle x1="-0.974725" y1="0.695325" x2="-0.358775" y2="0.701675" layer="21"/>
<rectangle x1="-0.034925" y1="0.695325" x2="0.180975" y2="0.701675" layer="21"/>
<rectangle x1="0.498475" y1="0.695325" x2="0.758825" y2="0.701675" layer="21"/>
<rectangle x1="0.828675" y1="0.695325" x2="1.089025" y2="0.701675" layer="21"/>
<rectangle x1="-0.974725" y1="0.701675" x2="-0.358775" y2="0.708025" layer="21"/>
<rectangle x1="-0.034925" y1="0.701675" x2="0.180975" y2="0.708025" layer="21"/>
<rectangle x1="0.498475" y1="0.701675" x2="0.746125" y2="0.708025" layer="21"/>
<rectangle x1="0.847725" y1="0.701675" x2="1.095375" y2="0.708025" layer="21"/>
<rectangle x1="-0.974725" y1="0.708025" x2="-0.352425" y2="0.714375" layer="21"/>
<rectangle x1="-0.034925" y1="0.708025" x2="0.180975" y2="0.714375" layer="21"/>
<rectangle x1="0.492125" y1="0.708025" x2="0.733425" y2="0.714375" layer="21"/>
<rectangle x1="0.860425" y1="0.708025" x2="1.095375" y2="0.714375" layer="21"/>
<rectangle x1="-0.974725" y1="0.714375" x2="-0.771525" y2="0.720725" layer="21"/>
<rectangle x1="-0.631825" y1="0.714375" x2="-0.352425" y2="0.720725" layer="21"/>
<rectangle x1="-0.034925" y1="0.714375" x2="0.180975" y2="0.720725" layer="21"/>
<rectangle x1="0.492125" y1="0.714375" x2="0.720725" y2="0.720725" layer="21"/>
<rectangle x1="0.866775" y1="0.714375" x2="1.101725" y2="0.720725" layer="21"/>
<rectangle x1="-0.974725" y1="0.720725" x2="-0.765175" y2="0.727075" layer="21"/>
<rectangle x1="-0.600075" y1="0.720725" x2="-0.346075" y2="0.727075" layer="21"/>
<rectangle x1="-0.034925" y1="0.720725" x2="0.180975" y2="0.727075" layer="21"/>
<rectangle x1="0.492125" y1="0.720725" x2="0.714375" y2="0.727075" layer="21"/>
<rectangle x1="0.879475" y1="0.720725" x2="1.101725" y2="0.727075" layer="21"/>
<rectangle x1="-0.974725" y1="0.727075" x2="-0.765175" y2="0.733425" layer="21"/>
<rectangle x1="-0.587375" y1="0.727075" x2="-0.346075" y2="0.733425" layer="21"/>
<rectangle x1="-0.034925" y1="0.727075" x2="0.180975" y2="0.733425" layer="21"/>
<rectangle x1="0.485775" y1="0.727075" x2="0.708025" y2="0.733425" layer="21"/>
<rectangle x1="0.879475" y1="0.727075" x2="1.101725" y2="0.733425" layer="21"/>
<rectangle x1="-0.974725" y1="0.733425" x2="-0.765175" y2="0.739775" layer="21"/>
<rectangle x1="-0.581025" y1="0.733425" x2="-0.346075" y2="0.739775" layer="21"/>
<rectangle x1="-0.034925" y1="0.733425" x2="0.180975" y2="0.739775" layer="21"/>
<rectangle x1="0.485775" y1="0.733425" x2="0.708025" y2="0.739775" layer="21"/>
<rectangle x1="0.885825" y1="0.733425" x2="1.108075" y2="0.739775" layer="21"/>
<rectangle x1="-0.974725" y1="0.739775" x2="-0.765175" y2="0.746125" layer="21"/>
<rectangle x1="-0.574675" y1="0.739775" x2="-0.339725" y2="0.746125" layer="21"/>
<rectangle x1="-0.034925" y1="0.739775" x2="0.180975" y2="0.746125" layer="21"/>
<rectangle x1="0.485775" y1="0.739775" x2="0.701675" y2="0.746125" layer="21"/>
<rectangle x1="0.892175" y1="0.739775" x2="1.108075" y2="0.746125" layer="21"/>
<rectangle x1="-0.974725" y1="0.746125" x2="-0.765175" y2="0.752475" layer="21"/>
<rectangle x1="-0.568325" y1="0.746125" x2="-0.339725" y2="0.752475" layer="21"/>
<rectangle x1="-0.034925" y1="0.746125" x2="0.180975" y2="0.752475" layer="21"/>
<rectangle x1="0.485775" y1="0.746125" x2="0.701675" y2="0.752475" layer="21"/>
<rectangle x1="0.892175" y1="0.746125" x2="1.108075" y2="0.752475" layer="21"/>
<rectangle x1="-0.974725" y1="0.752475" x2="-0.765175" y2="0.758825" layer="21"/>
<rectangle x1="-0.561975" y1="0.752475" x2="-0.339725" y2="0.758825" layer="21"/>
<rectangle x1="-0.034925" y1="0.752475" x2="0.180975" y2="0.758825" layer="21"/>
<rectangle x1="0.479425" y1="0.752475" x2="0.695325" y2="0.758825" layer="21"/>
<rectangle x1="0.898525" y1="0.752475" x2="1.108075" y2="0.758825" layer="21"/>
<rectangle x1="-0.974725" y1="0.758825" x2="-0.765175" y2="0.765175" layer="21"/>
<rectangle x1="-0.561975" y1="0.758825" x2="-0.339725" y2="0.765175" layer="21"/>
<rectangle x1="-0.034925" y1="0.758825" x2="0.180975" y2="0.765175" layer="21"/>
<rectangle x1="0.479425" y1="0.758825" x2="0.695325" y2="0.765175" layer="21"/>
<rectangle x1="0.898525" y1="0.758825" x2="1.114425" y2="0.765175" layer="21"/>
<rectangle x1="-0.974725" y1="0.765175" x2="-0.765175" y2="0.771525" layer="21"/>
<rectangle x1="-0.555625" y1="0.765175" x2="-0.333375" y2="0.771525" layer="21"/>
<rectangle x1="-0.034925" y1="0.765175" x2="0.180975" y2="0.771525" layer="21"/>
<rectangle x1="0.479425" y1="0.765175" x2="0.695325" y2="0.771525" layer="21"/>
<rectangle x1="0.898525" y1="0.765175" x2="1.114425" y2="0.771525" layer="21"/>
<rectangle x1="-0.974725" y1="0.771525" x2="-0.765175" y2="0.777875" layer="21"/>
<rectangle x1="-0.555625" y1="0.771525" x2="-0.333375" y2="0.777875" layer="21"/>
<rectangle x1="-0.034925" y1="0.771525" x2="0.180975" y2="0.777875" layer="21"/>
<rectangle x1="0.479425" y1="0.771525" x2="0.688975" y2="0.777875" layer="21"/>
<rectangle x1="0.904875" y1="0.771525" x2="1.114425" y2="0.777875" layer="21"/>
<rectangle x1="-0.974725" y1="0.777875" x2="-0.765175" y2="0.784225" layer="21"/>
<rectangle x1="-0.549275" y1="0.777875" x2="-0.333375" y2="0.784225" layer="21"/>
<rectangle x1="-0.034925" y1="0.777875" x2="0.180975" y2="0.784225" layer="21"/>
<rectangle x1="0.479425" y1="0.777875" x2="0.688975" y2="0.784225" layer="21"/>
<rectangle x1="0.904875" y1="0.777875" x2="1.114425" y2="0.784225" layer="21"/>
<rectangle x1="-0.974725" y1="0.784225" x2="-0.765175" y2="0.790575" layer="21"/>
<rectangle x1="-0.549275" y1="0.784225" x2="-0.333375" y2="0.790575" layer="21"/>
<rectangle x1="-0.034925" y1="0.784225" x2="0.180975" y2="0.790575" layer="21"/>
<rectangle x1="0.473075" y1="0.784225" x2="0.688975" y2="0.790575" layer="21"/>
<rectangle x1="0.904875" y1="0.784225" x2="1.114425" y2="0.790575" layer="21"/>
<rectangle x1="-0.974725" y1="0.790575" x2="-0.765175" y2="0.796925" layer="21"/>
<rectangle x1="-0.549275" y1="0.790575" x2="-0.333375" y2="0.796925" layer="21"/>
<rectangle x1="-0.034925" y1="0.790575" x2="0.180975" y2="0.796925" layer="21"/>
<rectangle x1="0.473075" y1="0.790575" x2="0.688975" y2="0.796925" layer="21"/>
<rectangle x1="0.904875" y1="0.790575" x2="1.120775" y2="0.796925" layer="21"/>
<rectangle x1="-0.974725" y1="0.796925" x2="-0.765175" y2="0.803275" layer="21"/>
<rectangle x1="-0.549275" y1="0.796925" x2="-0.333375" y2="0.803275" layer="21"/>
<rectangle x1="-0.034925" y1="0.796925" x2="0.180975" y2="0.803275" layer="21"/>
<rectangle x1="0.473075" y1="0.796925" x2="0.688975" y2="0.803275" layer="21"/>
<rectangle x1="0.904875" y1="0.796925" x2="1.120775" y2="0.803275" layer="21"/>
<rectangle x1="-0.974725" y1="0.803275" x2="-0.765175" y2="0.809625" layer="21"/>
<rectangle x1="-0.542925" y1="0.803275" x2="-0.327025" y2="0.809625" layer="21"/>
<rectangle x1="-0.034925" y1="0.803275" x2="0.180975" y2="0.809625" layer="21"/>
<rectangle x1="0.473075" y1="0.803275" x2="0.682625" y2="0.809625" layer="21"/>
<rectangle x1="0.911225" y1="0.803275" x2="1.120775" y2="0.809625" layer="21"/>
<rectangle x1="-0.974725" y1="0.809625" x2="-0.765175" y2="0.815975" layer="21"/>
<rectangle x1="-0.542925" y1="0.809625" x2="-0.327025" y2="0.815975" layer="21"/>
<rectangle x1="-0.034925" y1="0.809625" x2="0.180975" y2="0.815975" layer="21"/>
<rectangle x1="0.473075" y1="0.809625" x2="0.682625" y2="0.815975" layer="21"/>
<rectangle x1="0.911225" y1="0.809625" x2="1.120775" y2="0.815975" layer="21"/>
<rectangle x1="-0.974725" y1="0.815975" x2="-0.765175" y2="0.822325" layer="21"/>
<rectangle x1="-0.542925" y1="0.815975" x2="-0.327025" y2="0.822325" layer="21"/>
<rectangle x1="-0.034925" y1="0.815975" x2="0.180975" y2="0.822325" layer="21"/>
<rectangle x1="0.473075" y1="0.815975" x2="0.682625" y2="0.822325" layer="21"/>
<rectangle x1="0.911225" y1="0.815975" x2="1.120775" y2="0.822325" layer="21"/>
<rectangle x1="-0.974725" y1="0.822325" x2="-0.765175" y2="0.828675" layer="21"/>
<rectangle x1="-0.542925" y1="0.822325" x2="-0.327025" y2="0.828675" layer="21"/>
<rectangle x1="-0.034925" y1="0.822325" x2="0.180975" y2="0.828675" layer="21"/>
<rectangle x1="0.473075" y1="0.822325" x2="0.682625" y2="0.828675" layer="21"/>
<rectangle x1="0.911225" y1="0.822325" x2="1.120775" y2="0.828675" layer="21"/>
<rectangle x1="-0.974725" y1="0.828675" x2="-0.765175" y2="0.835025" layer="21"/>
<rectangle x1="-0.542925" y1="0.828675" x2="-0.327025" y2="0.835025" layer="21"/>
<rectangle x1="-0.034925" y1="0.828675" x2="0.180975" y2="0.835025" layer="21"/>
<rectangle x1="0.473075" y1="0.828675" x2="0.682625" y2="0.835025" layer="21"/>
<rectangle x1="0.911225" y1="0.828675" x2="1.120775" y2="0.835025" layer="21"/>
<rectangle x1="-0.974725" y1="0.835025" x2="-0.765175" y2="0.841375" layer="21"/>
<rectangle x1="-0.536575" y1="0.835025" x2="-0.327025" y2="0.841375" layer="21"/>
<rectangle x1="-0.034925" y1="0.835025" x2="0.180975" y2="0.841375" layer="21"/>
<rectangle x1="0.473075" y1="0.835025" x2="0.682625" y2="0.841375" layer="21"/>
<rectangle x1="0.911225" y1="0.835025" x2="1.120775" y2="0.841375" layer="21"/>
<rectangle x1="-0.974725" y1="0.841375" x2="-0.765175" y2="0.847725" layer="21"/>
<rectangle x1="-0.536575" y1="0.841375" x2="-0.327025" y2="0.847725" layer="21"/>
<rectangle x1="-0.034925" y1="0.841375" x2="0.180975" y2="0.847725" layer="21"/>
<rectangle x1="0.473075" y1="0.841375" x2="0.682625" y2="0.847725" layer="21"/>
<rectangle x1="0.911225" y1="0.841375" x2="1.120775" y2="0.847725" layer="21"/>
<rectangle x1="-0.974725" y1="0.847725" x2="-0.765175" y2="0.854075" layer="21"/>
<rectangle x1="-0.536575" y1="0.847725" x2="-0.327025" y2="0.854075" layer="21"/>
<rectangle x1="-0.034925" y1="0.847725" x2="0.180975" y2="0.854075" layer="21"/>
<rectangle x1="0.473075" y1="0.847725" x2="0.682625" y2="0.854075" layer="21"/>
<rectangle x1="0.911225" y1="0.847725" x2="1.120775" y2="0.854075" layer="21"/>
<rectangle x1="-0.974725" y1="0.854075" x2="-0.765175" y2="0.860425" layer="21"/>
<rectangle x1="-0.536575" y1="0.854075" x2="-0.327025" y2="0.860425" layer="21"/>
<rectangle x1="-0.034925" y1="0.854075" x2="0.180975" y2="0.860425" layer="21"/>
<rectangle x1="0.473075" y1="0.854075" x2="0.682625" y2="0.860425" layer="21"/>
<rectangle x1="0.911225" y1="0.854075" x2="1.120775" y2="0.860425" layer="21"/>
<rectangle x1="-0.974725" y1="0.860425" x2="-0.765175" y2="0.866775" layer="21"/>
<rectangle x1="-0.536575" y1="0.860425" x2="-0.327025" y2="0.866775" layer="21"/>
<rectangle x1="-0.034925" y1="0.860425" x2="0.180975" y2="0.866775" layer="21"/>
<rectangle x1="0.473075" y1="0.860425" x2="0.682625" y2="0.866775" layer="21"/>
<rectangle x1="0.911225" y1="0.860425" x2="1.120775" y2="0.866775" layer="21"/>
<rectangle x1="-0.974725" y1="0.866775" x2="-0.765175" y2="0.873125" layer="21"/>
<rectangle x1="-0.536575" y1="0.866775" x2="-0.320675" y2="0.873125" layer="21"/>
<rectangle x1="-0.034925" y1="0.866775" x2="0.180975" y2="0.873125" layer="21"/>
<rectangle x1="0.466725" y1="0.866775" x2="0.682625" y2="0.873125" layer="21"/>
<rectangle x1="0.911225" y1="0.866775" x2="1.120775" y2="0.873125" layer="21"/>
<rectangle x1="-0.974725" y1="0.873125" x2="-0.765175" y2="0.879475" layer="21"/>
<rectangle x1="-0.536575" y1="0.873125" x2="-0.320675" y2="0.879475" layer="21"/>
<rectangle x1="-0.034925" y1="0.873125" x2="0.180975" y2="0.879475" layer="21"/>
<rectangle x1="0.466725" y1="0.873125" x2="0.682625" y2="0.879475" layer="21"/>
<rectangle x1="0.911225" y1="0.873125" x2="1.120775" y2="0.879475" layer="21"/>
<rectangle x1="-0.974725" y1="0.879475" x2="-0.765175" y2="0.885825" layer="21"/>
<rectangle x1="-0.536575" y1="0.879475" x2="-0.320675" y2="0.885825" layer="21"/>
<rectangle x1="-0.034925" y1="0.879475" x2="0.180975" y2="0.885825" layer="21"/>
<rectangle x1="0.466725" y1="0.879475" x2="0.676275" y2="0.885825" layer="21"/>
<rectangle x1="0.911225" y1="0.879475" x2="1.120775" y2="0.885825" layer="21"/>
<rectangle x1="-0.974725" y1="0.885825" x2="-0.765175" y2="0.892175" layer="21"/>
<rectangle x1="-0.536575" y1="0.885825" x2="-0.320675" y2="0.892175" layer="21"/>
<rectangle x1="-0.034925" y1="0.885825" x2="0.180975" y2="0.892175" layer="21"/>
<rectangle x1="0.466725" y1="0.885825" x2="0.676275" y2="0.892175" layer="21"/>
<rectangle x1="0.911225" y1="0.885825" x2="1.120775" y2="0.892175" layer="21"/>
<rectangle x1="-0.974725" y1="0.892175" x2="-0.765175" y2="0.898525" layer="21"/>
<rectangle x1="-0.536575" y1="0.892175" x2="-0.320675" y2="0.898525" layer="21"/>
<rectangle x1="-0.034925" y1="0.892175" x2="0.180975" y2="0.898525" layer="21"/>
<rectangle x1="0.466725" y1="0.892175" x2="0.676275" y2="0.898525" layer="21"/>
<rectangle x1="0.911225" y1="0.892175" x2="1.120775" y2="0.898525" layer="21"/>
<rectangle x1="-0.974725" y1="0.898525" x2="-0.765175" y2="0.904875" layer="21"/>
<rectangle x1="-0.536575" y1="0.898525" x2="-0.320675" y2="0.904875" layer="21"/>
<rectangle x1="-0.034925" y1="0.898525" x2="0.180975" y2="0.904875" layer="21"/>
<rectangle x1="0.466725" y1="0.898525" x2="0.676275" y2="0.904875" layer="21"/>
<rectangle x1="0.911225" y1="0.898525" x2="1.120775" y2="0.904875" layer="21"/>
<rectangle x1="-0.974725" y1="0.904875" x2="-0.765175" y2="0.911225" layer="21"/>
<rectangle x1="-0.536575" y1="0.904875" x2="-0.320675" y2="0.911225" layer="21"/>
<rectangle x1="-0.034925" y1="0.904875" x2="0.180975" y2="0.911225" layer="21"/>
<rectangle x1="0.466725" y1="0.904875" x2="0.676275" y2="0.911225" layer="21"/>
<rectangle x1="0.911225" y1="0.904875" x2="1.120775" y2="0.911225" layer="21"/>
<rectangle x1="-0.974725" y1="0.911225" x2="-0.765175" y2="0.917575" layer="21"/>
<rectangle x1="-0.536575" y1="0.911225" x2="-0.320675" y2="0.917575" layer="21"/>
<rectangle x1="-0.034925" y1="0.911225" x2="0.180975" y2="0.917575" layer="21"/>
<rectangle x1="0.466725" y1="0.911225" x2="0.676275" y2="0.917575" layer="21"/>
<rectangle x1="0.911225" y1="0.911225" x2="1.120775" y2="0.917575" layer="21"/>
<rectangle x1="-0.974725" y1="0.917575" x2="-0.765175" y2="0.923925" layer="21"/>
<rectangle x1="-0.536575" y1="0.917575" x2="-0.320675" y2="0.923925" layer="21"/>
<rectangle x1="-0.034925" y1="0.917575" x2="0.180975" y2="0.923925" layer="21"/>
<rectangle x1="0.466725" y1="0.917575" x2="0.676275" y2="0.923925" layer="21"/>
<rectangle x1="0.911225" y1="0.917575" x2="1.120775" y2="0.923925" layer="21"/>
<rectangle x1="-0.974725" y1="0.923925" x2="-0.765175" y2="0.930275" layer="21"/>
<rectangle x1="-0.536575" y1="0.923925" x2="-0.320675" y2="0.930275" layer="21"/>
<rectangle x1="-0.034925" y1="0.923925" x2="0.180975" y2="0.930275" layer="21"/>
<rectangle x1="0.466725" y1="0.923925" x2="0.676275" y2="0.930275" layer="21"/>
<rectangle x1="0.911225" y1="0.923925" x2="1.120775" y2="0.930275" layer="21"/>
<rectangle x1="-0.974725" y1="0.930275" x2="-0.765175" y2="0.936625" layer="21"/>
<rectangle x1="-0.536575" y1="0.930275" x2="-0.320675" y2="0.936625" layer="21"/>
<rectangle x1="-0.034925" y1="0.930275" x2="0.180975" y2="0.936625" layer="21"/>
<rectangle x1="0.466725" y1="0.930275" x2="0.676275" y2="0.936625" layer="21"/>
<rectangle x1="0.911225" y1="0.930275" x2="1.120775" y2="0.936625" layer="21"/>
<rectangle x1="-0.974725" y1="0.936625" x2="-0.765175" y2="0.942975" layer="21"/>
<rectangle x1="-0.536575" y1="0.936625" x2="-0.320675" y2="0.942975" layer="21"/>
<rectangle x1="-0.034925" y1="0.936625" x2="0.180975" y2="0.942975" layer="21"/>
<rectangle x1="0.466725" y1="0.936625" x2="0.676275" y2="0.942975" layer="21"/>
<rectangle x1="0.911225" y1="0.936625" x2="1.120775" y2="0.942975" layer="21"/>
<rectangle x1="-0.974725" y1="0.942975" x2="-0.765175" y2="0.949325" layer="21"/>
<rectangle x1="-0.536575" y1="0.942975" x2="-0.320675" y2="0.949325" layer="21"/>
<rectangle x1="-0.034925" y1="0.942975" x2="0.180975" y2="0.949325" layer="21"/>
<rectangle x1="0.466725" y1="0.942975" x2="0.676275" y2="0.949325" layer="21"/>
<rectangle x1="0.911225" y1="0.942975" x2="1.120775" y2="0.949325" layer="21"/>
<rectangle x1="-0.974725" y1="0.949325" x2="-0.765175" y2="0.955675" layer="21"/>
<rectangle x1="-0.536575" y1="0.949325" x2="-0.320675" y2="0.955675" layer="21"/>
<rectangle x1="-0.034925" y1="0.949325" x2="0.180975" y2="0.955675" layer="21"/>
<rectangle x1="0.466725" y1="0.949325" x2="0.676275" y2="0.955675" layer="21"/>
<rectangle x1="0.911225" y1="0.949325" x2="1.120775" y2="0.955675" layer="21"/>
<rectangle x1="-0.974725" y1="0.955675" x2="-0.765175" y2="0.962025" layer="21"/>
<rectangle x1="-0.536575" y1="0.955675" x2="-0.320675" y2="0.962025" layer="21"/>
<rectangle x1="-0.034925" y1="0.955675" x2="0.180975" y2="0.962025" layer="21"/>
<rectangle x1="0.466725" y1="0.955675" x2="0.676275" y2="0.962025" layer="21"/>
<rectangle x1="0.911225" y1="0.955675" x2="1.120775" y2="0.962025" layer="21"/>
<rectangle x1="-0.974725" y1="0.962025" x2="-0.765175" y2="0.968375" layer="21"/>
<rectangle x1="-0.536575" y1="0.962025" x2="-0.320675" y2="0.968375" layer="21"/>
<rectangle x1="-0.034925" y1="0.962025" x2="0.180975" y2="0.968375" layer="21"/>
<rectangle x1="0.466725" y1="0.962025" x2="0.676275" y2="0.968375" layer="21"/>
<rectangle x1="0.911225" y1="0.962025" x2="1.120775" y2="0.968375" layer="21"/>
<rectangle x1="-0.974725" y1="0.968375" x2="-0.765175" y2="0.974725" layer="21"/>
<rectangle x1="-0.530225" y1="0.968375" x2="-0.320675" y2="0.974725" layer="21"/>
<rectangle x1="-0.034925" y1="0.968375" x2="0.180975" y2="0.974725" layer="21"/>
<rectangle x1="0.466725" y1="0.968375" x2="0.676275" y2="0.974725" layer="21"/>
<rectangle x1="0.911225" y1="0.968375" x2="1.120775" y2="0.974725" layer="21"/>
<rectangle x1="-0.974725" y1="0.974725" x2="-0.765175" y2="0.981075" layer="21"/>
<rectangle x1="-0.530225" y1="0.974725" x2="-0.320675" y2="0.981075" layer="21"/>
<rectangle x1="-0.034925" y1="0.974725" x2="0.180975" y2="0.981075" layer="21"/>
<rectangle x1="0.466725" y1="0.974725" x2="0.676275" y2="0.981075" layer="21"/>
<rectangle x1="0.911225" y1="0.974725" x2="1.120775" y2="0.981075" layer="21"/>
<rectangle x1="-0.974725" y1="0.981075" x2="-0.765175" y2="0.987425" layer="21"/>
<rectangle x1="-0.530225" y1="0.981075" x2="-0.320675" y2="0.987425" layer="21"/>
<rectangle x1="-0.034925" y1="0.981075" x2="0.180975" y2="0.987425" layer="21"/>
<rectangle x1="0.466725" y1="0.981075" x2="0.676275" y2="0.987425" layer="21"/>
<rectangle x1="0.911225" y1="0.981075" x2="1.120775" y2="0.987425" layer="21"/>
<rectangle x1="-0.974725" y1="0.987425" x2="-0.765175" y2="0.993775" layer="21"/>
<rectangle x1="-0.530225" y1="0.987425" x2="-0.320675" y2="0.993775" layer="21"/>
<rectangle x1="-0.034925" y1="0.987425" x2="0.180975" y2="0.993775" layer="21"/>
<rectangle x1="0.466725" y1="0.987425" x2="0.676275" y2="0.993775" layer="21"/>
<rectangle x1="0.911225" y1="0.987425" x2="1.120775" y2="0.993775" layer="21"/>
<rectangle x1="-0.974725" y1="0.993775" x2="-0.765175" y2="1.000125" layer="21"/>
<rectangle x1="-0.530225" y1="0.993775" x2="-0.320675" y2="1.000125" layer="21"/>
<rectangle x1="-0.034925" y1="0.993775" x2="0.180975" y2="1.000125" layer="21"/>
<rectangle x1="0.466725" y1="0.993775" x2="0.676275" y2="1.000125" layer="21"/>
<rectangle x1="0.911225" y1="0.993775" x2="1.120775" y2="1.000125" layer="21"/>
<rectangle x1="-0.974725" y1="1.000125" x2="-0.765175" y2="1.006475" layer="21"/>
<rectangle x1="-0.530225" y1="1.000125" x2="-0.320675" y2="1.006475" layer="21"/>
<rectangle x1="-0.034925" y1="1.000125" x2="0.180975" y2="1.006475" layer="21"/>
<rectangle x1="0.466725" y1="1.000125" x2="0.676275" y2="1.006475" layer="21"/>
<rectangle x1="0.911225" y1="1.000125" x2="1.120775" y2="1.006475" layer="21"/>
<rectangle x1="-0.974725" y1="1.006475" x2="-0.765175" y2="1.012825" layer="21"/>
<rectangle x1="-0.530225" y1="1.006475" x2="-0.320675" y2="1.012825" layer="21"/>
<rectangle x1="-0.034925" y1="1.006475" x2="0.180975" y2="1.012825" layer="21"/>
<rectangle x1="0.466725" y1="1.006475" x2="0.676275" y2="1.012825" layer="21"/>
<rectangle x1="0.911225" y1="1.006475" x2="1.120775" y2="1.012825" layer="21"/>
<rectangle x1="-0.974725" y1="1.012825" x2="-0.765175" y2="1.019175" layer="21"/>
<rectangle x1="-0.530225" y1="1.012825" x2="-0.320675" y2="1.019175" layer="21"/>
<rectangle x1="-0.034925" y1="1.012825" x2="0.180975" y2="1.019175" layer="21"/>
<rectangle x1="0.466725" y1="1.012825" x2="0.676275" y2="1.019175" layer="21"/>
<rectangle x1="0.911225" y1="1.012825" x2="1.120775" y2="1.019175" layer="21"/>
<rectangle x1="-0.974725" y1="1.019175" x2="-0.765175" y2="1.025525" layer="21"/>
<rectangle x1="-0.530225" y1="1.019175" x2="-0.320675" y2="1.025525" layer="21"/>
<rectangle x1="-0.034925" y1="1.019175" x2="0.180975" y2="1.025525" layer="21"/>
<rectangle x1="0.466725" y1="1.019175" x2="0.676275" y2="1.025525" layer="21"/>
<rectangle x1="0.911225" y1="1.019175" x2="1.120775" y2="1.025525" layer="21"/>
<rectangle x1="-0.974725" y1="1.025525" x2="-0.765175" y2="1.031875" layer="21"/>
<rectangle x1="-0.530225" y1="1.025525" x2="-0.320675" y2="1.031875" layer="21"/>
<rectangle x1="-0.034925" y1="1.025525" x2="0.180975" y2="1.031875" layer="21"/>
<rectangle x1="0.466725" y1="1.025525" x2="0.676275" y2="1.031875" layer="21"/>
<rectangle x1="0.911225" y1="1.025525" x2="1.120775" y2="1.031875" layer="21"/>
<rectangle x1="-0.974725" y1="1.031875" x2="-0.765175" y2="1.038225" layer="21"/>
<rectangle x1="-0.530225" y1="1.031875" x2="-0.320675" y2="1.038225" layer="21"/>
<rectangle x1="-0.034925" y1="1.031875" x2="0.180975" y2="1.038225" layer="21"/>
<rectangle x1="0.466725" y1="1.031875" x2="0.676275" y2="1.038225" layer="21"/>
<rectangle x1="0.911225" y1="1.031875" x2="1.120775" y2="1.038225" layer="21"/>
<rectangle x1="-0.974725" y1="1.038225" x2="-0.765175" y2="1.044575" layer="21"/>
<rectangle x1="-0.530225" y1="1.038225" x2="-0.320675" y2="1.044575" layer="21"/>
<rectangle x1="-0.034925" y1="1.038225" x2="0.180975" y2="1.044575" layer="21"/>
<rectangle x1="0.466725" y1="1.038225" x2="0.676275" y2="1.044575" layer="21"/>
<rectangle x1="0.911225" y1="1.038225" x2="1.120775" y2="1.044575" layer="21"/>
<rectangle x1="-0.974725" y1="1.044575" x2="-0.765175" y2="1.050925" layer="21"/>
<rectangle x1="-0.530225" y1="1.044575" x2="-0.320675" y2="1.050925" layer="21"/>
<rectangle x1="-0.034925" y1="1.044575" x2="0.180975" y2="1.050925" layer="21"/>
<rectangle x1="0.466725" y1="1.044575" x2="0.676275" y2="1.050925" layer="21"/>
<rectangle x1="0.911225" y1="1.044575" x2="1.120775" y2="1.050925" layer="21"/>
<rectangle x1="-0.974725" y1="1.050925" x2="-0.765175" y2="1.057275" layer="21"/>
<rectangle x1="-0.530225" y1="1.050925" x2="-0.320675" y2="1.057275" layer="21"/>
<rectangle x1="-0.034925" y1="1.050925" x2="0.180975" y2="1.057275" layer="21"/>
<rectangle x1="0.466725" y1="1.050925" x2="0.676275" y2="1.057275" layer="21"/>
<rectangle x1="0.911225" y1="1.050925" x2="1.120775" y2="1.057275" layer="21"/>
<rectangle x1="-0.974725" y1="1.057275" x2="-0.765175" y2="1.063625" layer="21"/>
<rectangle x1="-0.530225" y1="1.057275" x2="-0.320675" y2="1.063625" layer="21"/>
<rectangle x1="-0.034925" y1="1.057275" x2="0.180975" y2="1.063625" layer="21"/>
<rectangle x1="0.466725" y1="1.057275" x2="0.676275" y2="1.063625" layer="21"/>
<rectangle x1="0.911225" y1="1.057275" x2="1.120775" y2="1.063625" layer="21"/>
<rectangle x1="-0.974725" y1="1.063625" x2="-0.765175" y2="1.069975" layer="21"/>
<rectangle x1="-0.530225" y1="1.063625" x2="-0.320675" y2="1.069975" layer="21"/>
<rectangle x1="-0.034925" y1="1.063625" x2="0.180975" y2="1.069975" layer="21"/>
<rectangle x1="0.466725" y1="1.063625" x2="0.676275" y2="1.069975" layer="21"/>
<rectangle x1="0.911225" y1="1.063625" x2="1.120775" y2="1.069975" layer="21"/>
<rectangle x1="-0.974725" y1="1.069975" x2="-0.765175" y2="1.076325" layer="21"/>
<rectangle x1="-0.530225" y1="1.069975" x2="-0.320675" y2="1.076325" layer="21"/>
<rectangle x1="-0.034925" y1="1.069975" x2="0.180975" y2="1.076325" layer="21"/>
<rectangle x1="0.466725" y1="1.069975" x2="0.676275" y2="1.076325" layer="21"/>
<rectangle x1="0.911225" y1="1.069975" x2="1.120775" y2="1.076325" layer="21"/>
<rectangle x1="-0.974725" y1="1.076325" x2="-0.765175" y2="1.082675" layer="21"/>
<rectangle x1="-0.530225" y1="1.076325" x2="-0.320675" y2="1.082675" layer="21"/>
<rectangle x1="-0.034925" y1="1.076325" x2="0.180975" y2="1.082675" layer="21"/>
<rectangle x1="0.466725" y1="1.076325" x2="0.676275" y2="1.082675" layer="21"/>
<rectangle x1="0.911225" y1="1.076325" x2="1.120775" y2="1.082675" layer="21"/>
<rectangle x1="-0.974725" y1="1.082675" x2="-0.765175" y2="1.089025" layer="21"/>
<rectangle x1="-0.530225" y1="1.082675" x2="-0.320675" y2="1.089025" layer="21"/>
<rectangle x1="-0.034925" y1="1.082675" x2="0.180975" y2="1.089025" layer="21"/>
<rectangle x1="0.466725" y1="1.082675" x2="0.676275" y2="1.089025" layer="21"/>
<rectangle x1="0.911225" y1="1.082675" x2="1.120775" y2="1.089025" layer="21"/>
<rectangle x1="-0.974725" y1="1.089025" x2="-0.765175" y2="1.095375" layer="21"/>
<rectangle x1="-0.530225" y1="1.089025" x2="-0.320675" y2="1.095375" layer="21"/>
<rectangle x1="-0.034925" y1="1.089025" x2="0.180975" y2="1.095375" layer="21"/>
<rectangle x1="0.466725" y1="1.089025" x2="0.676275" y2="1.095375" layer="21"/>
<rectangle x1="0.911225" y1="1.089025" x2="1.120775" y2="1.095375" layer="21"/>
<rectangle x1="-0.974725" y1="1.095375" x2="-0.765175" y2="1.101725" layer="21"/>
<rectangle x1="-0.530225" y1="1.095375" x2="-0.320675" y2="1.101725" layer="21"/>
<rectangle x1="-0.034925" y1="1.095375" x2="0.180975" y2="1.101725" layer="21"/>
<rectangle x1="0.466725" y1="1.095375" x2="0.676275" y2="1.101725" layer="21"/>
<rectangle x1="0.911225" y1="1.095375" x2="1.120775" y2="1.101725" layer="21"/>
<rectangle x1="-0.974725" y1="1.101725" x2="-0.765175" y2="1.108075" layer="21"/>
<rectangle x1="-0.530225" y1="1.101725" x2="-0.320675" y2="1.108075" layer="21"/>
<rectangle x1="-0.034925" y1="1.101725" x2="0.180975" y2="1.108075" layer="21"/>
<rectangle x1="0.466725" y1="1.101725" x2="0.676275" y2="1.108075" layer="21"/>
<rectangle x1="0.911225" y1="1.101725" x2="1.120775" y2="1.108075" layer="21"/>
<rectangle x1="-0.974725" y1="1.108075" x2="-0.765175" y2="1.114425" layer="21"/>
<rectangle x1="-0.530225" y1="1.108075" x2="-0.320675" y2="1.114425" layer="21"/>
<rectangle x1="-0.034925" y1="1.108075" x2="0.180975" y2="1.114425" layer="21"/>
<rectangle x1="0.466725" y1="1.108075" x2="0.676275" y2="1.114425" layer="21"/>
<rectangle x1="0.911225" y1="1.108075" x2="1.120775" y2="1.114425" layer="21"/>
<rectangle x1="-0.974725" y1="1.114425" x2="-0.765175" y2="1.120775" layer="21"/>
<rectangle x1="-0.530225" y1="1.114425" x2="-0.320675" y2="1.120775" layer="21"/>
<rectangle x1="-0.034925" y1="1.114425" x2="0.180975" y2="1.120775" layer="21"/>
<rectangle x1="0.466725" y1="1.114425" x2="0.676275" y2="1.120775" layer="21"/>
<rectangle x1="0.911225" y1="1.114425" x2="1.120775" y2="1.120775" layer="21"/>
<rectangle x1="-0.974725" y1="1.120775" x2="-0.765175" y2="1.127125" layer="21"/>
<rectangle x1="-0.530225" y1="1.120775" x2="-0.320675" y2="1.127125" layer="21"/>
<rectangle x1="-0.034925" y1="1.120775" x2="0.180975" y2="1.127125" layer="21"/>
<rectangle x1="0.466725" y1="1.120775" x2="0.676275" y2="1.127125" layer="21"/>
<rectangle x1="0.911225" y1="1.120775" x2="1.120775" y2="1.127125" layer="21"/>
<rectangle x1="-0.974725" y1="1.127125" x2="-0.765175" y2="1.133475" layer="21"/>
<rectangle x1="-0.530225" y1="1.127125" x2="-0.320675" y2="1.133475" layer="21"/>
<rectangle x1="-0.034925" y1="1.127125" x2="0.180975" y2="1.133475" layer="21"/>
<rectangle x1="0.466725" y1="1.127125" x2="0.676275" y2="1.133475" layer="21"/>
<rectangle x1="0.911225" y1="1.127125" x2="1.120775" y2="1.133475" layer="21"/>
<rectangle x1="-0.974725" y1="1.133475" x2="-0.765175" y2="1.139825" layer="21"/>
<rectangle x1="-0.530225" y1="1.133475" x2="-0.320675" y2="1.139825" layer="21"/>
<rectangle x1="-0.034925" y1="1.133475" x2="0.180975" y2="1.139825" layer="21"/>
<rectangle x1="0.466725" y1="1.133475" x2="0.676275" y2="1.139825" layer="21"/>
<rectangle x1="0.911225" y1="1.133475" x2="1.120775" y2="1.139825" layer="21"/>
<rectangle x1="-0.974725" y1="1.139825" x2="-0.765175" y2="1.146175" layer="21"/>
<rectangle x1="-0.530225" y1="1.139825" x2="-0.320675" y2="1.146175" layer="21"/>
<rectangle x1="-0.034925" y1="1.139825" x2="0.180975" y2="1.146175" layer="21"/>
<rectangle x1="0.466725" y1="1.139825" x2="0.676275" y2="1.146175" layer="21"/>
<rectangle x1="0.911225" y1="1.139825" x2="1.120775" y2="1.146175" layer="21"/>
<rectangle x1="-0.974725" y1="1.146175" x2="-0.765175" y2="1.152525" layer="21"/>
<rectangle x1="-0.530225" y1="1.146175" x2="-0.320675" y2="1.152525" layer="21"/>
<rectangle x1="-0.034925" y1="1.146175" x2="0.180975" y2="1.152525" layer="21"/>
<rectangle x1="0.466725" y1="1.146175" x2="0.676275" y2="1.152525" layer="21"/>
<rectangle x1="0.911225" y1="1.146175" x2="1.120775" y2="1.152525" layer="21"/>
<rectangle x1="-0.974725" y1="1.152525" x2="-0.765175" y2="1.158875" layer="21"/>
<rectangle x1="-0.530225" y1="1.152525" x2="-0.320675" y2="1.158875" layer="21"/>
<rectangle x1="-0.034925" y1="1.152525" x2="0.180975" y2="1.158875" layer="21"/>
<rectangle x1="0.466725" y1="1.152525" x2="0.676275" y2="1.158875" layer="21"/>
<rectangle x1="0.911225" y1="1.152525" x2="1.120775" y2="1.158875" layer="21"/>
<rectangle x1="-0.974725" y1="1.158875" x2="-0.765175" y2="1.165225" layer="21"/>
<rectangle x1="-0.530225" y1="1.158875" x2="-0.320675" y2="1.165225" layer="21"/>
<rectangle x1="-0.034925" y1="1.158875" x2="0.180975" y2="1.165225" layer="21"/>
<rectangle x1="0.466725" y1="1.158875" x2="0.676275" y2="1.165225" layer="21"/>
<rectangle x1="0.911225" y1="1.158875" x2="1.120775" y2="1.165225" layer="21"/>
<rectangle x1="-0.974725" y1="1.165225" x2="-0.765175" y2="1.171575" layer="21"/>
<rectangle x1="-0.530225" y1="1.165225" x2="-0.320675" y2="1.171575" layer="21"/>
<rectangle x1="-0.034925" y1="1.165225" x2="0.180975" y2="1.171575" layer="21"/>
<rectangle x1="0.466725" y1="1.165225" x2="0.676275" y2="1.171575" layer="21"/>
<rectangle x1="0.911225" y1="1.165225" x2="1.120775" y2="1.171575" layer="21"/>
<rectangle x1="-0.974725" y1="1.171575" x2="-0.765175" y2="1.177925" layer="21"/>
<rectangle x1="-0.530225" y1="1.171575" x2="-0.320675" y2="1.177925" layer="21"/>
<rectangle x1="-0.034925" y1="1.171575" x2="0.180975" y2="1.177925" layer="21"/>
<rectangle x1="0.466725" y1="1.171575" x2="0.676275" y2="1.177925" layer="21"/>
<rectangle x1="0.911225" y1="1.171575" x2="1.120775" y2="1.177925" layer="21"/>
<rectangle x1="-0.974725" y1="1.177925" x2="-0.765175" y2="1.184275" layer="21"/>
<rectangle x1="-0.530225" y1="1.177925" x2="-0.320675" y2="1.184275" layer="21"/>
<rectangle x1="-0.034925" y1="1.177925" x2="0.180975" y2="1.184275" layer="21"/>
<rectangle x1="0.466725" y1="1.177925" x2="0.676275" y2="1.184275" layer="21"/>
<rectangle x1="0.911225" y1="1.177925" x2="1.120775" y2="1.184275" layer="21"/>
<rectangle x1="-0.974725" y1="1.184275" x2="-0.765175" y2="1.190625" layer="21"/>
<rectangle x1="-0.530225" y1="1.184275" x2="-0.320675" y2="1.190625" layer="21"/>
<rectangle x1="-0.034925" y1="1.184275" x2="0.180975" y2="1.190625" layer="21"/>
<rectangle x1="0.466725" y1="1.184275" x2="0.676275" y2="1.190625" layer="21"/>
<rectangle x1="0.911225" y1="1.184275" x2="1.120775" y2="1.190625" layer="21"/>
<rectangle x1="-0.974725" y1="1.190625" x2="-0.765175" y2="1.196975" layer="21"/>
<rectangle x1="-0.530225" y1="1.190625" x2="-0.320675" y2="1.196975" layer="21"/>
<rectangle x1="-0.034925" y1="1.190625" x2="0.180975" y2="1.196975" layer="21"/>
<rectangle x1="0.466725" y1="1.190625" x2="0.676275" y2="1.196975" layer="21"/>
<rectangle x1="0.911225" y1="1.190625" x2="1.120775" y2="1.196975" layer="21"/>
<rectangle x1="-0.974725" y1="1.196975" x2="-0.765175" y2="1.203325" layer="21"/>
<rectangle x1="-0.530225" y1="1.196975" x2="-0.320675" y2="1.203325" layer="21"/>
<rectangle x1="-0.034925" y1="1.196975" x2="0.180975" y2="1.203325" layer="21"/>
<rectangle x1="0.466725" y1="1.196975" x2="0.676275" y2="1.203325" layer="21"/>
<rectangle x1="0.911225" y1="1.196975" x2="1.120775" y2="1.203325" layer="21"/>
<rectangle x1="-0.974725" y1="1.203325" x2="-0.765175" y2="1.209675" layer="21"/>
<rectangle x1="-0.530225" y1="1.203325" x2="-0.320675" y2="1.209675" layer="21"/>
<rectangle x1="-0.034925" y1="1.203325" x2="0.180975" y2="1.209675" layer="21"/>
<rectangle x1="0.466725" y1="1.203325" x2="0.676275" y2="1.209675" layer="21"/>
<rectangle x1="0.911225" y1="1.203325" x2="1.120775" y2="1.209675" layer="21"/>
<rectangle x1="-0.974725" y1="1.209675" x2="-0.765175" y2="1.216025" layer="21"/>
<rectangle x1="-0.530225" y1="1.209675" x2="-0.320675" y2="1.216025" layer="21"/>
<rectangle x1="-0.034925" y1="1.209675" x2="0.180975" y2="1.216025" layer="21"/>
<rectangle x1="0.466725" y1="1.209675" x2="0.676275" y2="1.216025" layer="21"/>
<rectangle x1="0.911225" y1="1.209675" x2="1.120775" y2="1.216025" layer="21"/>
<rectangle x1="-0.974725" y1="1.216025" x2="-0.765175" y2="1.222375" layer="21"/>
<rectangle x1="-0.530225" y1="1.216025" x2="-0.320675" y2="1.222375" layer="21"/>
<rectangle x1="-0.034925" y1="1.216025" x2="0.180975" y2="1.222375" layer="21"/>
<rectangle x1="0.466725" y1="1.216025" x2="0.676275" y2="1.222375" layer="21"/>
<rectangle x1="0.911225" y1="1.216025" x2="1.120775" y2="1.222375" layer="21"/>
<rectangle x1="-0.974725" y1="1.222375" x2="-0.765175" y2="1.228725" layer="21"/>
<rectangle x1="-0.530225" y1="1.222375" x2="-0.320675" y2="1.228725" layer="21"/>
<rectangle x1="-0.034925" y1="1.222375" x2="0.180975" y2="1.228725" layer="21"/>
<rectangle x1="0.466725" y1="1.222375" x2="0.676275" y2="1.228725" layer="21"/>
<rectangle x1="0.911225" y1="1.222375" x2="1.120775" y2="1.228725" layer="21"/>
<rectangle x1="-0.974725" y1="1.228725" x2="-0.765175" y2="1.235075" layer="21"/>
<rectangle x1="-0.530225" y1="1.228725" x2="-0.320675" y2="1.235075" layer="21"/>
<rectangle x1="-0.034925" y1="1.228725" x2="0.180975" y2="1.235075" layer="21"/>
<rectangle x1="0.466725" y1="1.228725" x2="0.676275" y2="1.235075" layer="21"/>
<rectangle x1="0.911225" y1="1.228725" x2="1.120775" y2="1.235075" layer="21"/>
<rectangle x1="-0.974725" y1="1.235075" x2="-0.765175" y2="1.241425" layer="21"/>
<rectangle x1="-0.530225" y1="1.235075" x2="-0.320675" y2="1.241425" layer="21"/>
<rectangle x1="-0.034925" y1="1.235075" x2="0.180975" y2="1.241425" layer="21"/>
<rectangle x1="0.466725" y1="1.235075" x2="0.676275" y2="1.241425" layer="21"/>
<rectangle x1="0.911225" y1="1.235075" x2="1.120775" y2="1.241425" layer="21"/>
<rectangle x1="-0.974725" y1="1.241425" x2="-0.765175" y2="1.247775" layer="21"/>
<rectangle x1="-0.530225" y1="1.241425" x2="-0.320675" y2="1.247775" layer="21"/>
<rectangle x1="-0.034925" y1="1.241425" x2="0.180975" y2="1.247775" layer="21"/>
<rectangle x1="0.466725" y1="1.241425" x2="0.676275" y2="1.247775" layer="21"/>
<rectangle x1="0.911225" y1="1.241425" x2="1.120775" y2="1.247775" layer="21"/>
<rectangle x1="-0.974725" y1="1.247775" x2="-0.765175" y2="1.254125" layer="21"/>
<rectangle x1="-0.530225" y1="1.247775" x2="-0.320675" y2="1.254125" layer="21"/>
<rectangle x1="-0.034925" y1="1.247775" x2="0.180975" y2="1.254125" layer="21"/>
<rectangle x1="0.466725" y1="1.247775" x2="0.676275" y2="1.254125" layer="21"/>
<rectangle x1="0.911225" y1="1.247775" x2="1.120775" y2="1.254125" layer="21"/>
<rectangle x1="-0.974725" y1="1.254125" x2="-0.765175" y2="1.260475" layer="21"/>
<rectangle x1="-0.530225" y1="1.254125" x2="-0.320675" y2="1.260475" layer="21"/>
<rectangle x1="-0.034925" y1="1.254125" x2="0.180975" y2="1.260475" layer="21"/>
<rectangle x1="0.466725" y1="1.254125" x2="0.676275" y2="1.260475" layer="21"/>
<rectangle x1="0.911225" y1="1.254125" x2="1.120775" y2="1.260475" layer="21"/>
<rectangle x1="-0.974725" y1="1.260475" x2="-0.765175" y2="1.266825" layer="21"/>
<rectangle x1="-0.530225" y1="1.260475" x2="-0.320675" y2="1.266825" layer="21"/>
<rectangle x1="-0.034925" y1="1.260475" x2="0.180975" y2="1.266825" layer="21"/>
<rectangle x1="0.466725" y1="1.260475" x2="0.676275" y2="1.266825" layer="21"/>
<rectangle x1="0.911225" y1="1.260475" x2="1.120775" y2="1.266825" layer="21"/>
<rectangle x1="-0.974725" y1="1.266825" x2="-0.765175" y2="1.273175" layer="21"/>
<rectangle x1="-0.530225" y1="1.266825" x2="-0.320675" y2="1.273175" layer="21"/>
<rectangle x1="-0.034925" y1="1.266825" x2="0.180975" y2="1.273175" layer="21"/>
<rectangle x1="0.466725" y1="1.266825" x2="0.676275" y2="1.273175" layer="21"/>
<rectangle x1="0.911225" y1="1.266825" x2="1.120775" y2="1.273175" layer="21"/>
<rectangle x1="-0.974725" y1="1.273175" x2="-0.765175" y2="1.279525" layer="21"/>
<rectangle x1="-0.530225" y1="1.273175" x2="-0.320675" y2="1.279525" layer="21"/>
<rectangle x1="-0.034925" y1="1.273175" x2="0.180975" y2="1.279525" layer="21"/>
<rectangle x1="0.466725" y1="1.273175" x2="0.676275" y2="1.279525" layer="21"/>
<rectangle x1="0.911225" y1="1.273175" x2="1.120775" y2="1.279525" layer="21"/>
<rectangle x1="-0.974725" y1="1.279525" x2="-0.765175" y2="1.285875" layer="21"/>
<rectangle x1="-0.530225" y1="1.279525" x2="-0.320675" y2="1.285875" layer="21"/>
<rectangle x1="-0.034925" y1="1.279525" x2="0.180975" y2="1.285875" layer="21"/>
<rectangle x1="0.466725" y1="1.279525" x2="0.676275" y2="1.285875" layer="21"/>
<rectangle x1="0.911225" y1="1.279525" x2="1.120775" y2="1.285875" layer="21"/>
<rectangle x1="-0.974725" y1="1.285875" x2="-0.765175" y2="1.292225" layer="21"/>
<rectangle x1="-0.530225" y1="1.285875" x2="-0.320675" y2="1.292225" layer="21"/>
<rectangle x1="-0.034925" y1="1.285875" x2="0.180975" y2="1.292225" layer="21"/>
<rectangle x1="0.466725" y1="1.285875" x2="0.676275" y2="1.292225" layer="21"/>
<rectangle x1="0.911225" y1="1.285875" x2="1.120775" y2="1.292225" layer="21"/>
<rectangle x1="-0.974725" y1="1.292225" x2="-0.765175" y2="1.298575" layer="21"/>
<rectangle x1="-0.530225" y1="1.292225" x2="-0.320675" y2="1.298575" layer="21"/>
<rectangle x1="-0.034925" y1="1.292225" x2="0.180975" y2="1.298575" layer="21"/>
<rectangle x1="0.466725" y1="1.292225" x2="0.676275" y2="1.298575" layer="21"/>
<rectangle x1="0.911225" y1="1.292225" x2="1.120775" y2="1.298575" layer="21"/>
<rectangle x1="-0.974725" y1="1.298575" x2="-0.765175" y2="1.304925" layer="21"/>
<rectangle x1="-0.530225" y1="1.298575" x2="-0.320675" y2="1.304925" layer="21"/>
<rectangle x1="-0.034925" y1="1.298575" x2="0.180975" y2="1.304925" layer="21"/>
<rectangle x1="0.466725" y1="1.298575" x2="0.676275" y2="1.304925" layer="21"/>
<rectangle x1="0.911225" y1="1.298575" x2="1.120775" y2="1.304925" layer="21"/>
<rectangle x1="-0.974725" y1="1.304925" x2="-0.765175" y2="1.311275" layer="21"/>
<rectangle x1="-0.530225" y1="1.304925" x2="-0.320675" y2="1.311275" layer="21"/>
<rectangle x1="-0.034925" y1="1.304925" x2="0.180975" y2="1.311275" layer="21"/>
<rectangle x1="0.466725" y1="1.304925" x2="0.676275" y2="1.311275" layer="21"/>
<rectangle x1="0.911225" y1="1.304925" x2="1.120775" y2="1.311275" layer="21"/>
<rectangle x1="-0.974725" y1="1.311275" x2="-0.765175" y2="1.317625" layer="21"/>
<rectangle x1="-0.530225" y1="1.311275" x2="-0.320675" y2="1.317625" layer="21"/>
<rectangle x1="-0.034925" y1="1.311275" x2="0.180975" y2="1.317625" layer="21"/>
<rectangle x1="0.466725" y1="1.311275" x2="0.676275" y2="1.317625" layer="21"/>
<rectangle x1="0.911225" y1="1.311275" x2="1.120775" y2="1.317625" layer="21"/>
<rectangle x1="-0.974725" y1="1.317625" x2="-0.765175" y2="1.323975" layer="21"/>
<rectangle x1="-0.530225" y1="1.317625" x2="-0.320675" y2="1.323975" layer="21"/>
<rectangle x1="-0.034925" y1="1.317625" x2="0.180975" y2="1.323975" layer="21"/>
<rectangle x1="0.466725" y1="1.317625" x2="0.676275" y2="1.323975" layer="21"/>
<rectangle x1="0.911225" y1="1.317625" x2="1.120775" y2="1.323975" layer="21"/>
<rectangle x1="-0.974725" y1="1.323975" x2="-0.765175" y2="1.330325" layer="21"/>
<rectangle x1="-0.530225" y1="1.323975" x2="-0.320675" y2="1.330325" layer="21"/>
<rectangle x1="-0.034925" y1="1.323975" x2="0.180975" y2="1.330325" layer="21"/>
<rectangle x1="0.466725" y1="1.323975" x2="0.676275" y2="1.330325" layer="21"/>
<rectangle x1="0.911225" y1="1.323975" x2="1.120775" y2="1.330325" layer="21"/>
<rectangle x1="-0.974725" y1="1.330325" x2="-0.765175" y2="1.336675" layer="21"/>
<rectangle x1="-0.530225" y1="1.330325" x2="-0.320675" y2="1.336675" layer="21"/>
<rectangle x1="-0.034925" y1="1.330325" x2="0.180975" y2="1.336675" layer="21"/>
<rectangle x1="0.466725" y1="1.330325" x2="0.676275" y2="1.336675" layer="21"/>
<rectangle x1="0.911225" y1="1.330325" x2="1.120775" y2="1.336675" layer="21"/>
<rectangle x1="-0.974725" y1="1.336675" x2="-0.765175" y2="1.343025" layer="21"/>
<rectangle x1="-0.530225" y1="1.336675" x2="-0.320675" y2="1.343025" layer="21"/>
<rectangle x1="-0.034925" y1="1.336675" x2="0.180975" y2="1.343025" layer="21"/>
<rectangle x1="0.466725" y1="1.336675" x2="0.676275" y2="1.343025" layer="21"/>
<rectangle x1="0.911225" y1="1.336675" x2="1.120775" y2="1.343025" layer="21"/>
<rectangle x1="-0.974725" y1="1.343025" x2="-0.765175" y2="1.349375" layer="21"/>
<rectangle x1="-0.530225" y1="1.343025" x2="-0.320675" y2="1.349375" layer="21"/>
<rectangle x1="-0.034925" y1="1.343025" x2="0.180975" y2="1.349375" layer="21"/>
<rectangle x1="0.466725" y1="1.343025" x2="0.676275" y2="1.349375" layer="21"/>
<rectangle x1="0.911225" y1="1.343025" x2="1.120775" y2="1.349375" layer="21"/>
<rectangle x1="-0.974725" y1="1.349375" x2="-0.765175" y2="1.355725" layer="21"/>
<rectangle x1="-0.530225" y1="1.349375" x2="-0.320675" y2="1.355725" layer="21"/>
<rectangle x1="-0.034925" y1="1.349375" x2="0.180975" y2="1.355725" layer="21"/>
<rectangle x1="0.466725" y1="1.349375" x2="0.676275" y2="1.355725" layer="21"/>
<rectangle x1="0.911225" y1="1.349375" x2="1.120775" y2="1.355725" layer="21"/>
<rectangle x1="-0.974725" y1="1.355725" x2="-0.765175" y2="1.362075" layer="21"/>
<rectangle x1="-0.530225" y1="1.355725" x2="-0.320675" y2="1.362075" layer="21"/>
<rectangle x1="-0.034925" y1="1.355725" x2="0.180975" y2="1.362075" layer="21"/>
<rectangle x1="0.466725" y1="1.355725" x2="0.676275" y2="1.362075" layer="21"/>
<rectangle x1="0.911225" y1="1.355725" x2="1.120775" y2="1.362075" layer="21"/>
<rectangle x1="-0.974725" y1="1.362075" x2="-0.765175" y2="1.368425" layer="21"/>
<rectangle x1="-0.530225" y1="1.362075" x2="-0.320675" y2="1.368425" layer="21"/>
<rectangle x1="-0.034925" y1="1.362075" x2="0.180975" y2="1.368425" layer="21"/>
<rectangle x1="0.466725" y1="1.362075" x2="0.676275" y2="1.368425" layer="21"/>
<rectangle x1="0.911225" y1="1.362075" x2="1.120775" y2="1.368425" layer="21"/>
<rectangle x1="-0.974725" y1="1.368425" x2="-0.765175" y2="1.374775" layer="21"/>
<rectangle x1="-0.530225" y1="1.368425" x2="-0.320675" y2="1.374775" layer="21"/>
<rectangle x1="-0.034925" y1="1.368425" x2="0.180975" y2="1.374775" layer="21"/>
<rectangle x1="0.466725" y1="1.368425" x2="0.676275" y2="1.374775" layer="21"/>
<rectangle x1="0.911225" y1="1.368425" x2="1.120775" y2="1.374775" layer="21"/>
<rectangle x1="-0.974725" y1="1.374775" x2="-0.765175" y2="1.381125" layer="21"/>
<rectangle x1="-0.530225" y1="1.374775" x2="-0.320675" y2="1.381125" layer="21"/>
<rectangle x1="-0.034925" y1="1.374775" x2="0.180975" y2="1.381125" layer="21"/>
<rectangle x1="0.466725" y1="1.374775" x2="0.676275" y2="1.381125" layer="21"/>
<rectangle x1="0.911225" y1="1.374775" x2="1.120775" y2="1.381125" layer="21"/>
<rectangle x1="-0.974725" y1="1.381125" x2="-0.765175" y2="1.387475" layer="21"/>
<rectangle x1="-0.530225" y1="1.381125" x2="-0.320675" y2="1.387475" layer="21"/>
<rectangle x1="-0.034925" y1="1.381125" x2="0.180975" y2="1.387475" layer="21"/>
<rectangle x1="0.466725" y1="1.381125" x2="0.676275" y2="1.387475" layer="21"/>
<rectangle x1="0.911225" y1="1.381125" x2="1.120775" y2="1.387475" layer="21"/>
<rectangle x1="-0.974725" y1="1.387475" x2="-0.765175" y2="1.393825" layer="21"/>
<rectangle x1="-0.530225" y1="1.387475" x2="-0.320675" y2="1.393825" layer="21"/>
<rectangle x1="-0.034925" y1="1.387475" x2="0.180975" y2="1.393825" layer="21"/>
<rectangle x1="0.466725" y1="1.387475" x2="0.676275" y2="1.393825" layer="21"/>
<rectangle x1="0.911225" y1="1.387475" x2="1.120775" y2="1.393825" layer="21"/>
<rectangle x1="-0.974725" y1="1.393825" x2="-0.765175" y2="1.400175" layer="21"/>
<rectangle x1="-0.530225" y1="1.393825" x2="-0.320675" y2="1.400175" layer="21"/>
<rectangle x1="-0.034925" y1="1.393825" x2="0.180975" y2="1.400175" layer="21"/>
<rectangle x1="0.466725" y1="1.393825" x2="0.676275" y2="1.400175" layer="21"/>
<rectangle x1="0.911225" y1="1.393825" x2="1.120775" y2="1.400175" layer="21"/>
<rectangle x1="-0.974725" y1="1.400175" x2="-0.765175" y2="1.406525" layer="21"/>
<rectangle x1="-0.530225" y1="1.400175" x2="-0.320675" y2="1.406525" layer="21"/>
<rectangle x1="-0.034925" y1="1.400175" x2="0.180975" y2="1.406525" layer="21"/>
<rectangle x1="0.466725" y1="1.400175" x2="0.676275" y2="1.406525" layer="21"/>
<rectangle x1="0.911225" y1="1.400175" x2="1.120775" y2="1.406525" layer="21"/>
<rectangle x1="-0.974725" y1="1.406525" x2="-0.765175" y2="1.412875" layer="21"/>
<rectangle x1="-0.536575" y1="1.406525" x2="-0.320675" y2="1.412875" layer="21"/>
<rectangle x1="-0.034925" y1="1.406525" x2="0.180975" y2="1.412875" layer="21"/>
<rectangle x1="0.466725" y1="1.406525" x2="0.676275" y2="1.412875" layer="21"/>
<rectangle x1="0.911225" y1="1.406525" x2="1.120775" y2="1.412875" layer="21"/>
<rectangle x1="-0.974725" y1="1.412875" x2="-0.765175" y2="1.419225" layer="21"/>
<rectangle x1="-0.536575" y1="1.412875" x2="-0.320675" y2="1.419225" layer="21"/>
<rectangle x1="-0.034925" y1="1.412875" x2="0.180975" y2="1.419225" layer="21"/>
<rectangle x1="0.466725" y1="1.412875" x2="0.676275" y2="1.419225" layer="21"/>
<rectangle x1="0.911225" y1="1.412875" x2="1.120775" y2="1.419225" layer="21"/>
<rectangle x1="-0.974725" y1="1.419225" x2="-0.765175" y2="1.425575" layer="21"/>
<rectangle x1="-0.536575" y1="1.419225" x2="-0.320675" y2="1.425575" layer="21"/>
<rectangle x1="-0.034925" y1="1.419225" x2="0.180975" y2="1.425575" layer="21"/>
<rectangle x1="0.466725" y1="1.419225" x2="0.676275" y2="1.425575" layer="21"/>
<rectangle x1="0.911225" y1="1.419225" x2="1.120775" y2="1.425575" layer="21"/>
<rectangle x1="-0.974725" y1="1.425575" x2="-0.765175" y2="1.431925" layer="21"/>
<rectangle x1="-0.536575" y1="1.425575" x2="-0.320675" y2="1.431925" layer="21"/>
<rectangle x1="-0.034925" y1="1.425575" x2="0.180975" y2="1.431925" layer="21"/>
<rectangle x1="0.466725" y1="1.425575" x2="0.676275" y2="1.431925" layer="21"/>
<rectangle x1="0.911225" y1="1.425575" x2="1.120775" y2="1.431925" layer="21"/>
<rectangle x1="-0.974725" y1="1.431925" x2="-0.765175" y2="1.438275" layer="21"/>
<rectangle x1="-0.536575" y1="1.431925" x2="-0.320675" y2="1.438275" layer="21"/>
<rectangle x1="-0.034925" y1="1.431925" x2="0.180975" y2="1.438275" layer="21"/>
<rectangle x1="0.466725" y1="1.431925" x2="0.676275" y2="1.438275" layer="21"/>
<rectangle x1="0.911225" y1="1.431925" x2="1.120775" y2="1.438275" layer="21"/>
<rectangle x1="-0.974725" y1="1.438275" x2="-0.765175" y2="1.444625" layer="21"/>
<rectangle x1="-0.536575" y1="1.438275" x2="-0.320675" y2="1.444625" layer="21"/>
<rectangle x1="-0.034925" y1="1.438275" x2="0.180975" y2="1.444625" layer="21"/>
<rectangle x1="0.466725" y1="1.438275" x2="0.676275" y2="1.444625" layer="21"/>
<rectangle x1="0.911225" y1="1.438275" x2="1.120775" y2="1.444625" layer="21"/>
<rectangle x1="-0.974725" y1="1.444625" x2="-0.765175" y2="1.450975" layer="21"/>
<rectangle x1="-0.536575" y1="1.444625" x2="-0.320675" y2="1.450975" layer="21"/>
<rectangle x1="-0.034925" y1="1.444625" x2="0.180975" y2="1.450975" layer="21"/>
<rectangle x1="0.466725" y1="1.444625" x2="0.676275" y2="1.450975" layer="21"/>
<rectangle x1="0.911225" y1="1.444625" x2="1.120775" y2="1.450975" layer="21"/>
<rectangle x1="-0.974725" y1="1.450975" x2="-0.765175" y2="1.457325" layer="21"/>
<rectangle x1="-0.536575" y1="1.450975" x2="-0.320675" y2="1.457325" layer="21"/>
<rectangle x1="-0.034925" y1="1.450975" x2="0.180975" y2="1.457325" layer="21"/>
<rectangle x1="0.466725" y1="1.450975" x2="0.676275" y2="1.457325" layer="21"/>
<rectangle x1="0.911225" y1="1.450975" x2="1.120775" y2="1.457325" layer="21"/>
<rectangle x1="-0.974725" y1="1.457325" x2="-0.765175" y2="1.463675" layer="21"/>
<rectangle x1="-0.536575" y1="1.457325" x2="-0.320675" y2="1.463675" layer="21"/>
<rectangle x1="-0.034925" y1="1.457325" x2="0.180975" y2="1.463675" layer="21"/>
<rectangle x1="0.466725" y1="1.457325" x2="0.676275" y2="1.463675" layer="21"/>
<rectangle x1="0.911225" y1="1.457325" x2="1.120775" y2="1.463675" layer="21"/>
<rectangle x1="-0.974725" y1="1.463675" x2="-0.765175" y2="1.470025" layer="21"/>
<rectangle x1="-0.536575" y1="1.463675" x2="-0.320675" y2="1.470025" layer="21"/>
<rectangle x1="-0.034925" y1="1.463675" x2="0.180975" y2="1.470025" layer="21"/>
<rectangle x1="0.466725" y1="1.463675" x2="0.676275" y2="1.470025" layer="21"/>
<rectangle x1="0.911225" y1="1.463675" x2="1.120775" y2="1.470025" layer="21"/>
<rectangle x1="-0.974725" y1="1.470025" x2="-0.765175" y2="1.476375" layer="21"/>
<rectangle x1="-0.536575" y1="1.470025" x2="-0.320675" y2="1.476375" layer="21"/>
<rectangle x1="-0.034925" y1="1.470025" x2="0.180975" y2="1.476375" layer="21"/>
<rectangle x1="0.466725" y1="1.470025" x2="0.676275" y2="1.476375" layer="21"/>
<rectangle x1="0.911225" y1="1.470025" x2="1.120775" y2="1.476375" layer="21"/>
<rectangle x1="-0.974725" y1="1.476375" x2="-0.765175" y2="1.482725" layer="21"/>
<rectangle x1="-0.536575" y1="1.476375" x2="-0.320675" y2="1.482725" layer="21"/>
<rectangle x1="-0.034925" y1="1.476375" x2="0.180975" y2="1.482725" layer="21"/>
<rectangle x1="0.466725" y1="1.476375" x2="0.676275" y2="1.482725" layer="21"/>
<rectangle x1="0.911225" y1="1.476375" x2="1.120775" y2="1.482725" layer="21"/>
<rectangle x1="-0.974725" y1="1.482725" x2="-0.765175" y2="1.489075" layer="21"/>
<rectangle x1="-0.536575" y1="1.482725" x2="-0.320675" y2="1.489075" layer="21"/>
<rectangle x1="-0.034925" y1="1.482725" x2="0.180975" y2="1.489075" layer="21"/>
<rectangle x1="0.466725" y1="1.482725" x2="0.676275" y2="1.489075" layer="21"/>
<rectangle x1="0.911225" y1="1.482725" x2="1.120775" y2="1.489075" layer="21"/>
<rectangle x1="-0.974725" y1="1.489075" x2="-0.765175" y2="1.495425" layer="21"/>
<rectangle x1="-0.536575" y1="1.489075" x2="-0.320675" y2="1.495425" layer="21"/>
<rectangle x1="-0.034925" y1="1.489075" x2="0.180975" y2="1.495425" layer="21"/>
<rectangle x1="0.466725" y1="1.489075" x2="0.676275" y2="1.495425" layer="21"/>
<rectangle x1="0.911225" y1="1.489075" x2="1.120775" y2="1.495425" layer="21"/>
<rectangle x1="-0.974725" y1="1.495425" x2="-0.765175" y2="1.501775" layer="21"/>
<rectangle x1="-0.536575" y1="1.495425" x2="-0.320675" y2="1.501775" layer="21"/>
<rectangle x1="-0.034925" y1="1.495425" x2="0.180975" y2="1.501775" layer="21"/>
<rectangle x1="0.466725" y1="1.495425" x2="0.676275" y2="1.501775" layer="21"/>
<rectangle x1="0.911225" y1="1.495425" x2="1.120775" y2="1.501775" layer="21"/>
<rectangle x1="-0.974725" y1="1.501775" x2="-0.765175" y2="1.508125" layer="21"/>
<rectangle x1="-0.536575" y1="1.501775" x2="-0.320675" y2="1.508125" layer="21"/>
<rectangle x1="-0.034925" y1="1.501775" x2="0.180975" y2="1.508125" layer="21"/>
<rectangle x1="0.466725" y1="1.501775" x2="0.676275" y2="1.508125" layer="21"/>
<rectangle x1="0.911225" y1="1.501775" x2="1.120775" y2="1.508125" layer="21"/>
<rectangle x1="-0.974725" y1="1.508125" x2="-0.765175" y2="1.514475" layer="21"/>
<rectangle x1="-0.536575" y1="1.508125" x2="-0.327025" y2="1.514475" layer="21"/>
<rectangle x1="-0.034925" y1="1.508125" x2="0.180975" y2="1.514475" layer="21"/>
<rectangle x1="0.466725" y1="1.508125" x2="0.676275" y2="1.514475" layer="21"/>
<rectangle x1="0.911225" y1="1.508125" x2="1.120775" y2="1.514475" layer="21"/>
<rectangle x1="-0.974725" y1="1.514475" x2="-0.765175" y2="1.520825" layer="21"/>
<rectangle x1="-0.536575" y1="1.514475" x2="-0.327025" y2="1.520825" layer="21"/>
<rectangle x1="-0.034925" y1="1.514475" x2="0.180975" y2="1.520825" layer="21"/>
<rectangle x1="0.466725" y1="1.514475" x2="0.676275" y2="1.520825" layer="21"/>
<rectangle x1="0.911225" y1="1.514475" x2="1.120775" y2="1.520825" layer="21"/>
<rectangle x1="-0.974725" y1="1.520825" x2="-0.765175" y2="1.527175" layer="21"/>
<rectangle x1="-0.536575" y1="1.520825" x2="-0.327025" y2="1.527175" layer="21"/>
<rectangle x1="-0.034925" y1="1.520825" x2="0.180975" y2="1.527175" layer="21"/>
<rectangle x1="0.466725" y1="1.520825" x2="0.676275" y2="1.527175" layer="21"/>
<rectangle x1="0.911225" y1="1.520825" x2="1.120775" y2="1.527175" layer="21"/>
<rectangle x1="-0.974725" y1="1.527175" x2="-0.765175" y2="1.533525" layer="21"/>
<rectangle x1="-0.536575" y1="1.527175" x2="-0.327025" y2="1.533525" layer="21"/>
<rectangle x1="-0.034925" y1="1.527175" x2="0.180975" y2="1.533525" layer="21"/>
<rectangle x1="0.466725" y1="1.527175" x2="0.676275" y2="1.533525" layer="21"/>
<rectangle x1="0.911225" y1="1.527175" x2="1.120775" y2="1.533525" layer="21"/>
<rectangle x1="-0.974725" y1="1.533525" x2="-0.765175" y2="1.539875" layer="21"/>
<rectangle x1="-0.542925" y1="1.533525" x2="-0.327025" y2="1.539875" layer="21"/>
<rectangle x1="-0.034925" y1="1.533525" x2="0.180975" y2="1.539875" layer="21"/>
<rectangle x1="0.466725" y1="1.533525" x2="0.676275" y2="1.539875" layer="21"/>
<rectangle x1="0.911225" y1="1.533525" x2="1.120775" y2="1.539875" layer="21"/>
<rectangle x1="-0.974725" y1="1.539875" x2="-0.765175" y2="1.546225" layer="21"/>
<rectangle x1="-0.542925" y1="1.539875" x2="-0.327025" y2="1.546225" layer="21"/>
<rectangle x1="-0.034925" y1="1.539875" x2="0.180975" y2="1.546225" layer="21"/>
<rectangle x1="0.466725" y1="1.539875" x2="0.676275" y2="1.546225" layer="21"/>
<rectangle x1="0.911225" y1="1.539875" x2="1.120775" y2="1.546225" layer="21"/>
<rectangle x1="-0.974725" y1="1.546225" x2="-0.765175" y2="1.552575" layer="21"/>
<rectangle x1="-0.542925" y1="1.546225" x2="-0.327025" y2="1.552575" layer="21"/>
<rectangle x1="-0.034925" y1="1.546225" x2="0.180975" y2="1.552575" layer="21"/>
<rectangle x1="0.466725" y1="1.546225" x2="0.676275" y2="1.552575" layer="21"/>
<rectangle x1="0.911225" y1="1.546225" x2="1.120775" y2="1.552575" layer="21"/>
<rectangle x1="-0.974725" y1="1.552575" x2="-0.765175" y2="1.558925" layer="21"/>
<rectangle x1="-0.542925" y1="1.552575" x2="-0.327025" y2="1.558925" layer="21"/>
<rectangle x1="-0.034925" y1="1.552575" x2="0.180975" y2="1.558925" layer="21"/>
<rectangle x1="0.466725" y1="1.552575" x2="0.676275" y2="1.558925" layer="21"/>
<rectangle x1="0.911225" y1="1.552575" x2="1.120775" y2="1.558925" layer="21"/>
<rectangle x1="-0.974725" y1="1.558925" x2="-0.765175" y2="1.565275" layer="21"/>
<rectangle x1="-0.542925" y1="1.558925" x2="-0.327025" y2="1.565275" layer="21"/>
<rectangle x1="-0.034925" y1="1.558925" x2="0.180975" y2="1.565275" layer="21"/>
<rectangle x1="0.466725" y1="1.558925" x2="0.676275" y2="1.565275" layer="21"/>
<rectangle x1="0.911225" y1="1.558925" x2="1.120775" y2="1.565275" layer="21"/>
<rectangle x1="-0.974725" y1="1.565275" x2="-0.765175" y2="1.571625" layer="21"/>
<rectangle x1="-0.542925" y1="1.565275" x2="-0.333375" y2="1.571625" layer="21"/>
<rectangle x1="-0.034925" y1="1.565275" x2="0.180975" y2="1.571625" layer="21"/>
<rectangle x1="0.466725" y1="1.565275" x2="0.676275" y2="1.571625" layer="21"/>
<rectangle x1="0.911225" y1="1.565275" x2="1.120775" y2="1.571625" layer="21"/>
<rectangle x1="-0.974725" y1="1.571625" x2="-0.765175" y2="1.577975" layer="21"/>
<rectangle x1="-0.549275" y1="1.571625" x2="-0.333375" y2="1.577975" layer="21"/>
<rectangle x1="-0.034925" y1="1.571625" x2="0.180975" y2="1.577975" layer="21"/>
<rectangle x1="0.466725" y1="1.571625" x2="0.676275" y2="1.577975" layer="21"/>
<rectangle x1="0.911225" y1="1.571625" x2="1.120775" y2="1.577975" layer="21"/>
<rectangle x1="-0.974725" y1="1.577975" x2="-0.765175" y2="1.584325" layer="21"/>
<rectangle x1="-0.549275" y1="1.577975" x2="-0.333375" y2="1.584325" layer="21"/>
<rectangle x1="-0.034925" y1="1.577975" x2="0.180975" y2="1.584325" layer="21"/>
<rectangle x1="0.466725" y1="1.577975" x2="0.676275" y2="1.584325" layer="21"/>
<rectangle x1="0.911225" y1="1.577975" x2="1.120775" y2="1.584325" layer="21"/>
<rectangle x1="-0.974725" y1="1.584325" x2="-0.765175" y2="1.590675" layer="21"/>
<rectangle x1="-0.549275" y1="1.584325" x2="-0.333375" y2="1.590675" layer="21"/>
<rectangle x1="-0.034925" y1="1.584325" x2="0.180975" y2="1.590675" layer="21"/>
<rectangle x1="0.466725" y1="1.584325" x2="0.676275" y2="1.590675" layer="21"/>
<rectangle x1="0.911225" y1="1.584325" x2="1.120775" y2="1.590675" layer="21"/>
<rectangle x1="-0.974725" y1="1.590675" x2="-0.765175" y2="1.597025" layer="21"/>
<rectangle x1="-0.555625" y1="1.590675" x2="-0.333375" y2="1.597025" layer="21"/>
<rectangle x1="-0.034925" y1="1.590675" x2="0.180975" y2="1.597025" layer="21"/>
<rectangle x1="0.466725" y1="1.590675" x2="0.676275" y2="1.597025" layer="21"/>
<rectangle x1="0.911225" y1="1.590675" x2="1.120775" y2="1.597025" layer="21"/>
<rectangle x1="-0.974725" y1="1.597025" x2="-0.765175" y2="1.603375" layer="21"/>
<rectangle x1="-0.555625" y1="1.597025" x2="-0.333375" y2="1.603375" layer="21"/>
<rectangle x1="-0.034925" y1="1.597025" x2="0.180975" y2="1.603375" layer="21"/>
<rectangle x1="0.466725" y1="1.597025" x2="0.676275" y2="1.603375" layer="21"/>
<rectangle x1="0.911225" y1="1.597025" x2="1.120775" y2="1.603375" layer="21"/>
<rectangle x1="-0.974725" y1="1.603375" x2="-0.765175" y2="1.609725" layer="21"/>
<rectangle x1="-0.561975" y1="1.603375" x2="-0.339725" y2="1.609725" layer="21"/>
<rectangle x1="-0.034925" y1="1.603375" x2="0.180975" y2="1.609725" layer="21"/>
<rectangle x1="0.466725" y1="1.603375" x2="0.676275" y2="1.609725" layer="21"/>
<rectangle x1="0.911225" y1="1.603375" x2="1.120775" y2="1.609725" layer="21"/>
<rectangle x1="-0.974725" y1="1.609725" x2="-0.765175" y2="1.616075" layer="21"/>
<rectangle x1="-0.561975" y1="1.609725" x2="-0.339725" y2="1.616075" layer="21"/>
<rectangle x1="-0.034925" y1="1.609725" x2="0.180975" y2="1.616075" layer="21"/>
<rectangle x1="0.466725" y1="1.609725" x2="0.676275" y2="1.616075" layer="21"/>
<rectangle x1="0.911225" y1="1.609725" x2="1.120775" y2="1.616075" layer="21"/>
<rectangle x1="-0.974725" y1="1.616075" x2="-0.765175" y2="1.622425" layer="21"/>
<rectangle x1="-0.561975" y1="1.616075" x2="-0.339725" y2="1.622425" layer="21"/>
<rectangle x1="-0.034925" y1="1.616075" x2="0.180975" y2="1.622425" layer="21"/>
<rectangle x1="0.466725" y1="1.616075" x2="0.676275" y2="1.622425" layer="21"/>
<rectangle x1="0.911225" y1="1.616075" x2="1.120775" y2="1.622425" layer="21"/>
<rectangle x1="-0.974725" y1="1.622425" x2="-0.765175" y2="1.628775" layer="21"/>
<rectangle x1="-0.568325" y1="1.622425" x2="-0.339725" y2="1.628775" layer="21"/>
<rectangle x1="-0.034925" y1="1.622425" x2="0.180975" y2="1.628775" layer="21"/>
<rectangle x1="0.466725" y1="1.622425" x2="0.676275" y2="1.628775" layer="21"/>
<rectangle x1="0.911225" y1="1.622425" x2="1.120775" y2="1.628775" layer="21"/>
<rectangle x1="-0.974725" y1="1.628775" x2="-0.765175" y2="1.635125" layer="21"/>
<rectangle x1="-0.574675" y1="1.628775" x2="-0.346075" y2="1.635125" layer="21"/>
<rectangle x1="-0.034925" y1="1.628775" x2="0.180975" y2="1.635125" layer="21"/>
<rectangle x1="0.466725" y1="1.628775" x2="0.676275" y2="1.635125" layer="21"/>
<rectangle x1="0.911225" y1="1.628775" x2="1.120775" y2="1.635125" layer="21"/>
<rectangle x1="-0.974725" y1="1.635125" x2="-0.765175" y2="1.641475" layer="21"/>
<rectangle x1="-0.581025" y1="1.635125" x2="-0.346075" y2="1.641475" layer="21"/>
<rectangle x1="-0.034925" y1="1.635125" x2="0.180975" y2="1.641475" layer="21"/>
<rectangle x1="0.466725" y1="1.635125" x2="0.676275" y2="1.641475" layer="21"/>
<rectangle x1="0.911225" y1="1.635125" x2="1.120775" y2="1.641475" layer="21"/>
<rectangle x1="-0.974725" y1="1.641475" x2="-0.765175" y2="1.647825" layer="21"/>
<rectangle x1="-0.593725" y1="1.641475" x2="-0.346075" y2="1.647825" layer="21"/>
<rectangle x1="-0.034925" y1="1.641475" x2="0.180975" y2="1.647825" layer="21"/>
<rectangle x1="0.466725" y1="1.641475" x2="0.676275" y2="1.647825" layer="21"/>
<rectangle x1="0.911225" y1="1.641475" x2="1.120775" y2="1.647825" layer="21"/>
<rectangle x1="-0.974725" y1="1.647825" x2="-0.765175" y2="1.654175" layer="21"/>
<rectangle x1="-0.606425" y1="1.647825" x2="-0.346075" y2="1.654175" layer="21"/>
<rectangle x1="-0.238125" y1="1.647825" x2="0.384175" y2="1.654175" layer="21"/>
<rectangle x1="0.466725" y1="1.647825" x2="0.676275" y2="1.654175" layer="21"/>
<rectangle x1="0.911225" y1="1.647825" x2="1.120775" y2="1.654175" layer="21"/>
<rectangle x1="-0.974725" y1="1.654175" x2="-0.352425" y2="1.660525" layer="21"/>
<rectangle x1="-0.250825" y1="1.654175" x2="0.390525" y2="1.660525" layer="21"/>
<rectangle x1="0.466725" y1="1.654175" x2="0.676275" y2="1.660525" layer="21"/>
<rectangle x1="0.911225" y1="1.654175" x2="1.120775" y2="1.660525" layer="21"/>
<rectangle x1="-0.974725" y1="1.660525" x2="-0.352425" y2="1.666875" layer="21"/>
<rectangle x1="-0.250825" y1="1.660525" x2="0.396875" y2="1.666875" layer="21"/>
<rectangle x1="0.466725" y1="1.660525" x2="0.676275" y2="1.666875" layer="21"/>
<rectangle x1="0.911225" y1="1.660525" x2="1.120775" y2="1.666875" layer="21"/>
<rectangle x1="-0.974725" y1="1.666875" x2="-0.358775" y2="1.673225" layer="21"/>
<rectangle x1="-0.250825" y1="1.666875" x2="0.396875" y2="1.673225" layer="21"/>
<rectangle x1="0.466725" y1="1.666875" x2="0.676275" y2="1.673225" layer="21"/>
<rectangle x1="0.911225" y1="1.666875" x2="1.120775" y2="1.673225" layer="21"/>
<rectangle x1="-0.974725" y1="1.673225" x2="-0.358775" y2="1.679575" layer="21"/>
<rectangle x1="-0.250825" y1="1.673225" x2="0.396875" y2="1.679575" layer="21"/>
<rectangle x1="0.466725" y1="1.673225" x2="0.676275" y2="1.679575" layer="21"/>
<rectangle x1="0.911225" y1="1.673225" x2="1.120775" y2="1.679575" layer="21"/>
<rectangle x1="-0.974725" y1="1.679575" x2="-0.365125" y2="1.685925" layer="21"/>
<rectangle x1="-0.250825" y1="1.679575" x2="0.396875" y2="1.685925" layer="21"/>
<rectangle x1="0.466725" y1="1.679575" x2="0.676275" y2="1.685925" layer="21"/>
<rectangle x1="0.911225" y1="1.679575" x2="1.120775" y2="1.685925" layer="21"/>
<rectangle x1="-0.974725" y1="1.685925" x2="-0.365125" y2="1.692275" layer="21"/>
<rectangle x1="-0.250825" y1="1.685925" x2="0.396875" y2="1.692275" layer="21"/>
<rectangle x1="0.466725" y1="1.685925" x2="0.676275" y2="1.692275" layer="21"/>
<rectangle x1="0.911225" y1="1.685925" x2="1.120775" y2="1.692275" layer="21"/>
<rectangle x1="-0.974725" y1="1.692275" x2="-0.371475" y2="1.698625" layer="21"/>
<rectangle x1="-0.250825" y1="1.692275" x2="0.396875" y2="1.698625" layer="21"/>
<rectangle x1="0.466725" y1="1.692275" x2="0.676275" y2="1.698625" layer="21"/>
<rectangle x1="0.911225" y1="1.692275" x2="1.120775" y2="1.698625" layer="21"/>
<rectangle x1="-0.974725" y1="1.698625" x2="-0.371475" y2="1.704975" layer="21"/>
<rectangle x1="-0.250825" y1="1.698625" x2="0.396875" y2="1.704975" layer="21"/>
<rectangle x1="0.466725" y1="1.698625" x2="0.676275" y2="1.704975" layer="21"/>
<rectangle x1="0.911225" y1="1.698625" x2="1.120775" y2="1.704975" layer="21"/>
<rectangle x1="-0.974725" y1="1.704975" x2="-0.377825" y2="1.711325" layer="21"/>
<rectangle x1="-0.250825" y1="1.704975" x2="0.396875" y2="1.711325" layer="21"/>
<rectangle x1="0.466725" y1="1.704975" x2="0.676275" y2="1.711325" layer="21"/>
<rectangle x1="0.911225" y1="1.704975" x2="1.120775" y2="1.711325" layer="21"/>
<rectangle x1="-0.974725" y1="1.711325" x2="-0.384175" y2="1.717675" layer="21"/>
<rectangle x1="-0.250825" y1="1.711325" x2="0.396875" y2="1.717675" layer="21"/>
<rectangle x1="0.466725" y1="1.711325" x2="0.676275" y2="1.717675" layer="21"/>
<rectangle x1="0.911225" y1="1.711325" x2="1.120775" y2="1.717675" layer="21"/>
<rectangle x1="-0.974725" y1="1.717675" x2="-0.384175" y2="1.724025" layer="21"/>
<rectangle x1="-0.250825" y1="1.717675" x2="0.396875" y2="1.724025" layer="21"/>
<rectangle x1="0.466725" y1="1.717675" x2="0.676275" y2="1.724025" layer="21"/>
<rectangle x1="0.911225" y1="1.717675" x2="1.120775" y2="1.724025" layer="21"/>
<rectangle x1="-0.974725" y1="1.724025" x2="-0.390525" y2="1.730375" layer="21"/>
<rectangle x1="-0.250825" y1="1.724025" x2="0.396875" y2="1.730375" layer="21"/>
<rectangle x1="0.466725" y1="1.724025" x2="0.676275" y2="1.730375" layer="21"/>
<rectangle x1="0.911225" y1="1.724025" x2="1.120775" y2="1.730375" layer="21"/>
<rectangle x1="-0.974725" y1="1.730375" x2="-0.396875" y2="1.736725" layer="21"/>
<rectangle x1="-0.250825" y1="1.730375" x2="0.396875" y2="1.736725" layer="21"/>
<rectangle x1="0.466725" y1="1.730375" x2="0.676275" y2="1.736725" layer="21"/>
<rectangle x1="0.911225" y1="1.730375" x2="1.120775" y2="1.736725" layer="21"/>
<rectangle x1="-0.974725" y1="1.736725" x2="-0.403225" y2="1.743075" layer="21"/>
<rectangle x1="-0.250825" y1="1.736725" x2="0.396875" y2="1.743075" layer="21"/>
<rectangle x1="0.466725" y1="1.736725" x2="0.676275" y2="1.743075" layer="21"/>
<rectangle x1="0.911225" y1="1.736725" x2="1.120775" y2="1.743075" layer="21"/>
<rectangle x1="-0.974725" y1="1.743075" x2="-0.409575" y2="1.749425" layer="21"/>
<rectangle x1="-0.250825" y1="1.743075" x2="0.396875" y2="1.749425" layer="21"/>
<rectangle x1="0.466725" y1="1.743075" x2="0.676275" y2="1.749425" layer="21"/>
<rectangle x1="0.911225" y1="1.743075" x2="1.120775" y2="1.749425" layer="21"/>
<rectangle x1="-0.974725" y1="1.749425" x2="-0.415925" y2="1.755775" layer="21"/>
<rectangle x1="-0.250825" y1="1.749425" x2="0.396875" y2="1.755775" layer="21"/>
<rectangle x1="0.466725" y1="1.749425" x2="0.676275" y2="1.755775" layer="21"/>
<rectangle x1="0.911225" y1="1.749425" x2="1.120775" y2="1.755775" layer="21"/>
<rectangle x1="-0.974725" y1="1.755775" x2="-0.422275" y2="1.762125" layer="21"/>
<rectangle x1="-0.250825" y1="1.755775" x2="0.396875" y2="1.762125" layer="21"/>
<rectangle x1="0.466725" y1="1.755775" x2="0.676275" y2="1.762125" layer="21"/>
<rectangle x1="0.911225" y1="1.755775" x2="1.120775" y2="1.762125" layer="21"/>
<rectangle x1="-0.974725" y1="1.762125" x2="-0.434975" y2="1.768475" layer="21"/>
<rectangle x1="-0.250825" y1="1.762125" x2="0.396875" y2="1.768475" layer="21"/>
<rectangle x1="0.466725" y1="1.762125" x2="0.676275" y2="1.768475" layer="21"/>
<rectangle x1="0.911225" y1="1.762125" x2="1.120775" y2="1.768475" layer="21"/>
<rectangle x1="-0.974725" y1="1.768475" x2="-0.441325" y2="1.774825" layer="21"/>
<rectangle x1="-0.250825" y1="1.768475" x2="0.396875" y2="1.774825" layer="21"/>
<rectangle x1="0.466725" y1="1.768475" x2="0.676275" y2="1.774825" layer="21"/>
<rectangle x1="0.911225" y1="1.768475" x2="1.120775" y2="1.774825" layer="21"/>
<rectangle x1="-0.974725" y1="1.774825" x2="-0.454025" y2="1.781175" layer="21"/>
<rectangle x1="-0.250825" y1="1.774825" x2="0.396875" y2="1.781175" layer="21"/>
<rectangle x1="0.466725" y1="1.774825" x2="0.676275" y2="1.781175" layer="21"/>
<rectangle x1="0.911225" y1="1.774825" x2="1.120775" y2="1.781175" layer="21"/>
<rectangle x1="-0.974725" y1="1.781175" x2="-0.466725" y2="1.787525" layer="21"/>
<rectangle x1="-0.250825" y1="1.781175" x2="0.396875" y2="1.787525" layer="21"/>
<rectangle x1="0.466725" y1="1.781175" x2="0.676275" y2="1.787525" layer="21"/>
<rectangle x1="0.911225" y1="1.781175" x2="1.120775" y2="1.787525" layer="21"/>
<rectangle x1="-0.974725" y1="1.787525" x2="-0.485775" y2="1.793875" layer="21"/>
<rectangle x1="-0.250825" y1="1.787525" x2="0.396875" y2="1.793875" layer="21"/>
<rectangle x1="0.473075" y1="1.787525" x2="0.676275" y2="1.793875" layer="21"/>
<rectangle x1="0.917575" y1="1.787525" x2="1.120775" y2="1.793875" layer="21"/>
<rectangle x1="-0.974725" y1="1.793875" x2="-0.498475" y2="1.800225" layer="21"/>
<rectangle x1="-0.250825" y1="1.793875" x2="0.396875" y2="1.800225" layer="21"/>
<rectangle x1="0.473075" y1="1.793875" x2="0.676275" y2="1.800225" layer="21"/>
<rectangle x1="0.917575" y1="1.793875" x2="1.120775" y2="1.800225" layer="21"/>
<rectangle x1="-0.968375" y1="1.800225" x2="-0.523875" y2="1.806575" layer="21"/>
<rectangle x1="-0.244475" y1="1.800225" x2="0.390525" y2="1.806575" layer="21"/>
<rectangle x1="0.479425" y1="1.800225" x2="0.669925" y2="1.806575" layer="21"/>
<rectangle x1="0.923925" y1="1.800225" x2="1.114425" y2="1.806575" layer="21"/>
</package>
<package name="DTU-MEDIUM-13MM">
<rectangle x1="-3.408" y1="-10.512" x2="-3.376" y2="-10.48" layer="21"/>
<rectangle x1="3.376" y1="-10.512" x2="3.408" y2="-10.48" layer="21"/>
<rectangle x1="-3.472" y1="-10.48" x2="-3.28" y2="-10.448" layer="21"/>
<rectangle x1="3.28" y1="-10.48" x2="3.472" y2="-10.448" layer="21"/>
<rectangle x1="-3.536" y1="-10.448" x2="-3.216" y2="-10.416" layer="21"/>
<rectangle x1="3.216" y1="-10.448" x2="3.536" y2="-10.416" layer="21"/>
<rectangle x1="-3.6" y1="-10.416" x2="-3.12" y2="-10.384" layer="21"/>
<rectangle x1="3.12" y1="-10.416" x2="3.6" y2="-10.384" layer="21"/>
<rectangle x1="-3.664" y1="-10.384" x2="-3.056" y2="-10.352" layer="21"/>
<rectangle x1="3.056" y1="-10.384" x2="3.664" y2="-10.352" layer="21"/>
<rectangle x1="-3.728" y1="-10.352" x2="-2.96" y2="-10.32" layer="21"/>
<rectangle x1="2.96" y1="-10.352" x2="3.728" y2="-10.32" layer="21"/>
<rectangle x1="-3.76" y1="-10.32" x2="-2.896" y2="-10.288" layer="21"/>
<rectangle x1="2.896" y1="-10.32" x2="3.76" y2="-10.288" layer="21"/>
<rectangle x1="-3.824" y1="-10.288" x2="-2.8" y2="-10.256" layer="21"/>
<rectangle x1="2.8" y1="-10.288" x2="3.824" y2="-10.256" layer="21"/>
<rectangle x1="-3.888" y1="-10.256" x2="-2.704" y2="-10.224" layer="21"/>
<rectangle x1="2.704" y1="-10.256" x2="3.888" y2="-10.224" layer="21"/>
<rectangle x1="-3.952" y1="-10.224" x2="-2.64" y2="-10.192" layer="21"/>
<rectangle x1="2.64" y1="-10.224" x2="3.952" y2="-10.192" layer="21"/>
<rectangle x1="-4.016" y1="-10.192" x2="-2.544" y2="-10.16" layer="21"/>
<rectangle x1="2.544" y1="-10.192" x2="4.016" y2="-10.16" layer="21"/>
<rectangle x1="-4.08" y1="-10.16" x2="-2.448" y2="-10.128" layer="21"/>
<rectangle x1="2.448" y1="-10.16" x2="4.08" y2="-10.128" layer="21"/>
<rectangle x1="-4.144" y1="-10.128" x2="-2.352" y2="-10.096" layer="21"/>
<rectangle x1="2.352" y1="-10.128" x2="4.144" y2="-10.096" layer="21"/>
<rectangle x1="-4.208" y1="-10.096" x2="-2.288" y2="-10.064" layer="21"/>
<rectangle x1="2.288" y1="-10.096" x2="4.208" y2="-10.064" layer="21"/>
<rectangle x1="-4.272" y1="-10.064" x2="-2.192" y2="-10.032" layer="21"/>
<rectangle x1="2.192" y1="-10.064" x2="4.272" y2="-10.032" layer="21"/>
<rectangle x1="-4.336" y1="-10.032" x2="-2.096" y2="-10" layer="21"/>
<rectangle x1="2.096" y1="-10.032" x2="4.336" y2="-10" layer="21"/>
<rectangle x1="-4.4" y1="-10" x2="-2" y2="-9.968" layer="21"/>
<rectangle x1="2" y1="-10" x2="4.4" y2="-9.968" layer="21"/>
<rectangle x1="-4.432" y1="-9.968" x2="-1.904" y2="-9.936" layer="21"/>
<rectangle x1="1.904" y1="-9.968" x2="4.432" y2="-9.936" layer="21"/>
<rectangle x1="-4.496" y1="-9.936" x2="-1.84" y2="-9.904" layer="21"/>
<rectangle x1="1.84" y1="-9.936" x2="4.496" y2="-9.904" layer="21"/>
<rectangle x1="-4.56" y1="-9.904" x2="-1.744" y2="-9.872" layer="21"/>
<rectangle x1="1.744" y1="-9.904" x2="4.56" y2="-9.872" layer="21"/>
<rectangle x1="-4.624" y1="-9.872" x2="-1.648" y2="-9.84" layer="21"/>
<rectangle x1="1.648" y1="-9.872" x2="4.624" y2="-9.84" layer="21"/>
<rectangle x1="-4.688" y1="-9.84" x2="-1.52" y2="-9.808" layer="21"/>
<rectangle x1="1.52" y1="-9.84" x2="4.688" y2="-9.808" layer="21"/>
<rectangle x1="-4.752" y1="-9.808" x2="-1.424" y2="-9.776" layer="21"/>
<rectangle x1="1.424" y1="-9.808" x2="4.752" y2="-9.776" layer="21"/>
<rectangle x1="-4.816" y1="-9.776" x2="-1.328" y2="-9.744" layer="21"/>
<rectangle x1="1.328" y1="-9.776" x2="4.816" y2="-9.744" layer="21"/>
<rectangle x1="-4.88" y1="-9.744" x2="-1.2" y2="-9.712" layer="21"/>
<rectangle x1="1.2" y1="-9.744" x2="4.88" y2="-9.712" layer="21"/>
<rectangle x1="-4.944" y1="-9.712" x2="-1.072" y2="-9.68" layer="21"/>
<rectangle x1="1.072" y1="-9.712" x2="4.944" y2="-9.68" layer="21"/>
<rectangle x1="-5.008" y1="-9.68" x2="-0.912" y2="-9.648" layer="21"/>
<rectangle x1="0.912" y1="-9.68" x2="5.008" y2="-9.648" layer="21"/>
<rectangle x1="-5.072" y1="-9.648" x2="-0.784" y2="-9.616" layer="21"/>
<rectangle x1="0.784" y1="-9.648" x2="5.072" y2="-9.616" layer="21"/>
<rectangle x1="-5.104" y1="-9.616" x2="-0.592" y2="-9.584" layer="21"/>
<rectangle x1="0.592" y1="-9.616" x2="5.136" y2="-9.584" layer="21"/>
<rectangle x1="-5.168" y1="-9.584" x2="-0.304" y2="-9.552" layer="21"/>
<rectangle x1="0.304" y1="-9.584" x2="5.168" y2="-9.552" layer="21"/>
<rectangle x1="-5.232" y1="-9.552" x2="5.232" y2="-9.52" layer="21"/>
<rectangle x1="-5.296" y1="-9.52" x2="5.296" y2="-9.488" layer="21"/>
<rectangle x1="-5.36" y1="-9.488" x2="5.36" y2="-9.456" layer="21"/>
<rectangle x1="-5.424" y1="-9.456" x2="5.424" y2="-9.424" layer="21"/>
<rectangle x1="-5.488" y1="-9.424" x2="5.488" y2="-9.392" layer="21"/>
<rectangle x1="-5.552" y1="-9.392" x2="5.552" y2="-9.36" layer="21"/>
<rectangle x1="-5.616" y1="-9.36" x2="5.616" y2="-9.328" layer="21"/>
<rectangle x1="-5.648" y1="-9.328" x2="5.68" y2="-9.296" layer="21"/>
<rectangle x1="-5.712" y1="-9.296" x2="5.712" y2="-9.264" layer="21"/>
<rectangle x1="-5.776" y1="-9.264" x2="5.776" y2="-9.232" layer="21"/>
<rectangle x1="-5.84" y1="-9.232" x2="5.84" y2="-9.2" layer="21"/>
<rectangle x1="-5.872" y1="-9.2" x2="5.872" y2="-9.168" layer="21"/>
<rectangle x1="-5.84" y1="-9.168" x2="5.84" y2="-9.136" layer="21"/>
<rectangle x1="-5.776" y1="-9.136" x2="5.776" y2="-9.104" layer="21"/>
<rectangle x1="-5.712" y1="-9.104" x2="5.712" y2="-9.072" layer="21"/>
<rectangle x1="-5.648" y1="-9.072" x2="5.648" y2="-9.04" layer="21"/>
<rectangle x1="-5.584" y1="-9.04" x2="5.584" y2="-9.008" layer="21"/>
<rectangle x1="-5.52" y1="-9.008" x2="5.52" y2="-8.976" layer="21"/>
<rectangle x1="-5.456" y1="-8.976" x2="5.456" y2="-8.944" layer="21"/>
<rectangle x1="-5.392" y1="-8.944" x2="5.392" y2="-8.912" layer="21"/>
<rectangle x1="-5.328" y1="-8.912" x2="5.328" y2="-8.88" layer="21"/>
<rectangle x1="-5.264" y1="-8.88" x2="5.264" y2="-8.848" layer="21"/>
<rectangle x1="-5.2" y1="-8.848" x2="5.2" y2="-8.816" layer="21"/>
<rectangle x1="-5.136" y1="-8.816" x2="-0.464" y2="-8.784" layer="21"/>
<rectangle x1="0.464" y1="-8.816" x2="5.168" y2="-8.784" layer="21"/>
<rectangle x1="-5.104" y1="-8.784" x2="-0.688" y2="-8.752" layer="21"/>
<rectangle x1="0.688" y1="-8.784" x2="5.104" y2="-8.752" layer="21"/>
<rectangle x1="-5.04" y1="-8.752" x2="-0.848" y2="-8.72" layer="21"/>
<rectangle x1="0.848" y1="-8.752" x2="5.04" y2="-8.72" layer="21"/>
<rectangle x1="-4.976" y1="-8.72" x2="-0.976" y2="-8.688" layer="21"/>
<rectangle x1="1.008" y1="-8.72" x2="4.976" y2="-8.688" layer="21"/>
<rectangle x1="-4.912" y1="-8.688" x2="-1.104" y2="-8.656" layer="21"/>
<rectangle x1="1.136" y1="-8.688" x2="4.912" y2="-8.656" layer="21"/>
<rectangle x1="-4.848" y1="-8.656" x2="-1.232" y2="-8.624" layer="21"/>
<rectangle x1="1.232" y1="-8.656" x2="4.848" y2="-8.624" layer="21"/>
<rectangle x1="-4.784" y1="-8.624" x2="-1.36" y2="-8.592" layer="21"/>
<rectangle x1="1.36" y1="-8.624" x2="4.784" y2="-8.592" layer="21"/>
<rectangle x1="-4.72" y1="-8.592" x2="-1.456" y2="-8.56" layer="21"/>
<rectangle x1="1.456" y1="-8.592" x2="4.72" y2="-8.56" layer="21"/>
<rectangle x1="-4.656" y1="-8.56" x2="-1.584" y2="-8.528" layer="21"/>
<rectangle x1="1.584" y1="-8.56" x2="4.656" y2="-8.528" layer="21"/>
<rectangle x1="-4.592" y1="-8.528" x2="-1.68" y2="-8.496" layer="21"/>
<rectangle x1="1.68" y1="-8.528" x2="4.592" y2="-8.496" layer="21"/>
<rectangle x1="-4.528" y1="-8.496" x2="-1.776" y2="-8.464" layer="21"/>
<rectangle x1="1.776" y1="-8.496" x2="4.528" y2="-8.464" layer="21"/>
<rectangle x1="-4.464" y1="-8.464" x2="-1.872" y2="-8.432" layer="21"/>
<rectangle x1="1.872" y1="-8.464" x2="4.464" y2="-8.432" layer="21"/>
<rectangle x1="-4.432" y1="-8.432" x2="-1.968" y2="-8.4" layer="21"/>
<rectangle x1="1.968" y1="-8.432" x2="4.432" y2="-8.4" layer="21"/>
<rectangle x1="-4.368" y1="-8.4" x2="-2.064" y2="-8.368" layer="21"/>
<rectangle x1="2.064" y1="-8.4" x2="4.368" y2="-8.368" layer="21"/>
<rectangle x1="-4.304" y1="-8.368" x2="-2.128" y2="-8.336" layer="21"/>
<rectangle x1="2.16" y1="-8.368" x2="4.304" y2="-8.336" layer="21"/>
<rectangle x1="-4.24" y1="-8.336" x2="-2.224" y2="-8.304" layer="21"/>
<rectangle x1="2.224" y1="-8.336" x2="4.24" y2="-8.304" layer="21"/>
<rectangle x1="-4.176" y1="-8.304" x2="-2.32" y2="-8.272" layer="21"/>
<rectangle x1="2.32" y1="-8.304" x2="4.176" y2="-8.272" layer="21"/>
<rectangle x1="-4.112" y1="-8.272" x2="-2.416" y2="-8.24" layer="21"/>
<rectangle x1="2.416" y1="-8.272" x2="4.112" y2="-8.24" layer="21"/>
<rectangle x1="-4.048" y1="-8.24" x2="-2.48" y2="-8.208" layer="21"/>
<rectangle x1="2.48" y1="-8.24" x2="4.048" y2="-8.208" layer="21"/>
<rectangle x1="-3.984" y1="-8.208" x2="-2.576" y2="-8.176" layer="21"/>
<rectangle x1="2.576" y1="-8.208" x2="3.984" y2="-8.176" layer="21"/>
<rectangle x1="-3.92" y1="-8.176" x2="-2.672" y2="-8.144" layer="21"/>
<rectangle x1="2.672" y1="-8.176" x2="3.952" y2="-8.144" layer="21"/>
<rectangle x1="-3.888" y1="-8.144" x2="-2.736" y2="-8.112" layer="21"/>
<rectangle x1="2.736" y1="-8.144" x2="3.888" y2="-8.112" layer="21"/>
<rectangle x1="-3.824" y1="-8.112" x2="-2.832" y2="-8.08" layer="21"/>
<rectangle x1="2.832" y1="-8.112" x2="3.824" y2="-8.08" layer="21"/>
<rectangle x1="-3.76" y1="-8.08" x2="-2.928" y2="-8.048" layer="21"/>
<rectangle x1="2.928" y1="-8.08" x2="3.76" y2="-8.048" layer="21"/>
<rectangle x1="-3.696" y1="-8.048" x2="-2.992" y2="-8.016" layer="21"/>
<rectangle x1="2.992" y1="-8.048" x2="3.696" y2="-8.016" layer="21"/>
<rectangle x1="-3.632" y1="-8.016" x2="-3.088" y2="-7.984" layer="21"/>
<rectangle x1="3.088" y1="-8.016" x2="3.632" y2="-7.984" layer="21"/>
<rectangle x1="-3.568" y1="-7.984" x2="-3.184" y2="-7.952" layer="21"/>
<rectangle x1="3.184" y1="-7.984" x2="3.568" y2="-7.952" layer="21"/>
<rectangle x1="-3.504" y1="-7.952" x2="-3.248" y2="-7.92" layer="21"/>
<rectangle x1="3.248" y1="-7.952" x2="3.504" y2="-7.92" layer="21"/>
<rectangle x1="-3.44" y1="-7.92" x2="-3.344" y2="-7.888" layer="21"/>
<rectangle x1="3.344" y1="-7.92" x2="3.44" y2="-7.888" layer="21"/>
<rectangle x1="-3.408" y1="-7.248" x2="-3.376" y2="-7.216" layer="21"/>
<rectangle x1="3.376" y1="-7.248" x2="3.408" y2="-7.216" layer="21"/>
<rectangle x1="-3.472" y1="-7.216" x2="-3.312" y2="-7.184" layer="21"/>
<rectangle x1="3.312" y1="-7.216" x2="3.472" y2="-7.184" layer="21"/>
<rectangle x1="-3.536" y1="-7.184" x2="-3.216" y2="-7.152" layer="21"/>
<rectangle x1="3.216" y1="-7.184" x2="3.536" y2="-7.152" layer="21"/>
<rectangle x1="-3.6" y1="-7.152" x2="-3.12" y2="-7.12" layer="21"/>
<rectangle x1="3.152" y1="-7.152" x2="3.6" y2="-7.12" layer="21"/>
<rectangle x1="-3.664" y1="-7.12" x2="-3.056" y2="-7.088" layer="21"/>
<rectangle x1="3.056" y1="-7.12" x2="3.664" y2="-7.088" layer="21"/>
<rectangle x1="-3.696" y1="-7.088" x2="-2.96" y2="-7.056" layer="21"/>
<rectangle x1="2.96" y1="-7.088" x2="3.696" y2="-7.056" layer="21"/>
<rectangle x1="-3.76" y1="-7.056" x2="-2.896" y2="-7.024" layer="21"/>
<rectangle x1="2.896" y1="-7.056" x2="3.76" y2="-7.024" layer="21"/>
<rectangle x1="-3.824" y1="-7.024" x2="-2.8" y2="-6.992" layer="21"/>
<rectangle x1="2.8" y1="-7.024" x2="3.824" y2="-6.992" layer="21"/>
<rectangle x1="-3.888" y1="-6.992" x2="-2.704" y2="-6.96" layer="21"/>
<rectangle x1="2.704" y1="-6.992" x2="3.888" y2="-6.96" layer="21"/>
<rectangle x1="-3.952" y1="-6.96" x2="-2.64" y2="-6.928" layer="21"/>
<rectangle x1="2.64" y1="-6.96" x2="3.952" y2="-6.928" layer="21"/>
<rectangle x1="-4.016" y1="-6.928" x2="-2.544" y2="-6.896" layer="21"/>
<rectangle x1="2.544" y1="-6.928" x2="4.016" y2="-6.896" layer="21"/>
<rectangle x1="-4.08" y1="-6.896" x2="-2.448" y2="-6.864" layer="21"/>
<rectangle x1="2.448" y1="-6.896" x2="4.08" y2="-6.864" layer="21"/>
<rectangle x1="-4.144" y1="-6.864" x2="-2.352" y2="-6.832" layer="21"/>
<rectangle x1="2.352" y1="-6.864" x2="4.144" y2="-6.832" layer="21"/>
<rectangle x1="-4.208" y1="-6.832" x2="-2.288" y2="-6.8" layer="21"/>
<rectangle x1="2.288" y1="-6.832" x2="4.208" y2="-6.8" layer="21"/>
<rectangle x1="-4.272" y1="-6.8" x2="-2.192" y2="-6.768" layer="21"/>
<rectangle x1="2.192" y1="-6.8" x2="4.272" y2="-6.768" layer="21"/>
<rectangle x1="-4.336" y1="-6.768" x2="-2.096" y2="-6.736" layer="21"/>
<rectangle x1="2.096" y1="-6.768" x2="4.336" y2="-6.736" layer="21"/>
<rectangle x1="-4.368" y1="-6.736" x2="-2.032" y2="-6.704" layer="21"/>
<rectangle x1="2.032" y1="-6.736" x2="4.368" y2="-6.704" layer="21"/>
<rectangle x1="-4.432" y1="-6.704" x2="-1.936" y2="-6.672" layer="21"/>
<rectangle x1="1.936" y1="-6.704" x2="4.432" y2="-6.672" layer="21"/>
<rectangle x1="-4.496" y1="-6.672" x2="-1.84" y2="-6.64" layer="21"/>
<rectangle x1="1.84" y1="-6.672" x2="4.496" y2="-6.64" layer="21"/>
<rectangle x1="-4.56" y1="-6.64" x2="-1.744" y2="-6.608" layer="21"/>
<rectangle x1="1.744" y1="-6.64" x2="4.56" y2="-6.608" layer="21"/>
<rectangle x1="-4.624" y1="-6.608" x2="-1.648" y2="-6.576" layer="21"/>
<rectangle x1="1.648" y1="-6.608" x2="4.624" y2="-6.576" layer="21"/>
<rectangle x1="-4.688" y1="-6.576" x2="-1.52" y2="-6.544" layer="21"/>
<rectangle x1="1.552" y1="-6.576" x2="4.688" y2="-6.544" layer="21"/>
<rectangle x1="-4.752" y1="-6.544" x2="-1.424" y2="-6.512" layer="21"/>
<rectangle x1="1.424" y1="-6.544" x2="4.752" y2="-6.512" layer="21"/>
<rectangle x1="-4.816" y1="-6.512" x2="-1.328" y2="-6.48" layer="21"/>
<rectangle x1="1.328" y1="-6.512" x2="4.816" y2="-6.48" layer="21"/>
<rectangle x1="-4.88" y1="-6.48" x2="-1.2" y2="-6.448" layer="21"/>
<rectangle x1="1.2" y1="-6.48" x2="4.88" y2="-6.448" layer="21"/>
<rectangle x1="-4.944" y1="-6.448" x2="-1.072" y2="-6.416" layer="21"/>
<rectangle x1="1.072" y1="-6.448" x2="4.944" y2="-6.416" layer="21"/>
<rectangle x1="-5.008" y1="-6.416" x2="-0.944" y2="-6.384" layer="21"/>
<rectangle x1="0.944" y1="-6.416" x2="5.008" y2="-6.384" layer="21"/>
<rectangle x1="-5.04" y1="-6.384" x2="-0.784" y2="-6.352" layer="21"/>
<rectangle x1="0.784" y1="-6.384" x2="5.072" y2="-6.352" layer="21"/>
<rectangle x1="-5.104" y1="-6.352" x2="-0.592" y2="-6.32" layer="21"/>
<rectangle x1="0.592" y1="-6.352" x2="5.104" y2="-6.32" layer="21"/>
<rectangle x1="-5.168" y1="-6.32" x2="-0.336" y2="-6.288" layer="21"/>
<rectangle x1="0.336" y1="-6.32" x2="5.168" y2="-6.288" layer="21"/>
<rectangle x1="-5.232" y1="-6.288" x2="5.232" y2="-6.256" layer="21"/>
<rectangle x1="-5.296" y1="-6.256" x2="5.296" y2="-6.224" layer="21"/>
<rectangle x1="-5.36" y1="-6.224" x2="5.36" y2="-6.192" layer="21"/>
<rectangle x1="-5.424" y1="-6.192" x2="5.424" y2="-6.16" layer="21"/>
<rectangle x1="-5.488" y1="-6.16" x2="5.488" y2="-6.128" layer="21"/>
<rectangle x1="-5.552" y1="-6.128" x2="5.552" y2="-6.096" layer="21"/>
<rectangle x1="-5.616" y1="-6.096" x2="5.616" y2="-6.064" layer="21"/>
<rectangle x1="-5.68" y1="-6.064" x2="5.68" y2="-6.032" layer="21"/>
<rectangle x1="-5.712" y1="-6.032" x2="5.712" y2="-6" layer="21"/>
<rectangle x1="-5.776" y1="-6" x2="5.776" y2="-5.968" layer="21"/>
<rectangle x1="-5.84" y1="-5.968" x2="5.84" y2="-5.936" layer="21"/>
<rectangle x1="-5.872" y1="-5.936" x2="5.872" y2="-5.904" layer="21"/>
<rectangle x1="-5.84" y1="-5.904" x2="5.84" y2="-5.872" layer="21"/>
<rectangle x1="-5.776" y1="-5.872" x2="5.776" y2="-5.84" layer="21"/>
<rectangle x1="-5.712" y1="-5.84" x2="5.712" y2="-5.808" layer="21"/>
<rectangle x1="-5.648" y1="-5.808" x2="5.648" y2="-5.776" layer="21"/>
<rectangle x1="-5.584" y1="-5.776" x2="5.584" y2="-5.744" layer="21"/>
<rectangle x1="-5.52" y1="-5.744" x2="5.52" y2="-5.712" layer="21"/>
<rectangle x1="-5.456" y1="-5.712" x2="5.456" y2="-5.68" layer="21"/>
<rectangle x1="-5.392" y1="-5.68" x2="5.392" y2="-5.648" layer="21"/>
<rectangle x1="-5.328" y1="-5.648" x2="5.328" y2="-5.616" layer="21"/>
<rectangle x1="-5.264" y1="-5.616" x2="5.264" y2="-5.584" layer="21"/>
<rectangle x1="-5.2" y1="-5.584" x2="5.232" y2="-5.552" layer="21"/>
<rectangle x1="-5.168" y1="-5.552" x2="-0.464" y2="-5.52" layer="21"/>
<rectangle x1="0.464" y1="-5.552" x2="5.168" y2="-5.52" layer="21"/>
<rectangle x1="-5.104" y1="-5.52" x2="-0.656" y2="-5.488" layer="21"/>
<rectangle x1="0.656" y1="-5.52" x2="5.104" y2="-5.488" layer="21"/>
<rectangle x1="-5.04" y1="-5.488" x2="-0.848" y2="-5.456" layer="21"/>
<rectangle x1="0.848" y1="-5.488" x2="5.04" y2="-5.456" layer="21"/>
<rectangle x1="-4.976" y1="-5.456" x2="-0.976" y2="-5.424" layer="21"/>
<rectangle x1="0.976" y1="-5.456" x2="4.976" y2="-5.424" layer="21"/>
<rectangle x1="-4.912" y1="-5.424" x2="-1.104" y2="-5.392" layer="21"/>
<rectangle x1="1.104" y1="-5.424" x2="4.912" y2="-5.392" layer="21"/>
<rectangle x1="-4.848" y1="-5.392" x2="-1.232" y2="-5.36" layer="21"/>
<rectangle x1="1.232" y1="-5.392" x2="4.848" y2="-5.36" layer="21"/>
<rectangle x1="-4.784" y1="-5.36" x2="-1.36" y2="-5.328" layer="21"/>
<rectangle x1="1.36" y1="-5.36" x2="4.784" y2="-5.328" layer="21"/>
<rectangle x1="-4.72" y1="-5.328" x2="-1.456" y2="-5.296" layer="21"/>
<rectangle x1="1.456" y1="-5.328" x2="4.72" y2="-5.296" layer="21"/>
<rectangle x1="-4.656" y1="-5.296" x2="-1.584" y2="-5.264" layer="21"/>
<rectangle x1="1.584" y1="-5.296" x2="4.656" y2="-5.264" layer="21"/>
<rectangle x1="-4.592" y1="-5.264" x2="-1.68" y2="-5.232" layer="21"/>
<rectangle x1="1.68" y1="-5.264" x2="4.592" y2="-5.232" layer="21"/>
<rectangle x1="-4.528" y1="-5.232" x2="-1.776" y2="-5.2" layer="21"/>
<rectangle x1="1.776" y1="-5.232" x2="4.528" y2="-5.2" layer="21"/>
<rectangle x1="-4.496" y1="-5.2" x2="-1.872" y2="-5.168" layer="21"/>
<rectangle x1="1.872" y1="-5.2" x2="4.496" y2="-5.168" layer="21"/>
<rectangle x1="-4.432" y1="-5.168" x2="-1.968" y2="-5.136" layer="21"/>
<rectangle x1="1.968" y1="-5.168" x2="4.432" y2="-5.136" layer="21"/>
<rectangle x1="-4.368" y1="-5.136" x2="-2.032" y2="-5.104" layer="21"/>
<rectangle x1="2.064" y1="-5.136" x2="4.368" y2="-5.104" layer="21"/>
<rectangle x1="-4.304" y1="-5.104" x2="-2.128" y2="-5.072" layer="21"/>
<rectangle x1="2.128" y1="-5.104" x2="4.304" y2="-5.072" layer="21"/>
<rectangle x1="-4.24" y1="-5.072" x2="-2.224" y2="-5.04" layer="21"/>
<rectangle x1="2.224" y1="-5.072" x2="4.24" y2="-5.04" layer="21"/>
<rectangle x1="-4.176" y1="-5.04" x2="-2.32" y2="-5.008" layer="21"/>
<rectangle x1="2.32" y1="-5.04" x2="4.176" y2="-5.008" layer="21"/>
<rectangle x1="-4.112" y1="-5.008" x2="-2.384" y2="-4.976" layer="21"/>
<rectangle x1="2.384" y1="-5.008" x2="4.112" y2="-4.976" layer="21"/>
<rectangle x1="-4.048" y1="-4.976" x2="-2.48" y2="-4.944" layer="21"/>
<rectangle x1="2.48" y1="-4.976" x2="4.048" y2="-4.944" layer="21"/>
<rectangle x1="-3.984" y1="-4.944" x2="-2.576" y2="-4.912" layer="21"/>
<rectangle x1="2.576" y1="-4.944" x2="3.984" y2="-4.912" layer="21"/>
<rectangle x1="-3.952" y1="-4.912" x2="-2.64" y2="-4.88" layer="21"/>
<rectangle x1="2.672" y1="-4.912" x2="3.952" y2="-4.88" layer="21"/>
<rectangle x1="-3.888" y1="-4.88" x2="-2.736" y2="-4.848" layer="21"/>
<rectangle x1="2.736" y1="-4.88" x2="3.888" y2="-4.848" layer="21"/>
<rectangle x1="-3.824" y1="-4.848" x2="-2.832" y2="-4.816" layer="21"/>
<rectangle x1="2.832" y1="-4.848" x2="3.824" y2="-4.816" layer="21"/>
<rectangle x1="-3.76" y1="-4.816" x2="-2.928" y2="-4.784" layer="21"/>
<rectangle x1="2.928" y1="-4.816" x2="3.76" y2="-4.784" layer="21"/>
<rectangle x1="-3.696" y1="-4.784" x2="-2.992" y2="-4.752" layer="21"/>
<rectangle x1="2.992" y1="-4.784" x2="3.696" y2="-4.752" layer="21"/>
<rectangle x1="-3.632" y1="-4.752" x2="-3.088" y2="-4.72" layer="21"/>
<rectangle x1="3.088" y1="-4.752" x2="3.632" y2="-4.72" layer="21"/>
<rectangle x1="-3.568" y1="-4.72" x2="-3.152" y2="-4.688" layer="21"/>
<rectangle x1="3.152" y1="-4.72" x2="3.568" y2="-4.688" layer="21"/>
<rectangle x1="-3.504" y1="-4.688" x2="-3.248" y2="-4.656" layer="21"/>
<rectangle x1="3.248" y1="-4.688" x2="3.504" y2="-4.656" layer="21"/>
<rectangle x1="-3.44" y1="-4.656" x2="-3.312" y2="-4.624" layer="21"/>
<rectangle x1="3.312" y1="-4.656" x2="3.44" y2="-4.624" layer="21"/>
<rectangle x1="3.376" y1="-3.984" x2="3.408" y2="-3.952" layer="21"/>
<rectangle x1="-3.472" y1="-3.952" x2="-3.312" y2="-3.92" layer="21"/>
<rectangle x1="3.312" y1="-3.952" x2="3.472" y2="-3.92" layer="21"/>
<rectangle x1="-3.536" y1="-3.92" x2="-3.216" y2="-3.888" layer="21"/>
<rectangle x1="3.216" y1="-3.92" x2="3.536" y2="-3.888" layer="21"/>
<rectangle x1="-3.6" y1="-3.888" x2="-3.152" y2="-3.856" layer="21"/>
<rectangle x1="3.152" y1="-3.888" x2="3.6" y2="-3.856" layer="21"/>
<rectangle x1="-3.632" y1="-3.856" x2="-3.056" y2="-3.824" layer="21"/>
<rectangle x1="3.056" y1="-3.856" x2="3.632" y2="-3.824" layer="21"/>
<rectangle x1="-3.696" y1="-3.824" x2="-2.96" y2="-3.792" layer="21"/>
<rectangle x1="2.96" y1="-3.824" x2="3.696" y2="-3.792" layer="21"/>
<rectangle x1="-3.76" y1="-3.792" x2="-2.896" y2="-3.76" layer="21"/>
<rectangle x1="2.896" y1="-3.792" x2="3.76" y2="-3.76" layer="21"/>
<rectangle x1="-3.824" y1="-3.76" x2="-2.8" y2="-3.728" layer="21"/>
<rectangle x1="2.8" y1="-3.76" x2="3.824" y2="-3.728" layer="21"/>
<rectangle x1="-3.888" y1="-3.728" x2="-2.704" y2="-3.696" layer="21"/>
<rectangle x1="2.704" y1="-3.728" x2="3.888" y2="-3.696" layer="21"/>
<rectangle x1="-3.952" y1="-3.696" x2="-2.64" y2="-3.664" layer="21"/>
<rectangle x1="2.64" y1="-3.696" x2="3.952" y2="-3.664" layer="21"/>
<rectangle x1="-4.016" y1="-3.664" x2="-2.544" y2="-3.632" layer="21"/>
<rectangle x1="2.544" y1="-3.664" x2="4.016" y2="-3.632" layer="21"/>
<rectangle x1="-4.08" y1="-3.632" x2="-2.448" y2="-3.6" layer="21"/>
<rectangle x1="2.448" y1="-3.632" x2="4.08" y2="-3.6" layer="21"/>
<rectangle x1="-4.144" y1="-3.6" x2="-2.384" y2="-3.568" layer="21"/>
<rectangle x1="2.384" y1="-3.6" x2="4.144" y2="-3.568" layer="21"/>
<rectangle x1="-4.208" y1="-3.568" x2="-2.288" y2="-3.536" layer="21"/>
<rectangle x1="2.288" y1="-3.568" x2="4.208" y2="-3.536" layer="21"/>
<rectangle x1="-4.272" y1="-3.536" x2="-2.192" y2="-3.504" layer="21"/>
<rectangle x1="2.192" y1="-3.536" x2="4.272" y2="-3.504" layer="21"/>
<rectangle x1="-4.304" y1="-3.504" x2="-2.128" y2="-3.472" layer="21"/>
<rectangle x1="2.128" y1="-3.504" x2="4.304" y2="-3.472" layer="21"/>
<rectangle x1="-4.368" y1="-3.472" x2="-2.032" y2="-3.44" layer="21"/>
<rectangle x1="2.032" y1="-3.472" x2="4.368" y2="-3.44" layer="21"/>
<rectangle x1="-4.432" y1="-3.44" x2="-1.936" y2="-3.408" layer="21"/>
<rectangle x1="1.936" y1="-3.44" x2="4.432" y2="-3.408" layer="21"/>
<rectangle x1="-4.496" y1="-3.408" x2="-1.84" y2="-3.376" layer="21"/>
<rectangle x1="1.84" y1="-3.408" x2="4.496" y2="-3.376" layer="21"/>
<rectangle x1="-4.56" y1="-3.376" x2="-1.744" y2="-3.344" layer="21"/>
<rectangle x1="1.744" y1="-3.376" x2="4.56" y2="-3.344" layer="21"/>
<rectangle x1="-4.624" y1="-3.344" x2="-1.648" y2="-3.312" layer="21"/>
<rectangle x1="1.648" y1="-3.344" x2="4.624" y2="-3.312" layer="21"/>
<rectangle x1="-4.688" y1="-3.312" x2="-1.52" y2="-3.28" layer="21"/>
<rectangle x1="1.552" y1="-3.312" x2="4.688" y2="-3.28" layer="21"/>
<rectangle x1="-4.752" y1="-3.28" x2="-1.424" y2="-3.248" layer="21"/>
<rectangle x1="1.424" y1="-3.28" x2="4.752" y2="-3.248" layer="21"/>
<rectangle x1="-4.816" y1="-3.248" x2="-1.328" y2="-3.216" layer="21"/>
<rectangle x1="1.328" y1="-3.248" x2="4.816" y2="-3.216" layer="21"/>
<rectangle x1="-4.88" y1="-3.216" x2="-1.2" y2="-3.184" layer="21"/>
<rectangle x1="1.2" y1="-3.216" x2="4.88" y2="-3.184" layer="21"/>
<rectangle x1="-4.944" y1="-3.184" x2="-1.072" y2="-3.152" layer="21"/>
<rectangle x1="1.072" y1="-3.184" x2="4.944" y2="-3.152" layer="21"/>
<rectangle x1="-4.976" y1="-3.152" x2="-0.944" y2="-3.12" layer="21"/>
<rectangle x1="0.944" y1="-3.152" x2="5.008" y2="-3.12" layer="21"/>
<rectangle x1="-5.04" y1="-3.12" x2="-0.784" y2="-3.088" layer="21"/>
<rectangle x1="0.784" y1="-3.12" x2="5.04" y2="-3.088" layer="21"/>
<rectangle x1="-5.104" y1="-3.088" x2="-0.624" y2="-3.056" layer="21"/>
<rectangle x1="0.624" y1="-3.088" x2="5.104" y2="-3.056" layer="21"/>
<rectangle x1="-5.168" y1="-3.056" x2="-0.368" y2="-3.024" layer="21"/>
<rectangle x1="0.368" y1="-3.056" x2="5.168" y2="-3.024" layer="21"/>
<rectangle x1="-5.232" y1="-3.024" x2="5.232" y2="-2.992" layer="21"/>
<rectangle x1="-5.296" y1="-2.992" x2="5.296" y2="-2.96" layer="21"/>
<rectangle x1="-5.36" y1="-2.96" x2="5.36" y2="-2.928" layer="21"/>
<rectangle x1="-5.424" y1="-2.928" x2="5.424" y2="-2.896" layer="21"/>
<rectangle x1="-5.488" y1="-2.896" x2="5.488" y2="-2.864" layer="21"/>
<rectangle x1="-5.552" y1="-2.864" x2="5.552" y2="-2.832" layer="21"/>
<rectangle x1="-5.616" y1="-2.832" x2="5.616" y2="-2.8" layer="21"/>
<rectangle x1="-5.648" y1="-2.8" x2="5.68" y2="-2.768" layer="21"/>
<rectangle x1="-5.712" y1="-2.768" x2="5.712" y2="-2.736" layer="21"/>
<rectangle x1="-5.776" y1="-2.736" x2="5.776" y2="-2.704" layer="21"/>
<rectangle x1="-5.84" y1="-2.704" x2="5.84" y2="-2.672" layer="21"/>
<rectangle x1="-5.872" y1="-2.672" x2="5.872" y2="-2.64" layer="21"/>
<rectangle x1="-5.84" y1="-2.64" x2="5.84" y2="-2.608" layer="21"/>
<rectangle x1="-5.776" y1="-2.608" x2="5.776" y2="-2.576" layer="21"/>
<rectangle x1="-5.712" y1="-2.576" x2="5.712" y2="-2.544" layer="21"/>
<rectangle x1="-5.648" y1="-2.544" x2="5.648" y2="-2.512" layer="21"/>
<rectangle x1="-5.584" y1="-2.512" x2="5.584" y2="-2.48" layer="21"/>
<rectangle x1="-5.52" y1="-2.48" x2="5.52" y2="-2.448" layer="21"/>
<rectangle x1="-5.456" y1="-2.448" x2="5.456" y2="-2.416" layer="21"/>
<rectangle x1="-5.392" y1="-2.416" x2="5.392" y2="-2.384" layer="21"/>
<rectangle x1="-5.328" y1="-2.384" x2="5.328" y2="-2.352" layer="21"/>
<rectangle x1="-5.264" y1="-2.352" x2="5.296" y2="-2.32" layer="21"/>
<rectangle x1="-5.232" y1="-2.32" x2="5.232" y2="-2.288" layer="21"/>
<rectangle x1="-5.168" y1="-2.288" x2="-0.432" y2="-2.256" layer="21"/>
<rectangle x1="0.432" y1="-2.288" x2="5.168" y2="-2.256" layer="21"/>
<rectangle x1="-5.104" y1="-2.256" x2="-0.656" y2="-2.224" layer="21"/>
<rectangle x1="0.656" y1="-2.256" x2="5.104" y2="-2.224" layer="21"/>
<rectangle x1="-5.04" y1="-2.224" x2="-0.816" y2="-2.192" layer="21"/>
<rectangle x1="0.816" y1="-2.224" x2="5.04" y2="-2.192" layer="21"/>
<rectangle x1="-4.976" y1="-2.192" x2="-0.976" y2="-2.16" layer="21"/>
<rectangle x1="0.976" y1="-2.192" x2="4.976" y2="-2.16" layer="21"/>
<rectangle x1="-4.912" y1="-2.16" x2="-1.104" y2="-2.128" layer="21"/>
<rectangle x1="1.104" y1="-2.16" x2="4.912" y2="-2.128" layer="21"/>
<rectangle x1="-4.848" y1="-2.128" x2="-1.232" y2="-2.096" layer="21"/>
<rectangle x1="1.232" y1="-2.128" x2="4.848" y2="-2.096" layer="21"/>
<rectangle x1="-4.784" y1="-2.096" x2="-1.36" y2="-2.064" layer="21"/>
<rectangle x1="1.36" y1="-2.096" x2="4.784" y2="-2.064" layer="21"/>
<rectangle x1="-4.72" y1="-2.064" x2="-1.456" y2="-2.032" layer="21"/>
<rectangle x1="1.456" y1="-2.064" x2="4.72" y2="-2.032" layer="21"/>
<rectangle x1="-4.656" y1="-2.032" x2="-1.552" y2="-2" layer="21"/>
<rectangle x1="1.584" y1="-2.032" x2="4.656" y2="-2" layer="21"/>
<rectangle x1="-4.592" y1="-2" x2="-1.68" y2="-1.968" layer="21"/>
<rectangle x1="1.68" y1="-2" x2="4.592" y2="-1.968" layer="21"/>
<rectangle x1="-4.56" y1="-1.968" x2="-1.776" y2="-1.936" layer="21"/>
<rectangle x1="1.776" y1="-1.968" x2="4.56" y2="-1.936" layer="21"/>
<rectangle x1="-4.496" y1="-1.936" x2="-1.872" y2="-1.904" layer="21"/>
<rectangle x1="1.872" y1="-1.936" x2="4.496" y2="-1.904" layer="21"/>
<rectangle x1="-4.432" y1="-1.904" x2="-1.936" y2="-1.872" layer="21"/>
<rectangle x1="1.936" y1="-1.904" x2="4.432" y2="-1.872" layer="21"/>
<rectangle x1="-4.368" y1="-1.872" x2="-2.032" y2="-1.84" layer="21"/>
<rectangle x1="2.032" y1="-1.872" x2="4.368" y2="-1.84" layer="21"/>
<rectangle x1="-4.304" y1="-1.84" x2="-2.128" y2="-1.808" layer="21"/>
<rectangle x1="2.128" y1="-1.84" x2="4.304" y2="-1.808" layer="21"/>
<rectangle x1="-4.24" y1="-1.808" x2="-2.224" y2="-1.776" layer="21"/>
<rectangle x1="2.224" y1="-1.808" x2="4.24" y2="-1.776" layer="21"/>
<rectangle x1="-4.176" y1="-1.776" x2="-2.32" y2="-1.744" layer="21"/>
<rectangle x1="2.32" y1="-1.776" x2="4.176" y2="-1.744" layer="21"/>
<rectangle x1="-4.112" y1="-1.744" x2="-2.384" y2="-1.712" layer="21"/>
<rectangle x1="2.384" y1="-1.744" x2="4.112" y2="-1.712" layer="21"/>
<rectangle x1="-4.048" y1="-1.712" x2="-2.48" y2="-1.68" layer="21"/>
<rectangle x1="2.48" y1="-1.712" x2="4.048" y2="-1.68" layer="21"/>
<rectangle x1="-3.984" y1="-1.68" x2="-2.576" y2="-1.648" layer="21"/>
<rectangle x1="2.576" y1="-1.68" x2="4.016" y2="-1.648" layer="21"/>
<rectangle x1="-3.952" y1="-1.648" x2="-2.64" y2="-1.616" layer="21"/>
<rectangle x1="2.672" y1="-1.648" x2="3.952" y2="-1.616" layer="21"/>
<rectangle x1="-3.888" y1="-1.616" x2="-2.736" y2="-1.584" layer="21"/>
<rectangle x1="2.736" y1="-1.616" x2="3.888" y2="-1.584" layer="21"/>
<rectangle x1="-3.824" y1="-1.584" x2="-2.832" y2="-1.552" layer="21"/>
<rectangle x1="2.832" y1="-1.584" x2="3.824" y2="-1.552" layer="21"/>
<rectangle x1="-3.76" y1="-1.552" x2="-2.896" y2="-1.52" layer="21"/>
<rectangle x1="2.896" y1="-1.552" x2="3.76" y2="-1.52" layer="21"/>
<rectangle x1="-3.696" y1="-1.52" x2="-2.992" y2="-1.488" layer="21"/>
<rectangle x1="2.992" y1="-1.52" x2="3.696" y2="-1.488" layer="21"/>
<rectangle x1="-3.632" y1="-1.488" x2="-3.088" y2="-1.456" layer="21"/>
<rectangle x1="3.088" y1="-1.488" x2="3.632" y2="-1.456" layer="21"/>
<rectangle x1="-3.568" y1="-1.456" x2="-3.152" y2="-1.424" layer="21"/>
<rectangle x1="3.152" y1="-1.456" x2="3.568" y2="-1.424" layer="21"/>
<rectangle x1="-3.504" y1="-1.424" x2="-3.248" y2="-1.392" layer="21"/>
<rectangle x1="3.248" y1="-1.424" x2="3.504" y2="-1.392" layer="21"/>
<rectangle x1="-3.44" y1="-1.392" x2="-3.312" y2="-1.36" layer="21"/>
<rectangle x1="3.312" y1="-1.392" x2="3.44" y2="-1.36" layer="21"/>
<rectangle x1="3.344" y1="0.4" x2="3.92" y2="0.432" layer="21"/>
<rectangle x1="3.12" y1="0.432" x2="4.144" y2="0.464" layer="21"/>
<rectangle x1="2.992" y1="0.464" x2="4.272" y2="0.496" layer="21"/>
<rectangle x1="-5.232" y1="0.496" x2="-3.152" y2="0.528" layer="21"/>
<rectangle x1="-0.496" y1="0.496" x2="0.464" y2="0.528" layer="21"/>
<rectangle x1="2.864" y1="0.496" x2="4.368" y2="0.528" layer="21"/>
<rectangle x1="-5.264" y1="0.528" x2="-2.992" y2="0.56" layer="21"/>
<rectangle x1="-0.528" y1="0.528" x2="0.496" y2="0.56" layer="21"/>
<rectangle x1="2.8" y1="0.528" x2="4.464" y2="0.56" layer="21"/>
<rectangle x1="-5.264" y1="0.56" x2="-2.864" y2="0.592" layer="21"/>
<rectangle x1="-0.56" y1="0.56" x2="0.496" y2="0.592" layer="21"/>
<rectangle x1="2.736" y1="0.56" x2="4.528" y2="0.592" layer="21"/>
<rectangle x1="-5.264" y1="0.592" x2="-2.8" y2="0.624" layer="21"/>
<rectangle x1="-0.56" y1="0.592" x2="0.496" y2="0.624" layer="21"/>
<rectangle x1="2.672" y1="0.592" x2="4.592" y2="0.624" layer="21"/>
<rectangle x1="-5.264" y1="0.624" x2="-2.704" y2="0.656" layer="21"/>
<rectangle x1="-0.56" y1="0.624" x2="0.496" y2="0.656" layer="21"/>
<rectangle x1="2.608" y1="0.624" x2="4.656" y2="0.656" layer="21"/>
<rectangle x1="-5.264" y1="0.656" x2="-2.64" y2="0.688" layer="21"/>
<rectangle x1="-0.56" y1="0.656" x2="0.496" y2="0.688" layer="21"/>
<rectangle x1="2.576" y1="0.656" x2="4.688" y2="0.688" layer="21"/>
<rectangle x1="-5.264" y1="0.688" x2="-2.608" y2="0.72" layer="21"/>
<rectangle x1="-0.56" y1="0.688" x2="0.496" y2="0.72" layer="21"/>
<rectangle x1="2.512" y1="0.688" x2="4.72" y2="0.72" layer="21"/>
<rectangle x1="-5.264" y1="0.72" x2="-2.544" y2="0.752" layer="21"/>
<rectangle x1="-0.56" y1="0.72" x2="0.496" y2="0.752" layer="21"/>
<rectangle x1="2.48" y1="0.72" x2="4.784" y2="0.752" layer="21"/>
<rectangle x1="-5.264" y1="0.752" x2="-2.512" y2="0.784" layer="21"/>
<rectangle x1="-0.56" y1="0.752" x2="0.496" y2="0.784" layer="21"/>
<rectangle x1="2.448" y1="0.752" x2="4.816" y2="0.784" layer="21"/>
<rectangle x1="-5.264" y1="0.784" x2="-2.48" y2="0.816" layer="21"/>
<rectangle x1="-0.56" y1="0.784" x2="0.496" y2="0.816" layer="21"/>
<rectangle x1="2.416" y1="0.784" x2="4.848" y2="0.816" layer="21"/>
<rectangle x1="-5.264" y1="0.816" x2="-2.448" y2="0.848" layer="21"/>
<rectangle x1="-0.56" y1="0.816" x2="0.496" y2="0.848" layer="21"/>
<rectangle x1="2.384" y1="0.816" x2="4.88" y2="0.848" layer="21"/>
<rectangle x1="-5.264" y1="0.848" x2="-2.416" y2="0.88" layer="21"/>
<rectangle x1="-0.56" y1="0.848" x2="0.496" y2="0.88" layer="21"/>
<rectangle x1="2.352" y1="0.848" x2="4.912" y2="0.88" layer="21"/>
<rectangle x1="-5.264" y1="0.88" x2="-2.384" y2="0.912" layer="21"/>
<rectangle x1="-0.56" y1="0.88" x2="0.496" y2="0.912" layer="21"/>
<rectangle x1="2.32" y1="0.88" x2="4.912" y2="0.912" layer="21"/>
<rectangle x1="-5.264" y1="0.912" x2="-2.352" y2="0.944" layer="21"/>
<rectangle x1="-0.56" y1="0.912" x2="0.496" y2="0.944" layer="21"/>
<rectangle x1="2.288" y1="0.912" x2="4.944" y2="0.944" layer="21"/>
<rectangle x1="-5.264" y1="0.944" x2="-2.352" y2="0.976" layer="21"/>
<rectangle x1="-0.56" y1="0.944" x2="0.496" y2="0.976" layer="21"/>
<rectangle x1="2.256" y1="0.944" x2="4.976" y2="0.976" layer="21"/>
<rectangle x1="-5.264" y1="0.976" x2="-2.32" y2="1.008" layer="21"/>
<rectangle x1="-0.56" y1="0.976" x2="0.496" y2="1.008" layer="21"/>
<rectangle x1="2.256" y1="0.976" x2="5.008" y2="1.008" layer="21"/>
<rectangle x1="-5.264" y1="1.008" x2="-2.288" y2="1.04" layer="21"/>
<rectangle x1="-0.56" y1="1.008" x2="0.496" y2="1.04" layer="21"/>
<rectangle x1="2.224" y1="1.008" x2="5.008" y2="1.04" layer="21"/>
<rectangle x1="-5.264" y1="1.04" x2="-2.288" y2="1.072" layer="21"/>
<rectangle x1="-0.56" y1="1.04" x2="0.496" y2="1.072" layer="21"/>
<rectangle x1="2.224" y1="1.04" x2="5.04" y2="1.072" layer="21"/>
<rectangle x1="-5.264" y1="1.072" x2="-2.256" y2="1.104" layer="21"/>
<rectangle x1="-0.56" y1="1.072" x2="0.496" y2="1.104" layer="21"/>
<rectangle x1="2.192" y1="1.072" x2="5.04" y2="1.104" layer="21"/>
<rectangle x1="-5.264" y1="1.104" x2="-2.256" y2="1.136" layer="21"/>
<rectangle x1="-0.56" y1="1.104" x2="0.496" y2="1.136" layer="21"/>
<rectangle x1="2.192" y1="1.104" x2="3.44" y2="1.136" layer="21"/>
<rectangle x1="3.824" y1="1.104" x2="5.072" y2="1.136" layer="21"/>
<rectangle x1="-5.264" y1="1.136" x2="-2.224" y2="1.168" layer="21"/>
<rectangle x1="-0.56" y1="1.136" x2="0.496" y2="1.168" layer="21"/>
<rectangle x1="2.16" y1="1.136" x2="3.344" y2="1.168" layer="21"/>
<rectangle x1="3.92" y1="1.136" x2="5.072" y2="1.168" layer="21"/>
<rectangle x1="-5.264" y1="1.168" x2="-2.224" y2="1.2" layer="21"/>
<rectangle x1="-0.56" y1="1.168" x2="0.496" y2="1.2" layer="21"/>
<rectangle x1="2.16" y1="1.168" x2="3.28" y2="1.2" layer="21"/>
<rectangle x1="3.984" y1="1.168" x2="5.104" y2="1.2" layer="21"/>
<rectangle x1="-5.264" y1="1.2" x2="-4.304" y2="1.232" layer="21"/>
<rectangle x1="-3.504" y1="1.2" x2="-2.192" y2="1.232" layer="21"/>
<rectangle x1="-0.56" y1="1.2" x2="0.496" y2="1.232" layer="21"/>
<rectangle x1="2.128" y1="1.2" x2="3.248" y2="1.232" layer="21"/>
<rectangle x1="4.016" y1="1.2" x2="5.104" y2="1.232" layer="21"/>
<rectangle x1="-5.264" y1="1.232" x2="-4.304" y2="1.264" layer="21"/>
<rectangle x1="-3.408" y1="1.232" x2="-2.192" y2="1.264" layer="21"/>
<rectangle x1="-0.56" y1="1.232" x2="0.496" y2="1.264" layer="21"/>
<rectangle x1="2.128" y1="1.232" x2="3.216" y2="1.264" layer="21"/>
<rectangle x1="4.048" y1="1.232" x2="5.104" y2="1.264" layer="21"/>
<rectangle x1="-5.264" y1="1.264" x2="-4.304" y2="1.296" layer="21"/>
<rectangle x1="-3.344" y1="1.264" x2="-2.192" y2="1.296" layer="21"/>
<rectangle x1="-0.56" y1="1.264" x2="0.496" y2="1.296" layer="21"/>
<rectangle x1="2.128" y1="1.264" x2="3.184" y2="1.296" layer="21"/>
<rectangle x1="4.08" y1="1.264" x2="5.136" y2="1.296" layer="21"/>
<rectangle x1="-5.264" y1="1.296" x2="-4.304" y2="1.328" layer="21"/>
<rectangle x1="-3.28" y1="1.296" x2="-2.16" y2="1.328" layer="21"/>
<rectangle x1="-0.56" y1="1.296" x2="0.496" y2="1.328" layer="21"/>
<rectangle x1="2.096" y1="1.296" x2="3.152" y2="1.328" layer="21"/>
<rectangle x1="4.112" y1="1.296" x2="5.136" y2="1.328" layer="21"/>
<rectangle x1="-5.264" y1="1.328" x2="-4.304" y2="1.36" layer="21"/>
<rectangle x1="-3.248" y1="1.328" x2="-2.16" y2="1.36" layer="21"/>
<rectangle x1="-0.56" y1="1.328" x2="0.496" y2="1.36" layer="21"/>
<rectangle x1="2.096" y1="1.328" x2="3.12" y2="1.36" layer="21"/>
<rectangle x1="4.112" y1="1.328" x2="5.136" y2="1.36" layer="21"/>
<rectangle x1="-5.264" y1="1.36" x2="-4.304" y2="1.392" layer="21"/>
<rectangle x1="-3.216" y1="1.36" x2="-2.16" y2="1.392" layer="21"/>
<rectangle x1="-0.56" y1="1.36" x2="0.496" y2="1.392" layer="21"/>
<rectangle x1="2.096" y1="1.36" x2="3.12" y2="1.392" layer="21"/>
<rectangle x1="4.144" y1="1.36" x2="5.168" y2="1.392" layer="21"/>
<rectangle x1="-5.264" y1="1.392" x2="-4.304" y2="1.424" layer="21"/>
<rectangle x1="-3.216" y1="1.392" x2="-2.128" y2="1.424" layer="21"/>
<rectangle x1="-0.56" y1="1.392" x2="0.496" y2="1.424" layer="21"/>
<rectangle x1="2.096" y1="1.392" x2="3.088" y2="1.424" layer="21"/>
<rectangle x1="4.176" y1="1.392" x2="5.168" y2="1.424" layer="21"/>
<rectangle x1="-5.264" y1="1.424" x2="-4.304" y2="1.456" layer="21"/>
<rectangle x1="-3.184" y1="1.424" x2="-2.128" y2="1.456" layer="21"/>
<rectangle x1="-0.56" y1="1.424" x2="0.496" y2="1.456" layer="21"/>
<rectangle x1="2.064" y1="1.424" x2="3.088" y2="1.456" layer="21"/>
<rectangle x1="4.176" y1="1.424" x2="5.168" y2="1.456" layer="21"/>
<rectangle x1="-5.264" y1="1.456" x2="-4.304" y2="1.488" layer="21"/>
<rectangle x1="-3.152" y1="1.456" x2="-2.128" y2="1.488" layer="21"/>
<rectangle x1="-0.56" y1="1.456" x2="0.496" y2="1.488" layer="21"/>
<rectangle x1="2.064" y1="1.456" x2="3.088" y2="1.488" layer="21"/>
<rectangle x1="4.176" y1="1.456" x2="5.168" y2="1.488" layer="21"/>
<rectangle x1="-5.264" y1="1.488" x2="-4.304" y2="1.52" layer="21"/>
<rectangle x1="-3.152" y1="1.488" x2="-2.128" y2="1.52" layer="21"/>
<rectangle x1="-0.56" y1="1.488" x2="0.496" y2="1.52" layer="21"/>
<rectangle x1="2.064" y1="1.488" x2="3.056" y2="1.52" layer="21"/>
<rectangle x1="4.208" y1="1.488" x2="5.2" y2="1.52" layer="21"/>
<rectangle x1="-5.264" y1="1.52" x2="-4.304" y2="1.552" layer="21"/>
<rectangle x1="-3.12" y1="1.52" x2="-2.128" y2="1.552" layer="21"/>
<rectangle x1="-0.56" y1="1.52" x2="0.496" y2="1.552" layer="21"/>
<rectangle x1="2.064" y1="1.52" x2="3.056" y2="1.552" layer="21"/>
<rectangle x1="4.208" y1="1.52" x2="5.2" y2="1.552" layer="21"/>
<rectangle x1="-5.264" y1="1.552" x2="-4.304" y2="1.584" layer="21"/>
<rectangle x1="-3.12" y1="1.552" x2="-2.096" y2="1.584" layer="21"/>
<rectangle x1="-0.56" y1="1.552" x2="0.496" y2="1.584" layer="21"/>
<rectangle x1="2.064" y1="1.552" x2="3.056" y2="1.584" layer="21"/>
<rectangle x1="4.208" y1="1.552" x2="5.2" y2="1.584" layer="21"/>
<rectangle x1="-5.264" y1="1.584" x2="-4.304" y2="1.616" layer="21"/>
<rectangle x1="-3.12" y1="1.584" x2="-2.096" y2="1.616" layer="21"/>
<rectangle x1="-0.56" y1="1.584" x2="0.496" y2="1.616" layer="21"/>
<rectangle x1="2.064" y1="1.584" x2="3.024" y2="1.616" layer="21"/>
<rectangle x1="4.208" y1="1.584" x2="5.2" y2="1.616" layer="21"/>
<rectangle x1="-5.264" y1="1.616" x2="-4.304" y2="1.648" layer="21"/>
<rectangle x1="-3.088" y1="1.616" x2="-2.096" y2="1.648" layer="21"/>
<rectangle x1="-0.56" y1="1.616" x2="0.496" y2="1.648" layer="21"/>
<rectangle x1="2.064" y1="1.616" x2="3.024" y2="1.648" layer="21"/>
<rectangle x1="4.24" y1="1.616" x2="5.2" y2="1.648" layer="21"/>
<rectangle x1="-5.264" y1="1.648" x2="-4.304" y2="1.68" layer="21"/>
<rectangle x1="-3.088" y1="1.648" x2="-2.096" y2="1.68" layer="21"/>
<rectangle x1="-0.56" y1="1.648" x2="0.496" y2="1.68" layer="21"/>
<rectangle x1="2.032" y1="1.648" x2="3.024" y2="1.68" layer="21"/>
<rectangle x1="4.24" y1="1.648" x2="5.2" y2="1.68" layer="21"/>
<rectangle x1="-5.264" y1="1.68" x2="-4.304" y2="1.712" layer="21"/>
<rectangle x1="-3.088" y1="1.68" x2="-2.096" y2="1.712" layer="21"/>
<rectangle x1="-0.56" y1="1.68" x2="0.496" y2="1.712" layer="21"/>
<rectangle x1="2.032" y1="1.68" x2="3.024" y2="1.712" layer="21"/>
<rectangle x1="4.24" y1="1.68" x2="5.232" y2="1.712" layer="21"/>
<rectangle x1="-5.264" y1="1.712" x2="-4.304" y2="1.744" layer="21"/>
<rectangle x1="-3.088" y1="1.712" x2="-2.096" y2="1.744" layer="21"/>
<rectangle x1="-0.56" y1="1.712" x2="0.496" y2="1.744" layer="21"/>
<rectangle x1="2.032" y1="1.712" x2="3.024" y2="1.744" layer="21"/>
<rectangle x1="4.24" y1="1.712" x2="5.232" y2="1.744" layer="21"/>
<rectangle x1="-5.264" y1="1.744" x2="-4.304" y2="1.776" layer="21"/>
<rectangle x1="-3.088" y1="1.744" x2="-2.064" y2="1.776" layer="21"/>
<rectangle x1="-0.56" y1="1.744" x2="0.496" y2="1.776" layer="21"/>
<rectangle x1="2.032" y1="1.744" x2="3.024" y2="1.776" layer="21"/>
<rectangle x1="4.24" y1="1.744" x2="5.232" y2="1.776" layer="21"/>
<rectangle x1="-5.264" y1="1.776" x2="-4.304" y2="1.808" layer="21"/>
<rectangle x1="-3.056" y1="1.776" x2="-2.064" y2="1.808" layer="21"/>
<rectangle x1="-0.56" y1="1.776" x2="0.496" y2="1.808" layer="21"/>
<rectangle x1="2.032" y1="1.776" x2="3.024" y2="1.808" layer="21"/>
<rectangle x1="4.24" y1="1.776" x2="5.232" y2="1.808" layer="21"/>
<rectangle x1="-5.264" y1="1.808" x2="-4.304" y2="1.84" layer="21"/>
<rectangle x1="-3.056" y1="1.808" x2="-2.064" y2="1.84" layer="21"/>
<rectangle x1="-0.56" y1="1.808" x2="0.496" y2="1.84" layer="21"/>
<rectangle x1="2.032" y1="1.808" x2="2.992" y2="1.84" layer="21"/>
<rectangle x1="4.24" y1="1.808" x2="5.232" y2="1.84" layer="21"/>
<rectangle x1="-5.264" y1="1.84" x2="-4.304" y2="1.872" layer="21"/>
<rectangle x1="-3.056" y1="1.84" x2="-2.064" y2="1.872" layer="21"/>
<rectangle x1="-0.56" y1="1.84" x2="0.496" y2="1.872" layer="21"/>
<rectangle x1="2.032" y1="1.84" x2="2.992" y2="1.872" layer="21"/>
<rectangle x1="4.24" y1="1.84" x2="5.232" y2="1.872" layer="21"/>
<rectangle x1="-5.264" y1="1.872" x2="-4.304" y2="1.904" layer="21"/>
<rectangle x1="-3.056" y1="1.872" x2="-2.064" y2="1.904" layer="21"/>
<rectangle x1="-0.56" y1="1.872" x2="0.496" y2="1.904" layer="21"/>
<rectangle x1="2.032" y1="1.872" x2="2.992" y2="1.904" layer="21"/>
<rectangle x1="4.272" y1="1.872" x2="5.232" y2="1.904" layer="21"/>
<rectangle x1="-5.264" y1="1.904" x2="-4.304" y2="1.936" layer="21"/>
<rectangle x1="-3.056" y1="1.904" x2="-2.064" y2="1.936" layer="21"/>
<rectangle x1="-0.56" y1="1.904" x2="0.496" y2="1.936" layer="21"/>
<rectangle x1="2.032" y1="1.904" x2="2.992" y2="1.936" layer="21"/>
<rectangle x1="4.272" y1="1.904" x2="5.232" y2="1.936" layer="21"/>
<rectangle x1="-5.264" y1="1.936" x2="-4.304" y2="1.968" layer="21"/>
<rectangle x1="-3.056" y1="1.936" x2="-2.064" y2="1.968" layer="21"/>
<rectangle x1="-0.56" y1="1.936" x2="0.496" y2="1.968" layer="21"/>
<rectangle x1="2.032" y1="1.936" x2="2.992" y2="1.968" layer="21"/>
<rectangle x1="4.272" y1="1.936" x2="5.232" y2="1.968" layer="21"/>
<rectangle x1="-5.264" y1="1.968" x2="-4.304" y2="2" layer="21"/>
<rectangle x1="-3.056" y1="1.968" x2="-2.064" y2="2" layer="21"/>
<rectangle x1="-0.56" y1="1.968" x2="0.496" y2="2" layer="21"/>
<rectangle x1="2.032" y1="1.968" x2="2.992" y2="2" layer="21"/>
<rectangle x1="4.272" y1="1.968" x2="5.232" y2="2" layer="21"/>
<rectangle x1="-5.264" y1="2" x2="-4.304" y2="2.032" layer="21"/>
<rectangle x1="-3.056" y1="2" x2="-2.064" y2="2.032" layer="21"/>
<rectangle x1="-0.56" y1="2" x2="0.496" y2="2.032" layer="21"/>
<rectangle x1="2.032" y1="2" x2="2.992" y2="2.032" layer="21"/>
<rectangle x1="4.272" y1="2" x2="5.232" y2="2.032" layer="21"/>
<rectangle x1="-5.264" y1="2.032" x2="-4.304" y2="2.064" layer="21"/>
<rectangle x1="-3.056" y1="2.032" x2="-2.064" y2="2.064" layer="21"/>
<rectangle x1="-0.56" y1="2.032" x2="0.496" y2="2.064" layer="21"/>
<rectangle x1="2.032" y1="2.032" x2="2.992" y2="2.064" layer="21"/>
<rectangle x1="4.272" y1="2.032" x2="5.232" y2="2.064" layer="21"/>
<rectangle x1="-5.264" y1="2.064" x2="-4.304" y2="2.096" layer="21"/>
<rectangle x1="-3.056" y1="2.064" x2="-2.064" y2="2.096" layer="21"/>
<rectangle x1="-0.56" y1="2.064" x2="0.496" y2="2.096" layer="21"/>
<rectangle x1="2.032" y1="2.064" x2="2.992" y2="2.096" layer="21"/>
<rectangle x1="4.272" y1="2.064" x2="5.232" y2="2.096" layer="21"/>
<rectangle x1="-5.264" y1="2.096" x2="-4.304" y2="2.128" layer="21"/>
<rectangle x1="-3.056" y1="2.096" x2="-2.064" y2="2.128" layer="21"/>
<rectangle x1="-0.56" y1="2.096" x2="0.496" y2="2.128" layer="21"/>
<rectangle x1="2.032" y1="2.096" x2="2.992" y2="2.128" layer="21"/>
<rectangle x1="4.272" y1="2.096" x2="5.232" y2="2.128" layer="21"/>
<rectangle x1="-5.264" y1="2.128" x2="-4.304" y2="2.16" layer="21"/>
<rectangle x1="-3.024" y1="2.128" x2="-2.064" y2="2.16" layer="21"/>
<rectangle x1="-0.56" y1="2.128" x2="0.496" y2="2.16" layer="21"/>
<rectangle x1="2.032" y1="2.128" x2="2.992" y2="2.16" layer="21"/>
<rectangle x1="4.272" y1="2.128" x2="5.232" y2="2.16" layer="21"/>
<rectangle x1="-5.264" y1="2.16" x2="-4.304" y2="2.192" layer="21"/>
<rectangle x1="-3.024" y1="2.16" x2="-2.064" y2="2.192" layer="21"/>
<rectangle x1="-0.56" y1="2.16" x2="0.496" y2="2.192" layer="21"/>
<rectangle x1="2.032" y1="2.16" x2="2.992" y2="2.192" layer="21"/>
<rectangle x1="4.272" y1="2.16" x2="5.232" y2="2.192" layer="21"/>
<rectangle x1="-5.264" y1="2.192" x2="-4.304" y2="2.224" layer="21"/>
<rectangle x1="-3.024" y1="2.192" x2="-2.032" y2="2.224" layer="21"/>
<rectangle x1="-0.56" y1="2.192" x2="0.496" y2="2.224" layer="21"/>
<rectangle x1="2.032" y1="2.192" x2="2.992" y2="2.224" layer="21"/>
<rectangle x1="4.272" y1="2.192" x2="5.232" y2="2.224" layer="21"/>
<rectangle x1="-5.264" y1="2.224" x2="-4.304" y2="2.256" layer="21"/>
<rectangle x1="-3.024" y1="2.224" x2="-2.032" y2="2.256" layer="21"/>
<rectangle x1="-0.56" y1="2.224" x2="0.496" y2="2.256" layer="21"/>
<rectangle x1="2.032" y1="2.224" x2="2.992" y2="2.256" layer="21"/>
<rectangle x1="4.272" y1="2.224" x2="5.232" y2="2.256" layer="21"/>
<rectangle x1="-5.264" y1="2.256" x2="-4.304" y2="2.288" layer="21"/>
<rectangle x1="-3.024" y1="2.256" x2="-2.032" y2="2.288" layer="21"/>
<rectangle x1="-0.56" y1="2.256" x2="0.496" y2="2.288" layer="21"/>
<rectangle x1="2.032" y1="2.256" x2="2.992" y2="2.288" layer="21"/>
<rectangle x1="4.272" y1="2.256" x2="5.232" y2="2.288" layer="21"/>
<rectangle x1="-5.264" y1="2.288" x2="-4.304" y2="2.32" layer="21"/>
<rectangle x1="-3.024" y1="2.288" x2="-2.032" y2="2.32" layer="21"/>
<rectangle x1="-0.56" y1="2.288" x2="0.496" y2="2.32" layer="21"/>
<rectangle x1="2.032" y1="2.288" x2="2.992" y2="2.32" layer="21"/>
<rectangle x1="4.272" y1="2.288" x2="5.232" y2="2.32" layer="21"/>
<rectangle x1="-5.264" y1="2.32" x2="-4.304" y2="2.352" layer="21"/>
<rectangle x1="-3.024" y1="2.32" x2="-2.032" y2="2.352" layer="21"/>
<rectangle x1="-0.56" y1="2.32" x2="0.496" y2="2.352" layer="21"/>
<rectangle x1="2.032" y1="2.32" x2="2.992" y2="2.352" layer="21"/>
<rectangle x1="4.272" y1="2.32" x2="5.232" y2="2.352" layer="21"/>
<rectangle x1="-5.264" y1="2.352" x2="-4.304" y2="2.384" layer="21"/>
<rectangle x1="-3.024" y1="2.352" x2="-2.032" y2="2.384" layer="21"/>
<rectangle x1="-0.56" y1="2.352" x2="0.496" y2="2.384" layer="21"/>
<rectangle x1="2.032" y1="2.352" x2="2.992" y2="2.384" layer="21"/>
<rectangle x1="4.272" y1="2.352" x2="5.232" y2="2.384" layer="21"/>
<rectangle x1="-5.264" y1="2.384" x2="-4.304" y2="2.416" layer="21"/>
<rectangle x1="-3.024" y1="2.384" x2="-2.032" y2="2.416" layer="21"/>
<rectangle x1="-0.56" y1="2.384" x2="0.496" y2="2.416" layer="21"/>
<rectangle x1="2.032" y1="2.384" x2="2.992" y2="2.416" layer="21"/>
<rectangle x1="4.272" y1="2.384" x2="5.232" y2="2.416" layer="21"/>
<rectangle x1="-5.264" y1="2.416" x2="-4.304" y2="2.448" layer="21"/>
<rectangle x1="-3.024" y1="2.416" x2="-2.032" y2="2.448" layer="21"/>
<rectangle x1="-0.56" y1="2.416" x2="0.496" y2="2.448" layer="21"/>
<rectangle x1="2.032" y1="2.416" x2="2.992" y2="2.448" layer="21"/>
<rectangle x1="4.272" y1="2.416" x2="5.232" y2="2.448" layer="21"/>
<rectangle x1="-5.264" y1="2.448" x2="-4.304" y2="2.48" layer="21"/>
<rectangle x1="-3.024" y1="2.448" x2="-2.032" y2="2.48" layer="21"/>
<rectangle x1="-0.56" y1="2.448" x2="0.496" y2="2.48" layer="21"/>
<rectangle x1="2.032" y1="2.448" x2="2.992" y2="2.48" layer="21"/>
<rectangle x1="4.272" y1="2.448" x2="5.232" y2="2.48" layer="21"/>
<rectangle x1="-5.264" y1="2.48" x2="-4.304" y2="2.512" layer="21"/>
<rectangle x1="-3.024" y1="2.48" x2="-2.032" y2="2.512" layer="21"/>
<rectangle x1="-0.56" y1="2.48" x2="0.496" y2="2.512" layer="21"/>
<rectangle x1="2.032" y1="2.48" x2="2.992" y2="2.512" layer="21"/>
<rectangle x1="4.272" y1="2.48" x2="5.232" y2="2.512" layer="21"/>
<rectangle x1="-5.264" y1="2.512" x2="-4.304" y2="2.544" layer="21"/>
<rectangle x1="-3.024" y1="2.512" x2="-2.032" y2="2.544" layer="21"/>
<rectangle x1="-0.56" y1="2.512" x2="0.496" y2="2.544" layer="21"/>
<rectangle x1="2.032" y1="2.512" x2="2.992" y2="2.544" layer="21"/>
<rectangle x1="4.272" y1="2.512" x2="5.232" y2="2.544" layer="21"/>
<rectangle x1="-5.264" y1="2.544" x2="-4.304" y2="2.576" layer="21"/>
<rectangle x1="-3.024" y1="2.544" x2="-2.032" y2="2.576" layer="21"/>
<rectangle x1="-0.56" y1="2.544" x2="0.496" y2="2.576" layer="21"/>
<rectangle x1="2.032" y1="2.544" x2="2.992" y2="2.576" layer="21"/>
<rectangle x1="4.272" y1="2.544" x2="5.232" y2="2.576" layer="21"/>
<rectangle x1="-5.264" y1="2.576" x2="-4.304" y2="2.608" layer="21"/>
<rectangle x1="-3.024" y1="2.576" x2="-2.032" y2="2.608" layer="21"/>
<rectangle x1="-0.56" y1="2.576" x2="0.496" y2="2.608" layer="21"/>
<rectangle x1="2.032" y1="2.576" x2="2.992" y2="2.608" layer="21"/>
<rectangle x1="4.272" y1="2.576" x2="5.232" y2="2.608" layer="21"/>
<rectangle x1="-5.264" y1="2.608" x2="-4.304" y2="2.64" layer="21"/>
<rectangle x1="-3.024" y1="2.608" x2="-2.032" y2="2.64" layer="21"/>
<rectangle x1="-0.56" y1="2.608" x2="0.496" y2="2.64" layer="21"/>
<rectangle x1="2.032" y1="2.608" x2="2.992" y2="2.64" layer="21"/>
<rectangle x1="4.272" y1="2.608" x2="5.232" y2="2.64" layer="21"/>
<rectangle x1="-5.264" y1="2.64" x2="-4.304" y2="2.672" layer="21"/>
<rectangle x1="-3.024" y1="2.64" x2="-2.032" y2="2.672" layer="21"/>
<rectangle x1="-0.56" y1="2.64" x2="0.496" y2="2.672" layer="21"/>
<rectangle x1="2.032" y1="2.64" x2="2.992" y2="2.672" layer="21"/>
<rectangle x1="4.272" y1="2.64" x2="5.232" y2="2.672" layer="21"/>
<rectangle x1="-5.264" y1="2.672" x2="-4.304" y2="2.704" layer="21"/>
<rectangle x1="-3.024" y1="2.672" x2="-2.032" y2="2.704" layer="21"/>
<rectangle x1="-0.56" y1="2.672" x2="0.496" y2="2.704" layer="21"/>
<rectangle x1="2.032" y1="2.672" x2="2.992" y2="2.704" layer="21"/>
<rectangle x1="4.272" y1="2.672" x2="5.232" y2="2.704" layer="21"/>
<rectangle x1="-5.264" y1="2.704" x2="-4.304" y2="2.736" layer="21"/>
<rectangle x1="-3.024" y1="2.704" x2="-2.032" y2="2.736" layer="21"/>
<rectangle x1="-0.56" y1="2.704" x2="0.496" y2="2.736" layer="21"/>
<rectangle x1="2.032" y1="2.704" x2="2.992" y2="2.736" layer="21"/>
<rectangle x1="4.272" y1="2.704" x2="5.232" y2="2.736" layer="21"/>
<rectangle x1="-5.264" y1="2.736" x2="-4.304" y2="2.768" layer="21"/>
<rectangle x1="-3.024" y1="2.736" x2="-2.032" y2="2.768" layer="21"/>
<rectangle x1="-0.56" y1="2.736" x2="0.496" y2="2.768" layer="21"/>
<rectangle x1="2.032" y1="2.736" x2="2.992" y2="2.768" layer="21"/>
<rectangle x1="4.272" y1="2.736" x2="5.232" y2="2.768" layer="21"/>
<rectangle x1="-5.264" y1="2.768" x2="-4.304" y2="2.8" layer="21"/>
<rectangle x1="-3.024" y1="2.768" x2="-2.032" y2="2.8" layer="21"/>
<rectangle x1="-0.56" y1="2.768" x2="0.496" y2="2.8" layer="21"/>
<rectangle x1="2.032" y1="2.768" x2="2.992" y2="2.8" layer="21"/>
<rectangle x1="4.272" y1="2.768" x2="5.232" y2="2.8" layer="21"/>
<rectangle x1="-5.264" y1="2.8" x2="-4.304" y2="2.832" layer="21"/>
<rectangle x1="-3.024" y1="2.8" x2="-2.032" y2="2.832" layer="21"/>
<rectangle x1="-0.56" y1="2.8" x2="0.496" y2="2.832" layer="21"/>
<rectangle x1="2.032" y1="2.8" x2="2.992" y2="2.832" layer="21"/>
<rectangle x1="4.272" y1="2.8" x2="5.232" y2="2.832" layer="21"/>
<rectangle x1="-5.264" y1="2.832" x2="-4.304" y2="2.864" layer="21"/>
<rectangle x1="-3.024" y1="2.832" x2="-2.032" y2="2.864" layer="21"/>
<rectangle x1="-0.56" y1="2.832" x2="0.496" y2="2.864" layer="21"/>
<rectangle x1="2.032" y1="2.832" x2="2.992" y2="2.864" layer="21"/>
<rectangle x1="4.272" y1="2.832" x2="5.232" y2="2.864" layer="21"/>
<rectangle x1="-5.264" y1="2.864" x2="-4.304" y2="2.896" layer="21"/>
<rectangle x1="-3.024" y1="2.864" x2="-2.032" y2="2.896" layer="21"/>
<rectangle x1="-0.56" y1="2.864" x2="0.496" y2="2.896" layer="21"/>
<rectangle x1="2.032" y1="2.864" x2="2.992" y2="2.896" layer="21"/>
<rectangle x1="4.272" y1="2.864" x2="5.232" y2="2.896" layer="21"/>
<rectangle x1="-5.264" y1="2.896" x2="-4.304" y2="2.928" layer="21"/>
<rectangle x1="-3.024" y1="2.896" x2="-2.032" y2="2.928" layer="21"/>
<rectangle x1="-0.56" y1="2.896" x2="0.496" y2="2.928" layer="21"/>
<rectangle x1="2.032" y1="2.896" x2="2.992" y2="2.928" layer="21"/>
<rectangle x1="4.272" y1="2.896" x2="5.232" y2="2.928" layer="21"/>
<rectangle x1="-5.264" y1="2.928" x2="-4.304" y2="2.96" layer="21"/>
<rectangle x1="-3.024" y1="2.928" x2="-2.032" y2="2.96" layer="21"/>
<rectangle x1="-0.56" y1="2.928" x2="0.496" y2="2.96" layer="21"/>
<rectangle x1="2.032" y1="2.928" x2="2.992" y2="2.96" layer="21"/>
<rectangle x1="4.272" y1="2.928" x2="5.232" y2="2.96" layer="21"/>
<rectangle x1="-5.264" y1="2.96" x2="-4.304" y2="2.992" layer="21"/>
<rectangle x1="-3.024" y1="2.96" x2="-2.032" y2="2.992" layer="21"/>
<rectangle x1="-0.56" y1="2.96" x2="0.496" y2="2.992" layer="21"/>
<rectangle x1="2.032" y1="2.96" x2="2.992" y2="2.992" layer="21"/>
<rectangle x1="4.272" y1="2.96" x2="5.232" y2="2.992" layer="21"/>
<rectangle x1="-5.264" y1="2.992" x2="-4.304" y2="3.024" layer="21"/>
<rectangle x1="-3.024" y1="2.992" x2="-2.032" y2="3.024" layer="21"/>
<rectangle x1="-0.56" y1="2.992" x2="0.496" y2="3.024" layer="21"/>
<rectangle x1="2.032" y1="2.992" x2="2.992" y2="3.024" layer="21"/>
<rectangle x1="4.272" y1="2.992" x2="5.232" y2="3.024" layer="21"/>
<rectangle x1="-5.264" y1="3.024" x2="-4.304" y2="3.056" layer="21"/>
<rectangle x1="-3.024" y1="3.024" x2="-2.032" y2="3.056" layer="21"/>
<rectangle x1="-0.56" y1="3.024" x2="0.496" y2="3.056" layer="21"/>
<rectangle x1="2.032" y1="3.024" x2="2.992" y2="3.056" layer="21"/>
<rectangle x1="4.272" y1="3.024" x2="5.232" y2="3.056" layer="21"/>
<rectangle x1="-5.264" y1="3.056" x2="-4.304" y2="3.088" layer="21"/>
<rectangle x1="-3.024" y1="3.056" x2="-2.032" y2="3.088" layer="21"/>
<rectangle x1="-0.56" y1="3.056" x2="0.496" y2="3.088" layer="21"/>
<rectangle x1="2.032" y1="3.056" x2="2.992" y2="3.088" layer="21"/>
<rectangle x1="4.272" y1="3.056" x2="5.232" y2="3.088" layer="21"/>
<rectangle x1="-5.264" y1="3.088" x2="-4.304" y2="3.12" layer="21"/>
<rectangle x1="-3.024" y1="3.088" x2="-2.032" y2="3.12" layer="21"/>
<rectangle x1="-0.56" y1="3.088" x2="0.496" y2="3.12" layer="21"/>
<rectangle x1="2.032" y1="3.088" x2="2.992" y2="3.12" layer="21"/>
<rectangle x1="4.272" y1="3.088" x2="5.232" y2="3.12" layer="21"/>
<rectangle x1="-5.264" y1="3.12" x2="-4.304" y2="3.152" layer="21"/>
<rectangle x1="-3.024" y1="3.12" x2="-2.032" y2="3.152" layer="21"/>
<rectangle x1="-0.56" y1="3.12" x2="0.496" y2="3.152" layer="21"/>
<rectangle x1="2.032" y1="3.12" x2="2.992" y2="3.152" layer="21"/>
<rectangle x1="4.272" y1="3.12" x2="5.232" y2="3.152" layer="21"/>
<rectangle x1="-5.264" y1="3.152" x2="-4.304" y2="3.184" layer="21"/>
<rectangle x1="-3.024" y1="3.152" x2="-2.032" y2="3.184" layer="21"/>
<rectangle x1="-0.56" y1="3.152" x2="0.496" y2="3.184" layer="21"/>
<rectangle x1="2.032" y1="3.152" x2="2.992" y2="3.184" layer="21"/>
<rectangle x1="4.272" y1="3.152" x2="5.232" y2="3.184" layer="21"/>
<rectangle x1="-5.264" y1="3.184" x2="-4.304" y2="3.216" layer="21"/>
<rectangle x1="-3.024" y1="3.184" x2="-2.032" y2="3.216" layer="21"/>
<rectangle x1="-0.56" y1="3.184" x2="0.496" y2="3.216" layer="21"/>
<rectangle x1="2.032" y1="3.184" x2="2.992" y2="3.216" layer="21"/>
<rectangle x1="4.272" y1="3.184" x2="5.232" y2="3.216" layer="21"/>
<rectangle x1="-5.264" y1="3.216" x2="-4.304" y2="3.248" layer="21"/>
<rectangle x1="-3.024" y1="3.216" x2="-2.032" y2="3.248" layer="21"/>
<rectangle x1="-0.56" y1="3.216" x2="0.496" y2="3.248" layer="21"/>
<rectangle x1="2.032" y1="3.216" x2="2.992" y2="3.248" layer="21"/>
<rectangle x1="4.272" y1="3.216" x2="5.232" y2="3.248" layer="21"/>
<rectangle x1="-5.264" y1="3.248" x2="-4.304" y2="3.28" layer="21"/>
<rectangle x1="-3.024" y1="3.248" x2="-2.032" y2="3.28" layer="21"/>
<rectangle x1="-0.56" y1="3.248" x2="0.496" y2="3.28" layer="21"/>
<rectangle x1="2.032" y1="3.248" x2="2.992" y2="3.28" layer="21"/>
<rectangle x1="4.272" y1="3.248" x2="5.232" y2="3.28" layer="21"/>
<rectangle x1="-5.264" y1="3.28" x2="-4.304" y2="3.312" layer="21"/>
<rectangle x1="-3.024" y1="3.28" x2="-2.032" y2="3.312" layer="21"/>
<rectangle x1="-0.56" y1="3.28" x2="0.496" y2="3.312" layer="21"/>
<rectangle x1="2.032" y1="3.28" x2="2.992" y2="3.312" layer="21"/>
<rectangle x1="4.272" y1="3.28" x2="5.232" y2="3.312" layer="21"/>
<rectangle x1="-5.264" y1="3.312" x2="-4.304" y2="3.344" layer="21"/>
<rectangle x1="-3.024" y1="3.312" x2="-2.032" y2="3.344" layer="21"/>
<rectangle x1="-0.56" y1="3.312" x2="0.496" y2="3.344" layer="21"/>
<rectangle x1="2.032" y1="3.312" x2="2.992" y2="3.344" layer="21"/>
<rectangle x1="4.272" y1="3.312" x2="5.232" y2="3.344" layer="21"/>
<rectangle x1="-5.264" y1="3.344" x2="-4.304" y2="3.376" layer="21"/>
<rectangle x1="-3.024" y1="3.344" x2="-2.032" y2="3.376" layer="21"/>
<rectangle x1="-0.56" y1="3.344" x2="0.496" y2="3.376" layer="21"/>
<rectangle x1="2.032" y1="3.344" x2="2.992" y2="3.376" layer="21"/>
<rectangle x1="4.272" y1="3.344" x2="5.232" y2="3.376" layer="21"/>
<rectangle x1="-5.264" y1="3.376" x2="-4.304" y2="3.408" layer="21"/>
<rectangle x1="-3.024" y1="3.376" x2="-2.032" y2="3.408" layer="21"/>
<rectangle x1="-0.56" y1="3.376" x2="0.496" y2="3.408" layer="21"/>
<rectangle x1="2.032" y1="3.376" x2="2.992" y2="3.408" layer="21"/>
<rectangle x1="4.272" y1="3.376" x2="5.232" y2="3.408" layer="21"/>
<rectangle x1="-5.264" y1="3.408" x2="-4.304" y2="3.44" layer="21"/>
<rectangle x1="-3.024" y1="3.408" x2="-2.032" y2="3.44" layer="21"/>
<rectangle x1="-0.56" y1="3.408" x2="0.496" y2="3.44" layer="21"/>
<rectangle x1="2.032" y1="3.408" x2="2.992" y2="3.44" layer="21"/>
<rectangle x1="4.272" y1="3.408" x2="5.232" y2="3.44" layer="21"/>
<rectangle x1="-5.264" y1="3.44" x2="-4.304" y2="3.472" layer="21"/>
<rectangle x1="-3.024" y1="3.44" x2="-2.032" y2="3.472" layer="21"/>
<rectangle x1="-0.56" y1="3.44" x2="0.496" y2="3.472" layer="21"/>
<rectangle x1="2.032" y1="3.44" x2="2.992" y2="3.472" layer="21"/>
<rectangle x1="4.272" y1="3.44" x2="5.232" y2="3.472" layer="21"/>
<rectangle x1="-5.264" y1="3.472" x2="-4.304" y2="3.504" layer="21"/>
<rectangle x1="-3.024" y1="3.472" x2="-2.032" y2="3.504" layer="21"/>
<rectangle x1="-0.56" y1="3.472" x2="0.496" y2="3.504" layer="21"/>
<rectangle x1="2.032" y1="3.472" x2="2.992" y2="3.504" layer="21"/>
<rectangle x1="4.272" y1="3.472" x2="5.232" y2="3.504" layer="21"/>
<rectangle x1="-5.264" y1="3.504" x2="-4.304" y2="3.536" layer="21"/>
<rectangle x1="-3.024" y1="3.504" x2="-2.032" y2="3.536" layer="21"/>
<rectangle x1="-0.56" y1="3.504" x2="0.496" y2="3.536" layer="21"/>
<rectangle x1="2.032" y1="3.504" x2="2.992" y2="3.536" layer="21"/>
<rectangle x1="4.272" y1="3.504" x2="5.232" y2="3.536" layer="21"/>
<rectangle x1="-5.264" y1="3.536" x2="-4.304" y2="3.568" layer="21"/>
<rectangle x1="-3.024" y1="3.536" x2="-2.032" y2="3.568" layer="21"/>
<rectangle x1="-0.56" y1="3.536" x2="0.496" y2="3.568" layer="21"/>
<rectangle x1="2.032" y1="3.536" x2="2.992" y2="3.568" layer="21"/>
<rectangle x1="4.272" y1="3.536" x2="5.232" y2="3.568" layer="21"/>
<rectangle x1="-5.264" y1="3.568" x2="-4.304" y2="3.6" layer="21"/>
<rectangle x1="-3.024" y1="3.568" x2="-2.032" y2="3.6" layer="21"/>
<rectangle x1="-0.56" y1="3.568" x2="0.496" y2="3.6" layer="21"/>
<rectangle x1="2.032" y1="3.568" x2="2.992" y2="3.6" layer="21"/>
<rectangle x1="4.272" y1="3.568" x2="5.232" y2="3.6" layer="21"/>
<rectangle x1="-5.264" y1="3.6" x2="-4.304" y2="3.632" layer="21"/>
<rectangle x1="-3.024" y1="3.6" x2="-2.032" y2="3.632" layer="21"/>
<rectangle x1="-0.56" y1="3.6" x2="0.496" y2="3.632" layer="21"/>
<rectangle x1="2.032" y1="3.6" x2="2.992" y2="3.632" layer="21"/>
<rectangle x1="4.272" y1="3.6" x2="5.232" y2="3.632" layer="21"/>
<rectangle x1="-5.264" y1="3.632" x2="-4.304" y2="3.664" layer="21"/>
<rectangle x1="-3.024" y1="3.632" x2="-2.032" y2="3.664" layer="21"/>
<rectangle x1="-0.56" y1="3.632" x2="0.496" y2="3.664" layer="21"/>
<rectangle x1="2.032" y1="3.632" x2="2.992" y2="3.664" layer="21"/>
<rectangle x1="4.272" y1="3.632" x2="5.232" y2="3.664" layer="21"/>
<rectangle x1="-5.264" y1="3.664" x2="-4.304" y2="3.696" layer="21"/>
<rectangle x1="-3.024" y1="3.664" x2="-2.032" y2="3.696" layer="21"/>
<rectangle x1="-0.56" y1="3.664" x2="0.496" y2="3.696" layer="21"/>
<rectangle x1="2.032" y1="3.664" x2="2.992" y2="3.696" layer="21"/>
<rectangle x1="4.272" y1="3.664" x2="5.232" y2="3.696" layer="21"/>
<rectangle x1="-5.264" y1="3.696" x2="-4.304" y2="3.728" layer="21"/>
<rectangle x1="-3.024" y1="3.696" x2="-2.032" y2="3.728" layer="21"/>
<rectangle x1="-0.56" y1="3.696" x2="0.496" y2="3.728" layer="21"/>
<rectangle x1="2.032" y1="3.696" x2="2.992" y2="3.728" layer="21"/>
<rectangle x1="4.272" y1="3.696" x2="5.232" y2="3.728" layer="21"/>
<rectangle x1="-5.264" y1="3.728" x2="-4.304" y2="3.76" layer="21"/>
<rectangle x1="-3.024" y1="3.728" x2="-2.032" y2="3.76" layer="21"/>
<rectangle x1="-0.56" y1="3.728" x2="0.496" y2="3.76" layer="21"/>
<rectangle x1="2.032" y1="3.728" x2="2.992" y2="3.76" layer="21"/>
<rectangle x1="4.272" y1="3.728" x2="5.232" y2="3.76" layer="21"/>
<rectangle x1="-5.264" y1="3.76" x2="-4.304" y2="3.792" layer="21"/>
<rectangle x1="-3.024" y1="3.76" x2="-2.032" y2="3.792" layer="21"/>
<rectangle x1="-0.56" y1="3.76" x2="0.496" y2="3.792" layer="21"/>
<rectangle x1="2.032" y1="3.76" x2="2.992" y2="3.792" layer="21"/>
<rectangle x1="4.272" y1="3.76" x2="5.232" y2="3.792" layer="21"/>
<rectangle x1="-5.264" y1="3.792" x2="-4.304" y2="3.824" layer="21"/>
<rectangle x1="-3.024" y1="3.792" x2="-2.032" y2="3.824" layer="21"/>
<rectangle x1="-0.56" y1="3.792" x2="0.496" y2="3.824" layer="21"/>
<rectangle x1="2.032" y1="3.792" x2="2.992" y2="3.824" layer="21"/>
<rectangle x1="4.272" y1="3.792" x2="5.232" y2="3.824" layer="21"/>
<rectangle x1="-5.264" y1="3.824" x2="-4.304" y2="3.856" layer="21"/>
<rectangle x1="-3.024" y1="3.824" x2="-2.032" y2="3.856" layer="21"/>
<rectangle x1="-0.56" y1="3.824" x2="0.496" y2="3.856" layer="21"/>
<rectangle x1="2.032" y1="3.824" x2="2.992" y2="3.856" layer="21"/>
<rectangle x1="4.272" y1="3.824" x2="5.232" y2="3.856" layer="21"/>
<rectangle x1="-5.264" y1="3.856" x2="-4.304" y2="3.888" layer="21"/>
<rectangle x1="-3.024" y1="3.856" x2="-2.032" y2="3.888" layer="21"/>
<rectangle x1="-0.56" y1="3.856" x2="0.496" y2="3.888" layer="21"/>
<rectangle x1="2.032" y1="3.856" x2="2.992" y2="3.888" layer="21"/>
<rectangle x1="4.272" y1="3.856" x2="5.232" y2="3.888" layer="21"/>
<rectangle x1="-5.264" y1="3.888" x2="-4.304" y2="3.92" layer="21"/>
<rectangle x1="-3.024" y1="3.888" x2="-2.032" y2="3.92" layer="21"/>
<rectangle x1="-0.56" y1="3.888" x2="0.496" y2="3.92" layer="21"/>
<rectangle x1="2.032" y1="3.888" x2="2.992" y2="3.92" layer="21"/>
<rectangle x1="4.272" y1="3.888" x2="5.232" y2="3.92" layer="21"/>
<rectangle x1="-5.264" y1="3.92" x2="-4.304" y2="3.952" layer="21"/>
<rectangle x1="-3.024" y1="3.92" x2="-2.032" y2="3.952" layer="21"/>
<rectangle x1="-0.56" y1="3.92" x2="0.496" y2="3.952" layer="21"/>
<rectangle x1="2.032" y1="3.92" x2="2.992" y2="3.952" layer="21"/>
<rectangle x1="4.272" y1="3.92" x2="5.232" y2="3.952" layer="21"/>
<rectangle x1="-5.264" y1="3.952" x2="-4.304" y2="3.984" layer="21"/>
<rectangle x1="-3.024" y1="3.952" x2="-2.032" y2="3.984" layer="21"/>
<rectangle x1="-0.56" y1="3.952" x2="0.496" y2="3.984" layer="21"/>
<rectangle x1="2.032" y1="3.952" x2="2.992" y2="3.984" layer="21"/>
<rectangle x1="4.272" y1="3.952" x2="5.232" y2="3.984" layer="21"/>
<rectangle x1="-5.264" y1="3.984" x2="-4.304" y2="4.016" layer="21"/>
<rectangle x1="-3.024" y1="3.984" x2="-2.032" y2="4.016" layer="21"/>
<rectangle x1="-0.56" y1="3.984" x2="0.496" y2="4.016" layer="21"/>
<rectangle x1="2.032" y1="3.984" x2="2.992" y2="4.016" layer="21"/>
<rectangle x1="4.272" y1="3.984" x2="5.232" y2="4.016" layer="21"/>
<rectangle x1="-5.264" y1="4.016" x2="-4.304" y2="4.048" layer="21"/>
<rectangle x1="-3.024" y1="4.016" x2="-2.032" y2="4.048" layer="21"/>
<rectangle x1="-0.56" y1="4.016" x2="0.496" y2="4.048" layer="21"/>
<rectangle x1="2.032" y1="4.016" x2="2.992" y2="4.048" layer="21"/>
<rectangle x1="4.272" y1="4.016" x2="5.232" y2="4.048" layer="21"/>
<rectangle x1="-5.264" y1="4.048" x2="-4.304" y2="4.08" layer="21"/>
<rectangle x1="-3.024" y1="4.048" x2="-2.032" y2="4.08" layer="21"/>
<rectangle x1="-0.56" y1="4.048" x2="0.496" y2="4.08" layer="21"/>
<rectangle x1="2.032" y1="4.048" x2="2.992" y2="4.08" layer="21"/>
<rectangle x1="4.272" y1="4.048" x2="5.232" y2="4.08" layer="21"/>
<rectangle x1="-5.264" y1="4.08" x2="-4.304" y2="4.112" layer="21"/>
<rectangle x1="-3.024" y1="4.08" x2="-2.032" y2="4.112" layer="21"/>
<rectangle x1="-0.56" y1="4.08" x2="0.496" y2="4.112" layer="21"/>
<rectangle x1="2.032" y1="4.08" x2="2.992" y2="4.112" layer="21"/>
<rectangle x1="4.272" y1="4.08" x2="5.232" y2="4.112" layer="21"/>
<rectangle x1="-5.264" y1="4.112" x2="-4.304" y2="4.144" layer="21"/>
<rectangle x1="-3.024" y1="4.112" x2="-2.032" y2="4.144" layer="21"/>
<rectangle x1="-0.56" y1="4.112" x2="0.496" y2="4.144" layer="21"/>
<rectangle x1="2.032" y1="4.112" x2="2.992" y2="4.144" layer="21"/>
<rectangle x1="4.272" y1="4.112" x2="5.232" y2="4.144" layer="21"/>
<rectangle x1="-5.264" y1="4.144" x2="-4.304" y2="4.176" layer="21"/>
<rectangle x1="-3.024" y1="4.144" x2="-2.032" y2="4.176" layer="21"/>
<rectangle x1="-0.56" y1="4.144" x2="0.496" y2="4.176" layer="21"/>
<rectangle x1="2.032" y1="4.144" x2="2.992" y2="4.176" layer="21"/>
<rectangle x1="4.272" y1="4.144" x2="5.232" y2="4.176" layer="21"/>
<rectangle x1="-5.264" y1="4.176" x2="-4.304" y2="4.208" layer="21"/>
<rectangle x1="-3.024" y1="4.176" x2="-2.032" y2="4.208" layer="21"/>
<rectangle x1="-0.56" y1="4.176" x2="0.496" y2="4.208" layer="21"/>
<rectangle x1="2.032" y1="4.176" x2="2.992" y2="4.208" layer="21"/>
<rectangle x1="4.272" y1="4.176" x2="5.232" y2="4.208" layer="21"/>
<rectangle x1="-5.264" y1="4.208" x2="-4.304" y2="4.24" layer="21"/>
<rectangle x1="-3.024" y1="4.208" x2="-2.032" y2="4.24" layer="21"/>
<rectangle x1="-0.56" y1="4.208" x2="0.496" y2="4.24" layer="21"/>
<rectangle x1="2.032" y1="4.208" x2="2.992" y2="4.24" layer="21"/>
<rectangle x1="4.272" y1="4.208" x2="5.232" y2="4.24" layer="21"/>
<rectangle x1="-5.264" y1="4.24" x2="-4.304" y2="4.272" layer="21"/>
<rectangle x1="-3.024" y1="4.24" x2="-2.032" y2="4.272" layer="21"/>
<rectangle x1="-0.56" y1="4.24" x2="0.496" y2="4.272" layer="21"/>
<rectangle x1="2.032" y1="4.24" x2="2.992" y2="4.272" layer="21"/>
<rectangle x1="4.272" y1="4.24" x2="5.232" y2="4.272" layer="21"/>
<rectangle x1="-5.264" y1="4.272" x2="-4.304" y2="4.304" layer="21"/>
<rectangle x1="-3.024" y1="4.272" x2="-2.032" y2="4.304" layer="21"/>
<rectangle x1="-0.56" y1="4.272" x2="0.496" y2="4.304" layer="21"/>
<rectangle x1="2.032" y1="4.272" x2="2.992" y2="4.304" layer="21"/>
<rectangle x1="4.272" y1="4.272" x2="5.232" y2="4.304" layer="21"/>
<rectangle x1="-5.264" y1="4.304" x2="-4.304" y2="4.336" layer="21"/>
<rectangle x1="-3.024" y1="4.304" x2="-2.032" y2="4.336" layer="21"/>
<rectangle x1="-0.56" y1="4.304" x2="0.496" y2="4.336" layer="21"/>
<rectangle x1="2.032" y1="4.304" x2="2.992" y2="4.336" layer="21"/>
<rectangle x1="4.272" y1="4.304" x2="5.232" y2="4.336" layer="21"/>
<rectangle x1="-5.264" y1="4.336" x2="-4.304" y2="4.368" layer="21"/>
<rectangle x1="-3.024" y1="4.336" x2="-2.032" y2="4.368" layer="21"/>
<rectangle x1="-0.56" y1="4.336" x2="0.496" y2="4.368" layer="21"/>
<rectangle x1="2.032" y1="4.336" x2="2.992" y2="4.368" layer="21"/>
<rectangle x1="4.272" y1="4.336" x2="5.232" y2="4.368" layer="21"/>
<rectangle x1="-5.264" y1="4.368" x2="-4.304" y2="4.4" layer="21"/>
<rectangle x1="-3.024" y1="4.368" x2="-2.032" y2="4.4" layer="21"/>
<rectangle x1="-0.56" y1="4.368" x2="0.496" y2="4.4" layer="21"/>
<rectangle x1="2.032" y1="4.368" x2="2.992" y2="4.4" layer="21"/>
<rectangle x1="4.272" y1="4.368" x2="5.232" y2="4.4" layer="21"/>
<rectangle x1="-5.264" y1="4.4" x2="-4.304" y2="4.432" layer="21"/>
<rectangle x1="-3.024" y1="4.4" x2="-2.032" y2="4.432" layer="21"/>
<rectangle x1="-0.56" y1="4.4" x2="0.496" y2="4.432" layer="21"/>
<rectangle x1="2.032" y1="4.4" x2="2.992" y2="4.432" layer="21"/>
<rectangle x1="4.272" y1="4.4" x2="5.232" y2="4.432" layer="21"/>
<rectangle x1="-5.264" y1="4.432" x2="-4.304" y2="4.464" layer="21"/>
<rectangle x1="-3.024" y1="4.432" x2="-2.032" y2="4.464" layer="21"/>
<rectangle x1="-0.56" y1="4.432" x2="0.496" y2="4.464" layer="21"/>
<rectangle x1="2.032" y1="4.432" x2="2.992" y2="4.464" layer="21"/>
<rectangle x1="4.272" y1="4.432" x2="5.232" y2="4.464" layer="21"/>
<rectangle x1="-5.264" y1="4.464" x2="-4.304" y2="4.496" layer="21"/>
<rectangle x1="-3.024" y1="4.464" x2="-2.032" y2="4.496" layer="21"/>
<rectangle x1="-0.56" y1="4.464" x2="0.496" y2="4.496" layer="21"/>
<rectangle x1="2.032" y1="4.464" x2="2.992" y2="4.496" layer="21"/>
<rectangle x1="4.272" y1="4.464" x2="5.232" y2="4.496" layer="21"/>
<rectangle x1="-5.264" y1="4.496" x2="-4.304" y2="4.528" layer="21"/>
<rectangle x1="-3.024" y1="4.496" x2="-2.032" y2="4.528" layer="21"/>
<rectangle x1="-0.56" y1="4.496" x2="0.496" y2="4.528" layer="21"/>
<rectangle x1="2.032" y1="4.496" x2="2.992" y2="4.528" layer="21"/>
<rectangle x1="4.272" y1="4.496" x2="5.232" y2="4.528" layer="21"/>
<rectangle x1="-5.264" y1="4.528" x2="-4.304" y2="4.56" layer="21"/>
<rectangle x1="-3.024" y1="4.528" x2="-2.032" y2="4.56" layer="21"/>
<rectangle x1="-0.56" y1="4.528" x2="0.496" y2="4.56" layer="21"/>
<rectangle x1="2.032" y1="4.528" x2="2.992" y2="4.56" layer="21"/>
<rectangle x1="4.272" y1="4.528" x2="5.232" y2="4.56" layer="21"/>
<rectangle x1="-5.264" y1="4.56" x2="-4.304" y2="4.592" layer="21"/>
<rectangle x1="-3.024" y1="4.56" x2="-2.032" y2="4.592" layer="21"/>
<rectangle x1="-0.56" y1="4.56" x2="0.496" y2="4.592" layer="21"/>
<rectangle x1="2.032" y1="4.56" x2="2.992" y2="4.592" layer="21"/>
<rectangle x1="4.272" y1="4.56" x2="5.232" y2="4.592" layer="21"/>
<rectangle x1="-5.264" y1="4.592" x2="-4.304" y2="4.624" layer="21"/>
<rectangle x1="-3.024" y1="4.592" x2="-2.032" y2="4.624" layer="21"/>
<rectangle x1="-0.56" y1="4.592" x2="0.496" y2="4.624" layer="21"/>
<rectangle x1="2.032" y1="4.592" x2="2.992" y2="4.624" layer="21"/>
<rectangle x1="4.272" y1="4.592" x2="5.232" y2="4.624" layer="21"/>
<rectangle x1="-5.264" y1="4.624" x2="-4.304" y2="4.656" layer="21"/>
<rectangle x1="-3.024" y1="4.624" x2="-2.032" y2="4.656" layer="21"/>
<rectangle x1="-0.56" y1="4.624" x2="0.496" y2="4.656" layer="21"/>
<rectangle x1="2.032" y1="4.624" x2="2.992" y2="4.656" layer="21"/>
<rectangle x1="4.272" y1="4.624" x2="5.232" y2="4.656" layer="21"/>
<rectangle x1="-5.264" y1="4.656" x2="-4.304" y2="4.688" layer="21"/>
<rectangle x1="-3.024" y1="4.656" x2="-2.032" y2="4.688" layer="21"/>
<rectangle x1="-0.56" y1="4.656" x2="0.496" y2="4.688" layer="21"/>
<rectangle x1="2.032" y1="4.656" x2="2.992" y2="4.688" layer="21"/>
<rectangle x1="4.272" y1="4.656" x2="5.232" y2="4.688" layer="21"/>
<rectangle x1="-5.264" y1="4.688" x2="-4.304" y2="4.72" layer="21"/>
<rectangle x1="-3.024" y1="4.688" x2="-2.032" y2="4.72" layer="21"/>
<rectangle x1="-0.56" y1="4.688" x2="0.496" y2="4.72" layer="21"/>
<rectangle x1="2.032" y1="4.688" x2="2.992" y2="4.72" layer="21"/>
<rectangle x1="4.272" y1="4.688" x2="5.232" y2="4.72" layer="21"/>
<rectangle x1="-5.264" y1="4.72" x2="-4.304" y2="4.752" layer="21"/>
<rectangle x1="-3.024" y1="4.72" x2="-2.032" y2="4.752" layer="21"/>
<rectangle x1="-0.56" y1="4.72" x2="0.496" y2="4.752" layer="21"/>
<rectangle x1="2.032" y1="4.72" x2="2.992" y2="4.752" layer="21"/>
<rectangle x1="4.272" y1="4.72" x2="5.232" y2="4.752" layer="21"/>
<rectangle x1="-5.264" y1="4.752" x2="-4.304" y2="4.784" layer="21"/>
<rectangle x1="-3.024" y1="4.752" x2="-2.032" y2="4.784" layer="21"/>
<rectangle x1="-0.56" y1="4.752" x2="0.496" y2="4.784" layer="21"/>
<rectangle x1="2.032" y1="4.752" x2="2.992" y2="4.784" layer="21"/>
<rectangle x1="4.272" y1="4.752" x2="5.232" y2="4.784" layer="21"/>
<rectangle x1="-5.264" y1="4.784" x2="-4.304" y2="4.816" layer="21"/>
<rectangle x1="-3.024" y1="4.784" x2="-2.032" y2="4.816" layer="21"/>
<rectangle x1="-0.56" y1="4.784" x2="0.496" y2="4.816" layer="21"/>
<rectangle x1="2.032" y1="4.784" x2="2.992" y2="4.816" layer="21"/>
<rectangle x1="4.272" y1="4.784" x2="5.232" y2="4.816" layer="21"/>
<rectangle x1="-5.264" y1="4.816" x2="-4.304" y2="4.848" layer="21"/>
<rectangle x1="-3.024" y1="4.816" x2="-2.032" y2="4.848" layer="21"/>
<rectangle x1="-0.56" y1="4.816" x2="0.496" y2="4.848" layer="21"/>
<rectangle x1="2.032" y1="4.816" x2="2.992" y2="4.848" layer="21"/>
<rectangle x1="4.272" y1="4.816" x2="5.232" y2="4.848" layer="21"/>
<rectangle x1="-5.264" y1="4.848" x2="-4.304" y2="4.88" layer="21"/>
<rectangle x1="-3.024" y1="4.848" x2="-2.032" y2="4.88" layer="21"/>
<rectangle x1="-0.56" y1="4.848" x2="0.496" y2="4.88" layer="21"/>
<rectangle x1="2.032" y1="4.848" x2="2.992" y2="4.88" layer="21"/>
<rectangle x1="4.272" y1="4.848" x2="5.232" y2="4.88" layer="21"/>
<rectangle x1="-5.264" y1="4.88" x2="-4.304" y2="4.912" layer="21"/>
<rectangle x1="-3.024" y1="4.88" x2="-2.032" y2="4.912" layer="21"/>
<rectangle x1="-0.56" y1="4.88" x2="0.496" y2="4.912" layer="21"/>
<rectangle x1="2.032" y1="4.88" x2="2.992" y2="4.912" layer="21"/>
<rectangle x1="4.272" y1="4.88" x2="5.232" y2="4.912" layer="21"/>
<rectangle x1="-5.264" y1="4.912" x2="-4.304" y2="4.944" layer="21"/>
<rectangle x1="-3.024" y1="4.912" x2="-2.032" y2="4.944" layer="21"/>
<rectangle x1="-0.56" y1="4.912" x2="0.496" y2="4.944" layer="21"/>
<rectangle x1="2.032" y1="4.912" x2="2.992" y2="4.944" layer="21"/>
<rectangle x1="4.272" y1="4.912" x2="5.232" y2="4.944" layer="21"/>
<rectangle x1="-5.264" y1="4.944" x2="-4.304" y2="4.976" layer="21"/>
<rectangle x1="-3.024" y1="4.944" x2="-2.032" y2="4.976" layer="21"/>
<rectangle x1="-0.56" y1="4.944" x2="0.496" y2="4.976" layer="21"/>
<rectangle x1="2.032" y1="4.944" x2="2.992" y2="4.976" layer="21"/>
<rectangle x1="4.272" y1="4.944" x2="5.232" y2="4.976" layer="21"/>
<rectangle x1="-5.264" y1="4.976" x2="-4.304" y2="5.008" layer="21"/>
<rectangle x1="-3.024" y1="4.976" x2="-2.032" y2="5.008" layer="21"/>
<rectangle x1="-0.56" y1="4.976" x2="0.496" y2="5.008" layer="21"/>
<rectangle x1="2.032" y1="4.976" x2="2.992" y2="5.008" layer="21"/>
<rectangle x1="4.272" y1="4.976" x2="5.232" y2="5.008" layer="21"/>
<rectangle x1="-5.264" y1="5.008" x2="-4.304" y2="5.04" layer="21"/>
<rectangle x1="-3.024" y1="5.008" x2="-2.064" y2="5.04" layer="21"/>
<rectangle x1="-0.56" y1="5.008" x2="0.496" y2="5.04" layer="21"/>
<rectangle x1="2.032" y1="5.008" x2="2.992" y2="5.04" layer="21"/>
<rectangle x1="4.272" y1="5.008" x2="5.232" y2="5.04" layer="21"/>
<rectangle x1="-5.264" y1="5.04" x2="-4.304" y2="5.072" layer="21"/>
<rectangle x1="-3.024" y1="5.04" x2="-2.064" y2="5.072" layer="21"/>
<rectangle x1="-0.56" y1="5.04" x2="0.496" y2="5.072" layer="21"/>
<rectangle x1="2.032" y1="5.04" x2="2.992" y2="5.072" layer="21"/>
<rectangle x1="4.272" y1="5.04" x2="5.232" y2="5.072" layer="21"/>
<rectangle x1="-5.264" y1="5.072" x2="-4.304" y2="5.104" layer="21"/>
<rectangle x1="-3.056" y1="5.072" x2="-2.064" y2="5.104" layer="21"/>
<rectangle x1="-0.56" y1="5.072" x2="0.496" y2="5.104" layer="21"/>
<rectangle x1="2.032" y1="5.072" x2="2.992" y2="5.104" layer="21"/>
<rectangle x1="4.272" y1="5.072" x2="5.232" y2="5.104" layer="21"/>
<rectangle x1="-5.264" y1="5.104" x2="-4.304" y2="5.136" layer="21"/>
<rectangle x1="-3.056" y1="5.104" x2="-2.064" y2="5.136" layer="21"/>
<rectangle x1="-0.56" y1="5.104" x2="0.496" y2="5.136" layer="21"/>
<rectangle x1="2.032" y1="5.104" x2="2.992" y2="5.136" layer="21"/>
<rectangle x1="4.272" y1="5.104" x2="5.232" y2="5.136" layer="21"/>
<rectangle x1="-5.264" y1="5.136" x2="-4.304" y2="5.168" layer="21"/>
<rectangle x1="-3.056" y1="5.136" x2="-2.064" y2="5.168" layer="21"/>
<rectangle x1="-0.56" y1="5.136" x2="0.496" y2="5.168" layer="21"/>
<rectangle x1="2.032" y1="5.136" x2="2.992" y2="5.168" layer="21"/>
<rectangle x1="4.272" y1="5.136" x2="5.232" y2="5.168" layer="21"/>
<rectangle x1="-5.264" y1="5.168" x2="-4.304" y2="5.2" layer="21"/>
<rectangle x1="-3.056" y1="5.168" x2="-2.064" y2="5.2" layer="21"/>
<rectangle x1="-0.56" y1="5.168" x2="0.496" y2="5.2" layer="21"/>
<rectangle x1="2.032" y1="5.168" x2="2.992" y2="5.2" layer="21"/>
<rectangle x1="4.272" y1="5.168" x2="5.232" y2="5.2" layer="21"/>
<rectangle x1="-5.264" y1="5.2" x2="-4.304" y2="5.232" layer="21"/>
<rectangle x1="-3.056" y1="5.2" x2="-2.064" y2="5.232" layer="21"/>
<rectangle x1="-0.56" y1="5.2" x2="0.496" y2="5.232" layer="21"/>
<rectangle x1="2.032" y1="5.2" x2="2.992" y2="5.232" layer="21"/>
<rectangle x1="4.272" y1="5.2" x2="5.232" y2="5.232" layer="21"/>
<rectangle x1="-5.264" y1="5.232" x2="-4.304" y2="5.264" layer="21"/>
<rectangle x1="-3.056" y1="5.232" x2="-2.064" y2="5.264" layer="21"/>
<rectangle x1="-0.56" y1="5.232" x2="0.496" y2="5.264" layer="21"/>
<rectangle x1="2.032" y1="5.232" x2="2.992" y2="5.264" layer="21"/>
<rectangle x1="4.272" y1="5.232" x2="5.232" y2="5.264" layer="21"/>
<rectangle x1="-5.264" y1="5.264" x2="-4.304" y2="5.296" layer="21"/>
<rectangle x1="-3.056" y1="5.264" x2="-2.064" y2="5.296" layer="21"/>
<rectangle x1="-0.56" y1="5.264" x2="0.496" y2="5.296" layer="21"/>
<rectangle x1="2.032" y1="5.264" x2="2.992" y2="5.296" layer="21"/>
<rectangle x1="4.272" y1="5.264" x2="5.232" y2="5.296" layer="21"/>
<rectangle x1="-5.264" y1="5.296" x2="-4.304" y2="5.328" layer="21"/>
<rectangle x1="-3.056" y1="5.296" x2="-2.064" y2="5.328" layer="21"/>
<rectangle x1="-0.56" y1="5.296" x2="0.496" y2="5.328" layer="21"/>
<rectangle x1="2.032" y1="5.296" x2="2.992" y2="5.328" layer="21"/>
<rectangle x1="4.272" y1="5.296" x2="5.232" y2="5.328" layer="21"/>
<rectangle x1="-5.264" y1="5.328" x2="-4.304" y2="5.36" layer="21"/>
<rectangle x1="-3.056" y1="5.328" x2="-2.064" y2="5.36" layer="21"/>
<rectangle x1="-0.56" y1="5.328" x2="0.496" y2="5.36" layer="21"/>
<rectangle x1="2.032" y1="5.328" x2="2.992" y2="5.36" layer="21"/>
<rectangle x1="4.272" y1="5.328" x2="5.232" y2="5.36" layer="21"/>
<rectangle x1="-5.264" y1="5.36" x2="-4.304" y2="5.392" layer="21"/>
<rectangle x1="-3.056" y1="5.36" x2="-2.064" y2="5.392" layer="21"/>
<rectangle x1="-0.56" y1="5.36" x2="0.496" y2="5.392" layer="21"/>
<rectangle x1="2.032" y1="5.36" x2="2.992" y2="5.392" layer="21"/>
<rectangle x1="4.272" y1="5.36" x2="5.232" y2="5.392" layer="21"/>
<rectangle x1="-5.264" y1="5.392" x2="-4.304" y2="5.424" layer="21"/>
<rectangle x1="-3.056" y1="5.392" x2="-2.064" y2="5.424" layer="21"/>
<rectangle x1="-0.56" y1="5.392" x2="0.496" y2="5.424" layer="21"/>
<rectangle x1="2.032" y1="5.392" x2="2.992" y2="5.424" layer="21"/>
<rectangle x1="4.272" y1="5.392" x2="5.232" y2="5.424" layer="21"/>
<rectangle x1="-5.264" y1="5.424" x2="-4.304" y2="5.456" layer="21"/>
<rectangle x1="-3.088" y1="5.424" x2="-2.064" y2="5.456" layer="21"/>
<rectangle x1="-0.56" y1="5.424" x2="0.496" y2="5.456" layer="21"/>
<rectangle x1="2.032" y1="5.424" x2="2.992" y2="5.456" layer="21"/>
<rectangle x1="4.272" y1="5.424" x2="5.232" y2="5.456" layer="21"/>
<rectangle x1="-5.264" y1="5.456" x2="-4.304" y2="5.488" layer="21"/>
<rectangle x1="-3.088" y1="5.456" x2="-2.096" y2="5.488" layer="21"/>
<rectangle x1="-0.56" y1="5.456" x2="0.496" y2="5.488" layer="21"/>
<rectangle x1="2.032" y1="5.456" x2="2.992" y2="5.488" layer="21"/>
<rectangle x1="4.272" y1="5.456" x2="5.232" y2="5.488" layer="21"/>
<rectangle x1="-5.264" y1="5.488" x2="-4.304" y2="5.52" layer="21"/>
<rectangle x1="-3.088" y1="5.488" x2="-2.096" y2="5.52" layer="21"/>
<rectangle x1="-0.56" y1="5.488" x2="0.496" y2="5.52" layer="21"/>
<rectangle x1="2.032" y1="5.488" x2="2.992" y2="5.52" layer="21"/>
<rectangle x1="4.272" y1="5.488" x2="5.232" y2="5.52" layer="21"/>
<rectangle x1="-5.264" y1="5.52" x2="-4.304" y2="5.552" layer="21"/>
<rectangle x1="-3.088" y1="5.52" x2="-2.096" y2="5.552" layer="21"/>
<rectangle x1="-0.56" y1="5.52" x2="0.496" y2="5.552" layer="21"/>
<rectangle x1="2.032" y1="5.52" x2="2.992" y2="5.552" layer="21"/>
<rectangle x1="4.272" y1="5.52" x2="5.232" y2="5.552" layer="21"/>
<rectangle x1="-5.264" y1="5.552" x2="-4.304" y2="5.584" layer="21"/>
<rectangle x1="-3.088" y1="5.552" x2="-2.096" y2="5.584" layer="21"/>
<rectangle x1="-0.56" y1="5.552" x2="0.496" y2="5.584" layer="21"/>
<rectangle x1="2.032" y1="5.552" x2="2.992" y2="5.584" layer="21"/>
<rectangle x1="4.272" y1="5.552" x2="5.232" y2="5.584" layer="21"/>
<rectangle x1="-5.264" y1="5.584" x2="-4.304" y2="5.616" layer="21"/>
<rectangle x1="-3.12" y1="5.584" x2="-2.096" y2="5.616" layer="21"/>
<rectangle x1="-0.56" y1="5.584" x2="0.496" y2="5.616" layer="21"/>
<rectangle x1="2.032" y1="5.584" x2="2.992" y2="5.616" layer="21"/>
<rectangle x1="4.272" y1="5.584" x2="5.232" y2="5.616" layer="21"/>
<rectangle x1="-5.264" y1="5.616" x2="-4.304" y2="5.648" layer="21"/>
<rectangle x1="-3.12" y1="5.616" x2="-2.096" y2="5.648" layer="21"/>
<rectangle x1="-0.56" y1="5.616" x2="0.496" y2="5.648" layer="21"/>
<rectangle x1="2.032" y1="5.616" x2="2.992" y2="5.648" layer="21"/>
<rectangle x1="4.272" y1="5.616" x2="5.232" y2="5.648" layer="21"/>
<rectangle x1="-5.264" y1="5.648" x2="-4.304" y2="5.68" layer="21"/>
<rectangle x1="-3.12" y1="5.648" x2="-2.096" y2="5.68" layer="21"/>
<rectangle x1="-0.56" y1="5.648" x2="0.496" y2="5.68" layer="21"/>
<rectangle x1="2.032" y1="5.648" x2="2.992" y2="5.68" layer="21"/>
<rectangle x1="4.272" y1="5.648" x2="5.232" y2="5.68" layer="21"/>
<rectangle x1="-5.264" y1="5.68" x2="-4.304" y2="5.712" layer="21"/>
<rectangle x1="-3.152" y1="5.68" x2="-2.128" y2="5.712" layer="21"/>
<rectangle x1="-0.56" y1="5.68" x2="0.496" y2="5.712" layer="21"/>
<rectangle x1="2.032" y1="5.68" x2="2.992" y2="5.712" layer="21"/>
<rectangle x1="4.272" y1="5.68" x2="5.232" y2="5.712" layer="21"/>
<rectangle x1="-5.264" y1="5.712" x2="-4.304" y2="5.744" layer="21"/>
<rectangle x1="-3.152" y1="5.712" x2="-2.128" y2="5.744" layer="21"/>
<rectangle x1="-0.56" y1="5.712" x2="0.496" y2="5.744" layer="21"/>
<rectangle x1="2.032" y1="5.712" x2="2.992" y2="5.744" layer="21"/>
<rectangle x1="4.272" y1="5.712" x2="5.232" y2="5.744" layer="21"/>
<rectangle x1="-5.264" y1="5.744" x2="-4.304" y2="5.776" layer="21"/>
<rectangle x1="-3.184" y1="5.744" x2="-2.128" y2="5.776" layer="21"/>
<rectangle x1="-0.56" y1="5.744" x2="0.496" y2="5.776" layer="21"/>
<rectangle x1="2.032" y1="5.744" x2="2.992" y2="5.776" layer="21"/>
<rectangle x1="4.272" y1="5.744" x2="5.232" y2="5.776" layer="21"/>
<rectangle x1="-5.264" y1="5.776" x2="-4.304" y2="5.808" layer="21"/>
<rectangle x1="-3.184" y1="5.776" x2="-2.128" y2="5.808" layer="21"/>
<rectangle x1="-0.56" y1="5.776" x2="0.496" y2="5.808" layer="21"/>
<rectangle x1="2.032" y1="5.776" x2="2.992" y2="5.808" layer="21"/>
<rectangle x1="4.272" y1="5.776" x2="5.232" y2="5.808" layer="21"/>
<rectangle x1="-5.264" y1="5.808" x2="-4.304" y2="5.84" layer="21"/>
<rectangle x1="-3.216" y1="5.808" x2="-2.16" y2="5.84" layer="21"/>
<rectangle x1="-0.56" y1="5.808" x2="0.496" y2="5.84" layer="21"/>
<rectangle x1="2.032" y1="5.808" x2="2.992" y2="5.84" layer="21"/>
<rectangle x1="4.272" y1="5.808" x2="5.232" y2="5.84" layer="21"/>
<rectangle x1="-5.264" y1="5.84" x2="-4.304" y2="5.872" layer="21"/>
<rectangle x1="-3.248" y1="5.84" x2="-2.16" y2="5.872" layer="21"/>
<rectangle x1="-0.56" y1="5.84" x2="0.496" y2="5.872" layer="21"/>
<rectangle x1="2.032" y1="5.84" x2="2.992" y2="5.872" layer="21"/>
<rectangle x1="4.272" y1="5.84" x2="5.232" y2="5.872" layer="21"/>
<rectangle x1="-5.264" y1="5.872" x2="-4.304" y2="5.904" layer="21"/>
<rectangle x1="-3.28" y1="5.872" x2="-2.16" y2="5.904" layer="21"/>
<rectangle x1="-0.56" y1="5.872" x2="0.496" y2="5.904" layer="21"/>
<rectangle x1="2.032" y1="5.872" x2="2.992" y2="5.904" layer="21"/>
<rectangle x1="4.272" y1="5.872" x2="5.232" y2="5.904" layer="21"/>
<rectangle x1="-5.264" y1="5.904" x2="-4.304" y2="5.936" layer="21"/>
<rectangle x1="-3.312" y1="5.904" x2="-2.16" y2="5.936" layer="21"/>
<rectangle x1="-0.56" y1="5.904" x2="0.496" y2="5.936" layer="21"/>
<rectangle x1="2.032" y1="5.904" x2="2.992" y2="5.936" layer="21"/>
<rectangle x1="4.272" y1="5.904" x2="5.232" y2="5.936" layer="21"/>
<rectangle x1="-5.264" y1="5.936" x2="-4.304" y2="5.968" layer="21"/>
<rectangle x1="-3.376" y1="5.936" x2="-2.192" y2="5.968" layer="21"/>
<rectangle x1="-0.56" y1="5.936" x2="0.528" y2="5.968" layer="21"/>
<rectangle x1="2.032" y1="5.936" x2="2.992" y2="5.968" layer="21"/>
<rectangle x1="4.272" y1="5.936" x2="5.232" y2="5.968" layer="21"/>
<rectangle x1="-5.264" y1="5.968" x2="-4.304" y2="6" layer="21"/>
<rectangle x1="-3.44" y1="5.968" x2="-2.192" y2="6" layer="21"/>
<rectangle x1="-1.584" y1="5.968" x2="1.552" y2="6" layer="21"/>
<rectangle x1="2.032" y1="5.968" x2="2.992" y2="6" layer="21"/>
<rectangle x1="4.272" y1="5.968" x2="5.232" y2="6" layer="21"/>
<rectangle x1="-5.264" y1="6" x2="-2.224" y2="6.032" layer="21"/>
<rectangle x1="-1.616" y1="6" x2="1.552" y2="6.032" layer="21"/>
<rectangle x1="2.032" y1="6" x2="2.992" y2="6.032" layer="21"/>
<rectangle x1="4.272" y1="6" x2="5.232" y2="6.032" layer="21"/>
<rectangle x1="-5.264" y1="6.032" x2="-2.224" y2="6.064" layer="21"/>
<rectangle x1="-1.616" y1="6.032" x2="1.584" y2="6.064" layer="21"/>
<rectangle x1="2.032" y1="6.032" x2="2.992" y2="6.064" layer="21"/>
<rectangle x1="4.272" y1="6.032" x2="5.232" y2="6.064" layer="21"/>
<rectangle x1="-5.264" y1="6.064" x2="-2.224" y2="6.096" layer="21"/>
<rectangle x1="-1.616" y1="6.064" x2="1.584" y2="6.096" layer="21"/>
<rectangle x1="2.032" y1="6.064" x2="2.992" y2="6.096" layer="21"/>
<rectangle x1="4.272" y1="6.064" x2="5.232" y2="6.096" layer="21"/>
<rectangle x1="-5.264" y1="6.096" x2="-2.256" y2="6.128" layer="21"/>
<rectangle x1="-1.616" y1="6.096" x2="1.584" y2="6.128" layer="21"/>
<rectangle x1="2.032" y1="6.096" x2="2.992" y2="6.128" layer="21"/>
<rectangle x1="4.272" y1="6.096" x2="5.232" y2="6.128" layer="21"/>
<rectangle x1="-5.264" y1="6.128" x2="-2.256" y2="6.16" layer="21"/>
<rectangle x1="-1.616" y1="6.128" x2="1.584" y2="6.16" layer="21"/>
<rectangle x1="2.032" y1="6.128" x2="2.992" y2="6.16" layer="21"/>
<rectangle x1="4.272" y1="6.128" x2="5.232" y2="6.16" layer="21"/>
<rectangle x1="-5.264" y1="6.16" x2="-2.288" y2="6.192" layer="21"/>
<rectangle x1="-1.616" y1="6.16" x2="1.584" y2="6.192" layer="21"/>
<rectangle x1="2.032" y1="6.16" x2="2.992" y2="6.192" layer="21"/>
<rectangle x1="4.272" y1="6.16" x2="5.232" y2="6.192" layer="21"/>
<rectangle x1="-5.264" y1="6.192" x2="-2.32" y2="6.224" layer="21"/>
<rectangle x1="-1.616" y1="6.192" x2="1.584" y2="6.224" layer="21"/>
<rectangle x1="2.032" y1="6.192" x2="2.992" y2="6.224" layer="21"/>
<rectangle x1="4.272" y1="6.192" x2="5.232" y2="6.224" layer="21"/>
<rectangle x1="-5.264" y1="6.224" x2="-2.32" y2="6.256" layer="21"/>
<rectangle x1="-1.616" y1="6.224" x2="1.584" y2="6.256" layer="21"/>
<rectangle x1="2.032" y1="6.224" x2="2.992" y2="6.256" layer="21"/>
<rectangle x1="4.272" y1="6.224" x2="5.232" y2="6.256" layer="21"/>
<rectangle x1="-5.264" y1="6.256" x2="-2.352" y2="6.288" layer="21"/>
<rectangle x1="-1.616" y1="6.256" x2="1.584" y2="6.288" layer="21"/>
<rectangle x1="2.032" y1="6.256" x2="2.992" y2="6.288" layer="21"/>
<rectangle x1="4.272" y1="6.256" x2="5.232" y2="6.288" layer="21"/>
<rectangle x1="-5.264" y1="6.288" x2="-2.384" y2="6.32" layer="21"/>
<rectangle x1="-1.616" y1="6.288" x2="1.584" y2="6.32" layer="21"/>
<rectangle x1="2.032" y1="6.288" x2="2.992" y2="6.32" layer="21"/>
<rectangle x1="4.272" y1="6.288" x2="5.232" y2="6.32" layer="21"/>
<rectangle x1="-5.264" y1="6.32" x2="-2.416" y2="6.352" layer="21"/>
<rectangle x1="-1.616" y1="6.32" x2="1.584" y2="6.352" layer="21"/>
<rectangle x1="2.032" y1="6.32" x2="2.992" y2="6.352" layer="21"/>
<rectangle x1="4.272" y1="6.32" x2="5.232" y2="6.352" layer="21"/>
<rectangle x1="-5.264" y1="6.352" x2="-2.416" y2="6.384" layer="21"/>
<rectangle x1="-1.616" y1="6.352" x2="1.584" y2="6.384" layer="21"/>
<rectangle x1="2.032" y1="6.352" x2="2.992" y2="6.384" layer="21"/>
<rectangle x1="4.272" y1="6.352" x2="5.232" y2="6.384" layer="21"/>
<rectangle x1="-5.264" y1="6.384" x2="-2.448" y2="6.416" layer="21"/>
<rectangle x1="-1.616" y1="6.384" x2="1.584" y2="6.416" layer="21"/>
<rectangle x1="2.032" y1="6.384" x2="2.992" y2="6.416" layer="21"/>
<rectangle x1="4.272" y1="6.384" x2="5.232" y2="6.416" layer="21"/>
<rectangle x1="-5.264" y1="6.416" x2="-2.48" y2="6.448" layer="21"/>
<rectangle x1="-1.616" y1="6.416" x2="1.584" y2="6.448" layer="21"/>
<rectangle x1="2.032" y1="6.416" x2="2.992" y2="6.448" layer="21"/>
<rectangle x1="4.272" y1="6.416" x2="5.232" y2="6.448" layer="21"/>
<rectangle x1="-5.264" y1="6.448" x2="-2.544" y2="6.48" layer="21"/>
<rectangle x1="-1.616" y1="6.448" x2="1.584" y2="6.48" layer="21"/>
<rectangle x1="2.032" y1="6.448" x2="2.992" y2="6.48" layer="21"/>
<rectangle x1="4.272" y1="6.448" x2="5.232" y2="6.48" layer="21"/>
<rectangle x1="-5.264" y1="6.48" x2="-2.576" y2="6.512" layer="21"/>
<rectangle x1="-1.616" y1="6.48" x2="1.584" y2="6.512" layer="21"/>
<rectangle x1="2.032" y1="6.48" x2="2.992" y2="6.512" layer="21"/>
<rectangle x1="4.272" y1="6.48" x2="5.232" y2="6.512" layer="21"/>
<rectangle x1="-5.264" y1="6.512" x2="-2.608" y2="6.544" layer="21"/>
<rectangle x1="-1.616" y1="6.512" x2="1.584" y2="6.544" layer="21"/>
<rectangle x1="2.032" y1="6.512" x2="2.992" y2="6.544" layer="21"/>
<rectangle x1="4.272" y1="6.512" x2="5.232" y2="6.544" layer="21"/>
<rectangle x1="-5.264" y1="6.544" x2="-2.672" y2="6.576" layer="21"/>
<rectangle x1="-1.616" y1="6.544" x2="1.584" y2="6.576" layer="21"/>
<rectangle x1="2.032" y1="6.544" x2="2.992" y2="6.576" layer="21"/>
<rectangle x1="4.272" y1="6.544" x2="5.232" y2="6.576" layer="21"/>
<rectangle x1="-5.264" y1="6.576" x2="-2.736" y2="6.608" layer="21"/>
<rectangle x1="-1.616" y1="6.576" x2="1.584" y2="6.608" layer="21"/>
<rectangle x1="2.032" y1="6.576" x2="2.992" y2="6.608" layer="21"/>
<rectangle x1="4.272" y1="6.576" x2="5.232" y2="6.608" layer="21"/>
<rectangle x1="-5.264" y1="6.608" x2="-2.832" y2="6.64" layer="21"/>
<rectangle x1="-1.616" y1="6.608" x2="1.584" y2="6.64" layer="21"/>
<rectangle x1="2.032" y1="6.608" x2="2.992" y2="6.64" layer="21"/>
<rectangle x1="4.272" y1="6.608" x2="5.232" y2="6.64" layer="21"/>
<rectangle x1="-5.264" y1="6.64" x2="-2.928" y2="6.672" layer="21"/>
<rectangle x1="-1.616" y1="6.64" x2="1.584" y2="6.672" layer="21"/>
<rectangle x1="2.032" y1="6.64" x2="2.992" y2="6.672" layer="21"/>
<rectangle x1="4.272" y1="6.64" x2="5.232" y2="6.672" layer="21"/>
<rectangle x1="-5.264" y1="6.672" x2="-3.056" y2="6.704" layer="21"/>
<rectangle x1="-1.584" y1="6.672" x2="1.552" y2="6.704" layer="21"/>
<rectangle x1="2.032" y1="6.672" x2="2.96" y2="6.704" layer="21"/>
<rectangle x1="4.272" y1="6.672" x2="5.232" y2="6.704" layer="21"/>
<rectangle x1="-5.2" y1="6.704" x2="-3.312" y2="6.736" layer="21"/>
<rectangle x1="-1.552" y1="6.704" x2="1.52" y2="6.736" layer="21"/>
<rectangle x1="2.096" y1="6.704" x2="2.928" y2="6.736" layer="21"/>
<rectangle x1="4.336" y1="6.704" x2="5.168" y2="6.736" layer="21"/>
</package>
<package name="DTU-MEDIUM-9MM">
<rectangle x1="-2.4725" y1="-6.9115" x2="-2.4495" y2="-6.8885" layer="21"/>
<rectangle x1="2.4035" y1="-6.9115" x2="2.4265" y2="-6.8885" layer="21"/>
<rectangle x1="-2.5185" y1="-6.8885" x2="-2.3805" y2="-6.8655" layer="21"/>
<rectangle x1="2.3345" y1="-6.8885" x2="2.4725" y2="-6.8655" layer="21"/>
<rectangle x1="-2.5645" y1="-6.8655" x2="-2.3345" y2="-6.8425" layer="21"/>
<rectangle x1="2.2885" y1="-6.8655" x2="2.5185" y2="-6.8425" layer="21"/>
<rectangle x1="-2.6105" y1="-6.8425" x2="-2.2655" y2="-6.8195" layer="21"/>
<rectangle x1="2.2195" y1="-6.8425" x2="2.5645" y2="-6.8195" layer="21"/>
<rectangle x1="-2.6565" y1="-6.8195" x2="-2.2195" y2="-6.7965" layer="21"/>
<rectangle x1="2.1735" y1="-6.8195" x2="2.6105" y2="-6.7965" layer="21"/>
<rectangle x1="-2.7025" y1="-6.7965" x2="-2.1505" y2="-6.7735" layer="21"/>
<rectangle x1="2.1045" y1="-6.7965" x2="2.6565" y2="-6.7735" layer="21"/>
<rectangle x1="-2.7255" y1="-6.7735" x2="-2.1045" y2="-6.7505" layer="21"/>
<rectangle x1="2.0585" y1="-6.7735" x2="2.6795" y2="-6.7505" layer="21"/>
<rectangle x1="-2.7715" y1="-6.7505" x2="-2.0355" y2="-6.7275" layer="21"/>
<rectangle x1="1.9895" y1="-6.7505" x2="2.7255" y2="-6.7275" layer="21"/>
<rectangle x1="-2.8175" y1="-6.7275" x2="-1.9665" y2="-6.7045" layer="21"/>
<rectangle x1="1.9205" y1="-6.7275" x2="2.7715" y2="-6.7045" layer="21"/>
<rectangle x1="-2.8635" y1="-6.7045" x2="-1.9205" y2="-6.6815" layer="21"/>
<rectangle x1="1.8745" y1="-6.7045" x2="2.8175" y2="-6.6815" layer="21"/>
<rectangle x1="-2.9095" y1="-6.6815" x2="-1.8515" y2="-6.6585" layer="21"/>
<rectangle x1="1.8055" y1="-6.6815" x2="2.8635" y2="-6.6585" layer="21"/>
<rectangle x1="-2.9555" y1="-6.6585" x2="-1.7825" y2="-6.6355" layer="21"/>
<rectangle x1="1.7365" y1="-6.6585" x2="2.9095" y2="-6.6355" layer="21"/>
<rectangle x1="-3.0015" y1="-6.6355" x2="-1.7135" y2="-6.6125" layer="21"/>
<rectangle x1="1.6675" y1="-6.6355" x2="2.9555" y2="-6.6125" layer="21"/>
<rectangle x1="-3.0475" y1="-6.6125" x2="-1.6675" y2="-6.5895" layer="21"/>
<rectangle x1="1.6215" y1="-6.6125" x2="3.0015" y2="-6.5895" layer="21"/>
<rectangle x1="-3.0935" y1="-6.5895" x2="-1.5985" y2="-6.5665" layer="21"/>
<rectangle x1="1.5525" y1="-6.5895" x2="3.0475" y2="-6.5665" layer="21"/>
<rectangle x1="-3.1395" y1="-6.5665" x2="-1.5295" y2="-6.5435" layer="21"/>
<rectangle x1="1.4835" y1="-6.5665" x2="3.0935" y2="-6.5435" layer="21"/>
<rectangle x1="-3.1855" y1="-6.5435" x2="-1.4605" y2="-6.5205" layer="21"/>
<rectangle x1="1.4145" y1="-6.5435" x2="3.1395" y2="-6.5205" layer="21"/>
<rectangle x1="-3.2085" y1="-6.5205" x2="-1.3915" y2="-6.4975" layer="21"/>
<rectangle x1="1.3455" y1="-6.5205" x2="3.1625" y2="-6.4975" layer="21"/>
<rectangle x1="-3.2545" y1="-6.4975" x2="-1.3455" y2="-6.4745" layer="21"/>
<rectangle x1="1.2995" y1="-6.4975" x2="3.2085" y2="-6.4745" layer="21"/>
<rectangle x1="-3.3005" y1="-6.4745" x2="-1.2765" y2="-6.4515" layer="21"/>
<rectangle x1="1.2305" y1="-6.4745" x2="3.2545" y2="-6.4515" layer="21"/>
<rectangle x1="-3.3465" y1="-6.4515" x2="-1.2075" y2="-6.4285" layer="21"/>
<rectangle x1="1.1615" y1="-6.4515" x2="3.3005" y2="-6.4285" layer="21"/>
<rectangle x1="-3.3925" y1="-6.4285" x2="-1.1155" y2="-6.4055" layer="21"/>
<rectangle x1="1.0695" y1="-6.4285" x2="3.3465" y2="-6.4055" layer="21"/>
<rectangle x1="-3.4385" y1="-6.4055" x2="-1.0465" y2="-6.3825" layer="21"/>
<rectangle x1="1.0005" y1="-6.4055" x2="3.3925" y2="-6.3825" layer="21"/>
<rectangle x1="-3.4845" y1="-6.3825" x2="-0.9775" y2="-6.3595" layer="21"/>
<rectangle x1="0.9315" y1="-6.3825" x2="3.4385" y2="-6.3595" layer="21"/>
<rectangle x1="-3.5305" y1="-6.3595" x2="-0.8855" y2="-6.3365" layer="21"/>
<rectangle x1="0.8395" y1="-6.3595" x2="3.4845" y2="-6.3365" layer="21"/>
<rectangle x1="-3.5765" y1="-6.3365" x2="-0.7935" y2="-6.3135" layer="21"/>
<rectangle x1="0.7475" y1="-6.3365" x2="3.5305" y2="-6.3135" layer="21"/>
<rectangle x1="-3.6225" y1="-6.3135" x2="-0.6785" y2="-6.2905" layer="21"/>
<rectangle x1="0.6325" y1="-6.3135" x2="3.5765" y2="-6.2905" layer="21"/>
<rectangle x1="-3.6685" y1="-6.2905" x2="-0.5865" y2="-6.2675" layer="21"/>
<rectangle x1="0.5405" y1="-6.2905" x2="3.6225" y2="-6.2675" layer="21"/>
<rectangle x1="-3.6915" y1="-6.2675" x2="-0.4485" y2="-6.2445" layer="21"/>
<rectangle x1="0.4025" y1="-6.2675" x2="3.6685" y2="-6.2445" layer="21"/>
<rectangle x1="-3.7375" y1="-6.2445" x2="-0.2415" y2="-6.2215" layer="21"/>
<rectangle x1="0.1955" y1="-6.2445" x2="3.6915" y2="-6.2215" layer="21"/>
<rectangle x1="-3.7835" y1="-6.2215" x2="3.7375" y2="-6.1985" layer="21"/>
<rectangle x1="-3.8295" y1="-6.1985" x2="3.7835" y2="-6.1755" layer="21"/>
<rectangle x1="-3.8755" y1="-6.1755" x2="3.8295" y2="-6.1525" layer="21"/>
<rectangle x1="-3.9215" y1="-6.1525" x2="3.8755" y2="-6.1295" layer="21"/>
<rectangle x1="-3.9675" y1="-6.1295" x2="3.9215" y2="-6.1065" layer="21"/>
<rectangle x1="-4.0135" y1="-6.1065" x2="3.9675" y2="-6.0835" layer="21"/>
<rectangle x1="-4.0595" y1="-6.0835" x2="4.0135" y2="-6.0605" layer="21"/>
<rectangle x1="-4.0825" y1="-6.0605" x2="4.0595" y2="-6.0375" layer="21"/>
<rectangle x1="-4.1285" y1="-6.0375" x2="4.0825" y2="-6.0145" layer="21"/>
<rectangle x1="-4.1745" y1="-6.0145" x2="4.1285" y2="-5.9915" layer="21"/>
<rectangle x1="-4.2205" y1="-5.9915" x2="4.1745" y2="-5.9685" layer="21"/>
<rectangle x1="-4.2435" y1="-5.9685" x2="4.1975" y2="-5.9455" layer="21"/>
<rectangle x1="-4.2205" y1="-5.9455" x2="4.1745" y2="-5.9225" layer="21"/>
<rectangle x1="-4.1745" y1="-5.9225" x2="4.1285" y2="-5.8995" layer="21"/>
<rectangle x1="-4.1285" y1="-5.8995" x2="4.0825" y2="-5.8765" layer="21"/>
<rectangle x1="-4.0825" y1="-5.8765" x2="4.0365" y2="-5.8535" layer="21"/>
<rectangle x1="-4.0365" y1="-5.8535" x2="3.9905" y2="-5.8305" layer="21"/>
<rectangle x1="-3.9905" y1="-5.8305" x2="3.9445" y2="-5.8075" layer="21"/>
<rectangle x1="-3.9445" y1="-5.8075" x2="3.8985" y2="-5.7845" layer="21"/>
<rectangle x1="-3.8985" y1="-5.7845" x2="3.8525" y2="-5.7615" layer="21"/>
<rectangle x1="-3.8525" y1="-5.7615" x2="3.8065" y2="-5.7385" layer="21"/>
<rectangle x1="-3.8065" y1="-5.7385" x2="3.7605" y2="-5.7155" layer="21"/>
<rectangle x1="-3.7605" y1="-5.7155" x2="3.7145" y2="-5.6925" layer="21"/>
<rectangle x1="-3.7145" y1="-5.6925" x2="-0.3565" y2="-5.6695" layer="21"/>
<rectangle x1="0.3105" y1="-5.6925" x2="3.6915" y2="-5.6695" layer="21"/>
<rectangle x1="-3.6915" y1="-5.6695" x2="-0.5175" y2="-5.6465" layer="21"/>
<rectangle x1="0.4715" y1="-5.6695" x2="3.6455" y2="-5.6465" layer="21"/>
<rectangle x1="-3.6455" y1="-5.6465" x2="-0.6325" y2="-5.6235" layer="21"/>
<rectangle x1="0.5865" y1="-5.6465" x2="3.5995" y2="-5.6235" layer="21"/>
<rectangle x1="-3.5995" y1="-5.6235" x2="-0.7245" y2="-5.6005" layer="21"/>
<rectangle x1="0.7015" y1="-5.6235" x2="3.5535" y2="-5.6005" layer="21"/>
<rectangle x1="-3.5535" y1="-5.6005" x2="-0.8165" y2="-5.5775" layer="21"/>
<rectangle x1="0.7935" y1="-5.6005" x2="3.5075" y2="-5.5775" layer="21"/>
<rectangle x1="-3.5075" y1="-5.5775" x2="-0.9085" y2="-5.5545" layer="21"/>
<rectangle x1="0.8625" y1="-5.5775" x2="3.4615" y2="-5.5545" layer="21"/>
<rectangle x1="-3.4615" y1="-5.5545" x2="-1.0005" y2="-5.5315" layer="21"/>
<rectangle x1="0.9545" y1="-5.5545" x2="3.4155" y2="-5.5315" layer="21"/>
<rectangle x1="-3.4155" y1="-5.5315" x2="-1.0695" y2="-5.5085" layer="21"/>
<rectangle x1="1.0235" y1="-5.5315" x2="3.3695" y2="-5.5085" layer="21"/>
<rectangle x1="-3.3695" y1="-5.5085" x2="-1.1615" y2="-5.4855" layer="21"/>
<rectangle x1="1.1155" y1="-5.5085" x2="3.3235" y2="-5.4855" layer="21"/>
<rectangle x1="-3.3235" y1="-5.4855" x2="-1.2305" y2="-5.4625" layer="21"/>
<rectangle x1="1.1845" y1="-5.4855" x2="3.2775" y2="-5.4625" layer="21"/>
<rectangle x1="-3.2775" y1="-5.4625" x2="-1.2995" y2="-5.4395" layer="21"/>
<rectangle x1="1.2535" y1="-5.4625" x2="3.2315" y2="-5.4395" layer="21"/>
<rectangle x1="-3.2315" y1="-5.4395" x2="-1.3685" y2="-5.4165" layer="21"/>
<rectangle x1="1.3225" y1="-5.4395" x2="3.1855" y2="-5.4165" layer="21"/>
<rectangle x1="-3.2085" y1="-5.4165" x2="-1.4375" y2="-5.3935" layer="21"/>
<rectangle x1="1.3915" y1="-5.4165" x2="3.1625" y2="-5.3935" layer="21"/>
<rectangle x1="-3.1625" y1="-5.3935" x2="-1.5065" y2="-5.3705" layer="21"/>
<rectangle x1="1.4605" y1="-5.3935" x2="3.1165" y2="-5.3705" layer="21"/>
<rectangle x1="-3.1165" y1="-5.3705" x2="-1.5525" y2="-5.3475" layer="21"/>
<rectangle x1="1.5295" y1="-5.3705" x2="3.0705" y2="-5.3475" layer="21"/>
<rectangle x1="-3.0705" y1="-5.3475" x2="-1.6215" y2="-5.3245" layer="21"/>
<rectangle x1="1.5755" y1="-5.3475" x2="3.0245" y2="-5.3245" layer="21"/>
<rectangle x1="-3.0245" y1="-5.3245" x2="-1.6905" y2="-5.3015" layer="21"/>
<rectangle x1="1.6445" y1="-5.3245" x2="2.9785" y2="-5.3015" layer="21"/>
<rectangle x1="-2.9785" y1="-5.3015" x2="-1.7595" y2="-5.2785" layer="21"/>
<rectangle x1="1.7135" y1="-5.3015" x2="2.9325" y2="-5.2785" layer="21"/>
<rectangle x1="-2.9325" y1="-5.2785" x2="-1.8055" y2="-5.2555" layer="21"/>
<rectangle x1="1.7595" y1="-5.2785" x2="2.8865" y2="-5.2555" layer="21"/>
<rectangle x1="-2.8865" y1="-5.2555" x2="-1.8745" y2="-5.2325" layer="21"/>
<rectangle x1="1.8285" y1="-5.2555" x2="2.8405" y2="-5.2325" layer="21"/>
<rectangle x1="-2.8405" y1="-5.2325" x2="-1.9435" y2="-5.2095" layer="21"/>
<rectangle x1="1.8975" y1="-5.2325" x2="2.8175" y2="-5.2095" layer="21"/>
<rectangle x1="-2.8175" y1="-5.2095" x2="-1.9895" y2="-5.1865" layer="21"/>
<rectangle x1="1.9435" y1="-5.2095" x2="2.7715" y2="-5.1865" layer="21"/>
<rectangle x1="-2.7715" y1="-5.1865" x2="-2.0585" y2="-5.1635" layer="21"/>
<rectangle x1="2.0125" y1="-5.1865" x2="2.7255" y2="-5.1635" layer="21"/>
<rectangle x1="-2.7255" y1="-5.1635" x2="-2.1275" y2="-5.1405" layer="21"/>
<rectangle x1="2.0815" y1="-5.1635" x2="2.6795" y2="-5.1405" layer="21"/>
<rectangle x1="-2.6795" y1="-5.1405" x2="-2.1735" y2="-5.1175" layer="21"/>
<rectangle x1="2.1275" y1="-5.1405" x2="2.6335" y2="-5.1175" layer="21"/>
<rectangle x1="-2.6335" y1="-5.1175" x2="-2.2425" y2="-5.0945" layer="21"/>
<rectangle x1="2.1965" y1="-5.1175" x2="2.5875" y2="-5.0945" layer="21"/>
<rectangle x1="-2.5875" y1="-5.0945" x2="-2.3115" y2="-5.0715" layer="21"/>
<rectangle x1="2.2655" y1="-5.0945" x2="2.5415" y2="-5.0715" layer="21"/>
<rectangle x1="-2.5415" y1="-5.0715" x2="-2.3575" y2="-5.0485" layer="21"/>
<rectangle x1="2.3115" y1="-5.0715" x2="2.4955" y2="-5.0485" layer="21"/>
<rectangle x1="-2.4955" y1="-5.0485" x2="-2.4265" y2="-5.0255" layer="21"/>
<rectangle x1="2.3805" y1="-5.0485" x2="2.4495" y2="-5.0255" layer="21"/>
<rectangle x1="-2.4725" y1="-4.5655" x2="-2.4495" y2="-4.5425" layer="21"/>
<rectangle x1="2.4035" y1="-4.5655" x2="2.4265" y2="-4.5425" layer="21"/>
<rectangle x1="-2.5185" y1="-4.5425" x2="-2.4035" y2="-4.5195" layer="21"/>
<rectangle x1="2.3575" y1="-4.5425" x2="2.4725" y2="-4.5195" layer="21"/>
<rectangle x1="-2.5645" y1="-4.5195" x2="-2.3345" y2="-4.4965" layer="21"/>
<rectangle x1="2.2885" y1="-4.5195" x2="2.5185" y2="-4.4965" layer="21"/>
<rectangle x1="-2.6105" y1="-4.4965" x2="-2.2655" y2="-4.4735" layer="21"/>
<rectangle x1="2.2425" y1="-4.4965" x2="2.5645" y2="-4.4735" layer="21"/>
<rectangle x1="-2.6565" y1="-4.4735" x2="-2.2195" y2="-4.4505" layer="21"/>
<rectangle x1="2.1735" y1="-4.4735" x2="2.6105" y2="-4.4505" layer="21"/>
<rectangle x1="-2.6795" y1="-4.4505" x2="-2.1505" y2="-4.4275" layer="21"/>
<rectangle x1="2.1045" y1="-4.4505" x2="2.6335" y2="-4.4275" layer="21"/>
<rectangle x1="-2.7255" y1="-4.4275" x2="-2.1045" y2="-4.4045" layer="21"/>
<rectangle x1="2.0585" y1="-4.4275" x2="2.6795" y2="-4.4045" layer="21"/>
<rectangle x1="-2.7715" y1="-4.4045" x2="-2.0355" y2="-4.3815" layer="21"/>
<rectangle x1="1.9895" y1="-4.4045" x2="2.7255" y2="-4.3815" layer="21"/>
<rectangle x1="-2.8175" y1="-4.3815" x2="-1.9665" y2="-4.3585" layer="21"/>
<rectangle x1="1.9205" y1="-4.3815" x2="2.7715" y2="-4.3585" layer="21"/>
<rectangle x1="-2.8635" y1="-4.3585" x2="-1.9205" y2="-4.3355" layer="21"/>
<rectangle x1="1.8745" y1="-4.3585" x2="2.8175" y2="-4.3355" layer="21"/>
<rectangle x1="-2.9095" y1="-4.3355" x2="-1.8515" y2="-4.3125" layer="21"/>
<rectangle x1="1.8055" y1="-4.3355" x2="2.8635" y2="-4.3125" layer="21"/>
<rectangle x1="-2.9555" y1="-4.3125" x2="-1.7825" y2="-4.2895" layer="21"/>
<rectangle x1="1.7365" y1="-4.3125" x2="2.9095" y2="-4.2895" layer="21"/>
<rectangle x1="-3.0015" y1="-4.2895" x2="-1.7135" y2="-4.2665" layer="21"/>
<rectangle x1="1.6675" y1="-4.2895" x2="2.9555" y2="-4.2665" layer="21"/>
<rectangle x1="-3.0475" y1="-4.2665" x2="-1.6675" y2="-4.2435" layer="21"/>
<rectangle x1="1.6215" y1="-4.2665" x2="3.0015" y2="-4.2435" layer="21"/>
<rectangle x1="-3.0935" y1="-4.2435" x2="-1.5985" y2="-4.2205" layer="21"/>
<rectangle x1="1.5525" y1="-4.2435" x2="3.0475" y2="-4.2205" layer="21"/>
<rectangle x1="-3.1395" y1="-4.2205" x2="-1.5295" y2="-4.1975" layer="21"/>
<rectangle x1="1.4835" y1="-4.2205" x2="3.0935" y2="-4.1975" layer="21"/>
<rectangle x1="-3.1625" y1="-4.1975" x2="-1.4835" y2="-4.1745" layer="21"/>
<rectangle x1="1.4375" y1="-4.1975" x2="3.1165" y2="-4.1745" layer="21"/>
<rectangle x1="-3.2085" y1="-4.1745" x2="-1.4145" y2="-4.1515" layer="21"/>
<rectangle x1="1.3685" y1="-4.1745" x2="3.1625" y2="-4.1515" layer="21"/>
<rectangle x1="-3.2545" y1="-4.1515" x2="-1.3455" y2="-4.1285" layer="21"/>
<rectangle x1="1.2995" y1="-4.1515" x2="3.2085" y2="-4.1285" layer="21"/>
<rectangle x1="-3.3005" y1="-4.1285" x2="-1.2765" y2="-4.1055" layer="21"/>
<rectangle x1="1.2305" y1="-4.1285" x2="3.2545" y2="-4.1055" layer="21"/>
<rectangle x1="-3.3465" y1="-4.1055" x2="-1.2075" y2="-4.0825" layer="21"/>
<rectangle x1="1.1615" y1="-4.1055" x2="3.3005" y2="-4.0825" layer="21"/>
<rectangle x1="-3.3925" y1="-4.0825" x2="-1.1155" y2="-4.0595" layer="21"/>
<rectangle x1="1.0925" y1="-4.0825" x2="3.3465" y2="-4.0595" layer="21"/>
<rectangle x1="-3.4385" y1="-4.0595" x2="-1.0465" y2="-4.0365" layer="21"/>
<rectangle x1="1.0005" y1="-4.0595" x2="3.3925" y2="-4.0365" layer="21"/>
<rectangle x1="-3.4845" y1="-4.0365" x2="-0.9775" y2="-4.0135" layer="21"/>
<rectangle x1="0.9315" y1="-4.0365" x2="3.4385" y2="-4.0135" layer="21"/>
<rectangle x1="-3.5305" y1="-4.0135" x2="-0.8855" y2="-3.9905" layer="21"/>
<rectangle x1="0.8395" y1="-4.0135" x2="3.4845" y2="-3.9905" layer="21"/>
<rectangle x1="-3.5765" y1="-3.9905" x2="-0.7935" y2="-3.9675" layer="21"/>
<rectangle x1="0.7475" y1="-3.9905" x2="3.5305" y2="-3.9675" layer="21"/>
<rectangle x1="-3.6225" y1="-3.9675" x2="-0.7015" y2="-3.9445" layer="21"/>
<rectangle x1="0.6555" y1="-3.9675" x2="3.5765" y2="-3.9445" layer="21"/>
<rectangle x1="-3.6455" y1="-3.9445" x2="-0.5865" y2="-3.9215" layer="21"/>
<rectangle x1="0.5405" y1="-3.9445" x2="3.6225" y2="-3.9215" layer="21"/>
<rectangle x1="-3.6915" y1="-3.9215" x2="-0.4485" y2="-3.8985" layer="21"/>
<rectangle x1="0.4025" y1="-3.9215" x2="3.6455" y2="-3.8985" layer="21"/>
<rectangle x1="-3.7375" y1="-3.8985" x2="-0.2645" y2="-3.8755" layer="21"/>
<rectangle x1="0.2185" y1="-3.8985" x2="3.6915" y2="-3.8755" layer="21"/>
<rectangle x1="-3.7835" y1="-3.8755" x2="3.7375" y2="-3.8525" layer="21"/>
<rectangle x1="-3.8295" y1="-3.8525" x2="3.7835" y2="-3.8295" layer="21"/>
<rectangle x1="-3.8755" y1="-3.8295" x2="3.8295" y2="-3.8065" layer="21"/>
<rectangle x1="-3.9215" y1="-3.8065" x2="3.8755" y2="-3.7835" layer="21"/>
<rectangle x1="-3.9675" y1="-3.7835" x2="3.9215" y2="-3.7605" layer="21"/>
<rectangle x1="-4.0135" y1="-3.7605" x2="3.9675" y2="-3.7375" layer="21"/>
<rectangle x1="-4.0595" y1="-3.7375" x2="4.0135" y2="-3.7145" layer="21"/>
<rectangle x1="-4.1055" y1="-3.7145" x2="4.0595" y2="-3.6915" layer="21"/>
<rectangle x1="-4.1285" y1="-3.6915" x2="4.0825" y2="-3.6685" layer="21"/>
<rectangle x1="-4.1745" y1="-3.6685" x2="4.1285" y2="-3.6455" layer="21"/>
<rectangle x1="-4.2205" y1="-3.6455" x2="4.1745" y2="-3.6225" layer="21"/>
<rectangle x1="-4.2435" y1="-3.6225" x2="4.1975" y2="-3.5995" layer="21"/>
<rectangle x1="-4.2205" y1="-3.5995" x2="4.1745" y2="-3.5765" layer="21"/>
<rectangle x1="-4.1745" y1="-3.5765" x2="4.1285" y2="-3.5535" layer="21"/>
<rectangle x1="-4.1285" y1="-3.5535" x2="4.0825" y2="-3.5305" layer="21"/>
<rectangle x1="-4.0825" y1="-3.5305" x2="4.0365" y2="-3.5075" layer="21"/>
<rectangle x1="-4.0365" y1="-3.5075" x2="3.9905" y2="-3.4845" layer="21"/>
<rectangle x1="-3.9905" y1="-3.4845" x2="3.9445" y2="-3.4615" layer="21"/>
<rectangle x1="-3.9445" y1="-3.4615" x2="3.8985" y2="-3.4385" layer="21"/>
<rectangle x1="-3.8985" y1="-3.4385" x2="3.8525" y2="-3.4155" layer="21"/>
<rectangle x1="-3.8525" y1="-3.4155" x2="3.8065" y2="-3.3925" layer="21"/>
<rectangle x1="-3.8065" y1="-3.3925" x2="3.7605" y2="-3.3695" layer="21"/>
<rectangle x1="-3.7605" y1="-3.3695" x2="3.7375" y2="-3.3465" layer="21"/>
<rectangle x1="-3.7375" y1="-3.3465" x2="-0.3565" y2="-3.3235" layer="21"/>
<rectangle x1="0.3105" y1="-3.3465" x2="3.6915" y2="-3.3235" layer="21"/>
<rectangle x1="-3.6915" y1="-3.3235" x2="-0.4945" y2="-3.3005" layer="21"/>
<rectangle x1="0.4485" y1="-3.3235" x2="3.6455" y2="-3.3005" layer="21"/>
<rectangle x1="-3.6455" y1="-3.3005" x2="-0.6325" y2="-3.2775" layer="21"/>
<rectangle x1="0.5865" y1="-3.3005" x2="3.5995" y2="-3.2775" layer="21"/>
<rectangle x1="-3.5995" y1="-3.2775" x2="-0.7245" y2="-3.2545" layer="21"/>
<rectangle x1="0.6785" y1="-3.2775" x2="3.5535" y2="-3.2545" layer="21"/>
<rectangle x1="-3.5535" y1="-3.2545" x2="-0.8165" y2="-3.2315" layer="21"/>
<rectangle x1="0.7705" y1="-3.2545" x2="3.5075" y2="-3.2315" layer="21"/>
<rectangle x1="-3.5075" y1="-3.2315" x2="-0.9085" y2="-3.2085" layer="21"/>
<rectangle x1="0.8625" y1="-3.2315" x2="3.4615" y2="-3.2085" layer="21"/>
<rectangle x1="-3.4615" y1="-3.2085" x2="-1.0005" y2="-3.1855" layer="21"/>
<rectangle x1="0.9545" y1="-3.2085" x2="3.4155" y2="-3.1855" layer="21"/>
<rectangle x1="-3.4155" y1="-3.1855" x2="-1.0695" y2="-3.1625" layer="21"/>
<rectangle x1="1.0235" y1="-3.1855" x2="3.3695" y2="-3.1625" layer="21"/>
<rectangle x1="-3.3695" y1="-3.1625" x2="-1.1615" y2="-3.1395" layer="21"/>
<rectangle x1="1.1155" y1="-3.1625" x2="3.3235" y2="-3.1395" layer="21"/>
<rectangle x1="-3.3235" y1="-3.1395" x2="-1.2305" y2="-3.1165" layer="21"/>
<rectangle x1="1.1845" y1="-3.1395" x2="3.2775" y2="-3.1165" layer="21"/>
<rectangle x1="-3.2775" y1="-3.1165" x2="-1.2995" y2="-3.0935" layer="21"/>
<rectangle x1="1.2535" y1="-3.1165" x2="3.2315" y2="-3.0935" layer="21"/>
<rectangle x1="-3.2545" y1="-3.0935" x2="-1.3685" y2="-3.0705" layer="21"/>
<rectangle x1="1.3225" y1="-3.0935" x2="3.2085" y2="-3.0705" layer="21"/>
<rectangle x1="-3.2085" y1="-3.0705" x2="-1.4375" y2="-3.0475" layer="21"/>
<rectangle x1="1.3915" y1="-3.0705" x2="3.1625" y2="-3.0475" layer="21"/>
<rectangle x1="-3.1625" y1="-3.0475" x2="-1.4835" y2="-3.0245" layer="21"/>
<rectangle x1="1.4605" y1="-3.0475" x2="3.1165" y2="-3.0245" layer="21"/>
<rectangle x1="-3.1165" y1="-3.0245" x2="-1.5525" y2="-3.0015" layer="21"/>
<rectangle x1="1.5065" y1="-3.0245" x2="3.0705" y2="-3.0015" layer="21"/>
<rectangle x1="-3.0705" y1="-3.0015" x2="-1.6215" y2="-2.9785" layer="21"/>
<rectangle x1="1.5755" y1="-3.0015" x2="3.0245" y2="-2.9785" layer="21"/>
<rectangle x1="-3.0245" y1="-2.9785" x2="-1.6905" y2="-2.9555" layer="21"/>
<rectangle x1="1.6445" y1="-2.9785" x2="2.9785" y2="-2.9555" layer="21"/>
<rectangle x1="-2.9785" y1="-2.9555" x2="-1.7365" y2="-2.9325" layer="21"/>
<rectangle x1="1.6905" y1="-2.9555" x2="2.9325" y2="-2.9325" layer="21"/>
<rectangle x1="-2.9325" y1="-2.9325" x2="-1.8055" y2="-2.9095" layer="21"/>
<rectangle x1="1.7595" y1="-2.9325" x2="2.8865" y2="-2.9095" layer="21"/>
<rectangle x1="-2.8865" y1="-2.9095" x2="-1.8745" y2="-2.8865" layer="21"/>
<rectangle x1="1.8285" y1="-2.9095" x2="2.8405" y2="-2.8865" layer="21"/>
<rectangle x1="-2.8635" y1="-2.8865" x2="-1.9205" y2="-2.8635" layer="21"/>
<rectangle x1="1.8975" y1="-2.8865" x2="2.8175" y2="-2.8635" layer="21"/>
<rectangle x1="-2.8175" y1="-2.8635" x2="-1.9895" y2="-2.8405" layer="21"/>
<rectangle x1="1.9435" y1="-2.8635" x2="2.7715" y2="-2.8405" layer="21"/>
<rectangle x1="-2.7715" y1="-2.8405" x2="-2.0585" y2="-2.8175" layer="21"/>
<rectangle x1="2.0125" y1="-2.8405" x2="2.7255" y2="-2.8175" layer="21"/>
<rectangle x1="-2.7255" y1="-2.8175" x2="-2.1275" y2="-2.7945" layer="21"/>
<rectangle x1="2.0815" y1="-2.8175" x2="2.6795" y2="-2.7945" layer="21"/>
<rectangle x1="-2.6795" y1="-2.7945" x2="-2.1735" y2="-2.7715" layer="21"/>
<rectangle x1="2.1275" y1="-2.7945" x2="2.6335" y2="-2.7715" layer="21"/>
<rectangle x1="-2.6335" y1="-2.7715" x2="-2.2425" y2="-2.7485" layer="21"/>
<rectangle x1="2.1965" y1="-2.7715" x2="2.5875" y2="-2.7485" layer="21"/>
<rectangle x1="-2.5875" y1="-2.7485" x2="-2.2885" y2="-2.7255" layer="21"/>
<rectangle x1="2.2425" y1="-2.7485" x2="2.5415" y2="-2.7255" layer="21"/>
<rectangle x1="-2.5415" y1="-2.7255" x2="-2.3575" y2="-2.7025" layer="21"/>
<rectangle x1="2.3115" y1="-2.7255" x2="2.4955" y2="-2.7025" layer="21"/>
<rectangle x1="-2.4955" y1="-2.7025" x2="-2.4035" y2="-2.6795" layer="21"/>
<rectangle x1="2.3575" y1="-2.7025" x2="2.4495" y2="-2.6795" layer="21"/>
<rectangle x1="2.4035" y1="-2.2195" x2="2.4265" y2="-2.1965" layer="21"/>
<rectangle x1="-2.5185" y1="-2.1965" x2="-2.4035" y2="-2.1735" layer="21"/>
<rectangle x1="2.3575" y1="-2.1965" x2="2.4725" y2="-2.1735" layer="21"/>
<rectangle x1="-2.5645" y1="-2.1735" x2="-2.3345" y2="-2.1505" layer="21"/>
<rectangle x1="2.2885" y1="-2.1735" x2="2.5185" y2="-2.1505" layer="21"/>
<rectangle x1="-2.6105" y1="-2.1505" x2="-2.2885" y2="-2.1275" layer="21"/>
<rectangle x1="2.2425" y1="-2.1505" x2="2.5645" y2="-2.1275" layer="21"/>
<rectangle x1="-2.6335" y1="-2.1275" x2="-2.2195" y2="-2.1045" layer="21"/>
<rectangle x1="2.1735" y1="-2.1275" x2="2.5875" y2="-2.1045" layer="21"/>
<rectangle x1="-2.6795" y1="-2.1045" x2="-2.1505" y2="-2.0815" layer="21"/>
<rectangle x1="2.1045" y1="-2.1045" x2="2.6335" y2="-2.0815" layer="21"/>
<rectangle x1="-2.7255" y1="-2.0815" x2="-2.1045" y2="-2.0585" layer="21"/>
<rectangle x1="2.0585" y1="-2.0815" x2="2.6795" y2="-2.0585" layer="21"/>
<rectangle x1="-2.7715" y1="-2.0585" x2="-2.0355" y2="-2.0355" layer="21"/>
<rectangle x1="1.9895" y1="-2.0585" x2="2.7255" y2="-2.0355" layer="21"/>
<rectangle x1="-2.8175" y1="-2.0355" x2="-1.9665" y2="-2.0125" layer="21"/>
<rectangle x1="1.9205" y1="-2.0355" x2="2.7715" y2="-2.0125" layer="21"/>
<rectangle x1="-2.8635" y1="-2.0125" x2="-1.9205" y2="-1.9895" layer="21"/>
<rectangle x1="1.8745" y1="-2.0125" x2="2.8175" y2="-1.9895" layer="21"/>
<rectangle x1="-2.9095" y1="-1.9895" x2="-1.8515" y2="-1.9665" layer="21"/>
<rectangle x1="1.8055" y1="-1.9895" x2="2.8635" y2="-1.9665" layer="21"/>
<rectangle x1="-2.9555" y1="-1.9665" x2="-1.7825" y2="-1.9435" layer="21"/>
<rectangle x1="1.7365" y1="-1.9665" x2="2.9095" y2="-1.9435" layer="21"/>
<rectangle x1="-3.0015" y1="-1.9435" x2="-1.7365" y2="-1.9205" layer="21"/>
<rectangle x1="1.6905" y1="-1.9435" x2="2.9555" y2="-1.9205" layer="21"/>
<rectangle x1="-3.0475" y1="-1.9205" x2="-1.6675" y2="-1.8975" layer="21"/>
<rectangle x1="1.6215" y1="-1.9205" x2="3.0015" y2="-1.8975" layer="21"/>
<rectangle x1="-3.0935" y1="-1.8975" x2="-1.5985" y2="-1.8745" layer="21"/>
<rectangle x1="1.5525" y1="-1.8975" x2="3.0475" y2="-1.8745" layer="21"/>
<rectangle x1="-3.1165" y1="-1.8745" x2="-1.5525" y2="-1.8515" layer="21"/>
<rectangle x1="1.5065" y1="-1.8745" x2="3.0705" y2="-1.8515" layer="21"/>
<rectangle x1="-3.1625" y1="-1.8515" x2="-1.4835" y2="-1.8285" layer="21"/>
<rectangle x1="1.4375" y1="-1.8515" x2="3.1165" y2="-1.8285" layer="21"/>
<rectangle x1="-3.2085" y1="-1.8285" x2="-1.4145" y2="-1.8055" layer="21"/>
<rectangle x1="1.3685" y1="-1.8285" x2="3.1625" y2="-1.8055" layer="21"/>
<rectangle x1="-3.2545" y1="-1.8055" x2="-1.3455" y2="-1.7825" layer="21"/>
<rectangle x1="1.2995" y1="-1.8055" x2="3.2085" y2="-1.7825" layer="21"/>
<rectangle x1="-3.3005" y1="-1.7825" x2="-1.2765" y2="-1.7595" layer="21"/>
<rectangle x1="1.2305" y1="-1.7825" x2="3.2545" y2="-1.7595" layer="21"/>
<rectangle x1="-3.3465" y1="-1.7595" x2="-1.2075" y2="-1.7365" layer="21"/>
<rectangle x1="1.1615" y1="-1.7595" x2="3.3005" y2="-1.7365" layer="21"/>
<rectangle x1="-3.3925" y1="-1.7365" x2="-1.1155" y2="-1.7135" layer="21"/>
<rectangle x1="1.0925" y1="-1.7365" x2="3.3465" y2="-1.7135" layer="21"/>
<rectangle x1="-3.4385" y1="-1.7135" x2="-1.0465" y2="-1.6905" layer="21"/>
<rectangle x1="1.0005" y1="-1.7135" x2="3.3925" y2="-1.6905" layer="21"/>
<rectangle x1="-3.4845" y1="-1.6905" x2="-0.9775" y2="-1.6675" layer="21"/>
<rectangle x1="0.9315" y1="-1.6905" x2="3.4385" y2="-1.6675" layer="21"/>
<rectangle x1="-3.5305" y1="-1.6675" x2="-0.8855" y2="-1.6445" layer="21"/>
<rectangle x1="0.8395" y1="-1.6675" x2="3.4845" y2="-1.6445" layer="21"/>
<rectangle x1="-3.5765" y1="-1.6445" x2="-0.7935" y2="-1.6215" layer="21"/>
<rectangle x1="0.7475" y1="-1.6445" x2="3.5305" y2="-1.6215" layer="21"/>
<rectangle x1="-3.5995" y1="-1.6215" x2="-0.7015" y2="-1.5985" layer="21"/>
<rectangle x1="0.6555" y1="-1.6215" x2="3.5765" y2="-1.5985" layer="21"/>
<rectangle x1="-3.6455" y1="-1.5985" x2="-0.5865" y2="-1.5755" layer="21"/>
<rectangle x1="0.5405" y1="-1.5985" x2="3.5995" y2="-1.5755" layer="21"/>
<rectangle x1="-3.6915" y1="-1.5755" x2="-0.4715" y2="-1.5525" layer="21"/>
<rectangle x1="0.4255" y1="-1.5755" x2="3.6455" y2="-1.5525" layer="21"/>
<rectangle x1="-3.7375" y1="-1.5525" x2="-0.2875" y2="-1.5295" layer="21"/>
<rectangle x1="0.2415" y1="-1.5525" x2="3.6915" y2="-1.5295" layer="21"/>
<rectangle x1="-3.7835" y1="-1.5295" x2="3.7375" y2="-1.5065" layer="21"/>
<rectangle x1="-3.8295" y1="-1.5065" x2="3.7835" y2="-1.4835" layer="21"/>
<rectangle x1="-3.8755" y1="-1.4835" x2="3.8295" y2="-1.4605" layer="21"/>
<rectangle x1="-3.9215" y1="-1.4605" x2="3.8755" y2="-1.4375" layer="21"/>
<rectangle x1="-3.9675" y1="-1.4375" x2="3.9215" y2="-1.4145" layer="21"/>
<rectangle x1="-4.0135" y1="-1.4145" x2="3.9675" y2="-1.3915" layer="21"/>
<rectangle x1="-4.0595" y1="-1.3915" x2="4.0135" y2="-1.3685" layer="21"/>
<rectangle x1="-4.0825" y1="-1.3685" x2="4.0595" y2="-1.3455" layer="21"/>
<rectangle x1="-4.1285" y1="-1.3455" x2="4.0825" y2="-1.3225" layer="21"/>
<rectangle x1="-4.1745" y1="-1.3225" x2="4.1285" y2="-1.2995" layer="21"/>
<rectangle x1="-4.2205" y1="-1.2995" x2="4.1745" y2="-1.2765" layer="21"/>
<rectangle x1="-4.2435" y1="-1.2765" x2="4.1975" y2="-1.2535" layer="21"/>
<rectangle x1="-4.2205" y1="-1.2535" x2="4.1745" y2="-1.2305" layer="21"/>
<rectangle x1="-4.1745" y1="-1.2305" x2="4.1285" y2="-1.2075" layer="21"/>
<rectangle x1="-4.1285" y1="-1.2075" x2="4.0825" y2="-1.1845" layer="21"/>
<rectangle x1="-4.0825" y1="-1.1845" x2="4.0365" y2="-1.1615" layer="21"/>
<rectangle x1="-4.0365" y1="-1.1615" x2="3.9905" y2="-1.1385" layer="21"/>
<rectangle x1="-3.9905" y1="-1.1385" x2="3.9445" y2="-1.1155" layer="21"/>
<rectangle x1="-3.9445" y1="-1.1155" x2="3.8985" y2="-1.0925" layer="21"/>
<rectangle x1="-3.8985" y1="-1.0925" x2="3.8525" y2="-1.0695" layer="21"/>
<rectangle x1="-3.8525" y1="-1.0695" x2="3.8065" y2="-1.0465" layer="21"/>
<rectangle x1="-3.8065" y1="-1.0465" x2="3.7835" y2="-1.0235" layer="21"/>
<rectangle x1="-3.7835" y1="-1.0235" x2="3.7375" y2="-1.0005" layer="21"/>
<rectangle x1="-3.7375" y1="-1.0005" x2="-0.3335" y2="-0.9775" layer="21"/>
<rectangle x1="0.2875" y1="-1.0005" x2="3.6915" y2="-0.9775" layer="21"/>
<rectangle x1="-3.6915" y1="-0.9775" x2="-0.4945" y2="-0.9545" layer="21"/>
<rectangle x1="0.4485" y1="-0.9775" x2="3.6455" y2="-0.9545" layer="21"/>
<rectangle x1="-3.6455" y1="-0.9545" x2="-0.6095" y2="-0.9315" layer="21"/>
<rectangle x1="0.5635" y1="-0.9545" x2="3.5995" y2="-0.9315" layer="21"/>
<rectangle x1="-3.5995" y1="-0.9315" x2="-0.7245" y2="-0.9085" layer="21"/>
<rectangle x1="0.6785" y1="-0.9315" x2="3.5535" y2="-0.9085" layer="21"/>
<rectangle x1="-3.5535" y1="-0.9085" x2="-0.8165" y2="-0.8855" layer="21"/>
<rectangle x1="0.7705" y1="-0.9085" x2="3.5075" y2="-0.8855" layer="21"/>
<rectangle x1="-3.5075" y1="-0.8855" x2="-0.9085" y2="-0.8625" layer="21"/>
<rectangle x1="0.8625" y1="-0.8855" x2="3.4615" y2="-0.8625" layer="21"/>
<rectangle x1="-3.4615" y1="-0.8625" x2="-1.0005" y2="-0.8395" layer="21"/>
<rectangle x1="0.9545" y1="-0.8625" x2="3.4155" y2="-0.8395" layer="21"/>
<rectangle x1="-3.4155" y1="-0.8395" x2="-1.0695" y2="-0.8165" layer="21"/>
<rectangle x1="1.0235" y1="-0.8395" x2="3.3695" y2="-0.8165" layer="21"/>
<rectangle x1="-3.3695" y1="-0.8165" x2="-1.1385" y2="-0.7935" layer="21"/>
<rectangle x1="1.1155" y1="-0.8165" x2="3.3235" y2="-0.7935" layer="21"/>
<rectangle x1="-3.3235" y1="-0.7935" x2="-1.2305" y2="-0.7705" layer="21"/>
<rectangle x1="1.1845" y1="-0.7935" x2="3.2775" y2="-0.7705" layer="21"/>
<rectangle x1="-3.3005" y1="-0.7705" x2="-1.2995" y2="-0.7475" layer="21"/>
<rectangle x1="1.2535" y1="-0.7705" x2="3.2545" y2="-0.7475" layer="21"/>
<rectangle x1="-3.2545" y1="-0.7475" x2="-1.3685" y2="-0.7245" layer="21"/>
<rectangle x1="1.3225" y1="-0.7475" x2="3.2085" y2="-0.7245" layer="21"/>
<rectangle x1="-3.2085" y1="-0.7245" x2="-1.4145" y2="-0.7015" layer="21"/>
<rectangle x1="1.3685" y1="-0.7245" x2="3.1625" y2="-0.7015" layer="21"/>
<rectangle x1="-3.1625" y1="-0.7015" x2="-1.4835" y2="-0.6785" layer="21"/>
<rectangle x1="1.4375" y1="-0.7015" x2="3.1165" y2="-0.6785" layer="21"/>
<rectangle x1="-3.1165" y1="-0.6785" x2="-1.5525" y2="-0.6555" layer="21"/>
<rectangle x1="1.5065" y1="-0.6785" x2="3.0705" y2="-0.6555" layer="21"/>
<rectangle x1="-3.0705" y1="-0.6555" x2="-1.6215" y2="-0.6325" layer="21"/>
<rectangle x1="1.5755" y1="-0.6555" x2="3.0245" y2="-0.6325" layer="21"/>
<rectangle x1="-3.0245" y1="-0.6325" x2="-1.6905" y2="-0.6095" layer="21"/>
<rectangle x1="1.6445" y1="-0.6325" x2="2.9785" y2="-0.6095" layer="21"/>
<rectangle x1="-2.9785" y1="-0.6095" x2="-1.7365" y2="-0.5865" layer="21"/>
<rectangle x1="1.6905" y1="-0.6095" x2="2.9325" y2="-0.5865" layer="21"/>
<rectangle x1="-2.9325" y1="-0.5865" x2="-1.8055" y2="-0.5635" layer="21"/>
<rectangle x1="1.7595" y1="-0.5865" x2="2.8865" y2="-0.5635" layer="21"/>
<rectangle x1="-2.8865" y1="-0.5635" x2="-1.8745" y2="-0.5405" layer="21"/>
<rectangle x1="1.8285" y1="-0.5635" x2="2.8635" y2="-0.5405" layer="21"/>
<rectangle x1="-2.8635" y1="-0.5405" x2="-1.9205" y2="-0.5175" layer="21"/>
<rectangle x1="1.8975" y1="-0.5405" x2="2.8175" y2="-0.5175" layer="21"/>
<rectangle x1="-2.8175" y1="-0.5175" x2="-1.9895" y2="-0.4945" layer="21"/>
<rectangle x1="1.9435" y1="-0.5175" x2="2.7715" y2="-0.4945" layer="21"/>
<rectangle x1="-2.7715" y1="-0.4945" x2="-2.0585" y2="-0.4715" layer="21"/>
<rectangle x1="2.0125" y1="-0.4945" x2="2.7255" y2="-0.4715" layer="21"/>
<rectangle x1="-2.7255" y1="-0.4715" x2="-2.1045" y2="-0.4485" layer="21"/>
<rectangle x1="2.0585" y1="-0.4715" x2="2.6795" y2="-0.4485" layer="21"/>
<rectangle x1="-2.6795" y1="-0.4485" x2="-2.1735" y2="-0.4255" layer="21"/>
<rectangle x1="2.1275" y1="-0.4485" x2="2.6335" y2="-0.4255" layer="21"/>
<rectangle x1="-2.6335" y1="-0.4255" x2="-2.2425" y2="-0.4025" layer="21"/>
<rectangle x1="2.1965" y1="-0.4255" x2="2.5875" y2="-0.4025" layer="21"/>
<rectangle x1="-2.5875" y1="-0.4025" x2="-2.2885" y2="-0.3795" layer="21"/>
<rectangle x1="2.2425" y1="-0.4025" x2="2.5415" y2="-0.3795" layer="21"/>
<rectangle x1="-2.5415" y1="-0.3795" x2="-2.3575" y2="-0.3565" layer="21"/>
<rectangle x1="2.3115" y1="-0.3795" x2="2.4955" y2="-0.3565" layer="21"/>
<rectangle x1="-2.4955" y1="-0.3565" x2="-2.4035" y2="-0.3335" layer="21"/>
<rectangle x1="2.3575" y1="-0.3565" x2="2.4495" y2="-0.3335" layer="21"/>
<rectangle x1="2.3805" y1="0.9315" x2="2.7945" y2="0.9545" layer="21"/>
<rectangle x1="2.2195" y1="0.9545" x2="2.9555" y2="0.9775" layer="21"/>
<rectangle x1="2.1275" y1="0.9775" x2="3.0475" y2="1.0005" layer="21"/>
<rectangle x1="-3.7835" y1="1.0005" x2="-2.2885" y2="1.0235" layer="21"/>
<rectangle x1="-0.3795" y1="1.0005" x2="0.3105" y2="1.0235" layer="21"/>
<rectangle x1="2.0355" y1="1.0005" x2="3.1165" y2="1.0235" layer="21"/>
<rectangle x1="-3.8065" y1="1.0235" x2="-2.1735" y2="1.0465" layer="21"/>
<rectangle x1="-0.4025" y1="1.0235" x2="0.3335" y2="1.0465" layer="21"/>
<rectangle x1="1.9895" y1="1.0235" x2="3.1855" y2="1.0465" layer="21"/>
<rectangle x1="-3.8065" y1="1.0465" x2="-2.0815" y2="1.0695" layer="21"/>
<rectangle x1="-0.4255" y1="1.0465" x2="0.3335" y2="1.0695" layer="21"/>
<rectangle x1="1.9435" y1="1.0465" x2="3.2315" y2="1.0695" layer="21"/>
<rectangle x1="-3.8065" y1="1.0695" x2="-2.0355" y2="1.0925" layer="21"/>
<rectangle x1="-0.4255" y1="1.0695" x2="0.3335" y2="1.0925" layer="21"/>
<rectangle x1="1.8975" y1="1.0695" x2="3.2775" y2="1.0925" layer="21"/>
<rectangle x1="-3.8065" y1="1.0925" x2="-1.9665" y2="1.1155" layer="21"/>
<rectangle x1="-0.4255" y1="1.0925" x2="0.3335" y2="1.1155" layer="21"/>
<rectangle x1="1.8515" y1="1.0925" x2="3.3235" y2="1.1155" layer="21"/>
<rectangle x1="-3.8065" y1="1.1155" x2="-1.9205" y2="1.1385" layer="21"/>
<rectangle x1="-0.4255" y1="1.1155" x2="0.3335" y2="1.1385" layer="21"/>
<rectangle x1="1.8285" y1="1.1155" x2="3.3465" y2="1.1385" layer="21"/>
<rectangle x1="-3.8065" y1="1.1385" x2="-1.8975" y2="1.1615" layer="21"/>
<rectangle x1="-0.4255" y1="1.1385" x2="0.3335" y2="1.1615" layer="21"/>
<rectangle x1="1.7825" y1="1.1385" x2="3.3695" y2="1.1615" layer="21"/>
<rectangle x1="-3.8065" y1="1.1615" x2="-1.8515" y2="1.1845" layer="21"/>
<rectangle x1="-0.4255" y1="1.1615" x2="0.3335" y2="1.1845" layer="21"/>
<rectangle x1="1.7595" y1="1.1615" x2="3.4155" y2="1.1845" layer="21"/>
<rectangle x1="-3.8065" y1="1.1845" x2="-1.8285" y2="1.2075" layer="21"/>
<rectangle x1="-0.4255" y1="1.1845" x2="0.3335" y2="1.2075" layer="21"/>
<rectangle x1="1.7365" y1="1.1845" x2="3.4385" y2="1.2075" layer="21"/>
<rectangle x1="-3.8065" y1="1.2075" x2="-1.8055" y2="1.2305" layer="21"/>
<rectangle x1="-0.4255" y1="1.2075" x2="0.3335" y2="1.2305" layer="21"/>
<rectangle x1="1.7135" y1="1.2075" x2="3.4615" y2="1.2305" layer="21"/>
<rectangle x1="-3.8065" y1="1.2305" x2="-1.7825" y2="1.2535" layer="21"/>
<rectangle x1="-0.4255" y1="1.2305" x2="0.3335" y2="1.2535" layer="21"/>
<rectangle x1="1.6905" y1="1.2305" x2="3.4845" y2="1.2535" layer="21"/>
<rectangle x1="-3.8065" y1="1.2535" x2="-1.7595" y2="1.2765" layer="21"/>
<rectangle x1="-0.4255" y1="1.2535" x2="0.3335" y2="1.2765" layer="21"/>
<rectangle x1="1.6675" y1="1.2535" x2="3.5075" y2="1.2765" layer="21"/>
<rectangle x1="-3.8065" y1="1.2765" x2="-1.7365" y2="1.2995" layer="21"/>
<rectangle x1="-0.4255" y1="1.2765" x2="0.3335" y2="1.2995" layer="21"/>
<rectangle x1="1.6445" y1="1.2765" x2="3.5075" y2="1.2995" layer="21"/>
<rectangle x1="-3.8065" y1="1.2995" x2="-1.7135" y2="1.3225" layer="21"/>
<rectangle x1="-0.4255" y1="1.2995" x2="0.3335" y2="1.3225" layer="21"/>
<rectangle x1="1.6215" y1="1.2995" x2="3.5305" y2="1.3225" layer="21"/>
<rectangle x1="-3.8065" y1="1.3225" x2="-1.7135" y2="1.3455" layer="21"/>
<rectangle x1="-0.4255" y1="1.3225" x2="0.3335" y2="1.3455" layer="21"/>
<rectangle x1="1.5985" y1="1.3225" x2="3.5535" y2="1.3455" layer="21"/>
<rectangle x1="-3.8065" y1="1.3455" x2="-1.6905" y2="1.3685" layer="21"/>
<rectangle x1="-0.4255" y1="1.3455" x2="0.3335" y2="1.3685" layer="21"/>
<rectangle x1="1.5985" y1="1.3455" x2="3.5765" y2="1.3685" layer="21"/>
<rectangle x1="-3.8065" y1="1.3685" x2="-1.6675" y2="1.3915" layer="21"/>
<rectangle x1="-0.4255" y1="1.3685" x2="0.3335" y2="1.3915" layer="21"/>
<rectangle x1="1.5755" y1="1.3685" x2="3.5765" y2="1.3915" layer="21"/>
<rectangle x1="-3.8065" y1="1.3915" x2="-1.6675" y2="1.4145" layer="21"/>
<rectangle x1="-0.4255" y1="1.3915" x2="0.3335" y2="1.4145" layer="21"/>
<rectangle x1="1.5755" y1="1.3915" x2="3.5995" y2="1.4145" layer="21"/>
<rectangle x1="-3.8065" y1="1.4145" x2="-1.6445" y2="1.4375" layer="21"/>
<rectangle x1="-0.4255" y1="1.4145" x2="0.3335" y2="1.4375" layer="21"/>
<rectangle x1="1.5525" y1="1.4145" x2="3.5995" y2="1.4375" layer="21"/>
<rectangle x1="-3.8065" y1="1.4375" x2="-1.6445" y2="1.4605" layer="21"/>
<rectangle x1="-0.4255" y1="1.4375" x2="0.3335" y2="1.4605" layer="21"/>
<rectangle x1="1.5525" y1="1.4375" x2="2.4495" y2="1.4605" layer="21"/>
<rectangle x1="2.7255" y1="1.4375" x2="3.6225" y2="1.4605" layer="21"/>
<rectangle x1="-3.8065" y1="1.4605" x2="-1.6215" y2="1.4835" layer="21"/>
<rectangle x1="-0.4255" y1="1.4605" x2="0.3335" y2="1.4835" layer="21"/>
<rectangle x1="1.5295" y1="1.4605" x2="2.3805" y2="1.4835" layer="21"/>
<rectangle x1="2.7945" y1="1.4605" x2="3.6225" y2="1.4835" layer="21"/>
<rectangle x1="-3.8065" y1="1.4835" x2="-1.6215" y2="1.5065" layer="21"/>
<rectangle x1="-0.4255" y1="1.4835" x2="0.3335" y2="1.5065" layer="21"/>
<rectangle x1="1.5295" y1="1.4835" x2="2.3345" y2="1.5065" layer="21"/>
<rectangle x1="2.8405" y1="1.4835" x2="3.6455" y2="1.5065" layer="21"/>
<rectangle x1="-3.8065" y1="1.5065" x2="-3.1165" y2="1.5295" layer="21"/>
<rectangle x1="-2.5415" y1="1.5065" x2="-1.5985" y2="1.5295" layer="21"/>
<rectangle x1="-0.4255" y1="1.5065" x2="0.3335" y2="1.5295" layer="21"/>
<rectangle x1="1.5065" y1="1.5065" x2="2.3115" y2="1.5295" layer="21"/>
<rectangle x1="2.8635" y1="1.5065" x2="3.6455" y2="1.5295" layer="21"/>
<rectangle x1="-3.8065" y1="1.5295" x2="-3.1165" y2="1.5525" layer="21"/>
<rectangle x1="-2.4725" y1="1.5295" x2="-1.5985" y2="1.5525" layer="21"/>
<rectangle x1="-0.4255" y1="1.5295" x2="0.3335" y2="1.5525" layer="21"/>
<rectangle x1="1.5065" y1="1.5295" x2="2.2885" y2="1.5525" layer="21"/>
<rectangle x1="2.8865" y1="1.5295" x2="3.6455" y2="1.5525" layer="21"/>
<rectangle x1="-3.8065" y1="1.5525" x2="-3.1165" y2="1.5755" layer="21"/>
<rectangle x1="-2.4265" y1="1.5525" x2="-1.5985" y2="1.5755" layer="21"/>
<rectangle x1="-0.4255" y1="1.5525" x2="0.3335" y2="1.5755" layer="21"/>
<rectangle x1="1.5065" y1="1.5525" x2="2.2655" y2="1.5755" layer="21"/>
<rectangle x1="2.9095" y1="1.5525" x2="3.6685" y2="1.5755" layer="21"/>
<rectangle x1="-3.8065" y1="1.5755" x2="-3.1165" y2="1.5985" layer="21"/>
<rectangle x1="-2.3805" y1="1.5755" x2="-1.5755" y2="1.5985" layer="21"/>
<rectangle x1="-0.4255" y1="1.5755" x2="0.3335" y2="1.5985" layer="21"/>
<rectangle x1="1.4835" y1="1.5755" x2="2.2425" y2="1.5985" layer="21"/>
<rectangle x1="2.9325" y1="1.5755" x2="3.6685" y2="1.5985" layer="21"/>
<rectangle x1="-3.8065" y1="1.5985" x2="-3.1165" y2="1.6215" layer="21"/>
<rectangle x1="-2.3575" y1="1.5985" x2="-1.5755" y2="1.6215" layer="21"/>
<rectangle x1="-0.4255" y1="1.5985" x2="0.3335" y2="1.6215" layer="21"/>
<rectangle x1="1.4835" y1="1.5985" x2="2.2195" y2="1.6215" layer="21"/>
<rectangle x1="2.9325" y1="1.5985" x2="3.6685" y2="1.6215" layer="21"/>
<rectangle x1="-3.8065" y1="1.6215" x2="-3.1165" y2="1.6445" layer="21"/>
<rectangle x1="-2.3345" y1="1.6215" x2="-1.5755" y2="1.6445" layer="21"/>
<rectangle x1="-0.4255" y1="1.6215" x2="0.3335" y2="1.6445" layer="21"/>
<rectangle x1="1.4835" y1="1.6215" x2="2.2195" y2="1.6445" layer="21"/>
<rectangle x1="2.9555" y1="1.6215" x2="3.6915" y2="1.6445" layer="21"/>
<rectangle x1="-3.8065" y1="1.6445" x2="-3.1165" y2="1.6675" layer="21"/>
<rectangle x1="-2.3345" y1="1.6445" x2="-1.5525" y2="1.6675" layer="21"/>
<rectangle x1="-0.4255" y1="1.6445" x2="0.3335" y2="1.6675" layer="21"/>
<rectangle x1="1.4835" y1="1.6445" x2="2.1965" y2="1.6675" layer="21"/>
<rectangle x1="2.9785" y1="1.6445" x2="3.6915" y2="1.6675" layer="21"/>
<rectangle x1="-3.8065" y1="1.6675" x2="-3.1165" y2="1.6905" layer="21"/>
<rectangle x1="-2.3115" y1="1.6675" x2="-1.5525" y2="1.6905" layer="21"/>
<rectangle x1="-0.4255" y1="1.6675" x2="0.3335" y2="1.6905" layer="21"/>
<rectangle x1="1.4605" y1="1.6675" x2="2.1965" y2="1.6905" layer="21"/>
<rectangle x1="2.9785" y1="1.6675" x2="3.6915" y2="1.6905" layer="21"/>
<rectangle x1="-3.8065" y1="1.6905" x2="-3.1165" y2="1.7135" layer="21"/>
<rectangle x1="-2.2885" y1="1.6905" x2="-1.5525" y2="1.7135" layer="21"/>
<rectangle x1="-0.4255" y1="1.6905" x2="0.3335" y2="1.7135" layer="21"/>
<rectangle x1="1.4605" y1="1.6905" x2="2.1965" y2="1.7135" layer="21"/>
<rectangle x1="2.9785" y1="1.6905" x2="3.6915" y2="1.7135" layer="21"/>
<rectangle x1="-3.8065" y1="1.7135" x2="-3.1165" y2="1.7365" layer="21"/>
<rectangle x1="-2.2885" y1="1.7135" x2="-1.5525" y2="1.7365" layer="21"/>
<rectangle x1="-0.4255" y1="1.7135" x2="0.3335" y2="1.7365" layer="21"/>
<rectangle x1="1.4605" y1="1.7135" x2="2.1735" y2="1.7365" layer="21"/>
<rectangle x1="3.0015" y1="1.7135" x2="3.7145" y2="1.7365" layer="21"/>
<rectangle x1="-3.8065" y1="1.7365" x2="-3.1165" y2="1.7595" layer="21"/>
<rectangle x1="-2.2655" y1="1.7365" x2="-1.5525" y2="1.7595" layer="21"/>
<rectangle x1="-0.4255" y1="1.7365" x2="0.3335" y2="1.7595" layer="21"/>
<rectangle x1="1.4605" y1="1.7365" x2="2.1735" y2="1.7595" layer="21"/>
<rectangle x1="3.0015" y1="1.7365" x2="3.7145" y2="1.7595" layer="21"/>
<rectangle x1="-3.8065" y1="1.7595" x2="-3.1165" y2="1.7825" layer="21"/>
<rectangle x1="-2.2655" y1="1.7595" x2="-1.5295" y2="1.7825" layer="21"/>
<rectangle x1="-0.4255" y1="1.7595" x2="0.3335" y2="1.7825" layer="21"/>
<rectangle x1="1.4605" y1="1.7595" x2="2.1735" y2="1.7825" layer="21"/>
<rectangle x1="3.0015" y1="1.7595" x2="3.7145" y2="1.7825" layer="21"/>
<rectangle x1="-3.8065" y1="1.7825" x2="-3.1165" y2="1.8055" layer="21"/>
<rectangle x1="-2.2655" y1="1.7825" x2="-1.5295" y2="1.8055" layer="21"/>
<rectangle x1="-0.4255" y1="1.7825" x2="0.3335" y2="1.8055" layer="21"/>
<rectangle x1="1.4605" y1="1.7825" x2="2.1505" y2="1.8055" layer="21"/>
<rectangle x1="3.0015" y1="1.7825" x2="3.7145" y2="1.8055" layer="21"/>
<rectangle x1="-3.8065" y1="1.8055" x2="-3.1165" y2="1.8285" layer="21"/>
<rectangle x1="-2.2425" y1="1.8055" x2="-1.5295" y2="1.8285" layer="21"/>
<rectangle x1="-0.4255" y1="1.8055" x2="0.3335" y2="1.8285" layer="21"/>
<rectangle x1="1.4605" y1="1.8055" x2="2.1505" y2="1.8285" layer="21"/>
<rectangle x1="3.0245" y1="1.8055" x2="3.7145" y2="1.8285" layer="21"/>
<rectangle x1="-3.8065" y1="1.8285" x2="-3.1165" y2="1.8515" layer="21"/>
<rectangle x1="-2.2425" y1="1.8285" x2="-1.5295" y2="1.8515" layer="21"/>
<rectangle x1="-0.4255" y1="1.8285" x2="0.3335" y2="1.8515" layer="21"/>
<rectangle x1="1.4375" y1="1.8285" x2="2.1505" y2="1.8515" layer="21"/>
<rectangle x1="3.0245" y1="1.8285" x2="3.7145" y2="1.8515" layer="21"/>
<rectangle x1="-3.8065" y1="1.8515" x2="-3.1165" y2="1.8745" layer="21"/>
<rectangle x1="-2.2425" y1="1.8515" x2="-1.5295" y2="1.8745" layer="21"/>
<rectangle x1="-0.4255" y1="1.8515" x2="0.3335" y2="1.8745" layer="21"/>
<rectangle x1="1.4375" y1="1.8515" x2="2.1505" y2="1.8745" layer="21"/>
<rectangle x1="3.0245" y1="1.8515" x2="3.7375" y2="1.8745" layer="21"/>
<rectangle x1="-3.8065" y1="1.8745" x2="-3.1165" y2="1.8975" layer="21"/>
<rectangle x1="-2.2425" y1="1.8745" x2="-1.5295" y2="1.8975" layer="21"/>
<rectangle x1="-0.4255" y1="1.8745" x2="0.3335" y2="1.8975" layer="21"/>
<rectangle x1="1.4375" y1="1.8745" x2="2.1505" y2="1.8975" layer="21"/>
<rectangle x1="3.0245" y1="1.8745" x2="3.7375" y2="1.8975" layer="21"/>
<rectangle x1="-3.8065" y1="1.8975" x2="-3.1165" y2="1.9205" layer="21"/>
<rectangle x1="-2.2425" y1="1.8975" x2="-1.5065" y2="1.9205" layer="21"/>
<rectangle x1="-0.4255" y1="1.8975" x2="0.3335" y2="1.9205" layer="21"/>
<rectangle x1="1.4375" y1="1.8975" x2="2.1505" y2="1.9205" layer="21"/>
<rectangle x1="3.0245" y1="1.8975" x2="3.7375" y2="1.9205" layer="21"/>
<rectangle x1="-3.8065" y1="1.9205" x2="-3.1165" y2="1.9435" layer="21"/>
<rectangle x1="-2.2195" y1="1.9205" x2="-1.5065" y2="1.9435" layer="21"/>
<rectangle x1="-0.4255" y1="1.9205" x2="0.3335" y2="1.9435" layer="21"/>
<rectangle x1="1.4375" y1="1.9205" x2="2.1505" y2="1.9435" layer="21"/>
<rectangle x1="3.0245" y1="1.9205" x2="3.7375" y2="1.9435" layer="21"/>
<rectangle x1="-3.8065" y1="1.9435" x2="-3.1165" y2="1.9665" layer="21"/>
<rectangle x1="-2.2195" y1="1.9435" x2="-1.5065" y2="1.9665" layer="21"/>
<rectangle x1="-0.4255" y1="1.9435" x2="0.3335" y2="1.9665" layer="21"/>
<rectangle x1="1.4375" y1="1.9435" x2="2.1275" y2="1.9665" layer="21"/>
<rectangle x1="3.0245" y1="1.9435" x2="3.7375" y2="1.9665" layer="21"/>
<rectangle x1="-3.8065" y1="1.9665" x2="-3.1165" y2="1.9895" layer="21"/>
<rectangle x1="-2.2195" y1="1.9665" x2="-1.5065" y2="1.9895" layer="21"/>
<rectangle x1="-0.4255" y1="1.9665" x2="0.3335" y2="1.9895" layer="21"/>
<rectangle x1="1.4375" y1="1.9665" x2="2.1275" y2="1.9895" layer="21"/>
<rectangle x1="3.0245" y1="1.9665" x2="3.7375" y2="1.9895" layer="21"/>
<rectangle x1="-3.8065" y1="1.9895" x2="-3.1165" y2="2.0125" layer="21"/>
<rectangle x1="-2.2195" y1="1.9895" x2="-1.5065" y2="2.0125" layer="21"/>
<rectangle x1="-0.4255" y1="1.9895" x2="0.3335" y2="2.0125" layer="21"/>
<rectangle x1="1.4375" y1="1.9895" x2="2.1275" y2="2.0125" layer="21"/>
<rectangle x1="3.0475" y1="1.9895" x2="3.7375" y2="2.0125" layer="21"/>
<rectangle x1="-3.8065" y1="2.0125" x2="-3.1165" y2="2.0355" layer="21"/>
<rectangle x1="-2.2195" y1="2.0125" x2="-1.5065" y2="2.0355" layer="21"/>
<rectangle x1="-0.4255" y1="2.0125" x2="0.3335" y2="2.0355" layer="21"/>
<rectangle x1="1.4375" y1="2.0125" x2="2.1275" y2="2.0355" layer="21"/>
<rectangle x1="3.0475" y1="2.0125" x2="3.7375" y2="2.0355" layer="21"/>
<rectangle x1="-3.8065" y1="2.0355" x2="-3.1165" y2="2.0585" layer="21"/>
<rectangle x1="-2.2195" y1="2.0355" x2="-1.5065" y2="2.0585" layer="21"/>
<rectangle x1="-0.4255" y1="2.0355" x2="0.3335" y2="2.0585" layer="21"/>
<rectangle x1="1.4375" y1="2.0355" x2="2.1275" y2="2.0585" layer="21"/>
<rectangle x1="3.0475" y1="2.0355" x2="3.7375" y2="2.0585" layer="21"/>
<rectangle x1="-3.8065" y1="2.0585" x2="-3.1165" y2="2.0815" layer="21"/>
<rectangle x1="-2.2195" y1="2.0585" x2="-1.5065" y2="2.0815" layer="21"/>
<rectangle x1="-0.4255" y1="2.0585" x2="0.3335" y2="2.0815" layer="21"/>
<rectangle x1="1.4375" y1="2.0585" x2="2.1275" y2="2.0815" layer="21"/>
<rectangle x1="3.0475" y1="2.0585" x2="3.7375" y2="2.0815" layer="21"/>
<rectangle x1="-3.8065" y1="2.0815" x2="-3.1165" y2="2.1045" layer="21"/>
<rectangle x1="-2.2195" y1="2.0815" x2="-1.5065" y2="2.1045" layer="21"/>
<rectangle x1="-0.4255" y1="2.0815" x2="0.3335" y2="2.1045" layer="21"/>
<rectangle x1="1.4375" y1="2.0815" x2="2.1275" y2="2.1045" layer="21"/>
<rectangle x1="3.0475" y1="2.0815" x2="3.7375" y2="2.1045" layer="21"/>
<rectangle x1="-3.8065" y1="2.1045" x2="-3.1165" y2="2.1275" layer="21"/>
<rectangle x1="-2.2195" y1="2.1045" x2="-1.5065" y2="2.1275" layer="21"/>
<rectangle x1="-0.4255" y1="2.1045" x2="0.3335" y2="2.1275" layer="21"/>
<rectangle x1="1.4375" y1="2.1045" x2="2.1275" y2="2.1275" layer="21"/>
<rectangle x1="3.0475" y1="2.1045" x2="3.7375" y2="2.1275" layer="21"/>
<rectangle x1="-3.8065" y1="2.1275" x2="-3.1165" y2="2.1505" layer="21"/>
<rectangle x1="-2.2195" y1="2.1275" x2="-1.5065" y2="2.1505" layer="21"/>
<rectangle x1="-0.4255" y1="2.1275" x2="0.3335" y2="2.1505" layer="21"/>
<rectangle x1="1.4375" y1="2.1275" x2="2.1275" y2="2.1505" layer="21"/>
<rectangle x1="3.0475" y1="2.1275" x2="3.7375" y2="2.1505" layer="21"/>
<rectangle x1="-3.8065" y1="2.1505" x2="-3.1165" y2="2.1735" layer="21"/>
<rectangle x1="-2.2195" y1="2.1505" x2="-1.5065" y2="2.1735" layer="21"/>
<rectangle x1="-0.4255" y1="2.1505" x2="0.3335" y2="2.1735" layer="21"/>
<rectangle x1="1.4375" y1="2.1505" x2="2.1275" y2="2.1735" layer="21"/>
<rectangle x1="3.0475" y1="2.1505" x2="3.7375" y2="2.1735" layer="21"/>
<rectangle x1="-3.8065" y1="2.1735" x2="-3.1165" y2="2.1965" layer="21"/>
<rectangle x1="-2.1965" y1="2.1735" x2="-1.5065" y2="2.1965" layer="21"/>
<rectangle x1="-0.4255" y1="2.1735" x2="0.3335" y2="2.1965" layer="21"/>
<rectangle x1="1.4375" y1="2.1735" x2="2.1275" y2="2.1965" layer="21"/>
<rectangle x1="3.0475" y1="2.1735" x2="3.7375" y2="2.1965" layer="21"/>
<rectangle x1="-3.8065" y1="2.1965" x2="-3.1165" y2="2.2195" layer="21"/>
<rectangle x1="-2.1965" y1="2.1965" x2="-1.5065" y2="2.2195" layer="21"/>
<rectangle x1="-0.4255" y1="2.1965" x2="0.3335" y2="2.2195" layer="21"/>
<rectangle x1="1.4375" y1="2.1965" x2="2.1275" y2="2.2195" layer="21"/>
<rectangle x1="3.0475" y1="2.1965" x2="3.7375" y2="2.2195" layer="21"/>
<rectangle x1="-3.8065" y1="2.2195" x2="-3.1165" y2="2.2425" layer="21"/>
<rectangle x1="-2.1965" y1="2.2195" x2="-1.4835" y2="2.2425" layer="21"/>
<rectangle x1="-0.4255" y1="2.2195" x2="0.3335" y2="2.2425" layer="21"/>
<rectangle x1="1.4375" y1="2.2195" x2="2.1275" y2="2.2425" layer="21"/>
<rectangle x1="3.0475" y1="2.2195" x2="3.7375" y2="2.2425" layer="21"/>
<rectangle x1="-3.8065" y1="2.2425" x2="-3.1165" y2="2.2655" layer="21"/>
<rectangle x1="-2.1965" y1="2.2425" x2="-1.4835" y2="2.2655" layer="21"/>
<rectangle x1="-0.4255" y1="2.2425" x2="0.3335" y2="2.2655" layer="21"/>
<rectangle x1="1.4375" y1="2.2425" x2="2.1275" y2="2.2655" layer="21"/>
<rectangle x1="3.0475" y1="2.2425" x2="3.7375" y2="2.2655" layer="21"/>
<rectangle x1="-3.8065" y1="2.2655" x2="-3.1165" y2="2.2885" layer="21"/>
<rectangle x1="-2.1965" y1="2.2655" x2="-1.4835" y2="2.2885" layer="21"/>
<rectangle x1="-0.4255" y1="2.2655" x2="0.3335" y2="2.2885" layer="21"/>
<rectangle x1="1.4375" y1="2.2655" x2="2.1275" y2="2.2885" layer="21"/>
<rectangle x1="3.0475" y1="2.2655" x2="3.7375" y2="2.2885" layer="21"/>
<rectangle x1="-3.8065" y1="2.2885" x2="-3.1165" y2="2.3115" layer="21"/>
<rectangle x1="-2.1965" y1="2.2885" x2="-1.4835" y2="2.3115" layer="21"/>
<rectangle x1="-0.4255" y1="2.2885" x2="0.3335" y2="2.3115" layer="21"/>
<rectangle x1="1.4375" y1="2.2885" x2="2.1275" y2="2.3115" layer="21"/>
<rectangle x1="3.0475" y1="2.2885" x2="3.7375" y2="2.3115" layer="21"/>
<rectangle x1="-3.8065" y1="2.3115" x2="-3.1165" y2="2.3345" layer="21"/>
<rectangle x1="-2.1965" y1="2.3115" x2="-1.4835" y2="2.3345" layer="21"/>
<rectangle x1="-0.4255" y1="2.3115" x2="0.3335" y2="2.3345" layer="21"/>
<rectangle x1="1.4375" y1="2.3115" x2="2.1275" y2="2.3345" layer="21"/>
<rectangle x1="3.0475" y1="2.3115" x2="3.7375" y2="2.3345" layer="21"/>
<rectangle x1="-3.8065" y1="2.3345" x2="-3.1165" y2="2.3575" layer="21"/>
<rectangle x1="-2.1965" y1="2.3345" x2="-1.4835" y2="2.3575" layer="21"/>
<rectangle x1="-0.4255" y1="2.3345" x2="0.3335" y2="2.3575" layer="21"/>
<rectangle x1="1.4375" y1="2.3345" x2="2.1275" y2="2.3575" layer="21"/>
<rectangle x1="3.0475" y1="2.3345" x2="3.7375" y2="2.3575" layer="21"/>
<rectangle x1="-3.8065" y1="2.3575" x2="-3.1165" y2="2.3805" layer="21"/>
<rectangle x1="-2.1965" y1="2.3575" x2="-1.4835" y2="2.3805" layer="21"/>
<rectangle x1="-0.4255" y1="2.3575" x2="0.3335" y2="2.3805" layer="21"/>
<rectangle x1="1.4375" y1="2.3575" x2="2.1275" y2="2.3805" layer="21"/>
<rectangle x1="3.0475" y1="2.3575" x2="3.7375" y2="2.3805" layer="21"/>
<rectangle x1="-3.8065" y1="2.3805" x2="-3.1165" y2="2.4035" layer="21"/>
<rectangle x1="-2.1965" y1="2.3805" x2="-1.4835" y2="2.4035" layer="21"/>
<rectangle x1="-0.4255" y1="2.3805" x2="0.3335" y2="2.4035" layer="21"/>
<rectangle x1="1.4375" y1="2.3805" x2="2.1275" y2="2.4035" layer="21"/>
<rectangle x1="3.0475" y1="2.3805" x2="3.7375" y2="2.4035" layer="21"/>
<rectangle x1="-3.8065" y1="2.4035" x2="-3.1165" y2="2.4265" layer="21"/>
<rectangle x1="-2.1965" y1="2.4035" x2="-1.4835" y2="2.4265" layer="21"/>
<rectangle x1="-0.4255" y1="2.4035" x2="0.3335" y2="2.4265" layer="21"/>
<rectangle x1="1.4375" y1="2.4035" x2="2.1275" y2="2.4265" layer="21"/>
<rectangle x1="3.0475" y1="2.4035" x2="3.7375" y2="2.4265" layer="21"/>
<rectangle x1="-3.8065" y1="2.4265" x2="-3.1165" y2="2.4495" layer="21"/>
<rectangle x1="-2.1965" y1="2.4265" x2="-1.4835" y2="2.4495" layer="21"/>
<rectangle x1="-0.4255" y1="2.4265" x2="0.3335" y2="2.4495" layer="21"/>
<rectangle x1="1.4375" y1="2.4265" x2="2.1275" y2="2.4495" layer="21"/>
<rectangle x1="3.0475" y1="2.4265" x2="3.7375" y2="2.4495" layer="21"/>
<rectangle x1="-3.8065" y1="2.4495" x2="-3.1165" y2="2.4725" layer="21"/>
<rectangle x1="-2.1965" y1="2.4495" x2="-1.4835" y2="2.4725" layer="21"/>
<rectangle x1="-0.4255" y1="2.4495" x2="0.3335" y2="2.4725" layer="21"/>
<rectangle x1="1.4375" y1="2.4495" x2="2.1275" y2="2.4725" layer="21"/>
<rectangle x1="3.0475" y1="2.4495" x2="3.7375" y2="2.4725" layer="21"/>
<rectangle x1="-3.8065" y1="2.4725" x2="-3.1165" y2="2.4955" layer="21"/>
<rectangle x1="-2.1965" y1="2.4725" x2="-1.4835" y2="2.4955" layer="21"/>
<rectangle x1="-0.4255" y1="2.4725" x2="0.3335" y2="2.4955" layer="21"/>
<rectangle x1="1.4375" y1="2.4725" x2="2.1275" y2="2.4955" layer="21"/>
<rectangle x1="3.0475" y1="2.4725" x2="3.7375" y2="2.4955" layer="21"/>
<rectangle x1="-3.8065" y1="2.4955" x2="-3.1165" y2="2.5185" layer="21"/>
<rectangle x1="-2.1965" y1="2.4955" x2="-1.4835" y2="2.5185" layer="21"/>
<rectangle x1="-0.4255" y1="2.4955" x2="0.3335" y2="2.5185" layer="21"/>
<rectangle x1="1.4375" y1="2.4955" x2="2.1275" y2="2.5185" layer="21"/>
<rectangle x1="3.0475" y1="2.4955" x2="3.7375" y2="2.5185" layer="21"/>
<rectangle x1="-3.8065" y1="2.5185" x2="-3.1165" y2="2.5415" layer="21"/>
<rectangle x1="-2.1965" y1="2.5185" x2="-1.4835" y2="2.5415" layer="21"/>
<rectangle x1="-0.4255" y1="2.5185" x2="0.3335" y2="2.5415" layer="21"/>
<rectangle x1="1.4375" y1="2.5185" x2="2.1275" y2="2.5415" layer="21"/>
<rectangle x1="3.0475" y1="2.5185" x2="3.7375" y2="2.5415" layer="21"/>
<rectangle x1="-3.8065" y1="2.5415" x2="-3.1165" y2="2.5645" layer="21"/>
<rectangle x1="-2.1965" y1="2.5415" x2="-1.4835" y2="2.5645" layer="21"/>
<rectangle x1="-0.4255" y1="2.5415" x2="0.3335" y2="2.5645" layer="21"/>
<rectangle x1="1.4375" y1="2.5415" x2="2.1275" y2="2.5645" layer="21"/>
<rectangle x1="3.0475" y1="2.5415" x2="3.7375" y2="2.5645" layer="21"/>
<rectangle x1="-3.8065" y1="2.5645" x2="-3.1165" y2="2.5875" layer="21"/>
<rectangle x1="-2.1965" y1="2.5645" x2="-1.4835" y2="2.5875" layer="21"/>
<rectangle x1="-0.4255" y1="2.5645" x2="0.3335" y2="2.5875" layer="21"/>
<rectangle x1="1.4375" y1="2.5645" x2="2.1275" y2="2.5875" layer="21"/>
<rectangle x1="3.0475" y1="2.5645" x2="3.7375" y2="2.5875" layer="21"/>
<rectangle x1="-3.8065" y1="2.5875" x2="-3.1165" y2="2.6105" layer="21"/>
<rectangle x1="-2.1965" y1="2.5875" x2="-1.4835" y2="2.6105" layer="21"/>
<rectangle x1="-0.4255" y1="2.5875" x2="0.3335" y2="2.6105" layer="21"/>
<rectangle x1="1.4375" y1="2.5875" x2="2.1275" y2="2.6105" layer="21"/>
<rectangle x1="3.0475" y1="2.5875" x2="3.7375" y2="2.6105" layer="21"/>
<rectangle x1="-3.8065" y1="2.6105" x2="-3.1165" y2="2.6335" layer="21"/>
<rectangle x1="-2.1965" y1="2.6105" x2="-1.4835" y2="2.6335" layer="21"/>
<rectangle x1="-0.4255" y1="2.6105" x2="0.3335" y2="2.6335" layer="21"/>
<rectangle x1="1.4375" y1="2.6105" x2="2.1275" y2="2.6335" layer="21"/>
<rectangle x1="3.0475" y1="2.6105" x2="3.7375" y2="2.6335" layer="21"/>
<rectangle x1="-3.8065" y1="2.6335" x2="-3.1165" y2="2.6565" layer="21"/>
<rectangle x1="-2.1965" y1="2.6335" x2="-1.4835" y2="2.6565" layer="21"/>
<rectangle x1="-0.4255" y1="2.6335" x2="0.3335" y2="2.6565" layer="21"/>
<rectangle x1="1.4375" y1="2.6335" x2="2.1275" y2="2.6565" layer="21"/>
<rectangle x1="3.0475" y1="2.6335" x2="3.7375" y2="2.6565" layer="21"/>
<rectangle x1="-3.8065" y1="2.6565" x2="-3.1165" y2="2.6795" layer="21"/>
<rectangle x1="-2.1965" y1="2.6565" x2="-1.4835" y2="2.6795" layer="21"/>
<rectangle x1="-0.4255" y1="2.6565" x2="0.3335" y2="2.6795" layer="21"/>
<rectangle x1="1.4375" y1="2.6565" x2="2.1275" y2="2.6795" layer="21"/>
<rectangle x1="3.0475" y1="2.6565" x2="3.7375" y2="2.6795" layer="21"/>
<rectangle x1="-3.8065" y1="2.6795" x2="-3.1165" y2="2.7025" layer="21"/>
<rectangle x1="-2.1965" y1="2.6795" x2="-1.4835" y2="2.7025" layer="21"/>
<rectangle x1="-0.4255" y1="2.6795" x2="0.3335" y2="2.7025" layer="21"/>
<rectangle x1="1.4375" y1="2.6795" x2="2.1275" y2="2.7025" layer="21"/>
<rectangle x1="3.0475" y1="2.6795" x2="3.7375" y2="2.7025" layer="21"/>
<rectangle x1="-3.8065" y1="2.7025" x2="-3.1165" y2="2.7255" layer="21"/>
<rectangle x1="-2.1965" y1="2.7025" x2="-1.4835" y2="2.7255" layer="21"/>
<rectangle x1="-0.4255" y1="2.7025" x2="0.3335" y2="2.7255" layer="21"/>
<rectangle x1="1.4375" y1="2.7025" x2="2.1275" y2="2.7255" layer="21"/>
<rectangle x1="3.0475" y1="2.7025" x2="3.7375" y2="2.7255" layer="21"/>
<rectangle x1="-3.8065" y1="2.7255" x2="-3.1165" y2="2.7485" layer="21"/>
<rectangle x1="-2.1965" y1="2.7255" x2="-1.4835" y2="2.7485" layer="21"/>
<rectangle x1="-0.4255" y1="2.7255" x2="0.3335" y2="2.7485" layer="21"/>
<rectangle x1="1.4375" y1="2.7255" x2="2.1275" y2="2.7485" layer="21"/>
<rectangle x1="3.0475" y1="2.7255" x2="3.7375" y2="2.7485" layer="21"/>
<rectangle x1="-3.8065" y1="2.7485" x2="-3.1165" y2="2.7715" layer="21"/>
<rectangle x1="-2.1965" y1="2.7485" x2="-1.4835" y2="2.7715" layer="21"/>
<rectangle x1="-0.4255" y1="2.7485" x2="0.3335" y2="2.7715" layer="21"/>
<rectangle x1="1.4375" y1="2.7485" x2="2.1275" y2="2.7715" layer="21"/>
<rectangle x1="3.0475" y1="2.7485" x2="3.7375" y2="2.7715" layer="21"/>
<rectangle x1="-3.8065" y1="2.7715" x2="-3.1165" y2="2.7945" layer="21"/>
<rectangle x1="-2.1965" y1="2.7715" x2="-1.4835" y2="2.7945" layer="21"/>
<rectangle x1="-0.4255" y1="2.7715" x2="0.3335" y2="2.7945" layer="21"/>
<rectangle x1="1.4375" y1="2.7715" x2="2.1275" y2="2.7945" layer="21"/>
<rectangle x1="3.0475" y1="2.7715" x2="3.7375" y2="2.7945" layer="21"/>
<rectangle x1="-3.8065" y1="2.7945" x2="-3.1165" y2="2.8175" layer="21"/>
<rectangle x1="-2.1965" y1="2.7945" x2="-1.4835" y2="2.8175" layer="21"/>
<rectangle x1="-0.4255" y1="2.7945" x2="0.3335" y2="2.8175" layer="21"/>
<rectangle x1="1.4375" y1="2.7945" x2="2.1275" y2="2.8175" layer="21"/>
<rectangle x1="3.0475" y1="2.7945" x2="3.7375" y2="2.8175" layer="21"/>
<rectangle x1="-3.8065" y1="2.8175" x2="-3.1165" y2="2.8405" layer="21"/>
<rectangle x1="-2.1965" y1="2.8175" x2="-1.4835" y2="2.8405" layer="21"/>
<rectangle x1="-0.4255" y1="2.8175" x2="0.3335" y2="2.8405" layer="21"/>
<rectangle x1="1.4375" y1="2.8175" x2="2.1275" y2="2.8405" layer="21"/>
<rectangle x1="3.0475" y1="2.8175" x2="3.7375" y2="2.8405" layer="21"/>
<rectangle x1="-3.8065" y1="2.8405" x2="-3.1165" y2="2.8635" layer="21"/>
<rectangle x1="-2.1965" y1="2.8405" x2="-1.4835" y2="2.8635" layer="21"/>
<rectangle x1="-0.4255" y1="2.8405" x2="0.3335" y2="2.8635" layer="21"/>
<rectangle x1="1.4375" y1="2.8405" x2="2.1275" y2="2.8635" layer="21"/>
<rectangle x1="3.0475" y1="2.8405" x2="3.7375" y2="2.8635" layer="21"/>
<rectangle x1="-3.8065" y1="2.8635" x2="-3.1165" y2="2.8865" layer="21"/>
<rectangle x1="-2.1965" y1="2.8635" x2="-1.4835" y2="2.8865" layer="21"/>
<rectangle x1="-0.4255" y1="2.8635" x2="0.3335" y2="2.8865" layer="21"/>
<rectangle x1="1.4375" y1="2.8635" x2="2.1275" y2="2.8865" layer="21"/>
<rectangle x1="3.0475" y1="2.8635" x2="3.7375" y2="2.8865" layer="21"/>
<rectangle x1="-3.8065" y1="2.8865" x2="-3.1165" y2="2.9095" layer="21"/>
<rectangle x1="-2.1965" y1="2.8865" x2="-1.4835" y2="2.9095" layer="21"/>
<rectangle x1="-0.4255" y1="2.8865" x2="0.3335" y2="2.9095" layer="21"/>
<rectangle x1="1.4375" y1="2.8865" x2="2.1275" y2="2.9095" layer="21"/>
<rectangle x1="3.0475" y1="2.8865" x2="3.7375" y2="2.9095" layer="21"/>
<rectangle x1="-3.8065" y1="2.9095" x2="-3.1165" y2="2.9325" layer="21"/>
<rectangle x1="-2.1965" y1="2.9095" x2="-1.4835" y2="2.9325" layer="21"/>
<rectangle x1="-0.4255" y1="2.9095" x2="0.3335" y2="2.9325" layer="21"/>
<rectangle x1="1.4375" y1="2.9095" x2="2.1275" y2="2.9325" layer="21"/>
<rectangle x1="3.0475" y1="2.9095" x2="3.7375" y2="2.9325" layer="21"/>
<rectangle x1="-3.8065" y1="2.9325" x2="-3.1165" y2="2.9555" layer="21"/>
<rectangle x1="-2.1965" y1="2.9325" x2="-1.4835" y2="2.9555" layer="21"/>
<rectangle x1="-0.4255" y1="2.9325" x2="0.3335" y2="2.9555" layer="21"/>
<rectangle x1="1.4375" y1="2.9325" x2="2.1275" y2="2.9555" layer="21"/>
<rectangle x1="3.0475" y1="2.9325" x2="3.7375" y2="2.9555" layer="21"/>
<rectangle x1="-3.8065" y1="2.9555" x2="-3.1165" y2="2.9785" layer="21"/>
<rectangle x1="-2.1965" y1="2.9555" x2="-1.4835" y2="2.9785" layer="21"/>
<rectangle x1="-0.4255" y1="2.9555" x2="0.3335" y2="2.9785" layer="21"/>
<rectangle x1="1.4375" y1="2.9555" x2="2.1275" y2="2.9785" layer="21"/>
<rectangle x1="3.0475" y1="2.9555" x2="3.7375" y2="2.9785" layer="21"/>
<rectangle x1="-3.8065" y1="2.9785" x2="-3.1165" y2="3.0015" layer="21"/>
<rectangle x1="-2.1965" y1="2.9785" x2="-1.4835" y2="3.0015" layer="21"/>
<rectangle x1="-0.4255" y1="2.9785" x2="0.3335" y2="3.0015" layer="21"/>
<rectangle x1="1.4375" y1="2.9785" x2="2.1275" y2="3.0015" layer="21"/>
<rectangle x1="3.0475" y1="2.9785" x2="3.7375" y2="3.0015" layer="21"/>
<rectangle x1="-3.8065" y1="3.0015" x2="-3.1165" y2="3.0245" layer="21"/>
<rectangle x1="-2.1965" y1="3.0015" x2="-1.4835" y2="3.0245" layer="21"/>
<rectangle x1="-0.4255" y1="3.0015" x2="0.3335" y2="3.0245" layer="21"/>
<rectangle x1="1.4375" y1="3.0015" x2="2.1275" y2="3.0245" layer="21"/>
<rectangle x1="3.0475" y1="3.0015" x2="3.7375" y2="3.0245" layer="21"/>
<rectangle x1="-3.8065" y1="3.0245" x2="-3.1165" y2="3.0475" layer="21"/>
<rectangle x1="-2.1965" y1="3.0245" x2="-1.4835" y2="3.0475" layer="21"/>
<rectangle x1="-0.4255" y1="3.0245" x2="0.3335" y2="3.0475" layer="21"/>
<rectangle x1="1.4375" y1="3.0245" x2="2.1275" y2="3.0475" layer="21"/>
<rectangle x1="3.0475" y1="3.0245" x2="3.7375" y2="3.0475" layer="21"/>
<rectangle x1="-3.8065" y1="3.0475" x2="-3.1165" y2="3.0705" layer="21"/>
<rectangle x1="-2.1965" y1="3.0475" x2="-1.4835" y2="3.0705" layer="21"/>
<rectangle x1="-0.4255" y1="3.0475" x2="0.3335" y2="3.0705" layer="21"/>
<rectangle x1="1.4375" y1="3.0475" x2="2.1275" y2="3.0705" layer="21"/>
<rectangle x1="3.0475" y1="3.0475" x2="3.7375" y2="3.0705" layer="21"/>
<rectangle x1="-3.8065" y1="3.0705" x2="-3.1165" y2="3.0935" layer="21"/>
<rectangle x1="-2.1965" y1="3.0705" x2="-1.4835" y2="3.0935" layer="21"/>
<rectangle x1="-0.4255" y1="3.0705" x2="0.3335" y2="3.0935" layer="21"/>
<rectangle x1="1.4375" y1="3.0705" x2="2.1275" y2="3.0935" layer="21"/>
<rectangle x1="3.0475" y1="3.0705" x2="3.7375" y2="3.0935" layer="21"/>
<rectangle x1="-3.8065" y1="3.0935" x2="-3.1165" y2="3.1165" layer="21"/>
<rectangle x1="-2.1965" y1="3.0935" x2="-1.4835" y2="3.1165" layer="21"/>
<rectangle x1="-0.4255" y1="3.0935" x2="0.3335" y2="3.1165" layer="21"/>
<rectangle x1="1.4375" y1="3.0935" x2="2.1275" y2="3.1165" layer="21"/>
<rectangle x1="3.0475" y1="3.0935" x2="3.7375" y2="3.1165" layer="21"/>
<rectangle x1="-3.8065" y1="3.1165" x2="-3.1165" y2="3.1395" layer="21"/>
<rectangle x1="-2.1965" y1="3.1165" x2="-1.4835" y2="3.1395" layer="21"/>
<rectangle x1="-0.4255" y1="3.1165" x2="0.3335" y2="3.1395" layer="21"/>
<rectangle x1="1.4375" y1="3.1165" x2="2.1275" y2="3.1395" layer="21"/>
<rectangle x1="3.0475" y1="3.1165" x2="3.7375" y2="3.1395" layer="21"/>
<rectangle x1="-3.8065" y1="3.1395" x2="-3.1165" y2="3.1625" layer="21"/>
<rectangle x1="-2.1965" y1="3.1395" x2="-1.4835" y2="3.1625" layer="21"/>
<rectangle x1="-0.4255" y1="3.1395" x2="0.3335" y2="3.1625" layer="21"/>
<rectangle x1="1.4375" y1="3.1395" x2="2.1275" y2="3.1625" layer="21"/>
<rectangle x1="3.0475" y1="3.1395" x2="3.7375" y2="3.1625" layer="21"/>
<rectangle x1="-3.8065" y1="3.1625" x2="-3.1165" y2="3.1855" layer="21"/>
<rectangle x1="-2.1965" y1="3.1625" x2="-1.4835" y2="3.1855" layer="21"/>
<rectangle x1="-0.4255" y1="3.1625" x2="0.3335" y2="3.1855" layer="21"/>
<rectangle x1="1.4375" y1="3.1625" x2="2.1275" y2="3.1855" layer="21"/>
<rectangle x1="3.0475" y1="3.1625" x2="3.7375" y2="3.1855" layer="21"/>
<rectangle x1="-3.8065" y1="3.1855" x2="-3.1165" y2="3.2085" layer="21"/>
<rectangle x1="-2.1965" y1="3.1855" x2="-1.4835" y2="3.2085" layer="21"/>
<rectangle x1="-0.4255" y1="3.1855" x2="0.3335" y2="3.2085" layer="21"/>
<rectangle x1="1.4375" y1="3.1855" x2="2.1275" y2="3.2085" layer="21"/>
<rectangle x1="3.0475" y1="3.1855" x2="3.7375" y2="3.2085" layer="21"/>
<rectangle x1="-3.8065" y1="3.2085" x2="-3.1165" y2="3.2315" layer="21"/>
<rectangle x1="-2.1965" y1="3.2085" x2="-1.4835" y2="3.2315" layer="21"/>
<rectangle x1="-0.4255" y1="3.2085" x2="0.3335" y2="3.2315" layer="21"/>
<rectangle x1="1.4375" y1="3.2085" x2="2.1275" y2="3.2315" layer="21"/>
<rectangle x1="3.0475" y1="3.2085" x2="3.7375" y2="3.2315" layer="21"/>
<rectangle x1="-3.8065" y1="3.2315" x2="-3.1165" y2="3.2545" layer="21"/>
<rectangle x1="-2.1965" y1="3.2315" x2="-1.4835" y2="3.2545" layer="21"/>
<rectangle x1="-0.4255" y1="3.2315" x2="0.3335" y2="3.2545" layer="21"/>
<rectangle x1="1.4375" y1="3.2315" x2="2.1275" y2="3.2545" layer="21"/>
<rectangle x1="3.0475" y1="3.2315" x2="3.7375" y2="3.2545" layer="21"/>
<rectangle x1="-3.8065" y1="3.2545" x2="-3.1165" y2="3.2775" layer="21"/>
<rectangle x1="-2.1965" y1="3.2545" x2="-1.4835" y2="3.2775" layer="21"/>
<rectangle x1="-0.4255" y1="3.2545" x2="0.3335" y2="3.2775" layer="21"/>
<rectangle x1="1.4375" y1="3.2545" x2="2.1275" y2="3.2775" layer="21"/>
<rectangle x1="3.0475" y1="3.2545" x2="3.7375" y2="3.2775" layer="21"/>
<rectangle x1="-3.8065" y1="3.2775" x2="-3.1165" y2="3.3005" layer="21"/>
<rectangle x1="-2.1965" y1="3.2775" x2="-1.4835" y2="3.3005" layer="21"/>
<rectangle x1="-0.4255" y1="3.2775" x2="0.3335" y2="3.3005" layer="21"/>
<rectangle x1="1.4375" y1="3.2775" x2="2.1275" y2="3.3005" layer="21"/>
<rectangle x1="3.0475" y1="3.2775" x2="3.7375" y2="3.3005" layer="21"/>
<rectangle x1="-3.8065" y1="3.3005" x2="-3.1165" y2="3.3235" layer="21"/>
<rectangle x1="-2.1965" y1="3.3005" x2="-1.4835" y2="3.3235" layer="21"/>
<rectangle x1="-0.4255" y1="3.3005" x2="0.3335" y2="3.3235" layer="21"/>
<rectangle x1="1.4375" y1="3.3005" x2="2.1275" y2="3.3235" layer="21"/>
<rectangle x1="3.0475" y1="3.3005" x2="3.7375" y2="3.3235" layer="21"/>
<rectangle x1="-3.8065" y1="3.3235" x2="-3.1165" y2="3.3465" layer="21"/>
<rectangle x1="-2.1965" y1="3.3235" x2="-1.4835" y2="3.3465" layer="21"/>
<rectangle x1="-0.4255" y1="3.3235" x2="0.3335" y2="3.3465" layer="21"/>
<rectangle x1="1.4375" y1="3.3235" x2="2.1275" y2="3.3465" layer="21"/>
<rectangle x1="3.0475" y1="3.3235" x2="3.7375" y2="3.3465" layer="21"/>
<rectangle x1="-3.8065" y1="3.3465" x2="-3.1165" y2="3.3695" layer="21"/>
<rectangle x1="-2.1965" y1="3.3465" x2="-1.4835" y2="3.3695" layer="21"/>
<rectangle x1="-0.4255" y1="3.3465" x2="0.3335" y2="3.3695" layer="21"/>
<rectangle x1="1.4375" y1="3.3465" x2="2.1275" y2="3.3695" layer="21"/>
<rectangle x1="3.0475" y1="3.3465" x2="3.7375" y2="3.3695" layer="21"/>
<rectangle x1="-3.8065" y1="3.3695" x2="-3.1165" y2="3.3925" layer="21"/>
<rectangle x1="-2.1965" y1="3.3695" x2="-1.4835" y2="3.3925" layer="21"/>
<rectangle x1="-0.4255" y1="3.3695" x2="0.3335" y2="3.3925" layer="21"/>
<rectangle x1="1.4375" y1="3.3695" x2="2.1275" y2="3.3925" layer="21"/>
<rectangle x1="3.0475" y1="3.3695" x2="3.7375" y2="3.3925" layer="21"/>
<rectangle x1="-3.8065" y1="3.3925" x2="-3.1165" y2="3.4155" layer="21"/>
<rectangle x1="-2.1965" y1="3.3925" x2="-1.4835" y2="3.4155" layer="21"/>
<rectangle x1="-0.4255" y1="3.3925" x2="0.3335" y2="3.4155" layer="21"/>
<rectangle x1="1.4375" y1="3.3925" x2="2.1275" y2="3.4155" layer="21"/>
<rectangle x1="3.0475" y1="3.3925" x2="3.7375" y2="3.4155" layer="21"/>
<rectangle x1="-3.8065" y1="3.4155" x2="-3.1165" y2="3.4385" layer="21"/>
<rectangle x1="-2.1965" y1="3.4155" x2="-1.4835" y2="3.4385" layer="21"/>
<rectangle x1="-0.4255" y1="3.4155" x2="0.3335" y2="3.4385" layer="21"/>
<rectangle x1="1.4375" y1="3.4155" x2="2.1275" y2="3.4385" layer="21"/>
<rectangle x1="3.0475" y1="3.4155" x2="3.7375" y2="3.4385" layer="21"/>
<rectangle x1="-3.8065" y1="3.4385" x2="-3.1165" y2="3.4615" layer="21"/>
<rectangle x1="-2.1965" y1="3.4385" x2="-1.4835" y2="3.4615" layer="21"/>
<rectangle x1="-0.4255" y1="3.4385" x2="0.3335" y2="3.4615" layer="21"/>
<rectangle x1="1.4375" y1="3.4385" x2="2.1275" y2="3.4615" layer="21"/>
<rectangle x1="3.0475" y1="3.4385" x2="3.7375" y2="3.4615" layer="21"/>
<rectangle x1="-3.8065" y1="3.4615" x2="-3.1165" y2="3.4845" layer="21"/>
<rectangle x1="-2.1965" y1="3.4615" x2="-1.4835" y2="3.4845" layer="21"/>
<rectangle x1="-0.4255" y1="3.4615" x2="0.3335" y2="3.4845" layer="21"/>
<rectangle x1="1.4375" y1="3.4615" x2="2.1275" y2="3.4845" layer="21"/>
<rectangle x1="3.0475" y1="3.4615" x2="3.7375" y2="3.4845" layer="21"/>
<rectangle x1="-3.8065" y1="3.4845" x2="-3.1165" y2="3.5075" layer="21"/>
<rectangle x1="-2.1965" y1="3.4845" x2="-1.4835" y2="3.5075" layer="21"/>
<rectangle x1="-0.4255" y1="3.4845" x2="0.3335" y2="3.5075" layer="21"/>
<rectangle x1="1.4375" y1="3.4845" x2="2.1275" y2="3.5075" layer="21"/>
<rectangle x1="3.0475" y1="3.4845" x2="3.7375" y2="3.5075" layer="21"/>
<rectangle x1="-3.8065" y1="3.5075" x2="-3.1165" y2="3.5305" layer="21"/>
<rectangle x1="-2.1965" y1="3.5075" x2="-1.4835" y2="3.5305" layer="21"/>
<rectangle x1="-0.4255" y1="3.5075" x2="0.3335" y2="3.5305" layer="21"/>
<rectangle x1="1.4375" y1="3.5075" x2="2.1275" y2="3.5305" layer="21"/>
<rectangle x1="3.0475" y1="3.5075" x2="3.7375" y2="3.5305" layer="21"/>
<rectangle x1="-3.8065" y1="3.5305" x2="-3.1165" y2="3.5535" layer="21"/>
<rectangle x1="-2.1965" y1="3.5305" x2="-1.4835" y2="3.5535" layer="21"/>
<rectangle x1="-0.4255" y1="3.5305" x2="0.3335" y2="3.5535" layer="21"/>
<rectangle x1="1.4375" y1="3.5305" x2="2.1275" y2="3.5535" layer="21"/>
<rectangle x1="3.0475" y1="3.5305" x2="3.7375" y2="3.5535" layer="21"/>
<rectangle x1="-3.8065" y1="3.5535" x2="-3.1165" y2="3.5765" layer="21"/>
<rectangle x1="-2.1965" y1="3.5535" x2="-1.4835" y2="3.5765" layer="21"/>
<rectangle x1="-0.4255" y1="3.5535" x2="0.3335" y2="3.5765" layer="21"/>
<rectangle x1="1.4375" y1="3.5535" x2="2.1275" y2="3.5765" layer="21"/>
<rectangle x1="3.0475" y1="3.5535" x2="3.7375" y2="3.5765" layer="21"/>
<rectangle x1="-3.8065" y1="3.5765" x2="-3.1165" y2="3.5995" layer="21"/>
<rectangle x1="-2.1965" y1="3.5765" x2="-1.4835" y2="3.5995" layer="21"/>
<rectangle x1="-0.4255" y1="3.5765" x2="0.3335" y2="3.5995" layer="21"/>
<rectangle x1="1.4375" y1="3.5765" x2="2.1275" y2="3.5995" layer="21"/>
<rectangle x1="3.0475" y1="3.5765" x2="3.7375" y2="3.5995" layer="21"/>
<rectangle x1="-3.8065" y1="3.5995" x2="-3.1165" y2="3.6225" layer="21"/>
<rectangle x1="-2.1965" y1="3.5995" x2="-1.4835" y2="3.6225" layer="21"/>
<rectangle x1="-0.4255" y1="3.5995" x2="0.3335" y2="3.6225" layer="21"/>
<rectangle x1="1.4375" y1="3.5995" x2="2.1275" y2="3.6225" layer="21"/>
<rectangle x1="3.0475" y1="3.5995" x2="3.7375" y2="3.6225" layer="21"/>
<rectangle x1="-3.8065" y1="3.6225" x2="-3.1165" y2="3.6455" layer="21"/>
<rectangle x1="-2.1965" y1="3.6225" x2="-1.4835" y2="3.6455" layer="21"/>
<rectangle x1="-0.4255" y1="3.6225" x2="0.3335" y2="3.6455" layer="21"/>
<rectangle x1="1.4375" y1="3.6225" x2="2.1275" y2="3.6455" layer="21"/>
<rectangle x1="3.0475" y1="3.6225" x2="3.7375" y2="3.6455" layer="21"/>
<rectangle x1="-3.8065" y1="3.6455" x2="-3.1165" y2="3.6685" layer="21"/>
<rectangle x1="-2.1965" y1="3.6455" x2="-1.4835" y2="3.6685" layer="21"/>
<rectangle x1="-0.4255" y1="3.6455" x2="0.3335" y2="3.6685" layer="21"/>
<rectangle x1="1.4375" y1="3.6455" x2="2.1275" y2="3.6685" layer="21"/>
<rectangle x1="3.0475" y1="3.6455" x2="3.7375" y2="3.6685" layer="21"/>
<rectangle x1="-3.8065" y1="3.6685" x2="-3.1165" y2="3.6915" layer="21"/>
<rectangle x1="-2.1965" y1="3.6685" x2="-1.4835" y2="3.6915" layer="21"/>
<rectangle x1="-0.4255" y1="3.6685" x2="0.3335" y2="3.6915" layer="21"/>
<rectangle x1="1.4375" y1="3.6685" x2="2.1275" y2="3.6915" layer="21"/>
<rectangle x1="3.0475" y1="3.6685" x2="3.7375" y2="3.6915" layer="21"/>
<rectangle x1="-3.8065" y1="3.6915" x2="-3.1165" y2="3.7145" layer="21"/>
<rectangle x1="-2.1965" y1="3.6915" x2="-1.4835" y2="3.7145" layer="21"/>
<rectangle x1="-0.4255" y1="3.6915" x2="0.3335" y2="3.7145" layer="21"/>
<rectangle x1="1.4375" y1="3.6915" x2="2.1275" y2="3.7145" layer="21"/>
<rectangle x1="3.0475" y1="3.6915" x2="3.7375" y2="3.7145" layer="21"/>
<rectangle x1="-3.8065" y1="3.7145" x2="-3.1165" y2="3.7375" layer="21"/>
<rectangle x1="-2.1965" y1="3.7145" x2="-1.4835" y2="3.7375" layer="21"/>
<rectangle x1="-0.4255" y1="3.7145" x2="0.3335" y2="3.7375" layer="21"/>
<rectangle x1="1.4375" y1="3.7145" x2="2.1275" y2="3.7375" layer="21"/>
<rectangle x1="3.0475" y1="3.7145" x2="3.7375" y2="3.7375" layer="21"/>
<rectangle x1="-3.8065" y1="3.7375" x2="-3.1165" y2="3.7605" layer="21"/>
<rectangle x1="-2.1965" y1="3.7375" x2="-1.4835" y2="3.7605" layer="21"/>
<rectangle x1="-0.4255" y1="3.7375" x2="0.3335" y2="3.7605" layer="21"/>
<rectangle x1="1.4375" y1="3.7375" x2="2.1275" y2="3.7605" layer="21"/>
<rectangle x1="3.0475" y1="3.7375" x2="3.7375" y2="3.7605" layer="21"/>
<rectangle x1="-3.8065" y1="3.7605" x2="-3.1165" y2="3.7835" layer="21"/>
<rectangle x1="-2.1965" y1="3.7605" x2="-1.4835" y2="3.7835" layer="21"/>
<rectangle x1="-0.4255" y1="3.7605" x2="0.3335" y2="3.7835" layer="21"/>
<rectangle x1="1.4375" y1="3.7605" x2="2.1275" y2="3.7835" layer="21"/>
<rectangle x1="3.0475" y1="3.7605" x2="3.7375" y2="3.7835" layer="21"/>
<rectangle x1="-3.8065" y1="3.7835" x2="-3.1165" y2="3.8065" layer="21"/>
<rectangle x1="-2.1965" y1="3.7835" x2="-1.4835" y2="3.8065" layer="21"/>
<rectangle x1="-0.4255" y1="3.7835" x2="0.3335" y2="3.8065" layer="21"/>
<rectangle x1="1.4375" y1="3.7835" x2="2.1275" y2="3.8065" layer="21"/>
<rectangle x1="3.0475" y1="3.7835" x2="3.7375" y2="3.8065" layer="21"/>
<rectangle x1="-3.8065" y1="3.8065" x2="-3.1165" y2="3.8295" layer="21"/>
<rectangle x1="-2.1965" y1="3.8065" x2="-1.4835" y2="3.8295" layer="21"/>
<rectangle x1="-0.4255" y1="3.8065" x2="0.3335" y2="3.8295" layer="21"/>
<rectangle x1="1.4375" y1="3.8065" x2="2.1275" y2="3.8295" layer="21"/>
<rectangle x1="3.0475" y1="3.8065" x2="3.7375" y2="3.8295" layer="21"/>
<rectangle x1="-3.8065" y1="3.8295" x2="-3.1165" y2="3.8525" layer="21"/>
<rectangle x1="-2.1965" y1="3.8295" x2="-1.4835" y2="3.8525" layer="21"/>
<rectangle x1="-0.4255" y1="3.8295" x2="0.3335" y2="3.8525" layer="21"/>
<rectangle x1="1.4375" y1="3.8295" x2="2.1275" y2="3.8525" layer="21"/>
<rectangle x1="3.0475" y1="3.8295" x2="3.7375" y2="3.8525" layer="21"/>
<rectangle x1="-3.8065" y1="3.8525" x2="-3.1165" y2="3.8755" layer="21"/>
<rectangle x1="-2.1965" y1="3.8525" x2="-1.4835" y2="3.8755" layer="21"/>
<rectangle x1="-0.4255" y1="3.8525" x2="0.3335" y2="3.8755" layer="21"/>
<rectangle x1="1.4375" y1="3.8525" x2="2.1275" y2="3.8755" layer="21"/>
<rectangle x1="3.0475" y1="3.8525" x2="3.7375" y2="3.8755" layer="21"/>
<rectangle x1="-3.8065" y1="3.8755" x2="-3.1165" y2="3.8985" layer="21"/>
<rectangle x1="-2.1965" y1="3.8755" x2="-1.4835" y2="3.8985" layer="21"/>
<rectangle x1="-0.4255" y1="3.8755" x2="0.3335" y2="3.8985" layer="21"/>
<rectangle x1="1.4375" y1="3.8755" x2="2.1275" y2="3.8985" layer="21"/>
<rectangle x1="3.0475" y1="3.8755" x2="3.7375" y2="3.8985" layer="21"/>
<rectangle x1="-3.8065" y1="3.8985" x2="-3.1165" y2="3.9215" layer="21"/>
<rectangle x1="-2.1965" y1="3.8985" x2="-1.4835" y2="3.9215" layer="21"/>
<rectangle x1="-0.4255" y1="3.8985" x2="0.3335" y2="3.9215" layer="21"/>
<rectangle x1="1.4375" y1="3.8985" x2="2.1275" y2="3.9215" layer="21"/>
<rectangle x1="3.0475" y1="3.8985" x2="3.7375" y2="3.9215" layer="21"/>
<rectangle x1="-3.8065" y1="3.9215" x2="-3.1165" y2="3.9445" layer="21"/>
<rectangle x1="-2.1965" y1="3.9215" x2="-1.4835" y2="3.9445" layer="21"/>
<rectangle x1="-0.4255" y1="3.9215" x2="0.3335" y2="3.9445" layer="21"/>
<rectangle x1="1.4375" y1="3.9215" x2="2.1275" y2="3.9445" layer="21"/>
<rectangle x1="3.0475" y1="3.9215" x2="3.7375" y2="3.9445" layer="21"/>
<rectangle x1="-3.8065" y1="3.9445" x2="-3.1165" y2="3.9675" layer="21"/>
<rectangle x1="-2.1965" y1="3.9445" x2="-1.4835" y2="3.9675" layer="21"/>
<rectangle x1="-0.4255" y1="3.9445" x2="0.3335" y2="3.9675" layer="21"/>
<rectangle x1="1.4375" y1="3.9445" x2="2.1275" y2="3.9675" layer="21"/>
<rectangle x1="3.0475" y1="3.9445" x2="3.7375" y2="3.9675" layer="21"/>
<rectangle x1="-3.8065" y1="3.9675" x2="-3.1165" y2="3.9905" layer="21"/>
<rectangle x1="-2.1965" y1="3.9675" x2="-1.4835" y2="3.9905" layer="21"/>
<rectangle x1="-0.4255" y1="3.9675" x2="0.3335" y2="3.9905" layer="21"/>
<rectangle x1="1.4375" y1="3.9675" x2="2.1275" y2="3.9905" layer="21"/>
<rectangle x1="3.0475" y1="3.9675" x2="3.7375" y2="3.9905" layer="21"/>
<rectangle x1="-3.8065" y1="3.9905" x2="-3.1165" y2="4.0135" layer="21"/>
<rectangle x1="-2.1965" y1="3.9905" x2="-1.4835" y2="4.0135" layer="21"/>
<rectangle x1="-0.4255" y1="3.9905" x2="0.3335" y2="4.0135" layer="21"/>
<rectangle x1="1.4375" y1="3.9905" x2="2.1275" y2="4.0135" layer="21"/>
<rectangle x1="3.0475" y1="3.9905" x2="3.7375" y2="4.0135" layer="21"/>
<rectangle x1="-3.8065" y1="4.0135" x2="-3.1165" y2="4.0365" layer="21"/>
<rectangle x1="-2.1965" y1="4.0135" x2="-1.4835" y2="4.0365" layer="21"/>
<rectangle x1="-0.4255" y1="4.0135" x2="0.3335" y2="4.0365" layer="21"/>
<rectangle x1="1.4375" y1="4.0135" x2="2.1275" y2="4.0365" layer="21"/>
<rectangle x1="3.0475" y1="4.0135" x2="3.7375" y2="4.0365" layer="21"/>
<rectangle x1="-3.8065" y1="4.0365" x2="-3.1165" y2="4.0595" layer="21"/>
<rectangle x1="-2.1965" y1="4.0365" x2="-1.4835" y2="4.0595" layer="21"/>
<rectangle x1="-0.4255" y1="4.0365" x2="0.3335" y2="4.0595" layer="21"/>
<rectangle x1="1.4375" y1="4.0365" x2="2.1275" y2="4.0595" layer="21"/>
<rectangle x1="3.0475" y1="4.0365" x2="3.7375" y2="4.0595" layer="21"/>
<rectangle x1="-3.8065" y1="4.0595" x2="-3.1165" y2="4.0825" layer="21"/>
<rectangle x1="-2.1965" y1="4.0595" x2="-1.4835" y2="4.0825" layer="21"/>
<rectangle x1="-0.4255" y1="4.0595" x2="0.3335" y2="4.0825" layer="21"/>
<rectangle x1="1.4375" y1="4.0595" x2="2.1275" y2="4.0825" layer="21"/>
<rectangle x1="3.0475" y1="4.0595" x2="3.7375" y2="4.0825" layer="21"/>
<rectangle x1="-3.8065" y1="4.0825" x2="-3.1165" y2="4.1055" layer="21"/>
<rectangle x1="-2.1965" y1="4.0825" x2="-1.4835" y2="4.1055" layer="21"/>
<rectangle x1="-0.4255" y1="4.0825" x2="0.3335" y2="4.1055" layer="21"/>
<rectangle x1="1.4375" y1="4.0825" x2="2.1275" y2="4.1055" layer="21"/>
<rectangle x1="3.0475" y1="4.0825" x2="3.7375" y2="4.1055" layer="21"/>
<rectangle x1="-3.8065" y1="4.1055" x2="-3.1165" y2="4.1285" layer="21"/>
<rectangle x1="-2.1965" y1="4.1055" x2="-1.4835" y2="4.1285" layer="21"/>
<rectangle x1="-0.4255" y1="4.1055" x2="0.3335" y2="4.1285" layer="21"/>
<rectangle x1="1.4375" y1="4.1055" x2="2.1275" y2="4.1285" layer="21"/>
<rectangle x1="3.0475" y1="4.1055" x2="3.7375" y2="4.1285" layer="21"/>
<rectangle x1="-3.8065" y1="4.1285" x2="-3.1165" y2="4.1515" layer="21"/>
<rectangle x1="-2.1965" y1="4.1285" x2="-1.4835" y2="4.1515" layer="21"/>
<rectangle x1="-0.4255" y1="4.1285" x2="0.3335" y2="4.1515" layer="21"/>
<rectangle x1="1.4375" y1="4.1285" x2="2.1275" y2="4.1515" layer="21"/>
<rectangle x1="3.0475" y1="4.1285" x2="3.7375" y2="4.1515" layer="21"/>
<rectangle x1="-3.8065" y1="4.1515" x2="-3.1165" y2="4.1745" layer="21"/>
<rectangle x1="-2.1965" y1="4.1515" x2="-1.4835" y2="4.1745" layer="21"/>
<rectangle x1="-0.4255" y1="4.1515" x2="0.3335" y2="4.1745" layer="21"/>
<rectangle x1="1.4375" y1="4.1515" x2="2.1275" y2="4.1745" layer="21"/>
<rectangle x1="3.0475" y1="4.1515" x2="3.7375" y2="4.1745" layer="21"/>
<rectangle x1="-3.8065" y1="4.1745" x2="-3.1165" y2="4.1975" layer="21"/>
<rectangle x1="-2.1965" y1="4.1745" x2="-1.4835" y2="4.1975" layer="21"/>
<rectangle x1="-0.4255" y1="4.1745" x2="0.3335" y2="4.1975" layer="21"/>
<rectangle x1="1.4375" y1="4.1745" x2="2.1275" y2="4.1975" layer="21"/>
<rectangle x1="3.0475" y1="4.1745" x2="3.7375" y2="4.1975" layer="21"/>
<rectangle x1="-3.8065" y1="4.1975" x2="-3.1165" y2="4.2205" layer="21"/>
<rectangle x1="-2.1965" y1="4.1975" x2="-1.4835" y2="4.2205" layer="21"/>
<rectangle x1="-0.4255" y1="4.1975" x2="0.3335" y2="4.2205" layer="21"/>
<rectangle x1="1.4375" y1="4.1975" x2="2.1275" y2="4.2205" layer="21"/>
<rectangle x1="3.0475" y1="4.1975" x2="3.7375" y2="4.2205" layer="21"/>
<rectangle x1="-3.8065" y1="4.2205" x2="-3.1165" y2="4.2435" layer="21"/>
<rectangle x1="-2.1965" y1="4.2205" x2="-1.4835" y2="4.2435" layer="21"/>
<rectangle x1="-0.4255" y1="4.2205" x2="0.3335" y2="4.2435" layer="21"/>
<rectangle x1="1.4375" y1="4.2205" x2="2.1275" y2="4.2435" layer="21"/>
<rectangle x1="3.0475" y1="4.2205" x2="3.7375" y2="4.2435" layer="21"/>
<rectangle x1="-3.8065" y1="4.2435" x2="-3.1165" y2="4.2665" layer="21"/>
<rectangle x1="-2.1965" y1="4.2435" x2="-1.5065" y2="4.2665" layer="21"/>
<rectangle x1="-0.4255" y1="4.2435" x2="0.3335" y2="4.2665" layer="21"/>
<rectangle x1="1.4375" y1="4.2435" x2="2.1275" y2="4.2665" layer="21"/>
<rectangle x1="3.0475" y1="4.2435" x2="3.7375" y2="4.2665" layer="21"/>
<rectangle x1="-3.8065" y1="4.2665" x2="-3.1165" y2="4.2895" layer="21"/>
<rectangle x1="-2.1965" y1="4.2665" x2="-1.5065" y2="4.2895" layer="21"/>
<rectangle x1="-0.4255" y1="4.2665" x2="0.3335" y2="4.2895" layer="21"/>
<rectangle x1="1.4375" y1="4.2665" x2="2.1275" y2="4.2895" layer="21"/>
<rectangle x1="3.0475" y1="4.2665" x2="3.7375" y2="4.2895" layer="21"/>
<rectangle x1="-3.8065" y1="4.2895" x2="-3.1165" y2="4.3125" layer="21"/>
<rectangle x1="-2.2195" y1="4.2895" x2="-1.5065" y2="4.3125" layer="21"/>
<rectangle x1="-0.4255" y1="4.2895" x2="0.3335" y2="4.3125" layer="21"/>
<rectangle x1="1.4375" y1="4.2895" x2="2.1275" y2="4.3125" layer="21"/>
<rectangle x1="3.0475" y1="4.2895" x2="3.7375" y2="4.3125" layer="21"/>
<rectangle x1="-3.8065" y1="4.3125" x2="-3.1165" y2="4.3355" layer="21"/>
<rectangle x1="-2.2195" y1="4.3125" x2="-1.5065" y2="4.3355" layer="21"/>
<rectangle x1="-0.4255" y1="4.3125" x2="0.3335" y2="4.3355" layer="21"/>
<rectangle x1="1.4375" y1="4.3125" x2="2.1275" y2="4.3355" layer="21"/>
<rectangle x1="3.0475" y1="4.3125" x2="3.7375" y2="4.3355" layer="21"/>
<rectangle x1="-3.8065" y1="4.3355" x2="-3.1165" y2="4.3585" layer="21"/>
<rectangle x1="-2.2195" y1="4.3355" x2="-1.5065" y2="4.3585" layer="21"/>
<rectangle x1="-0.4255" y1="4.3355" x2="0.3335" y2="4.3585" layer="21"/>
<rectangle x1="1.4375" y1="4.3355" x2="2.1275" y2="4.3585" layer="21"/>
<rectangle x1="3.0475" y1="4.3355" x2="3.7375" y2="4.3585" layer="21"/>
<rectangle x1="-3.8065" y1="4.3585" x2="-3.1165" y2="4.3815" layer="21"/>
<rectangle x1="-2.2195" y1="4.3585" x2="-1.5065" y2="4.3815" layer="21"/>
<rectangle x1="-0.4255" y1="4.3585" x2="0.3335" y2="4.3815" layer="21"/>
<rectangle x1="1.4375" y1="4.3585" x2="2.1275" y2="4.3815" layer="21"/>
<rectangle x1="3.0475" y1="4.3585" x2="3.7375" y2="4.3815" layer="21"/>
<rectangle x1="-3.8065" y1="4.3815" x2="-3.1165" y2="4.4045" layer="21"/>
<rectangle x1="-2.2195" y1="4.3815" x2="-1.5065" y2="4.4045" layer="21"/>
<rectangle x1="-0.4255" y1="4.3815" x2="0.3335" y2="4.4045" layer="21"/>
<rectangle x1="1.4375" y1="4.3815" x2="2.1275" y2="4.4045" layer="21"/>
<rectangle x1="3.0475" y1="4.3815" x2="3.7375" y2="4.4045" layer="21"/>
<rectangle x1="-3.8065" y1="4.4045" x2="-3.1165" y2="4.4275" layer="21"/>
<rectangle x1="-2.2195" y1="4.4045" x2="-1.5065" y2="4.4275" layer="21"/>
<rectangle x1="-0.4255" y1="4.4045" x2="0.3335" y2="4.4275" layer="21"/>
<rectangle x1="1.4375" y1="4.4045" x2="2.1275" y2="4.4275" layer="21"/>
<rectangle x1="3.0475" y1="4.4045" x2="3.7375" y2="4.4275" layer="21"/>
<rectangle x1="-3.8065" y1="4.4275" x2="-3.1165" y2="4.4505" layer="21"/>
<rectangle x1="-2.2195" y1="4.4275" x2="-1.5065" y2="4.4505" layer="21"/>
<rectangle x1="-0.4255" y1="4.4275" x2="0.3335" y2="4.4505" layer="21"/>
<rectangle x1="1.4375" y1="4.4275" x2="2.1275" y2="4.4505" layer="21"/>
<rectangle x1="3.0475" y1="4.4275" x2="3.7375" y2="4.4505" layer="21"/>
<rectangle x1="-3.8065" y1="4.4505" x2="-3.1165" y2="4.4735" layer="21"/>
<rectangle x1="-2.2195" y1="4.4505" x2="-1.5065" y2="4.4735" layer="21"/>
<rectangle x1="-0.4255" y1="4.4505" x2="0.3335" y2="4.4735" layer="21"/>
<rectangle x1="1.4375" y1="4.4505" x2="2.1275" y2="4.4735" layer="21"/>
<rectangle x1="3.0475" y1="4.4505" x2="3.7375" y2="4.4735" layer="21"/>
<rectangle x1="-3.8065" y1="4.4735" x2="-3.1165" y2="4.4965" layer="21"/>
<rectangle x1="-2.2195" y1="4.4735" x2="-1.5065" y2="4.4965" layer="21"/>
<rectangle x1="-0.4255" y1="4.4735" x2="0.3335" y2="4.4965" layer="21"/>
<rectangle x1="1.4375" y1="4.4735" x2="2.1275" y2="4.4965" layer="21"/>
<rectangle x1="3.0475" y1="4.4735" x2="3.7375" y2="4.4965" layer="21"/>
<rectangle x1="-3.8065" y1="4.4965" x2="-3.1165" y2="4.5195" layer="21"/>
<rectangle x1="-2.2195" y1="4.4965" x2="-1.5065" y2="4.5195" layer="21"/>
<rectangle x1="-0.4255" y1="4.4965" x2="0.3335" y2="4.5195" layer="21"/>
<rectangle x1="1.4375" y1="4.4965" x2="2.1275" y2="4.5195" layer="21"/>
<rectangle x1="3.0475" y1="4.4965" x2="3.7375" y2="4.5195" layer="21"/>
<rectangle x1="-3.8065" y1="4.5195" x2="-3.1165" y2="4.5425" layer="21"/>
<rectangle x1="-2.2195" y1="4.5195" x2="-1.5065" y2="4.5425" layer="21"/>
<rectangle x1="-0.4255" y1="4.5195" x2="0.3335" y2="4.5425" layer="21"/>
<rectangle x1="1.4375" y1="4.5195" x2="2.1275" y2="4.5425" layer="21"/>
<rectangle x1="3.0475" y1="4.5195" x2="3.7375" y2="4.5425" layer="21"/>
<rectangle x1="-3.8065" y1="4.5425" x2="-3.1165" y2="4.5655" layer="21"/>
<rectangle x1="-2.2425" y1="4.5425" x2="-1.5065" y2="4.5655" layer="21"/>
<rectangle x1="-0.4255" y1="4.5425" x2="0.3335" y2="4.5655" layer="21"/>
<rectangle x1="1.4375" y1="4.5425" x2="2.1275" y2="4.5655" layer="21"/>
<rectangle x1="3.0475" y1="4.5425" x2="3.7375" y2="4.5655" layer="21"/>
<rectangle x1="-3.8065" y1="4.5655" x2="-3.1165" y2="4.5885" layer="21"/>
<rectangle x1="-2.2425" y1="4.5655" x2="-1.5295" y2="4.5885" layer="21"/>
<rectangle x1="-0.4255" y1="4.5655" x2="0.3335" y2="4.5885" layer="21"/>
<rectangle x1="1.4375" y1="4.5655" x2="2.1275" y2="4.5885" layer="21"/>
<rectangle x1="3.0475" y1="4.5655" x2="3.7375" y2="4.5885" layer="21"/>
<rectangle x1="-3.8065" y1="4.5885" x2="-3.1165" y2="4.6115" layer="21"/>
<rectangle x1="-2.2425" y1="4.5885" x2="-1.5295" y2="4.6115" layer="21"/>
<rectangle x1="-0.4255" y1="4.5885" x2="0.3335" y2="4.6115" layer="21"/>
<rectangle x1="1.4375" y1="4.5885" x2="2.1275" y2="4.6115" layer="21"/>
<rectangle x1="3.0475" y1="4.5885" x2="3.7375" y2="4.6115" layer="21"/>
<rectangle x1="-3.8065" y1="4.6115" x2="-3.1165" y2="4.6345" layer="21"/>
<rectangle x1="-2.2425" y1="4.6115" x2="-1.5295" y2="4.6345" layer="21"/>
<rectangle x1="-0.4255" y1="4.6115" x2="0.3335" y2="4.6345" layer="21"/>
<rectangle x1="1.4375" y1="4.6115" x2="2.1275" y2="4.6345" layer="21"/>
<rectangle x1="3.0475" y1="4.6115" x2="3.7375" y2="4.6345" layer="21"/>
<rectangle x1="-3.8065" y1="4.6345" x2="-3.1165" y2="4.6575" layer="21"/>
<rectangle x1="-2.2425" y1="4.6345" x2="-1.5295" y2="4.6575" layer="21"/>
<rectangle x1="-0.4255" y1="4.6345" x2="0.3335" y2="4.6575" layer="21"/>
<rectangle x1="1.4375" y1="4.6345" x2="2.1275" y2="4.6575" layer="21"/>
<rectangle x1="3.0475" y1="4.6345" x2="3.7375" y2="4.6575" layer="21"/>
<rectangle x1="-3.8065" y1="4.6575" x2="-3.1165" y2="4.6805" layer="21"/>
<rectangle x1="-2.2655" y1="4.6575" x2="-1.5295" y2="4.6805" layer="21"/>
<rectangle x1="-0.4255" y1="4.6575" x2="0.3335" y2="4.6805" layer="21"/>
<rectangle x1="1.4375" y1="4.6575" x2="2.1275" y2="4.6805" layer="21"/>
<rectangle x1="3.0475" y1="4.6575" x2="3.7375" y2="4.6805" layer="21"/>
<rectangle x1="-3.8065" y1="4.6805" x2="-3.1165" y2="4.7035" layer="21"/>
<rectangle x1="-2.2655" y1="4.6805" x2="-1.5295" y2="4.7035" layer="21"/>
<rectangle x1="-0.4255" y1="4.6805" x2="0.3335" y2="4.7035" layer="21"/>
<rectangle x1="1.4375" y1="4.6805" x2="2.1275" y2="4.7035" layer="21"/>
<rectangle x1="3.0475" y1="4.6805" x2="3.7375" y2="4.7035" layer="21"/>
<rectangle x1="-3.8065" y1="4.7035" x2="-3.1165" y2="4.7265" layer="21"/>
<rectangle x1="-2.2655" y1="4.7035" x2="-1.5295" y2="4.7265" layer="21"/>
<rectangle x1="-0.4255" y1="4.7035" x2="0.3335" y2="4.7265" layer="21"/>
<rectangle x1="1.4375" y1="4.7035" x2="2.1275" y2="4.7265" layer="21"/>
<rectangle x1="3.0475" y1="4.7035" x2="3.7375" y2="4.7265" layer="21"/>
<rectangle x1="-3.8065" y1="4.7265" x2="-3.1165" y2="4.7495" layer="21"/>
<rectangle x1="-2.2885" y1="4.7265" x2="-1.5525" y2="4.7495" layer="21"/>
<rectangle x1="-0.4255" y1="4.7265" x2="0.3335" y2="4.7495" layer="21"/>
<rectangle x1="1.4375" y1="4.7265" x2="2.1275" y2="4.7495" layer="21"/>
<rectangle x1="3.0475" y1="4.7265" x2="3.7375" y2="4.7495" layer="21"/>
<rectangle x1="-3.8065" y1="4.7495" x2="-3.1165" y2="4.7725" layer="21"/>
<rectangle x1="-2.2885" y1="4.7495" x2="-1.5525" y2="4.7725" layer="21"/>
<rectangle x1="-0.4255" y1="4.7495" x2="0.3335" y2="4.7725" layer="21"/>
<rectangle x1="1.4375" y1="4.7495" x2="2.1275" y2="4.7725" layer="21"/>
<rectangle x1="3.0475" y1="4.7495" x2="3.7375" y2="4.7725" layer="21"/>
<rectangle x1="-3.8065" y1="4.7725" x2="-3.1165" y2="4.7955" layer="21"/>
<rectangle x1="-2.3115" y1="4.7725" x2="-1.5525" y2="4.7955" layer="21"/>
<rectangle x1="-0.4255" y1="4.7725" x2="0.3335" y2="4.7955" layer="21"/>
<rectangle x1="1.4375" y1="4.7725" x2="2.1275" y2="4.7955" layer="21"/>
<rectangle x1="3.0475" y1="4.7725" x2="3.7375" y2="4.7955" layer="21"/>
<rectangle x1="-3.8065" y1="4.7955" x2="-3.1165" y2="4.8185" layer="21"/>
<rectangle x1="-2.3115" y1="4.7955" x2="-1.5525" y2="4.8185" layer="21"/>
<rectangle x1="-0.4255" y1="4.7955" x2="0.3335" y2="4.8185" layer="21"/>
<rectangle x1="1.4375" y1="4.7955" x2="2.1275" y2="4.8185" layer="21"/>
<rectangle x1="3.0475" y1="4.7955" x2="3.7375" y2="4.8185" layer="21"/>
<rectangle x1="-3.8065" y1="4.8185" x2="-3.1165" y2="4.8415" layer="21"/>
<rectangle x1="-2.3345" y1="4.8185" x2="-1.5755" y2="4.8415" layer="21"/>
<rectangle x1="-0.4255" y1="4.8185" x2="0.3335" y2="4.8415" layer="21"/>
<rectangle x1="1.4375" y1="4.8185" x2="2.1275" y2="4.8415" layer="21"/>
<rectangle x1="3.0475" y1="4.8185" x2="3.7375" y2="4.8415" layer="21"/>
<rectangle x1="-3.8065" y1="4.8415" x2="-3.1165" y2="4.8645" layer="21"/>
<rectangle x1="-2.3575" y1="4.8415" x2="-1.5755" y2="4.8645" layer="21"/>
<rectangle x1="-0.4255" y1="4.8415" x2="0.3335" y2="4.8645" layer="21"/>
<rectangle x1="1.4375" y1="4.8415" x2="2.1275" y2="4.8645" layer="21"/>
<rectangle x1="3.0475" y1="4.8415" x2="3.7375" y2="4.8645" layer="21"/>
<rectangle x1="-3.8065" y1="4.8645" x2="-3.1165" y2="4.8875" layer="21"/>
<rectangle x1="-2.3805" y1="4.8645" x2="-1.5755" y2="4.8875" layer="21"/>
<rectangle x1="-0.4255" y1="4.8645" x2="0.3335" y2="4.8875" layer="21"/>
<rectangle x1="1.4375" y1="4.8645" x2="2.1275" y2="4.8875" layer="21"/>
<rectangle x1="3.0475" y1="4.8645" x2="3.7375" y2="4.8875" layer="21"/>
<rectangle x1="-3.8065" y1="4.8875" x2="-3.1165" y2="4.9105" layer="21"/>
<rectangle x1="-2.4035" y1="4.8875" x2="-1.5755" y2="4.9105" layer="21"/>
<rectangle x1="-0.4255" y1="4.8875" x2="0.3335" y2="4.9105" layer="21"/>
<rectangle x1="1.4375" y1="4.8875" x2="2.1275" y2="4.9105" layer="21"/>
<rectangle x1="3.0475" y1="4.8875" x2="3.7375" y2="4.9105" layer="21"/>
<rectangle x1="-3.8065" y1="4.9105" x2="-3.1165" y2="4.9335" layer="21"/>
<rectangle x1="-2.4495" y1="4.9105" x2="-1.5985" y2="4.9335" layer="21"/>
<rectangle x1="-0.4255" y1="4.9105" x2="0.3565" y2="4.9335" layer="21"/>
<rectangle x1="1.4375" y1="4.9105" x2="2.1275" y2="4.9335" layer="21"/>
<rectangle x1="3.0475" y1="4.9105" x2="3.7375" y2="4.9335" layer="21"/>
<rectangle x1="-3.8065" y1="4.9335" x2="-3.1165" y2="4.9565" layer="21"/>
<rectangle x1="-2.4955" y1="4.9335" x2="-1.5985" y2="4.9565" layer="21"/>
<rectangle x1="-1.1615" y1="4.9335" x2="1.0925" y2="4.9565" layer="21"/>
<rectangle x1="1.4375" y1="4.9335" x2="2.1275" y2="4.9565" layer="21"/>
<rectangle x1="3.0475" y1="4.9335" x2="3.7375" y2="4.9565" layer="21"/>
<rectangle x1="-3.8065" y1="4.9565" x2="-1.6215" y2="4.9795" layer="21"/>
<rectangle x1="-1.1845" y1="4.9565" x2="1.0925" y2="4.9795" layer="21"/>
<rectangle x1="1.4375" y1="4.9565" x2="2.1275" y2="4.9795" layer="21"/>
<rectangle x1="3.0475" y1="4.9565" x2="3.7375" y2="4.9795" layer="21"/>
<rectangle x1="-3.8065" y1="4.9795" x2="-1.6215" y2="5.0025" layer="21"/>
<rectangle x1="-1.1845" y1="4.9795" x2="1.1155" y2="5.0025" layer="21"/>
<rectangle x1="1.4375" y1="4.9795" x2="2.1275" y2="5.0025" layer="21"/>
<rectangle x1="3.0475" y1="4.9795" x2="3.7375" y2="5.0025" layer="21"/>
<rectangle x1="-3.8065" y1="5.0025" x2="-1.6215" y2="5.0255" layer="21"/>
<rectangle x1="-1.1845" y1="5.0025" x2="1.1155" y2="5.0255" layer="21"/>
<rectangle x1="1.4375" y1="5.0025" x2="2.1275" y2="5.0255" layer="21"/>
<rectangle x1="3.0475" y1="5.0025" x2="3.7375" y2="5.0255" layer="21"/>
<rectangle x1="-3.8065" y1="5.0255" x2="-1.6445" y2="5.0485" layer="21"/>
<rectangle x1="-1.1845" y1="5.0255" x2="1.1155" y2="5.0485" layer="21"/>
<rectangle x1="1.4375" y1="5.0255" x2="2.1275" y2="5.0485" layer="21"/>
<rectangle x1="3.0475" y1="5.0255" x2="3.7375" y2="5.0485" layer="21"/>
<rectangle x1="-3.8065" y1="5.0485" x2="-1.6445" y2="5.0715" layer="21"/>
<rectangle x1="-1.1845" y1="5.0485" x2="1.1155" y2="5.0715" layer="21"/>
<rectangle x1="1.4375" y1="5.0485" x2="2.1275" y2="5.0715" layer="21"/>
<rectangle x1="3.0475" y1="5.0485" x2="3.7375" y2="5.0715" layer="21"/>
<rectangle x1="-3.8065" y1="5.0715" x2="-1.6675" y2="5.0945" layer="21"/>
<rectangle x1="-1.1845" y1="5.0715" x2="1.1155" y2="5.0945" layer="21"/>
<rectangle x1="1.4375" y1="5.0715" x2="2.1275" y2="5.0945" layer="21"/>
<rectangle x1="3.0475" y1="5.0715" x2="3.7375" y2="5.0945" layer="21"/>
<rectangle x1="-3.8065" y1="5.0945" x2="-1.6905" y2="5.1175" layer="21"/>
<rectangle x1="-1.1845" y1="5.0945" x2="1.1155" y2="5.1175" layer="21"/>
<rectangle x1="1.4375" y1="5.0945" x2="2.1275" y2="5.1175" layer="21"/>
<rectangle x1="3.0475" y1="5.0945" x2="3.7375" y2="5.1175" layer="21"/>
<rectangle x1="-3.8065" y1="5.1175" x2="-1.6905" y2="5.1405" layer="21"/>
<rectangle x1="-1.1845" y1="5.1175" x2="1.1155" y2="5.1405" layer="21"/>
<rectangle x1="1.4375" y1="5.1175" x2="2.1275" y2="5.1405" layer="21"/>
<rectangle x1="3.0475" y1="5.1175" x2="3.7375" y2="5.1405" layer="21"/>
<rectangle x1="-3.8065" y1="5.1405" x2="-1.7135" y2="5.1635" layer="21"/>
<rectangle x1="-1.1845" y1="5.1405" x2="1.1155" y2="5.1635" layer="21"/>
<rectangle x1="1.4375" y1="5.1405" x2="2.1275" y2="5.1635" layer="21"/>
<rectangle x1="3.0475" y1="5.1405" x2="3.7375" y2="5.1635" layer="21"/>
<rectangle x1="-3.8065" y1="5.1635" x2="-1.7365" y2="5.1865" layer="21"/>
<rectangle x1="-1.1845" y1="5.1635" x2="1.1155" y2="5.1865" layer="21"/>
<rectangle x1="1.4375" y1="5.1635" x2="2.1275" y2="5.1865" layer="21"/>
<rectangle x1="3.0475" y1="5.1635" x2="3.7375" y2="5.1865" layer="21"/>
<rectangle x1="-3.8065" y1="5.1865" x2="-1.7595" y2="5.2095" layer="21"/>
<rectangle x1="-1.1845" y1="5.1865" x2="1.1155" y2="5.2095" layer="21"/>
<rectangle x1="1.4375" y1="5.1865" x2="2.1275" y2="5.2095" layer="21"/>
<rectangle x1="3.0475" y1="5.1865" x2="3.7375" y2="5.2095" layer="21"/>
<rectangle x1="-3.8065" y1="5.2095" x2="-1.7595" y2="5.2325" layer="21"/>
<rectangle x1="-1.1845" y1="5.2095" x2="1.1155" y2="5.2325" layer="21"/>
<rectangle x1="1.4375" y1="5.2095" x2="2.1275" y2="5.2325" layer="21"/>
<rectangle x1="3.0475" y1="5.2095" x2="3.7375" y2="5.2325" layer="21"/>
<rectangle x1="-3.8065" y1="5.2325" x2="-1.7825" y2="5.2555" layer="21"/>
<rectangle x1="-1.1845" y1="5.2325" x2="1.1155" y2="5.2555" layer="21"/>
<rectangle x1="1.4375" y1="5.2325" x2="2.1275" y2="5.2555" layer="21"/>
<rectangle x1="3.0475" y1="5.2325" x2="3.7375" y2="5.2555" layer="21"/>
<rectangle x1="-3.8065" y1="5.2555" x2="-1.8055" y2="5.2785" layer="21"/>
<rectangle x1="-1.1845" y1="5.2555" x2="1.1155" y2="5.2785" layer="21"/>
<rectangle x1="1.4375" y1="5.2555" x2="2.1275" y2="5.2785" layer="21"/>
<rectangle x1="3.0475" y1="5.2555" x2="3.7375" y2="5.2785" layer="21"/>
<rectangle x1="-3.8065" y1="5.2785" x2="-1.8515" y2="5.3015" layer="21"/>
<rectangle x1="-1.1845" y1="5.2785" x2="1.1155" y2="5.3015" layer="21"/>
<rectangle x1="1.4375" y1="5.2785" x2="2.1275" y2="5.3015" layer="21"/>
<rectangle x1="3.0475" y1="5.2785" x2="3.7375" y2="5.3015" layer="21"/>
<rectangle x1="-3.8065" y1="5.3015" x2="-1.8745" y2="5.3245" layer="21"/>
<rectangle x1="-1.1845" y1="5.3015" x2="1.1155" y2="5.3245" layer="21"/>
<rectangle x1="1.4375" y1="5.3015" x2="2.1275" y2="5.3245" layer="21"/>
<rectangle x1="3.0475" y1="5.3015" x2="3.7375" y2="5.3245" layer="21"/>
<rectangle x1="-3.8065" y1="5.3245" x2="-1.8975" y2="5.3475" layer="21"/>
<rectangle x1="-1.1845" y1="5.3245" x2="1.1155" y2="5.3475" layer="21"/>
<rectangle x1="1.4375" y1="5.3245" x2="2.1275" y2="5.3475" layer="21"/>
<rectangle x1="3.0475" y1="5.3245" x2="3.7375" y2="5.3475" layer="21"/>
<rectangle x1="-3.8065" y1="5.3475" x2="-1.9435" y2="5.3705" layer="21"/>
<rectangle x1="-1.1845" y1="5.3475" x2="1.1155" y2="5.3705" layer="21"/>
<rectangle x1="1.4375" y1="5.3475" x2="2.1275" y2="5.3705" layer="21"/>
<rectangle x1="3.0475" y1="5.3475" x2="3.7375" y2="5.3705" layer="21"/>
<rectangle x1="-3.8065" y1="5.3705" x2="-1.9895" y2="5.3935" layer="21"/>
<rectangle x1="-1.1845" y1="5.3705" x2="1.1155" y2="5.3935" layer="21"/>
<rectangle x1="1.4375" y1="5.3705" x2="2.1275" y2="5.3935" layer="21"/>
<rectangle x1="3.0475" y1="5.3705" x2="3.7375" y2="5.3935" layer="21"/>
<rectangle x1="-3.8065" y1="5.3935" x2="-2.0585" y2="5.4165" layer="21"/>
<rectangle x1="-1.1845" y1="5.3935" x2="1.1155" y2="5.4165" layer="21"/>
<rectangle x1="1.4375" y1="5.3935" x2="2.1275" y2="5.4165" layer="21"/>
<rectangle x1="3.0475" y1="5.3935" x2="3.7375" y2="5.4165" layer="21"/>
<rectangle x1="-3.8065" y1="5.4165" x2="-2.1275" y2="5.4395" layer="21"/>
<rectangle x1="-1.1845" y1="5.4165" x2="1.1155" y2="5.4395" layer="21"/>
<rectangle x1="1.4375" y1="5.4165" x2="2.1275" y2="5.4395" layer="21"/>
<rectangle x1="3.0475" y1="5.4165" x2="3.7375" y2="5.4395" layer="21"/>
<rectangle x1="-3.8065" y1="5.4395" x2="-2.2195" y2="5.4625" layer="21"/>
<rectangle x1="-1.1615" y1="5.4395" x2="1.0925" y2="5.4625" layer="21"/>
<rectangle x1="1.4375" y1="5.4395" x2="2.1045" y2="5.4625" layer="21"/>
<rectangle x1="3.0475" y1="5.4395" x2="3.7375" y2="5.4625" layer="21"/>
<rectangle x1="-3.7605" y1="5.4625" x2="-2.4035" y2="5.4855" layer="21"/>
<rectangle x1="-1.1385" y1="5.4625" x2="1.0695" y2="5.4855" layer="21"/>
<rectangle x1="1.4835" y1="5.4625" x2="2.0815" y2="5.4855" layer="21"/>
<rectangle x1="3.0935" y1="5.4625" x2="3.6915" y2="5.4855" layer="21"/>
</package>
<package name="DTU-SMALL-4MM">
<rectangle x1="-1.16675" y1="-3.30255" x2="-1.14205" y2="-3.2902125" layer="21"/>
<rectangle x1="1.11725" y1="-3.30255" x2="1.15425" y2="-3.2902125" layer="21"/>
<rectangle x1="-1.19145" y1="-3.2902125" x2="-1.10495" y2="-3.2778625" layer="21"/>
<rectangle x1="1.09255" y1="-3.2902125" x2="1.17895" y2="-3.2778625" layer="21"/>
<rectangle x1="-1.21605" y1="-3.2778625" x2="-1.08025" y2="-3.265525" layer="21"/>
<rectangle x1="1.05555" y1="-3.2778625" x2="1.20365" y2="-3.265525" layer="21"/>
<rectangle x1="-1.24075" y1="-3.265521875" x2="-1.04325" y2="-3.253171875" layer="21"/>
<rectangle x1="1.03085" y1="-3.265521875" x2="1.21595" y2="-3.253171875" layer="21"/>
<rectangle x1="-1.26545" y1="-3.253171875" x2="-1.00625" y2="-3.240821875" layer="21"/>
<rectangle x1="0.99375" y1="-3.253171875" x2="1.24065" y2="-3.240821875" layer="21"/>
<rectangle x1="-1.29015" y1="-3.240821875" x2="-0.98155" y2="-3.228484375" layer="21"/>
<rectangle x1="0.95675" y1="-3.240821875" x2="1.26535" y2="-3.228484375" layer="21"/>
<rectangle x1="-1.31485" y1="-3.22848125" x2="-0.94445" y2="-3.21613125" layer="21"/>
<rectangle x1="0.93205" y1="-3.22848125" x2="1.29005" y2="-3.21613125" layer="21"/>
<rectangle x1="-1.32725" y1="-3.21613125" x2="-0.91985" y2="-3.20379375" layer="21"/>
<rectangle x1="0.89505" y1="-3.21613125" x2="1.31475" y2="-3.20379375" layer="21"/>
<rectangle x1="-1.35195" y1="-3.203790625" x2="-0.88275" y2="-3.191440625" layer="21"/>
<rectangle x1="0.85795" y1="-3.203790625" x2="1.33945" y2="-3.191440625" layer="21"/>
<rectangle x1="-1.37655" y1="-3.191440625" x2="-0.84575" y2="-3.179103125" layer="21"/>
<rectangle x1="0.83325" y1="-3.191440625" x2="1.36415" y2="-3.179103125" layer="21"/>
<rectangle x1="-1.40125" y1="-3.1791" x2="-0.80865" y2="-3.16675" layer="21"/>
<rectangle x1="0.79625" y1="-3.1791" x2="1.38885" y2="-3.16675" layer="21"/>
<rectangle x1="-1.42595" y1="-3.16675" x2="-0.78405" y2="-3.1544" layer="21"/>
<rectangle x1="0.75925" y1="-3.16675" x2="1.41355" y2="-3.1544" layer="21"/>
<rectangle x1="-1.45065" y1="-3.1544" x2="-0.74695" y2="-3.1420625" layer="21"/>
<rectangle x1="0.73455" y1="-3.1544" x2="1.43825" y2="-3.1420625" layer="21"/>
<rectangle x1="-1.47535" y1="-3.1420625" x2="-0.70995" y2="-3.1297125" layer="21"/>
<rectangle x1="0.69745" y1="-3.1420625" x2="1.46295" y2="-3.1297125" layer="21"/>
<rectangle x1="-1.50005" y1="-3.1297125" x2="-0.67285" y2="-3.117375" layer="21"/>
<rectangle x1="0.66045" y1="-3.1297125" x2="1.47525" y2="-3.117375" layer="21"/>
<rectangle x1="-1.52475" y1="-3.117371875" x2="-0.63585" y2="-3.105021875" layer="21"/>
<rectangle x1="0.62345" y1="-3.117371875" x2="1.49995" y2="-3.105021875" layer="21"/>
<rectangle x1="-1.54945" y1="-3.105021875" x2="-0.59885" y2="-3.092684375" layer="21"/>
<rectangle x1="0.58635" y1="-3.105021875" x2="1.52465" y2="-3.092684375" layer="21"/>
<rectangle x1="-1.57415" y1="-3.09268125" x2="-0.56175" y2="-3.08033125" layer="21"/>
<rectangle x1="0.54935" y1="-3.09268125" x2="1.54935" y2="-3.08033125" layer="21"/>
<rectangle x1="-1.58645" y1="-3.08033125" x2="-0.52475" y2="-3.06798125" layer="21"/>
<rectangle x1="0.51225" y1="-3.08033125" x2="1.57405" y2="-3.06798125" layer="21"/>
<rectangle x1="-1.61115" y1="-3.06798125" x2="-0.48775" y2="-3.05564375" layer="21"/>
<rectangle x1="0.47525" y1="-3.06798125" x2="1.59875" y2="-3.05564375" layer="21"/>
<rectangle x1="-1.63585" y1="-3.055640625" x2="-0.43835" y2="-3.043290625" layer="21"/>
<rectangle x1="0.42585" y1="-3.055640625" x2="1.62345" y2="-3.043290625" layer="21"/>
<rectangle x1="-1.66055" y1="-3.043290625" x2="-0.40125" y2="-3.030953125" layer="21"/>
<rectangle x1="0.37645" y1="-3.043290625" x2="1.64805" y2="-3.030953125" layer="21"/>
<rectangle x1="-1.68525" y1="-3.03095" x2="-0.35195" y2="-3.0186" layer="21"/>
<rectangle x1="0.32715" y1="-3.03095" x2="1.67275" y2="-3.0186" layer="21"/>
<rectangle x1="-1.70995" y1="-3.0186" x2="-0.29015" y2="-3.0062625" layer="21"/>
<rectangle x1="0.27775" y1="-3.0186" x2="1.69745" y2="-3.0062625" layer="21"/>
<rectangle x1="-1.73465" y1="-3.0062625" x2="-0.22845" y2="-2.9939125" layer="21"/>
<rectangle x1="0.21595" y1="-3.0062625" x2="1.70985" y2="-2.9939125" layer="21"/>
<rectangle x1="-1.75935" y1="-2.9939125" x2="-0.14205" y2="-2.981575" layer="21"/>
<rectangle x1="0.12955" y1="-2.9939125" x2="1.73455" y2="-2.981575" layer="21"/>
<rectangle x1="-1.78405" y1="-2.981571875" x2="1.75925" y2="-2.969221875" layer="21"/>
<rectangle x1="-1.80865" y1="-2.969221875" x2="1.78395" y2="-2.956871875" layer="21"/>
<rectangle x1="-1.82105" y1="-2.956871875" x2="1.80855" y2="-2.944534375" layer="21"/>
<rectangle x1="-1.84575" y1="-2.94453125" x2="1.83325" y2="-2.93218125" layer="21"/>
<rectangle x1="-1.87045" y1="-2.93218125" x2="1.85795" y2="-2.91984375" layer="21"/>
<rectangle x1="-1.89515" y1="-2.919840625" x2="1.88265" y2="-2.907490625" layer="21"/>
<rectangle x1="-1.91985" y1="-2.907490625" x2="1.90735" y2="-2.895153125" layer="21"/>
<rectangle x1="-1.94445" y1="-2.89515" x2="1.93205" y2="-2.8828" layer="21"/>
<rectangle x1="-1.96915" y1="-2.8828" x2="1.95675" y2="-2.87045" layer="21"/>
<rectangle x1="-1.99385" y1="-2.87045" x2="1.96905" y2="-2.8581125" layer="21"/>
<rectangle x1="-1.99385" y1="-2.8581125" x2="1.98145" y2="-2.8457625" layer="21"/>
<rectangle x1="-1.96915" y1="-2.8457625" x2="1.95675" y2="-2.833425" layer="21"/>
<rectangle x1="-1.94445" y1="-2.833421875" x2="1.93205" y2="-2.821071875" layer="21"/>
<rectangle x1="-1.91985" y1="-2.821071875" x2="1.90735" y2="-2.808734375" layer="21"/>
<rectangle x1="-1.89515" y1="-2.80873125" x2="1.88265" y2="-2.79638125" layer="21"/>
<rectangle x1="-1.87045" y1="-2.79638125" x2="1.85795" y2="-2.78403125" layer="21"/>
<rectangle x1="-1.84575" y1="-2.78403125" x2="1.83325" y2="-2.77169375" layer="21"/>
<rectangle x1="-1.83335" y1="-2.771690625" x2="1.80855" y2="-2.759340625" layer="21"/>
<rectangle x1="-1.80865" y1="-2.759340625" x2="1.78395" y2="-2.747003125" layer="21"/>
<rectangle x1="-1.78405" y1="-2.747" x2="1.75925" y2="-2.73465" layer="21"/>
<rectangle x1="-1.75935" y1="-2.73465" x2="-0.14205" y2="-2.7223125" layer="21"/>
<rectangle x1="0.12955" y1="-2.73465" x2="1.73455" y2="-2.7223125" layer="21"/>
<rectangle x1="-1.73465" y1="-2.7223125" x2="-0.22845" y2="-2.7099625" layer="21"/>
<rectangle x1="0.21595" y1="-2.7223125" x2="1.72215" y2="-2.7099625" layer="21"/>
<rectangle x1="-1.70995" y1="-2.7099625" x2="-0.29015" y2="-2.6976125" layer="21"/>
<rectangle x1="0.27775" y1="-2.7099625" x2="1.69745" y2="-2.6976125" layer="21"/>
<rectangle x1="-1.68525" y1="-2.6976125" x2="-0.35195" y2="-2.685275" layer="21"/>
<rectangle x1="0.32715" y1="-2.6976125" x2="1.67275" y2="-2.685275" layer="21"/>
<rectangle x1="-1.66055" y1="-2.685271875" x2="-0.40125" y2="-2.672921875" layer="21"/>
<rectangle x1="0.37645" y1="-2.685271875" x2="1.64805" y2="-2.672921875" layer="21"/>
<rectangle x1="-1.63585" y1="-2.672921875" x2="-0.43835" y2="-2.660584375" layer="21"/>
<rectangle x1="0.42585" y1="-2.672921875" x2="1.62345" y2="-2.660584375" layer="21"/>
<rectangle x1="-1.61115" y1="-2.66058125" x2="-0.48775" y2="-2.64823125" layer="21"/>
<rectangle x1="0.47525" y1="-2.66058125" x2="1.59875" y2="-2.64823125" layer="21"/>
<rectangle x1="-1.58645" y1="-2.64823125" x2="-0.52475" y2="-2.63589375" layer="21"/>
<rectangle x1="0.51225" y1="-2.64823125" x2="1.57405" y2="-2.63589375" layer="21"/>
<rectangle x1="-1.57415" y1="-2.635890625" x2="-0.56175" y2="-2.623540625" layer="21"/>
<rectangle x1="0.54935" y1="-2.635890625" x2="1.54935" y2="-2.623540625" layer="21"/>
<rectangle x1="-1.54945" y1="-2.623540625" x2="-0.59885" y2="-2.611190625" layer="21"/>
<rectangle x1="0.58635" y1="-2.623540625" x2="1.52465" y2="-2.611190625" layer="21"/>
<rectangle x1="-1.52475" y1="-2.611190625" x2="-0.63585" y2="-2.598853125" layer="21"/>
<rectangle x1="0.62345" y1="-2.611190625" x2="1.49995" y2="-2.598853125" layer="21"/>
<rectangle x1="-1.50005" y1="-2.59885" x2="-0.67285" y2="-2.5865" layer="21"/>
<rectangle x1="0.66045" y1="-2.59885" x2="1.47525" y2="-2.5865" layer="21"/>
<rectangle x1="-1.47535" y1="-2.5865" x2="-0.70995" y2="-2.5741625" layer="21"/>
<rectangle x1="0.69745" y1="-2.5865" x2="1.46295" y2="-2.5741625" layer="21"/>
<rectangle x1="-1.45065" y1="-2.5741625" x2="-0.74695" y2="-2.5618125" layer="21"/>
<rectangle x1="0.73455" y1="-2.5741625" x2="1.43825" y2="-2.5618125" layer="21"/>
<rectangle x1="-1.42595" y1="-2.5618125" x2="-0.78405" y2="-2.549475" layer="21"/>
<rectangle x1="0.75925" y1="-2.5618125" x2="1.41355" y2="-2.549475" layer="21"/>
<rectangle x1="-1.40125" y1="-2.549471875" x2="-0.80865" y2="-2.537121875" layer="21"/>
<rectangle x1="0.79625" y1="-2.549471875" x2="1.38885" y2="-2.537121875" layer="21"/>
<rectangle x1="-1.37655" y1="-2.537121875" x2="-0.84575" y2="-2.524784375" layer="21"/>
<rectangle x1="0.83325" y1="-2.537121875" x2="1.36415" y2="-2.524784375" layer="21"/>
<rectangle x1="-1.35195" y1="-2.52478125" x2="-0.88275" y2="-2.51243125" layer="21"/>
<rectangle x1="0.85795" y1="-2.52478125" x2="1.33945" y2="-2.51243125" layer="21"/>
<rectangle x1="-1.32725" y1="-2.51243125" x2="-0.90745" y2="-2.50008125" layer="21"/>
<rectangle x1="0.89505" y1="-2.51243125" x2="1.31475" y2="-2.50008125" layer="21"/>
<rectangle x1="-1.31485" y1="-2.50008125" x2="-0.94445" y2="-2.48774375" layer="21"/>
<rectangle x1="0.93205" y1="-2.50008125" x2="1.29005" y2="-2.48774375" layer="21"/>
<rectangle x1="-1.29015" y1="-2.487740625" x2="-0.98155" y2="-2.475390625" layer="21"/>
<rectangle x1="0.95675" y1="-2.487740625" x2="1.26535" y2="-2.475390625" layer="21"/>
<rectangle x1="-1.26545" y1="-2.475390625" x2="-1.00625" y2="-2.463053125" layer="21"/>
<rectangle x1="0.99375" y1="-2.475390625" x2="1.24065" y2="-2.463053125" layer="21"/>
<rectangle x1="-1.24075" y1="-2.46305" x2="-1.04325" y2="-2.4507" layer="21"/>
<rectangle x1="1.03085" y1="-2.46305" x2="1.22835" y2="-2.4507" layer="21"/>
<rectangle x1="-1.21605" y1="-2.4507" x2="-1.08025" y2="-2.4383625" layer="21"/>
<rectangle x1="1.05555" y1="-2.4507" x2="1.20365" y2="-2.4383625" layer="21"/>
<rectangle x1="-1.19145" y1="-2.4383625" x2="-1.10495" y2="-2.4260125" layer="21"/>
<rectangle x1="1.09255" y1="-2.4383625" x2="1.17895" y2="-2.4260125" layer="21"/>
<rectangle x1="-1.16675" y1="-2.4260125" x2="-1.14205" y2="-2.4136625" layer="21"/>
<rectangle x1="1.11725" y1="-2.4260125" x2="1.15425" y2="-2.4136625" layer="21"/>
<rectangle x1="1.12955" y1="-2.203790625" x2="1.14195" y2="-2.191440625" layer="21"/>
<rectangle x1="-1.17905" y1="-2.191440625" x2="-1.11735" y2="-2.179103125" layer="21"/>
<rectangle x1="1.10485" y1="-2.191440625" x2="1.16665" y2="-2.179103125" layer="21"/>
<rectangle x1="-1.20375" y1="-2.1791" x2="-1.09265" y2="-2.16675" layer="21"/>
<rectangle x1="1.06785" y1="-2.1791" x2="1.19135" y2="-2.16675" layer="21"/>
<rectangle x1="-1.22845" y1="-2.16675" x2="-1.05565" y2="-2.1544" layer="21"/>
<rectangle x1="1.04315" y1="-2.16675" x2="1.21595" y2="-2.1544" layer="21"/>
<rectangle x1="-1.25315" y1="-2.1544" x2="-1.01855" y2="-2.1420625" layer="21"/>
<rectangle x1="1.00615" y1="-2.1544" x2="1.24065" y2="-2.1420625" layer="21"/>
<rectangle x1="-1.27785" y1="-2.1420625" x2="-0.99385" y2="-2.1297125" layer="21"/>
<rectangle x1="0.96905" y1="-2.1420625" x2="1.26535" y2="-2.1297125" layer="21"/>
<rectangle x1="-1.30255" y1="-2.1297125" x2="-0.95685" y2="-2.117375" layer="21"/>
<rectangle x1="0.94435" y1="-2.1297125" x2="1.29005" y2="-2.117375" layer="21"/>
<rectangle x1="-1.32725" y1="-2.117371875" x2="-0.91985" y2="-2.105021875" layer="21"/>
<rectangle x1="0.90735" y1="-2.117371875" x2="1.30245" y2="-2.105021875" layer="21"/>
<rectangle x1="-1.35195" y1="-2.105021875" x2="-0.89515" y2="-2.092684375" layer="21"/>
<rectangle x1="0.87035" y1="-2.105021875" x2="1.32715" y2="-2.092684375" layer="21"/>
<rectangle x1="-1.37655" y1="-2.09268125" x2="-0.85805" y2="-2.08033125" layer="21"/>
<rectangle x1="0.84565" y1="-2.09268125" x2="1.35185" y2="-2.08033125" layer="21"/>
<rectangle x1="-1.38895" y1="-2.08033125" x2="-0.82105" y2="-2.06798125" layer="21"/>
<rectangle x1="0.80855" y1="-2.08033125" x2="1.37645" y2="-2.06798125" layer="21"/>
<rectangle x1="-1.41365" y1="-2.06798125" x2="-0.79635" y2="-2.05564375" layer="21"/>
<rectangle x1="0.77155" y1="-2.06798125" x2="1.40115" y2="-2.05564375" layer="21"/>
<rectangle x1="-1.43835" y1="-2.055640625" x2="-0.75935" y2="-2.043290625" layer="21"/>
<rectangle x1="0.74685" y1="-2.055640625" x2="1.42585" y2="-2.043290625" layer="21"/>
<rectangle x1="-1.46305" y1="-2.043290625" x2="-0.72225" y2="-2.030953125" layer="21"/>
<rectangle x1="0.70985" y1="-2.043290625" x2="1.45055" y2="-2.030953125" layer="21"/>
<rectangle x1="-1.48775" y1="-2.03095" x2="-0.68525" y2="-2.0186" layer="21"/>
<rectangle x1="0.67275" y1="-2.03095" x2="1.47525" y2="-2.0186" layer="21"/>
<rectangle x1="-1.51235" y1="-2.0186" x2="-0.64815" y2="-2.0062625" layer="21"/>
<rectangle x1="0.63575" y1="-2.0186" x2="1.49995" y2="-2.0062625" layer="21"/>
<rectangle x1="-1.53705" y1="-2.0062625" x2="-0.61115" y2="-1.9939125" layer="21"/>
<rectangle x1="0.59875" y1="-2.0062625" x2="1.52465" y2="-1.9939125" layer="21"/>
<rectangle x1="-1.56175" y1="-1.9939125" x2="-0.57415" y2="-1.981575" layer="21"/>
<rectangle x1="0.56165" y1="-1.9939125" x2="1.53695" y2="-1.981575" layer="21"/>
<rectangle x1="-1.58645" y1="-1.981571875" x2="-0.53705" y2="-1.969221875" layer="21"/>
<rectangle x1="0.52465" y1="-1.981571875" x2="1.56165" y2="-1.969221875" layer="21"/>
<rectangle x1="-1.61115" y1="-1.969221875" x2="-0.50005" y2="-1.956871875" layer="21"/>
<rectangle x1="0.48765" y1="-1.969221875" x2="1.58635" y2="-1.956871875" layer="21"/>
<rectangle x1="-1.63585" y1="-1.956871875" x2="-0.46305" y2="-1.944534375" layer="21"/>
<rectangle x1="0.43825" y1="-1.956871875" x2="1.61105" y2="-1.944534375" layer="21"/>
<rectangle x1="-1.64815" y1="-1.94453125" x2="-0.41365" y2="-1.93218125" layer="21"/>
<rectangle x1="0.40115" y1="-1.94453125" x2="1.63575" y2="-1.93218125" layer="21"/>
<rectangle x1="-1.67285" y1="-1.93218125" x2="-0.36425" y2="-1.91984375" layer="21"/>
<rectangle x1="0.35185" y1="-1.93218125" x2="1.66045" y2="-1.91984375" layer="21"/>
<rectangle x1="-1.69755" y1="-1.919840625" x2="-0.31485" y2="-1.907490625" layer="21"/>
<rectangle x1="0.30245" y1="-1.919840625" x2="1.68515" y2="-1.907490625" layer="21"/>
<rectangle x1="-1.72225" y1="-1.907490625" x2="-0.25315" y2="-1.895153125" layer="21"/>
<rectangle x1="0.24065" y1="-1.907490625" x2="1.70985" y2="-1.895153125" layer="21"/>
<rectangle x1="-1.74695" y1="-1.89515" x2="-0.17905" y2="-1.8828" layer="21"/>
<rectangle x1="0.16665" y1="-1.89515" x2="1.73455" y2="-1.8828" layer="21"/>
<rectangle x1="-1.77165" y1="-1.8828" x2="1.75925" y2="-1.87045" layer="21"/>
<rectangle x1="-1.79635" y1="-1.87045" x2="1.78395" y2="-1.8581125" layer="21"/>
<rectangle x1="-1.82105" y1="-1.8581125" x2="1.79625" y2="-1.8457625" layer="21"/>
<rectangle x1="-1.84575" y1="-1.8457625" x2="1.82095" y2="-1.833425" layer="21"/>
<rectangle x1="-1.87045" y1="-1.833421875" x2="1.84565" y2="-1.821071875" layer="21"/>
<rectangle x1="-1.88275" y1="-1.821071875" x2="1.87035" y2="-1.808734375" layer="21"/>
<rectangle x1="-1.90745" y1="-1.80873125" x2="1.89505" y2="-1.79638125" layer="21"/>
<rectangle x1="-1.93215" y1="-1.79638125" x2="1.91975" y2="-1.78403125" layer="21"/>
<rectangle x1="-1.95685" y1="-1.78403125" x2="1.94435" y2="-1.77169375" layer="21"/>
<rectangle x1="-1.98155" y1="-1.771690625" x2="1.96905" y2="-1.759340625" layer="21"/>
<rectangle x1="-2.00625" y1="-1.759340625" x2="1.98145" y2="-1.747003125" layer="21"/>
<rectangle x1="-1.98155" y1="-1.747" x2="1.95675" y2="-1.73465" layer="21"/>
<rectangle x1="-1.95685" y1="-1.73465" x2="1.93205" y2="-1.7223125" layer="21"/>
<rectangle x1="-1.93215" y1="-1.7223125" x2="1.90735" y2="-1.7099625" layer="21"/>
<rectangle x1="-1.90745" y1="-1.7099625" x2="1.89505" y2="-1.6976125" layer="21"/>
<rectangle x1="-1.88275" y1="-1.6976125" x2="1.87035" y2="-1.685275" layer="21"/>
<rectangle x1="-1.85805" y1="-1.685271875" x2="1.84565" y2="-1.672921875" layer="21"/>
<rectangle x1="-1.83335" y1="-1.672921875" x2="1.82095" y2="-1.660584375" layer="21"/>
<rectangle x1="-1.80865" y1="-1.66058125" x2="1.79625" y2="-1.64823125" layer="21"/>
<rectangle x1="-1.78405" y1="-1.64823125" x2="1.77155" y2="-1.63589375" layer="21"/>
<rectangle x1="-1.75935" y1="-1.635890625" x2="-0.09265" y2="-1.623540625" layer="21"/>
<rectangle x1="0.06785" y1="-1.635890625" x2="1.74685" y2="-1.623540625" layer="21"/>
<rectangle x1="-1.74695" y1="-1.623540625" x2="-0.20375" y2="-1.611190625" layer="21"/>
<rectangle x1="0.17895" y1="-1.623540625" x2="1.72215" y2="-1.611190625" layer="21"/>
<rectangle x1="-1.72225" y1="-1.611190625" x2="-0.26545" y2="-1.598853125" layer="21"/>
<rectangle x1="0.25305" y1="-1.611190625" x2="1.69745" y2="-1.598853125" layer="21"/>
<rectangle x1="-1.69755" y1="-1.59885" x2="-0.32725" y2="-1.5865" layer="21"/>
<rectangle x1="0.31475" y1="-1.59885" x2="1.67275" y2="-1.5865" layer="21"/>
<rectangle x1="-1.67285" y1="-1.5865" x2="-0.37655" y2="-1.5741625" layer="21"/>
<rectangle x1="0.36415" y1="-1.5865" x2="1.66045" y2="-1.5741625" layer="21"/>
<rectangle x1="-1.64815" y1="-1.5741625" x2="-0.42595" y2="-1.5618125" layer="21"/>
<rectangle x1="0.41355" y1="-1.5741625" x2="1.63575" y2="-1.5618125" layer="21"/>
<rectangle x1="-1.62355" y1="-1.5618125" x2="-0.47535" y2="-1.549475" layer="21"/>
<rectangle x1="0.45055" y1="-1.5618125" x2="1.61105" y2="-1.549475" layer="21"/>
<rectangle x1="-1.59885" y1="-1.549471875" x2="-0.51235" y2="-1.537121875" layer="21"/>
<rectangle x1="0.49995" y1="-1.549471875" x2="1.58635" y2="-1.537121875" layer="21"/>
<rectangle x1="-1.57415" y1="-1.537121875" x2="-0.54945" y2="-1.524784375" layer="21"/>
<rectangle x1="0.53695" y1="-1.537121875" x2="1.56165" y2="-1.524784375" layer="21"/>
<rectangle x1="-1.54945" y1="-1.52478125" x2="-0.58645" y2="-1.51243125" layer="21"/>
<rectangle x1="0.57405" y1="-1.52478125" x2="1.53695" y2="-1.51243125" layer="21"/>
<rectangle x1="-1.52475" y1="-1.51243125" x2="-0.62355" y2="-1.50008125" layer="21"/>
<rectangle x1="0.61105" y1="-1.51243125" x2="1.51225" y2="-1.50008125" layer="21"/>
<rectangle x1="-1.51235" y1="-1.50008125" x2="-0.66055" y2="-1.48774375" layer="21"/>
<rectangle x1="0.64805" y1="-1.50008125" x2="1.48765" y2="-1.48774375" layer="21"/>
<rectangle x1="-1.48775" y1="-1.487740625" x2="-0.69755" y2="-1.475390625" layer="21"/>
<rectangle x1="0.68515" y1="-1.487740625" x2="1.46295" y2="-1.475390625" layer="21"/>
<rectangle x1="-1.46305" y1="-1.475390625" x2="-0.73465" y2="-1.463053125" layer="21"/>
<rectangle x1="0.72215" y1="-1.475390625" x2="1.43825" y2="-1.463053125" layer="21"/>
<rectangle x1="-1.43835" y1="-1.46305" x2="-0.77165" y2="-1.4507" layer="21"/>
<rectangle x1="0.74685" y1="-1.46305" x2="1.41355" y2="-1.4507" layer="21"/>
<rectangle x1="-1.41365" y1="-1.4507" x2="-0.79635" y2="-1.4383625" layer="21"/>
<rectangle x1="0.78395" y1="-1.4507" x2="1.40115" y2="-1.4383625" layer="21"/>
<rectangle x1="-1.38895" y1="-1.4383625" x2="-0.83335" y2="-1.4260125" layer="21"/>
<rectangle x1="0.82095" y1="-1.4383625" x2="1.37645" y2="-1.4260125" layer="21"/>
<rectangle x1="-1.36425" y1="-1.4260125" x2="-0.87045" y2="-1.4136625" layer="21"/>
<rectangle x1="0.84565" y1="-1.4260125" x2="1.35185" y2="-1.4136625" layer="21"/>
<rectangle x1="-1.33955" y1="-1.4136625" x2="-0.89515" y2="-1.401325" layer="21"/>
<rectangle x1="0.88265" y1="-1.4136625" x2="1.32715" y2="-1.401325" layer="21"/>
<rectangle x1="-1.31485" y1="-1.401321875" x2="-0.93215" y2="-1.388971875" layer="21"/>
<rectangle x1="0.91975" y1="-1.401321875" x2="1.30245" y2="-1.388971875" layer="21"/>
<rectangle x1="-1.29015" y1="-1.388971875" x2="-0.96915" y2="-1.376634375" layer="21"/>
<rectangle x1="0.94435" y1="-1.388971875" x2="1.27775" y2="-1.376634375" layer="21"/>
<rectangle x1="-1.26545" y1="-1.37663125" x2="-0.99385" y2="-1.36428125" layer="21"/>
<rectangle x1="0.98145" y1="-1.37663125" x2="1.25305" y2="-1.36428125" layer="21"/>
<rectangle x1="-1.25315" y1="-1.36428125" x2="-1.03095" y2="-1.35194375" layer="21"/>
<rectangle x1="1.01845" y1="-1.36428125" x2="1.22835" y2="-1.35194375" layer="21"/>
<rectangle x1="-1.22845" y1="-1.351940625" x2="-1.06795" y2="-1.339590625" layer="21"/>
<rectangle x1="1.04315" y1="-1.351940625" x2="1.20365" y2="-1.339590625" layer="21"/>
<rectangle x1="-1.20375" y1="-1.339590625" x2="-1.09265" y2="-1.327240625" layer="21"/>
<rectangle x1="1.08015" y1="-1.339590625" x2="1.17895" y2="-1.327240625" layer="21"/>
<rectangle x1="-1.17905" y1="-1.327240625" x2="-1.12965" y2="-1.314903125" layer="21"/>
<rectangle x1="1.10485" y1="-1.327240625" x2="1.16665" y2="-1.314903125" layer="21"/>
<rectangle x1="-1.17905" y1="-1.09268125" x2="-1.12965" y2="-1.08033125" layer="21"/>
<rectangle x1="1.11725" y1="-1.09268125" x2="1.15425" y2="-1.08033125" layer="21"/>
<rectangle x1="-1.20375" y1="-1.08033125" x2="-1.09265" y2="-1.06798125" layer="21"/>
<rectangle x1="1.08015" y1="-1.08033125" x2="1.17895" y2="-1.06798125" layer="21"/>
<rectangle x1="-1.21605" y1="-1.06798125" x2="-1.06795" y2="-1.05564375" layer="21"/>
<rectangle x1="1.05555" y1="-1.06798125" x2="1.20365" y2="-1.05564375" layer="21"/>
<rectangle x1="-1.24075" y1="-1.055640625" x2="-1.03095" y2="-1.043290625" layer="21"/>
<rectangle x1="1.01845" y1="-1.055640625" x2="1.22835" y2="-1.043290625" layer="21"/>
<rectangle x1="-1.26545" y1="-1.043290625" x2="-1.00625" y2="-1.030953125" layer="21"/>
<rectangle x1="0.98145" y1="-1.043290625" x2="1.25305" y2="-1.030953125" layer="21"/>
<rectangle x1="-1.29015" y1="-1.03095" x2="-0.96915" y2="-1.0186" layer="21"/>
<rectangle x1="0.95675" y1="-1.03095" x2="1.27775" y2="-1.0186" layer="21"/>
<rectangle x1="-1.31485" y1="-1.0186" x2="-0.93215" y2="-1.0062625" layer="21"/>
<rectangle x1="0.91975" y1="-1.0186" x2="1.30245" y2="-1.0062625" layer="21"/>
<rectangle x1="-1.33955" y1="-1.0062625" x2="-0.90745" y2="-0.9939125" layer="21"/>
<rectangle x1="0.88265" y1="-1.0062625" x2="1.32715" y2="-0.9939125" layer="21"/>
<rectangle x1="-1.36425" y1="-0.9939125" x2="-0.87045" y2="-0.981575" layer="21"/>
<rectangle x1="0.85795" y1="-0.9939125" x2="1.35185" y2="-0.981575" layer="21"/>
<rectangle x1="-1.38895" y1="-0.981571875" x2="-0.83335" y2="-0.969221875" layer="21"/>
<rectangle x1="0.82095" y1="-0.981571875" x2="1.36415" y2="-0.969221875" layer="21"/>
<rectangle x1="-1.41365" y1="-0.969221875" x2="-0.80865" y2="-0.956871875" layer="21"/>
<rectangle x1="0.78395" y1="-0.969221875" x2="1.38885" y2="-0.956871875" layer="21"/>
<rectangle x1="-1.43835" y1="-0.956871875" x2="-0.77165" y2="-0.944534375" layer="21"/>
<rectangle x1="0.75925" y1="-0.956871875" x2="1.41355" y2="-0.944534375" layer="21"/>
<rectangle x1="-1.45065" y1="-0.94453125" x2="-0.73465" y2="-0.93218125" layer="21"/>
<rectangle x1="0.72215" y1="-0.94453125" x2="1.43825" y2="-0.93218125" layer="21"/>
<rectangle x1="-1.47535" y1="-0.93218125" x2="-0.69755" y2="-0.91984375" layer="21"/>
<rectangle x1="0.68515" y1="-0.93218125" x2="1.46295" y2="-0.91984375" layer="21"/>
<rectangle x1="-1.50005" y1="-0.919840625" x2="-0.67285" y2="-0.907490625" layer="21"/>
<rectangle x1="0.64805" y1="-0.919840625" x2="1.48765" y2="-0.907490625" layer="21"/>
<rectangle x1="-1.52475" y1="-0.907490625" x2="-0.63585" y2="-0.895153125" layer="21"/>
<rectangle x1="0.61105" y1="-0.907490625" x2="1.51225" y2="-0.895153125" layer="21"/>
<rectangle x1="-1.54945" y1="-0.89515" x2="-0.59885" y2="-0.8828" layer="21"/>
<rectangle x1="0.57405" y1="-0.89515" x2="1.53695" y2="-0.8828" layer="21"/>
<rectangle x1="-1.57415" y1="-0.8828" x2="-0.56175" y2="-0.87045" layer="21"/>
<rectangle x1="0.53695" y1="-0.8828" x2="1.56165" y2="-0.87045" layer="21"/>
<rectangle x1="-1.59885" y1="-0.87045" x2="-0.51235" y2="-0.8581125" layer="21"/>
<rectangle x1="0.49995" y1="-0.87045" x2="1.58635" y2="-0.8581125" layer="21"/>
<rectangle x1="-1.62355" y1="-0.8581125" x2="-0.47535" y2="-0.8457625" layer="21"/>
<rectangle x1="0.46295" y1="-0.8581125" x2="1.59875" y2="-0.8457625" layer="21"/>
<rectangle x1="-1.64815" y1="-0.8457625" x2="-0.43835" y2="-0.833425" layer="21"/>
<rectangle x1="0.41355" y1="-0.8457625" x2="1.62345" y2="-0.833425" layer="21"/>
<rectangle x1="-1.67285" y1="-0.833421875" x2="-0.38895" y2="-0.821071875" layer="21"/>
<rectangle x1="0.36415" y1="-0.833421875" x2="1.64805" y2="-0.821071875" layer="21"/>
<rectangle x1="-1.69755" y1="-0.821071875" x2="-0.33955" y2="-0.808734375" layer="21"/>
<rectangle x1="0.31475" y1="-0.821071875" x2="1.67275" y2="-0.808734375" layer="21"/>
<rectangle x1="-1.70995" y1="-0.80873125" x2="-0.27785" y2="-0.79638125" layer="21"/>
<rectangle x1="0.26535" y1="-0.80873125" x2="1.69745" y2="-0.79638125" layer="21"/>
<rectangle x1="-1.73465" y1="-0.79638125" x2="-0.21605" y2="-0.78403125" layer="21"/>
<rectangle x1="0.19135" y1="-0.79638125" x2="1.72215" y2="-0.78403125" layer="21"/>
<rectangle x1="-1.75935" y1="-0.78403125" x2="-0.10495" y2="-0.77169375" layer="21"/>
<rectangle x1="0.09255" y1="-0.78403125" x2="1.74685" y2="-0.77169375" layer="21"/>
<rectangle x1="-1.78405" y1="-0.771690625" x2="1.77155" y2="-0.759340625" layer="21"/>
<rectangle x1="-1.80865" y1="-0.759340625" x2="1.79625" y2="-0.747003125" layer="21"/>
<rectangle x1="-1.83335" y1="-0.747" x2="1.82095" y2="-0.73465" layer="21"/>
<rectangle x1="-1.85805" y1="-0.73465" x2="1.84565" y2="-0.7223125" layer="21"/>
<rectangle x1="-1.88275" y1="-0.7223125" x2="1.85795" y2="-0.7099625" layer="21"/>
<rectangle x1="-1.90745" y1="-0.7099625" x2="1.88265" y2="-0.6976125" layer="21"/>
<rectangle x1="-1.93215" y1="-0.6976125" x2="1.90735" y2="-0.685275" layer="21"/>
<rectangle x1="-1.95685" y1="-0.685271875" x2="1.93205" y2="-0.672921875" layer="21"/>
<rectangle x1="-1.96915" y1="-0.672921875" x2="1.95675" y2="-0.660584375" layer="21"/>
<rectangle x1="-1.99385" y1="-0.66058125" x2="1.98145" y2="-0.64823125" layer="21"/>
<rectangle x1="-1.98155" y1="-0.64823125" x2="1.96905" y2="-0.63589375" layer="21"/>
<rectangle x1="-1.95685" y1="-0.635890625" x2="1.94435" y2="-0.623540625" layer="21"/>
<rectangle x1="-1.94445" y1="-0.623540625" x2="1.91975" y2="-0.611190625" layer="21"/>
<rectangle x1="-1.91985" y1="-0.611190625" x2="1.89505" y2="-0.598853125" layer="21"/>
<rectangle x1="-1.89515" y1="-0.59885" x2="1.87035" y2="-0.5865" layer="21"/>
<rectangle x1="-1.87045" y1="-0.5865" x2="1.84565" y2="-0.5741625" layer="21"/>
<rectangle x1="-1.84575" y1="-0.5741625" x2="1.83325" y2="-0.5618125" layer="21"/>
<rectangle x1="-1.82105" y1="-0.5618125" x2="1.80855" y2="-0.549475" layer="21"/>
<rectangle x1="-1.79635" y1="-0.549471875" x2="1.78395" y2="-0.537121875" layer="21"/>
<rectangle x1="-1.77165" y1="-0.537121875" x2="1.75925" y2="-0.524784375" layer="21"/>
<rectangle x1="-1.74695" y1="-0.52478125" x2="-0.16675" y2="-0.51243125" layer="21"/>
<rectangle x1="0.15425" y1="-0.52478125" x2="1.73455" y2="-0.51243125" layer="21"/>
<rectangle x1="-1.72225" y1="-0.51243125" x2="-0.24075" y2="-0.50008125" layer="21"/>
<rectangle x1="0.22835" y1="-0.51243125" x2="1.70985" y2="-0.50008125" layer="21"/>
<rectangle x1="-1.69755" y1="-0.50008125" x2="-0.30255" y2="-0.48774375" layer="21"/>
<rectangle x1="0.29005" y1="-0.50008125" x2="1.68515" y2="-0.48774375" layer="21"/>
<rectangle x1="-1.68525" y1="-0.487740625" x2="-0.36425" y2="-0.475390625" layer="21"/>
<rectangle x1="0.33945" y1="-0.487740625" x2="1.66045" y2="-0.475390625" layer="21"/>
<rectangle x1="-1.66055" y1="-0.475390625" x2="-0.41365" y2="-0.463053125" layer="21"/>
<rectangle x1="0.38885" y1="-0.475390625" x2="1.63575" y2="-0.463053125" layer="21"/>
<rectangle x1="-1.63585" y1="-0.46305" x2="-0.45065" y2="-0.4507" layer="21"/>
<rectangle x1="0.43825" y1="-0.46305" x2="1.61105" y2="-0.4507" layer="21"/>
<rectangle x1="-1.61115" y1="-0.4507" x2="-0.50005" y2="-0.4383625" layer="21"/>
<rectangle x1="0.47525" y1="-0.4507" x2="1.59875" y2="-0.4383625" layer="21"/>
<rectangle x1="-1.58645" y1="-0.4383625" x2="-0.53705" y2="-0.4260125" layer="21"/>
<rectangle x1="0.52465" y1="-0.4383625" x2="1.57405" y2="-0.4260125" layer="21"/>
<rectangle x1="-1.56175" y1="-0.4260125" x2="-0.57415" y2="-0.4136625" layer="21"/>
<rectangle x1="0.56165" y1="-0.4260125" x2="1.54935" y2="-0.4136625" layer="21"/>
<rectangle x1="-1.53705" y1="-0.4136625" x2="-0.61115" y2="-0.401325" layer="21"/>
<rectangle x1="0.59875" y1="-0.4136625" x2="1.52465" y2="-0.401325" layer="21"/>
<rectangle x1="-1.51235" y1="-0.401321875" x2="-0.64815" y2="-0.388971875" layer="21"/>
<rectangle x1="0.63575" y1="-0.401321875" x2="1.49995" y2="-0.388971875" layer="21"/>
<rectangle x1="-1.48775" y1="-0.388971875" x2="-0.68525" y2="-0.376634375" layer="21"/>
<rectangle x1="0.67275" y1="-0.388971875" x2="1.47525" y2="-0.376634375" layer="21"/>
<rectangle x1="-1.46305" y1="-0.37663125" x2="-0.72225" y2="-0.36428125" layer="21"/>
<rectangle x1="0.69745" y1="-0.37663125" x2="1.45055" y2="-0.36428125" layer="21"/>
<rectangle x1="-1.45065" y1="-0.36428125" x2="-0.75935" y2="-0.35194375" layer="21"/>
<rectangle x1="0.73455" y1="-0.36428125" x2="1.42585" y2="-0.35194375" layer="21"/>
<rectangle x1="-1.42595" y1="-0.351940625" x2="-0.78405" y2="-0.339590625" layer="21"/>
<rectangle x1="0.77155" y1="-0.351940625" x2="1.40115" y2="-0.339590625" layer="21"/>
<rectangle x1="-1.40125" y1="-0.339590625" x2="-0.82105" y2="-0.327240625" layer="21"/>
<rectangle x1="0.80855" y1="-0.339590625" x2="1.37645" y2="-0.327240625" layer="21"/>
<rectangle x1="-1.37655" y1="-0.327240625" x2="-0.85805" y2="-0.314903125" layer="21"/>
<rectangle x1="0.83325" y1="-0.327240625" x2="1.35185" y2="-0.314903125" layer="21"/>
<rectangle x1="-1.35195" y1="-0.3149" x2="-0.88275" y2="-0.30255" layer="21"/>
<rectangle x1="0.87035" y1="-0.3149" x2="1.33945" y2="-0.30255" layer="21"/>
<rectangle x1="-1.32725" y1="-0.30255" x2="-0.91985" y2="-0.2902125" layer="21"/>
<rectangle x1="0.90735" y1="-0.30255" x2="1.31475" y2="-0.2902125" layer="21"/>
<rectangle x1="-1.30255" y1="-0.2902125" x2="-0.95685" y2="-0.2778625" layer="21"/>
<rectangle x1="0.93205" y1="-0.2902125" x2="1.29005" y2="-0.2778625" layer="21"/>
<rectangle x1="-1.27785" y1="-0.2778625" x2="-0.98155" y2="-0.265525" layer="21"/>
<rectangle x1="0.96905" y1="-0.2778625" x2="1.26535" y2="-0.265525" layer="21"/>
<rectangle x1="-1.25315" y1="-0.265521875" x2="-1.01855" y2="-0.253171875" layer="21"/>
<rectangle x1="1.00615" y1="-0.265521875" x2="1.24065" y2="-0.253171875" layer="21"/>
<rectangle x1="-1.22845" y1="-0.253171875" x2="-1.05565" y2="-0.240821875" layer="21"/>
<rectangle x1="1.03085" y1="-0.253171875" x2="1.21595" y2="-0.240821875" layer="21"/>
<rectangle x1="-1.21605" y1="-0.240821875" x2="-1.08025" y2="-0.228484375" layer="21"/>
<rectangle x1="1.06785" y1="-0.240821875" x2="1.19135" y2="-0.228484375" layer="21"/>
<rectangle x1="-1.19145" y1="-0.22848125" x2="-1.11735" y2="-0.21613125" layer="21"/>
<rectangle x1="1.09255" y1="-0.22848125" x2="1.16665" y2="-0.21613125" layer="21"/>
<rectangle x1="-1.16675" y1="-0.21613125" x2="-1.14205" y2="-0.20379375" layer="21"/>
<rectangle x1="1.12955" y1="-0.21613125" x2="1.14195" y2="-0.20379375" layer="21"/>
<rectangle x1="1.15425" y1="0.376459375" x2="1.27775" y2="0.388809375" layer="21"/>
<rectangle x1="1.04315" y1="0.388809375" x2="1.38885" y2="0.401146875" layer="21"/>
<rectangle x1="0.99375" y1="0.40115" x2="1.43825" y2="0.4135" layer="21"/>
<rectangle x1="-1.78405" y1="0.4135" x2="-1.05565" y2="0.4258375" layer="21"/>
<rectangle x1="-0.19145" y1="0.4135" x2="0.15425" y2="0.4258375" layer="21"/>
<rectangle x1="0.95675" y1="0.4135" x2="1.47525" y2="0.4258375" layer="21"/>
<rectangle x1="-1.79635" y1="0.4258375" x2="-0.99385" y2="0.4381875" layer="21"/>
<rectangle x1="-0.19145" y1="0.4258375" x2="0.16665" y2="0.4381875" layer="21"/>
<rectangle x1="0.91975" y1="0.4258375" x2="1.51225" y2="0.4381875" layer="21"/>
<rectangle x1="-1.79635" y1="0.4381875" x2="-0.95685" y2="0.450525" layer="21"/>
<rectangle x1="-0.19145" y1="0.4381875" x2="0.16665" y2="0.450525" layer="21"/>
<rectangle x1="0.89505" y1="0.4381875" x2="1.53695" y2="0.450525" layer="21"/>
<rectangle x1="-1.79635" y1="0.450528125" x2="-0.93215" y2="0.462878125" layer="21"/>
<rectangle x1="-0.19145" y1="0.450528125" x2="0.16665" y2="0.462878125" layer="21"/>
<rectangle x1="0.88265" y1="0.450528125" x2="1.56165" y2="0.462878125" layer="21"/>
<rectangle x1="-1.79635" y1="0.462878125" x2="-0.90745" y2="0.475215625" layer="21"/>
<rectangle x1="-0.19145" y1="0.462878125" x2="0.16665" y2="0.475215625" layer="21"/>
<rectangle x1="0.85795" y1="0.462878125" x2="1.57405" y2="0.475215625" layer="21"/>
<rectangle x1="-1.79635" y1="0.47521875" x2="-0.88275" y2="0.48756875" layer="21"/>
<rectangle x1="-0.19145" y1="0.47521875" x2="0.16665" y2="0.48756875" layer="21"/>
<rectangle x1="0.84565" y1="0.47521875" x2="1.59875" y2="0.48756875" layer="21"/>
<rectangle x1="-1.79635" y1="0.48756875" x2="-0.87045" y2="0.49991875" layer="21"/>
<rectangle x1="-0.19145" y1="0.48756875" x2="0.16665" y2="0.49991875" layer="21"/>
<rectangle x1="0.82095" y1="0.48756875" x2="1.61105" y2="0.49991875" layer="21"/>
<rectangle x1="-1.79635" y1="0.49991875" x2="-0.85805" y2="0.51225625" layer="21"/>
<rectangle x1="-0.19145" y1="0.49991875" x2="0.16665" y2="0.51225625" layer="21"/>
<rectangle x1="0.80855" y1="0.49991875" x2="1.62345" y2="0.51225625" layer="21"/>
<rectangle x1="-1.79635" y1="0.512259375" x2="-0.83335" y2="0.524609375" layer="21"/>
<rectangle x1="-0.19145" y1="0.512259375" x2="0.16665" y2="0.524609375" layer="21"/>
<rectangle x1="0.79625" y1="0.512259375" x2="1.63575" y2="0.524609375" layer="21"/>
<rectangle x1="-1.79635" y1="0.524609375" x2="-0.82105" y2="0.536946875" layer="21"/>
<rectangle x1="-0.19145" y1="0.524609375" x2="0.16665" y2="0.536946875" layer="21"/>
<rectangle x1="0.78395" y1="0.524609375" x2="1.64805" y2="0.536946875" layer="21"/>
<rectangle x1="-1.79635" y1="0.53695" x2="-0.82105" y2="0.5493" layer="21"/>
<rectangle x1="-0.19145" y1="0.53695" x2="0.16665" y2="0.5493" layer="21"/>
<rectangle x1="0.77155" y1="0.53695" x2="1.66045" y2="0.5493" layer="21"/>
<rectangle x1="-1.79635" y1="0.5493" x2="-0.80865" y2="0.5616375" layer="21"/>
<rectangle x1="-0.19145" y1="0.5493" x2="0.16665" y2="0.5616375" layer="21"/>
<rectangle x1="0.75925" y1="0.5493" x2="1.66045" y2="0.5616375" layer="21"/>
<rectangle x1="-1.79635" y1="0.5616375" x2="-0.79635" y2="0.5739875" layer="21"/>
<rectangle x1="-0.19145" y1="0.5616375" x2="0.16665" y2="0.5739875" layer="21"/>
<rectangle x1="0.75925" y1="0.5616375" x2="1.67275" y2="0.5739875" layer="21"/>
<rectangle x1="-1.79635" y1="0.5739875" x2="-0.78405" y2="0.5863375" layer="21"/>
<rectangle x1="-0.19145" y1="0.5739875" x2="0.16665" y2="0.5863375" layer="21"/>
<rectangle x1="0.74685" y1="0.5739875" x2="1.68515" y2="0.5863375" layer="21"/>
<rectangle x1="-1.79635" y1="0.5863375" x2="-0.78405" y2="0.598675" layer="21"/>
<rectangle x1="-0.19145" y1="0.5863375" x2="0.16665" y2="0.598675" layer="21"/>
<rectangle x1="0.74685" y1="0.5863375" x2="1.68515" y2="0.598675" layer="21"/>
<rectangle x1="-1.79635" y1="0.598678125" x2="-0.77165" y2="0.611028125" layer="21"/>
<rectangle x1="-0.19145" y1="0.598678125" x2="0.16665" y2="0.611028125" layer="21"/>
<rectangle x1="0.73455" y1="0.598678125" x2="1.69745" y2="0.611028125" layer="21"/>
<rectangle x1="-1.79635" y1="0.611028125" x2="-0.77165" y2="0.623365625" layer="21"/>
<rectangle x1="-0.19145" y1="0.611028125" x2="0.16665" y2="0.623365625" layer="21"/>
<rectangle x1="0.72215" y1="0.611028125" x2="1.69745" y2="0.623365625" layer="21"/>
<rectangle x1="-1.79635" y1="0.62336875" x2="-0.75935" y2="0.63571875" layer="21"/>
<rectangle x1="-0.19145" y1="0.62336875" x2="0.16665" y2="0.63571875" layer="21"/>
<rectangle x1="0.72215" y1="0.62336875" x2="1.15425" y2="0.63571875" layer="21"/>
<rectangle x1="1.27775" y1="0.62336875" x2="1.70985" y2="0.63571875" layer="21"/>
<rectangle x1="-1.79635" y1="0.63571875" x2="-0.75935" y2="0.64805625" layer="21"/>
<rectangle x1="-0.19145" y1="0.63571875" x2="0.16665" y2="0.64805625" layer="21"/>
<rectangle x1="0.72215" y1="0.63571875" x2="1.11725" y2="0.64805625" layer="21"/>
<rectangle x1="1.31475" y1="0.63571875" x2="1.70985" y2="0.64805625" layer="21"/>
<rectangle x1="-1.79635" y1="0.648059375" x2="-0.74695" y2="0.660409375" layer="21"/>
<rectangle x1="-0.19145" y1="0.648059375" x2="0.16665" y2="0.660409375" layer="21"/>
<rectangle x1="0.70985" y1="0.648059375" x2="1.10485" y2="0.660409375" layer="21"/>
<rectangle x1="1.33945" y1="0.648059375" x2="1.72215" y2="0.660409375" layer="21"/>
<rectangle x1="-1.79635" y1="0.660409375" x2="-1.46305" y2="0.672759375" layer="21"/>
<rectangle x1="-1.17905" y1="0.660409375" x2="-0.74695" y2="0.672759375" layer="21"/>
<rectangle x1="-0.19145" y1="0.660409375" x2="0.16665" y2="0.672759375" layer="21"/>
<rectangle x1="0.70985" y1="0.660409375" x2="1.08015" y2="0.672759375" layer="21"/>
<rectangle x1="1.35185" y1="0.660409375" x2="1.72215" y2="0.672759375" layer="21"/>
<rectangle x1="-1.79635" y1="0.672759375" x2="-1.46305" y2="0.685096875" layer="21"/>
<rectangle x1="-1.14205" y1="0.672759375" x2="-0.73465" y2="0.685096875" layer="21"/>
<rectangle x1="-0.19145" y1="0.672759375" x2="0.16665" y2="0.685096875" layer="21"/>
<rectangle x1="0.69745" y1="0.672759375" x2="1.06785" y2="0.685096875" layer="21"/>
<rectangle x1="1.36415" y1="0.672759375" x2="1.73455" y2="0.685096875" layer="21"/>
<rectangle x1="-1.79635" y1="0.6851" x2="-1.46305" y2="0.69745" layer="21"/>
<rectangle x1="-1.12965" y1="0.6851" x2="-0.73465" y2="0.69745" layer="21"/>
<rectangle x1="-0.19145" y1="0.6851" x2="0.16665" y2="0.69745" layer="21"/>
<rectangle x1="0.69745" y1="0.6851" x2="1.05555" y2="0.69745" layer="21"/>
<rectangle x1="1.37645" y1="0.6851" x2="1.73455" y2="0.69745" layer="21"/>
<rectangle x1="-1.79635" y1="0.69745" x2="-1.46305" y2="0.7097875" layer="21"/>
<rectangle x1="-1.11735" y1="0.69745" x2="-0.73465" y2="0.7097875" layer="21"/>
<rectangle x1="-0.19145" y1="0.69745" x2="0.16665" y2="0.7097875" layer="21"/>
<rectangle x1="0.69745" y1="0.69745" x2="1.05555" y2="0.7097875" layer="21"/>
<rectangle x1="1.38885" y1="0.69745" x2="1.73455" y2="0.7097875" layer="21"/>
<rectangle x1="-1.79635" y1="0.7097875" x2="-1.46305" y2="0.7221375" layer="21"/>
<rectangle x1="-1.10495" y1="0.7097875" x2="-0.73465" y2="0.7221375" layer="21"/>
<rectangle x1="-0.19145" y1="0.7097875" x2="0.16665" y2="0.7221375" layer="21"/>
<rectangle x1="0.69745" y1="0.7097875" x2="1.04315" y2="0.7221375" layer="21"/>
<rectangle x1="1.38885" y1="0.7097875" x2="1.73455" y2="0.7221375" layer="21"/>
<rectangle x1="-1.79635" y1="0.7221375" x2="-1.46305" y2="0.734475" layer="21"/>
<rectangle x1="-1.09265" y1="0.7221375" x2="-0.72225" y2="0.734475" layer="21"/>
<rectangle x1="-0.19145" y1="0.7221375" x2="0.16665" y2="0.734475" layer="21"/>
<rectangle x1="0.68515" y1="0.7221375" x2="1.04315" y2="0.734475" layer="21"/>
<rectangle x1="1.40115" y1="0.7221375" x2="1.74685" y2="0.734475" layer="21"/>
<rectangle x1="-1.79635" y1="0.734478125" x2="-1.46305" y2="0.746828125" layer="21"/>
<rectangle x1="-1.08025" y1="0.734478125" x2="-0.72225" y2="0.746828125" layer="21"/>
<rectangle x1="-0.19145" y1="0.734478125" x2="0.16665" y2="0.746828125" layer="21"/>
<rectangle x1="0.68515" y1="0.734478125" x2="1.03085" y2="0.746828125" layer="21"/>
<rectangle x1="1.40115" y1="0.734478125" x2="1.74685" y2="0.746828125" layer="21"/>
<rectangle x1="-1.79635" y1="0.746828125" x2="-1.46305" y2="0.759178125" layer="21"/>
<rectangle x1="-1.08025" y1="0.746828125" x2="-0.72225" y2="0.759178125" layer="21"/>
<rectangle x1="-0.19145" y1="0.746828125" x2="0.16665" y2="0.759178125" layer="21"/>
<rectangle x1="0.68515" y1="0.746828125" x2="1.03085" y2="0.759178125" layer="21"/>
<rectangle x1="1.40115" y1="0.746828125" x2="1.74685" y2="0.759178125" layer="21"/>
<rectangle x1="-1.79635" y1="0.759178125" x2="-1.46305" y2="0.771515625" layer="21"/>
<rectangle x1="-1.06795" y1="0.759178125" x2="-0.72225" y2="0.771515625" layer="21"/>
<rectangle x1="-0.19145" y1="0.759178125" x2="0.16665" y2="0.771515625" layer="21"/>
<rectangle x1="0.68515" y1="0.759178125" x2="1.03085" y2="0.771515625" layer="21"/>
<rectangle x1="1.41355" y1="0.759178125" x2="1.74685" y2="0.771515625" layer="21"/>
<rectangle x1="-1.79635" y1="0.77151875" x2="-1.46305" y2="0.78386875" layer="21"/>
<rectangle x1="-1.06795" y1="0.77151875" x2="-0.72225" y2="0.78386875" layer="21"/>
<rectangle x1="-0.19145" y1="0.77151875" x2="0.16665" y2="0.78386875" layer="21"/>
<rectangle x1="0.68515" y1="0.77151875" x2="1.01845" y2="0.78386875" layer="21"/>
<rectangle x1="1.41355" y1="0.77151875" x2="1.74685" y2="0.78386875" layer="21"/>
<rectangle x1="-1.79635" y1="0.78386875" x2="-1.46305" y2="0.79620625" layer="21"/>
<rectangle x1="-1.06795" y1="0.78386875" x2="-0.70995" y2="0.79620625" layer="21"/>
<rectangle x1="-0.19145" y1="0.78386875" x2="0.16665" y2="0.79620625" layer="21"/>
<rectangle x1="0.68515" y1="0.78386875" x2="1.01845" y2="0.79620625" layer="21"/>
<rectangle x1="1.41355" y1="0.78386875" x2="1.75925" y2="0.79620625" layer="21"/>
<rectangle x1="-1.79635" y1="0.796209375" x2="-1.46305" y2="0.808559375" layer="21"/>
<rectangle x1="-1.05565" y1="0.796209375" x2="-0.70995" y2="0.808559375" layer="21"/>
<rectangle x1="-0.19145" y1="0.796209375" x2="0.16665" y2="0.808559375" layer="21"/>
<rectangle x1="0.68515" y1="0.796209375" x2="1.01845" y2="0.808559375" layer="21"/>
<rectangle x1="1.41355" y1="0.796209375" x2="1.75925" y2="0.808559375" layer="21"/>
<rectangle x1="-1.79635" y1="0.808559375" x2="-1.46305" y2="0.820896875" layer="21"/>
<rectangle x1="-1.05565" y1="0.808559375" x2="-0.70995" y2="0.820896875" layer="21"/>
<rectangle x1="-0.19145" y1="0.808559375" x2="0.16665" y2="0.820896875" layer="21"/>
<rectangle x1="0.67275" y1="0.808559375" x2="1.01845" y2="0.820896875" layer="21"/>
<rectangle x1="1.41355" y1="0.808559375" x2="1.75925" y2="0.820896875" layer="21"/>
<rectangle x1="-1.79635" y1="0.8209" x2="-1.46305" y2="0.83325" layer="21"/>
<rectangle x1="-1.05565" y1="0.8209" x2="-0.70995" y2="0.83325" layer="21"/>
<rectangle x1="-0.19145" y1="0.8209" x2="0.16665" y2="0.83325" layer="21"/>
<rectangle x1="0.67275" y1="0.8209" x2="1.01845" y2="0.83325" layer="21"/>
<rectangle x1="1.42585" y1="0.8209" x2="1.75925" y2="0.83325" layer="21"/>
<rectangle x1="-1.79635" y1="0.83325" x2="-1.46305" y2="0.8456" layer="21"/>
<rectangle x1="-1.05565" y1="0.83325" x2="-0.70995" y2="0.8456" layer="21"/>
<rectangle x1="-0.19145" y1="0.83325" x2="0.16665" y2="0.8456" layer="21"/>
<rectangle x1="0.67275" y1="0.83325" x2="1.01845" y2="0.8456" layer="21"/>
<rectangle x1="1.42585" y1="0.83325" x2="1.75925" y2="0.8456" layer="21"/>
<rectangle x1="-1.79635" y1="0.8456" x2="-1.46305" y2="0.8579375" layer="21"/>
<rectangle x1="-1.05565" y1="0.8456" x2="-0.70995" y2="0.8579375" layer="21"/>
<rectangle x1="-0.19145" y1="0.8456" x2="0.16665" y2="0.8579375" layer="21"/>
<rectangle x1="0.67275" y1="0.8456" x2="1.01845" y2="0.8579375" layer="21"/>
<rectangle x1="1.42585" y1="0.8456" x2="1.75925" y2="0.8579375" layer="21"/>
<rectangle x1="-1.79635" y1="0.8579375" x2="-1.46305" y2="0.8702875" layer="21"/>
<rectangle x1="-1.04325" y1="0.8579375" x2="-0.70995" y2="0.8702875" layer="21"/>
<rectangle x1="-0.19145" y1="0.8579375" x2="0.16665" y2="0.8702875" layer="21"/>
<rectangle x1="0.67275" y1="0.8579375" x2="1.00615" y2="0.8702875" layer="21"/>
<rectangle x1="1.42585" y1="0.8579375" x2="1.75925" y2="0.8702875" layer="21"/>
<rectangle x1="-1.79635" y1="0.8702875" x2="-1.46305" y2="0.882625" layer="21"/>
<rectangle x1="-1.04325" y1="0.8702875" x2="-0.70995" y2="0.882625" layer="21"/>
<rectangle x1="-0.19145" y1="0.8702875" x2="0.16665" y2="0.882625" layer="21"/>
<rectangle x1="0.67275" y1="0.8702875" x2="1.00615" y2="0.882625" layer="21"/>
<rectangle x1="1.42585" y1="0.8702875" x2="1.75925" y2="0.882625" layer="21"/>
<rectangle x1="-1.79635" y1="0.882628125" x2="-1.46305" y2="0.894978125" layer="21"/>
<rectangle x1="-1.04325" y1="0.882628125" x2="-0.69755" y2="0.894978125" layer="21"/>
<rectangle x1="-0.19145" y1="0.882628125" x2="0.16665" y2="0.894978125" layer="21"/>
<rectangle x1="0.67275" y1="0.882628125" x2="1.00615" y2="0.894978125" layer="21"/>
<rectangle x1="1.42585" y1="0.882628125" x2="1.75925" y2="0.894978125" layer="21"/>
<rectangle x1="-1.79635" y1="0.894978125" x2="-1.46305" y2="0.907315625" layer="21"/>
<rectangle x1="-1.04325" y1="0.894978125" x2="-0.69755" y2="0.907315625" layer="21"/>
<rectangle x1="-0.19145" y1="0.894978125" x2="0.16665" y2="0.907315625" layer="21"/>
<rectangle x1="0.67275" y1="0.894978125" x2="1.00615" y2="0.907315625" layer="21"/>
<rectangle x1="1.42585" y1="0.894978125" x2="1.75925" y2="0.907315625" layer="21"/>
<rectangle x1="-1.79635" y1="0.90731875" x2="-1.46305" y2="0.91966875" layer="21"/>
<rectangle x1="-1.04325" y1="0.90731875" x2="-0.69755" y2="0.91966875" layer="21"/>
<rectangle x1="-0.19145" y1="0.90731875" x2="0.16665" y2="0.91966875" layer="21"/>
<rectangle x1="0.67275" y1="0.90731875" x2="1.00615" y2="0.91966875" layer="21"/>
<rectangle x1="1.42585" y1="0.90731875" x2="1.75925" y2="0.91966875" layer="21"/>
<rectangle x1="-1.79635" y1="0.91966875" x2="-1.46305" y2="0.93201875" layer="21"/>
<rectangle x1="-1.04325" y1="0.91966875" x2="-0.69755" y2="0.93201875" layer="21"/>
<rectangle x1="-0.19145" y1="0.91966875" x2="0.16665" y2="0.93201875" layer="21"/>
<rectangle x1="0.67275" y1="0.91966875" x2="1.00615" y2="0.93201875" layer="21"/>
<rectangle x1="1.42585" y1="0.91966875" x2="1.75925" y2="0.93201875" layer="21"/>
<rectangle x1="-1.79635" y1="0.93201875" x2="-1.46305" y2="0.94435625" layer="21"/>
<rectangle x1="-1.04325" y1="0.93201875" x2="-0.69755" y2="0.94435625" layer="21"/>
<rectangle x1="-0.19145" y1="0.93201875" x2="0.16665" y2="0.94435625" layer="21"/>
<rectangle x1="0.67275" y1="0.93201875" x2="1.00615" y2="0.94435625" layer="21"/>
<rectangle x1="1.42585" y1="0.93201875" x2="1.75925" y2="0.94435625" layer="21"/>
<rectangle x1="-1.79635" y1="0.944359375" x2="-1.46305" y2="0.956709375" layer="21"/>
<rectangle x1="-1.04325" y1="0.944359375" x2="-0.69755" y2="0.956709375" layer="21"/>
<rectangle x1="-0.19145" y1="0.944359375" x2="0.16665" y2="0.956709375" layer="21"/>
<rectangle x1="0.67275" y1="0.944359375" x2="1.00615" y2="0.956709375" layer="21"/>
<rectangle x1="1.42585" y1="0.944359375" x2="1.75925" y2="0.956709375" layer="21"/>
<rectangle x1="-1.79635" y1="0.956709375" x2="-1.46305" y2="0.969046875" layer="21"/>
<rectangle x1="-1.04325" y1="0.956709375" x2="-0.69755" y2="0.969046875" layer="21"/>
<rectangle x1="-0.19145" y1="0.956709375" x2="0.16665" y2="0.969046875" layer="21"/>
<rectangle x1="0.67275" y1="0.956709375" x2="1.00615" y2="0.969046875" layer="21"/>
<rectangle x1="1.42585" y1="0.956709375" x2="1.75925" y2="0.969046875" layer="21"/>
<rectangle x1="-1.79635" y1="0.96905" x2="-1.46305" y2="0.9814" layer="21"/>
<rectangle x1="-1.04325" y1="0.96905" x2="-0.69755" y2="0.9814" layer="21"/>
<rectangle x1="-0.19145" y1="0.96905" x2="0.16665" y2="0.9814" layer="21"/>
<rectangle x1="0.67275" y1="0.96905" x2="1.00615" y2="0.9814" layer="21"/>
<rectangle x1="1.42585" y1="0.96905" x2="1.75925" y2="0.9814" layer="21"/>
<rectangle x1="-1.79635" y1="0.9814" x2="-1.46305" y2="0.9937375" layer="21"/>
<rectangle x1="-1.04325" y1="0.9814" x2="-0.69755" y2="0.9937375" layer="21"/>
<rectangle x1="-0.19145" y1="0.9814" x2="0.16665" y2="0.9937375" layer="21"/>
<rectangle x1="0.67275" y1="0.9814" x2="1.00615" y2="0.9937375" layer="21"/>
<rectangle x1="1.42585" y1="0.9814" x2="1.75925" y2="0.9937375" layer="21"/>
<rectangle x1="-1.79635" y1="0.9937375" x2="-1.46305" y2="1.0060875" layer="21"/>
<rectangle x1="-1.04325" y1="0.9937375" x2="-0.69755" y2="1.0060875" layer="21"/>
<rectangle x1="-0.19145" y1="0.9937375" x2="0.16665" y2="1.0060875" layer="21"/>
<rectangle x1="0.67275" y1="0.9937375" x2="1.00615" y2="1.0060875" layer="21"/>
<rectangle x1="1.42585" y1="0.9937375" x2="1.75925" y2="1.0060875" layer="21"/>
<rectangle x1="-1.79635" y1="1.0060875" x2="-1.46305" y2="1.018425" layer="21"/>
<rectangle x1="-1.04325" y1="1.0060875" x2="-0.69755" y2="1.018425" layer="21"/>
<rectangle x1="-0.19145" y1="1.0060875" x2="0.16665" y2="1.018425" layer="21"/>
<rectangle x1="0.67275" y1="1.0060875" x2="1.00615" y2="1.018425" layer="21"/>
<rectangle x1="1.42585" y1="1.0060875" x2="1.75925" y2="1.018425" layer="21"/>
<rectangle x1="-1.79635" y1="1.018428125" x2="-1.46305" y2="1.030778125" layer="21"/>
<rectangle x1="-1.04325" y1="1.018428125" x2="-0.69755" y2="1.030778125" layer="21"/>
<rectangle x1="-0.19145" y1="1.018428125" x2="0.16665" y2="1.030778125" layer="21"/>
<rectangle x1="0.67275" y1="1.018428125" x2="1.00615" y2="1.030778125" layer="21"/>
<rectangle x1="1.42585" y1="1.018428125" x2="1.75925" y2="1.030778125" layer="21"/>
<rectangle x1="-1.79635" y1="1.030778125" x2="-1.46305" y2="1.043128125" layer="21"/>
<rectangle x1="-1.04325" y1="1.030778125" x2="-0.69755" y2="1.043128125" layer="21"/>
<rectangle x1="-0.19145" y1="1.030778125" x2="0.16665" y2="1.043128125" layer="21"/>
<rectangle x1="0.67275" y1="1.030778125" x2="1.00615" y2="1.043128125" layer="21"/>
<rectangle x1="1.42585" y1="1.030778125" x2="1.75925" y2="1.043128125" layer="21"/>
<rectangle x1="-1.79635" y1="1.043128125" x2="-1.46305" y2="1.055465625" layer="21"/>
<rectangle x1="-1.04325" y1="1.043128125" x2="-0.69755" y2="1.055465625" layer="21"/>
<rectangle x1="-0.19145" y1="1.043128125" x2="0.16665" y2="1.055465625" layer="21"/>
<rectangle x1="0.67275" y1="1.043128125" x2="1.00615" y2="1.055465625" layer="21"/>
<rectangle x1="1.42585" y1="1.043128125" x2="1.75925" y2="1.055465625" layer="21"/>
<rectangle x1="-1.79635" y1="1.05546875" x2="-1.46305" y2="1.06781875" layer="21"/>
<rectangle x1="-1.03095" y1="1.05546875" x2="-0.69755" y2="1.06781875" layer="21"/>
<rectangle x1="-0.19145" y1="1.05546875" x2="0.16665" y2="1.06781875" layer="21"/>
<rectangle x1="0.67275" y1="1.05546875" x2="1.00615" y2="1.06781875" layer="21"/>
<rectangle x1="1.42585" y1="1.05546875" x2="1.75925" y2="1.06781875" layer="21"/>
<rectangle x1="-1.79635" y1="1.06781875" x2="-1.46305" y2="1.08015625" layer="21"/>
<rectangle x1="-1.03095" y1="1.06781875" x2="-0.69755" y2="1.08015625" layer="21"/>
<rectangle x1="-0.19145" y1="1.06781875" x2="0.16665" y2="1.08015625" layer="21"/>
<rectangle x1="0.67275" y1="1.06781875" x2="1.00615" y2="1.08015625" layer="21"/>
<rectangle x1="1.42585" y1="1.06781875" x2="1.75925" y2="1.08015625" layer="21"/>
<rectangle x1="-1.79635" y1="1.080159375" x2="-1.46305" y2="1.092509375" layer="21"/>
<rectangle x1="-1.03095" y1="1.080159375" x2="-0.69755" y2="1.092509375" layer="21"/>
<rectangle x1="-0.19145" y1="1.080159375" x2="0.16665" y2="1.092509375" layer="21"/>
<rectangle x1="0.67275" y1="1.080159375" x2="1.00615" y2="1.092509375" layer="21"/>
<rectangle x1="1.42585" y1="1.080159375" x2="1.75925" y2="1.092509375" layer="21"/>
<rectangle x1="-1.79635" y1="1.092509375" x2="-1.46305" y2="1.104846875" layer="21"/>
<rectangle x1="-1.03095" y1="1.092509375" x2="-0.69755" y2="1.104846875" layer="21"/>
<rectangle x1="-0.19145" y1="1.092509375" x2="0.16665" y2="1.104846875" layer="21"/>
<rectangle x1="0.67275" y1="1.092509375" x2="1.00615" y2="1.104846875" layer="21"/>
<rectangle x1="1.42585" y1="1.092509375" x2="1.75925" y2="1.104846875" layer="21"/>
<rectangle x1="-1.79635" y1="1.10485" x2="-1.46305" y2="1.1172" layer="21"/>
<rectangle x1="-1.03095" y1="1.10485" x2="-0.69755" y2="1.1172" layer="21"/>
<rectangle x1="-0.19145" y1="1.10485" x2="0.16665" y2="1.1172" layer="21"/>
<rectangle x1="0.67275" y1="1.10485" x2="1.00615" y2="1.1172" layer="21"/>
<rectangle x1="1.42585" y1="1.10485" x2="1.75925" y2="1.1172" layer="21"/>
<rectangle x1="-1.79635" y1="1.1172" x2="-1.46305" y2="1.12955" layer="21"/>
<rectangle x1="-1.03095" y1="1.1172" x2="-0.69755" y2="1.12955" layer="21"/>
<rectangle x1="-0.19145" y1="1.1172" x2="0.16665" y2="1.12955" layer="21"/>
<rectangle x1="0.67275" y1="1.1172" x2="1.00615" y2="1.12955" layer="21"/>
<rectangle x1="1.42585" y1="1.1172" x2="1.75925" y2="1.12955" layer="21"/>
<rectangle x1="-1.79635" y1="1.12955" x2="-1.46305" y2="1.1418875" layer="21"/>
<rectangle x1="-1.03095" y1="1.12955" x2="-0.69755" y2="1.1418875" layer="21"/>
<rectangle x1="-0.19145" y1="1.12955" x2="0.16665" y2="1.1418875" layer="21"/>
<rectangle x1="0.67275" y1="1.12955" x2="1.00615" y2="1.1418875" layer="21"/>
<rectangle x1="1.42585" y1="1.12955" x2="1.75925" y2="1.1418875" layer="21"/>
<rectangle x1="-1.79635" y1="1.1418875" x2="-1.46305" y2="1.1542375" layer="21"/>
<rectangle x1="-1.03095" y1="1.1418875" x2="-0.69755" y2="1.1542375" layer="21"/>
<rectangle x1="-0.19145" y1="1.1418875" x2="0.16665" y2="1.1542375" layer="21"/>
<rectangle x1="0.67275" y1="1.1418875" x2="1.00615" y2="1.1542375" layer="21"/>
<rectangle x1="1.42585" y1="1.1418875" x2="1.75925" y2="1.1542375" layer="21"/>
<rectangle x1="-1.79635" y1="1.1542375" x2="-1.46305" y2="1.166575" layer="21"/>
<rectangle x1="-1.03095" y1="1.1542375" x2="-0.69755" y2="1.166575" layer="21"/>
<rectangle x1="-0.19145" y1="1.1542375" x2="0.16665" y2="1.166575" layer="21"/>
<rectangle x1="0.67275" y1="1.1542375" x2="1.00615" y2="1.166575" layer="21"/>
<rectangle x1="1.42585" y1="1.1542375" x2="1.75925" y2="1.166575" layer="21"/>
<rectangle x1="-1.79635" y1="1.166578125" x2="-1.46305" y2="1.178928125" layer="21"/>
<rectangle x1="-1.03095" y1="1.166578125" x2="-0.69755" y2="1.178928125" layer="21"/>
<rectangle x1="-0.19145" y1="1.166578125" x2="0.16665" y2="1.178928125" layer="21"/>
<rectangle x1="0.67275" y1="1.166578125" x2="1.00615" y2="1.178928125" layer="21"/>
<rectangle x1="1.42585" y1="1.166578125" x2="1.75925" y2="1.178928125" layer="21"/>
<rectangle x1="-1.79635" y1="1.178928125" x2="-1.46305" y2="1.191265625" layer="21"/>
<rectangle x1="-1.03095" y1="1.178928125" x2="-0.69755" y2="1.191265625" layer="21"/>
<rectangle x1="-0.19145" y1="1.178928125" x2="0.16665" y2="1.191265625" layer="21"/>
<rectangle x1="0.67275" y1="1.178928125" x2="1.00615" y2="1.191265625" layer="21"/>
<rectangle x1="1.42585" y1="1.178928125" x2="1.75925" y2="1.191265625" layer="21"/>
<rectangle x1="-1.79635" y1="1.19126875" x2="-1.46305" y2="1.20361875" layer="21"/>
<rectangle x1="-1.03095" y1="1.19126875" x2="-0.69755" y2="1.20361875" layer="21"/>
<rectangle x1="-0.19145" y1="1.19126875" x2="0.16665" y2="1.20361875" layer="21"/>
<rectangle x1="0.67275" y1="1.19126875" x2="1.00615" y2="1.20361875" layer="21"/>
<rectangle x1="1.42585" y1="1.19126875" x2="1.75925" y2="1.20361875" layer="21"/>
<rectangle x1="-1.79635" y1="1.20361875" x2="-1.46305" y2="1.21596875" layer="21"/>
<rectangle x1="-1.03095" y1="1.20361875" x2="-0.69755" y2="1.21596875" layer="21"/>
<rectangle x1="-0.19145" y1="1.20361875" x2="0.16665" y2="1.21596875" layer="21"/>
<rectangle x1="0.67275" y1="1.20361875" x2="1.00615" y2="1.21596875" layer="21"/>
<rectangle x1="1.42585" y1="1.20361875" x2="1.75925" y2="1.21596875" layer="21"/>
<rectangle x1="-1.79635" y1="1.21596875" x2="-1.46305" y2="1.22830625" layer="21"/>
<rectangle x1="-1.03095" y1="1.21596875" x2="-0.69755" y2="1.22830625" layer="21"/>
<rectangle x1="-0.19145" y1="1.21596875" x2="0.16665" y2="1.22830625" layer="21"/>
<rectangle x1="0.67275" y1="1.21596875" x2="1.00615" y2="1.22830625" layer="21"/>
<rectangle x1="1.42585" y1="1.21596875" x2="1.75925" y2="1.22830625" layer="21"/>
<rectangle x1="-1.79635" y1="1.228309375" x2="-1.46305" y2="1.240659375" layer="21"/>
<rectangle x1="-1.03095" y1="1.228309375" x2="-0.69755" y2="1.240659375" layer="21"/>
<rectangle x1="-0.19145" y1="1.228309375" x2="0.16665" y2="1.240659375" layer="21"/>
<rectangle x1="0.67275" y1="1.228309375" x2="1.00615" y2="1.240659375" layer="21"/>
<rectangle x1="1.42585" y1="1.228309375" x2="1.75925" y2="1.240659375" layer="21"/>
<rectangle x1="-1.79635" y1="1.240659375" x2="-1.46305" y2="1.252996875" layer="21"/>
<rectangle x1="-1.03095" y1="1.240659375" x2="-0.69755" y2="1.252996875" layer="21"/>
<rectangle x1="-0.19145" y1="1.240659375" x2="0.16665" y2="1.252996875" layer="21"/>
<rectangle x1="0.67275" y1="1.240659375" x2="1.00615" y2="1.252996875" layer="21"/>
<rectangle x1="1.42585" y1="1.240659375" x2="1.75925" y2="1.252996875" layer="21"/>
<rectangle x1="-1.79635" y1="1.253" x2="-1.46305" y2="1.26535" layer="21"/>
<rectangle x1="-1.03095" y1="1.253" x2="-0.69755" y2="1.26535" layer="21"/>
<rectangle x1="-0.19145" y1="1.253" x2="0.16665" y2="1.26535" layer="21"/>
<rectangle x1="0.67275" y1="1.253" x2="1.00615" y2="1.26535" layer="21"/>
<rectangle x1="1.42585" y1="1.253" x2="1.75925" y2="1.26535" layer="21"/>
<rectangle x1="-1.79635" y1="1.26535" x2="-1.46305" y2="1.2776875" layer="21"/>
<rectangle x1="-1.03095" y1="1.26535" x2="-0.69755" y2="1.2776875" layer="21"/>
<rectangle x1="-0.19145" y1="1.26535" x2="0.16665" y2="1.2776875" layer="21"/>
<rectangle x1="0.67275" y1="1.26535" x2="1.00615" y2="1.2776875" layer="21"/>
<rectangle x1="1.42585" y1="1.26535" x2="1.75925" y2="1.2776875" layer="21"/>
<rectangle x1="-1.79635" y1="1.2776875" x2="-1.46305" y2="1.2900375" layer="21"/>
<rectangle x1="-1.03095" y1="1.2776875" x2="-0.69755" y2="1.2900375" layer="21"/>
<rectangle x1="-0.19145" y1="1.2776875" x2="0.16665" y2="1.2900375" layer="21"/>
<rectangle x1="0.67275" y1="1.2776875" x2="1.00615" y2="1.2900375" layer="21"/>
<rectangle x1="1.42585" y1="1.2776875" x2="1.75925" y2="1.2900375" layer="21"/>
<rectangle x1="-1.79635" y1="1.2900375" x2="-1.46305" y2="1.3023875" layer="21"/>
<rectangle x1="-1.03095" y1="1.2900375" x2="-0.69755" y2="1.3023875" layer="21"/>
<rectangle x1="-0.19145" y1="1.2900375" x2="0.16665" y2="1.3023875" layer="21"/>
<rectangle x1="0.67275" y1="1.2900375" x2="1.00615" y2="1.3023875" layer="21"/>
<rectangle x1="1.42585" y1="1.2900375" x2="1.75925" y2="1.3023875" layer="21"/>
<rectangle x1="-1.79635" y1="1.3023875" x2="-1.46305" y2="1.314725" layer="21"/>
<rectangle x1="-1.03095" y1="1.3023875" x2="-0.69755" y2="1.314725" layer="21"/>
<rectangle x1="-0.19145" y1="1.3023875" x2="0.16665" y2="1.314725" layer="21"/>
<rectangle x1="0.67275" y1="1.3023875" x2="1.00615" y2="1.314725" layer="21"/>
<rectangle x1="1.42585" y1="1.3023875" x2="1.75925" y2="1.314725" layer="21"/>
<rectangle x1="-1.79635" y1="1.314728125" x2="-1.46305" y2="1.327078125" layer="21"/>
<rectangle x1="-1.03095" y1="1.314728125" x2="-0.69755" y2="1.327078125" layer="21"/>
<rectangle x1="-0.19145" y1="1.314728125" x2="0.16665" y2="1.327078125" layer="21"/>
<rectangle x1="0.67275" y1="1.314728125" x2="1.00615" y2="1.327078125" layer="21"/>
<rectangle x1="1.42585" y1="1.314728125" x2="1.75925" y2="1.327078125" layer="21"/>
<rectangle x1="-1.79635" y1="1.327078125" x2="-1.46305" y2="1.339415625" layer="21"/>
<rectangle x1="-1.03095" y1="1.327078125" x2="-0.69755" y2="1.339415625" layer="21"/>
<rectangle x1="-0.19145" y1="1.327078125" x2="0.16665" y2="1.339415625" layer="21"/>
<rectangle x1="0.67275" y1="1.327078125" x2="1.00615" y2="1.339415625" layer="21"/>
<rectangle x1="1.42585" y1="1.327078125" x2="1.75925" y2="1.339415625" layer="21"/>
<rectangle x1="-1.79635" y1="1.33941875" x2="-1.46305" y2="1.35176875" layer="21"/>
<rectangle x1="-1.03095" y1="1.33941875" x2="-0.69755" y2="1.35176875" layer="21"/>
<rectangle x1="-0.19145" y1="1.33941875" x2="0.16665" y2="1.35176875" layer="21"/>
<rectangle x1="0.67275" y1="1.33941875" x2="1.00615" y2="1.35176875" layer="21"/>
<rectangle x1="1.42585" y1="1.33941875" x2="1.75925" y2="1.35176875" layer="21"/>
<rectangle x1="-1.79635" y1="1.35176875" x2="-1.46305" y2="1.36410625" layer="21"/>
<rectangle x1="-1.03095" y1="1.35176875" x2="-0.69755" y2="1.36410625" layer="21"/>
<rectangle x1="-0.19145" y1="1.35176875" x2="0.16665" y2="1.36410625" layer="21"/>
<rectangle x1="0.67275" y1="1.35176875" x2="1.00615" y2="1.36410625" layer="21"/>
<rectangle x1="1.42585" y1="1.35176875" x2="1.75925" y2="1.36410625" layer="21"/>
<rectangle x1="-1.79635" y1="1.364109375" x2="-1.46305" y2="1.376459375" layer="21"/>
<rectangle x1="-1.03095" y1="1.364109375" x2="-0.69755" y2="1.376459375" layer="21"/>
<rectangle x1="-0.19145" y1="1.364109375" x2="0.16665" y2="1.376459375" layer="21"/>
<rectangle x1="0.67275" y1="1.364109375" x2="1.00615" y2="1.376459375" layer="21"/>
<rectangle x1="1.42585" y1="1.364109375" x2="1.75925" y2="1.376459375" layer="21"/>
<rectangle x1="-1.79635" y1="1.376459375" x2="-1.46305" y2="1.388809375" layer="21"/>
<rectangle x1="-1.03095" y1="1.376459375" x2="-0.69755" y2="1.388809375" layer="21"/>
<rectangle x1="-0.19145" y1="1.376459375" x2="0.16665" y2="1.388809375" layer="21"/>
<rectangle x1="0.67275" y1="1.376459375" x2="1.00615" y2="1.388809375" layer="21"/>
<rectangle x1="1.42585" y1="1.376459375" x2="1.75925" y2="1.388809375" layer="21"/>
<rectangle x1="-1.79635" y1="1.388809375" x2="-1.46305" y2="1.401146875" layer="21"/>
<rectangle x1="-1.03095" y1="1.388809375" x2="-0.69755" y2="1.401146875" layer="21"/>
<rectangle x1="-0.19145" y1="1.388809375" x2="0.16665" y2="1.401146875" layer="21"/>
<rectangle x1="0.67275" y1="1.388809375" x2="1.00615" y2="1.401146875" layer="21"/>
<rectangle x1="1.42585" y1="1.388809375" x2="1.75925" y2="1.401146875" layer="21"/>
<rectangle x1="-1.79635" y1="1.40115" x2="-1.46305" y2="1.4135" layer="21"/>
<rectangle x1="-1.03095" y1="1.40115" x2="-0.69755" y2="1.4135" layer="21"/>
<rectangle x1="-0.19145" y1="1.40115" x2="0.16665" y2="1.4135" layer="21"/>
<rectangle x1="0.67275" y1="1.40115" x2="1.00615" y2="1.4135" layer="21"/>
<rectangle x1="1.42585" y1="1.40115" x2="1.75925" y2="1.4135" layer="21"/>
<rectangle x1="-1.79635" y1="1.4135" x2="-1.46305" y2="1.4258375" layer="21"/>
<rectangle x1="-1.03095" y1="1.4135" x2="-0.69755" y2="1.4258375" layer="21"/>
<rectangle x1="-0.19145" y1="1.4135" x2="0.16665" y2="1.4258375" layer="21"/>
<rectangle x1="0.67275" y1="1.4135" x2="1.00615" y2="1.4258375" layer="21"/>
<rectangle x1="1.42585" y1="1.4135" x2="1.75925" y2="1.4258375" layer="21"/>
<rectangle x1="-1.79635" y1="1.4258375" x2="-1.46305" y2="1.4381875" layer="21"/>
<rectangle x1="-1.03095" y1="1.4258375" x2="-0.69755" y2="1.4381875" layer="21"/>
<rectangle x1="-0.19145" y1="1.4258375" x2="0.16665" y2="1.4381875" layer="21"/>
<rectangle x1="0.67275" y1="1.4258375" x2="1.00615" y2="1.4381875" layer="21"/>
<rectangle x1="1.42585" y1="1.4258375" x2="1.75925" y2="1.4381875" layer="21"/>
<rectangle x1="-1.79635" y1="1.4381875" x2="-1.46305" y2="1.450525" layer="21"/>
<rectangle x1="-1.03095" y1="1.4381875" x2="-0.69755" y2="1.450525" layer="21"/>
<rectangle x1="-0.19145" y1="1.4381875" x2="0.16665" y2="1.450525" layer="21"/>
<rectangle x1="0.67275" y1="1.4381875" x2="1.00615" y2="1.450525" layer="21"/>
<rectangle x1="1.42585" y1="1.4381875" x2="1.75925" y2="1.450525" layer="21"/>
<rectangle x1="-1.79635" y1="1.450528125" x2="-1.46305" y2="1.462878125" layer="21"/>
<rectangle x1="-1.03095" y1="1.450528125" x2="-0.69755" y2="1.462878125" layer="21"/>
<rectangle x1="-0.19145" y1="1.450528125" x2="0.16665" y2="1.462878125" layer="21"/>
<rectangle x1="0.67275" y1="1.450528125" x2="1.00615" y2="1.462878125" layer="21"/>
<rectangle x1="1.42585" y1="1.450528125" x2="1.75925" y2="1.462878125" layer="21"/>
<rectangle x1="-1.79635" y1="1.462878125" x2="-1.46305" y2="1.475215625" layer="21"/>
<rectangle x1="-1.03095" y1="1.462878125" x2="-0.69755" y2="1.475215625" layer="21"/>
<rectangle x1="-0.19145" y1="1.462878125" x2="0.16665" y2="1.475215625" layer="21"/>
<rectangle x1="0.67275" y1="1.462878125" x2="1.00615" y2="1.475215625" layer="21"/>
<rectangle x1="1.42585" y1="1.462878125" x2="1.75925" y2="1.475215625" layer="21"/>
<rectangle x1="-1.79635" y1="1.47521875" x2="-1.46305" y2="1.48756875" layer="21"/>
<rectangle x1="-1.03095" y1="1.47521875" x2="-0.69755" y2="1.48756875" layer="21"/>
<rectangle x1="-0.19145" y1="1.47521875" x2="0.16665" y2="1.48756875" layer="21"/>
<rectangle x1="0.67275" y1="1.47521875" x2="1.00615" y2="1.48756875" layer="21"/>
<rectangle x1="1.42585" y1="1.47521875" x2="1.75925" y2="1.48756875" layer="21"/>
<rectangle x1="-1.79635" y1="1.48756875" x2="-1.46305" y2="1.49991875" layer="21"/>
<rectangle x1="-1.03095" y1="1.48756875" x2="-0.69755" y2="1.49991875" layer="21"/>
<rectangle x1="-0.19145" y1="1.48756875" x2="0.16665" y2="1.49991875" layer="21"/>
<rectangle x1="0.67275" y1="1.48756875" x2="1.00615" y2="1.49991875" layer="21"/>
<rectangle x1="1.42585" y1="1.48756875" x2="1.75925" y2="1.49991875" layer="21"/>
<rectangle x1="-1.79635" y1="1.49991875" x2="-1.46305" y2="1.51225625" layer="21"/>
<rectangle x1="-1.03095" y1="1.49991875" x2="-0.69755" y2="1.51225625" layer="21"/>
<rectangle x1="-0.19145" y1="1.49991875" x2="0.16665" y2="1.51225625" layer="21"/>
<rectangle x1="0.67275" y1="1.49991875" x2="1.00615" y2="1.51225625" layer="21"/>
<rectangle x1="1.42585" y1="1.49991875" x2="1.75925" y2="1.51225625" layer="21"/>
<rectangle x1="-1.79635" y1="1.512259375" x2="-1.46305" y2="1.524609375" layer="21"/>
<rectangle x1="-1.03095" y1="1.512259375" x2="-0.69755" y2="1.524609375" layer="21"/>
<rectangle x1="-0.19145" y1="1.512259375" x2="0.16665" y2="1.524609375" layer="21"/>
<rectangle x1="0.67275" y1="1.512259375" x2="1.00615" y2="1.524609375" layer="21"/>
<rectangle x1="1.42585" y1="1.512259375" x2="1.75925" y2="1.524609375" layer="21"/>
<rectangle x1="-1.79635" y1="1.524609375" x2="-1.46305" y2="1.536946875" layer="21"/>
<rectangle x1="-1.03095" y1="1.524609375" x2="-0.69755" y2="1.536946875" layer="21"/>
<rectangle x1="-0.19145" y1="1.524609375" x2="0.16665" y2="1.536946875" layer="21"/>
<rectangle x1="0.67275" y1="1.524609375" x2="1.00615" y2="1.536946875" layer="21"/>
<rectangle x1="1.42585" y1="1.524609375" x2="1.75925" y2="1.536946875" layer="21"/>
<rectangle x1="-1.79635" y1="1.53695" x2="-1.46305" y2="1.5493" layer="21"/>
<rectangle x1="-1.03095" y1="1.53695" x2="-0.69755" y2="1.5493" layer="21"/>
<rectangle x1="-0.19145" y1="1.53695" x2="0.16665" y2="1.5493" layer="21"/>
<rectangle x1="0.67275" y1="1.53695" x2="1.00615" y2="1.5493" layer="21"/>
<rectangle x1="1.42585" y1="1.53695" x2="1.75925" y2="1.5493" layer="21"/>
<rectangle x1="-1.79635" y1="1.5493" x2="-1.46305" y2="1.5616375" layer="21"/>
<rectangle x1="-1.03095" y1="1.5493" x2="-0.69755" y2="1.5616375" layer="21"/>
<rectangle x1="-0.19145" y1="1.5493" x2="0.16665" y2="1.5616375" layer="21"/>
<rectangle x1="0.67275" y1="1.5493" x2="1.00615" y2="1.5616375" layer="21"/>
<rectangle x1="1.42585" y1="1.5493" x2="1.75925" y2="1.5616375" layer="21"/>
<rectangle x1="-1.79635" y1="1.5616375" x2="-1.46305" y2="1.5739875" layer="21"/>
<rectangle x1="-1.03095" y1="1.5616375" x2="-0.69755" y2="1.5739875" layer="21"/>
<rectangle x1="-0.19145" y1="1.5616375" x2="0.16665" y2="1.5739875" layer="21"/>
<rectangle x1="0.67275" y1="1.5616375" x2="1.00615" y2="1.5739875" layer="21"/>
<rectangle x1="1.42585" y1="1.5616375" x2="1.75925" y2="1.5739875" layer="21"/>
<rectangle x1="-1.79635" y1="1.5739875" x2="-1.46305" y2="1.5863375" layer="21"/>
<rectangle x1="-1.03095" y1="1.5739875" x2="-0.69755" y2="1.5863375" layer="21"/>
<rectangle x1="-0.19145" y1="1.5739875" x2="0.16665" y2="1.5863375" layer="21"/>
<rectangle x1="0.67275" y1="1.5739875" x2="1.00615" y2="1.5863375" layer="21"/>
<rectangle x1="1.42585" y1="1.5739875" x2="1.75925" y2="1.5863375" layer="21"/>
<rectangle x1="-1.79635" y1="1.5863375" x2="-1.46305" y2="1.598675" layer="21"/>
<rectangle x1="-1.03095" y1="1.5863375" x2="-0.69755" y2="1.598675" layer="21"/>
<rectangle x1="-0.19145" y1="1.5863375" x2="0.16665" y2="1.598675" layer="21"/>
<rectangle x1="0.67275" y1="1.5863375" x2="1.00615" y2="1.598675" layer="21"/>
<rectangle x1="1.42585" y1="1.5863375" x2="1.75925" y2="1.598675" layer="21"/>
<rectangle x1="-1.79635" y1="1.598678125" x2="-1.46305" y2="1.611028125" layer="21"/>
<rectangle x1="-1.03095" y1="1.598678125" x2="-0.69755" y2="1.611028125" layer="21"/>
<rectangle x1="-0.19145" y1="1.598678125" x2="0.16665" y2="1.611028125" layer="21"/>
<rectangle x1="0.67275" y1="1.598678125" x2="1.00615" y2="1.611028125" layer="21"/>
<rectangle x1="1.42585" y1="1.598678125" x2="1.75925" y2="1.611028125" layer="21"/>
<rectangle x1="-1.79635" y1="1.611028125" x2="-1.46305" y2="1.623365625" layer="21"/>
<rectangle x1="-1.03095" y1="1.611028125" x2="-0.69755" y2="1.623365625" layer="21"/>
<rectangle x1="-0.19145" y1="1.611028125" x2="0.16665" y2="1.623365625" layer="21"/>
<rectangle x1="0.67275" y1="1.611028125" x2="1.00615" y2="1.623365625" layer="21"/>
<rectangle x1="1.42585" y1="1.611028125" x2="1.75925" y2="1.623365625" layer="21"/>
<rectangle x1="-1.79635" y1="1.62336875" x2="-1.46305" y2="1.63571875" layer="21"/>
<rectangle x1="-1.03095" y1="1.62336875" x2="-0.69755" y2="1.63571875" layer="21"/>
<rectangle x1="-0.19145" y1="1.62336875" x2="0.16665" y2="1.63571875" layer="21"/>
<rectangle x1="0.67275" y1="1.62336875" x2="1.00615" y2="1.63571875" layer="21"/>
<rectangle x1="1.42585" y1="1.62336875" x2="1.75925" y2="1.63571875" layer="21"/>
<rectangle x1="-1.79635" y1="1.63571875" x2="-1.46305" y2="1.64805625" layer="21"/>
<rectangle x1="-1.03095" y1="1.63571875" x2="-0.69755" y2="1.64805625" layer="21"/>
<rectangle x1="-0.19145" y1="1.63571875" x2="0.16665" y2="1.64805625" layer="21"/>
<rectangle x1="0.67275" y1="1.63571875" x2="1.00615" y2="1.64805625" layer="21"/>
<rectangle x1="1.42585" y1="1.63571875" x2="1.75925" y2="1.64805625" layer="21"/>
<rectangle x1="-1.79635" y1="1.648059375" x2="-1.46305" y2="1.660409375" layer="21"/>
<rectangle x1="-1.03095" y1="1.648059375" x2="-0.69755" y2="1.660409375" layer="21"/>
<rectangle x1="-0.19145" y1="1.648059375" x2="0.16665" y2="1.660409375" layer="21"/>
<rectangle x1="0.67275" y1="1.648059375" x2="1.00615" y2="1.660409375" layer="21"/>
<rectangle x1="1.42585" y1="1.648059375" x2="1.75925" y2="1.660409375" layer="21"/>
<rectangle x1="-1.79635" y1="1.660409375" x2="-1.46305" y2="1.672759375" layer="21"/>
<rectangle x1="-1.03095" y1="1.660409375" x2="-0.69755" y2="1.672759375" layer="21"/>
<rectangle x1="-0.19145" y1="1.660409375" x2="0.16665" y2="1.672759375" layer="21"/>
<rectangle x1="0.67275" y1="1.660409375" x2="1.00615" y2="1.672759375" layer="21"/>
<rectangle x1="1.42585" y1="1.660409375" x2="1.75925" y2="1.672759375" layer="21"/>
<rectangle x1="-1.79635" y1="1.672759375" x2="-1.46305" y2="1.685096875" layer="21"/>
<rectangle x1="-1.03095" y1="1.672759375" x2="-0.69755" y2="1.685096875" layer="21"/>
<rectangle x1="-0.19145" y1="1.672759375" x2="0.16665" y2="1.685096875" layer="21"/>
<rectangle x1="0.67275" y1="1.672759375" x2="1.00615" y2="1.685096875" layer="21"/>
<rectangle x1="1.42585" y1="1.672759375" x2="1.75925" y2="1.685096875" layer="21"/>
<rectangle x1="-1.79635" y1="1.6851" x2="-1.46305" y2="1.69745" layer="21"/>
<rectangle x1="-1.03095" y1="1.6851" x2="-0.69755" y2="1.69745" layer="21"/>
<rectangle x1="-0.19145" y1="1.6851" x2="0.16665" y2="1.69745" layer="21"/>
<rectangle x1="0.67275" y1="1.6851" x2="1.00615" y2="1.69745" layer="21"/>
<rectangle x1="1.42585" y1="1.6851" x2="1.75925" y2="1.69745" layer="21"/>
<rectangle x1="-1.79635" y1="1.69745" x2="-1.46305" y2="1.7097875" layer="21"/>
<rectangle x1="-1.03095" y1="1.69745" x2="-0.69755" y2="1.7097875" layer="21"/>
<rectangle x1="-0.19145" y1="1.69745" x2="0.16665" y2="1.7097875" layer="21"/>
<rectangle x1="0.67275" y1="1.69745" x2="1.00615" y2="1.7097875" layer="21"/>
<rectangle x1="1.42585" y1="1.69745" x2="1.75925" y2="1.7097875" layer="21"/>
<rectangle x1="-1.79635" y1="1.7097875" x2="-1.46305" y2="1.7221375" layer="21"/>
<rectangle x1="-1.03095" y1="1.7097875" x2="-0.69755" y2="1.7221375" layer="21"/>
<rectangle x1="-0.19145" y1="1.7097875" x2="0.16665" y2="1.7221375" layer="21"/>
<rectangle x1="0.67275" y1="1.7097875" x2="1.00615" y2="1.7221375" layer="21"/>
<rectangle x1="1.42585" y1="1.7097875" x2="1.75925" y2="1.7221375" layer="21"/>
<rectangle x1="-1.79635" y1="1.7221375" x2="-1.46305" y2="1.734475" layer="21"/>
<rectangle x1="-1.03095" y1="1.7221375" x2="-0.69755" y2="1.734475" layer="21"/>
<rectangle x1="-0.19145" y1="1.7221375" x2="0.16665" y2="1.734475" layer="21"/>
<rectangle x1="0.67275" y1="1.7221375" x2="1.00615" y2="1.734475" layer="21"/>
<rectangle x1="1.42585" y1="1.7221375" x2="1.75925" y2="1.734475" layer="21"/>
<rectangle x1="-1.79635" y1="1.734478125" x2="-1.46305" y2="1.746828125" layer="21"/>
<rectangle x1="-1.03095" y1="1.734478125" x2="-0.69755" y2="1.746828125" layer="21"/>
<rectangle x1="-0.19145" y1="1.734478125" x2="0.16665" y2="1.746828125" layer="21"/>
<rectangle x1="0.67275" y1="1.734478125" x2="1.00615" y2="1.746828125" layer="21"/>
<rectangle x1="1.42585" y1="1.734478125" x2="1.75925" y2="1.746828125" layer="21"/>
<rectangle x1="-1.79635" y1="1.746828125" x2="-1.46305" y2="1.759178125" layer="21"/>
<rectangle x1="-1.03095" y1="1.746828125" x2="-0.69755" y2="1.759178125" layer="21"/>
<rectangle x1="-0.19145" y1="1.746828125" x2="0.16665" y2="1.759178125" layer="21"/>
<rectangle x1="0.67275" y1="1.746828125" x2="1.00615" y2="1.759178125" layer="21"/>
<rectangle x1="1.42585" y1="1.746828125" x2="1.75925" y2="1.759178125" layer="21"/>
<rectangle x1="-1.79635" y1="1.759178125" x2="-1.46305" y2="1.771515625" layer="21"/>
<rectangle x1="-1.03095" y1="1.759178125" x2="-0.69755" y2="1.771515625" layer="21"/>
<rectangle x1="-0.19145" y1="1.759178125" x2="0.16665" y2="1.771515625" layer="21"/>
<rectangle x1="0.67275" y1="1.759178125" x2="1.00615" y2="1.771515625" layer="21"/>
<rectangle x1="1.42585" y1="1.759178125" x2="1.75925" y2="1.771515625" layer="21"/>
<rectangle x1="-1.79635" y1="1.77151875" x2="-1.46305" y2="1.78386875" layer="21"/>
<rectangle x1="-1.03095" y1="1.77151875" x2="-0.69755" y2="1.78386875" layer="21"/>
<rectangle x1="-0.19145" y1="1.77151875" x2="0.16665" y2="1.78386875" layer="21"/>
<rectangle x1="0.67275" y1="1.77151875" x2="1.00615" y2="1.78386875" layer="21"/>
<rectangle x1="1.42585" y1="1.77151875" x2="1.75925" y2="1.78386875" layer="21"/>
<rectangle x1="-1.79635" y1="1.78386875" x2="-1.46305" y2="1.79620625" layer="21"/>
<rectangle x1="-1.03095" y1="1.78386875" x2="-0.69755" y2="1.79620625" layer="21"/>
<rectangle x1="-0.19145" y1="1.78386875" x2="0.16665" y2="1.79620625" layer="21"/>
<rectangle x1="0.67275" y1="1.78386875" x2="1.00615" y2="1.79620625" layer="21"/>
<rectangle x1="1.42585" y1="1.78386875" x2="1.75925" y2="1.79620625" layer="21"/>
<rectangle x1="-1.79635" y1="1.796209375" x2="-1.46305" y2="1.808559375" layer="21"/>
<rectangle x1="-1.03095" y1="1.796209375" x2="-0.69755" y2="1.808559375" layer="21"/>
<rectangle x1="-0.19145" y1="1.796209375" x2="0.16665" y2="1.808559375" layer="21"/>
<rectangle x1="0.67275" y1="1.796209375" x2="1.00615" y2="1.808559375" layer="21"/>
<rectangle x1="1.42585" y1="1.796209375" x2="1.75925" y2="1.808559375" layer="21"/>
<rectangle x1="-1.79635" y1="1.808559375" x2="-1.46305" y2="1.820896875" layer="21"/>
<rectangle x1="-1.03095" y1="1.808559375" x2="-0.69755" y2="1.820896875" layer="21"/>
<rectangle x1="-0.19145" y1="1.808559375" x2="0.16665" y2="1.820896875" layer="21"/>
<rectangle x1="0.67275" y1="1.808559375" x2="1.00615" y2="1.820896875" layer="21"/>
<rectangle x1="1.42585" y1="1.808559375" x2="1.75925" y2="1.820896875" layer="21"/>
<rectangle x1="-1.79635" y1="1.8209" x2="-1.46305" y2="1.83325" layer="21"/>
<rectangle x1="-1.03095" y1="1.8209" x2="-0.69755" y2="1.83325" layer="21"/>
<rectangle x1="-0.19145" y1="1.8209" x2="0.16665" y2="1.83325" layer="21"/>
<rectangle x1="0.67275" y1="1.8209" x2="1.00615" y2="1.83325" layer="21"/>
<rectangle x1="1.42585" y1="1.8209" x2="1.75925" y2="1.83325" layer="21"/>
<rectangle x1="-1.79635" y1="1.83325" x2="-1.46305" y2="1.8456" layer="21"/>
<rectangle x1="-1.03095" y1="1.83325" x2="-0.69755" y2="1.8456" layer="21"/>
<rectangle x1="-0.19145" y1="1.83325" x2="0.16665" y2="1.8456" layer="21"/>
<rectangle x1="0.67275" y1="1.83325" x2="1.00615" y2="1.8456" layer="21"/>
<rectangle x1="1.42585" y1="1.83325" x2="1.75925" y2="1.8456" layer="21"/>
<rectangle x1="-1.79635" y1="1.8456" x2="-1.46305" y2="1.8579375" layer="21"/>
<rectangle x1="-1.03095" y1="1.8456" x2="-0.69755" y2="1.8579375" layer="21"/>
<rectangle x1="-0.19145" y1="1.8456" x2="0.16665" y2="1.8579375" layer="21"/>
<rectangle x1="0.67275" y1="1.8456" x2="1.00615" y2="1.8579375" layer="21"/>
<rectangle x1="1.42585" y1="1.8456" x2="1.75925" y2="1.8579375" layer="21"/>
<rectangle x1="-1.79635" y1="1.8579375" x2="-1.46305" y2="1.8702875" layer="21"/>
<rectangle x1="-1.03095" y1="1.8579375" x2="-0.69755" y2="1.8702875" layer="21"/>
<rectangle x1="-0.19145" y1="1.8579375" x2="0.16665" y2="1.8702875" layer="21"/>
<rectangle x1="0.67275" y1="1.8579375" x2="1.00615" y2="1.8702875" layer="21"/>
<rectangle x1="1.42585" y1="1.8579375" x2="1.75925" y2="1.8702875" layer="21"/>
<rectangle x1="-1.79635" y1="1.8702875" x2="-1.46305" y2="1.882625" layer="21"/>
<rectangle x1="-1.03095" y1="1.8702875" x2="-0.69755" y2="1.882625" layer="21"/>
<rectangle x1="-0.19145" y1="1.8702875" x2="0.16665" y2="1.882625" layer="21"/>
<rectangle x1="0.67275" y1="1.8702875" x2="1.00615" y2="1.882625" layer="21"/>
<rectangle x1="1.42585" y1="1.8702875" x2="1.75925" y2="1.882625" layer="21"/>
<rectangle x1="-1.79635" y1="1.882628125" x2="-1.46305" y2="1.894978125" layer="21"/>
<rectangle x1="-1.04325" y1="1.882628125" x2="-0.69755" y2="1.894978125" layer="21"/>
<rectangle x1="-0.19145" y1="1.882628125" x2="0.16665" y2="1.894978125" layer="21"/>
<rectangle x1="0.67275" y1="1.882628125" x2="1.00615" y2="1.894978125" layer="21"/>
<rectangle x1="1.42585" y1="1.882628125" x2="1.75925" y2="1.894978125" layer="21"/>
<rectangle x1="-1.79635" y1="1.894978125" x2="-1.46305" y2="1.907315625" layer="21"/>
<rectangle x1="-1.04325" y1="1.894978125" x2="-0.69755" y2="1.907315625" layer="21"/>
<rectangle x1="-0.19145" y1="1.894978125" x2="0.16665" y2="1.907315625" layer="21"/>
<rectangle x1="0.67275" y1="1.894978125" x2="1.00615" y2="1.907315625" layer="21"/>
<rectangle x1="1.42585" y1="1.894978125" x2="1.75925" y2="1.907315625" layer="21"/>
<rectangle x1="-1.79635" y1="1.90731875" x2="-1.46305" y2="1.91966875" layer="21"/>
<rectangle x1="-1.04325" y1="1.90731875" x2="-0.69755" y2="1.91966875" layer="21"/>
<rectangle x1="-0.19145" y1="1.90731875" x2="0.16665" y2="1.91966875" layer="21"/>
<rectangle x1="0.67275" y1="1.90731875" x2="1.00615" y2="1.91966875" layer="21"/>
<rectangle x1="1.42585" y1="1.90731875" x2="1.75925" y2="1.91966875" layer="21"/>
<rectangle x1="-1.79635" y1="1.91966875" x2="-1.46305" y2="1.93201875" layer="21"/>
<rectangle x1="-1.04325" y1="1.91966875" x2="-0.69755" y2="1.93201875" layer="21"/>
<rectangle x1="-0.19145" y1="1.91966875" x2="0.16665" y2="1.93201875" layer="21"/>
<rectangle x1="0.67275" y1="1.91966875" x2="1.00615" y2="1.93201875" layer="21"/>
<rectangle x1="1.42585" y1="1.91966875" x2="1.75925" y2="1.93201875" layer="21"/>
<rectangle x1="-1.79635" y1="1.93201875" x2="-1.46305" y2="1.94435625" layer="21"/>
<rectangle x1="-1.04325" y1="1.93201875" x2="-0.69755" y2="1.94435625" layer="21"/>
<rectangle x1="-0.19145" y1="1.93201875" x2="0.16665" y2="1.94435625" layer="21"/>
<rectangle x1="0.67275" y1="1.93201875" x2="1.00615" y2="1.94435625" layer="21"/>
<rectangle x1="1.42585" y1="1.93201875" x2="1.75925" y2="1.94435625" layer="21"/>
<rectangle x1="-1.79635" y1="1.944359375" x2="-1.46305" y2="1.956709375" layer="21"/>
<rectangle x1="-1.04325" y1="1.944359375" x2="-0.69755" y2="1.956709375" layer="21"/>
<rectangle x1="-0.19145" y1="1.944359375" x2="0.16665" y2="1.956709375" layer="21"/>
<rectangle x1="0.67275" y1="1.944359375" x2="1.00615" y2="1.956709375" layer="21"/>
<rectangle x1="1.42585" y1="1.944359375" x2="1.75925" y2="1.956709375" layer="21"/>
<rectangle x1="-1.79635" y1="1.956709375" x2="-1.46305" y2="1.969046875" layer="21"/>
<rectangle x1="-1.04325" y1="1.956709375" x2="-0.69755" y2="1.969046875" layer="21"/>
<rectangle x1="-0.19145" y1="1.956709375" x2="0.16665" y2="1.969046875" layer="21"/>
<rectangle x1="0.67275" y1="1.956709375" x2="1.00615" y2="1.969046875" layer="21"/>
<rectangle x1="1.42585" y1="1.956709375" x2="1.75925" y2="1.969046875" layer="21"/>
<rectangle x1="-1.79635" y1="1.96905" x2="-1.46305" y2="1.9814" layer="21"/>
<rectangle x1="-1.04325" y1="1.96905" x2="-0.69755" y2="1.9814" layer="21"/>
<rectangle x1="-0.19145" y1="1.96905" x2="0.16665" y2="1.9814" layer="21"/>
<rectangle x1="0.67275" y1="1.96905" x2="1.00615" y2="1.9814" layer="21"/>
<rectangle x1="1.42585" y1="1.96905" x2="1.75925" y2="1.9814" layer="21"/>
<rectangle x1="-1.79635" y1="1.9814" x2="-1.46305" y2="1.9937375" layer="21"/>
<rectangle x1="-1.04325" y1="1.9814" x2="-0.69755" y2="1.9937375" layer="21"/>
<rectangle x1="-0.19145" y1="1.9814" x2="0.16665" y2="1.9937375" layer="21"/>
<rectangle x1="0.67275" y1="1.9814" x2="1.00615" y2="1.9937375" layer="21"/>
<rectangle x1="1.42585" y1="1.9814" x2="1.75925" y2="1.9937375" layer="21"/>
<rectangle x1="-1.79635" y1="1.9937375" x2="-1.46305" y2="2.0060875" layer="21"/>
<rectangle x1="-1.04325" y1="1.9937375" x2="-0.69755" y2="2.0060875" layer="21"/>
<rectangle x1="-0.19145" y1="1.9937375" x2="0.16665" y2="2.0060875" layer="21"/>
<rectangle x1="0.67275" y1="1.9937375" x2="1.00615" y2="2.0060875" layer="21"/>
<rectangle x1="1.42585" y1="1.9937375" x2="1.75925" y2="2.0060875" layer="21"/>
<rectangle x1="-1.79635" y1="2.0060875" x2="-1.46305" y2="2.018425" layer="21"/>
<rectangle x1="-1.04325" y1="2.0060875" x2="-0.69755" y2="2.018425" layer="21"/>
<rectangle x1="-0.19145" y1="2.0060875" x2="0.16665" y2="2.018425" layer="21"/>
<rectangle x1="0.67275" y1="2.0060875" x2="1.00615" y2="2.018425" layer="21"/>
<rectangle x1="1.42585" y1="2.0060875" x2="1.75925" y2="2.018425" layer="21"/>
<rectangle x1="-1.79635" y1="2.018428125" x2="-1.46305" y2="2.030778125" layer="21"/>
<rectangle x1="-1.04325" y1="2.018428125" x2="-0.69755" y2="2.030778125" layer="21"/>
<rectangle x1="-0.19145" y1="2.018428125" x2="0.16665" y2="2.030778125" layer="21"/>
<rectangle x1="0.67275" y1="2.018428125" x2="1.00615" y2="2.030778125" layer="21"/>
<rectangle x1="1.42585" y1="2.018428125" x2="1.75925" y2="2.030778125" layer="21"/>
<rectangle x1="-1.79635" y1="2.030778125" x2="-1.46305" y2="2.043128125" layer="21"/>
<rectangle x1="-1.04325" y1="2.030778125" x2="-0.69755" y2="2.043128125" layer="21"/>
<rectangle x1="-0.19145" y1="2.030778125" x2="0.16665" y2="2.043128125" layer="21"/>
<rectangle x1="0.67275" y1="2.030778125" x2="1.00615" y2="2.043128125" layer="21"/>
<rectangle x1="1.42585" y1="2.030778125" x2="1.75925" y2="2.043128125" layer="21"/>
<rectangle x1="-1.79635" y1="2.043128125" x2="-1.46305" y2="2.055465625" layer="21"/>
<rectangle x1="-1.04325" y1="2.043128125" x2="-0.69755" y2="2.055465625" layer="21"/>
<rectangle x1="-0.19145" y1="2.043128125" x2="0.16665" y2="2.055465625" layer="21"/>
<rectangle x1="0.67275" y1="2.043128125" x2="1.00615" y2="2.055465625" layer="21"/>
<rectangle x1="1.42585" y1="2.043128125" x2="1.75925" y2="2.055465625" layer="21"/>
<rectangle x1="-1.79635" y1="2.05546875" x2="-1.46305" y2="2.06781875" layer="21"/>
<rectangle x1="-1.04325" y1="2.05546875" x2="-0.70995" y2="2.06781875" layer="21"/>
<rectangle x1="-0.19145" y1="2.05546875" x2="0.16665" y2="2.06781875" layer="21"/>
<rectangle x1="0.67275" y1="2.05546875" x2="1.00615" y2="2.06781875" layer="21"/>
<rectangle x1="1.42585" y1="2.05546875" x2="1.75925" y2="2.06781875" layer="21"/>
<rectangle x1="-1.79635" y1="2.06781875" x2="-1.46305" y2="2.08015625" layer="21"/>
<rectangle x1="-1.05565" y1="2.06781875" x2="-0.70995" y2="2.08015625" layer="21"/>
<rectangle x1="-0.19145" y1="2.06781875" x2="0.16665" y2="2.08015625" layer="21"/>
<rectangle x1="0.67275" y1="2.06781875" x2="1.00615" y2="2.08015625" layer="21"/>
<rectangle x1="1.42585" y1="2.06781875" x2="1.75925" y2="2.08015625" layer="21"/>
<rectangle x1="-1.79635" y1="2.080159375" x2="-1.46305" y2="2.092509375" layer="21"/>
<rectangle x1="-1.05565" y1="2.080159375" x2="-0.70995" y2="2.092509375" layer="21"/>
<rectangle x1="-0.19145" y1="2.080159375" x2="0.16665" y2="2.092509375" layer="21"/>
<rectangle x1="0.67275" y1="2.080159375" x2="1.00615" y2="2.092509375" layer="21"/>
<rectangle x1="1.42585" y1="2.080159375" x2="1.75925" y2="2.092509375" layer="21"/>
<rectangle x1="-1.79635" y1="2.092509375" x2="-1.46305" y2="2.104846875" layer="21"/>
<rectangle x1="-1.05565" y1="2.092509375" x2="-0.70995" y2="2.104846875" layer="21"/>
<rectangle x1="-0.19145" y1="2.092509375" x2="0.16665" y2="2.104846875" layer="21"/>
<rectangle x1="0.67275" y1="2.092509375" x2="1.00615" y2="2.104846875" layer="21"/>
<rectangle x1="1.42585" y1="2.092509375" x2="1.75925" y2="2.104846875" layer="21"/>
<rectangle x1="-1.79635" y1="2.10485" x2="-1.46305" y2="2.1172" layer="21"/>
<rectangle x1="-1.05565" y1="2.10485" x2="-0.70995" y2="2.1172" layer="21"/>
<rectangle x1="-0.19145" y1="2.10485" x2="0.16665" y2="2.1172" layer="21"/>
<rectangle x1="0.67275" y1="2.10485" x2="1.00615" y2="2.1172" layer="21"/>
<rectangle x1="1.42585" y1="2.10485" x2="1.75925" y2="2.1172" layer="21"/>
<rectangle x1="-1.79635" y1="2.1172" x2="-1.46305" y2="2.12955" layer="21"/>
<rectangle x1="-1.05565" y1="2.1172" x2="-0.70995" y2="2.12955" layer="21"/>
<rectangle x1="-0.19145" y1="2.1172" x2="0.16665" y2="2.12955" layer="21"/>
<rectangle x1="0.67275" y1="2.1172" x2="1.00615" y2="2.12955" layer="21"/>
<rectangle x1="1.42585" y1="2.1172" x2="1.75925" y2="2.12955" layer="21"/>
<rectangle x1="-1.79635" y1="2.12955" x2="-1.46305" y2="2.1418875" layer="21"/>
<rectangle x1="-1.06795" y1="2.12955" x2="-0.70995" y2="2.1418875" layer="21"/>
<rectangle x1="-0.19145" y1="2.12955" x2="0.16665" y2="2.1418875" layer="21"/>
<rectangle x1="0.67275" y1="2.12955" x2="1.00615" y2="2.1418875" layer="21"/>
<rectangle x1="1.42585" y1="2.12955" x2="1.75925" y2="2.1418875" layer="21"/>
<rectangle x1="-1.79635" y1="2.1418875" x2="-1.46305" y2="2.1542375" layer="21"/>
<rectangle x1="-1.06795" y1="2.1418875" x2="-0.70995" y2="2.1542375" layer="21"/>
<rectangle x1="-0.19145" y1="2.1418875" x2="0.16665" y2="2.1542375" layer="21"/>
<rectangle x1="0.67275" y1="2.1418875" x2="1.00615" y2="2.1542375" layer="21"/>
<rectangle x1="1.42585" y1="2.1418875" x2="1.75925" y2="2.1542375" layer="21"/>
<rectangle x1="-1.79635" y1="2.1542375" x2="-1.46305" y2="2.166575" layer="21"/>
<rectangle x1="-1.06795" y1="2.1542375" x2="-0.72225" y2="2.166575" layer="21"/>
<rectangle x1="-0.19145" y1="2.1542375" x2="0.16665" y2="2.166575" layer="21"/>
<rectangle x1="0.67275" y1="2.1542375" x2="1.00615" y2="2.166575" layer="21"/>
<rectangle x1="1.42585" y1="2.1542375" x2="1.75925" y2="2.166575" layer="21"/>
<rectangle x1="-1.79635" y1="2.166578125" x2="-1.46305" y2="2.178928125" layer="21"/>
<rectangle x1="-1.08025" y1="2.166578125" x2="-0.72225" y2="2.178928125" layer="21"/>
<rectangle x1="-0.19145" y1="2.166578125" x2="0.16665" y2="2.178928125" layer="21"/>
<rectangle x1="0.67275" y1="2.166578125" x2="1.00615" y2="2.178928125" layer="21"/>
<rectangle x1="1.42585" y1="2.166578125" x2="1.75925" y2="2.178928125" layer="21"/>
<rectangle x1="-1.79635" y1="2.178928125" x2="-1.46305" y2="2.191265625" layer="21"/>
<rectangle x1="-1.08025" y1="2.178928125" x2="-0.72225" y2="2.191265625" layer="21"/>
<rectangle x1="-0.19145" y1="2.178928125" x2="0.16665" y2="2.191265625" layer="21"/>
<rectangle x1="0.67275" y1="2.178928125" x2="1.00615" y2="2.191265625" layer="21"/>
<rectangle x1="1.42585" y1="2.178928125" x2="1.75925" y2="2.191265625" layer="21"/>
<rectangle x1="-1.79635" y1="2.19126875" x2="-1.46305" y2="2.20361875" layer="21"/>
<rectangle x1="-1.09265" y1="2.19126875" x2="-0.72225" y2="2.20361875" layer="21"/>
<rectangle x1="-0.19145" y1="2.19126875" x2="0.16665" y2="2.20361875" layer="21"/>
<rectangle x1="0.67275" y1="2.19126875" x2="1.00615" y2="2.20361875" layer="21"/>
<rectangle x1="1.42585" y1="2.19126875" x2="1.75925" y2="2.20361875" layer="21"/>
<rectangle x1="-1.79635" y1="2.20361875" x2="-1.46305" y2="2.21596875" layer="21"/>
<rectangle x1="-1.09265" y1="2.20361875" x2="-0.72225" y2="2.21596875" layer="21"/>
<rectangle x1="-0.19145" y1="2.20361875" x2="0.16665" y2="2.21596875" layer="21"/>
<rectangle x1="0.67275" y1="2.20361875" x2="1.00615" y2="2.21596875" layer="21"/>
<rectangle x1="1.42585" y1="2.20361875" x2="1.75925" y2="2.21596875" layer="21"/>
<rectangle x1="-1.79635" y1="2.21596875" x2="-1.46305" y2="2.22830625" layer="21"/>
<rectangle x1="-1.10495" y1="2.21596875" x2="-0.73465" y2="2.22830625" layer="21"/>
<rectangle x1="-0.19145" y1="2.21596875" x2="0.16665" y2="2.22830625" layer="21"/>
<rectangle x1="0.67275" y1="2.21596875" x2="1.00615" y2="2.22830625" layer="21"/>
<rectangle x1="1.42585" y1="2.21596875" x2="1.75925" y2="2.22830625" layer="21"/>
<rectangle x1="-1.79635" y1="2.228309375" x2="-1.46305" y2="2.240659375" layer="21"/>
<rectangle x1="-1.11735" y1="2.228309375" x2="-0.73465" y2="2.240659375" layer="21"/>
<rectangle x1="-0.19145" y1="2.228309375" x2="0.16665" y2="2.240659375" layer="21"/>
<rectangle x1="0.67275" y1="2.228309375" x2="1.00615" y2="2.240659375" layer="21"/>
<rectangle x1="1.42585" y1="2.228309375" x2="1.75925" y2="2.240659375" layer="21"/>
<rectangle x1="-1.79635" y1="2.240659375" x2="-1.46305" y2="2.252996875" layer="21"/>
<rectangle x1="-1.12965" y1="2.240659375" x2="-0.73465" y2="2.252996875" layer="21"/>
<rectangle x1="-0.19145" y1="2.240659375" x2="0.16665" y2="2.252996875" layer="21"/>
<rectangle x1="0.67275" y1="2.240659375" x2="1.00615" y2="2.252996875" layer="21"/>
<rectangle x1="1.42585" y1="2.240659375" x2="1.75925" y2="2.252996875" layer="21"/>
<rectangle x1="-1.79635" y1="2.253" x2="-1.46305" y2="2.26535" layer="21"/>
<rectangle x1="-1.15435" y1="2.253" x2="-0.74695" y2="2.26535" layer="21"/>
<rectangle x1="-0.53705" y1="2.253" x2="0.49995" y2="2.26535" layer="21"/>
<rectangle x1="0.67275" y1="2.253" x2="1.00615" y2="2.26535" layer="21"/>
<rectangle x1="1.42585" y1="2.253" x2="1.75925" y2="2.26535" layer="21"/>
<rectangle x1="-1.79635" y1="2.26535" x2="-1.45065" y2="2.2776875" layer="21"/>
<rectangle x1="-1.20375" y1="2.26535" x2="-0.74695" y2="2.2776875" layer="21"/>
<rectangle x1="-0.54945" y1="2.26535" x2="0.52465" y2="2.2776875" layer="21"/>
<rectangle x1="0.67275" y1="2.26535" x2="1.00615" y2="2.2776875" layer="21"/>
<rectangle x1="1.42585" y1="2.26535" x2="1.75925" y2="2.2776875" layer="21"/>
<rectangle x1="-1.79635" y1="2.2776875" x2="-0.74695" y2="2.2900375" layer="21"/>
<rectangle x1="-0.56175" y1="2.2776875" x2="0.52465" y2="2.2900375" layer="21"/>
<rectangle x1="0.67275" y1="2.2776875" x2="1.00615" y2="2.2900375" layer="21"/>
<rectangle x1="1.42585" y1="2.2776875" x2="1.75925" y2="2.2900375" layer="21"/>
<rectangle x1="-1.79635" y1="2.2900375" x2="-0.75935" y2="2.3023875" layer="21"/>
<rectangle x1="-0.56175" y1="2.2900375" x2="0.52465" y2="2.3023875" layer="21"/>
<rectangle x1="0.67275" y1="2.2900375" x2="1.00615" y2="2.3023875" layer="21"/>
<rectangle x1="1.42585" y1="2.2900375" x2="1.75925" y2="2.3023875" layer="21"/>
<rectangle x1="-1.79635" y1="2.3023875" x2="-0.75935" y2="2.314725" layer="21"/>
<rectangle x1="-0.56175" y1="2.3023875" x2="0.52465" y2="2.314725" layer="21"/>
<rectangle x1="0.67275" y1="2.3023875" x2="1.00615" y2="2.314725" layer="21"/>
<rectangle x1="1.42585" y1="2.3023875" x2="1.75925" y2="2.314725" layer="21"/>
<rectangle x1="-1.79635" y1="2.314728125" x2="-0.77165" y2="2.327078125" layer="21"/>
<rectangle x1="-0.56175" y1="2.314728125" x2="0.52465" y2="2.327078125" layer="21"/>
<rectangle x1="0.67275" y1="2.314728125" x2="1.00615" y2="2.327078125" layer="21"/>
<rectangle x1="1.42585" y1="2.314728125" x2="1.75925" y2="2.327078125" layer="21"/>
<rectangle x1="-1.79635" y1="2.327078125" x2="-0.77165" y2="2.339415625" layer="21"/>
<rectangle x1="-0.56175" y1="2.327078125" x2="0.52465" y2="2.339415625" layer="21"/>
<rectangle x1="0.67275" y1="2.327078125" x2="1.00615" y2="2.339415625" layer="21"/>
<rectangle x1="1.42585" y1="2.327078125" x2="1.75925" y2="2.339415625" layer="21"/>
<rectangle x1="-1.79635" y1="2.33941875" x2="-0.78405" y2="2.35176875" layer="21"/>
<rectangle x1="-0.56175" y1="2.33941875" x2="0.52465" y2="2.35176875" layer="21"/>
<rectangle x1="0.67275" y1="2.33941875" x2="1.00615" y2="2.35176875" layer="21"/>
<rectangle x1="1.42585" y1="2.33941875" x2="1.75925" y2="2.35176875" layer="21"/>
<rectangle x1="-1.79635" y1="2.35176875" x2="-0.79635" y2="2.36410625" layer="21"/>
<rectangle x1="-0.56175" y1="2.35176875" x2="0.52465" y2="2.36410625" layer="21"/>
<rectangle x1="0.67275" y1="2.35176875" x2="1.00615" y2="2.36410625" layer="21"/>
<rectangle x1="1.42585" y1="2.35176875" x2="1.75925" y2="2.36410625" layer="21"/>
<rectangle x1="-1.79635" y1="2.364109375" x2="-0.79635" y2="2.376459375" layer="21"/>
<rectangle x1="-0.56175" y1="2.364109375" x2="0.52465" y2="2.376459375" layer="21"/>
<rectangle x1="0.67275" y1="2.364109375" x2="1.00615" y2="2.376459375" layer="21"/>
<rectangle x1="1.42585" y1="2.364109375" x2="1.75925" y2="2.376459375" layer="21"/>
<rectangle x1="-1.79635" y1="2.376459375" x2="-0.80865" y2="2.388809375" layer="21"/>
<rectangle x1="-0.56175" y1="2.376459375" x2="0.52465" y2="2.388809375" layer="21"/>
<rectangle x1="0.67275" y1="2.376459375" x2="1.00615" y2="2.388809375" layer="21"/>
<rectangle x1="1.42585" y1="2.376459375" x2="1.75925" y2="2.388809375" layer="21"/>
<rectangle x1="-1.79635" y1="2.388809375" x2="-0.82105" y2="2.401146875" layer="21"/>
<rectangle x1="-0.56175" y1="2.388809375" x2="0.52465" y2="2.401146875" layer="21"/>
<rectangle x1="0.67275" y1="2.388809375" x2="1.00615" y2="2.401146875" layer="21"/>
<rectangle x1="1.42585" y1="2.388809375" x2="1.75925" y2="2.401146875" layer="21"/>
<rectangle x1="-1.79635" y1="2.40115" x2="-0.83335" y2="2.4135" layer="21"/>
<rectangle x1="-0.56175" y1="2.40115" x2="0.52465" y2="2.4135" layer="21"/>
<rectangle x1="0.67275" y1="2.40115" x2="1.00615" y2="2.4135" layer="21"/>
<rectangle x1="1.42585" y1="2.40115" x2="1.75925" y2="2.4135" layer="21"/>
<rectangle x1="-1.79635" y1="2.4135" x2="-0.84575" y2="2.4258375" layer="21"/>
<rectangle x1="-0.56175" y1="2.4135" x2="0.52465" y2="2.4258375" layer="21"/>
<rectangle x1="0.67275" y1="2.4135" x2="1.00615" y2="2.4258375" layer="21"/>
<rectangle x1="1.42585" y1="2.4135" x2="1.75925" y2="2.4258375" layer="21"/>
<rectangle x1="-1.79635" y1="2.4258375" x2="-0.85805" y2="2.4381875" layer="21"/>
<rectangle x1="-0.56175" y1="2.4258375" x2="0.52465" y2="2.4381875" layer="21"/>
<rectangle x1="0.67275" y1="2.4258375" x2="1.00615" y2="2.4381875" layer="21"/>
<rectangle x1="1.42585" y1="2.4258375" x2="1.75925" y2="2.4381875" layer="21"/>
<rectangle x1="-1.79635" y1="2.4381875" x2="-0.87045" y2="2.450525" layer="21"/>
<rectangle x1="-0.56175" y1="2.4381875" x2="0.52465" y2="2.450525" layer="21"/>
<rectangle x1="0.67275" y1="2.4381875" x2="1.00615" y2="2.450525" layer="21"/>
<rectangle x1="1.42585" y1="2.4381875" x2="1.75925" y2="2.450525" layer="21"/>
<rectangle x1="-1.79635" y1="2.450528125" x2="-0.89515" y2="2.462878125" layer="21"/>
<rectangle x1="-0.56175" y1="2.450528125" x2="0.52465" y2="2.462878125" layer="21"/>
<rectangle x1="0.67275" y1="2.450528125" x2="1.00615" y2="2.462878125" layer="21"/>
<rectangle x1="1.42585" y1="2.450528125" x2="1.75925" y2="2.462878125" layer="21"/>
<rectangle x1="-1.79635" y1="2.462878125" x2="-0.91985" y2="2.475215625" layer="21"/>
<rectangle x1="-0.56175" y1="2.462878125" x2="0.52465" y2="2.475215625" layer="21"/>
<rectangle x1="0.67275" y1="2.462878125" x2="1.00615" y2="2.475215625" layer="21"/>
<rectangle x1="1.42585" y1="2.462878125" x2="1.75925" y2="2.475215625" layer="21"/>
<rectangle x1="-1.79635" y1="2.47521875" x2="-0.94445" y2="2.48756875" layer="21"/>
<rectangle x1="-0.56175" y1="2.47521875" x2="0.52465" y2="2.48756875" layer="21"/>
<rectangle x1="0.67275" y1="2.47521875" x2="1.00615" y2="2.48756875" layer="21"/>
<rectangle x1="1.42585" y1="2.47521875" x2="1.75925" y2="2.48756875" layer="21"/>
<rectangle x1="-1.79635" y1="2.48756875" x2="-0.96915" y2="2.49991875" layer="21"/>
<rectangle x1="-0.56175" y1="2.48756875" x2="0.52465" y2="2.49991875" layer="21"/>
<rectangle x1="0.67275" y1="2.48756875" x2="1.00615" y2="2.49991875" layer="21"/>
<rectangle x1="1.42585" y1="2.48756875" x2="1.75925" y2="2.49991875" layer="21"/>
<rectangle x1="-1.78405" y1="2.49991875" x2="-1.01855" y2="2.51225625" layer="21"/>
<rectangle x1="-0.54945" y1="2.49991875" x2="0.52465" y2="2.51225625" layer="21"/>
<rectangle x1="0.67275" y1="2.49991875" x2="1.00615" y2="2.51225625" layer="21"/>
<rectangle x1="1.43825" y1="2.49991875" x2="1.75925" y2="2.51225625" layer="21"/>
<rectangle x1="-1.77165" y1="2.512259375" x2="-1.09265" y2="2.524609375" layer="21"/>
<rectangle x1="-0.53705" y1="2.512259375" x2="0.51225" y2="2.524609375" layer="21"/>
<rectangle x1="0.68515" y1="2.512259375" x2="0.99375" y2="2.524609375" layer="21"/>
<rectangle x1="1.43825" y1="2.512259375" x2="1.74685" y2="2.524609375" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="DTU">
<text x="0" y="0" size="2.54" layer="95" align="bottom-center">DTU</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="DTU">
<gates>
<gate name="G$1" symbol="DTU" x="0" y="0"/>
</gates>
<devices>
<device name="LARGE" package="DTU-LARGE">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMALL-5MM" package="DTU-SMALL-5MM">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="XTRASMALL-2.5MM" package="DTU-XTRASMALL-2.5MM">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MEDIUM-13MM" package="DTU-MEDIUM-13MM">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MEDIUM-9MM" package="DTU-MEDIUM-9MM">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMALL-4MM" package="DTU-SMALL-4MM">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-3m" urn="urn:adsk.eagle:library:119">
<description>&lt;b&gt;3M Connectors&lt;/b&gt;&lt;p&gt;
PCMCIA-CompactFlash Connectors&lt;p&gt;
Zero Insertion Force Socked&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="PAK100/2500-10" urn="urn:adsk.eagle:footprint:5511/1" library_version="1">
<description>&lt;b&gt;3M (TM) Pak 100 4-Wall Header&lt;/b&gt; Straight&lt;p&gt;
Source: 3M</description>
<wire x1="-10" y1="4.2" x2="10" y2="4.2" width="0.2032" layer="21"/>
<wire x1="10" y1="4.2" x2="10" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="10" y1="-4.2" x2="5.938" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="5.938" y1="-4.2" x2="5.938" y2="-3.9" width="0.2032" layer="21"/>
<wire x1="5.938" y1="-3.9" x2="4.459" y2="-3.9" width="0.2032" layer="21"/>
<wire x1="4.459" y1="-3.9" x2="4.459" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="4.459" y1="-4.2" x2="1.883" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="1.883" y1="-4.2" x2="1.883" y2="-2.65" width="0.2032" layer="21"/>
<wire x1="1.883" y1="-2.65" x2="-1.883" y2="-2.65" width="0.2032" layer="21"/>
<wire x1="-1.883" y1="-2.65" x2="-1.883" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="1.883" y1="-4.2" x2="-1.883" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="-1.883" y1="-4.2" x2="-10" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="-10" y1="-4.2" x2="-10" y2="4.2" width="0.2032" layer="21"/>
<wire x1="-8.875" y1="3.275" x2="8.875" y2="3.275" width="0.2032" layer="21"/>
<wire x1="8.875" y1="3.275" x2="8.875" y2="-3.275" width="0.2032" layer="21"/>
<wire x1="8.875" y1="-3.275" x2="1.883" y2="-3.275" width="0.2032" layer="21"/>
<wire x1="-1.883" y1="-3.275" x2="-8.875" y2="-3.275" width="0.2032" layer="21"/>
<wire x1="-8.875" y1="-3.275" x2="-8.875" y2="3.275" width="0.2032" layer="21"/>
<pad name="1" x="-5.08" y="-1.27" drill="1" diameter="1.4224"/>
<pad name="2" x="-5.08" y="1.27" drill="1" diameter="1.4224"/>
<pad name="3" x="-2.54" y="-1.27" drill="1" diameter="1.4224"/>
<pad name="4" x="-2.54" y="1.27" drill="1" diameter="1.4224"/>
<pad name="5" x="0" y="-1.27" drill="1" diameter="1.4224"/>
<pad name="6" x="0" y="1.27" drill="1" diameter="1.4224"/>
<pad name="7" x="2.54" y="-1.27" drill="1" diameter="1.4224"/>
<pad name="8" x="2.54" y="1.27" drill="1" diameter="1.4224"/>
<pad name="9" x="5.08" y="-1.27" drill="1" diameter="1.4224"/>
<pad name="10" x="5.08" y="1.27" drill="1" diameter="1.4224"/>
<text x="-10.16" y="4.572" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="4.572" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="PAK100/2500-5-10" urn="urn:adsk.eagle:footprint:5510/1" library_version="1">
<description>&lt;b&gt;3M (TM) Pak 100 4-Wall Header&lt;/b&gt; Right Angle&lt;p&gt;
Source: 3M</description>
<wire x1="11.27" y1="1.875" x2="11.27" y2="11.075" width="0.2032" layer="21"/>
<wire x1="11.27" y1="11.075" x2="5.938" y2="11.075" width="0.2032" layer="21"/>
<wire x1="4.459" y1="11.075" x2="5.938" y2="11.075" width="0.2032" layer="21"/>
<wire x1="5.938" y1="11.075" x2="5.938" y2="10.105" width="0.2032" layer="21"/>
<wire x1="5.938" y1="10.105" x2="4.459" y2="10.105" width="0.2032" layer="21"/>
<wire x1="4.459" y1="10.105" x2="4.459" y2="11.075" width="0.2032" layer="21"/>
<wire x1="4.459" y1="11.075" x2="1.883" y2="11.075" width="0.2032" layer="21"/>
<wire x1="1.883" y1="11.075" x2="1.883" y2="4.01" width="0.2032" layer="21"/>
<wire x1="1.883" y1="4.01" x2="-1.883" y2="4.01" width="0.2032" layer="21"/>
<wire x1="-1.883" y1="4.01" x2="-1.883" y2="11.075" width="0.2032" layer="21"/>
<wire x1="1.883" y1="11.075" x2="-1.883" y2="11.075" width="0.2032" layer="21"/>
<wire x1="-1.883" y1="11.075" x2="-10" y2="11.075" width="0.2032" layer="21"/>
<wire x1="-10" y1="11.075" x2="-10" y2="1.875" width="0.2032" layer="21"/>
<wire x1="-6.456" y1="1.875" x2="-10" y2="1.875" width="0.2032" layer="21"/>
<wire x1="7.709" y1="1.875" x2="-6.452" y2="1.875" width="0.2032" layer="51"/>
<wire x1="11.27" y1="1.875" x2="7.713" y2="1.875" width="0.2032" layer="21"/>
<pad name="1" x="-5.08" y="-1.27" drill="1" diameter="1.4224"/>
<pad name="2" x="-5.08" y="1.27" drill="1" diameter="1.4224"/>
<pad name="3" x="-2.54" y="-1.27" drill="1" diameter="1.4224"/>
<pad name="4" x="-2.54" y="1.27" drill="1" diameter="1.4224"/>
<pad name="5" x="0" y="-1.27" drill="1" diameter="1.4224"/>
<pad name="6" x="0" y="1.27" drill="1" diameter="1.4224"/>
<pad name="7" x="2.54" y="-1.27" drill="1" diameter="1.4224"/>
<pad name="8" x="2.54" y="1.27" drill="1" diameter="1.4224"/>
<pad name="9" x="5.08" y="-1.27" drill="1" diameter="1.4224"/>
<pad name="10" x="5.08" y="1.27" drill="1" diameter="1.4224"/>
<text x="-10.16" y="-3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<polygon width="0.2032" layer="21">
<vertex x="-6.36" y="10.945"/>
<vertex x="-3.81" y="10.945"/>
<vertex x="-5.085" y="8.37"/>
</polygon>
</package>
</packages>
<packages3d>
<package3d name="PAK100/2500-10" urn="urn:adsk.eagle:package:5571/1" type="box" library_version="1">
<description>3M (TM) Pak 100 4-Wall Header Straight
Source: 3M</description>
</package3d>
<package3d name="PAK100/2500-5-10" urn="urn:adsk.eagle:package:5573/1" type="box" library_version="1">
<description>3M (TM) Pak 100 4-Wall Header Right Angle
Source: 3M</description>
</package3d>
</packages3d>
<symbols>
<symbol name="PINV" urn="urn:adsk.eagle:symbol:5508/1" library_version="1">
<text x="-1.27" y="0.889" size="1.778" layer="95" rot="R180">&gt;NAME</text>
<text x="-3.81" y="2.667" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="0" y1="-0.254" x2="2.794" y2="0.254" layer="94"/>
<pin name="KL" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="PIN" urn="urn:adsk.eagle:symbol:5509/1" library_version="1">
<text x="-1.27" y="0.889" size="1.778" layer="95" rot="R180">&gt;NAME</text>
<rectangle x1="0" y1="-0.254" x2="2.794" y2="0.254" layer="94"/>
<pin name="KL" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="2510-" urn="urn:adsk.eagle:component:5613/1" prefix="X" library_version="1">
<description>&lt;b&gt;3M (TM) Pak 100 4-Wall Header&lt;/b&gt;&lt;p&gt;
Source: 3M</description>
<gates>
<gate name="-1" symbol="PINV" x="0" y="0" addlevel="always"/>
<gate name="-2" symbol="PIN" x="0" y="-2.54" addlevel="always"/>
<gate name="-3" symbol="PIN" x="0" y="-5.08" addlevel="always"/>
<gate name="-4" symbol="PIN" x="0" y="-7.62" addlevel="always"/>
<gate name="-5" symbol="PIN" x="0" y="-10.16" addlevel="always"/>
<gate name="-6" symbol="PIN" x="0" y="-12.7" addlevel="always"/>
<gate name="-7" symbol="PIN" x="0" y="-15.24" addlevel="always"/>
<gate name="-8" symbol="PIN" x="0" y="-17.78" addlevel="always"/>
<gate name="-9" symbol="PIN" x="0" y="-20.32" addlevel="always"/>
<gate name="-10" symbol="PIN" x="0" y="-22.86" addlevel="always"/>
</gates>
<devices>
<device name="" package="PAK100/2500-10">
<connects>
<connect gate="-1" pin="KL" pad="1"/>
<connect gate="-10" pin="KL" pad="10"/>
<connect gate="-2" pin="KL" pad="2"/>
<connect gate="-3" pin="KL" pad="3"/>
<connect gate="-4" pin="KL" pad="4"/>
<connect gate="-5" pin="KL" pad="5"/>
<connect gate="-6" pin="KL" pad="6"/>
<connect gate="-7" pin="KL" pad="7"/>
<connect gate="-8" pin="KL" pad="8"/>
<connect gate="-9" pin="KL" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5571/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="3M" constant="no"/>
<attribute name="MPN" value="2510-6002UB" constant="no"/>
<attribute name="OC_FARNELL" value="9838244" constant="no"/>
<attribute name="OC_NEWARK" value="46F4725" constant="no"/>
</technology>
</technologies>
</device>
<device name="5" package="PAK100/2500-5-10">
<connects>
<connect gate="-1" pin="KL" pad="1"/>
<connect gate="-10" pin="KL" pad="10"/>
<connect gate="-2" pin="KL" pad="2"/>
<connect gate="-3" pin="KL" pad="3"/>
<connect gate="-4" pin="KL" pad="4"/>
<connect gate="-5" pin="KL" pad="5"/>
<connect gate="-6" pin="KL" pad="6"/>
<connect gate="-7" pin="KL" pad="7"/>
<connect gate="-8" pin="KL" pad="8"/>
<connect gate="-9" pin="KL" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5573/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="1788669" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-3m">
<description>&lt;b&gt;3M Connectors&lt;/b&gt;&lt;p&gt;
PCMCIA-CompactFlash Connectors&lt;p&gt;
Zero Insertion Force Socked&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="PAK100/2500-06">
<description>&lt;b&gt;3M (TM) Pak 100 4-Wall Header&lt;/b&gt; Straight&lt;p&gt;
Source: 3M</description>
<wire x1="-7.46" y1="4.2" x2="7.46" y2="4.2" width="0.2032" layer="21"/>
<wire x1="7.46" y1="4.2" x2="7.46" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="7.46" y1="-4.2" x2="0.858" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="0.858" y1="-4.2" x2="0.858" y2="-3.9" width="0.2032" layer="21"/>
<wire x1="0.858" y1="-3.9" x2="-0.621" y2="-3.9" width="0.2032" layer="21"/>
<wire x1="-0.621" y1="-3.9" x2="-0.621" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="-0.621" y1="-4.2" x2="1.883" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="1.883" y1="-4.2" x2="1.883" y2="-2.65" width="0.2032" layer="21"/>
<wire x1="1.883" y1="-2.65" x2="-1.883" y2="-2.65" width="0.2032" layer="21"/>
<wire x1="-1.883" y1="-2.65" x2="-1.883" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="-0.621" y1="-4.2" x2="-1.883" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="-1.883" y1="-4.2" x2="-7.46" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="-7.46" y1="-4.2" x2="-7.46" y2="4.2" width="0.2032" layer="21"/>
<wire x1="-6.335" y1="3.275" x2="6.335" y2="3.275" width="0.2032" layer="21"/>
<wire x1="6.335" y1="3.275" x2="6.335" y2="-3.275" width="0.2032" layer="21"/>
<wire x1="6.335" y1="-3.275" x2="1.883" y2="-3.275" width="0.2032" layer="21"/>
<wire x1="-1.883" y1="-3.275" x2="-6.335" y2="-3.275" width="0.2032" layer="21"/>
<wire x1="-6.335" y1="-3.275" x2="-6.335" y2="3.275" width="0.2032" layer="21"/>
<pad name="1" x="-2.54" y="-1.27" drill="1" diameter="1.4224"/>
<pad name="2" x="-2.54" y="1.27" drill="1" diameter="1.4224"/>
<pad name="3" x="0" y="-1.27" drill="1" diameter="1.4224"/>
<pad name="4" x="0" y="1.27" drill="1" diameter="1.4224"/>
<pad name="5" x="2.54" y="-1.27" drill="1" diameter="1.4224"/>
<pad name="6" x="2.54" y="1.27" drill="1" diameter="1.4224"/>
<text x="-7.62" y="4.572" size="1.27" layer="25">&gt;NAME</text>
<text x="0" y="4.572" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="6-PIN">
<wire x1="3.81" y1="-5.08" x2="-3.81" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="5.08" x2="-3.81" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-1.27" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-1.27" y2="-2.54" width="0.6096" layer="94"/>
<text x="-3.81" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="5.842" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="4" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="6" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="2506-">
<gates>
<gate name="G$1" symbol="6-PIN" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PAK100/2500-06">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Aesthetics">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find non-functional items- supply symbols, logos, notations, frame blocks, etc.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="FIDUCIAL-1X2">
<smd name="1" x="0" y="0" dx="1" dy="1" layer="1" roundness="100" cream="no"/>
</package>
<package name="MICRO-FIDUCIAL">
<smd name="1" x="0" y="0" dx="0.635" dy="0.635" layer="1" roundness="100" cream="no"/>
</package>
</packages>
<symbols>
<symbol name="FIDUCIAL">
<wire x1="-0.762" y1="0.762" x2="0.762" y2="-0.762" width="0.254" layer="94"/>
<wire x1="0.762" y1="0.762" x2="-0.762" y2="-0.762" width="0.254" layer="94"/>
<circle x="0" y="0" radius="1.27" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FIDUCIAL" prefix="FD">
<description>&lt;b&gt;Fiducial Alignment Points&lt;/b&gt;
Various fiducial points for machine vision alignment.</description>
<gates>
<gate name="G$1" symbol="FIDUCIAL" x="0" y="0"/>
</gates>
<devices>
<device name="1X2" package="FIDUCIAL-1X2">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="UFIDUCIAL" package="MICRO-FIDUCIAL">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="opto-trans-siemens" urn="urn:adsk.eagle:library:317">
<description>&lt;b&gt;Siemens Opto Transistors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="BPW32" urn="urn:adsk.eagle:footprint:21775/1" library_version="3">
<description>&lt;B&gt;PHOTO DIODE&lt;/B&gt;</description>
<wire x1="-1.143" y1="-1.651" x2="1.143" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="1.143" y1="1.651" x2="1.143" y2="1.524" width="0.1524" layer="21"/>
<wire x1="1.143" y1="1.651" x2="-1.143" y2="1.651" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-1.651" x2="-1.143" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="1.651" x2="-0.381" y2="2.159" width="0.1524" layer="51"/>
<wire x1="0.381" y1="2.159" x2="0.381" y2="1.651" width="0.1524" layer="51"/>
<wire x1="0.381" y1="2.159" x2="-0.381" y2="2.159" width="0.1524" layer="51"/>
<wire x1="-0.381" y1="-2.159" x2="-0.381" y2="-2.032" width="0.1524" layer="51"/>
<wire x1="0.381" y1="-1.651" x2="0.381" y2="-2.159" width="0.1524" layer="51"/>
<wire x1="0.381" y1="-2.159" x2="-0.381" y2="-2.159" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="-2.159" x2="-0.381" y2="-2.159" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="-2.032" x2="-0.889" y2="-2.159" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="-2.032" x2="-0.381" y2="-2.032" width="0.1524" layer="51"/>
<wire x1="-0.381" y1="-2.032" x2="-0.381" y2="-1.651" width="0.1524" layer="51"/>
<wire x1="1.143" y1="1.524" x2="-1.143" y2="1.524" width="0.1524" layer="21"/>
<wire x1="1.143" y1="1.524" x2="1.143" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="1.524" x2="-1.143" y2="1.651" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-1.524" x2="1.143" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-1.524" x2="-1.143" y2="1.524" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-1.524" x2="1.143" y2="-1.651" width="0.1524" layer="21"/>
<pad name="K" x="0" y="-2.54" drill="0.8128" shape="long"/>
<pad name="A" x="0" y="2.54" drill="0.8128" shape="long"/>
<text x="-1.651" y="-2.032" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="2.921" y="-2.032" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.635" y1="-0.635" x2="0.635" y2="0.635" layer="21"/>
</package>
</packages>
<packages3d>
<package3d name="BPW32" urn="urn:adsk.eagle:package:21789/1" type="box" library_version="3">
<description>PHOTO DIODE</description>
<packageinstances>
<packageinstance name="BPW32"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="OED" urn="urn:adsk.eagle:symbol:21773/2" library_version="3">
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-3.683" y1="-3.048" x2="-2.286" y2="-1.651" width="0.1524" layer="94"/>
<wire x1="-2.286" y1="-1.651" x2="-3.175" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="-3.175" y1="-2.032" x2="-2.667" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.667" y1="-2.54" x2="-2.286" y2="-1.651" width="0.1524" layer="94"/>
<wire x1="-2.413" y1="-0.508" x2="-3.302" y2="-0.889" width="0.1524" layer="94"/>
<wire x1="-3.302" y1="-0.889" x2="-2.794" y2="-1.397" width="0.1524" layer="94"/>
<wire x1="-2.794" y1="-1.397" x2="-2.413" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="-1.905" x2="-2.413" y2="-0.508" width="0.1524" layer="94"/>
<text x="3.556" y="-4.318" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-4.318" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="point" direction="pas" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="BPW32" urn="urn:adsk.eagle:component:21802/3" prefix="D" library_version="3">
<description>&lt;B&gt;PHOTO DIODE&lt;/B&gt;</description>
<gates>
<gate name="1" symbol="OED" x="0" y="0"/>
</gates>
<devices>
<device name="" package="BPW32">
<connects>
<connect gate="1" pin="A" pad="A"/>
<connect gate="1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:21789/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="QP1" library="transistor-small-signal" deviceset="BSS84" device="" value="BSS84"/>
<part name="X1" library="con-samtec" deviceset="SSW-102-02-S-S" device="" value="BPW46"/>
<part name="R11" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="820k"/>
<part name="R12" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="150k"/>
<part name="R14" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="5.6k"/>
<part name="R13" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="56k"/>
<part name="R15" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="12k"/>
<part name="C11" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="330n"/>
<part name="QN1" library="transistor-small-signal" deviceset="BSS123" device="" value="MMBF170-7-F "/>
<part name="C12" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="100n"/>
<part name="C13" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="470nF"/>
<part name="SUPPLY1" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY2" library="supply2" deviceset="GND" device=""/>
<part name="C14" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="2.2n"/>
<part name="FRAME1" library="frames" deviceset="DINA4_P" device="" value="Line sensor"/>
<part name="SUPPLY3" library="supply2" deviceset="VDD" device=""/>
<part name="QP2" library="transistor-small-signal" deviceset="BSS84" device="" value="BSS84"/>
<part name="X2" library="con-samtec" deviceset="SSW-102-02-S-S" device="" value="BPW46"/>
<part name="R21" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="820k"/>
<part name="R22" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="150k"/>
<part name="R24" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="5.6k"/>
<part name="R23" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="56k"/>
<part name="R25" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="12k"/>
<part name="C21" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="330n"/>
<part name="QN2" library="transistor-small-signal" deviceset="BSS123" device="" value="MMBF170-7-F "/>
<part name="C22" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="100n"/>
<part name="C23" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="470n"/>
<part name="SUPPLY4" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY5" library="supply2" deviceset="GND" device=""/>
<part name="C24" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="2.2n"/>
<part name="SUPPLY6" library="supply2" deviceset="VDD" device=""/>
<part name="QP3" library="transistor-small-signal" deviceset="BSS84" device="" value="BSS84"/>
<part name="X3" library="con-samtec" deviceset="SSW-102-02-S-S" device="" value="BPW46"/>
<part name="R31" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="820k"/>
<part name="R32" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="150k"/>
<part name="R34" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="5.6k"/>
<part name="R33" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="56k"/>
<part name="R35" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="12k"/>
<part name="C31" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="330n"/>
<part name="QN3" library="transistor-small-signal" deviceset="BSS123" device="" value="MMBF170-7-F "/>
<part name="C32" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="100n"/>
<part name="C33" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="470n"/>
<part name="SUPPLY7" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY8" library="supply2" deviceset="GND" device=""/>
<part name="C34" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="2.2n"/>
<part name="SUPPLY9" library="supply2" deviceset="VDD" device=""/>
<part name="QP4" library="transistor-small-signal" deviceset="BSS84" device="" value="BSS84"/>
<part name="X4" library="con-samtec" deviceset="SSW-102-02-S-S" device="" value="BPW46"/>
<part name="R41" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="820k"/>
<part name="R42" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="150k"/>
<part name="R44" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="5.6k"/>
<part name="R43" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="56k"/>
<part name="R45" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="12k"/>
<part name="C41" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="330n"/>
<part name="QN4" library="transistor-small-signal" deviceset="BSS123" device="" value="MMBF170-7-F "/>
<part name="C42" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="100n"/>
<part name="C43" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="470n"/>
<part name="SUPPLY10" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY11" library="supply2" deviceset="GND" device=""/>
<part name="C44" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="2.2n"/>
<part name="SUPPLY12" library="supply2" deviceset="VDD" device=""/>
<part name="QP5" library="transistor-small-signal" deviceset="BSS84" device="" value="BSS84"/>
<part name="X5" library="con-samtec" deviceset="SSW-102-02-S-S" device="" value="BPW46"/>
<part name="R51" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="820k"/>
<part name="R52" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="150k"/>
<part name="R54" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="5.6k"/>
<part name="R53" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="56k"/>
<part name="R55" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="12k"/>
<part name="C51" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="330n"/>
<part name="QN5" library="transistor-small-signal" deviceset="BSS123" device="" value="MMBF170-7-F "/>
<part name="C52" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="100n"/>
<part name="C53" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="470n"/>
<part name="SUPPLY13" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY14" library="supply2" deviceset="GND" device=""/>
<part name="C54" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="2.2n"/>
<part name="SUPPLY15" library="supply2" deviceset="VDD" device=""/>
<part name="QP6" library="transistor-small-signal" deviceset="BSS84" device="" value="BSS84"/>
<part name="X6" library="con-samtec" deviceset="SSW-102-02-S-S" device="" value="BPW46"/>
<part name="R61" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="820k"/>
<part name="R62" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="150k"/>
<part name="R64" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="5.6k"/>
<part name="R63" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="56k"/>
<part name="R65" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="12k"/>
<part name="C61" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="330n"/>
<part name="QN6" library="transistor-small-signal" deviceset="BSS123" device="" value="MMBF170-7-F "/>
<part name="C62" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="100n"/>
<part name="C63" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="470n"/>
<part name="SUPPLY16" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY17" library="supply2" deviceset="GND" device=""/>
<part name="C64" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="2.2n"/>
<part name="SUPPLY18" library="supply2" deviceset="VDD" device=""/>
<part name="QP7" library="transistor-small-signal" deviceset="BSS84" device="" value="BSS84"/>
<part name="X7" library="con-samtec" deviceset="SSW-102-02-S-S" device="" value="BPW46"/>
<part name="R71" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="820k"/>
<part name="R72" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="150k"/>
<part name="R74" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="5.6k"/>
<part name="R73" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="56k"/>
<part name="R75" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="12k"/>
<part name="C71" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="330n"/>
<part name="QN7" library="transistor-small-signal" deviceset="BSS123" device="" value="MMBF170-7-F "/>
<part name="C72" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="100n"/>
<part name="C73" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="470n"/>
<part name="SUPPLY19" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY20" library="supply2" deviceset="GND" device=""/>
<part name="C74" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="2.2n"/>
<part name="SUPPLY21" library="supply2" deviceset="VDD" device=""/>
<part name="QP8" library="transistor-small-signal" deviceset="BSS84" device="" value="BSS84"/>
<part name="X8" library="con-samtec" deviceset="SSW-102-02-S-S" device="" value="BPW46"/>
<part name="R81" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="820k"/>
<part name="R82" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="150k"/>
<part name="R84" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="5.6k"/>
<part name="R83" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="56k"/>
<part name="R85" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="12k"/>
<part name="C81" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="330n"/>
<part name="QN8" library="transistor-small-signal" deviceset="BSS123" device="" value="MMBF170-7-F "/>
<part name="C82" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="100n"/>
<part name="C83" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="470n"/>
<part name="SUPPLY22" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY23" library="supply2" deviceset="GND" device=""/>
<part name="C84" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="2.2n"/>
<part name="SUPPLY24" library="supply2" deviceset="VDD" device=""/>
<part name="SENSOR" library="con-3m" library_urn="urn:adsk.eagle:library:119" deviceset="2510-" device="" package3d_urn="urn:adsk.eagle:package:5571/1" value="N2510-6002RB 5x2"/>
<part name="SUPPLY25" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY26" library="supply2" deviceset="VDD" device=""/>
<part name="L1" library="led" deviceset="LED" device="3MM" value="red"/>
<part name="L2" library="led" deviceset="LED" device="3MM" value="red"/>
<part name="L3" library="led" deviceset="LED" device="3MM" value="red"/>
<part name="L4" library="led" deviceset="LED" device="3MM" value="red"/>
<part name="L5" library="led" deviceset="LED" device="3MM" value="red"/>
<part name="L6" library="led" deviceset="LED" device="3MM" value="red"/>
<part name="L7" library="led" deviceset="LED" device="3MM" value="red"/>
<part name="L8" library="led" deviceset="LED" device="3MM" value="red"/>
<part name="L9" library="led" deviceset="LED" device="3MM" value="red"/>
<part name="QL1" library="transistor-small-signal" deviceset="BSS123" device="" value="MMBF170-7-F "/>
<part name="QL2" library="transistor-small-signal" deviceset="BSS123" device="" value="MMBF170-7-F "/>
<part name="QL3" library="transistor-small-signal" deviceset="BSS123" device="" value="MMBF170-7-F "/>
<part name="RL1" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="33"/>
<part name="RL2" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="33"/>
<part name="RL3" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="33"/>
<part name="RL4" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="10k"/>
<part name="CV2" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="1u"/>
<part name="CV1" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="1u"/>
<part name="RL5" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="3.3k"/>
<part name="CL1" library="SparkFun-Capacitors" deviceset="CAP" device="1210" value="10u"/>
<part name="CL2" library="SparkFun-Capacitors" deviceset="CAP" device="1210" value="10u"/>
<part name="CV3" library="SparkFun-Capacitors" deviceset="CAP" device="1210" value="10u"/>
<part name="U$1" library="Logos" deviceset="DTU" device="SMALL-5MM"/>
<part name="U$2" library="Logos" deviceset="DTU" device="LARGE"/>
<part name="LED-C" library="con-3m" deviceset="2506-" device=""/>
<part name="FD1" library="SparkFun-Aesthetics" deviceset="FIDUCIAL" device="1X2"/>
<part name="FD2" library="SparkFun-Aesthetics" deviceset="FIDUCIAL" device="1X2"/>
<part name="FD3" library="SparkFun-Aesthetics" deviceset="FIDUCIAL" device="1X2"/>
<part name="D1" library="opto-trans-siemens" library_urn="urn:adsk.eagle:library:317" deviceset="BPW32" device="" package3d_urn="urn:adsk.eagle:package:21789/1"/>
</parts>
<sheets>
<sheet>
<plain>
<text x="48.26" y="20.32" size="3.81" layer="91">DTU</text>
<text x="55.88" y="2.54" size="2.54" layer="91">HENNING SI HØJ</text>
<text x="132.08" y="2.54" size="3.81" layer="91">C</text>
<text x="25.4" y="63.5" size="1.778" layer="91">+12V</text>
<text x="27.94" y="12.7" size="1.778" layer="91">GND</text>
<text x="38.1" y="60.96" size="1.778" layer="91">low</text>
<text x="37.084" y="36.322" size="1.778" layer="91">high</text>
<text x="-2.54" y="241.3" size="5.08" layer="97">1</text>
<text x="48.26" y="241.3" size="5.08" layer="97">2</text>
<text x="-2.54" y="182.88" size="5.08" layer="97">3</text>
<text x="53.34" y="182.88" size="5.08" layer="97">4</text>
<text x="2.54" y="127" size="5.08" layer="97">5</text>
<text x="55.88" y="127" size="5.08" layer="97">6</text>
<text x="104.14" y="241.3" size="5.08" layer="97">7</text>
<text x="106.68" y="180.34" size="5.08" layer="97">8</text>
</plain>
<instances>
<instance part="QP1" gate="G$1" x="10.16" y="213.36" smashed="yes">
<attribute name="NAME" x="12.7" y="213.36" size="1.778" layer="95"/>
<attribute name="VALUE" x="12.7" y="210.82" size="1.778" layer="96"/>
</instance>
<instance part="X1" gate="-1" x="-15.24" y="228.6" smashed="yes">
<attribute name="VALUE" x="-10.16" y="231.14" size="1.778" layer="96"/>
<attribute name="NAME" x="-18.288" y="229.362" size="1.524" layer="95" rot="R180"/>
</instance>
<instance part="X1" gate="-2" x="-15.24" y="238.76" smashed="yes">
<attribute name="NAME" x="-18.288" y="239.522" size="1.524" layer="95" rot="R180"/>
</instance>
<instance part="R11" gate="G$1" x="-5.08" y="210.82" smashed="yes" rot="R90">
<attribute name="NAME" x="-6.5786" y="207.01" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-1.778" y="207.01" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R12" gate="G$1" x="5.08" y="208.28" smashed="yes" rot="R90">
<attribute name="NAME" x="3.5814" y="204.47" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="8.382" y="204.47" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R14" gate="G$1" x="10.16" y="223.52" smashed="yes" rot="R90">
<attribute name="NAME" x="8.6614" y="219.71" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="13.462" y="219.71" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R13" gate="G$1" x="10.16" y="233.68" smashed="yes" rot="R90">
<attribute name="NAME" x="8.6614" y="229.87" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="13.462" y="229.87" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R15" gate="G$1" x="10.16" y="203.2" smashed="yes" rot="R90">
<attribute name="NAME" x="13.7414" y="204.47" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="13.462" y="199.39" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C11" gate="G$1" x="-5.08" y="200.66" smashed="yes">
<attribute name="NAME" x="-3.556" y="203.581" size="1.778" layer="95"/>
<attribute name="VALUE" x="-3.556" y="198.501" size="1.778" layer="96"/>
</instance>
<instance part="QN1" gate="G$1" x="-12.7" y="208.28" smashed="yes" rot="MR0">
<attribute name="NAME" x="-15.24" y="210.82" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="-7.62" y="213.36" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="C12" gate="G$1" x="2.54" y="215.9" smashed="yes" rot="R90">
<attribute name="NAME" x="-0.381" y="217.424" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="4.699" y="217.424" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C13" gate="G$1" x="20.32" y="223.52" smashed="yes">
<attribute name="NAME" x="21.844" y="226.441" size="1.778" layer="95"/>
<attribute name="VALUE" x="21.844" y="221.361" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY1" gate="GND" x="20.32" y="218.44" smashed="yes">
<attribute name="VALUE" x="18.415" y="215.265" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY2" gate="GND" x="2.54" y="195.58" smashed="yes">
<attribute name="VALUE" x="0.635" y="192.405" size="1.778" layer="96"/>
</instance>
<instance part="C14" gate="G$1" x="20.32" y="200.66" smashed="yes">
<attribute name="NAME" x="21.844" y="203.581" size="1.778" layer="95"/>
<attribute name="VALUE" x="21.844" y="198.501" size="1.778" layer="96"/>
</instance>
<instance part="FRAME1" gate="G$1" x="-35.56" y="-5.08" smashed="yes"/>
<instance part="FRAME1" gate="G$2" x="43.18" y="-5.08" smashed="yes">
<attribute name="LAST_DATE_TIME" x="55.88" y="-3.81" size="2.54" layer="94"/>
<attribute name="SHEET" x="129.54" y="-3.81" size="2.54" layer="94"/>
<attribute name="DRAWING_NAME" x="60.96" y="13.97" size="2.54" layer="94"/>
</instance>
<instance part="SUPPLY3" gate="G$1" x="10.16" y="241.3" smashed="yes">
<attribute name="VALUE" x="8.255" y="244.475" size="1.778" layer="96"/>
</instance>
<instance part="QP2" gate="G$1" x="60.96" y="213.36" smashed="yes">
<attribute name="NAME" x="63.5" y="213.36" size="1.778" layer="95"/>
<attribute name="VALUE" x="63.5" y="210.82" size="1.778" layer="96"/>
</instance>
<instance part="X2" gate="-1" x="35.56" y="228.6" smashed="yes">
<attribute name="VALUE" x="33.02" y="231.14" size="1.778" layer="96"/>
<attribute name="NAME" x="32.512" y="229.362" size="1.524" layer="95" rot="R180"/>
</instance>
<instance part="X2" gate="-2" x="35.56" y="238.76" smashed="yes">
<attribute name="NAME" x="32.512" y="239.522" size="1.524" layer="95" rot="R180"/>
</instance>
<instance part="R21" gate="G$1" x="45.72" y="210.82" smashed="yes" rot="R90">
<attribute name="NAME" x="44.2214" y="207.01" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="49.022" y="207.01" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R22" gate="G$1" x="55.88" y="208.28" smashed="yes" rot="R90">
<attribute name="NAME" x="54.3814" y="204.47" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="59.182" y="204.47" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R24" gate="G$1" x="60.96" y="223.52" smashed="yes" rot="R90">
<attribute name="NAME" x="59.4614" y="219.71" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="64.262" y="219.71" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R23" gate="G$1" x="60.96" y="233.68" smashed="yes" rot="R90">
<attribute name="NAME" x="59.4614" y="229.87" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="64.262" y="229.87" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R25" gate="G$1" x="60.96" y="203.2" smashed="yes" rot="R90">
<attribute name="NAME" x="59.4614" y="199.39" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="64.262" y="199.39" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C21" gate="G$1" x="45.72" y="200.66" smashed="yes">
<attribute name="NAME" x="47.244" y="203.581" size="1.778" layer="95"/>
<attribute name="VALUE" x="47.244" y="198.501" size="1.778" layer="96"/>
</instance>
<instance part="QN2" gate="G$1" x="38.1" y="208.28" smashed="yes" rot="MR0">
<attribute name="NAME" x="38.1" y="215.9" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="48.26" y="218.44" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="C22" gate="G$1" x="53.34" y="215.9" smashed="yes" rot="R90">
<attribute name="NAME" x="50.419" y="217.424" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="55.499" y="217.424" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C23" gate="G$1" x="71.12" y="223.52" smashed="yes">
<attribute name="NAME" x="72.644" y="226.441" size="1.778" layer="95"/>
<attribute name="VALUE" x="72.644" y="221.361" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY4" gate="GND" x="71.12" y="218.44" smashed="yes">
<attribute name="VALUE" x="69.215" y="215.265" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY5" gate="GND" x="53.34" y="195.58" smashed="yes">
<attribute name="VALUE" x="51.435" y="192.405" size="1.778" layer="96"/>
</instance>
<instance part="C24" gate="G$1" x="71.12" y="200.66" smashed="yes">
<attribute name="NAME" x="72.644" y="203.581" size="1.778" layer="95"/>
<attribute name="VALUE" x="72.644" y="198.501" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY6" gate="G$1" x="60.96" y="241.3" smashed="yes">
<attribute name="VALUE" x="59.055" y="244.475" size="1.778" layer="96"/>
</instance>
<instance part="QP3" gate="G$1" x="7.62" y="154.94" smashed="yes">
<attribute name="NAME" x="10.16" y="154.94" size="1.778" layer="95"/>
<attribute name="VALUE" x="10.16" y="152.4" size="1.778" layer="96"/>
</instance>
<instance part="X3" gate="-1" x="-17.78" y="175.26" smashed="yes">
<attribute name="VALUE" x="-20.32" y="177.8" size="1.778" layer="96"/>
<attribute name="NAME" x="-20.828" y="176.022" size="1.524" layer="95" rot="R180"/>
</instance>
<instance part="X3" gate="-2" x="-17.78" y="180.34" smashed="yes">
<attribute name="NAME" x="-20.828" y="181.102" size="1.524" layer="95" rot="R180"/>
</instance>
<instance part="R31" gate="G$1" x="-7.62" y="152.4" smashed="yes" rot="R90">
<attribute name="NAME" x="-9.1186" y="148.59" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-4.318" y="148.59" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R32" gate="G$1" x="2.54" y="149.86" smashed="yes" rot="R90">
<attribute name="NAME" x="1.0414" y="146.05" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="5.842" y="146.05" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R34" gate="G$1" x="7.62" y="165.1" smashed="yes" rot="R90">
<attribute name="NAME" x="6.1214" y="161.29" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="10.922" y="161.29" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R33" gate="G$1" x="7.62" y="175.26" smashed="yes" rot="R90">
<attribute name="NAME" x="6.1214" y="171.45" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="10.922" y="171.45" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R35" gate="G$1" x="7.62" y="144.78" smashed="yes" rot="R90">
<attribute name="NAME" x="6.1214" y="140.97" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="10.922" y="140.97" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C31" gate="G$1" x="-7.62" y="142.24" smashed="yes">
<attribute name="NAME" x="-6.096" y="145.161" size="1.778" layer="95"/>
<attribute name="VALUE" x="-6.096" y="140.081" size="1.778" layer="96"/>
</instance>
<instance part="QN3" gate="G$1" x="-15.24" y="149.86" smashed="yes" rot="MR0">
<attribute name="NAME" x="-17.78" y="149.86" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="-17.78" y="147.32" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="C32" gate="G$1" x="0" y="157.48" smashed="yes" rot="R90">
<attribute name="NAME" x="-2.921" y="159.004" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="2.159" y="159.004" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C33" gate="G$1" x="17.78" y="165.1" smashed="yes">
<attribute name="NAME" x="19.304" y="168.021" size="1.778" layer="95"/>
<attribute name="VALUE" x="19.304" y="162.941" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY7" gate="GND" x="17.78" y="160.02" smashed="yes">
<attribute name="VALUE" x="15.875" y="156.845" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY8" gate="GND" x="0" y="137.16" smashed="yes">
<attribute name="VALUE" x="-1.905" y="133.985" size="1.778" layer="96"/>
</instance>
<instance part="C34" gate="G$1" x="17.78" y="142.24" smashed="yes">
<attribute name="NAME" x="19.304" y="145.161" size="1.778" layer="95"/>
<attribute name="VALUE" x="19.304" y="140.081" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY9" gate="G$1" x="7.62" y="182.88" smashed="yes">
<attribute name="VALUE" x="5.715" y="186.055" size="1.778" layer="96"/>
</instance>
<instance part="QP4" gate="G$1" x="63.5" y="154.94" smashed="yes">
<attribute name="NAME" x="66.04" y="154.94" size="1.778" layer="95"/>
<attribute name="VALUE" x="66.04" y="152.4" size="1.778" layer="96"/>
</instance>
<instance part="X4" gate="-1" x="38.1" y="170.18" smashed="yes">
<attribute name="VALUE" x="35.56" y="172.72" size="1.778" layer="96"/>
<attribute name="NAME" x="35.052" y="170.942" size="1.524" layer="95" rot="R180"/>
</instance>
<instance part="X4" gate="-2" x="38.1" y="180.34" smashed="yes">
<attribute name="NAME" x="35.052" y="181.102" size="1.524" layer="95" rot="R180"/>
</instance>
<instance part="R41" gate="G$1" x="48.26" y="152.4" smashed="yes" rot="R90">
<attribute name="NAME" x="46.7614" y="148.59" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="51.562" y="148.59" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R42" gate="G$1" x="58.42" y="149.86" smashed="yes" rot="R90">
<attribute name="NAME" x="56.9214" y="146.05" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="61.722" y="146.05" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R44" gate="G$1" x="63.5" y="165.1" smashed="yes" rot="R90">
<attribute name="NAME" x="62.0014" y="161.29" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="66.802" y="161.29" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R43" gate="G$1" x="63.5" y="175.26" smashed="yes" rot="R90">
<attribute name="NAME" x="62.0014" y="171.45" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="66.802" y="171.45" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R45" gate="G$1" x="63.5" y="144.78" smashed="yes" rot="R90">
<attribute name="NAME" x="62.0014" y="140.97" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="66.802" y="140.97" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C41" gate="G$1" x="48.26" y="142.24" smashed="yes">
<attribute name="NAME" x="49.784" y="145.161" size="1.778" layer="95"/>
<attribute name="VALUE" x="49.784" y="140.081" size="1.778" layer="96"/>
</instance>
<instance part="QN4" gate="G$1" x="40.64" y="149.86" smashed="yes" rot="MR0">
<attribute name="NAME" x="38.1" y="149.86" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="38.1" y="147.32" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="C42" gate="G$1" x="55.88" y="157.48" smashed="yes" rot="R90">
<attribute name="NAME" x="52.959" y="159.004" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="58.039" y="159.004" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C43" gate="G$1" x="73.66" y="165.1" smashed="yes">
<attribute name="NAME" x="75.184" y="168.021" size="1.778" layer="95"/>
<attribute name="VALUE" x="75.184" y="162.941" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY10" gate="GND" x="73.66" y="160.02" smashed="yes">
<attribute name="VALUE" x="71.755" y="156.845" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY11" gate="GND" x="55.88" y="137.16" smashed="yes">
<attribute name="VALUE" x="53.975" y="133.985" size="1.778" layer="96"/>
</instance>
<instance part="C44" gate="G$1" x="73.66" y="142.24" smashed="yes">
<attribute name="NAME" x="75.184" y="145.161" size="1.778" layer="95"/>
<attribute name="VALUE" x="75.184" y="140.081" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY12" gate="G$1" x="63.5" y="182.88" smashed="yes">
<attribute name="VALUE" x="61.595" y="186.055" size="1.778" layer="96"/>
</instance>
<instance part="QP5" gate="G$1" x="12.7" y="99.06" smashed="yes">
<attribute name="NAME" x="15.24" y="99.06" size="1.778" layer="95"/>
<attribute name="VALUE" x="15.24" y="96.52" size="1.778" layer="96"/>
</instance>
<instance part="X5" gate="-1" x="-12.7" y="116.84" smashed="yes">
<attribute name="VALUE" x="-15.24" y="119.38" size="1.778" layer="96"/>
<attribute name="NAME" x="-15.748" y="117.602" size="1.524" layer="95" rot="R180"/>
</instance>
<instance part="X5" gate="-2" x="-12.7" y="124.46" smashed="yes">
<attribute name="NAME" x="-15.748" y="125.222" size="1.524" layer="95" rot="R180"/>
</instance>
<instance part="R51" gate="G$1" x="-2.54" y="96.52" smashed="yes" rot="R90">
<attribute name="NAME" x="-4.0386" y="92.71" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="0.762" y="92.71" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R52" gate="G$1" x="7.62" y="93.98" smashed="yes" rot="R90">
<attribute name="NAME" x="6.1214" y="90.17" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="10.922" y="90.17" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R54" gate="G$1" x="12.7" y="109.22" smashed="yes" rot="R90">
<attribute name="NAME" x="11.2014" y="105.41" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="16.002" y="105.41" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R53" gate="G$1" x="12.7" y="119.38" smashed="yes" rot="R90">
<attribute name="NAME" x="11.2014" y="115.57" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="16.002" y="115.57" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R55" gate="G$1" x="12.7" y="88.9" smashed="yes" rot="R90">
<attribute name="NAME" x="11.2014" y="85.09" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="16.002" y="85.09" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C51" gate="G$1" x="-2.54" y="86.36" smashed="yes">
<attribute name="NAME" x="-1.016" y="89.281" size="1.778" layer="95"/>
<attribute name="VALUE" x="-1.016" y="84.201" size="1.778" layer="96"/>
</instance>
<instance part="QN5" gate="G$1" x="-10.16" y="93.98" smashed="yes" rot="MR0">
<attribute name="NAME" x="-12.7" y="93.98" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="-12.7" y="91.44" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="C52" gate="G$1" x="5.08" y="101.6" smashed="yes" rot="R90">
<attribute name="NAME" x="2.159" y="103.124" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="7.239" y="103.124" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C53" gate="G$1" x="22.86" y="109.22" smashed="yes">
<attribute name="NAME" x="24.384" y="112.141" size="1.778" layer="95"/>
<attribute name="VALUE" x="24.384" y="107.061" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY13" gate="GND" x="22.86" y="104.14" smashed="yes">
<attribute name="VALUE" x="20.955" y="100.965" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY14" gate="GND" x="5.08" y="81.28" smashed="yes">
<attribute name="VALUE" x="3.175" y="78.105" size="1.778" layer="96"/>
</instance>
<instance part="C54" gate="G$1" x="22.86" y="86.36" smashed="yes">
<attribute name="NAME" x="24.384" y="89.281" size="1.778" layer="95"/>
<attribute name="VALUE" x="24.384" y="84.201" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY15" gate="G$1" x="12.7" y="127" smashed="yes">
<attribute name="VALUE" x="10.795" y="130.175" size="1.778" layer="96"/>
</instance>
<instance part="QP6" gate="G$1" x="66.04" y="99.06" smashed="yes">
<attribute name="NAME" x="68.58" y="99.06" size="1.778" layer="95"/>
<attribute name="VALUE" x="68.58" y="96.52" size="1.778" layer="96"/>
</instance>
<instance part="X6" gate="-1" x="40.64" y="119.38" smashed="yes">
<attribute name="VALUE" x="38.1" y="121.92" size="1.778" layer="96"/>
<attribute name="NAME" x="37.592" y="120.142" size="1.524" layer="95" rot="R180"/>
</instance>
<instance part="X6" gate="-2" x="40.64" y="124.46" smashed="yes">
<attribute name="NAME" x="37.592" y="125.222" size="1.524" layer="95" rot="R180"/>
</instance>
<instance part="R61" gate="G$1" x="50.8" y="96.52" smashed="yes" rot="R90">
<attribute name="NAME" x="49.3014" y="92.71" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="54.102" y="92.71" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R62" gate="G$1" x="60.96" y="93.98" smashed="yes" rot="R90">
<attribute name="NAME" x="59.4614" y="90.17" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="64.262" y="90.17" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R64" gate="G$1" x="66.04" y="109.22" smashed="yes" rot="R90">
<attribute name="NAME" x="64.5414" y="105.41" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="69.342" y="105.41" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R63" gate="G$1" x="66.04" y="119.38" smashed="yes" rot="R90">
<attribute name="NAME" x="64.5414" y="115.57" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="69.342" y="115.57" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R65" gate="G$1" x="66.04" y="88.9" smashed="yes" rot="R90">
<attribute name="NAME" x="64.5414" y="85.09" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="69.342" y="85.09" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C61" gate="G$1" x="50.8" y="86.36" smashed="yes">
<attribute name="NAME" x="52.324" y="89.281" size="1.778" layer="95"/>
<attribute name="VALUE" x="52.324" y="84.201" size="1.778" layer="96"/>
</instance>
<instance part="QN6" gate="G$1" x="43.18" y="93.98" smashed="yes" rot="MR0">
<attribute name="NAME" x="40.64" y="93.98" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="40.64" y="91.44" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="C62" gate="G$1" x="58.42" y="101.6" smashed="yes" rot="R90">
<attribute name="NAME" x="55.499" y="103.124" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="60.579" y="103.124" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C63" gate="G$1" x="76.2" y="109.22" smashed="yes">
<attribute name="NAME" x="77.724" y="112.141" size="1.778" layer="95"/>
<attribute name="VALUE" x="77.724" y="107.061" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY16" gate="GND" x="76.2" y="104.14" smashed="yes">
<attribute name="VALUE" x="74.295" y="100.965" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY17" gate="GND" x="58.42" y="81.28" smashed="yes">
<attribute name="VALUE" x="56.515" y="78.105" size="1.778" layer="96"/>
</instance>
<instance part="C64" gate="G$1" x="76.2" y="86.36" smashed="yes">
<attribute name="NAME" x="77.724" y="89.281" size="1.778" layer="95"/>
<attribute name="VALUE" x="77.724" y="84.201" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY18" gate="G$1" x="66.04" y="127" smashed="yes">
<attribute name="VALUE" x="64.135" y="130.175" size="1.778" layer="96"/>
</instance>
<instance part="QP7" gate="G$1" x="114.3" y="213.36" smashed="yes">
<attribute name="NAME" x="116.84" y="213.36" size="1.778" layer="95"/>
<attribute name="VALUE" x="116.84" y="210.82" size="1.778" layer="96"/>
</instance>
<instance part="X7" gate="-1" x="88.9" y="228.6" smashed="yes">
<attribute name="VALUE" x="86.36" y="231.14" size="1.778" layer="96"/>
<attribute name="NAME" x="85.852" y="229.362" size="1.524" layer="95" rot="R180"/>
</instance>
<instance part="X7" gate="-2" x="88.9" y="238.76" smashed="yes">
<attribute name="NAME" x="85.852" y="239.522" size="1.524" layer="95" rot="R180"/>
</instance>
<instance part="R71" gate="G$1" x="99.06" y="210.82" smashed="yes" rot="R90">
<attribute name="NAME" x="97.5614" y="207.01" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="102.362" y="207.01" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R72" gate="G$1" x="109.22" y="208.28" smashed="yes" rot="R90">
<attribute name="NAME" x="107.7214" y="204.47" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="112.522" y="204.47" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R74" gate="G$1" x="114.3" y="223.52" smashed="yes" rot="R90">
<attribute name="NAME" x="112.8014" y="219.71" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="117.602" y="219.71" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R73" gate="G$1" x="114.3" y="233.68" smashed="yes" rot="R90">
<attribute name="NAME" x="112.8014" y="229.87" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="117.602" y="229.87" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R75" gate="G$1" x="114.3" y="203.2" smashed="yes" rot="R90">
<attribute name="NAME" x="112.8014" y="199.39" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="117.602" y="199.39" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C71" gate="G$1" x="99.06" y="200.66" smashed="yes">
<attribute name="NAME" x="100.584" y="203.581" size="1.778" layer="95"/>
<attribute name="VALUE" x="100.584" y="198.501" size="1.778" layer="96"/>
</instance>
<instance part="QN7" gate="G$1" x="91.44" y="208.28" smashed="yes" rot="MR0">
<attribute name="NAME" x="88.9" y="208.28" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="88.9" y="205.74" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="C72" gate="G$1" x="106.68" y="215.9" smashed="yes" rot="R90">
<attribute name="NAME" x="103.759" y="217.424" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="108.839" y="217.424" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C73" gate="G$1" x="124.46" y="223.52" smashed="yes">
<attribute name="NAME" x="125.984" y="226.441" size="1.778" layer="95"/>
<attribute name="VALUE" x="125.984" y="221.361" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY19" gate="GND" x="124.46" y="218.44" smashed="yes">
<attribute name="VALUE" x="122.555" y="215.265" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY20" gate="GND" x="106.68" y="195.58" smashed="yes">
<attribute name="VALUE" x="104.775" y="192.405" size="1.778" layer="96"/>
</instance>
<instance part="C74" gate="G$1" x="124.46" y="200.66" smashed="yes">
<attribute name="NAME" x="125.984" y="203.581" size="1.778" layer="95"/>
<attribute name="VALUE" x="125.984" y="198.501" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY21" gate="G$1" x="114.3" y="241.3" smashed="yes">
<attribute name="VALUE" x="112.395" y="244.475" size="1.778" layer="96"/>
</instance>
<instance part="QP8" gate="G$1" x="116.84" y="152.4" smashed="yes">
<attribute name="NAME" x="119.38" y="152.4" size="1.778" layer="95"/>
<attribute name="VALUE" x="119.38" y="149.86" size="1.778" layer="96"/>
</instance>
<instance part="X8" gate="-1" x="91.44" y="170.18" smashed="yes">
<attribute name="VALUE" x="88.9" y="172.72" size="1.778" layer="96"/>
<attribute name="NAME" x="88.392" y="170.942" size="1.524" layer="95" rot="R180"/>
</instance>
<instance part="X8" gate="-2" x="91.44" y="177.8" smashed="yes">
<attribute name="NAME" x="88.392" y="178.562" size="1.524" layer="95" rot="R180"/>
</instance>
<instance part="R81" gate="G$1" x="101.6" y="149.86" smashed="yes" rot="R90">
<attribute name="NAME" x="100.1014" y="146.05" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="104.902" y="146.05" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R82" gate="G$1" x="111.76" y="147.32" smashed="yes" rot="R90">
<attribute name="NAME" x="110.2614" y="143.51" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="115.062" y="143.51" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R84" gate="G$1" x="116.84" y="162.56" smashed="yes" rot="R90">
<attribute name="NAME" x="115.3414" y="158.75" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="120.142" y="158.75" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R83" gate="G$1" x="116.84" y="172.72" smashed="yes" rot="R90">
<attribute name="NAME" x="115.3414" y="168.91" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="120.142" y="168.91" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R85" gate="G$1" x="116.84" y="142.24" smashed="yes" rot="R90">
<attribute name="NAME" x="115.3414" y="138.43" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="120.142" y="138.43" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C81" gate="G$1" x="101.6" y="139.7" smashed="yes">
<attribute name="NAME" x="103.124" y="142.621" size="1.778" layer="95"/>
<attribute name="VALUE" x="103.124" y="137.541" size="1.778" layer="96"/>
</instance>
<instance part="QN8" gate="G$1" x="93.98" y="147.32" smashed="yes" rot="MR0">
<attribute name="NAME" x="91.44" y="147.32" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="91.44" y="144.78" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="C82" gate="G$1" x="109.22" y="154.94" smashed="yes" rot="R90">
<attribute name="NAME" x="106.299" y="156.464" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="111.379" y="156.464" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C83" gate="G$1" x="127" y="162.56" smashed="yes">
<attribute name="NAME" x="128.524" y="165.481" size="1.778" layer="95"/>
<attribute name="VALUE" x="128.524" y="160.401" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY22" gate="GND" x="127" y="157.48" smashed="yes">
<attribute name="VALUE" x="125.095" y="154.305" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY23" gate="GND" x="109.22" y="134.62" smashed="yes">
<attribute name="VALUE" x="107.315" y="131.445" size="1.778" layer="96"/>
</instance>
<instance part="C84" gate="G$1" x="127" y="139.7" smashed="yes">
<attribute name="NAME" x="128.524" y="142.621" size="1.778" layer="95"/>
<attribute name="VALUE" x="128.524" y="137.541" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY24" gate="G$1" x="116.84" y="180.34" smashed="yes">
<attribute name="VALUE" x="114.935" y="183.515" size="1.778" layer="96"/>
</instance>
<instance part="SENSOR" gate="-1" x="109.22" y="114.3" smashed="yes">
<attribute name="NAME" x="107.95" y="115.189" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="105.41" y="116.967" size="1.778" layer="96"/>
</instance>
<instance part="SENSOR" gate="-2" x="109.22" y="111.76" smashed="yes">
<attribute name="NAME" x="107.95" y="112.649" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="SENSOR" gate="-3" x="109.22" y="109.22" smashed="yes" rot="MR0">
<attribute name="NAME" x="110.49" y="110.109" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="SENSOR" gate="-4" x="109.22" y="106.68" smashed="yes" rot="MR0">
<attribute name="NAME" x="110.49" y="107.569" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="SENSOR" gate="-5" x="109.22" y="104.14" smashed="yes" rot="MR0">
<attribute name="NAME" x="110.49" y="105.029" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="SENSOR" gate="-6" x="109.22" y="101.6" smashed="yes" rot="MR0">
<attribute name="NAME" x="110.49" y="102.489" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="SENSOR" gate="-7" x="109.22" y="99.06" smashed="yes" rot="MR0">
<attribute name="NAME" x="110.49" y="99.949" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="SENSOR" gate="-8" x="109.22" y="96.52" smashed="yes" rot="R180">
<attribute name="NAME" x="110.49" y="95.631" size="1.778" layer="95"/>
</instance>
<instance part="SENSOR" gate="-9" x="109.22" y="93.98" smashed="yes" rot="R180">
<attribute name="NAME" x="110.49" y="93.091" size="1.778" layer="95"/>
</instance>
<instance part="SENSOR" gate="-10" x="109.22" y="91.44" smashed="yes" rot="MR180">
<attribute name="NAME" x="107.95" y="90.551" size="1.778" layer="95" rot="MR0"/>
</instance>
<instance part="SUPPLY25" gate="GND" x="134.62" y="111.76" smashed="yes">
<attribute name="VALUE" x="132.715" y="108.585" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY26" gate="G$1" x="134.62" y="101.6" smashed="yes">
<attribute name="VALUE" x="132.715" y="104.775" size="1.778" layer="96"/>
</instance>
<instance part="L1" gate="G$1" x="-17.78" y="63.5" smashed="yes">
<attribute name="NAME" x="-14.224" y="58.928" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-12.065" y="58.928" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="L2" gate="G$1" x="-17.78" y="55.88" smashed="yes">
<attribute name="NAME" x="-14.224" y="51.308" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-12.065" y="51.308" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="L3" gate="G$1" x="-17.78" y="48.26" smashed="yes">
<attribute name="NAME" x="-14.224" y="43.688" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-12.065" y="43.688" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="L4" gate="G$1" x="-2.54" y="63.5" smashed="yes">
<attribute name="NAME" x="1.016" y="58.928" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="3.175" y="58.928" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="L5" gate="G$1" x="-2.54" y="55.88" smashed="yes">
<attribute name="NAME" x="1.016" y="51.308" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="3.175" y="51.308" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="L6" gate="G$1" x="-2.54" y="48.26" smashed="yes">
<attribute name="NAME" x="1.016" y="43.688" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="3.175" y="43.688" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="L7" gate="G$1" x="12.7" y="63.5" smashed="yes">
<attribute name="NAME" x="16.256" y="58.928" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="18.415" y="58.928" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="L8" gate="G$1" x="12.7" y="55.88" smashed="yes">
<attribute name="NAME" x="16.256" y="51.308" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="18.415" y="51.308" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="L9" gate="G$1" x="12.7" y="48.26" smashed="yes">
<attribute name="NAME" x="16.256" y="43.688" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="18.415" y="43.688" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="QL1" gate="G$1" x="-17.78" y="35.56" smashed="yes" rot="MR0">
<attribute name="NAME" x="-20.32" y="35.56" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="-7.62" y="30.48" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="QL2" gate="G$1" x="-2.54" y="35.56" smashed="yes" rot="MR0">
<attribute name="NAME" x="-5.08" y="35.56" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="5.08" y="25.4" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="QL3" gate="G$1" x="12.7" y="35.56" smashed="yes" rot="MR0">
<attribute name="NAME" x="10.16" y="35.56" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="22.86" y="30.48" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="RL1" gate="G$1" x="-17.78" y="20.32" smashed="yes" rot="R90">
<attribute name="NAME" x="-19.2786" y="16.51" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-14.478" y="16.51" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="RL2" gate="G$1" x="-2.54" y="20.32" smashed="yes" rot="R90">
<attribute name="NAME" x="-4.0386" y="16.51" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="0.762" y="16.51" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="RL3" gate="G$1" x="12.7" y="20.32" smashed="yes" rot="R90">
<attribute name="NAME" x="11.2014" y="16.51" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="16.002" y="16.51" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="RL4" gate="G$1" x="25.4" y="20.32" smashed="yes" rot="R90">
<attribute name="NAME" x="23.9014" y="16.51" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="28.702" y="16.51" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="CV2" gate="G$1" x="139.7" y="124.46" smashed="yes" rot="R180">
<attribute name="NAME" x="138.176" y="121.539" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="138.176" y="126.619" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="CV1" gate="G$1" x="132.08" y="124.46" smashed="yes" rot="R180">
<attribute name="NAME" x="130.556" y="121.539" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="130.556" y="126.619" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="RL5" gate="G$1" x="45.72" y="43.18" smashed="yes" rot="R90">
<attribute name="NAME" x="44.2214" y="39.37" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="49.022" y="39.37" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="CL1" gate="G$1" x="60.96" y="45.72" smashed="yes">
<attribute name="NAME" x="62.484" y="48.641" size="1.778" layer="95"/>
<attribute name="VALUE" x="62.484" y="43.561" size="1.778" layer="96"/>
</instance>
<instance part="CL2" gate="G$1" x="-27.94" y="55.88" smashed="yes">
<attribute name="NAME" x="-26.416" y="58.801" size="1.778" layer="95"/>
<attribute name="VALUE" x="-26.416" y="53.721" size="1.778" layer="96"/>
</instance>
<instance part="CV3" gate="G$1" x="124.46" y="124.46" smashed="yes" rot="R180">
<attribute name="NAME" x="122.936" y="121.539" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="122.936" y="126.619" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="U$1" gate="G$1" x="101.6" y="55.88" smashed="yes"/>
<instance part="U$2" gate="G$1" x="101.6" y="50.8" smashed="yes"/>
<instance part="LED-C" gate="G$1" x="35.56" y="50.8" smashed="yes" rot="R270">
<attribute name="VALUE" x="27.94" y="54.61" size="1.778" layer="96" rot="R270"/>
<attribute name="NAME" x="41.402" y="54.61" size="1.778" layer="95" rot="R270"/>
</instance>
<instance part="FD1" gate="G$1" x="111.76" y="58.42" smashed="yes"/>
<instance part="FD2" gate="G$1" x="111.76" y="53.34" smashed="yes"/>
<instance part="FD3" gate="G$1" x="116.84" y="53.34" smashed="yes"/>
<instance part="D1" gate="1" x="-12.7" y="233.68" smashed="yes" rot="R180">
<attribute name="NAME" x="-16.256" y="235.458" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="-41.275" y="235.458" size="1.778" layer="96" rot="R270"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="N$1" class="0">
<segment>
<pinref part="R14" gate="G$1" pin="1"/>
<pinref part="QP1" gate="G$1" pin="S"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="R11" gate="G$1" pin="1"/>
<pinref part="C11" gate="G$1" pin="1"/>
<pinref part="QN1" gate="G$1" pin="G"/>
<wire x1="-5.08" y1="205.74" x2="-7.62" y2="205.74" width="0.1524" layer="91"/>
<junction x="-5.08" y="205.74"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="QN1" gate="G$1" pin="D"/>
<wire x1="-12.7" y1="213.36" x2="-12.7" y2="215.9" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="215.9" x2="-5.08" y2="215.9" width="0.1524" layer="91"/>
<pinref part="R11" gate="G$1" pin="2"/>
<pinref part="C12" gate="G$1" pin="1"/>
<pinref part="X1" gate="-1" pin="1"/>
<wire x1="-12.7" y1="228.6" x2="-12.7" y2="215.9" width="0.1524" layer="91"/>
<junction x="-12.7" y="215.9"/>
<wire x1="-2.54" y1="215.9" x2="-5.08" y2="215.9" width="0.1524" layer="91"/>
<junction x="-5.08" y="215.9"/>
<pinref part="D1" gate="1" pin="A"/>
<wire x1="-12.7" y1="231.14" x2="-12.7" y2="228.6" width="0.1524" layer="91"/>
<junction x="-12.7" y="228.6"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="5.08" y1="213.36" x2="5.08" y2="215.9" width="0.1524" layer="91"/>
<pinref part="QP1" gate="G$1" pin="G"/>
<pinref part="C12" gate="G$1" pin="2"/>
<junction x="5.08" y="215.9"/>
</segment>
</net>
<net name="LS0" class="0">
<segment>
<pinref part="QP1" gate="G$1" pin="D"/>
<pinref part="R15" gate="G$1" pin="2"/>
<wire x1="20.32" y1="208.28" x2="10.16" y2="208.28" width="0.1524" layer="91"/>
<junction x="10.16" y="208.28"/>
<pinref part="C14" gate="G$1" pin="1"/>
<wire x1="20.32" y1="208.28" x2="20.32" y2="205.74" width="0.1524" layer="91"/>
<wire x1="78.74" y1="162.56" x2="78.74" y2="190.5" width="0.1524" layer="91"/>
<wire x1="78.74" y1="190.5" x2="27.94" y2="190.5" width="0.1524" layer="91"/>
<wire x1="27.94" y1="190.5" x2="27.94" y2="208.28" width="0.1524" layer="91"/>
<wire x1="27.94" y1="208.28" x2="20.32" y2="208.28" width="0.1524" layer="91"/>
<junction x="20.32" y="208.28"/>
<wire x1="78.74" y1="162.56" x2="91.44" y2="162.56" width="0.1524" layer="91"/>
<wire x1="91.44" y1="162.56" x2="91.44" y2="109.22" width="0.1524" layer="91"/>
<pinref part="SENSOR" gate="-3" pin="KL"/>
<wire x1="91.44" y1="109.22" x2="104.14" y2="109.22" width="0.1524" layer="91"/>
<label x="22.86" y="208.28" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="R14" gate="G$1" pin="2"/>
<pinref part="R13" gate="G$1" pin="1"/>
<pinref part="C13" gate="G$1" pin="1"/>
<wire x1="20.32" y1="228.6" x2="10.16" y2="228.6" width="0.1524" layer="91"/>
<junction x="10.16" y="228.6"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="C13" gate="G$1" pin="2"/>
<pinref part="SUPPLY1" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="R15" gate="G$1" pin="1"/>
<pinref part="C11" gate="G$1" pin="2"/>
<wire x1="2.54" y1="198.12" x2="-5.08" y2="198.12" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="198.12" x2="-12.7" y2="198.12" width="0.1524" layer="91"/>
<junction x="-5.08" y="198.12"/>
<pinref part="QN1" gate="G$1" pin="S"/>
<wire x1="-12.7" y1="203.2" x2="-12.7" y2="198.12" width="0.1524" layer="91"/>
<pinref part="SUPPLY2" gate="GND" pin="GND"/>
<junction x="2.54" y="198.12"/>
<pinref part="C14" gate="G$1" pin="2"/>
<wire x1="20.32" y1="198.12" x2="10.16" y2="198.12" width="0.1524" layer="91"/>
<junction x="10.16" y="198.12"/>
<wire x1="2.54" y1="198.12" x2="5.08" y2="198.12" width="0.1524" layer="91"/>
<pinref part="R12" gate="G$1" pin="1"/>
<wire x1="5.08" y1="198.12" x2="10.16" y2="198.12" width="0.1524" layer="91"/>
<wire x1="5.08" y1="198.12" x2="5.08" y2="203.2" width="0.1524" layer="91"/>
<junction x="5.08" y="198.12"/>
</segment>
<segment>
<pinref part="C23" gate="G$1" pin="2"/>
<pinref part="SUPPLY4" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="R25" gate="G$1" pin="1"/>
<pinref part="C21" gate="G$1" pin="2"/>
<wire x1="53.34" y1="198.12" x2="45.72" y2="198.12" width="0.1524" layer="91"/>
<wire x1="45.72" y1="198.12" x2="38.1" y2="198.12" width="0.1524" layer="91"/>
<junction x="45.72" y="198.12"/>
<pinref part="QN2" gate="G$1" pin="S"/>
<wire x1="38.1" y1="203.2" x2="38.1" y2="198.12" width="0.1524" layer="91"/>
<pinref part="SUPPLY5" gate="GND" pin="GND"/>
<junction x="53.34" y="198.12"/>
<pinref part="C24" gate="G$1" pin="2"/>
<wire x1="71.12" y1="198.12" x2="60.96" y2="198.12" width="0.1524" layer="91"/>
<junction x="60.96" y="198.12"/>
<wire x1="53.34" y1="198.12" x2="55.88" y2="198.12" width="0.1524" layer="91"/>
<pinref part="R22" gate="G$1" pin="1"/>
<wire x1="55.88" y1="198.12" x2="60.96" y2="198.12" width="0.1524" layer="91"/>
<wire x1="55.88" y1="198.12" x2="55.88" y2="203.2" width="0.1524" layer="91"/>
<junction x="55.88" y="198.12"/>
</segment>
<segment>
<pinref part="C33" gate="G$1" pin="2"/>
<pinref part="SUPPLY7" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="R35" gate="G$1" pin="1"/>
<pinref part="C31" gate="G$1" pin="2"/>
<wire x1="0" y1="139.7" x2="-7.62" y2="139.7" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="139.7" x2="-15.24" y2="139.7" width="0.1524" layer="91"/>
<junction x="-7.62" y="139.7"/>
<pinref part="QN3" gate="G$1" pin="S"/>
<wire x1="-15.24" y1="144.78" x2="-15.24" y2="139.7" width="0.1524" layer="91"/>
<pinref part="SUPPLY8" gate="GND" pin="GND"/>
<junction x="0" y="139.7"/>
<pinref part="C34" gate="G$1" pin="2"/>
<wire x1="17.78" y1="139.7" x2="7.62" y2="139.7" width="0.1524" layer="91"/>
<junction x="7.62" y="139.7"/>
<wire x1="0" y1="139.7" x2="2.54" y2="139.7" width="0.1524" layer="91"/>
<pinref part="R32" gate="G$1" pin="1"/>
<wire x1="2.54" y1="139.7" x2="7.62" y2="139.7" width="0.1524" layer="91"/>
<wire x1="2.54" y1="139.7" x2="2.54" y2="144.78" width="0.1524" layer="91"/>
<junction x="2.54" y="139.7"/>
<junction x="17.78" y="139.7"/>
</segment>
<segment>
<pinref part="C43" gate="G$1" pin="2"/>
<pinref part="SUPPLY10" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="R45" gate="G$1" pin="1"/>
<pinref part="C41" gate="G$1" pin="2"/>
<wire x1="55.88" y1="139.7" x2="48.26" y2="139.7" width="0.1524" layer="91"/>
<wire x1="48.26" y1="139.7" x2="40.64" y2="139.7" width="0.1524" layer="91"/>
<junction x="48.26" y="139.7"/>
<pinref part="QN4" gate="G$1" pin="S"/>
<wire x1="40.64" y1="144.78" x2="40.64" y2="139.7" width="0.1524" layer="91"/>
<pinref part="SUPPLY11" gate="GND" pin="GND"/>
<junction x="55.88" y="139.7"/>
<pinref part="C44" gate="G$1" pin="2"/>
<wire x1="73.66" y1="139.7" x2="63.5" y2="139.7" width="0.1524" layer="91"/>
<junction x="63.5" y="139.7"/>
<wire x1="55.88" y1="139.7" x2="58.42" y2="139.7" width="0.1524" layer="91"/>
<junction x="58.42" y="139.7"/>
<wire x1="58.42" y1="139.7" x2="63.5" y2="139.7" width="0.1524" layer="91"/>
<pinref part="R42" gate="G$1" pin="1"/>
<wire x1="58.42" y1="139.7" x2="58.42" y2="144.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C53" gate="G$1" pin="2"/>
<pinref part="SUPPLY13" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C51" gate="G$1" pin="2"/>
<wire x1="5.08" y1="83.82" x2="-2.54" y2="83.82" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="83.82" x2="-10.16" y2="83.82" width="0.1524" layer="91"/>
<junction x="-2.54" y="83.82"/>
<pinref part="QN5" gate="G$1" pin="S"/>
<wire x1="-10.16" y1="88.9" x2="-10.16" y2="83.82" width="0.1524" layer="91"/>
<pinref part="SUPPLY14" gate="GND" pin="GND"/>
<pinref part="R55" gate="G$1" pin="1"/>
<pinref part="C54" gate="G$1" pin="2"/>
<wire x1="22.86" y1="83.82" x2="12.7" y2="83.82" width="0.1524" layer="91"/>
<wire x1="12.7" y1="83.82" x2="7.62" y2="83.82" width="0.1524" layer="91"/>
<pinref part="R52" gate="G$1" pin="1"/>
<wire x1="5.08" y1="83.82" x2="7.62" y2="83.82" width="0.1524" layer="91"/>
<wire x1="7.62" y1="83.82" x2="7.62" y2="88.9" width="0.1524" layer="91"/>
<junction x="7.62" y="83.82"/>
<junction x="12.7" y="83.82"/>
<junction x="5.08" y="83.82"/>
</segment>
<segment>
<pinref part="C63" gate="G$1" pin="2"/>
<pinref part="SUPPLY16" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="R65" gate="G$1" pin="1"/>
<pinref part="C61" gate="G$1" pin="2"/>
<wire x1="58.42" y1="83.82" x2="50.8" y2="83.82" width="0.1524" layer="91"/>
<wire x1="50.8" y1="83.82" x2="43.18" y2="83.82" width="0.1524" layer="91"/>
<junction x="50.8" y="83.82"/>
<pinref part="QN6" gate="G$1" pin="S"/>
<wire x1="43.18" y1="88.9" x2="43.18" y2="83.82" width="0.1524" layer="91"/>
<pinref part="SUPPLY17" gate="GND" pin="GND"/>
<junction x="58.42" y="83.82"/>
<pinref part="C64" gate="G$1" pin="2"/>
<wire x1="76.2" y1="83.82" x2="66.04" y2="83.82" width="0.1524" layer="91"/>
<junction x="66.04" y="83.82"/>
<wire x1="58.42" y1="83.82" x2="60.96" y2="83.82" width="0.1524" layer="91"/>
<pinref part="R62" gate="G$1" pin="1"/>
<wire x1="60.96" y1="83.82" x2="66.04" y2="83.82" width="0.1524" layer="91"/>
<wire x1="60.96" y1="83.82" x2="60.96" y2="88.9" width="0.1524" layer="91"/>
<junction x="60.96" y="83.82"/>
</segment>
<segment>
<pinref part="C73" gate="G$1" pin="2"/>
<pinref part="SUPPLY19" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="R75" gate="G$1" pin="1"/>
<pinref part="R72" gate="G$1" pin="1"/>
<pinref part="C71" gate="G$1" pin="2"/>
<wire x1="106.68" y1="198.12" x2="99.06" y2="198.12" width="0.1524" layer="91"/>
<wire x1="99.06" y1="198.12" x2="91.44" y2="198.12" width="0.1524" layer="91"/>
<junction x="99.06" y="198.12"/>
<pinref part="QN7" gate="G$1" pin="S"/>
<wire x1="91.44" y1="203.2" x2="91.44" y2="198.12" width="0.1524" layer="91"/>
<pinref part="SUPPLY20" gate="GND" pin="GND"/>
<junction x="106.68" y="198.12"/>
<pinref part="C74" gate="G$1" pin="2"/>
<wire x1="124.46" y1="198.12" x2="114.3" y2="198.12" width="0.1524" layer="91"/>
<junction x="114.3" y="198.12"/>
<wire x1="106.68" y1="198.12" x2="109.22" y2="198.12" width="0.1524" layer="91"/>
<wire x1="114.3" y1="198.12" x2="109.22" y2="198.12" width="0.1524" layer="91"/>
<wire x1="109.22" y1="198.12" x2="109.22" y2="203.2" width="0.1524" layer="91"/>
<junction x="109.22" y="198.12"/>
</segment>
<segment>
<pinref part="C83" gate="G$1" pin="2"/>
<pinref part="SUPPLY22" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="R85" gate="G$1" pin="1"/>
<pinref part="R82" gate="G$1" pin="1"/>
<pinref part="C81" gate="G$1" pin="2"/>
<wire x1="109.22" y1="137.16" x2="101.6" y2="137.16" width="0.1524" layer="91"/>
<wire x1="101.6" y1="137.16" x2="93.98" y2="137.16" width="0.1524" layer="91"/>
<junction x="101.6" y="137.16"/>
<pinref part="QN8" gate="G$1" pin="S"/>
<wire x1="93.98" y1="142.24" x2="93.98" y2="137.16" width="0.1524" layer="91"/>
<pinref part="SUPPLY23" gate="GND" pin="GND"/>
<junction x="109.22" y="137.16"/>
<pinref part="C84" gate="G$1" pin="2"/>
<wire x1="127" y1="137.16" x2="116.84" y2="137.16" width="0.1524" layer="91"/>
<junction x="116.84" y="137.16"/>
<wire x1="109.22" y1="137.16" x2="111.76" y2="137.16" width="0.1524" layer="91"/>
<wire x1="116.84" y1="137.16" x2="111.76" y2="137.16" width="0.1524" layer="91"/>
<wire x1="111.76" y1="137.16" x2="111.76" y2="142.24" width="0.1524" layer="91"/>
<junction x="111.76" y="137.16"/>
<junction x="127" y="137.16"/>
</segment>
<segment>
<pinref part="SENSOR" gate="-1" pin="KL"/>
<pinref part="SUPPLY25" gate="GND" pin="GND"/>
<wire x1="114.3" y1="114.3" x2="119.38" y2="114.3" width="0.1524" layer="91"/>
<pinref part="CV2" gate="G$1" pin="2"/>
<wire x1="119.38" y1="114.3" x2="134.62" y2="114.3" width="0.1524" layer="91"/>
<wire x1="139.7" y1="127" x2="132.08" y2="127" width="0.1524" layer="91"/>
<wire x1="132.08" y1="127" x2="124.46" y2="127" width="0.1524" layer="91"/>
<wire x1="124.46" y1="127" x2="119.38" y2="127" width="0.1524" layer="91"/>
<wire x1="119.38" y1="127" x2="119.38" y2="114.3" width="0.1524" layer="91"/>
<pinref part="CV1" gate="G$1" pin="2"/>
<junction x="132.08" y="127"/>
<junction x="119.38" y="114.3"/>
<pinref part="CV3" gate="G$1" pin="2"/>
<junction x="124.46" y="127"/>
</segment>
</net>
<net name="VDD" class="0">
<segment>
<pinref part="R13" gate="G$1" pin="2"/>
<pinref part="SUPPLY3" gate="G$1" pin="VDD"/>
<pinref part="X1" gate="-2" pin="1"/>
<junction x="10.16" y="238.76"/>
<pinref part="D1" gate="1" pin="C"/>
<wire x1="-12.7" y1="238.76" x2="10.16" y2="238.76" width="0.1524" layer="91"/>
<junction x="-12.7" y="238.76"/>
</segment>
<segment>
<pinref part="R23" gate="G$1" pin="2"/>
<pinref part="SUPPLY6" gate="G$1" pin="VDD"/>
<pinref part="X2" gate="-2" pin="1"/>
<wire x1="38.1" y1="238.76" x2="60.96" y2="238.76" width="0.1524" layer="91"/>
<junction x="60.96" y="238.76"/>
</segment>
<segment>
<pinref part="R33" gate="G$1" pin="2"/>
<pinref part="SUPPLY9" gate="G$1" pin="VDD"/>
<pinref part="X3" gate="-2" pin="1"/>
<wire x1="7.62" y1="180.34" x2="-15.24" y2="180.34" width="0.1524" layer="91"/>
<junction x="7.62" y="180.34"/>
</segment>
<segment>
<pinref part="R43" gate="G$1" pin="2"/>
<pinref part="SUPPLY12" gate="G$1" pin="VDD"/>
<pinref part="X4" gate="-2" pin="1"/>
<wire x1="40.64" y1="180.34" x2="63.5" y2="180.34" width="0.1524" layer="91"/>
<junction x="63.5" y="180.34"/>
</segment>
<segment>
<pinref part="R53" gate="G$1" pin="2"/>
<pinref part="SUPPLY15" gate="G$1" pin="VDD"/>
<pinref part="X5" gate="-2" pin="1"/>
<wire x1="-10.16" y1="124.46" x2="12.7" y2="124.46" width="0.1524" layer="91"/>
<junction x="12.7" y="124.46"/>
</segment>
<segment>
<pinref part="R63" gate="G$1" pin="2"/>
<pinref part="SUPPLY18" gate="G$1" pin="VDD"/>
<pinref part="X6" gate="-2" pin="1"/>
<wire x1="43.18" y1="124.46" x2="66.04" y2="124.46" width="0.1524" layer="91"/>
<junction x="66.04" y="124.46"/>
</segment>
<segment>
<pinref part="R73" gate="G$1" pin="2"/>
<pinref part="SUPPLY21" gate="G$1" pin="VDD"/>
<pinref part="X7" gate="-2" pin="1"/>
<wire x1="91.44" y1="238.76" x2="114.3" y2="238.76" width="0.1524" layer="91"/>
<junction x="114.3" y="238.76"/>
</segment>
<segment>
<pinref part="R83" gate="G$1" pin="2"/>
<pinref part="SUPPLY24" gate="G$1" pin="VDD"/>
<pinref part="X8" gate="-2" pin="1"/>
<wire x1="116.84" y1="177.8" x2="93.98" y2="177.8" width="0.1524" layer="91"/>
<junction x="116.84" y="177.8"/>
</segment>
<segment>
<pinref part="SENSOR" gate="-2" pin="KL"/>
<wire x1="114.3" y1="111.76" x2="129.54" y2="111.76" width="0.1524" layer="91"/>
<wire x1="129.54" y1="111.76" x2="129.54" y2="99.06" width="0.1524" layer="91"/>
<pinref part="SUPPLY26" gate="G$1" pin="VDD"/>
<wire x1="129.54" y1="99.06" x2="134.62" y2="99.06" width="0.1524" layer="91"/>
<pinref part="CV2" gate="G$1" pin="1"/>
<wire x1="139.7" y1="119.38" x2="139.7" y2="99.06" width="0.1524" layer="91"/>
<wire x1="139.7" y1="99.06" x2="134.62" y2="99.06" width="0.1524" layer="91"/>
<pinref part="CV1" gate="G$1" pin="1"/>
<wire x1="132.08" y1="119.38" x2="139.7" y2="119.38" width="0.1524" layer="91"/>
<junction x="139.7" y="119.38"/>
<junction x="134.62" y="99.06"/>
<pinref part="CV3" gate="G$1" pin="1"/>
<wire x1="124.46" y1="119.38" x2="132.08" y2="119.38" width="0.1524" layer="91"/>
<junction x="132.08" y="119.38"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="R24" gate="G$1" pin="1"/>
<pinref part="QP2" gate="G$1" pin="S"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="R21" gate="G$1" pin="1"/>
<pinref part="C21" gate="G$1" pin="1"/>
<pinref part="QN2" gate="G$1" pin="G"/>
<wire x1="45.72" y1="205.74" x2="43.18" y2="205.74" width="0.1524" layer="91"/>
<junction x="45.72" y="205.74"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="QN2" gate="G$1" pin="D"/>
<wire x1="38.1" y1="213.36" x2="38.1" y2="215.9" width="0.1524" layer="91"/>
<wire x1="38.1" y1="215.9" x2="45.72" y2="215.9" width="0.1524" layer="91"/>
<pinref part="R21" gate="G$1" pin="2"/>
<pinref part="C22" gate="G$1" pin="1"/>
<pinref part="X2" gate="-1" pin="1"/>
<wire x1="38.1" y1="228.6" x2="38.1" y2="215.9" width="0.1524" layer="91"/>
<junction x="38.1" y="215.9"/>
<wire x1="48.26" y1="215.9" x2="45.72" y2="215.9" width="0.1524" layer="91"/>
<junction x="45.72" y="215.9"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="R22" gate="G$1" pin="2"/>
<wire x1="55.88" y1="213.36" x2="55.88" y2="215.9" width="0.1524" layer="91"/>
<pinref part="QP2" gate="G$1" pin="G"/>
<pinref part="C22" gate="G$1" pin="2"/>
<junction x="55.88" y="215.9"/>
</segment>
</net>
<net name="LS1" class="0">
<segment>
<pinref part="QP2" gate="G$1" pin="D"/>
<pinref part="R25" gate="G$1" pin="2"/>
<wire x1="71.12" y1="208.28" x2="60.96" y2="208.28" width="0.1524" layer="91"/>
<junction x="60.96" y="208.28"/>
<pinref part="C24" gate="G$1" pin="1"/>
<wire x1="71.12" y1="208.28" x2="71.12" y2="205.74" width="0.1524" layer="91"/>
<wire x1="88.9" y1="165.1" x2="81.28" y2="165.1" width="0.1524" layer="91"/>
<wire x1="81.28" y1="165.1" x2="81.28" y2="208.28" width="0.1524" layer="91"/>
<wire x1="81.28" y1="208.28" x2="71.12" y2="208.28" width="0.1524" layer="91"/>
<junction x="71.12" y="208.28"/>
<wire x1="88.9" y1="165.1" x2="88.9" y2="106.68" width="0.1524" layer="91"/>
<pinref part="SENSOR" gate="-4" pin="KL"/>
<wire x1="88.9" y1="106.68" x2="104.14" y2="106.68" width="0.1524" layer="91"/>
<label x="76.2" y="208.28" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="R24" gate="G$1" pin="2"/>
<pinref part="R23" gate="G$1" pin="1"/>
<pinref part="C23" gate="G$1" pin="1"/>
<wire x1="71.12" y1="228.6" x2="60.96" y2="228.6" width="0.1524" layer="91"/>
<junction x="60.96" y="228.6"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="R34" gate="G$1" pin="1"/>
<pinref part="QP3" gate="G$1" pin="S"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="R31" gate="G$1" pin="1"/>
<pinref part="C31" gate="G$1" pin="1"/>
<pinref part="QN3" gate="G$1" pin="G"/>
<wire x1="-7.62" y1="147.32" x2="-10.16" y2="147.32" width="0.1524" layer="91"/>
<junction x="-7.62" y="147.32"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="QN3" gate="G$1" pin="D"/>
<wire x1="-15.24" y1="154.94" x2="-15.24" y2="157.48" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="157.48" x2="-7.62" y2="157.48" width="0.1524" layer="91"/>
<pinref part="R31" gate="G$1" pin="2"/>
<pinref part="C32" gate="G$1" pin="1"/>
<pinref part="X3" gate="-1" pin="1"/>
<wire x1="-15.24" y1="175.26" x2="-15.24" y2="157.48" width="0.1524" layer="91"/>
<junction x="-15.24" y="157.48"/>
<wire x1="-5.08" y1="157.48" x2="-7.62" y2="157.48" width="0.1524" layer="91"/>
<junction x="-7.62" y="157.48"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="R32" gate="G$1" pin="2"/>
<wire x1="2.54" y1="154.94" x2="2.54" y2="157.48" width="0.1524" layer="91"/>
<pinref part="QP3" gate="G$1" pin="G"/>
<pinref part="C32" gate="G$1" pin="2"/>
<junction x="2.54" y="157.48"/>
</segment>
</net>
<net name="LS3" class="0">
<segment>
<pinref part="QP3" gate="G$1" pin="D"/>
<pinref part="R35" gate="G$1" pin="2"/>
<wire x1="17.78" y1="149.86" x2="7.62" y2="149.86" width="0.1524" layer="91"/>
<junction x="7.62" y="149.86"/>
<pinref part="C34" gate="G$1" pin="1"/>
<wire x1="17.78" y1="149.86" x2="17.78" y2="147.32" width="0.1524" layer="91"/>
<wire x1="86.36" y1="132.08" x2="25.4" y2="132.08" width="0.1524" layer="91"/>
<wire x1="25.4" y1="132.08" x2="25.4" y2="149.86" width="0.1524" layer="91"/>
<wire x1="25.4" y1="149.86" x2="17.78" y2="149.86" width="0.1524" layer="91"/>
<junction x="17.78" y="149.86"/>
<wire x1="86.36" y1="104.14" x2="86.36" y2="132.08" width="0.1524" layer="91"/>
<pinref part="SENSOR" gate="-5" pin="KL"/>
<wire x1="86.36" y1="104.14" x2="104.14" y2="104.14" width="0.1524" layer="91"/>
<label x="20.32" y="149.86" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="R34" gate="G$1" pin="2"/>
<pinref part="R33" gate="G$1" pin="1"/>
<pinref part="C33" gate="G$1" pin="1"/>
<wire x1="17.78" y1="170.18" x2="7.62" y2="170.18" width="0.1524" layer="91"/>
<junction x="7.62" y="170.18"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="R44" gate="G$1" pin="1"/>
<pinref part="QP4" gate="G$1" pin="S"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="R41" gate="G$1" pin="1"/>
<pinref part="C41" gate="G$1" pin="1"/>
<pinref part="QN4" gate="G$1" pin="G"/>
<wire x1="48.26" y1="147.32" x2="45.72" y2="147.32" width="0.1524" layer="91"/>
<junction x="48.26" y="147.32"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="QN4" gate="G$1" pin="D"/>
<wire x1="40.64" y1="154.94" x2="40.64" y2="157.48" width="0.1524" layer="91"/>
<wire x1="40.64" y1="157.48" x2="48.26" y2="157.48" width="0.1524" layer="91"/>
<pinref part="R41" gate="G$1" pin="2"/>
<pinref part="C42" gate="G$1" pin="1"/>
<pinref part="X4" gate="-1" pin="1"/>
<wire x1="40.64" y1="170.18" x2="40.64" y2="157.48" width="0.1524" layer="91"/>
<junction x="40.64" y="157.48"/>
<wire x1="50.8" y1="157.48" x2="48.26" y2="157.48" width="0.1524" layer="91"/>
<junction x="48.26" y="157.48"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="R42" gate="G$1" pin="2"/>
<wire x1="58.42" y1="154.94" x2="58.42" y2="157.48" width="0.1524" layer="91"/>
<pinref part="QP4" gate="G$1" pin="G"/>
<pinref part="C42" gate="G$1" pin="2"/>
<junction x="58.42" y="157.48"/>
</segment>
</net>
<net name="LS4" class="0">
<segment>
<pinref part="QP4" gate="G$1" pin="D"/>
<pinref part="R45" gate="G$1" pin="2"/>
<pinref part="C44" gate="G$1" pin="1"/>
<wire x1="73.66" y1="147.32" x2="73.66" y2="149.86" width="0.1524" layer="91"/>
<wire x1="73.66" y1="149.86" x2="63.5" y2="149.86" width="0.1524" layer="91"/>
<junction x="63.5" y="149.86"/>
<wire x1="83.82" y1="149.86" x2="73.66" y2="149.86" width="0.1524" layer="91"/>
<junction x="73.66" y="149.86"/>
<wire x1="83.82" y1="149.86" x2="83.82" y2="101.6" width="0.1524" layer="91"/>
<pinref part="SENSOR" gate="-6" pin="KL"/>
<wire x1="83.82" y1="101.6" x2="104.14" y2="101.6" width="0.1524" layer="91"/>
<label x="78.74" y="149.86" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="R44" gate="G$1" pin="2"/>
<pinref part="R43" gate="G$1" pin="1"/>
<pinref part="C43" gate="G$1" pin="1"/>
<wire x1="73.66" y1="170.18" x2="63.5" y2="170.18" width="0.1524" layer="91"/>
<junction x="63.5" y="170.18"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="R54" gate="G$1" pin="1"/>
<pinref part="QP5" gate="G$1" pin="S"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="R51" gate="G$1" pin="1"/>
<pinref part="C51" gate="G$1" pin="1"/>
<pinref part="QN5" gate="G$1" pin="G"/>
<wire x1="-2.54" y1="91.44" x2="-5.08" y2="91.44" width="0.1524" layer="91"/>
<junction x="-2.54" y="91.44"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="QN5" gate="G$1" pin="D"/>
<wire x1="-10.16" y1="99.06" x2="-10.16" y2="101.6" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="101.6" x2="-2.54" y2="101.6" width="0.1524" layer="91"/>
<pinref part="R51" gate="G$1" pin="2"/>
<pinref part="C52" gate="G$1" pin="1"/>
<pinref part="X5" gate="-1" pin="1"/>
<wire x1="-10.16" y1="101.6" x2="-10.16" y2="116.84" width="0.1524" layer="91"/>
<junction x="-10.16" y="101.6"/>
<wire x1="0" y1="101.6" x2="-2.54" y2="101.6" width="0.1524" layer="91"/>
<junction x="-2.54" y="101.6"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="R52" gate="G$1" pin="2"/>
<wire x1="7.62" y1="99.06" x2="7.62" y2="101.6" width="0.1524" layer="91"/>
<pinref part="QP5" gate="G$1" pin="G"/>
<pinref part="C52" gate="G$1" pin="2"/>
<junction x="7.62" y="101.6"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="R54" gate="G$1" pin="2"/>
<pinref part="R53" gate="G$1" pin="1"/>
<pinref part="C53" gate="G$1" pin="1"/>
<wire x1="22.86" y1="114.3" x2="12.7" y2="114.3" width="0.1524" layer="91"/>
<junction x="12.7" y="114.3"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="R64" gate="G$1" pin="1"/>
<pinref part="QP6" gate="G$1" pin="S"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="R61" gate="G$1" pin="1"/>
<pinref part="C61" gate="G$1" pin="1"/>
<pinref part="QN6" gate="G$1" pin="G"/>
<wire x1="50.8" y1="91.44" x2="48.26" y2="91.44" width="0.1524" layer="91"/>
<junction x="50.8" y="91.44"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="QN6" gate="G$1" pin="D"/>
<wire x1="43.18" y1="99.06" x2="43.18" y2="101.6" width="0.1524" layer="91"/>
<wire x1="43.18" y1="101.6" x2="50.8" y2="101.6" width="0.1524" layer="91"/>
<pinref part="R61" gate="G$1" pin="2"/>
<pinref part="C62" gate="G$1" pin="1"/>
<pinref part="X6" gate="-1" pin="1"/>
<wire x1="43.18" y1="119.38" x2="43.18" y2="101.6" width="0.1524" layer="91"/>
<junction x="43.18" y="101.6"/>
<wire x1="53.34" y1="101.6" x2="50.8" y2="101.6" width="0.1524" layer="91"/>
<junction x="50.8" y="101.6"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<pinref part="R62" gate="G$1" pin="2"/>
<wire x1="60.96" y1="99.06" x2="60.96" y2="101.6" width="0.1524" layer="91"/>
<pinref part="QP6" gate="G$1" pin="G"/>
<pinref part="C62" gate="G$1" pin="2"/>
<junction x="60.96" y="101.6"/>
</segment>
</net>
<net name="LS7" class="0">
<segment>
<pinref part="QP6" gate="G$1" pin="D"/>
<pinref part="R65" gate="G$1" pin="2"/>
<wire x1="76.2" y1="93.98" x2="66.04" y2="93.98" width="0.1524" layer="91"/>
<junction x="66.04" y="93.98"/>
<pinref part="C64" gate="G$1" pin="1"/>
<wire x1="76.2" y1="93.98" x2="76.2" y2="91.44" width="0.1524" layer="91"/>
<wire x1="76.2" y1="93.98" x2="86.36" y2="93.98" width="0.1524" layer="91"/>
<wire x1="86.36" y1="93.98" x2="86.36" y2="96.52" width="0.1524" layer="91"/>
<pinref part="SENSOR" gate="-8" pin="KL"/>
<wire x1="86.36" y1="96.52" x2="104.14" y2="96.52" width="0.1524" layer="91"/>
<junction x="76.2" y="93.98"/>
<label x="76.2" y="93.98" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="R64" gate="G$1" pin="2"/>
<pinref part="R63" gate="G$1" pin="1"/>
<pinref part="C63" gate="G$1" pin="1"/>
<wire x1="76.2" y1="114.3" x2="66.04" y2="114.3" width="0.1524" layer="91"/>
<junction x="66.04" y="114.3"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="R74" gate="G$1" pin="1"/>
<pinref part="QP7" gate="G$1" pin="S"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<pinref part="R71" gate="G$1" pin="1"/>
<pinref part="C71" gate="G$1" pin="1"/>
<pinref part="QN7" gate="G$1" pin="G"/>
<wire x1="99.06" y1="205.74" x2="96.52" y2="205.74" width="0.1524" layer="91"/>
<junction x="99.06" y="205.74"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="QN7" gate="G$1" pin="D"/>
<wire x1="91.44" y1="213.36" x2="91.44" y2="215.9" width="0.1524" layer="91"/>
<wire x1="91.44" y1="215.9" x2="99.06" y2="215.9" width="0.1524" layer="91"/>
<pinref part="R71" gate="G$1" pin="2"/>
<pinref part="C72" gate="G$1" pin="1"/>
<pinref part="X7" gate="-1" pin="1"/>
<wire x1="91.44" y1="228.6" x2="91.44" y2="215.9" width="0.1524" layer="91"/>
<junction x="91.44" y="215.9"/>
<wire x1="101.6" y1="215.9" x2="99.06" y2="215.9" width="0.1524" layer="91"/>
<junction x="99.06" y="215.9"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<pinref part="R72" gate="G$1" pin="2"/>
<wire x1="109.22" y1="213.36" x2="109.22" y2="215.9" width="0.1524" layer="91"/>
<pinref part="QP7" gate="G$1" pin="G"/>
<pinref part="C72" gate="G$1" pin="2"/>
<junction x="109.22" y="215.9"/>
</segment>
</net>
<net name="LS2" class="0">
<segment>
<pinref part="QP7" gate="G$1" pin="D"/>
<pinref part="R75" gate="G$1" pin="2"/>
<wire x1="124.46" y1="208.28" x2="114.3" y2="208.28" width="0.1524" layer="91"/>
<junction x="114.3" y="208.28"/>
<pinref part="C74" gate="G$1" pin="1"/>
<wire x1="124.46" y1="208.28" x2="124.46" y2="205.74" width="0.1524" layer="91"/>
<wire x1="93.98" y1="129.54" x2="137.16" y2="129.54" width="0.1524" layer="91"/>
<wire x1="137.16" y1="129.54" x2="137.16" y2="208.28" width="0.1524" layer="91"/>
<wire x1="137.16" y1="208.28" x2="124.46" y2="208.28" width="0.1524" layer="91"/>
<junction x="124.46" y="208.28"/>
<wire x1="93.98" y1="129.54" x2="93.98" y2="93.98" width="0.1524" layer="91"/>
<pinref part="SENSOR" gate="-9" pin="KL"/>
<wire x1="93.98" y1="93.98" x2="104.14" y2="93.98" width="0.1524" layer="91"/>
<label x="132.08" y="208.28" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<pinref part="R74" gate="G$1" pin="2"/>
<pinref part="R73" gate="G$1" pin="1"/>
<pinref part="C73" gate="G$1" pin="1"/>
<wire x1="124.46" y1="228.6" x2="114.3" y2="228.6" width="0.1524" layer="91"/>
<junction x="114.3" y="228.6"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<pinref part="R84" gate="G$1" pin="1"/>
<pinref part="QP8" gate="G$1" pin="S"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<pinref part="R81" gate="G$1" pin="1"/>
<pinref part="C81" gate="G$1" pin="1"/>
<pinref part="QN8" gate="G$1" pin="G"/>
<wire x1="101.6" y1="144.78" x2="99.06" y2="144.78" width="0.1524" layer="91"/>
<junction x="101.6" y="144.78"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<pinref part="QN8" gate="G$1" pin="D"/>
<wire x1="93.98" y1="152.4" x2="93.98" y2="154.94" width="0.1524" layer="91"/>
<wire x1="93.98" y1="154.94" x2="101.6" y2="154.94" width="0.1524" layer="91"/>
<pinref part="R81" gate="G$1" pin="2"/>
<pinref part="C82" gate="G$1" pin="1"/>
<pinref part="X8" gate="-1" pin="1"/>
<wire x1="93.98" y1="170.18" x2="93.98" y2="154.94" width="0.1524" layer="91"/>
<junction x="93.98" y="154.94"/>
<wire x1="104.14" y1="154.94" x2="101.6" y2="154.94" width="0.1524" layer="91"/>
<junction x="101.6" y="154.94"/>
</segment>
</net>
<net name="N$47" class="0">
<segment>
<pinref part="R82" gate="G$1" pin="2"/>
<wire x1="111.76" y1="152.4" x2="111.76" y2="154.94" width="0.1524" layer="91"/>
<pinref part="QP8" gate="G$1" pin="G"/>
<pinref part="C82" gate="G$1" pin="2"/>
<junction x="111.76" y="154.94"/>
</segment>
</net>
<net name="LS5" class="0">
<segment>
<wire x1="142.24" y1="91.44" x2="142.24" y2="147.32" width="0.1524" layer="91"/>
<pinref part="QP8" gate="G$1" pin="D"/>
<pinref part="R85" gate="G$1" pin="2"/>
<wire x1="127" y1="147.32" x2="116.84" y2="147.32" width="0.1524" layer="91"/>
<junction x="116.84" y="147.32"/>
<pinref part="C84" gate="G$1" pin="1"/>
<wire x1="127" y1="147.32" x2="127" y2="144.78" width="0.1524" layer="91"/>
<wire x1="142.24" y1="147.32" x2="127" y2="147.32" width="0.1524" layer="91"/>
<junction x="127" y="147.32"/>
<pinref part="SENSOR" gate="-10" pin="KL"/>
<wire x1="142.24" y1="91.44" x2="114.3" y2="91.44" width="0.1524" layer="91"/>
<label x="139.7" y="147.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$49" class="0">
<segment>
<pinref part="R84" gate="G$1" pin="2"/>
<pinref part="R83" gate="G$1" pin="1"/>
<pinref part="C83" gate="G$1" pin="1"/>
<wire x1="127" y1="167.64" x2="116.84" y2="167.64" width="0.1524" layer="91"/>
<junction x="116.84" y="167.64"/>
</segment>
</net>
<net name="LS6" class="0">
<segment>
<pinref part="QP5" gate="G$1" pin="D"/>
<pinref part="R55" gate="G$1" pin="2"/>
<wire x1="12.7" y1="93.98" x2="22.86" y2="93.98" width="0.1524" layer="91"/>
<junction x="12.7" y="93.98"/>
<pinref part="C54" gate="G$1" pin="1"/>
<wire x1="22.86" y1="93.98" x2="22.86" y2="91.44" width="0.1524" layer="91"/>
<wire x1="22.86" y1="93.98" x2="30.48" y2="93.98" width="0.1524" layer="91"/>
<wire x1="30.48" y1="93.98" x2="30.48" y2="78.74" width="0.1524" layer="91"/>
<wire x1="30.48" y1="78.74" x2="83.82" y2="78.74" width="0.1524" layer="91"/>
<junction x="22.86" y="93.98"/>
<wire x1="83.82" y1="78.74" x2="83.82" y2="99.06" width="0.1524" layer="91"/>
<pinref part="SENSOR" gate="-7" pin="KL"/>
<wire x1="83.82" y1="99.06" x2="104.14" y2="99.06" width="0.1524" layer="91"/>
<label x="25.4" y="93.98" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="L1" gate="G$1" pin="C"/>
<pinref part="L2" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="L2" gate="G$1" pin="C"/>
<pinref part="L3" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="L4" gate="G$1" pin="C"/>
<pinref part="L5" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="L5" gate="G$1" pin="C"/>
<pinref part="L6" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="L7" gate="G$1" pin="C"/>
<pinref part="L8" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="L8" gate="G$1" pin="C"/>
<pinref part="L9" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<pinref part="L3" gate="G$1" pin="C"/>
<pinref part="QL1" gate="G$1" pin="D"/>
<wire x1="-17.78" y1="43.18" x2="-17.78" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$48" class="0">
<segment>
<pinref part="L6" gate="G$1" pin="C"/>
<pinref part="QL2" gate="G$1" pin="D"/>
<wire x1="-2.54" y1="43.18" x2="-2.54" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$50" class="0">
<segment>
<pinref part="L9" gate="G$1" pin="C"/>
<pinref part="QL3" gate="G$1" pin="D"/>
<wire x1="12.7" y1="40.64" x2="12.7" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$51" class="0">
<segment>
<pinref part="QL1" gate="G$1" pin="S"/>
<pinref part="RL1" gate="G$1" pin="2"/>
<wire x1="-17.78" y1="30.48" x2="-17.78" y2="25.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$52" class="0">
<segment>
<pinref part="QL2" gate="G$1" pin="S"/>
<pinref part="RL2" gate="G$1" pin="2"/>
<wire x1="-2.54" y1="30.48" x2="-2.54" y2="25.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$53" class="0">
<segment>
<pinref part="QL3" gate="G$1" pin="S"/>
<pinref part="RL3" gate="G$1" pin="2"/>
<wire x1="12.7" y1="30.48" x2="12.7" y2="25.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GND2" class="0">
<segment>
<pinref part="RL3" gate="G$1" pin="1"/>
<wire x1="-2.54" y1="15.24" x2="12.7" y2="15.24" width="0.1524" layer="91"/>
<wire x1="38.1" y1="15.24" x2="33.02" y2="15.24" width="0.1524" layer="91"/>
<pinref part="RL2" gate="G$1" pin="1"/>
<pinref part="RL1" gate="G$1" pin="1"/>
<wire x1="33.02" y1="15.24" x2="12.7" y2="15.24" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="15.24" x2="-17.78" y2="15.24" width="0.1524" layer="91"/>
<pinref part="RL4" gate="G$1" pin="1"/>
<wire x1="25.4" y1="15.24" x2="12.7" y2="15.24" width="0.1524" layer="91"/>
<wire x1="25.4" y1="15.24" x2="33.02" y2="15.24" width="0.1524" layer="91"/>
<wire x1="33.02" y1="15.24" x2="33.02" y2="43.18" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="15.24" x2="-17.78" y2="15.24" width="0.1524" layer="91"/>
<junction x="25.4" y="15.24"/>
<junction x="12.7" y="15.24"/>
<junction x="-2.54" y="15.24"/>
<junction x="-17.78" y="15.24"/>
<wire x1="38.1" y1="33.02" x2="38.1" y2="15.24" width="0.1524" layer="91"/>
<pinref part="CL2" gate="G$1" pin="2"/>
<wire x1="-27.94" y1="53.34" x2="-27.94" y2="15.24" width="0.1524" layer="91"/>
<pinref part="CL1" gate="G$1" pin="2"/>
<wire x1="60.96" y1="43.18" x2="60.96" y2="33.02" width="0.1524" layer="91"/>
<wire x1="60.96" y1="33.02" x2="38.1" y2="33.02" width="0.1524" layer="91"/>
<junction x="33.02" y="15.24"/>
<pinref part="LED-C" gate="G$1" pin="1"/>
</segment>
</net>
<net name="VDD2" class="0">
<segment>
<pinref part="L1" gate="G$1" pin="A"/>
<pinref part="L4" gate="G$1" pin="A"/>
<pinref part="L7" gate="G$1" pin="A"/>
<wire x1="-17.78" y1="66.04" x2="-2.54" y2="66.04" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="66.04" x2="12.7" y2="66.04" width="0.1524" layer="91"/>
<wire x1="60.96" y1="66.04" x2="12.7" y2="66.04" width="0.1524" layer="91"/>
<wire x1="33.02" y1="58.42" x2="33.02" y2="66.04" width="0.1524" layer="91"/>
<wire x1="33.02" y1="66.04" x2="12.7" y2="66.04" width="0.1524" layer="91"/>
<junction x="12.7" y="66.04"/>
<junction x="-2.54" y="66.04"/>
<pinref part="CL2" gate="G$1" pin="1"/>
<wire x1="-27.94" y1="60.96" x2="-27.94" y2="66.04" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="66.04" x2="-17.78" y2="66.04" width="0.1524" layer="91"/>
<pinref part="CL1" gate="G$1" pin="1"/>
<wire x1="60.96" y1="50.8" x2="60.96" y2="66.04" width="0.1524" layer="91"/>
<junction x="-17.78" y="66.04"/>
<pinref part="LED-C" gate="G$1" pin="2"/>
</segment>
</net>
<net name="GATE_CTRL" class="0">
<segment>
<pinref part="QL1" gate="G$1" pin="G"/>
<wire x1="-12.7" y1="33.02" x2="-12.7" y2="27.94" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="27.94" x2="2.54" y2="27.94" width="0.1524" layer="91"/>
<pinref part="QL2" gate="G$1" pin="G"/>
<wire x1="2.54" y1="33.02" x2="2.54" y2="27.94" width="0.1524" layer="91"/>
<wire x1="2.54" y1="27.94" x2="17.78" y2="27.94" width="0.1524" layer="91"/>
<pinref part="QL3" gate="G$1" pin="G"/>
<wire x1="17.78" y1="27.94" x2="17.78" y2="33.02" width="0.1524" layer="91"/>
<wire x1="17.78" y1="33.02" x2="25.4" y2="33.02" width="0.1524" layer="91"/>
<pinref part="RL4" gate="G$1" pin="2"/>
<wire x1="25.4" y1="25.4" x2="25.4" y2="33.02" width="0.1524" layer="91"/>
<wire x1="25.4" y1="33.02" x2="25.4" y2="40.64" width="0.1524" layer="91"/>
<wire x1="25.4" y1="40.64" x2="35.56" y2="40.64" width="0.1524" layer="91"/>
<wire x1="35.56" y1="40.64" x2="35.56" y2="43.18" width="0.1524" layer="91"/>
<junction x="17.78" y="33.02"/>
<junction x="25.4" y="33.02"/>
<junction x="2.54" y="27.94"/>
<pinref part="RL5" gate="G$1" pin="1"/>
<wire x1="45.72" y1="38.1" x2="45.72" y2="35.56" width="0.1524" layer="91"/>
<wire x1="45.72" y1="35.56" x2="35.56" y2="35.56" width="0.1524" layer="91"/>
<wire x1="35.56" y1="35.56" x2="35.56" y2="40.64" width="0.1524" layer="91"/>
<junction x="35.56" y="40.64"/>
<pinref part="LED-C" gate="G$1" pin="3"/>
</segment>
</net>
<net name="N$54" class="0">
<segment>
<pinref part="RL5" gate="G$1" pin="2"/>
<wire x1="45.72" y1="48.26" x2="45.72" y2="63.5" width="0.1524" layer="91"/>
<wire x1="45.72" y1="63.5" x2="35.56" y2="63.5" width="0.1524" layer="91"/>
<wire x1="35.56" y1="63.5" x2="35.56" y2="58.42" width="0.1524" layer="91"/>
<pinref part="LED-C" gate="G$1" pin="4"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
