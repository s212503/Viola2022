% Mission ArUco log started at 2020-01-05 13:33:33.298
% 1   Time [sec]
% 3   image number
% 4   ArUco ID
% 5-7 Position (x,y,z) [m] (robot coordinates)
% 8   Distance to marker [m]
% 9   Marker angle [radians] - assumed vertical marker.
% 10  Marker is vertical (on a wall)
% 11  Processing time [sec].
1578227630.884 1333 63 0.753 0.439 0.122  0.872 -0.2668 1 0.213
